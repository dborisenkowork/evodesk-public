import {EvoDeskRESTApiEnvironmentConfig} from '../modules/app/EvodeskRESTApi/configs/EvodeskRESTApiEnvironment.config';

export enum ENVIRONMENT
{
    // noinspection JSUnusedGlobalSymbols
    Dev = <any>'dev', /* Development (default) environment. Use it for development purposes. */
    Mock = <any>'mock', /* Development environment with mock data. Use it for development purposes. */
    Test = <any>'test', /* Test environment. Used by ng test command. */
    Prod = <any>'prod', /* Prod environment. AVOID using it in real production, consider to use Build environment instead. */
    Build = <any>'build', /* Build with replacement string for injection build-specific configuration. Can be prod, stage, QA or anything else. */
    QA = <any>'qa', /* QA environment. Use it to deploy features for QA */
    Stage = <any>'stage', /* Stage environment. Use it to demonstrate features for end users */
}

export interface EnvironmentConfiguration
{
    env: ENVIRONMENT;
    serverAssetsURL: string;
    serverHostName: string;
    angular: {
        useHashStrategy: boolean;
        enableProductionMode: boolean;
        enableRouterTracing: boolean;
    };
    availableLocales: Array<string>;
    mobileAppLinks: {
        osx: string;
        android: string;
    };
    socialNetworkLinks: {
        vk: string;
        telegram: string;
        twitter: string;
        facebook: string;
        youtube: string;
    };
    modules: {
        EvoDeskRESTApi: EvoDeskRESTApiEnvironmentConfig;
        payeer: {
            sendUrl: string;
            m_shop: string;
        };
    };
}
