import {Provider} from '@angular/core';

import {EnvironmentConfiguration} from './environments';

import {environment as devEnvironment} from './environment-dev.config';
import {api as devApi} from './environment-dev.config';

export const environment: EnvironmentConfiguration = devEnvironment;
export const api: Array<Provider> = devApi;
