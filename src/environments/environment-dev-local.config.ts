import {Provider} from '@angular/core';

import {ENVIRONMENT, EnvironmentConfiguration} from './environments';

import {httpApi} from './apis/http.config';
import {environmentDefaults} from './defaults.config';

export const api: Array<Provider> = httpApi;

export const environment: EnvironmentConfiguration = {
    ...environmentDefaults,
    env: ENVIRONMENT.Dev,
    serverHostName: 'http://127.0.0.1',
    serverAssetsURL: 'http://127.0.0.1/api',
    modules: {
        EvoDeskRESTApi: {
            apiEndpoint: 'http://127.0.0.1/api',
            serverHostName: 'http://127.0.0.1',
            payeer: {
                sendUrl: 'https://payeer.com/merchant/',
                m_shop: '483711525',
            },
        },
    },
};
