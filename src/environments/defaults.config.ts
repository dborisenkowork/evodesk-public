export const environmentDefaults: any = {
    angular: {
        useHashStrategy: false,
        enableProductionMode: false,
        enableRouterTracing: false,
    },
    mobileAppLinks: {
        osx: 'https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1299422103',
        android: 'https://play.google.com/store/apps/details?id=ru.evodesk.dnk',
    },
    socialNetworkLinks: {
        vk: 'https://vk.com/evodesk',
        telegram: 'https://t.me/evochain',
        twitter: 'https://www.facebook.com/EvodeskDNK',
        facebook: 'https://twitter.com/EvoDeskDNK',
        youtube: 'https://www.youtube.com/channel/UCRWDhz0SwVzo1Nxx-1ztjDQ',
    },
    availableLocales: [
        'ru_RU',
        'en_GB',
    ],
};
