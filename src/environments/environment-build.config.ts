import {Provider} from '@angular/core';

import {EnvironmentConfiguration} from './environments';

import {httpApi} from './apis/http.config';

export const api: Array<Provider> = httpApi;

export const environment: EnvironmentConfiguration = <any> 'e';
