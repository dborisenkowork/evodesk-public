import {Provider} from '@angular/core';

import {ENVIRONMENT, EnvironmentConfiguration} from './environments';

import {httpApi} from './apis/http.config';
import {environmentDefaults} from './defaults.config';

export const api: Array<Provider> = httpApi;

export const environment: EnvironmentConfiguration = {
    ...environmentDefaults,
    env: ENVIRONMENT.Dev,
    serverHostName: 'https://dev.evodesk-dev.one',
    serverAssetsURL: 'https://dev.evodesk-dev.one/api',
    modules: {
        EvoDeskRESTApi: {
            apiEndpoint: 'https://dev.evodesk-dev.one/api',
            serverHostName: 'https://dev.evodesk-dev.one',
            payeer: {
                sendUrl: 'https://payeer.com/merchant/',
                m_shop: '483711525',
            },
        },
    },
};
