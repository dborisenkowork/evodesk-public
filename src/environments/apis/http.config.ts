import {Provider} from '@angular/core';

import {SyncCompetenceScript} from '../../modules/app/EvodeskRESTApi/scripts/SyncCompetence.script';

import {UserRESTService} from '../../modules/app/EvodeskRESTApi/services/UserREST.service';
import {PartnersRESTService} from '../../modules/app/EvodeskRESTApi/services/PartnersREST.service';
import {BillingRESTService} from '../../modules/app/EvodeskRESTApi/services/BillingREST.service';
import {LeadersRESTService} from '../../modules/app/EvodeskRESTApi/services/LeadersREST.service';
import {RatingRESTService} from '../../modules/app/EvodeskRESTApi/services/RatingREST.service';
import {FaqRESTService} from '../../modules/app/EvodeskRESTApi/services/FaqREST.service';
import {ShopRESTService} from '../../modules/app/EvodeskRESTApi/services/ShopREST.service';
import {NotificationRESTService} from '../../modules/app/EvodeskRESTApi/services/NotificationREST.service';
import {ProfileRESTService} from '../../modules/app/EvodeskRESTApi/services/ProfileREST.service';
import {LandingRESTService} from '../../modules/app/EvodeskRESTApi/services/LandingREST.service';
import {IndustryRESTService} from '../../modules/app/EvodeskRESTApi/services/IndustryREST.service';
import {PostRESTService} from '../../modules/app/EvodeskRESTApi/services/PostREST.service';
import {AttachmentRESTService} from '../../modules/app/EvodeskRESTApi/services/AttachmentREST.service';
import {SearchRESTService} from '../../modules/app/EvodeskRESTApi/services/SearchREST.service';
import {PostCommentRESTService} from '../../modules/app/EvodeskRESTApi/services/PostCommentREST.service';
import {AlbumsRESTService} from '../../modules/app/EvodeskRESTApi/services/AlbumsREST.service';
import {ProfileEducationRESTService} from '../../modules/app/EvodeskRESTApi/services/ProfileEducationREST.service';
import {ProfileWorkExperienceRESTService} from '../../modules/app/EvodeskRESTApi/services/ProfileWorkExperienceREST.service';
import {NewsRESTService} from '../../modules/app/EvodeskRESTApi/services/NewsREST.service';
import {InstantMessagesRESTService} from '../../modules/app/EvodeskRESTApi/services/InstantMessagesREST.service';
import {CompetenceRESTService} from '../../modules/app/EvodeskRESTApi/services/CompetenceREST.service';
import {NotificationSettingsRESTService} from '../../modules/app/EvodeskRESTApi/services/NotificationSettingsREST.service';
import {DeskRESTService} from '../../modules/app/EvodeskRESTApi/services/DeskREST.service';
import {DeskDashboardRESTService} from '../../modules/app/EvodeskRESTApi/services/DeskDashboardREST.service';

export const httpApi: Array<Provider> = [
    SyncCompetenceScript,

    UserRESTService,
    PartnersRESTService,
    BillingRESTService,
    LeadersRESTService,
    RatingRESTService,
    FaqRESTService,
    ShopRESTService,
    NotificationRESTService,
    ProfileRESTService,
    LandingRESTService,
    IndustryRESTService,
    PostRESTService,
    PostCommentRESTService,
    AttachmentRESTService,
    SearchRESTService,
    ProfileEducationRESTService,
    ProfileWorkExperienceRESTService,
    AlbumsRESTService,
    NewsRESTService,
    InstantMessagesRESTService,
    CompetenceRESTService,
    NotificationSettingsRESTService,
    DeskRESTService,
    DeskDashboardRESTService,
];
