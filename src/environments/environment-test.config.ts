import {Provider} from '@angular/core';

import {ENVIRONMENT, EnvironmentConfiguration} from './environments';

import {httpApi} from './apis/http.config';
import {environmentDefaults} from './defaults.config';

export const api: Array<Provider> = httpApi;

export const environment: EnvironmentConfiguration = {
    ...environmentDefaults,
    env: ENVIRONMENT.Test,
    serverHostName: 'http://0.0.0.0',
    serverAssetsURL: 'http://0.0.0.0',
    modules: {
        EvoDeskRESTApi: {
            apiEndpoint: 'http://0.0.0.0',
            serverHostName: 'http://0.0.0.0',
            payeer: {
                sendUrl: 'https://payeer.com/merchant/',
                m_shop: '483711525',
            },
        },
    },
};
