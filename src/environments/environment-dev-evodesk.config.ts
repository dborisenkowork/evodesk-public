import {Provider} from '@angular/core';

import {ENVIRONMENT, EnvironmentConfiguration} from './environments';

import {httpApi} from './apis/http.config';
import {environmentDefaults} from './defaults.config';

export const api: Array<Provider> = httpApi;

export const environment: EnvironmentConfiguration = {
    ...environmentDefaults,
    env: ENVIRONMENT.Dev,
    serverHostName: 'https://evodesk.ru',
    serverAssetsURL: 'https://evodesk.ru/api',
    modules: {
        EvoDeskRESTApi: {
            apiEndpoint: 'https://evodesk.ru/api',
            serverHostName: 'https://evodesk.ru',
            payeer: {
                sendUrl: 'https://payeer.com/merchant/',
                m_shop: '483711525',
            },
        },
    },
};
