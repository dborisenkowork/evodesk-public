import {Provider} from '@angular/core';

import {ENVIRONMENT, EnvironmentConfiguration} from './environments';

import {httpApi} from './apis/http.config';
import {environmentDefaults} from './defaults.config';

export const api: Array<Provider> = httpApi;

export const environment: EnvironmentConfiguration = {
    ...environmentDefaults,
    env: ENVIRONMENT.Dev,
    serverHostName: 'http://localhost:8000',
    serverAssetsURL: 'http://localhost:8080',
    modules: {
        EvoDeskRESTApi: {
            apiEndpoint: 'http://localhost:8080',
            serverHostName: 'http://localhost:8000',
            payeer: {
                sendUrl: 'https://payeer.com/merchant/',
                m_shop: '483711525',
            },
        },
    },
};
