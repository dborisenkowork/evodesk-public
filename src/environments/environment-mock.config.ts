import {Provider} from '@angular/core';

import {ENVIRONMENT, EnvironmentConfiguration} from './environments';

import {mockApi} from './apis/mock.config';
import {environmentDefaults} from './defaults.config';

export const api: Array<Provider> = mockApi;

export const environment: EnvironmentConfiguration = {
    ...environmentDefaults,
    env: ENVIRONMENT.Mock,
    serverHostName: 'http://evodesk.ru',
    serverAssetsURL: 'http://evodesk.ru',
    modules: {
        EvoDeskRESTApi: {
            apiEndpoint: 'http://0.0.0.0',
            serverHostName: 'http://evodesk.ru',
            payeer: {
                sendUrl: 'https://payeer.com/merchant/',
                m_shop: '483711525',
            },
        },
    },
};
