import {Provider} from '@angular/core';

import {ENVIRONMENT, EnvironmentConfiguration} from './environments';

import {httpApi} from './apis/http.config';
import {environmentDefaults} from './defaults.config';

export const api: Array<Provider> = httpApi;

export const environment: EnvironmentConfiguration = {
    ...environmentDefaults,
    env: ENVIRONMENT.Prod,
    angular: {
        ...environmentDefaults.angular,
        useHashStrategy: false,
        enableProductionMode: true,
        enableRouterTracing: false,
    },
    serverHostName: 'https://evodesk.ru',
    serverAssetsURL: 'https://evodesk.ru/api',
    mobileAppLinks: {
        osx: 'https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1299422103',
        android: 'https://play.google.com/store/apps/details?id=ru.evodesk.dnk',
    },
    modules: {
        EvoDeskRESTApi: {
            apiEndpoint: 'https://evodesk.ru/api',
            serverHostName: 'https://evodesk.ru',
            payeer: {
                sendUrl: 'https://payeer.com/merchant/',
                m_shop: '483711525',
            },
},
    },
};
