export enum ViewMode {
    Desktop = 'desktop',
    Tablet = 'tablet',
    Mobile = 'mobile',
}

export enum ViewModeWithWideScreen {
    Wide = 'wide',
    Desktop = 'desktop',
    Tablet = 'tablet',
    Mobile = 'mobile',
}
