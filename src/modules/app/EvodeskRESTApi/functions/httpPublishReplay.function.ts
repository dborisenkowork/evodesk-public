import {publishLast, refCount, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {ApplicationRef} from '@angular/core';

import {IS_NGZONE_DISABLED} from '../../EvodeskApp/configs/EvodeskApp.config';

export const HTTP_PUBLISH_REPLAY_REF: { appRef?: ApplicationRef } = {};

export function httpPublishReplay<T>(input: Observable<any>): Observable<T> {
    return input.pipe(publishLast(), refCount(), tap(() => {
        if (IS_NGZONE_DISABLED) {
            HTTP_PUBLISH_REPLAY_REF.appRef.tick();
        }
    }));
}

export function shareLast(): ((source: Observable<any>) => Observable<any>) {
    return function(source) {
       return source.pipe(publishLast(), refCount());
    };
}
