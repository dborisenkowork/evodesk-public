import {InjectionToken} from '@angular/core';

export const EVODESK_REST_API_MODULE_CONFIG: InjectionToken<EvoDeskRESTApiEnvironmentConfig> = new InjectionToken<EvoDeskRESTApiEnvironmentConfig>('EvodeskRESTApiModule environment configuration');

export interface EvoDeskRESTApiEnvironmentConfig
{
    serverHostName: string;
    apiEndpoint: string; /* Use values like "http://evodesk.ru/", "http://127.0.0.1/" etc. Do not use relative path.
                            Do care about SSR (Server-Side Rendering) and make sure you configured CORS well for
                            endpoints. */
    payeer: {
        sendUrl: string;
        m_shop: string;
    };
}
