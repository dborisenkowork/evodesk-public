import {Injectable} from '@angular/core';

import * as moment from 'moment';

import {Observable, of} from 'rxjs';
import {delay} from 'rxjs/operators';

import {EvodeskNotification, NotificationAchievementEarnedEntity, NotificationCardChecklistAddedEntity, NotificationCardChecklistDeletedEntity, NotificationCardCommentAddedEntity, NotificationCardCommentDeletedEntity, NotificationCardDeadlineAddedEntity, NotificationCardDeadlineDeletedEarnedEntity, NotificationCardDeadlineUpdatedEntity, NotificationCardDeletedEntity, NotificationCardDescriptionChangedEntity, NotificationCardLabelAddedEntity, NotificationCardLabelDeletedEntity, NotificationCardMovedToAnotherColumnEntity, NotificationCardParticipantDeletedEntity, NotificationCardTitleChangedEntity, NotificationEvoCoinReceivedEntity, NotificationEvoPointsReceivedEntity, NotificationId, NotificationIMReceivedMessageEarnedEntity, NotificationPostCommentedEntity, NotificationPostCommentLikedEntity, NotificationPostEntity, NotificationPostLikedEntity, NotificationPostRepostedEntity, NotificationTypeKind} from '../models/Notification.model';

import {GetNotificationsResponse200} from '../services/NotificationREST.service';

import {PostType} from '../models/post/Post.model';
import {evodeskDeskDefaultPalette} from '../../../features/EvodeskDesk/configs/EvodeskDesk.constants';

let incrementId: number = 1;

const mockData: Array<EvodeskNotification> = [];

window['pushMockNotifications'] = () => {
    setOf(NotificationTypeKind.AchievementEarned).forEach(n => mockData.push(n));
};

function randomDeskColor(): string {
    const colors: Array<string> = evodeskDeskDefaultPalette.map(c => c.color);

    return colors[Math.floor(Math.random() * colors.length)];
}

const sampleSets: Array<{ type: NotificationTypeKind, notifications: Array<EvodeskNotification> }> = [
    {
        type: NotificationTypeKind.AchievementEarned,
        notifications: [
            {
                kind: NotificationTypeKind.AchievementEarned,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-1', 'minutes').toDate().getTime(),
                    entity: <NotificationAchievementEarnedEntity>{
                        version: 1,
                        achievement: {
                            title: 'Вау, гречка!',
                            text: 'Заходил в систему 30 дней подряд',
                            image: '/template/img/achievements/labels/6.png',
                            brief: 'buckwheat3',
                            bonus: '200+500 баллов',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.AchievementEarned,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardDeleted,
        notifications: [
            {
                kind: NotificationTypeKind.CardDeleted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardDeletedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardDeleted,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardDeleted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardDeletedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardDeleted,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardParticipantDeleted,
        notifications: [
            {
                kind: NotificationTypeKind.CardParticipantDeleted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardParticipantDeletedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        participantId: -1,
                        participantAvatar: null,
                        participantName: 'Иванов Иван',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardParticipantDeleted,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardParticipantDeleted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardParticipantDeletedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        participantId: -1,
                        participantAvatar: null,
                        participantName: 'Иванов Иван',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardParticipantDeleted,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardParticipantAdded,
        notifications: [
            {
                kind: NotificationTypeKind.CardParticipantAdded,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardParticipantDeletedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        participantId: -1,
                        participantAvatar: null,
                        participantName: 'Иванов Иван',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardParticipantAdded,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardParticipantAdded,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardParticipantDeletedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        participantId: -1,
                        participantAvatar: null,
                        participantName: 'Иванов Иван',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardParticipantAdded,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardTitleChanged,
        notifications: [
            {
                kind: NotificationTypeKind.CardTitleChanged,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardTitleChangedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        oldTitle: 'Подготовка к Новому Году',
                        newTitle: 'Подготовка к Новому Году и Рождеству',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.AchievementEarned,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardTitleChanged,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardTitleChangedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        oldTitle: 'Подготовка к Новому Году',
                        newTitle: 'Подготовка к Новому Году и Рождеству',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.AchievementEarned,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardDescriptionChanged,
        notifications: [
            {
                kind: NotificationTypeKind.CardDescriptionChanged,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardDescriptionChangedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardDescriptionChanged,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardDescriptionChanged,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardDescriptionChangedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardDescriptionChanged,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardLabelAdded,
        notifications: [
            {
                kind: NotificationTypeKind.CardLabelAdded,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardLabelAddedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        labelId: -1,
                        labelColor: randomDeskColor(),
                        labelName: 'MAJOR',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardLabelAdded,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardLabelAdded,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardLabelAddedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        labelId: -1,
                        labelColor: randomDeskColor(),
                        labelName: 'MAJOR',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardLabelAdded,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardLabelDeleted,
        notifications: [
            {
                kind: NotificationTypeKind.CardLabelDeleted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardLabelDeletedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        labelId: -1,
                        labelColor: randomDeskColor(),
                        labelName: 'MAJOR',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardLabelDeleted,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardLabelDeleted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardLabelDeletedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        labelId: -1,
                        labelColor: randomDeskColor(),
                        labelName: 'MAJOR',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardLabelDeleted,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardCommentAdded,
        notifications: [
            {
                kind: NotificationTypeKind.CardCommentAdded,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardCommentAddedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        commentText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardCommentAdded,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardCommentAdded,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardCommentAddedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        commentText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardCommentAdded,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardCommentDeleted,
        notifications: [
            {
                kind: NotificationTypeKind.CardCommentDeleted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardCommentDeletedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        commentText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardCommentDeleted,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardCommentDeleted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardCommentDeletedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        commentText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardCommentDeleted,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardChecklistAdded,
        notifications: [
            {
                kind: NotificationTypeKind.CardChecklistAdded,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardChecklistAddedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        checklistId: -1,
                        checklistName: 'Тестовый чеклист',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardChecklistAdded,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardChecklistAdded,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardChecklistAddedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        checklistId: -1,
                        checklistName: 'Тестовый чеклист',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardChecklistAdded,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardChecklistDeleted,
        notifications: [
            {
                kind: NotificationTypeKind.CardChecklistDeleted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardChecklistDeletedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        checklistId: -1,
                        checklistName: 'Тестовый чеклист',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardChecklistDeleted,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardChecklistDeleted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardChecklistDeletedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        checklistId: -1,
                        checklistName: 'Тестовый чеклист',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardChecklistDeleted,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardMovedToAnotherColumn,
        notifications: [
            {
                kind: NotificationTypeKind.CardMovedToAnotherColumn,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardMovedToAnotherColumnEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        previousColumnId: 2,
                        newColumnId: 3,
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardMovedToAnotherColumn,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardMovedToAnotherColumn,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardMovedToAnotherColumnEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        previousColumnId: 2,
                        newColumnId: 3,
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardMovedToAnotherColumn,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardDeadlineAdded,
        notifications: [
            {
                kind: NotificationTypeKind.CardDeadlineAdded,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardDeadlineAddedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        newDeadline: '2018-09-21 00:00:00',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardDeadlineAdded,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardDeadlineAdded,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardDeadlineAddedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        newDeadline: '2018-09-21 00:00:00',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardDeadlineAdded,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardDeadlineUpdated,
        notifications: [
            {
                kind: NotificationTypeKind.CardDeadlineUpdated,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardDeadlineUpdatedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        previousDeadline: '2018-09-21 00:00:00',
                        newDeadline: '2018-09-21 00:00:00',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardDeadlineUpdated,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardDeadlineUpdated,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardDeadlineUpdatedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        previousDeadline: '2018-09-21 00:00:00',
                        newDeadline: '2018-09-21 00:00:00',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardDeadlineUpdated,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.CardDeadlineDeleted,
        notifications: [
            {
                kind: NotificationTypeKind.CardDeadlineDeleted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardDeadlineDeletedEarnedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: -1,
                        deskOwnedId: 13868,
                        deskType: 'personal',
                        cardTitle: 'Тестовая карточка',
                        previousDeadline: '2018-09-21 00:00:00',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardDeadlineDeleted,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.CardDeadlineDeleted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationCardDeadlineDeletedEarnedEntity>{
                        version: 1,
                        cardId: -1,
                        deskId: 28106,
                        deskOwnedId: 13868,
                        deskType: 'multidesk',
                        deskName: 'Evodesk – Frontend',
                        deskColor: randomDeskColor(),
                        cardTitle: 'Тестовая карточка',
                        previousDeadline: '2018-09-21 00:00:00',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.CardDeadlineDeleted,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.IMReceivedMessage,
        notifications: [
            {
                kind: NotificationTypeKind.IMReceivedMessage,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationIMReceivedMessageEarnedEntity>{
                        version: 1,
                        message: {
                            id: -1,
                            dialogId: 26411,
                            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                            createdAt: '2018-09-18 11:05:28',
                        },
                        author: {
                            id: 12543,
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                            name: 'Иванов Иван Иванович',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.IMReceivedMessage,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.EvoCoinReceived,
        notifications: [
            {
                kind: NotificationTypeKind.EvoCoinReceived,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationEvoCoinReceivedEntity>{
                        version: 1,
                        received: '99.50',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.EvoCoinReceived,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.EvoPointsReceived,
        notifications: [
            {
                kind: NotificationTypeKind.EvoPointsReceived,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationEvoPointsReceivedEntity>{
                        version: 1,
                        received: '99.50',
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.EvoPointsReceived,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.Post,
        notifications: [
            {
                kind: NotificationTypeKind.Post,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostEntity>{
                        version: 1,
                        post: {
                            id: 291,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 0,
                            comments: 0,
                            created_at: '2018-09-18 12:01:26',
                            update_at: '2018-09-18 12:01:26',
                            text: '\nАксолотль мексиканской амбистомы (Ambystoma mexicanum)\nАксоло́тль (Axolotl) — неотеническая личинка некоторых видов амбистом, земноводных из семейства амбистомовых (Ambystomidae) отряда хвостатых (Caudata).\n\nОсобенность аксолотля состоит в том, что он не достигает половой зрелости и становится способным к размножению, не превратившись во взрослую форму, не претерпев метаморфоз. У этих личинок хорошо развита щитовидная железа, она обычно вырабатывает достаточное количество индуцирующего метаморфозы гормона тироксина. Однако, если переселить аксолотля в более сухую и прохладную среду или понизить уровень воды при домашнем разведении, он превращается во взрослую амбистому. Превращение аксолотля в амбистому можно вызвать также добавлением в пищу или инъекцией гормона тироксина. Превращение может произойти в течение нескольких недель, при этом исчезнут наружные жабры аксолотля, изменится окраска, форма тела. Но вводить аксолотля в метаморфоз без поддержки специалиста опасно для жизни животного. Как правило, попытки в домашних условиях превратить аксолотля в амбистому в 99 % случаев заканчиваются смертью личинки.\n\nЧаще всего название «аксолотль» применяют по отношению к личинке мексиканской амбистомы (большинство содержащихся в лабораторных или домашних условиях аксолотлей принадлежат к этому виду) или тигровой амбистомы, но так можно назвать личинку любой амбистомы, способной к неотении[1].\n\nВ дословном переводе с классического науатля аксолотль (точнее, «ашолотль», axolotl) — «водяная собака», «водяное чудище» (ср. atl — вода, xolotl — собака, вместе — axolotl), что вполне соответствует его внешнему виду: аксолотль похож на крупного головастого тритона с торчащими в стороны тремя парами наружных жабр. Голова у аксолотля очень большая и широкая, несоразмерная с телом, рот тоже широкий, а глазки маленькие — создаётся впечатление, что личинка всё время улыбается. Помимо прочего, эти животные обладают способностью регенерировать утраченную часть тела. Общая длина — до 30 см. Как и все личинки хвостатых земноводных, аксолотли ведут хищный образ жизни.',
                            type: PostType.Post,
                            post: null,
                            attachments: [],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        postBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.EvoPointsReceived,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.Post,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostEntity>{
                        version: 1,
                        post: {
                            id: 293,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 12:43:06',
                            update_at: '2018-09-18 12:43:06',
                            text: 'Пост (фотография)',
                            type: 'post',
                            post: null,
                            attachments: [
                                {
                                    id: 531,
                                    type: 'photo',
                                    path: '056061372f6b5c2a5bd9082263f79d1b.png',
                                    preview: '056061372f6b5c2a5bd9082263f79d1b.png',
                                },
                            ],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        postBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.EvoPointsReceived,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.Post,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostEntity>{
                        version: 1,
                        post: {
                            id: 294,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 12:43:29',
                            update_at: '2018-09-18 12:43:29',
                            text: 'Пост (видео)',
                            type: 'post',
                            post: null,
                            attachments: [
                                {
                                    id: 532,
                                    type: 'video',
                                    path: 'https://www.youtube.com/watch?v=C3FAZwcw5LQ',
                                    preview: 'https://i.ytimg.com/vi/C3FAZwcw5LQ/maxresdefault.jpg',
                                },
                            ],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        postBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.EvoPointsReceived,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.Post,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostEntity>{
                        version: 1,
                        post: {
                            id: 295,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 12:43:47',
                            update_at: '2018-09-18 12:43:47',
                            text: 'Пост (ссылка)',
                            type: 'post',
                            post: null,
                            attachments: [
                                {
                                    id: 533,
                                    type: 'link',
                                    path: 'https://medium.com/s/story/the-commuting-conundrum-should-employers-fund-our-commute-4c7c0af6c56',
                                    preview: 'https://cdn-images-1.medium.com/focal/1200/632/52/52/0*bUFvtsJmWQRUzgjp',
                                },
                            ],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        postBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.EvoPointsReceived,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.PostReposted,
        notifications: [
            {
                kind: NotificationTypeKind.PostReposted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostRepostedEntity>{
                        version: 1,
                        post: {
                            id: 291,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 0,
                            comments: 0,
                            created_at: '2018-09-18 12:01:26',
                            update_at: '2018-09-18 12:01:26',
                            text: '\nАксолотль мексиканской амбистомы (Ambystoma mexicanum)\nАксоло́тль (Axolotl) — неотеническая личинка некоторых видов амбистом, земноводных из семейства амбистомовых (Ambystomidae) отряда хвостатых (Caudata).\n\nОсобенность аксолотля состоит в том, что он не достигает половой зрелости и становится способным к размножению, не превратившись во взрослую форму, не претерпев метаморфоз. У этих личинок хорошо развита щитовидная железа, она обычно вырабатывает достаточное количество индуцирующего метаморфозы гормона тироксина. Однако, если переселить аксолотля в более сухую и прохладную среду или понизить уровень воды при домашнем разведении, он превращается во взрослую амбистому. Превращение аксолотля в амбистому можно вызвать также добавлением в пищу или инъекцией гормона тироксина. Превращение может произойти в течение нескольких недель, при этом исчезнут наружные жабры аксолотля, изменится окраска, форма тела. Но вводить аксолотля в метаморфоз без поддержки специалиста опасно для жизни животного. Как правило, попытки в домашних условиях превратить аксолотля в амбистому в 99 % случаев заканчиваются смертью личинки.\n\nЧаще всего название «аксолотль» применяют по отношению к личинке мексиканской амбистомы (большинство содержащихся в лабораторных или домашних условиях аксолотлей принадлежат к этому виду) или тигровой амбистомы, но так можно назвать личинку любой амбистомы, способной к неотении[1].\n\nВ дословном переводе с классического науатля аксолотль (точнее, «ашолотль», axolotl) — «водяная собака», «водяное чудище» (ср. atl — вода, xolotl — собака, вместе — axolotl), что вполне соответствует его внешнему виду: аксолотль похож на крупного головастого тритона с торчащими в стороны тремя парами наружных жабр. Голова у аксолотля очень большая и широкая, несоразмерная с телом, рот тоже широкий, а глазки маленькие — создаётся впечатление, что личинка всё время улыбается. Помимо прочего, эти животные обладают способностью регенерировать утраченную часть тела. Общая длина — до 30 см. Как и все личинки хвостатых земноводных, аксолотли ведут хищный образ жизни.',
                            type: PostType.Post,
                            post: null,
                            attachments: [],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        repost: {
                            id: 296,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 17:00:19',
                            update_at: '2018-09-18 17:00:19',
                            text: 'Тестовый репост',
                            type: 'post',
                            post: undefined,
                            attachments: [],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        repostBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.PostReposted,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.PostReposted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostRepostedEntity>{
                        version: 1,
                        post: {
                            id: 293,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 12:43:06',
                            update_at: '2018-09-18 12:43:06',
                            text: 'Пост (фотография)',
                            type: 'post',
                            post: null,
                            attachments: [
                                {
                                    id: 531,
                                    type: 'photo',
                                    path: '056061372f6b5c2a5bd9082263f79d1b.png',
                                    preview: '056061372f6b5c2a5bd9082263f79d1b.png',
                                },
                            ],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        repost: {
                            id: 296,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 17:00:19',
                            update_at: '2018-09-18 17:00:19',
                            text: 'Тестовый репост',
                            type: 'post',
                            post: undefined,
                            attachments: [],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        repostBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.PostReposted,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.PostReposted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostRepostedEntity>{
                        version: 1,
                        post: {
                            id: 294,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 12:43:29',
                            update_at: '2018-09-18 12:43:29',
                            text: 'Пост (видео)',
                            type: 'post',
                            post: null,
                            attachments: [
                                {
                                    id: 532,
                                    type: 'video',
                                    path: 'https://www.youtube.com/watch?v=C3FAZwcw5LQ',
                                    preview: 'https://i.ytimg.com/vi/C3FAZwcw5LQ/maxresdefault.jpg',
                                },
                            ],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        repost: {
                            id: 296,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 17:00:19',
                            update_at: '2018-09-18 17:00:19',
                            text: 'Тестовый репост',
                            type: 'post',
                            post: undefined,
                            attachments: [],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        repostBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.PostReposted,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.PostReposted,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostRepostedEntity>{
                        version: 1,
                        post: {
                            id: 295,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 12:43:47',
                            update_at: '2018-09-18 12:43:47',
                            text: 'Пост (ссылка)',
                            type: 'post',
                            post: null,
                            attachments: [
                                {
                                    id: 533,
                                    type: 'link',
                                    path: 'https://medium.com/s/story/the-commuting-conundrum-should-employers-fund-our-commute-4c7c0af6c56',
                                    preview: 'https://cdn-images-1.medium.com/focal/1200/632/52/52/0*bUFvtsJmWQRUzgjp',
                                },
                            ],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        repost: {
                            id: 296,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 17:00:19',
                            update_at: '2018-09-18 17:00:19',
                            text: 'Тестовый репост',
                            type: 'post',
                            post: undefined,
                            attachments: [],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        repostBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.PostReposted,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.PostCommented,
        notifications: [
            {
                kind: NotificationTypeKind.PostCommented,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostCommentedEntity>{
                        version: 1,
                        post: {
                            id: 291,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 0,
                            comments: 0,
                            created_at: '2018-09-18 12:01:26',
                            update_at: '2018-09-18 12:01:26',
                            text: '\nАксолотль мексиканской амбистомы (Ambystoma mexicanum)\nАксоло́тль (Axolotl) — неотеническая личинка некоторых видов амбистом, земноводных из семейства амбистомовых (Ambystomidae) отряда хвостатых (Caudata).\n\nОсобенность аксолотля состоит в том, что он не достигает половой зрелости и становится способным к размножению, не превратившись во взрослую форму, не претерпев метаморфоз. У этих личинок хорошо развита щитовидная железа, она обычно вырабатывает достаточное количество индуцирующего метаморфозы гормона тироксина. Однако, если переселить аксолотля в более сухую и прохладную среду или понизить уровень воды при домашнем разведении, он превращается во взрослую амбистому. Превращение аксолотля в амбистому можно вызвать также добавлением в пищу или инъекцией гормона тироксина. Превращение может произойти в течение нескольких недель, при этом исчезнут наружные жабры аксолотля, изменится окраска, форма тела. Но вводить аксолотля в метаморфоз без поддержки специалиста опасно для жизни животного. Как правило, попытки в домашних условиях превратить аксолотля в амбистому в 99 % случаев заканчиваются смертью личинки.\n\nЧаще всего название «аксолотль» применяют по отношению к личинке мексиканской амбистомы (большинство содержащихся в лабораторных или домашних условиях аксолотлей принадлежат к этому виду) или тигровой амбистомы, но так можно назвать личинку любой амбистомы, способной к неотении[1].\n\nВ дословном переводе с классического науатля аксолотль (точнее, «ашолотль», axolotl) — «водяная собака», «водяное чудище» (ср. atl — вода, xolotl — собака, вместе — axolotl), что вполне соответствует его внешнему виду: аксолотль похож на крупного головастого тритона с торчащими в стороны тремя парами наружных жабр. Голова у аксолотля очень большая и широкая, несоразмерная с телом, рот тоже широкий, а глазки маленькие — создаётся впечатление, что личинка всё время улыбается. Помимо прочего, эти животные обладают способностью регенерировать утраченную часть тела. Общая длина — до 30 см. Как и все личинки хвостатых земноводных, аксолотли ведут хищный образ жизни.',
                            type: PostType.Post,
                            post: null,
                            attachments: [],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        comment: {
                            id: 207,
                            post_id: {
                                id: 288,
                                user_id: 13868,
                                likes: 0,
                                reposts: 0,
                                views: 2,
                                comments: 1,
                                created_at: '2018-09-17 13:07:14',
                                update_at: '2018-09-17 13:07:14',
                                text: 'привет 123',
                                type: 'post',
                                post: null,
                                attachments: [],
                                pinned: '0',
                                hidden: null,
                                isLiked: false,
                                isReposted: false,
                            },
                            user_id: 13868,
                            email: 'dmitrii.borisenko@lpsoftwarefactory.com',
                            avatar: '39c2b0fdb24a333f0d23bd73f8f912d4.png',
                            name: 'Борисенко Дмитрий Владимирович',
                            text: '{"text":"Аксолотль мексиканской амбистомы (Ambystoma mexicanum)\\nАксоло́тль (Axolotl) — неотеническая личинка некоторых видов амбистом, земноводных из семейства амбистомовых (Ambystomidae) отряда хвостатых (Caudata).\\n\\nОсобенность аксолотля состоит в том, что он не достигает половой зрелости и становится способным к размножению, не превратившись во взрослую форму, не претерпев метаморфоз. У этих личинок хорошо развита щитовидная железа, она обычно вырабатывает достаточное количество индуцирующего метаморфозы гормона тироксина. Однако, если переселить аксолотля в более сухую и прохладную среду или понизить уровень воды при домашнем разведении, он превращается во взрослую амбистому. Превращение аксолотля в амбистому можно вызвать также добавлением в пищу или инъекцией гормона тироксина. Превращение может произойти в течение нескольких недель, при этом исчезнут наружные жабры аксолотля, изменится окраска, форма тела. Но вводить аксолотля в метаморфоз без поддержки специалиста опасно для жизни животного. Как правило, попытки в домашних условиях превратить аксолотля в амбистому в 99 % случаев заканчиваются смертью личинки.\\n\\nЧаще всего название «аксолотль» применяют по отношению к личинке мексиканской амбистомы (большинство содержащихся в лабораторных или домашних условиях аксолотлей принадлежат к этому виду) или тигровой амбистомы, но так можно назвать личинку любой амбистомы, способной к неотении[1].\\n\\nВ дословном переводе с классического науатля аксолотль (точнее, «ашолотль», axolotl) — «водяная собака», «водяное чудище» (ср. atl — вода, xolotl — собака, вместе — axolotl), что вполне соответствует его внешнему виду: аксолотль похож на крупного головастого тритона с торчащими в стороны тремя парами наружных жабр. Голова у аксолотля очень большая и широкая, несоразмерная с телом, рот тоже широкий, а глазки маленькие — создаётся впечатление, что личинка всё время улыбается. Помимо прочего, эти животные обладают способностью регенерировать утраченную часть тела. Общая длина — до 30 см. Как и все личинки хвостатых земноводных, аксолотли ведут хищный образ жизни."}',
                            likes: 0,
                            created_at: '2018-09-18 17:55:27',
                            updated_at: '2018-09-18 17:55:27',
                            isLiked: false,
                        },
                        commentedBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.PostCommented,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.PostCommented,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostCommentedEntity>{
                        version: 1,
                        post: {
                            id: 293,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 12:43:06',
                            update_at: '2018-09-18 12:43:06',
                            text: 'Пост (фотография)',
                            type: 'post',
                            post: null,
                            attachments: [
                                {
                                    id: 531,
                                    type: 'photo',
                                    path: '056061372f6b5c2a5bd9082263f79d1b.png',
                                    preview: '056061372f6b5c2a5bd9082263f79d1b.png',
                                },
                            ],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        comment: {
                            id: 207,
                            post_id: {
                                id: 288,
                                user_id: 13868,
                                likes: 0,
                                reposts: 0,
                                views: 2,
                                comments: 1,
                                created_at: '2018-09-17 13:07:14',
                                update_at: '2018-09-17 13:07:14',
                                text: 'привет 123',
                                type: 'post',
                                post: null,
                                attachments: [],
                                pinned: '0',
                                hidden: null,
                                isLiked: false,
                                isReposted: false,
                            },
                            user_id: 13868,
                            email: 'dmitrii.borisenko@lpsoftwarefactory.com',
                            avatar: '39c2b0fdb24a333f0d23bd73f8f912d4.png',
                            name: 'Борисенко Дмитрий Владимирович',
                            text: '{"text":"Аксолотль мексиканской амбистомы (Ambystoma mexicanum)\\nАксоло́тль (Axolotl) — неотеническая личинка некоторых видов амбистом, земноводных из семейства амбистомовых (Ambystomidae) отряда хвостатых (Caudata).\\n\\nОсобенность аксолотля состоит в том, что он не достигает половой зрелости и становится способным к размножению, не превратившись во взрослую форму, не претерпев метаморфоз. У этих личинок хорошо развита щитовидная железа, она обычно вырабатывает достаточное количество индуцирующего метаморфозы гормона тироксина. Однако, если переселить аксолотля в более сухую и прохладную среду или понизить уровень воды при домашнем разведении, он превращается во взрослую амбистому. Превращение аксолотля в амбистому можно вызвать также добавлением в пищу или инъекцией гормона тироксина. Превращение может произойти в течение нескольких недель, при этом исчезнут наружные жабры аксолотля, изменится окраска, форма тела. Но вводить аксолотля в метаморфоз без поддержки специалиста опасно для жизни животного. Как правило, попытки в домашних условиях превратить аксолотля в амбистому в 99 % случаев заканчиваются смертью личинки.\\n\\nЧаще всего название «аксолотль» применяют по отношению к личинке мексиканской амбистомы (большинство содержащихся в лабораторных или домашних условиях аксолотлей принадлежат к этому виду) или тигровой амбистомы, но так можно назвать личинку любой амбистомы, способной к неотении[1].\\n\\nВ дословном переводе с классического науатля аксолотль (точнее, «ашолотль», axolotl) — «водяная собака», «водяное чудище» (ср. atl — вода, xolotl — собака, вместе — axolotl), что вполне соответствует его внешнему виду: аксолотль похож на крупного головастого тритона с торчащими в стороны тремя парами наружных жабр. Голова у аксолотля очень большая и широкая, несоразмерная с телом, рот тоже широкий, а глазки маленькие — создаётся впечатление, что личинка всё время улыбается. Помимо прочего, эти животные обладают способностью регенерировать утраченную часть тела. Общая длина — до 30 см. Как и все личинки хвостатых земноводных, аксолотли ведут хищный образ жизни."}',
                            likes: 0,
                            created_at: '2018-09-18 17:55:27',
                            updated_at: '2018-09-18 17:55:27',
                            isLiked: false,
                        },
                        commentedBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.PostCommented,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.PostCommented,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostCommentedEntity>{
                        version: 1,
                        post: {
                            id: 294,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 12:43:29',
                            update_at: '2018-09-18 12:43:29',
                            text: 'Пост (видео)',
                            type: 'post',
                            post: null,
                            attachments: [
                                {
                                    id: 532,
                                    type: 'video',
                                    path: 'https://www.youtube.com/watch?v=C3FAZwcw5LQ',
                                    preview: 'https://i.ytimg.com/vi/C3FAZwcw5LQ/maxresdefault.jpg',
                                },
                            ],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        comment: {
                            id: 207,
                            post_id: {
                                id: 288,
                                user_id: 13868,
                                likes: 0,
                                reposts: 0,
                                views: 2,
                                comments: 1,
                                created_at: '2018-09-17 13:07:14',
                                update_at: '2018-09-17 13:07:14',
                                text: 'привет 123',
                                type: 'post',
                                post: null,
                                attachments: [],
                                pinned: '0',
                                hidden: null,
                                isLiked: false,
                                isReposted: false,
                            },
                            user_id: 13868,
                            email: 'dmitrii.borisenko@lpsoftwarefactory.com',
                            avatar: '39c2b0fdb24a333f0d23bd73f8f912d4.png',
                            name: 'Борисенко Дмитрий Владимирович',
                            text: '{"text":"Аксолотль мексиканской амбистомы (Ambystoma mexicanum)\\nАксоло́тль (Axolotl) — неотеническая личинка некоторых видов амбистом, земноводных из семейства амбистомовых (Ambystomidae) отряда хвостатых (Caudata).\\n\\nОсобенность аксолотля состоит в том, что он не достигает половой зрелости и становится способным к размножению, не превратившись во взрослую форму, не претерпев метаморфоз. У этих личинок хорошо развита щитовидная железа, она обычно вырабатывает достаточное количество индуцирующего метаморфозы гормона тироксина. Однако, если переселить аксолотля в более сухую и прохладную среду или понизить уровень воды при домашнем разведении, он превращается во взрослую амбистому. Превращение аксолотля в амбистому можно вызвать также добавлением в пищу или инъекцией гормона тироксина. Превращение может произойти в течение нескольких недель, при этом исчезнут наружные жабры аксолотля, изменится окраска, форма тела. Но вводить аксолотля в метаморфоз без поддержки специалиста опасно для жизни животного. Как правило, попытки в домашних условиях превратить аксолотля в амбистому в 99 % случаев заканчиваются смертью личинки.\\n\\nЧаще всего название «аксолотль» применяют по отношению к личинке мексиканской амбистомы (большинство содержащихся в лабораторных или домашних условиях аксолотлей принадлежат к этому виду) или тигровой амбистомы, но так можно назвать личинку любой амбистомы, способной к неотении[1].\\n\\nВ дословном переводе с классического науатля аксолотль (точнее, «ашолотль», axolotl) — «водяная собака», «водяное чудище» (ср. atl — вода, xolotl — собака, вместе — axolotl), что вполне соответствует его внешнему виду: аксолотль похож на крупного головастого тритона с торчащими в стороны тремя парами наружных жабр. Голова у аксолотля очень большая и широкая, несоразмерная с телом, рот тоже широкий, а глазки маленькие — создаётся впечатление, что личинка всё время улыбается. Помимо прочего, эти животные обладают способностью регенерировать утраченную часть тела. Общая длина — до 30 см. Как и все личинки хвостатых земноводных, аксолотли ведут хищный образ жизни."}',
                            likes: 0,
                            created_at: '2018-09-18 17:55:27',
                            updated_at: '2018-09-18 17:55:27',
                            isLiked: false,
                        },
                        commentedBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.PostCommented,
                    },
                    text: '',
                },
            },
            {
                kind: NotificationTypeKind.PostCommented,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostCommentedEntity>{
                        version: 1,
                        post: {
                            id: 295,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 1,
                            comments: 0,
                            created_at: '2018-09-18 12:43:47',
                            update_at: '2018-09-18 12:43:47',
                            text: 'Пост (ссылка)',
                            type: 'post',
                            post: null,
                            attachments: [
                                {
                                    id: 533,
                                    type: 'link',
                                    path: 'https://medium.com/s/story/the-commuting-conundrum-should-employers-fund-our-commute-4c7c0af6c56',
                                    preview: 'https://cdn-images-1.medium.com/focal/1200/632/52/52/0*bUFvtsJmWQRUzgjp',
                                },
                            ],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        comment: {
                            id: 207,
                            post_id: {
                                id: 288,
                                user_id: 13868,
                                likes: 0,
                                reposts: 0,
                                views: 2,
                                comments: 1,
                                created_at: '2018-09-17 13:07:14',
                                update_at: '2018-09-17 13:07:14',
                                text: 'привет 123',
                                type: 'post',
                                post: null,
                                attachments: [],
                                pinned: '0',
                                hidden: null,
                                isLiked: false,
                                isReposted: false,
                            },
                            user_id: 13868,
                            email: 'dmitrii.borisenko@lpsoftwarefactory.com',
                            avatar: '39c2b0fdb24a333f0d23bd73f8f912d4.png',
                            name: 'Борисенко Дмитрий Владимирович',
                            text: '{"text":"Аксолотль мексиканской амбистомы (Ambystoma mexicanum)\\nАксоло́тль (Axolotl) — неотеническая личинка некоторых видов амбистом, земноводных из семейства амбистомовых (Ambystomidae) отряда хвостатых (Caudata).\\n\\nОсобенность аксолотля состоит в том, что он не достигает половой зрелости и становится способным к размножению, не превратившись во взрослую форму, не претерпев метаморфоз. У этих личинок хорошо развита щитовидная железа, она обычно вырабатывает достаточное количество индуцирующего метаморфозы гормона тироксина. Однако, если переселить аксолотля в более сухую и прохладную среду или понизить уровень воды при домашнем разведении, он превращается во взрослую амбистому. Превращение аксолотля в амбистому можно вызвать также добавлением в пищу или инъекцией гормона тироксина. Превращение может произойти в течение нескольких недель, при этом исчезнут наружные жабры аксолотля, изменится окраска, форма тела. Но вводить аксолотля в метаморфоз без поддержки специалиста опасно для жизни животного. Как правило, попытки в домашних условиях превратить аксолотля в амбистому в 99 % случаев заканчиваются смертью личинки.\\n\\nЧаще всего название «аксолотль» применяют по отношению к личинке мексиканской амбистомы (большинство содержащихся в лабораторных или домашних условиях аксолотлей принадлежат к этому виду) или тигровой амбистомы, но так можно назвать личинку любой амбистомы, способной к неотении[1].\\n\\nВ дословном переводе с классического науатля аксолотль (точнее, «ашолотль», axolotl) — «водяная собака», «водяное чудище» (ср. atl — вода, xolotl — собака, вместе — axolotl), что вполне соответствует его внешнему виду: аксолотль похож на крупного головастого тритона с торчащими в стороны тремя парами наружных жабр. Голова у аксолотля очень большая и широкая, несоразмерная с телом, рот тоже широкий, а глазки маленькие — создаётся впечатление, что личинка всё время улыбается. Помимо прочего, эти животные обладают способностью регенерировать утраченную часть тела. Общая длина — до 30 см. Как и все личинки хвостатых земноводных, аксолотли ведут хищный образ жизни."}',
                            likes: 0,
                            created_at: '2018-09-18 17:55:27',
                            updated_at: '2018-09-18 17:55:27',
                            isLiked: false,
                        },
                        commentedBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.PostCommented,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.PostLiked,
        notifications: [
            {
                kind: NotificationTypeKind.PostLiked,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostLikedEntity>{
                        version: 1,
                        post: {
                            id: 291,
                            user_id: 12543,
                            likes: 0,
                            reposts: 0,
                            views: 0,
                            comments: 0,
                            created_at: '2018-09-18 12:01:26',
                            update_at: '2018-09-18 12:01:26',
                            text: '\nАксолотль мексиканской амбистомы (Ambystoma mexicanum)\nАксоло́тль (Axolotl) — неотеническая личинка некоторых видов амбистом, земноводных из семейства амбистомовых (Ambystomidae) отряда хвостатых (Caudata).\n\nОсобенность аксолотля состоит в том, что он не достигает половой зрелости и становится способным к размножению, не превратившись во взрослую форму, не претерпев метаморфоз. У этих личинок хорошо развита щитовидная железа, она обычно вырабатывает достаточное количество индуцирующего метаморфозы гормона тироксина. Однако, если переселить аксолотля в более сухую и прохладную среду или понизить уровень воды при домашнем разведении, он превращается во взрослую амбистому. Превращение аксолотля в амбистому можно вызвать также добавлением в пищу или инъекцией гормона тироксина. Превращение может произойти в течение нескольких недель, при этом исчезнут наружные жабры аксолотля, изменится окраска, форма тела. Но вводить аксолотля в метаморфоз без поддержки специалиста опасно для жизни животного. Как правило, попытки в домашних условиях превратить аксолотля в амбистому в 99 % случаев заканчиваются смертью личинки.\n\nЧаще всего название «аксолотль» применяют по отношению к личинке мексиканской амбистомы (большинство содержащихся в лабораторных или домашних условиях аксолотлей принадлежат к этому виду) или тигровой амбистомы, но так можно назвать личинку любой амбистомы, способной к неотении[1].\n\nВ дословном переводе с классического науатля аксолотль (точнее, «ашолотль», axolotl) — «водяная собака», «водяное чудище» (ср. atl — вода, xolotl — собака, вместе — axolotl), что вполне соответствует его внешнему виду: аксолотль похож на крупного головастого тритона с торчащими в стороны тремя парами наружных жабр. Голова у аксолотля очень большая и широкая, несоразмерная с телом, рот тоже широкий, а глазки маленькие — создаётся впечатление, что личинка всё время улыбается. Помимо прочего, эти животные обладают способностью регенерировать утраченную часть тела. Общая длина — до 30 см. Как и все личинки хвостатых земноводных, аксолотли ведут хищный образ жизни.',
                            type: PostType.Post,
                            post: null,
                            attachments: [],
                            pinned: '0',
                            hidden: null,
                            isLiked: false,
                            isReposted: false,
                        },
                        likedBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.PostLiked,
                    },
                    text: '',
                },
            },
        ],
    },
    {
        type: NotificationTypeKind.PostCommentLiked,
        notifications: [
            {
                kind: NotificationTypeKind.PostCommentLiked,
                model: {
                    id: ++incrementId,
                    viewedAt: null,
                    createdAt: moment(new Date()).add('-2', 'minutes').toDate().getTime(),
                    entity: <NotificationPostCommentLikedEntity>{
                        version: 1,
                        comment: {
                            id: 207,
                            post_id: {
                                id: 288,
                                user_id: 13868,
                                likes: 0,
                                reposts: 0,
                                views: 2,
                                comments: 1,
                                created_at: '2018-09-17 13:07:14',
                                update_at: '2018-09-17 13:07:14',
                                text: 'привет 123',
                                type: 'post',
                                post: null,
                                attachments: [],
                                pinned: '0',
                                hidden: null,
                                isLiked: false,
                                isReposted: false,
                            },
                            user_id: 13868,
                            email: 'dmitrii.borisenko@lpsoftwarefactory.com',
                            avatar: '39c2b0fdb24a333f0d23bd73f8f912d4.png',
                            name: 'Борисенко Дмитрий Владимирович',
                            text: '{"text":"Аксолотль мексиканской амбистомы (Ambystoma mexicanum)\\nАксоло́тль (Axolotl) — неотеническая личинка некоторых видов амбистом, земноводных из семейства амбистомовых (Ambystomidae) отряда хвостатых (Caudata).\\n\\nОсобенность аксолотля состоит в том, что он не достигает половой зрелости и становится способным к размножению, не превратившись во взрослую форму, не претерпев метаморфоз. У этих личинок хорошо развита щитовидная железа, она обычно вырабатывает достаточное количество индуцирующего метаморфозы гормона тироксина. Однако, если переселить аксолотля в более сухую и прохладную среду или понизить уровень воды при домашнем разведении, он превращается во взрослую амбистому. Превращение аксолотля в амбистому можно вызвать также добавлением в пищу или инъекцией гормона тироксина. Превращение может произойти в течение нескольких недель, при этом исчезнут наружные жабры аксолотля, изменится окраска, форма тела. Но вводить аксолотля в метаморфоз без поддержки специалиста опасно для жизни животного. Как правило, попытки в домашних условиях превратить аксолотля в амбистому в 99 % случаев заканчиваются смертью личинки.\\n\\nЧаще всего название «аксолотль» применяют по отношению к личинке мексиканской амбистомы (большинство содержащихся в лабораторных или домашних условиях аксолотлей принадлежат к этому виду) или тигровой амбистомы, но так можно назвать личинку любой амбистомы, способной к неотении[1].\\n\\nВ дословном переводе с классического науатля аксолотль (точнее, «ашолотль», axolotl) — «водяная собака», «водяное чудище» (ср. atl — вода, xolotl — собака, вместе — axolotl), что вполне соответствует его внешнему виду: аксолотль похож на крупного головастого тритона с торчащими в стороны тремя парами наружных жабр. Голова у аксолотля очень большая и широкая, несоразмерная с телом, рот тоже широкий, а глазки маленькие — создаётся впечатление, что личинка всё время улыбается. Помимо прочего, эти животные обладают способностью регенерировать утраченную часть тела. Общая длина — до 30 см. Как и все личинки хвостатых земноводных, аксолотли ведут хищный образ жизни."}',
                            likes: 0,
                            created_at: '2018-09-18 17:55:27',
                            updated_at: '2018-09-18 17:55:27',
                            isLiked: false,
                        },
                        likedBy: {
                            id: 12543,
                            name: 'Иванов** Иван Иванович',
                            avatar: '0571c5dc33c4f502ed4f36e7f07d70f5.png',
                        },
                    },
                    notificationType: {
                        id: ++incrementId,
                        icon: 'demo.png',
                        name: '',
                        type: NotificationTypeKind.PostCommentLiked,
                    },
                    text: '',
                },
            },
        ],
    },
];


function setOf(type: NotificationTypeKind): Array<EvodeskNotification> {
    return sampleSets.filter(s => s.type === type)[0].notifications.map(n => {
        return <any>{
            ...n,
            model: {
                ...n.model,
                id: ++incrementId,
            },
        };
    });
}

setOf(NotificationTypeKind.PostLiked).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.PostCommented).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.PostCommentLiked).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.PostReposted).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.Post).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.EvoPointsReceived).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.EvoCoinReceived).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.IMReceivedMessage).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardDeadlineDeleted).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardDeadlineUpdated).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardDeadlineAdded).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.AchievementEarned).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardMovedToAnotherColumn).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardChecklistAdded).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardChecklistDeleted).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardCommentDeleted).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardCommentAdded).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardLabelDeleted).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardLabelAdded).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardDescriptionChanged).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardTitleChanged).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardParticipantAdded).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardParticipantDeleted).forEach(n => mockData.push(n));
setOf(NotificationTypeKind.CardDeleted).forEach(n => mockData.push(n));

@Injectable()
export class NotificationRESTMockService
{
    pull(options: { limit: number; page: number }): Observable<GetNotificationsResponse200> {
        mockData.sort((a, b) => b.model.id - a.model.id);

        const sorted: Array<EvodeskNotification> = [
            ...mockData.filter(n => n.model.viewedAt === null),
            ...mockData.filter(n => n.model.viewedAt !== null),
        ];

        return of(<GetNotificationsResponse200>{
            notifications: sorted.slice((options.page - 1) * options.limit, (options.page - 1) * options.limit + options.limit),
            count_new: sorted.filter(n => ! n.model.viewedAt).length,
            pagination: {
                total_pages: Math.ceil(sorted.length / options.limit),
                current_page: options.page,
            },
        }).pipe(delay(1000));
    }

    setAsViewed(notificationIds: Array<NotificationId>): Observable<void> {
        mockData.forEach(m => {
            if (!!~notificationIds.indexOf(m.model.id)) {
                m.model.viewedAt = new Date().getTime();
            }
        });

        return of(undefined).pipe(delay(1000));
    }
}
