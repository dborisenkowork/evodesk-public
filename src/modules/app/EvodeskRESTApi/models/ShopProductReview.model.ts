import {ProfileModel} from './profile/Profile.model';
import {ShopProductModel} from './ShopProduct.model';

export type ShopProductReviewId = number;

interface BackendDateStamp {
    date: string; // 2018-09-26 03:17:55.000000
    timezone_type: number; // 3
    timezone: string; // Europe/Moscow
}

export interface ShopProductReviewModel {
    id: ShopProductReviewId;
    service: ShopProductModel;
    creation_at: BackendDateStamp;
    answer: null;
    name: string;
    phone: string;
    price: string;
    review: string | null;
    review_at: BackendDateStamp;
    stars: number | null;
    status: null;
    user: ProfileModel;
}
