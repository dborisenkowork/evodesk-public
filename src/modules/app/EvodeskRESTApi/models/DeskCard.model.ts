import {DeskLabelModel} from './DeskLabel.model';
import {DeskCardAttachmentModel} from './DeskCardAttachment.model';
import {DeskCardLogRecordModel} from './DeskCardLogRecord.model';
import {DeskMemberModel} from './DeskMember.model';
import {DeskCardChecklistModel} from './DeskCardChecklist.model';
import {DeskCardCommentModel} from './DeskCardComment.model';
import {DeskCardBorderModel} from './DeskCardBorderColor.model';
import {DeskCardResponsibleModel} from './DeskResponsible.model';

export type DeskCardId = number;

export type DeskCardDeadline = string | null;
export type DeskCardDeletedAt = string | null;

export interface DeskCardProjectEntity {
    id: number;
    deskId: number;
    order: number;
    title: string;
}

export interface DeskCardModel
{
    id: DeskCardId;
    deskId: number;
    deskName: string;
    title: string;
    description: string | undefined;
    video: null;
    deadline: DeskCardDeadline;
    daily: boolean;
    private: 0 | 1;
    system: boolean;
    deletedAt: DeskCardDeletedAt;
    order: number;
    column: number;
    labels: Array<DeskLabelModel>;
    attachments: Array<DeskCardAttachmentModel>;
    checklists: Array<DeskCardChecklistModel>;
    comments: Array<DeskCardCommentModel>;
    actions: Array<DeskCardLogRecordModel>;
    projects: Array<DeskCardProjectEntity>;
    members: Array<DeskMemberModel>;
    responsible: Array<DeskCardResponsibleModel>;
    isTodoToday?: boolean;
    isDoneToday?: boolean;
    isArchive?: boolean;
    estimation?: null | number;
    color?: DeskCardBorderModel;
}

export interface DeskArchiveCardModel {
  column: string;
  daily: string;
  date_set_today: string;
  deadline: string | null;
  deleted_at: string;
  description: string;
  desk: string;
  id: string;
  order: string;
  orderMind: string;
  parent_system_card: string | null;
  private: 0 | 1;
  system: string;
  title: string;
  video: string | null;
}
