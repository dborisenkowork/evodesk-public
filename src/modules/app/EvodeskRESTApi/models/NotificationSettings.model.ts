export type NotificationSettingsTypeId = number;

export interface NotificationSettingsType {
    id: NotificationSettingsTypeId;
    name: string;
    icon: string;
}

export interface NotificationSettingsModel {
    notificationType: NotificationSettingsType;
    emailNotify: boolean;
    mobileNotify: boolean;
    webNotify: boolean;
}
