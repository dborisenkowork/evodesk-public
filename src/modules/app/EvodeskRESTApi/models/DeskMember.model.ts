export type DeskMemberId = number;
export type DeskMemberAvatar = string | null;

export interface DeskMemberModel {
    id: DeskMemberId;
    avatar: DeskMemberAvatar;
    email: string;
}
