export interface ProfileLegalStatusModel {
    id: number;
    title: string;
}
