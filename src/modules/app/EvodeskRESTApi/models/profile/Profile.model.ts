import {DeskModel} from '../Desk.model';

export const MIN_ALIAS_LENGTH: number = 4;
export const MAX_ALIAS_LENGTH: number = 20;

export type ProfileId = number;
export type ProfileUrlAlias = string | null | undefined;
export type ProfileAvatar = string | null;

export function validateUrlAlias(input: string) {
    const urlAliasRegexp: RegExp = /^[a-zA-Z0-9_]+$/;
    const urlAliasDigitOnly: RegExp = /^(\d+)$/;

    return input.length >= MIN_ALIAS_LENGTH
        && input.length <= MAX_ALIAS_LENGTH
        && urlAliasRegexp.test(input)
        && ! urlAliasDigitOnly.test(input)
    ;
}

export enum PageVisibility {
    Everyone = '0',
    MySubscribers = '1',
    SubscribersRelated = '2',
}

export enum PhotoVisibility {
    Everyone = '0',
    MySubscribers = '1',
    SubscribersRelated = '2',
}

export enum SubscribeAccess {
    Everyone = '0',
    MySubscriptions = '1',
    SubscribersRelated = '2',
}

export interface ProfileModel {
    id: ProfileId;
    email: string;
    desk: number;
    admin: null;
    name: null | string;
    additional: null;
    avatar: ProfileAvatar;
    address: null | string;
    birth_date: null | string;
    phone: null | string;
    company_name: null;
    position: null | string;
    industry: null | string;
    industry_area: null | string;
    industry_niche: null | string;
    industry_competence: null | string;
    balance: string;
    balanceRub: string;
    balanceEvocoin: string;
    coefficient: string;
    activeted: string;
    type: 'unpaid';
    shop_description: null;
    shop_title: null;
    images: Array<any>;
    rub: string;
    coins: string;
    rating: null;
    hidewallet: string;
    payeer: null;
    promocodes: Array<any>;
    tutorial: string;
    timezone: string;
    promo: string;
    assistant: null;
    assistant_expire: null;
    experience: string;
    linkVk: null;
    linkFb: null;
    linkTw: null;
    linkGp: null;
    linkOk: null;
    linkMr: null;
    sharedVk: boolean;
    sharedFb: boolean;
    sharedTw: boolean;
    sharedGp: boolean;
    sharedOk: boolean;
    sharedMr: boolean;
    level: number;
    currentExp: number;
    currentExpPercent: number;
    nextExp: number;
    availableDesks: Array<DeskModel>;
    statistics: {
        tasks: {
            completed: number;
            overdue: number;
            failed: number;
        };
    };
    url_alias: ProfileUrlAlias;
    page_visibality: PageVisibility;
    photo_visibality: PhotoVisibility;
    subscribe_level: SubscribeAccess;
}
