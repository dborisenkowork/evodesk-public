export type ProfileEducationId = number;

export interface ProfileEducationModelMetadataEntry {
    type: ProfileEducationType;
    translate: string;
    canContainsSpecialty: boolean;
}

export enum ProfileEducationType {
    Secondary = 'secondary',
    SecondaryProfessional = 'secondary-professional',
    HigherBachelor = 'higher-bachelor',
    HigherSpecialistDegree = 'higher-specialist-degree',
    HigherMastersDegree = 'higher-masters-degree',
    HigherAspirant = 'higher-aspirant',
    HigherResidency = 'higher-residency',
    HigherAssistentTraining = 'higher-assistent-training',
}

export interface ProfileEducationModel {
    id: ProfileEducationId;
    type: string;
    city: string;
    year: number;
    name: string;
    specialty: string;
}

export const profileEducationTypeMetadata: Array<ProfileEducationModelMetadataEntry> = [
    {
        type: ProfileEducationType.Secondary,
        translate: 'EvodeskRESTAPI.models.profile.ProfileEducation.Types.Secondary',
        canContainsSpecialty: false,
    },
    {
        type: ProfileEducationType.SecondaryProfessional,
        translate: 'EvodeskRESTAPI.models.profile.ProfileEducation.Types.SecondaryProfessional',
        canContainsSpecialty: true,
    },
    {
        type: ProfileEducationType.HigherBachelor,
        translate: 'EvodeskRESTAPI.models.profile.ProfileEducation.Types.HigherBachelor',
        canContainsSpecialty: true,
    },
    {
        type: ProfileEducationType.HigherSpecialistDegree,
        translate: 'EvodeskRESTAPI.models.profile.ProfileEducation.Types.HigherSpecialistDegree',
        canContainsSpecialty: true,
    },
    {
        type: ProfileEducationType.HigherMastersDegree,
        translate: 'EvodeskRESTAPI.models.profile.ProfileEducation.Types.HigherMastersDegree',
        canContainsSpecialty: true,
    },
    {
        type: ProfileEducationType.HigherAspirant,
        translate: 'EvodeskRESTAPI.models.profile.ProfileEducation.Types.HigherAspirant',
        canContainsSpecialty: true,
    },
    {
        type: ProfileEducationType.HigherResidency,
        translate: 'EvodeskRESTAPI.models.profile.ProfileEducation.Types.HigherResidency',
        canContainsSpecialty: true,
    },
    {
        type: ProfileEducationType.HigherAssistentTraining,
        translate: 'EvodeskRESTAPI.models.profile.ProfileEducation.Types.HigherAssistentTraining',
        canContainsSpecialty: true,
    },
];
