import {WorkCompetenceApproveModel} from '../WorkCompetenceApprove.model';

export type ProfileExperienceId = number;

export function parseWorkExperienceJSON(input: string): ProfileExperienceJSON {
    const emptyJSONModel: Function = () => {
        return {
            about: input,
            responsibilities: [],
            skills: [],
            tools: [],
        };
    };

    try {
        const parsed: any = JSON.parse(input);

        if (!!parsed && (typeof parsed === 'object')) {
            return parsed;
        } else {
            return emptyJSONModel();
        }
    } catch (err) {
        return emptyJSONModel();
    }
}

export interface ProfileExperienceJSON {
    about: string;
    responsibilities: Array<{ value: string }>;
    tools: Array<{ value: string }>;
    skills: Array<{ value: string }>;
}

export interface ProfileExperienceModel {
    id: ProfileExperienceId;
    text: string;
    site: string;
    date_start: string;
    date_finish: string;
    place: string;
    position: string;
    sphere: number;
    approves: Array<WorkCompetenceApproveModel>;
}
