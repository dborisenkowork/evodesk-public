export type DeskCardCommentId = number;

export interface DeskCardCommentAuthorModel
{
    id: number;
    email: string;
}

export interface DeskCardCommentModel
{
    id: DeskCardCommentId;
    card: number;
    creation_at: string;
    parent: DeskCardCommentId | null;
    text: string;
    user: DeskCardCommentAuthorModel;
    changed: boolean;
}
