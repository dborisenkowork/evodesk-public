import {ProfileAvatar, ProfileId} from './profile/Profile.model';
import {AchievementModel} from './achievements/Achievement.model';
import {PostCommentModel} from './post/PostComment.model';
import {PostModel} from './post/Post.model';
import {DeskId} from './Desk.model';
import {DeskColumnId} from './DeskColumnDefinition.model';

export type NotificationId = number;

export enum NotificationTypeKind {
    AchievementEarned = 'AchievementEarned',
    CardDeleted = 'CardDeleted',
    CardParticipantAdded = 'CardParticipantAdded',
    CardParticipantDeleted = 'CardParticipantDeleted',
    CardTitleChanged = 'CardTitleChanged',
    CardDescriptionChanged = 'CardDescriptionChanged',
    CardLabelAdded = 'CardLabelAdded',
    CardLabelDeleted = 'CardLabelDeleted',
    CardCommentAdded = 'CardCommentAdded',
    CardCommentDeleted = 'CardCommentDeleted',
    CardChecklistAdded = 'CardChecklistAdded',
    CardChecklistDeleted = 'CardChecklistDeleted',
    CardMovedToAnotherColumn = 'CardMovedToAnotherColumn',
    CardDeadlineUpdated = 'CardDeadlineUpdated',
    CardDeadlineAdded = 'CardDeadlineAdded',
    CardDeadlineDeleted = 'CardDeadlineDeleted',
    IMReceivedMessage = 'IMReceivedMessage',
    EvoCoinReceived = 'EvoCoinReceived',
    EvoPointsReceived = 'EvoPointsReceived',
    Post = 'Post',
    PostLiked = 'PostLiked',
    PostReposted = 'PostReposted',
    PostCommented = 'PostCommented',
    PostCommentLiked = 'PostCommentLiked',
    EvodeskReleaseNotes = 'EvodeskReleaseNotes',
}

export interface NotificationType {
    id: number;
    type: NotificationTypeKind;
    name: string;
    icon: string;
}

export type EvodeskNotification =
      { kind: NotificationTypeKind.AchievementEarned, model: NotificationModel<NotificationAchievementEarnedEntity> }
    | { kind: NotificationTypeKind.CardDeleted, model: NotificationModel<NotificationCardDeletedEntity> }
    | { kind: NotificationTypeKind.CardParticipantAdded, model: NotificationModel<NotificationCardParticipantAddedEntity> }
    | { kind: NotificationTypeKind.CardParticipantDeleted, model: NotificationModel<NotificationCardParticipantDeletedEntity> }
    | { kind: NotificationTypeKind.CardTitleChanged, model: NotificationModel<NotificationCardTitleChangedEntity> }
    | { kind: NotificationTypeKind.CardDescriptionChanged, model: NotificationModel<NotificationCardDescriptionChangedEntity> }
    | { kind: NotificationTypeKind.CardLabelAdded, model: NotificationModel<NotificationCardLabelAddedEntity> }
    | { kind: NotificationTypeKind.CardLabelDeleted, model: NotificationModel<NotificationCardLabelDeletedEntity> }
    | { kind: NotificationTypeKind.CardCommentAdded, model: NotificationModel<NotificationCardCommentAddedEntity> }
    | { kind: NotificationTypeKind.CardCommentDeleted, model: NotificationModel<NotificationCardCommentDeletedEntity> }
    | { kind: NotificationTypeKind.CardChecklistAdded, model: NotificationModel<NotificationCardChecklistAddedEntity> }
    | { kind: NotificationTypeKind.CardChecklistDeleted, model: NotificationModel<NotificationCardChecklistDeletedEntity> }
    | { kind: NotificationTypeKind.CardMovedToAnotherColumn, model: NotificationModel<NotificationCardMovedToAnotherColumnEntity> }
    | { kind: NotificationTypeKind.CardDeadlineUpdated, model: NotificationModel<NotificationCardDeadlineUpdatedEntity> }
    | { kind: NotificationTypeKind.CardDeadlineAdded, model: NotificationModel<NotificationCardDeadlineAddedEntity> }
    | { kind: NotificationTypeKind.CardDeadlineDeleted, model: NotificationModel<NotificationCardDeadlineDeletedEarnedEntity> }
    | { kind: NotificationTypeKind.IMReceivedMessage, model: NotificationModel<NotificationIMReceivedMessageEarnedEntity> }
    | { kind: NotificationTypeKind.EvoCoinReceived, model: NotificationModel<NotificationEvoCoinReceivedEntity> }
    | { kind: NotificationTypeKind.EvoPointsReceived, model: NotificationModel<NotificationEvoPointsReceivedEntity> }
    | { kind: NotificationTypeKind.Post, model: NotificationModel<NotificationPostEntity> }
    | { kind: NotificationTypeKind.PostLiked, model: NotificationModel<NotificationPostLikedEntity> }
    | { kind: NotificationTypeKind.PostReposted, model: NotificationModel<NotificationPostRepostedEntity> }
    | { kind: NotificationTypeKind.PostCommented, model: NotificationModel<NotificationPostCommentedEntity> }
    | { kind: NotificationTypeKind.PostCommentLiked, model: NotificationModel<NotificationPostCommentLikedEntity> }
    | { kind: NotificationTypeKind.EvodeskReleaseNotes, model: NotificationModel<NotificationPostCommentLikedEntity> }
;

export interface NotificationModel<T extends AbstractNotificationModelEntity> {
    id: NotificationId;
    notificationType: NotificationType;
    entity: T;
    text: string;
    createdAt: number; // 1535614608
    viewedAt: number | null; // 1535614608
}

export interface AbstractNotificationCardEntity {
    deskId: DeskId;
    cardId: DeskId;
    cardTitle: string;
    deskType: 'personal' | 'multidesk';
    deskOwnedId: ProfileId;
    deskName?: string;
    deskColor?: string;
}

export interface AbstractNotificationModelEntity {
    version: number;
}

export interface NotificationAchievementEarnedEntity extends AbstractNotificationModelEntity {
    version: 1;
    achievement: AchievementModel;
}

export interface NotificationCardDeletedEntity extends AbstractNotificationCardEntity {
    version: 1;
}

export interface NotificationCardParticipantAddedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    participantId: ProfileId;
    participantAvatar: ProfileAvatar;
    participantName: string; // profile.name || profile.email
}

export interface NotificationCardParticipantDeletedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    participantId: ProfileId;
    participantAvatar: ProfileAvatar;
    participantName: string; // profile.name || profile.email
}

export interface NotificationCardTitleChangedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    oldTitle: string;
    newTitle: string;
}

export interface NotificationCardTitleChangedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
}

export interface NotificationCardDescriptionChangedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
}

export interface NotificationCardLabelAddedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    labelId: number;
    labelColor: string;
    labelName: string;
}

export interface NotificationCardLabelDeletedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    labelId: number;
    labelColor: string;
    labelName: string;
}

export interface NotificationCardCommentAddedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    commentText: string;
}

export interface NotificationCardCommentDeletedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    commentText: string;
}

export interface NotificationCardChecklistAddedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    checklistId: number;
    checklistName: string;
}

export interface NotificationCardChecklistDeletedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    checklistId: number;
    checklistName: string;
}

export interface NotificationCardMovedToAnotherColumnEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    previousColumnId: DeskColumnId;
    newColumnId: DeskColumnId;
}

export interface NotificationCardDeadlineUpdatedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    previousDeadline: string | null;
    newDeadline: string | null;
}

export interface NotificationCardDeadlineAddedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    newDeadline: string | null;
}

export interface NotificationCardDeadlineDeletedEarnedEntity extends AbstractNotificationModelEntity, AbstractNotificationCardEntity {
    version: 1;
    previousDeadline: string | null;
}

export interface NotificationIMReceivedMessageEarnedEntity extends AbstractNotificationModelEntity {
    version: 1;
    message: {
        id: number;
        dialogId: number;
        text: string;
        createdAt: string; // 2018-09-18 11:05:28
    };
    author: {
        id: ProfileId;
        avatar: ProfileAvatar;
        name: string; // profile.name || profile.email
    };
}

export interface NotificationEvoCoinReceivedEntity extends AbstractNotificationModelEntity {
    version: 1;
    received: string; // '1.00'
}

export interface NotificationEvoPointsReceivedEntity extends AbstractNotificationModelEntity {
    version: 1;
    received: string; // '1.00'
}

export interface NotificationPostEntity extends AbstractNotificationModelEntity {
    version: 1;
    post: PostModel;
    postBy: {
        id: ProfileId;
        avatar: ProfileAvatar;
        name: string; // profile.name || profile.email
    };
}

export interface NotificationPostRepostedEntity extends AbstractNotificationModelEntity {
    version: 1;
    post: PostModel;
    repost: PostModel;
    repostBy: {
        id: ProfileId;
        avatar: ProfileAvatar;
        name: string; // profile.name || profile.email
    };
}

export interface NotificationPostLikedEntity extends AbstractNotificationModelEntity {
    version: 1;
    post: PostModel;
    likedBy: {
        id: ProfileId;
        avatar: ProfileAvatar;
        name: string; // profile.name || profile.email
    };
}

export interface NotificationPostCommentedEntity extends AbstractNotificationModelEntity {
    version: 1;
    post: PostModel;
    comment: PostCommentModel;
    commentedBy: {
        id: ProfileId;
        avatar: ProfileAvatar;
        name: string; // profile.name || profile.email
    };
}

export interface NotificationPostCommentLikedEntity extends AbstractNotificationModelEntity {
    version: 1;
    comment: PostCommentModel;
    likedBy: {
        id: ProfileId;
        avatar: ProfileAvatar;
        name: string; // profile.name || profile.email
    };
}

export interface NotificationEvodeskReleaseNotesEntity extends AbstractNotificationModelEntity {
    version: 1;
    release: string; // '1', 'some-release-name', ...
}
