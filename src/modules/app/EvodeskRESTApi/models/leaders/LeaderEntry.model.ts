export type LeaderEntryAvatar = string | undefined;

export interface LeaderEntryModel
{
    id: string;
    avatar: LeaderEntryAvatar;
    email: string;
    experience: string;
    name: string;
    position: number;
    self: number;
}
