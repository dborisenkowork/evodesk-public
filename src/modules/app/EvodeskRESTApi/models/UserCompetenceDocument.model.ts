export type UserCompetenceDocumentId = number;

export enum UserCompetenceDocumentStatus {
    Pending = 0,
    Accepted = 1,
    Rejected = 2,
}

export interface UserCompetenceDocumentModel {
    id: UserCompetenceDocumentId;
    created_at: string; // "2018-09-01 13:32:58"
    points: number;
    status: UserCompetenceDocumentStatus;
    title: string;
    user_id: number;
    user_name: string | undefined;
    user_email: string;
    competence_user_id: number;
    competence_id: {
        id: number;
        title: string;
    };
    admin_comment: null | string;
}
