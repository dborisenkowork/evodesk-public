import {ProfileId} from './profile/Profile.model';
import {CompetenceId} from './Competence.model';

export type UserCompetenceSkillId = number;

export interface UserCompetenceSkillModel {
    id: UserCompetenceSkillId;
    user_id: ProfileId;
    competence_id: CompetenceId;
    skill: string;
}

