import {ProfileId} from './profile/Profile.model';

export interface PartnerModel {
    id: ProfileId;
    name: string;
    regs: number;
    pnts: number;
}
