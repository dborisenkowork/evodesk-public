import {ProfileExperienceId} from './profile/ProfileExperience.model';

export type WorkCompetenceApproveId = number;

export interface WorkCompetenceApproveModel {
    id: WorkCompetenceApproveId;
    created_at: string; // "2018-08-27 12:17:59"
    experience_id: ProfileExperienceId;
    status: number; /* 0 | 1 | 2 */
    user_id: number;
    user_position: string;
    user_name: string;
    user_email: string;
}
