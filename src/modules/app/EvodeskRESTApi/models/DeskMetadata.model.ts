import {DeskMemberModel} from './DeskMember.model';

export interface DeskMetadataModel
{
    is: number;
    isDefault: null;
    invite_code: string;
    members: Array<DeskMemberModel>;
    title: string;
    ownerId: number;
}
