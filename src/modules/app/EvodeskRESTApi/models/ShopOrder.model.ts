import {ShopProductModel} from './ShopProduct.model';
import {ProfileModel} from './profile/Profile.model';

export type ShopOrderId = number;

interface BackendDateStamp {
    date: string; // 2018-09-26 03:17:55.000000
    timezone_type: number; // 3
    timezone: string; // Europe/Moscow
}

export interface ShopOrderModel {
    id: ShopOrderId;
    service: ShopProductModel;
    user: ProfileModel;
    name: null;
    phone: null;
    price: string; // '100.00'
    creation_at: BackendDateStamp;
    reviews: null;
    stars: null;
    answer: null;
    review_at: null;
    status: 0 | 1;
}
