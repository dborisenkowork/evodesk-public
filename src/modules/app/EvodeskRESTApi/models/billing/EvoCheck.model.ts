export type EvoCheckId = number;

export interface EvoCheckModel {
    id: EvoCheckId;
    balance_from: number;
    balance_to: number | null;
    balance_sum: string;
    code: string;
    secret: string;
    created_at: {
        date: string;
        timezone_type: number;
        timezone: string;
    };
    closed_at: null | {
        date: string;
        timezone_type: number;
        timezone: string;
    };
}
