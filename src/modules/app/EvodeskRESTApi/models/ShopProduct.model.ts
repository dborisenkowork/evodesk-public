import {ShopCategoryId} from './ShopCategory.model';
import {ProfileModel} from './profile/Profile.model';

export type ShopProductId = number;
export type ShopProductSlug = string;

interface BackendDateStamp {
    date: string; // 2018-09-26 03:17:55.000000
    timezone_type: number; // 3
    timezone: string; // Europe/Moscow
}

export interface ShopProductModel {
    id: ShopProductId;
    category: ShopCategoryId;
    slug: ShopProductSlug;
    title: string;
    owner: null | ProfileModel;
    description: string; // HTML
    price: string; // "1500.00"
    image: string; // 530cdbd665e9edc519352cf317139551.jpeg
    file: null;
    link: null;
    status: 0 | 1;
    status_message: undefined | string;
    created_at: BackendDateStamp;
    deleted: 0 | 1;
    deletedAt: BackendDateStamp | null;
    stars: number;
    orders: number;
    reviews: number;
    promocodes: boolean;
    haspromocode: boolean;
    system: boolean;
    isFavorite: boolean;
}
