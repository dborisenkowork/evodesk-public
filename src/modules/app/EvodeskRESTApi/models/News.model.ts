import {PostModel} from './post/Post.model';
import {ProfileModel} from './profile/Profile.model';
import {PostCommentModel} from './post/PostComment.model';
import {PostLikeModel} from './PostLike.model';
import {PostCommentLikeModel} from './PostCommentLike.model';

export type NewsEntry = NewsPostEntry | NewsPostReposted | NewsPostLiked | NewsPostCommented | NewsPostCommentLiked;

export enum NewsEntryType {
    Post = 'post',
    PostLiked = 'likePost',
    PostReposted = 'repost',
    PostCommented = 'getComment',
    PostCommentLiked = 'likeComment',
}

export interface NewsPostEntry {
    type: NewsEntryType.Post;
    data: {
        entity: PostModel;
    };
}

export interface NewsPostLiked {
    type: NewsEntryType.PostLiked;
    data: {
        user: ProfileModel;
        entity: PostModel;
        like: PostLikeModel;
    };
}

export interface NewsPostCommented {
    type: NewsEntryType.PostCommented;
    data: {
        user: ProfileModel;
        entity: PostCommentModel;
    };
}

export interface NewsPostReposted {
    type: NewsEntryType.PostReposted;
    data: {
        user: ProfileModel;
        entity: PostModel;
        repost: PostModel;
    };
}

export interface NewsPostCommentLiked {
    type: NewsEntryType.PostCommentLiked;
    data: {
        user: ProfileModel;
        entity: PostCommentLikeModel;
        comment: PostCommentModel;
    };
}
