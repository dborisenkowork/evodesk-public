import {CompetenceModel} from './Competence.model';

export type CompetenceCategoryId = number;

export interface CompetenceCategoryModel {
    id: CompetenceCategoryId;
    title: string;
    sort_order: number;
    competences: Array<CompetenceModel>;
}
