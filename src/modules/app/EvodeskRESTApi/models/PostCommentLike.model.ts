import {PostCommentModel} from './post/PostComment.model';

export interface PostCommentLikeModel {
    id: number;
    comment_id: PostCommentModel;
    user_id: string; // "12345"
    created_at: string; // 2018-08-22 13:56:40
}
