export type ShopCategoryId = number;
export type ShopCategorySlug = string;

export interface ShopCategoryModel {
    id: ShopCategoryId;
    slug: ShopCategorySlug;
    comission: string; // "10.00"
    title: string;
}
