export type NotificationTypeId = number;

export interface NotificationType {
    id: NotificationTypeId;
    name: string;
    icon: string;
}

export interface NotificationsSettingsModel {
    emailNotify: true;
    mobileNotify: true;
    webNotify: true;
    notificationType: Array<NotificationType>;
}
