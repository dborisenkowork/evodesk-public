import {ProfileId, ProfileModel} from '../profile/Profile.model';

import * as moment from 'moment';

export type IMRESTDialogId = number;

export type IMRESTMessageId = number;

export interface IMRESTMessage {
    id: IMRESTMessageId;
    attachment_file: string;
    creation_at: string;
    read: 0 | 1;
    group: number;
    text: string;
    user_from: {
        id: ProfileModel;
    };
    user_to: {
        id: ProfileModel;
    };
}

export interface IMRESTChatMessage {
    author_avatar: string;
    author_email: string;
    author_id: ProfileId;
    author_name: string;
    id: IMRESTMessageId;
    name: string;
    name2: string | null;
    email: string;
    avatar: string | null;
    avatar2: string | null;
    user_to: ProfileId;
    text: string;
    read: 0 | 1;
    group: IMRESTDialogId;
    creation_at: {
        date: string; // "2018-08-11 11:58:10.000000"
        timezone: string; // "Europe/Moscow"
        timezone_type: number; // 3
    };
}

export interface IMRESTSendMessageResponse {
    id: IMRESTMessageId;
    attachment_file: string;
    creation_at: string;
    read: 0 | 1;
    group: number;
    text: string;
    user_from: {
        id: ProfileModel;
    };
    user_to: {
        id: ProfileModel;
    };
}

export interface IMRESTContact {
    avatar: string | null;
    avatar2: string | null;
    creation_at: {
        date: string; // "2018-08-11 11:58:10.000000"
        timezone: string; // "Europe/Moscow"
        timezone_type: number; // 3
    };
    email: string;
    group: number;
    id: number;
    name: null | string;
    read: 0 | 1;
    text: string;
}

export function imRESTMessageBackendDateStringToDate(input: string): Date {
    return moment(('' + input).substr(0, 19), 'YYYY-MM-DD HH:mm:ss').toDate();
}
