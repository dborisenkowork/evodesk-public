import {ProfileId} from './profile/Profile.model';

export type CompetenceMarkId = number;

export interface CompetenceMarkModel {
    id: CompetenceMarkId;
    created_at: string; // "2018-09-01 02:57:47"
    user_id: ProfileId;
    comment: string;
    competence_id: number;
    mark: 0 | 1;
    marker_id: number;
    status: null;
}
