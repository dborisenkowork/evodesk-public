export type DeskCardAttachmentId = number;
export type CardAttachmentName = null | string;
export type CardAttachmentPreview = null;
export type CardAttachmentLink = null | string;
export type DeskCardMIMEType = string;

export interface DeskCardAttachmentModel
{
    id: DeskCardAttachmentId;
    card: number;
    path: string;
    created_at: string;
    name: CardAttachmentName;
    type: DeskCardMIMEType;
    preview: CardAttachmentPreview;
    link: CardAttachmentLink;
    width?: number;
    height?: number;
}
