export type DeskCardChecklistId = number;
export type DeskCardChecklistItemId = number;

export interface DeskCardChecklistModel
{
    id: DeskCardChecklistId;
    name: string;
    items: Array<DeskCardChecklistItemModel>;
}

export interface DeskCardChecklistItemModel
{
    id: DeskCardChecklistItemId;
    title: string;
    completed: boolean;
}
