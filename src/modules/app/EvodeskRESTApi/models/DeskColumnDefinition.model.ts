export type DeskColumnId = number;

export enum DeskColumnType
{
    Projects = <any>'projects',
    Backlog = <any>'backlog',
    CommittedWeek = <any>'committed-week',
    CommittedToday = <any>'committed-today',
    Done = <any>'done',
    Failed = <any>'failed',
}

export type DeskColumnEntitiesType = 'cards' | 'projects';

export interface DeskColumnDefinitionModel
{
    id: DeskColumnId;
    type: DeskColumnType;
    name: string;
    order: number;
    entities: DeskColumnEntitiesType;
}

export interface DeskCardCanBeMovedNextToEntry {
    from: DeskColumnType;
    to: DeskColumnType;
}

export const deskCardCanBeAddedTo: Array<DeskColumnType> = [
    DeskColumnType.Backlog,
    DeskColumnType.CommittedWeek,
    DeskColumnType.CommittedToday,
];

export const doneColumns: Array<DeskColumnType> = [
    DeskColumnType.Done,
];

export const failedColumns: Array<DeskColumnType> = [
    DeskColumnType.Failed,
];

export const deskCardCanBeMovedNextTo: Array<DeskCardCanBeMovedNextToEntry> = [
    { from: DeskColumnType.Backlog, to: DeskColumnType.CommittedWeek },
    { from: DeskColumnType.CommittedWeek, to: DeskColumnType.CommittedToday },
    { from: DeskColumnType.CommittedToday, to: DeskColumnType.Done },
    { from: DeskColumnType.Done, to: DeskColumnType.Failed },
];


const mapTitles: Array<{ id: number, title: string, to: DeskColumnType }> = [
    { id: 1, title: 'Общий список задач', to: DeskColumnType.Backlog },
    { id: 2, title: 'Рывок на неделю', to: DeskColumnType.CommittedWeek },
    { id: 3, title: 'Задачи на сегодня', to: DeskColumnType.CommittedToday },
    { id: 4, title: 'Выполнено', to: DeskColumnType.Done },
    { id: 5, title: 'Провалено', to: DeskColumnType.Failed },
    { id: 7, title: 'Проекты', to: DeskColumnType.Projects },
];

export function deskColumnTypeToTranslation(type: DeskColumnType): string {
    switch (type) {
        default:
            throw new Error(`Unknown desk column type ${type}`);

        case DeskColumnType.Projects:
            return 'EvodeskDesk.models.DeskColumnDefinition.Types.Projects';

        case DeskColumnType.Backlog:
            return 'EvodeskDesk.models.DeskColumnDefinition.Types.Backlog';

        case DeskColumnType.CommittedWeek:
            return 'EvodeskDesk.models.DeskColumnDefinition.Types.CommittedWeek';

        case DeskColumnType.CommittedToday:
            return 'EvodeskDesk.models.DeskColumnDefinition.Types.CommittedToday';

        case DeskColumnType.Done:
            return 'EvodeskDesk.models.DeskColumnDefinition.Types.Done';

        case DeskColumnType.Failed:
            return 'EvodeskDesk.models.DeskColumnDefinition.Types.Failed';
    }
}

export function getDeskColumnTypeById(id: number): DeskColumnType {
    const result: { title: string, to: DeskColumnType } = mapTitles.filter(r => r.id === id)[0];

    if (result) {
        return result.to;
    } else {
        throw new Error(`Unknown column type for column ID("${id}")`);
    }
}


export function getDeskColumnTypeByName(input: { name: string }): DeskColumnType {
    const result: { title: string, to: DeskColumnType } = mapTitles.filter(r => r.title === input.name)[0];

    if (result) {
        return result.to;
    } else {
        throw new Error(`Unknown column type for column "${input.name}"`);
    }
}
