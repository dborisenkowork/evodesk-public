export interface CompetenceIndexesModel {
    competence: number;
    activity: number;
    popularity: number;
    business_activity: number;
}
