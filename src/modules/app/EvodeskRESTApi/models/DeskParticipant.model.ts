export type DeskParticipantId = number;

export interface DeskParticipantModel {
    id: DeskParticipantId;
    active?: boolean;
    email: string;
    name?: string;
    avatar: string | null;
}
