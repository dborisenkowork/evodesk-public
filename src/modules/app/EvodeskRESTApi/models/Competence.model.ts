export type CompetenceId = number;

export interface CompetenceModel {
    id: CompetenceId;
    title: string;
}
