export interface UserActivityModel {
    id: number;
    datetime: string; // 2018-07-31 10:17:35
    device: string;
    place: string;
    browser: string;
}
