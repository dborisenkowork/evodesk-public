import {ProfileId, ProfileModel} from '../profile/Profile.model';

export type UserId = ProfileId;

export type UserModel = ProfileModel;

export interface UserBalanceModel {
    points: number;
    evocoin: number;
    rub: number;
}
