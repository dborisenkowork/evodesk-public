export interface UserBalanceModel
{
  point: string;
  type: 'paid' | 'unpaid';
}
