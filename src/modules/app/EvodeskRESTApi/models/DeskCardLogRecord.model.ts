import {UserModel} from './user/User.model';

export type DeskLogRecordId = number;

export interface DeskCardLogRecordModel {
    id: DeskLogRecordId;
    user: UserModel;
    action: string;
    changedField: string;
    creation_at: string; /* Example: 2018-05-01 00:51:46 */
    entity: string;
    entity_id: string;
    value: string;
}
