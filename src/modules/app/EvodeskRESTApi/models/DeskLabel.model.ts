export type DeskLabelId = number;
export type DeskLabelName = string | null;

export interface DeskLabelModel
{
    id: DeskLabelId;
    color: DeskLabelColor;
    name: DeskLabelName;
}

export enum DeskLabelColor {
    Red = 'Red',
    Orange = 'Orange',
    Yellow = 'Yellow',
    Green = 'Green',
    Cyan = 'Cyan',
    Blue = 'Blue',
    DeepBlue = 'DeepBlue',
    Purple = 'Purple',
    Pink = 'Pink',
    Gray = 'Gray',
}

export interface DeskLabelColorDescription {
    id: DeskLabelColor;
    color: string;
    translate: string;
}
