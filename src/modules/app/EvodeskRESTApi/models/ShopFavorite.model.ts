import {ShopProductModel} from './ShopProduct.model';

import {ProfileModel} from './profile/Profile.model';

export type ShopFavoriteId = number;

export interface ShopFavoriteModel {
    id: ShopFavoriteId;
    service: ShopProductModel;
    user: ProfileModel;
}
