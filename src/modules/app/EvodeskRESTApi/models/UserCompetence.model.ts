import {ProfileId} from './profile/Profile.model';
import {CompetenceModel} from './Competence.model';

export type UserCompetenceModelId = number;

export interface UserCompetenceModel {
    id: UserCompetenceModelId;
    user_id: ProfileId;
    competence: CompetenceModel;
    active: 0 | 1;
    can_mark: boolean;
    documents: number;
    marks: number;
    points: string;
    skills: number;
}
