import {DeskCardId} from './DeskCard.model';
import {DeskModel} from './Desk.model';

export type DeskProjectId = number;

export interface DeskProjectModel {
    id: DeskProjectId;
    cards: Array<DeskCardId>;
    desk: DeskModel;
    order: number;
    title: string;
    users: Array<number>;
}
