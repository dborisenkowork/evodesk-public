import {PostModel} from './post/Post.model';

export interface PostLikeModel {
    id: number;
    post_id: PostModel;
    user_id: string; // "12345",
    created_at: string; // 2018-08-22 14:26:18
}
