export interface RatingEntryModel
{
    date: string;
    points: string;
    type: string;
}
