export interface Album {
  description: string;
  id: number;
  name: string;
  photos: Array<Photo>;
  preview: string;
  updated_at: string;
  user_id: string;
}

export interface Photo {
  id: number;
  description: string;
  path?: string;
}
