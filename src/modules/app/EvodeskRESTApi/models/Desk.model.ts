import {DeskParticipantModel} from './DeskParticipant.model';
import {ProfileId} from './profile/Profile.model';

export type DeskId = number;
export type DeskModelAvatar = null | string;
export type DeskModelColor = null | string;

export interface DeskModel
{
    id: DeskId;
    invite_code: string;
    name: string | null;
    avatar: DeskModelAvatar;
    ownerId: ProfileId;
    users?: Array<DeskParticipantModel>;
    members?: Array<DeskParticipantModel>;
    color: DeskModelColor;
}
