import {PostModel} from './Post.model';
import {UserId} from '../user/User.model';

export type PostCommentId = number;

export interface PostCommentModel {
    id: PostCommentId;
    text: string;
    avatar: string | null;
    created_at: string;
    updated_at: string | null;
    user_id: UserId;
    isLiked: boolean;
    likes: number;
    name: string;
    email: string;
    post_id: PostModel;
}
