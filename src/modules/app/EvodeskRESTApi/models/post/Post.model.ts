import {UserId} from '../user/User.model';

export type PostId = number;
export type PostModelDateTime = string; // "2018-06-27 19:09:03"

export enum PostType {
    Post = 'post',
    Text = 'text',
    Photo = 'photo',
    Video = 'video',
}

export type AnyAttachmentModel = AttachmentPhotoModel | AttachmentVideoModel | AttachmentLinkModel;

export type AttachmentId = number;

export enum AttachmentType {
    Post = 'post',
    Photo = 'photo',
    Video = 'video',
    Link = 'link',
}

interface AttachmentBase /* DO NOT export it. Thank you. */ {
    id: AttachmentId;
    type: AttachmentType;
    created_at: string;
    updated_at: string; // "2018-07-01 23:38:16"
    name: null;
    path: null;
    post_id: null | PostId;
    preview: string | null;
    user_id: null;
}

export interface AttachmentPhotoModel extends AttachmentBase {
}

export interface AttachmentVideoModel extends AttachmentBase {
}

export interface AttachmentLinkModel extends AttachmentBase {
}


export interface AttachmentListItemPost { type: AttachmentType.Post; attachment: AttachmentPostModel; }
export interface AttachmentListItemPhoto { type: AttachmentType.Photo; attachment: AttachmentPhotoModel; }
export interface AttachmentListItemVideo { type: AttachmentType.Video; attachment: AttachmentVideoModel; }
export interface AttachmentListItemLink { type: AttachmentType.Link; attachment: AttachmentLinkModel; }

export type AttachmentListItem = AttachmentListItemPost | AttachmentListItemPhoto | AttachmentListItemVideo | AttachmentListItemLink;

export interface PostModel {
    id: PostId;
    attachments: Array<AnyAttachmentModel>;
    created_at: PostModelDateTime;
    hidden: null;
    isLiked: boolean;
    isReposted: boolean;
    reposts: number;
    likes: number;
    pinned: '0' | '1';
    text: string;
    type: PostType;
    update_at: PostModelDateTime;
    user_id: UserId;
    views: number;
    comments: number;
    post?: PostModel;
}

export interface AttachmentPostModel {
    id: number;
    origPost: PostModel;
    repost: PostModel;
}
