import {DeskProjectId} from './DeskProject.model';

export type DeskProjectMemberId = number;
export type DeskProjectMemberAvatar = string | null;

export interface DeskProjectMemberModel {
    id: DeskProjectMemberId;
    name: null | string;
    project: DeskProjectId;
    avatar: DeskProjectMemberAvatar;
    active: boolean;
    email: string;
}
