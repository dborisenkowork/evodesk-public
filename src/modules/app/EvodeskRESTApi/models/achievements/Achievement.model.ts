export interface AchievementModel
{
    bonus: string;
    brief: string;
    image: string;
    text: string;
    title: string;
}
