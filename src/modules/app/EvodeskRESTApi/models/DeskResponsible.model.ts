export type DeskCardResponsibleId = number;
export type DeskCardResponsibleAvatar = string | null;

export interface DeskCardResponsibleModel {
    id: DeskCardResponsibleId;
    active?: boolean;
    avatar: DeskCardResponsibleAvatar;
    name?: string;
    email: string;
}
