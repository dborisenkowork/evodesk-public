export interface DeskInviteModel
{
    id: number;
    name: string;
    owner: boolean;
    inviting: string;
}
