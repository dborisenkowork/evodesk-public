import {Injectable} from '@angular/core';

import {Observable, Subject, concat, forkJoin, of} from 'rxjs';
import {map, takeUntil, tap} from 'rxjs/operators';

import * as R from 'ramda';

import {AddSkillsRequest, CompetenceRESTService, RemoveSkillsRequest, SyncUserCompetencesRequestEntry} from '../services/CompetenceREST.service';

import {ProfileId} from '../models/profile/Profile.model';
import {CompetenceId} from '../models/Competence.model';
import {UserCompetenceModelId} from '../models/UserCompetence.model';
import {UserCompetenceDocumentId, UserCompetenceDocumentModel} from '../models/UserCompetenceDocument.model';

export interface SyncUserCompetencesRequest {
    competences: Array<SyncUserCompetencesRequestEntry>;
}

export interface SyncUserCompetenceResponse {
    uploadedDocuments: Array<UserCompetenceDocumentModel>;
    deletedDocuments: Array<UserCompetenceDocumentId>;
}

@Injectable()
export class SyncCompetenceScript
{
    constructor(
        private competenceRESTService: CompetenceRESTService,
    ) {}

    run(profileId: ProfileId, request: SyncUserCompetencesRequest): Observable<SyncUserCompetenceResponse> {
        const response: SyncUserCompetenceResponse = {
            uploadedDocuments: [],
            deletedDocuments: [],
        };

        return Observable.create((done) => {
            concat(
                this.setCompetences(profileId, request),
                this.setCompetenceSkills(profileId, request),
                this.uploadDocuments(profileId, request).pipe(tap((uResponse) => {
                    response.uploadedDocuments = uResponse;
                })),
                this.deleteDocuments(profileId, request).pipe(tap((uResponse) => {
                    response.deletedDocuments = uResponse;
                })),
            ).subscribe(
                undefined,
                (httpError) => {
                    done.error(httpError);
                },
                () => {
                    done.next(response);
                    done.complete();
                },
            );
        });
    }

    private setCompetences(profileId: ProfileId, request: SyncUserCompetencesRequest): Observable<any> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((httpResponse) => {
            this.competenceRESTService.getCompetenceOfProfile(profileId).pipe(takeUntil(unsubscribe$), map(sc => sc.filter(s => s.active.toString() === '1'))).subscribe((serverCompetences) => {
                const toCreateCompetences: Array<CompetenceId> = R.differenceWith((a, b) => a === b, request.competences.map(c => c.competenceId), serverCompetences.map(c => c.competence.id));
                const toDeleteCompetences: Array<CompetenceId> = R.differenceWith((a, b) => a === b, serverCompetences.map(c => c.competence.id), request.competences.map(c => c.competenceId));

                const toCreateCompetencesQueries: Array<Observable<any>> = [];
                const toDeleteCompetencesQueries: Array<Observable<any>> = [];

                toCreateCompetences.forEach(cId => toCreateCompetencesQueries.push(this.competenceRESTService.addCompetenceToProfile(cId)));
                toDeleteCompetences.forEach(cId => toDeleteCompetencesQueries.push(this.competenceRESTService.removeCompetenceFromProfile(cId)));

                if (toCreateCompetencesQueries.length || toDeleteCompetencesQueries.length) {
                    concat(
                        forkJoin(...toDeleteCompetencesQueries, of(undefined)),
                        forkJoin(...toCreateCompetencesQueries, of(undefined)),
                    ).subscribe(
                        undefined,
                        (httpError) => {
                            httpResponse.error(httpError);
                        },
                        () => {
                            httpResponse.next(undefined);
                            httpResponse.complete();
                        },
                    );
                } else {
                    httpResponse.next(undefined);
                    httpResponse.complete();
                }
            });

            return (() => {
                unsubscribe$.next(undefined);
            });
        });
    }

    private setCompetenceSkills(profileId: ProfileId, request: SyncUserCompetencesRequest): Observable<any> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((q) => {
            this.competenceRESTService.getCompetenceOfProfile(profileId).pipe(takeUntil(unsubscribe$), map(sc => sc.filter(s => s.active.toString() === '1'))).subscribe((serverCompetences) => {
                if (serverCompetences.length) {
                    const mapCompetenceToSkills: { [competenceId: number]: Array<string> } = {};

                    forkJoin(
                        serverCompetences.map(sC => Observable.create((w) => {
                            this.competenceRESTService.getSkillsOfCurrentProfile(sC.id).pipe(takeUntil(unsubscribe$)).subscribe(
                                (wResponse) => {
                                    mapCompetenceToSkills[sC.competence.id] = wResponse.map(ww => ww.skill);

                                    w.next(undefined);
                                    w.complete();
                                },
                                (wError) => {
                                    w.error(wError);
                                },
                            );
                        })),
                    ).subscribe(() => {
                        const doApplySkills: Function = (() => {
                            const createRequests: Array<AddSkillsRequest> = [];
                            const deleteRequests: Array<RemoveSkillsRequest> = [];

                            serverCompetences.forEach((sC) => {
                                const associatedRequest: SyncUserCompetencesRequestEntry = request.competences.filter(r => r.competenceId === sC.competence.id)[0];

                                const wasSkills: Array<string> = mapCompetenceToSkills[sC.competence.id];
                                const setSkills: Array<string> = (associatedRequest ? associatedRequest.skills : []);

                                deleteRequests.push({
                                    competenceId: sC.competence.id,
                                    skills: wasSkills.filter(s => !~setSkills.map(ss => ss.toLocaleLowerCase()).indexOf(s.toLocaleLowerCase())),
                                });

                                createRequests.push({
                                    competenceId: sC.competence.id,
                                    skills: setSkills.filter(s => !~wasSkills.map(ss => ss.toLocaleLowerCase()).indexOf(s.toLocaleLowerCase())),
                                });
                            });

                            concat(
                                this.competenceRESTService.removeSkills(deleteRequests.filter(r => r.skills.length > 0)),
                                this.competenceRESTService.addSkills(createRequests.filter(r => r.skills.length > 0)),
                            ).subscribe(
                                undefined,
                                (httpError) => {
                                    q.error(httpError);
                                },
                                () => {
                                    q.next(undefined);
                                    q.complete();
                                },
                            );
                        });

                        doApplySkills();
                    }, (httpError) => q.error(httpError));
                } else {
                    q.next(undefined);
                    q.complete();
                }
            });

            return (() => {
                unsubscribe$.next(undefined);
            });
        });
    }

    private uploadDocuments(profileId: ProfileId, request: SyncUserCompetencesRequest): Observable<Array<UserCompetenceDocumentModel>> {
        const unsubscribe$: Subject<void> = new Subject<void>();
        const uploadedDocuments: Array<UserCompetenceDocumentModel> = [];

        return Observable.create((q) => {
            this.competenceRESTService.getCompetenceOfProfile(profileId).pipe(takeUntil(unsubscribe$), map(sc => sc.filter(s => s.active.toString() === '1'))).subscribe((serverCompetences) => {
                if (serverCompetences.length) {
                    const uploadQueries: Array<{ userCompetenceId: UserCompetenceModelId; file: Blob; title: string; }> = [];

                    serverCompetences.forEach(sC => {
                        const associatedWithRequest: SyncUserCompetencesRequestEntry = request.competences.filter(r => r.competenceId === sC.competence.id)[0];

                        if (associatedWithRequest && associatedWithRequest.uploadDocuments) {
                            associatedWithRequest.uploadDocuments.forEach((u) => {
                                uploadQueries.push({
                                    userCompetenceId: sC.id,
                                    title: u.title,
                                    file: u.file,
                                });
                            });
                        }
                    });

                    if (uploadQueries.length) {
                        forkJoin(
                            uploadQueries.map(u => this.competenceRESTService.uploadDocumentForUserCompetence(u.userCompetenceId, u.file, u.title).pipe(tap(uResponse => {
                                uploadedDocuments.push(uResponse);
                            }))),
                        ).subscribe(
                            () => {
                                q.next(uploadedDocuments);
                                q.complete();
                            },
                            (httpError) => {
                                q.error(httpError);
                            },
                        );
                    } else {
                        q.next([]);
                        q.complete();
                    }
                } else {
                    q.next([]);
                    q.complete();
                }
            });

            return (() => {
                unsubscribe$.next(undefined);
            });
        });
    }

    private deleteDocuments(profileId: ProfileId, request: SyncUserCompetencesRequest): Observable<Array<UserCompetenceDocumentId>> {
        const unsubscribe$: Subject<void> = new Subject<void>();
        const deletedDocuments: Array<UserCompetenceDocumentId> = [];

        return Observable.create((q) => {
            this.competenceRESTService.getCompetenceOfProfile(profileId).pipe(takeUntil(unsubscribe$), map(sc => sc.filter(s => s.active.toString() === '1'))).subscribe((serverCompetences) => {
                if (serverCompetences.length) {
                    const mapCompetenceToDocuments: { [competenceId: number]: Array<UserCompetenceDocumentModel> } = {};

                    forkJoin(
                        serverCompetences.map(sC => Observable.create((w) => {
                            this.competenceRESTService.getDocumentsOfCurrentProfile(sC.id).pipe(takeUntil(unsubscribe$)).subscribe(
                                (wResponse) => {
                                    mapCompetenceToDocuments[sC.competence.id] = wResponse;

                                    w.next(undefined);
                                    w.complete();
                                },
                                (wError) => {
                                    w.error(wError);
                                },
                            );
                        })),
                    ).subscribe(() => {
                        const deleteQueries: Array<UserCompetenceDocumentId> = [];

                        serverCompetences.forEach(sC => {
                            const currentDocuments: Array<UserCompetenceDocumentId> = request.competences.filter(r => r.competenceId === sC.competence.id)[0].documents.map(d => d.id);
                            const existingDocuments: Array<UserCompetenceDocumentId> = mapCompetenceToDocuments[sC.competence.id].map(d => d.id);

                            existingDocuments.forEach(eId => {
                                if (!~currentDocuments.indexOf(eId)) {
                                    deleteQueries.push(eId);
                                }
                            });
                        });

                        if (deleteQueries.length) {
                            forkJoin(
                                deleteQueries.map(documentId => this.competenceRESTService.deleteDocumentOfUserCompetence(documentId).pipe(tap(uResponse => {
                                    deletedDocuments.push(documentId);
                                }))),
                            ).subscribe(
                                () => {
                                    q.next(deletedDocuments);
                                    q.complete();
                                },
                                (httpError) => {
                                    q.error(httpError);
                                },
                            );
                        } else {
                            q.next([]);
                            q.complete();
                        }
                    }, (httpError) => q.error(httpError));
                } else {
                    q.next([]);
                    q.complete();
                }
            });

            return (() => {
                unsubscribe$.next(undefined);
            });
        });
    }
}
