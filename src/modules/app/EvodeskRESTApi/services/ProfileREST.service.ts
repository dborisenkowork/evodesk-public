import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {of as observableOf, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {UserId} from '../models/user/User.model';
import {ProfileModel} from '../models/profile/Profile.model';
import {AchievementModel} from '../models/achievements/Achievement.model';
import {ProfileLegalStatusModel} from '../models/profile/ProfileLegalStatus.model';

export type IsFollowed = boolean;

export interface GetRelationsResponse200Relation {
    id: number;
    email: string;
    name: string | null;
    position: string | null;
}

export interface GetRelationsResponse200 {
    followers: Array<GetRelationsResponse200Relation>;
    following: Array<GetRelationsResponse200Relation>;
}

export interface ChangePasswordRequest {
    old: string;
    new: string;
    repeat: string;
}

export type ResolveResponse200 = boolean | number;

function normalizeRelation(input: GetRelationsResponse200Relation): GetRelationsResponse200Relation {
    return {
        ...input,
        id: parseInt(<any>input.id, 10),
    };
}

@Injectable()
export class ProfileRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    resolve(alias: string): Observable<ResolveResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/alias/${alias}`).pipe(map((result) => {
                if (typeof result === 'object') {
                    return false;
                } else if (parseInt(result, 10) > 0) {
                    return parseInt(result, 10);
                } else {
                    return false;
                }
            })),
        );
    }

    getProfile(id: number): Observable<ProfileModel> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/profile/${id}`),
        );
    }

    changePassword(profileId: number, request: ChangePasswordRequest): Observable<void> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/profile/${profileId}/password`, request),
        );
    }

    uploadAvatar(profileId: number, file: Blob): Observable<void> {
        const formData: FormData = new FormData();

        formData.append('file', file);

        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/profile/${profileId}/avatar`, formData),
        );
    }

    getProfileAchievements(profileId: number): Observable<Array<AchievementModel>> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/profile/${profileId}/getAchievements`),
        );
    }

    saveProfile(profileId: number, newProfile: ProfileModel): Observable<ProfileModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/profile/${profileId}`, newProfile),
        );
    }

    markProfileWithTutorialCompletedFlag(userId: UserId): Observable<void> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/profile/${userId}/completetutorial`, {
                step: 8,
            }),
        );
    }

    markProfileWithTutorialCanceledFlag(userId: UserId) {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/profile/${userId}/completetutorial`, {
                step: 13,
            }),
        );
    }

    getAvailableProfileLegalStatus(): Observable<Array<ProfileLegalStatusModel>> {
        return observableOf([
            {
                id: 1,
                title: 'Физическое лицо',
            },
            {
                id: 2,
                title: 'Индивидуальный предприниматель',
            },
            {
                id: 3,
                title: 'Общество с ограниченной ответственностью',
            },
        ]);
    }

    updateProfile(user): Observable<any> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/profile/${user.id}`, user),
        );
    }

    getExperienceList(): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/experience`),
        );
    }

    addExperience(experience: any): Observable<any> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/experience`, experience),
        );
    }

    getExperience(id: number): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/experience/${id}`),
        );
    }

    saveExperience(experience: any): Observable<any> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/experience/${experience.id}`, experience),
        );
    }

    clearExperience(): Observable<any> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/experience`),
        );
    }

    getRelations(profileId: number): Observable<GetRelationsResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/relations/${profileId}`).pipe(map((httpResponse: GetRelationsResponse200) => {
                return <GetRelationsResponse200>{
                    following: httpResponse.following.map(e => {
                        return normalizeRelation(e);
                    }),
                    followers: httpResponse.followers.map(e => {
                        return normalizeRelation(e);
                    }),
                };
            })),
        );
    }

    getFollowers(profileId: number): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/followers/${profileId}`),
        );
    }

    getFollowing(profileId: number): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/following/${profileId}`),
        );
    }

    followOn(profileId: number): Observable<IsFollowed> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/follow/${profileId}`, null),
        );
    }

    followOff(id: number): Observable<IsFollowed> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/unfollow/${id}`, null),
        );
    }


    followerOn(profileId: number): Observable<IsFollowed> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/follow/${profileId}`, null),
        );
    }

    followerOff(id: number): Observable<IsFollowed> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/unfollow/${id}`, null),
        );
    }

    getAlbums(): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/album`),
        );
    }

    createAlbum(album: any): Observable<any> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/album`, album),
        );
    }

    clearAlbums(): Observable<any> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/album`),
        );
    }

    getAlbum(id: number): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/album/${id}`),
        );
    }

    saveAlbum(album: any): Observable<any> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/album/${album.id}`, album),
        );
    }

    deleteAlbum(id: number): Observable<any> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/album/${id}`),
        );
    }

    addToAlbum(data: any, id: number): Observable<any> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/album/${id}/add`, data),
        );
    }
}
