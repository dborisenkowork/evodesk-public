import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {IndustryModel} from '../models/industry/Industry.model';

@Injectable()
export class IndustryRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    getIndustries(): Observable<Array<IndustryModel>> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/getIndustry`),
        );
    }
}
