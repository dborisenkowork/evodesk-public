import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {UserId} from '../models/user/User.model';
import {AttachmentId} from '../models/post/Post.model';
import {PostId, PostModel, PostType} from '../models/post/Post.model';
import {ProfileId} from '../models/profile/Profile.model';

export interface CreateNewPostRequest {
    text: string;
    type: PostType;
    pinned: boolean;
    attachments: Array<AttachmentId>;
}

export interface UpdatePostRequest {
    text: string;
    type: PostType;
    pinned: boolean;
    attachments: Array<AttachmentId>;
}

export interface GetPostsResponse200 {
    posts: Array<PostModel>;
}

export interface GetPostsOfUsersOptions {
    from?: number;
    limit?: number;
}

export interface RepostRequest {
    profileId: ProfileId;
    postId: PostId;
    text: string;
}

export interface LikedByRequest {
    before?: ProfileId;
    after?: ProfileId;
    num?: number;
}

export interface RepostedByRequest {
    before?: ProfileId;
    after?: ProfileId;
    num?: number;
}

export type LikedByResponse200 = Array<{
    avatar: string;
    email: string;
    id: ProfileId;
    like_id: number;
    name: string;
    is_followed: boolean;
}>;

export type RepostedByResponse200 = Array<{
    avatar: string;
    email: string;
    id: ProfileId;
    repost_id: number;
    name: string;
    is_followed: boolean;
}>;

@Injectable()
export class PostRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    createNewPost(request: CreateNewPostRequest): Observable<PostModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/post`, {
                ...request,
                pinned: request.pinned ? '1' : '0',
            }),
        );
    }

    updatePost(postId: PostId, request: UpdatePostRequest): Observable<PostModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/post/${postId}`, {
                ...request,
                pinned: request.pinned ? '1' : '0',
            }),
        );
    }

    repost(request: RepostRequest): Observable<PostModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/feed/${request.profileId}/post/${request.postId}/repost`, {
                text: request.text,
            }),
        );
    }

    deletePost(postId: PostId): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/post/${postId}`),
        );
    }

    getPost(postId: PostId): Observable<PostModel> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/post/${postId}`),
        );
    }

    getPosts(): Observable<GetPostsResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/post`),
        );
    }

    getPostsOfUser(userId: UserId, options: GetPostsOfUsersOptions = {}): Observable<GetPostsResponse200> {
        const qp: any = {};

        if (options.from || options.limit) {
            qp.offset = options.from;
            qp.limit = options.limit;
        }

        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/feed/${userId}`, {
                params: qp,
            }),
        );
    }

    likePost(postId: PostId): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/like/post/${postId}`),
        );
    }

    savePost(post: PostModel): Observable<PostModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/post/${post.id}`, post),
        );
    }

    unlikePost(postId: PostId): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/unlike/post/${postId}`),
        );
    }

    likeRepost(postId: PostId): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/like/repost/${postId}`),
        );
    }

    dislikeRepost(postId: PostId): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/unlike/repost/${postId}`),
        );
    }

    likedBy(postId: PostId, request: LikedByRequest = {}): Observable<LikedByResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/feed/post/${postId}/likesusers`, {
                params: <any>request,
            }),
        );
    }

    repostedBy(postId: PostId, request: RepostedByRequest = {}): Observable<RepostedByResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/feed/post/${postId}/repostsusers`, {
                params: <any>request,
            }),
        );
    }
}
