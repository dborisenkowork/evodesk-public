import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {IMRESTChatMessage, IMRESTContact, IMRESTDialogId, IMRESTMessage, IMRESTMessageId, IMRESTSendMessageResponse} from '../models/im/IM.model';
import {ProfileId} from '../models/profile/Profile.model';

export interface IMRESTStartDialogRequest {
    from: ProfileId;
    recipientIds: Array<ProfileId>;
    message: string;
}

export interface IMRESTSendMesssageRequest {
    from: ProfileId;
    recipientIds: Array<ProfileId>;
    message: string;
}

export type IMRESTGetDialogMessagesOptions =
    undefined
    | { num: number }
    | { num: number; beforeId: IMRESTMessageId }
;

@Injectable()
export class InstantMessagesRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    startDialog(request: IMRESTStartDialogRequest): Observable<IMRESTMessage> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/user/message`, {
                message:  request.message,
                user_from: request.from,
                user_to: request.recipientIds.length === 1 ? request.recipientIds[0] : request.recipientIds,
            }),
        );
    }

    getDialogs(): Observable<Array<IMRESTContact>> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/message/getmessages`),
        );
    }

    sendMessage(request: IMRESTSendMesssageRequest): Observable<IMRESTSendMessageResponse> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/user/message`, {
                message:  request.message,
                user_from: request.from,
                user_to: request.recipientIds.length === 1 ? request.recipientIds[0] : request.recipientIds,
            }),
        );
    }

    sendMessageWithoutPublishReplay(request: IMRESTSendMesssageRequest): Observable<IMRESTMessage> {
        return this.http.post(`${this.moduleConfig.apiEndpoint}/user/message`, {
            message:  request.message,
            user_from: request.from,
            user_to: request.recipientIds.length === 1 ? request.recipientIds[0] : request.recipientIds,
        }) as Observable<IMRESTMessage>;
    }

    getMessagesOfDialog(dialogId: IMRESTDialogId, options?: IMRESTGetDialogMessagesOptions): Observable<Array<IMRESTChatMessage>> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/message/getmessageschat/${dialogId}`, {
                params: <any>options,
            }),
        );
    }

    dropDialog(dialogId: IMRESTDialogId): Observable<void> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/message/removechat/${dialogId}`),
        );
    }

    blockUser(profileId: ProfileId): Observable<void> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/message/addBlockUser/${profileId}`),
        );
    }

    markDialogAsRead(dialogId: IMRESTDialogId): Observable<void> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/message/setmessagesread/${dialogId}`),
        );
    }

    getNumUnreadMessages(): Observable<number> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/message/getcountnotreadmessage`).pipe(map(r => parseInt(r[0].messages_count, 10))),
        );
    }
}
