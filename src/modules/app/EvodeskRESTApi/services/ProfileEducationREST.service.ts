import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {ProfileId} from '../models/profile/Profile.model';
import {ProfileEducationId, ProfileEducationModel} from '../models/profile/ProfileEducation.model';

export interface CreateEducationRequest {
    type: string;
    city: string;
    year: string;
    name: string;
    specialty: string;
}

export interface UpdateEducationRequest {
    type: string;
    city: string;
    year: string;
    name: string;
    specialty: string;
}

@Injectable()
export class ProfileEducationRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    getEducation(profileId: ProfileId): Observable<Array<ProfileEducationModel>> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/education`, {
                params: {
                    profile: profileId.toString(),
                    offset: '0',
                    limit: '100',
                },
            }),
        );
    }

    createEducation(profileId: ProfileId, request: CreateEducationRequest): Observable<ProfileEducationModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/education`, request),
        );
    }

    updateEducation(profileId: ProfileId, educationId: ProfileEducationId, request: UpdateEducationRequest): Observable<ProfileEducationModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/education/${educationId}`, request),
        );
    }

    deleteEducation(profileId: ProfileId, educationId: ProfileEducationId): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/education/${educationId}`),
        );
    }
}
