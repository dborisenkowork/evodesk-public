import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable, of} from 'rxjs';

import {map} from 'rxjs/operators';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {ProfileId} from '../models/profile/Profile.model';
import {CompetenceIndexesModel} from '../models/CompetenceIndexes.model';
import {CompetenceCategoryModel} from '../models/CompetenceCategory.model';
import {UserCompetenceModel, UserCompetenceModelId} from '../models/UserCompetence.model';
import {CompetenceId, CompetenceModel} from '../models/Competence.model';
import {UserCompetenceDocumentId, UserCompetenceDocumentModel} from '../models/UserCompetenceDocument.model';
import {UserCompetenceSkillModel} from '../models/UserCompetenceSkill.model';
import {CompetenceMarkModel} from '../models/CompetenceMark.model';

export interface SyncUserCompetencesRequestEntry {
    competenceId: CompetenceId;
    skills: Array<string>;
    documents: Array<UserCompetenceDocumentModel>;
    uploadDocuments: Array<{ title: string; file: Blob }>;
}

export interface AddSkillsRequest {
    competenceId: CompetenceId;
    skills: Array<string>;
}

export interface RemoveSkillsRequest {
    competenceId: CompetenceId;
    skills: Array<string>;
}

function int(input: any): any {
    return parseInt(input, 10);
}

function normalizeCompetenceModel(input: CompetenceModel): CompetenceModel {
    return {
        ...input,
        id: int(input.id),
    };
}

function normalizeUserCompetenceModel(input: UserCompetenceModel): UserCompetenceModel {
    return {
        ...input,
        id: int(input.id),
        user_id: int(input.user_id),
        competence: normalizeCompetenceModel(input.competence),
        active: int(input.active),
    };
}

function normalizeUserCompetenceSkillModel(input: UserCompetenceSkillModel): UserCompetenceSkillModel {
    return {
        ...input,
        id: int(input.id),
        competence_id: typeof input.competence_id === 'object' ? int((<UserCompetenceModel>input.competence_id).id) : int(input.competence_id),
        user_id: int(input.user_id),
    };
}

function normalizeUserCompetenceDocumentModel(input: UserCompetenceDocumentModel): UserCompetenceDocumentModel {
    return {
        ...input,
        id: int(input.id),
        user_id: int(input.user_id),
        status: int(input.status),
        competence_id: {
            ...input.competence_id,
            id: int(input.competence_id.id),
        },
    };
}

function normalizeUserCompetenceMarkModel(input: CompetenceMarkModel): CompetenceMarkModel {
    return {
        ...input,
        id: int(input.id),
        user_id: int(input.user_id),
        competence_id: int(input.competence_id),
        marker_id: int(input.marker_id),
        status: int(input.status),
    };
}

@Injectable()
export class CompetenceRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    indexes(profileId: ProfileId): Observable<CompetenceIndexesModel> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/social/indexes/${profileId}`),
        );
    }

    getCompetenceCategories(): Observable<Array<CompetenceCategoryModel>> {
        return httpPublishReplay(
            this.http.get<{ competence_categories: Array<CompetenceCategoryModel> }>(`${this.moduleConfig.apiEndpoint}/competencecategory`).pipe(map(httpResponse => (httpResponse).competence_categories)),
        );
    }

    getCompetenceOfProfile(profileId: ProfileId): Observable<Array<UserCompetenceModel>> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/competence/user/${profileId}`).pipe(map(httpResponse => (<Array<UserCompetenceModel>>httpResponse).map(e => normalizeUserCompetenceModel(e)))),
        );
    }

    addCompetenceToProfile(competenceId: CompetenceId): Observable<UserCompetenceModel> {
        return httpPublishReplay(
            this.http.post<UserCompetenceModel>(`${this.moduleConfig.apiEndpoint}/competence/${competenceId}/add`, {}).pipe(map(httpResponse => normalizeUserCompetenceModel(httpResponse))),
        );
    }

    removeCompetenceFromProfile(competenceId: CompetenceId): Observable<void> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/competence/${competenceId}/remove`, {}),
        );
    }

    addSkills(requests: Array<AddSkillsRequest>): Observable<void> {
        if (requests.length === 0) {
            return of(undefined);
        } else {
            const mapRS: { [competenceId: number]: Array<string> } = {};

            requests.forEach(r => {
                mapRS[r.competenceId] = r.skills;
            });

            return httpPublishReplay(
                this.http.post(`${this.moduleConfig.apiEndpoint}/competenceaddskills`, { skills: mapRS }),
            );
        }
    }

    removeSkills(requests: Array<RemoveSkillsRequest>): Observable<void> {
        if (requests.length === 0) {
            return of(undefined);
        } else {
            const mapRS: { [competenceId: number]: Array<string> } = {};

            requests.forEach(r => {
                mapRS[r.competenceId] = r.skills;
            });

            return httpPublishReplay(
                this.http.post(`${this.moduleConfig.apiEndpoint}/competenceremoveskills`, { skills: mapRS }),
            );
        }
    }

    getSkillsOfCurrentProfile(userCompetenceId: UserCompetenceModelId): Observable<Array<UserCompetenceSkillModel>> {
        return httpPublishReplay(
            this.http.get<Array<UserCompetenceSkillModel>>(`${this.moduleConfig.apiEndpoint}/competenceuser/${userCompetenceId}/skills`).pipe(map(s => s.map(e => normalizeUserCompetenceSkillModel(e)))),
        );
    }

    getDocumentsOfCurrentProfile(userCompetenceId: UserCompetenceModelId): Observable<Array<UserCompetenceDocumentModel>> {
        return httpPublishReplay(
            this.http.get<Array<UserCompetenceDocumentModel>>(`${this.moduleConfig.apiEndpoint}/competenceuser/${userCompetenceId}/documents`).pipe(map(h => h.map(hh => normalizeUserCompetenceDocumentModel(hh)))),
        );
    }

    uploadDocumentForUserCompetence(userCompetenceId: UserCompetenceModelId, file: Blob, title: string): Observable<UserCompetenceDocumentModel> {
        const fd: FormData = new FormData();

        fd.append('title', title);
        fd.append('file', file);

        return httpPublishReplay(
            this.http.post<UserCompetenceDocumentModel>(`${this.moduleConfig.apiEndpoint}/competenceuser/${userCompetenceId}/document`, fd).pipe(map(h => normalizeUserCompetenceDocumentModel(h))),
        );
    }

    deleteDocumentOfUserCompetence(documentId: UserCompetenceDocumentId): Observable<void> {
        return httpPublishReplay(
            this.http.post<UserCompetenceDocumentModel>(`${this.moduleConfig.apiEndpoint}/competencedocument/${documentId}/delete`, {}),
        );
    }

    plusMarkToUserCompetence(profileId: ProfileId, competenceId: CompetenceId, comment: string): Observable<CompetenceMarkModel> {
        return httpPublishReplay(
            this.http.post<CompetenceMarkModel>(`${this.moduleConfig.apiEndpoint}/competence/${competenceId}/user/${profileId}/mark`, {
                mark: 1,
                comment: comment,
            }).pipe(map(h => normalizeUserCompetenceMarkModel(h))),
        );
    }

    minusMarkToUserCompetence(profileId: ProfileId, competenceId: CompetenceId, comment: string): Observable<CompetenceMarkModel> {
        return httpPublishReplay(
            this.http.post<CompetenceMarkModel>(`${this.moduleConfig.apiEndpoint}/competence/${competenceId}/user/${profileId}/mark`, {
                mark: 0,
                comment: comment,
            }).pipe(map(h => normalizeUserCompetenceMarkModel(h))),
        );
    }

    getDocumentUrl(documentId: UserCompetenceDocumentId): string {
        return `${this.moduleConfig.apiEndpoint}/competencedocument/${documentId}/download`;
    }

    getMarksOfUserCompetence(userCompetenceId: UserCompetenceModelId, options: { limit: number, offset: number }): Observable<Array<CompetenceMarkModel>> {
        return httpPublishReplay(
            this.http.get<Array<CompetenceMarkModel>>(`${this.moduleConfig.apiEndpoint}/competenceuser/${userCompetenceId}/marks`, {
                params: <any>options,
            }).pipe(map(h => h.map(hh => normalizeUserCompetenceMarkModel(hh)))),
        );
    }
}
