import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {RatingEntryModel} from '../models/rating/RatingEntry.model';
import {AchievementModel} from '../models/achievements/Achievement.model';

export interface RatingGetHistoryRequest {
    profileId: number;
}

export type RatingGetHistoryResponse200 = Array<RatingEntryModel>;
export type AchievmentResponse = Array<AchievementModel>;

@Injectable()
export class RatingRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    getRatingHistory(request: RatingGetHistoryRequest): Observable<RatingGetHistoryResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/profile/${request.profileId}/getRatingHistory`),
        );
    }

    getAchievements(profileId: number): Observable<AchievmentResponse> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/profile/${profileId}/getAchievements`),
        );
    }
}
