import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {PartnerModel} from '../models/Partner.model';

export interface GetDataForPartnersResponse200 {
    code: string;
    top: Array<PartnerModel>;
    statistic: {
        totalTodayUsers: number;
        totalTodayPoints: number;
        totalAllUsers: number;
        totalAllPoints: number;
    };
}

@Injectable()
export class PartnersRESTService {
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    getDataForPartners(profileId: number): Observable<GetDataForPartnersResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/user/${profileId}/getDataForPartners`),
        );
    }
}
