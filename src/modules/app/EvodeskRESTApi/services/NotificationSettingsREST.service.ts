import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {NotificationSettingsModel, NotificationSettingsTypeId} from '../models/NotificationSettings.model';

export interface SetNotificationEntry {
    notificationTypeId: NotificationSettingsTypeId;
    emailNotify: boolean;
    mobileNotify: boolean;
    webNotify: boolean;
}

@Injectable()
export class NotificationSettingsRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    setSettings(settings: Array<SetNotificationEntry>): Observable<void> {
        return httpPublishReplay(
            this.http.put<void>(`${this.moduleConfig.apiEndpoint}/notification/settings`, {
                settings: settings,
            }),
        );
    }

    getSettings(): Observable<Array<NotificationSettingsModel>> {
        return httpPublishReplay(
            this.http.get<Array<NotificationSettingsModel>>(`${this.moduleConfig.apiEndpoint}/notification/settings`).pipe(map(h => (<any>h).settings)),
        );
    }
}
