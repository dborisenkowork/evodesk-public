import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';


import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {NewsEntry} from '../models/News.model';

export type NewsResponse200 = Array<NewsEntry>;

export type NewsRequestOptions =
      undefined
    | { limit: number }
    | { limit: number; offset: number; }
    | { limit: number; afterId: number; }
    | { limit: number; beforeId: number; }
    | { limit: number; afterId: number; beforeId: number; }
;

@Injectable()
export class NewsRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    newsIndex(options: NewsRequestOptions): Observable<NewsResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/allNews`, {
                params: <any>options,
            }),
        );
    }

    newsFiltered(options: NewsRequestOptions): Observable<NewsResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/news`, {
                params: <any>options,
            }),
        );
    }

    newsNotifications(options: NewsRequestOptions): Observable<NewsResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/notifications`, {
                params: <any>options,
            }),
        );
    }

    newsSubscribers(options: NewsRequestOptions): Observable<NewsResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/followingNews`, {
                params: <any>options,
            }),
        );
    }

    newsWorld(options: NewsRequestOptions): Observable<NewsResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/news`, {
                params: <any>options,
            }),
        );
    }

    newsAdverts(options: NewsRequestOptions): Observable<NewsResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/followingAdverts`, {
                params: <any>options,
            }),
        );
    }
}
