import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {PostId} from '../models/post/Post.model';
import {PostCommentId, PostCommentModel} from '../models/post/PostComment.model';
import {ProfileId} from '../models/profile/Profile.model';

export interface PostCommentCreateRequest {
    text: string;
}

export interface PostCommentEditRequest {
    text: string;
}

export interface PostPath {
    profileId: ProfileId;
    postId: PostId;
}

export interface PostCommentPath {
    profileId: ProfileId;
    postId: PostId;
    commentId: PostCommentId;
}

export interface GetPostCommentsOptions {
    num?: number;
    before?: number;
    after?: number;
}

@Injectable()
export class PostCommentRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    createNewPostComment(path: PostPath, request: PostCommentCreateRequest): Observable<PostCommentModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/feed/${path.profileId}/post/${path.postId}/comment`, request),
        );
    }

    deletePostComment(path: PostCommentPath): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/feed/${path.profileId}/post/${path.postId}/comment/${path.commentId}`),
        );
    }

    editPostComment(path: PostCommentPath, request: PostCommentEditRequest): Observable<PostCommentModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/feed/${path.profileId}/post/${path.postId}/comment/${path.commentId}`, request),
        );
    }

    getPostComments(path: PostPath, options: GetPostCommentsOptions = {}): Observable<Array<PostCommentModel>> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/feed/${path.profileId}/post/${path.postId}/comment`, {
                params: <any>options,
            }),
        );
    }

    likePostComment(postCommentId: PostCommentId): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/like/comment/${postCommentId}`),
        );
    }

    dislikePostComment(postCommentId: PostCommentId): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/unlike/comment/${postCommentId}`),
        );
    }
}
