import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {Album} from '../models/album/Album.model';

interface AddAlbumRequest {
    name: string;
}

interface UpdateAlbumRequest {
    name: string;
    description: string;
}

interface AddPhotoRequest {
    file: Blob;
    description: string;
}

export interface AddPhotoResponse {
    album_id: number | null;
    created_at: string;
    id: number;
    path: string;
    user_id: number;
}

interface MoveListToAlbumRequest {
    album_id: number;
    photos: Array<{
        id: number,
        description: string,
    }>;
}

interface DeletePhotosRequest {
    photos: Array<{
        id: number;
    }>;
}

@Injectable()
export class AlbumsRESTService {
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    getAlbums(profileId: number): Observable<Array<Album>> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/albums/profile/${profileId}/getAlbums`),
        );
    }

    addAlbum(profileId: number, body: AddAlbumRequest): Observable<any> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/albums/profile/${profileId}/addAlbum`, body),
        );
    }

    deleteAll(profileId: number): Observable<any> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/albums/profile/${profileId}/deleteAll`),
        );
    }

    getAlbum(profileId: number, albumId: number): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/albums/profile/${profileId}/album/${albumId}/getAlbum`),
        );
    }

    updateAlbum(profileId: number, albumId: number, body: UpdateAlbumRequest): Observable<any> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/albums/album/${albumId}/updateAlbum`, body),
        );
    }

    deleteAlbum(albumId: number): Observable<any> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/albums/album/${albumId}/deleteAction`),
        );
    }

    getPhoto(profileId: number, albumId: number, photoId: number): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/albums/profile/${profileId}/album/${albumId}/photo/${photoId}`),
        );
    }

    getAllPhotos(profileId: number): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/albums/profile/${profileId}/getallphotos`),
        );
    }

    getInfo(profileId: number, albumId: number, photoId: number): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/albums/profile/${profileId}/album/${albumId}/info/${photoId}`),
        );
    }

    deletePhotosList(photoIds: Array<number>): Observable<any> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/albums/deletePhotosList`, {photos: photoIds}),
        );
    }

    addPhoto(body: AddPhotoRequest): Observable<AddPhotoResponse> {
        const formData: FormData = new FormData();

        formData.append('file', body.file);
        formData.append('description', body.description);

        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/albums/addPhoto`, formData),
        );
    }

    updatePhoto(profileId: number, albumId: number, photoId: number, body: AddPhotoRequest): Observable<any> {
        const formData: FormData = new FormData();

        formData.append('file', body.file);
        formData.append('description', body.description);

        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/albums/profile/${profileId}/album/${albumId}/updatePhoto/${photoId}`, formData),
        );
    }

    moveListToAlbum(body: MoveListToAlbumRequest) {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/albums/moveListToAlbum`, body),
        );
    }

    deletePhotos(body: DeletePhotosRequest) {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/albums/deletePhotosList`, body),
        );
    }
}
