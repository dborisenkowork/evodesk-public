import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {DeskMemberId} from '../models/DeskMember.model';
import {DeskId, DeskModel} from '../models/Desk.model';
import {DeskInviteModel} from '../models/DeskInvite.model';
import {DeskParticipantModel} from '../models/DeskParticipant.model';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

export interface AcceptInviteToDeskResponse200 {
    status: 'ok';
    name: string;
    desk: DeskModel;
}

export interface DeclineInviteToDeskResponse200 {
    status: 'ok';
    deskId: number;
    name: string;
}

export interface DeskResponse200
{
    availableDesks: Array<DeskModel>;
    myDesks: Array<DeskModel>;
}

export interface SetColorResponse200 {
    color: string;
}

export type InvitesResponse200 = Array<DeskInviteModel>;

export interface CreateNewDeskResponse200 {
    id: number;
    title: string;
    invite_code: string;
    color: null;
    isDefault: null;
}

export interface SendInviteResponseSuccess200 {
    status: 'ok';
    user: DeskParticipantModel;
}

export interface SendInviteResponseError200 {
    status: 'error';
}

export interface InviteToEvodeskResponse200 {
    status: 'ok';
    user: DeskParticipantModel;
}

export type SendInviteResponse200 = SendInviteResponseSuccess200 | SendInviteResponseError200;

@Injectable()
export class DeskDashboardRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    desks(): Observable<DeskResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/multidesks`),
        );
    }

    createNewDesk(name: string): Observable<CreateNewDeskResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/save`, {
                name: name,
            }),
        );
    }

    deleteDesk(id: DeskId): Observable<void> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/${id}/delete`, {}),
        );
    }

    invites(): Observable<InvitesResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/multidesks/invites`),
        );
    }

    rename(id: DeskId, newName: string): Observable<void> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/${id}/rename`, {
                name: newName,
            }),
        );
    }

    setColor(id: DeskId, newColor: string): Observable<SetColorResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/${id}/color`, {
                color: newColor,
            }),
        );
    }

    inviteToDesk(id: DeskId, email: string): Observable<SendInviteResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/${id}/member`, {
                active: true,
                email: email,
            }),
        );
    }

    inviteToEvodesk(id: DeskId, userId: number, email: string): Observable<InviteToEvodeskResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/${id}/letter`, {
                email: email,
                inviting: userId,
            }),
        );
    }

    acceptInviteByLink(id: DeskId): Observable<void> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/${id}/inviteUser`, {}),
        );
    }

    acceptInviteToDesk(id: DeskId): Observable<AcceptInviteToDeskResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/${id}/invite`, {
                action: 'success',
            }),
        );
    }

    declineInviteToDesk(id: DeskId): Observable<DeclineInviteToDeskResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/${id}/invite`, {
                action: 'deny',
            }),
        );
    }

    deleteParticipant(deskId: DeskId, memberId: DeskMemberId): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/multidesk/${deskId}/member/${memberId}`),
        );
    }
}
