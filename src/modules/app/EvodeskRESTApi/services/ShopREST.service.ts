import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {CurrentUserService} from '../../../features/EvodeskAuth/services/CurrentUser.service';

import {ShopCategoryId, ShopCategoryModel} from '../models/ShopCategory.model';
import {ShopProductId, ShopProductModel, ShopProductSlug} from '../models/ShopProduct.model';
import {ShopOrderId, ShopOrderModel} from '../models/ShopOrder.model';
import {ShopProductReviewModel} from '../models/ShopProductReview.model';
import {ShopFavoriteModel} from '../models/ShopFavorite.model';

export interface ShopRESTGetProductsQuery {
    search?: string | undefined | null;
    orderBy?: string;
    orderDirection?: string;
    category?: ShopCategoryId | undefined;
    limit?: number;
    page?: number;
}

export interface ShopRESTGetFavoritesQuery {
    orderBy?: string;
    orderDirection?: string;
    limit?: number;
    page?: number;
}

export interface ShopRESTPostReviewQuery {
    review: string;
    stars: number;
}

export interface ShopRESTGetProductsResponse {
    total: number;
    products: Array<ShopProductModel>;
}

function normalizeShopProduct(input: ShopProductModel): ShopProductModel {
    return {
        ...input,
        id: int(input.id),
        category: int(input.category),
        deleted: <any>int(input.deleted),
        orders: int(input.orders),
        reviews: int(input.reviews),
        stars: int(input.stars),
        status: <any>int(input.status),
    };
}

function normalizeShopProductReview(input: ShopProductReviewModel): ShopProductReviewModel {
    return {
        ...input,
        id: int(input.id),
        service: normalizeShopProduct(input.service),
        stars: input.stars === null ? null : int(input.stars),
    };
}

function int(input: any): number {
    return parseInt(input, 10);
}

@Injectable()
export class ShopRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
        private currentUser: CurrentUserService,
    ) {}

    getCategories(): Observable<Array<ShopCategoryModel>> {
        return httpPublishReplay(
            this.http.get<Array<ShopCategoryModel>>(`${this.moduleConfig.apiEndpoint}/shop/category`),
        );
    }

    downloadProduct(serviceId: number, fileType: string, fileName: string): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/shop/service/${serviceId}/${fileType}?name=${fileName}`),
        );
    }

    getProducts(query: ShopRESTGetProductsQuery): Observable<ShopRESTGetProductsResponse> {
        const qpData: Array<{ key: string; value: string }> = [];

        const doIf: Function = (trueCase: Function, todo: Function) => {
            if (trueCase()) {
                todo();
            }
        };

        doIf(() => !! query.search, () => qpData.push({ key: 'search', value: query.search }));
        doIf(() => !! query.orderBy, () => qpData.push({ key: 'orderBy', value: query.orderBy }));
        doIf(() => !! query.orderDirection, () => qpData.push({ key: 'orderDirection', value: query.orderDirection }));
        doIf(() => !! query.category, () => qpData.push({ key: 'category', value: query.category.toString() }));
        doIf(() => !! query.limit, () => qpData.push({ key: 'limit', value: query.limit.toString() }));
        doIf(() => !! query.page, () => qpData.push({ key: 'page', value: query.page.toString() }));

        const qp: any = {};

        qpData.forEach(k => {
            qp[k.key] = k.value;
        });

        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/shop/findservice`, {
                params: <any>qp,
            }).pipe(map((httpResponse: any) => {
                return {
                    total: int(httpResponse.total),
                    products: httpResponse.services.map(p => normalizeShopProduct(p)),
                };
            })),
        );
    }

    getProduct(productSlug: ShopProductSlug): Observable<ShopProductModel> {
        return httpPublishReplay(
            this.http.get<Array<ShopCategoryModel>>(`${this.moduleConfig.apiEndpoint}/shop/service/${productSlug}`),
        );
    }

    orderProduct(productId: ShopProductId): Observable<void> {
        return httpPublishReplay(
            this.http.post<void>(`${this.moduleConfig.apiEndpoint}/shop/order`, {
                itemId: productId,
            }),
        );
    }

    orderProductWithRubles(productId: ShopProductId): Observable<void> {
        return httpPublishReplay(
            this.http.post<void>(`${this.moduleConfig.apiEndpoint}/shop/order?buyByRub=1`, {
                itemId: productId,
            }),
        );
    }


    getOrders(): Observable<Array<ShopOrderModel>> {
        return httpPublishReplay(
            this.http.get<Array<ShopOrderModel>>(`${this.moduleConfig.apiEndpoint}/shop/order`),
        );
    }

    postReview(orderId: ShopOrderId, query: ShopRESTPostReviewQuery): Observable<ShopOrderModel> {
        return httpPublishReplay(
            this.http.post<void>(`${this.moduleConfig.apiEndpoint}/shop/addreview/${orderId}`, query),
        );
    }

    getReviews(shopProductId: ShopProductId): Observable<Array<ShopProductReviewModel>> {
        return httpPublishReplay(
            this.http.get<Array<ShopProductReviewModel>>(`${this.moduleConfig.apiEndpoint}/shop/review/service/${shopProductId}`)
                .pipe(map(httpResponse => {
                    return httpResponse.map(r => normalizeShopProductReview(r));
                })),
        );
    }

    getFavorites(query: ShopRESTGetFavoritesQuery = {}): Observable<Array<ShopFavoriteModel>> {
        const qpData: Array<{ key: string; value: string }> = [];

        const doIf: Function = (trueCase: Function, todo: Function) => {
            if (trueCase()) {
                todo();
            }
        };

        doIf(() => !! query.orderBy, () => qpData.push({ key: 'orderBy', value: query.orderBy }));
        doIf(() => !! query.orderDirection, () => qpData.push({ key: 'orderDirection', value: query.orderDirection }));
        doIf(() => !! query.limit, () => qpData.push({ key: 'limit', value: query.limit.toString() }));
        doIf(() => !! query.page, () => qpData.push({ key: 'page', value: query.page.toString() }));

        const qp: any = {};

        qpData.forEach(k => {
            qp[k.key] = k.value;
        });

        return httpPublishReplay(
            this.http.get<any>(`${this.moduleConfig.apiEndpoint}/shop/favorite`, { params: <any>qp }).pipe(map(h => {
                return h.items.map((p: ShopProductModel) => {
                    return <ShopFavoriteModel>{
                        id: p.id,
                        service: p,
                        user: this.currentUser.impersonatedAs,
                    };
                });
            })),
        );
    }

    addFavorite(productId: ShopProductId): Observable<void> {
        return httpPublishReplay(
            this.http.post<void>(`${this.moduleConfig.apiEndpoint}/shop/favorite/${productId}`, {}),
        );
    }

    deleteFavorite(productId: ShopProductId): Observable<void> {
        return httpPublishReplay(
            this.http.delete<void>(`${this.moduleConfig.apiEndpoint}/shop/favorite/${productId}`),
        );
    }
}
