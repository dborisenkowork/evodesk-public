
import {map} from 'rxjs/operators';
import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {ProfileModel} from '../models/profile/Profile.model';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

export interface SearchQuery {
    queryString: string;
    page?: number;
}

export interface SearchResults {
    profiles: Array<ProfileModel>;
}

export const FIND_USER_LIMIT: number = 100;

@Injectable()
export class SearchRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    search (query: SearchQuery): Observable<SearchResults> {
        const offset: number = query.page > 0 ? FIND_USER_LIMIT * query.page + 1 : 0;

        return httpPublishReplay(
            this.http
                .post(`${this.moduleConfig.apiEndpoint}/user/find?limit=${FIND_USER_LIMIT}&offset=${offset}`, {
                    query: query.queryString,
                }, {}).pipe(
                map(res => {
                    return {
                        profiles: res,
                    };
                })),
        );
    }
}
