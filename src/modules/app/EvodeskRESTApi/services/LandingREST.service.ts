import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

export type RegisterConferenceResponse200 = 0 | 1;

export interface RegisterConferenceRequest {
    name: string;
    email: string;
    phone: string;
    vklad: boolean;
}

export interface RegisterInsideRequest {
    name: string;
    phone: string;
    email: string;
    money: string;
}

export type RegisterInsideResponse200 = void;

@Injectable()
export class LandingRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    registerConference(request: RegisterConferenceRequest): Observable<RegisterConferenceResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/saveUserFormLanding`, request),
        );
    }

    registerInside(request: RegisterInsideRequest): Observable<RegisterInsideResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/saveUserFormLanding`, request),
        );
    }
}
