import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {ProfileId} from '../models/profile/Profile.model';
import {ProfileExperienceId, ProfileExperienceModel} from '../models/profile/ProfileExperience.model';
import {WorkCompetenceApproveId, WorkCompetenceApproveModel} from '../models/WorkCompetenceApprove.model';

export interface ProfileWorkExperienceCreateRequest {
    text: string;
    site: string;
    date_start: string;
    date_finish: string;
    place: string;
    position: string;
    sphere: number;
}

export interface ProfileWorkExperienceUpdateRequest {
    text: string;
    site: string;
    date_start: string;
    date_finish: string;
    place: string;
    position: string;
    sphere: number;
}

function normalizeEntity(e: ProfileExperienceModel): ProfileExperienceModel {
    return {
        ...e,
        id: parseInt(<any>e.id, 10),
        approves: e.approves.map(a => {
            return normalizeApproveEntity(a);
        }),
    };
}

function  normalizeApproveEntity(a: WorkCompetenceApproveModel): WorkCompetenceApproveModel {
    return {
        ...a,
        id: parseInt(<any>a.id, 10),
        user_id: parseInt(<any>a.user_id, 10),
        status: <any>parseInt(<any>a.status, 10),
    };
}

@Injectable()
export class ProfileWorkExperienceRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    getExperienceOfProfile(profileId: ProfileId): Observable<Array<ProfileExperienceModel>> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/experience`, {
                params: {
                    profile: profileId.toString(),
                    offset: '0',
                    limit: '100',
                },
            }).pipe(map((experiences: Array<ProfileExperienceModel>) => {
                return experiences.map(e => {
                    return normalizeEntity(e);
                });
            })),
        );
    }

    addExperience(profileId: ProfileId, request: ProfileWorkExperienceCreateRequest): Observable<ProfileExperienceModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/experience`, request).pipe(map((response: ProfileExperienceModel) => {
                return normalizeEntity(response);
            })),
        );
    }

    updateExperience(profileId: ProfileId, profileExperienceId: ProfileExperienceId, request: ProfileWorkExperienceUpdateRequest): Observable<ProfileExperienceModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/experience/${profileExperienceId}`, request),
        );
    }

    deleteExperience(profileId: ProfileId, profileExperienceId: ProfileExperienceId): Observable<ProfileExperienceId> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/experience/${profileExperienceId}`),
        );
    }

    sendApproveRequest(wordExperienceId: ProfileExperienceId, approverProfileId: ProfileId): Observable<WorkCompetenceApproveModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/experience/${wordExperienceId}/approve/add`, {
                user_id: approverProfileId,
            }).pipe(map((a: WorkCompetenceApproveModel) => {
                return normalizeApproveEntity(a);
            })),
        );
    }

    cancelApproveRequest(wordExperienceId: ProfileExperienceId, approveId: WorkCompetenceApproveId): Observable<WorkCompetenceApproveModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/experience/${wordExperienceId}/approve/${approveId}/remove`, {}),
        );
    }

    acceptApproveRequest(wordExperienceId: ProfileExperienceId, approveId: WorkCompetenceApproveId): Observable<void> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/experience/${wordExperienceId}/approve/${approveId}/confirm`, {}),
        );
    }

    rejectApproveRequest(wordExperienceId: ProfileExperienceId, approveId: WorkCompetenceApproveId): Observable<void> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/experience/${wordExperienceId}/approve/${approveId}/reject`, {}),
        );
    }
}
