import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {EvoCheckModel} from '../models/billing/EvoCheck.model';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

type AmountWithoutCommissions = number;

export interface EvocoinToRubExchangeRateResponse200 {
    totalRub: number;
}

export interface PayeerBuyRequest {
    userId: number;
    amount: string;
}

export interface PayeerBuyResponse200 {
    amount: string;
    m_desc: string;
    order_id: number;
    sign: string;
}

export interface BillingWithdrawRequest {
    sum: string;
    account: string;
}

export interface BillingWithdrawResponse200 {
    auth_error: '0';
    errors: false;
    historyId: number;
}

export interface BillingWithdrawResponseErrors {
    errors: Array<string>;
}

export interface BillingCreateEvoCheckRequest {
    sum: AmountWithoutCommissions;
}

export interface BillingCreateEvoCheckResponse200 {
    balance_from: number;
    balance_sum: number;
    balance_to: null;
    closed_at: null;
    code: string;
    created_at: string;
    id: number;
    secret: string;
}

export interface BillingCreateEvoCheckErrors {
    errors: Array<string>;
}

@Injectable()
export class BillingRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    payeerBuy(request: PayeerBuyRequest): Observable<PayeerBuyResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/billing/payeer/buy`, request),
        );
    }

    payeerWithdraw(request: BillingWithdrawRequest): Observable<BillingWithdrawResponse200 | BillingWithdrawResponseErrors> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/billing/payeer/withdraw`, request),
        );    }

    createEvoCheck(request: BillingCreateEvoCheckRequest): Observable<BillingCreateEvoCheckResponse200 | BillingCreateEvoCheckErrors> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/evcheck`, request),
        );
    }

    listOfEvoChecks(): Observable<Array<EvoCheckModel>> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/evchecks`),
        );
    }

    getEvocoinToRubExchangeRate(): Observable<EvocoinToRubExchangeRateResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/billing/rubprice`),
        );
    }

    applyEvoCheck(code: string, secret: string): Observable<EvoCheckModel> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/evcheck/${code}/${secret}`),
        );
    }
}
