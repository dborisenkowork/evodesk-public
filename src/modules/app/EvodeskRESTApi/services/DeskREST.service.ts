import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpProgressEvent, HttpRequest, HttpResponse} from '@angular/common/http';

import {forkJoin as observableForkJoin, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {DeskColumnDefinitionModel, DeskColumnType, getDeskColumnTypeByName} from '../models/DeskColumnDefinition.model';
import {DeskModel} from '../models/Desk.model';
import {DeskArchiveCardModel, DeskCardId, DeskCardModel} from '../models/DeskCard.model';
import {DeskLabelModel} from '../models/DeskLabel.model';
import {DeskCardAttachmentModel} from '../models/DeskCardAttachment.model';
import {DeskProjectId, DeskProjectModel} from '../models/DeskProject.model';
import {DeskMemberId} from '../models/DeskMember.model';
import {DeskProjectMemberModel} from '../models/DeskProjectMember.model';
import {DeskCardChecklistItemModel, DeskCardChecklistModel} from '../models/DeskCardChecklist.model';
import {DeskCardCommentModel} from '../models/DeskCardComment.model';
import {DeskParticipantModel} from '../models/DeskParticipant.model';
import {ProfileModel} from '../models/profile/Profile.model';

export type DeskResponse200 = DeskModel;
export type DeskColumnsResponse200 = Array<DeskColumnDefinitionModel>;
export type DeskProjectsResponse200 = Array<DeskProjectModel>;
export type DeskCardsResponse200 = Array<DeskCardModel>;
export type DeskLabelsResponse200 = Array<DeskLabelModel>;

export interface DeskAddProjectResponse200 {
    deskId: number;
    id: number;
    order: number;
    title: string;
}

export interface DeskUpdateProjectRequest {
    deskId: number;
    projectId: number;
    title: string;
    orders: number;
}

export type DeskUpdateProjectResponse200 = { 'status': 'ok' } | { 'status': 'error' };
export type DeskAddCardToProjectResponse200 = { 'status': 'ok' } | { 'status': 'error' };
export type DeskDeleteCardFromProjectResponse200 = { 'status': 'ok' } | { 'status': 'error' };

export interface DeskAddCardSubmitData
{
    deskId: number;
    deskName: string;
    title: string;
    column: number;
    order: number;
    projectId?: number;
}

export interface DeskAddProjectSubmitData
{
    deskId: number;
    deskName: string;
    orders: number;
    title: string;
    columnId: number;
    members: Array<DeskParticipantModel>;
}

export enum DeskUpdateCardChangedField
{
    Title = <any>'title',
    Description = <any>'description',
    Labels = <any>'labels',
    Column = <any>'column',
    Order = <any>'order',
    Deadline= <any>'deadline',
    Private = <any>'private',
    Daily = <any>'daily',
    Estimation = <any>'estimation',
}

export enum DeskUpdateCardCommentChangedField
{
    Text = <any>'text',
}

export interface DeskNewChecklistRequest
{
    title: string;
}

export type UpdateChecklistRequest = DeskCardChecklistModel;

export interface DeleteChecklistRequest
{
    checklistId: number;
}

export interface DeskNewChecklistItemRequest
{
    checklistId: number;
    title: string;
    completed: boolean;
}

export type DeskUpdateChecklistItemRequest = DeskCardChecklistItemModel;

export interface DeskDeleteChecklistItemRequest
{
    checklistId: number;
    checklistItemId: number;
}

export interface DeskAttachLinkToCardRequest {
    link: string;
    title: string;
}

export interface UploadFileAndAttachToCardRequest {
    file: File | Blob;
}

export type DeskAddMemberToDeskResponse200 = DeskAddMemberToDeskResponseError | DeskAddMemberToDeskResponseSuccess;

export interface DeskAddMemberToDeskResponseError {
    status: 'error';
}

export interface DeskAddMemberToDeskResponseSuccess {
    status: 'ok';
    user: {
        id: number;
        active: boolean;
        avatar: null | string;
        email: string;
    };
}

export type DeskAddMemberToProjectResponse200 = DeskAddMemberToProjectResponseSuccess | DeskAddMemberToProjectResponseError;

export interface DeskAddMemberToProjectResponseSuccess {
    status: 'ok';
    user: DeskProjectMemberModel;
}

export interface DeskAddMemberToProjectResponseError {
    status: 'error';
}

export type DeskSendInviteResponse200 = DeskSendInviteResponseSuccess | DeskSendInviteResponseError;

export interface DeskSendInviteResponseSuccess {
    status: 'ok';
    user: DeskParticipantModel;
}

export interface DeskSendInviteResponseError {
    errors: Array<string>;
}

export const DESK_FIND_PARTICIPANT_LIMIT: number = 100;

@Injectable()
export class DeskRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    desk(deskId: number): Observable<DeskResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/desk/${deskId}`),
        );
    }

    getInvite(deskId: number, inviteCode: string): Observable<any> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/join/${inviteCode}`),
        );
    }

    columns(deskId: number): Observable<DeskColumnsResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/column`).pipe(
                map((response: Array<any>) => {
                    return response.map(r => {
                        const q: DeskColumnType = getDeskColumnTypeByName(r);

                        return <DeskColumnDefinitionModel>{
                            id: r.id,
                            name: r.name,
                            order: r.order,
                            type: q,
                            entities: <any>(() => {
                                return q === DeskColumnType.Projects ? 'projects' : 'cards';
                            })(),
                        };
                    });
                })),
        );
    }

    cards(deskId: number): Observable<DeskCardsResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/cards`),
        );
    }

    projects(deskId: number): Observable<DeskProjectsResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/project`),
        );
    }

    labels(deskId: number): Observable<DeskLabelsResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/label`),
        );
    }

    hasAccessToDesk(deskId: number, userId: number): Observable<boolean> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/checkPermissions`, { desk_id: deskId, user_id: userId }).pipe(map(r => (r as any).isEnable)),
        );
    }

    addProject(request: DeskAddProjectSubmitData): Observable<DeskAddProjectResponse200> {
        const toSubmit: any = {
            actions: [],
            attachments: [],
            checklists: [],
            column: request.columnId,
            deskId: request.deskId,
            deskName: request.deskName,
            labels: [],
            members: request.members,
            orders: request.orders,
            responsible: [],
            responsibleEdit: false,
            system: 0,
            title: request.title,
        };

        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${request.deskId}/card`, toSubmit),
        );
    }

    deleteProject(deskId: number, projectId: number): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/multidesk/${deskId}/project/${projectId}/delete`),
        );
    }

    updateProject(request: DeskUpdateProjectRequest): Observable<DeskUpdateProjectResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/${request.deskId}/project/${request.projectId}/update`, {
                deskId: request.deskId.toString(),
                projectId: request.projectId,
                titleProject: request.title,
                orders: request.orders,
            }),
        );
    }

    addCardToProject(deskId: number, cardId: number, projectId: number): Observable<DeskAddCardToProjectResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/${deskId}/project/card/add`, {
                deskId: deskId.toString(),
                cardId: cardId,
                projectId: projectId,
            }),
        );
    }

    deleteCardFromProject(deskId: number, cardId: number, projectId: number): Observable<DeskDeleteCardFromProjectResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/${deskId}/project/card/add`, {
                deskId: deskId.toString(),
                cardId: cardId,
                projectId: projectId,
            }),
        );
    }

    addCard(request: DeskAddCardSubmitData): Observable<DeskCardModel> {
        const toSubmit: any = {
            deskId: request.deskId,
            deskName: request.deskName,
            title: request.title,
            column: request.column,
            order: request.order,
            description: '',
            system: 0,
            daily: 0,
            labels: [],
            responsible: [],
            attachments: [],
            checklists: [],
            actions: [],
        };

        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${request.deskId}/card`, toSubmit),
        );
    }

    getCard(deskId: number, cardId: number): Observable<DeskCardModel> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}`),
        );
    }

    updateCard(card: DeskCardModel, changedField: DeskUpdateCardChangedField): Observable<DeskCardModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${card.deskId}/card/${card.id}`, {
                ...card,
                changedField: changedField,
            }),
        );
    }

    updateCards(deskId: number, cards: Array<{ card: DeskCardModel, changedField: DeskUpdateCardChangedField }>): Observable<DeskCardModel> {
        return httpPublishReplay(Observable.create(nextUpdated => {
            const queries: Array<Observable<DeskCardModel>> = cards.map(r => {
                const query$: Observable<DeskCardModel> = this.updateCard(r.card, r.changedField);

                query$.subscribe(
                    (response) => {
                        nextUpdated.next(response);
                    },
                    (httpError) => {
                        nextUpdated.error(httpError);
                    },
                );

                return query$;
            });

            observableForkJoin(queries).subscribe(
                () => {
                    nextUpdated.complete();
                },
                (httpError) => {
                    nextUpdated.error(httpError);
                },
            );
        }));
    }

    singleQueryUpdateCards(deskId: number, cards: Array<DeskCardModel>): Observable<void> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/cards`, cards),
        );
    }

    deleteCards(deskId: number, cardIds: Array<DeskCardId>): Observable<Array<DeskCardId>> {
        return observableForkJoin(
            cardIds.map(cardId => this.http.delete(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}`)),
        ) as Observable<Array<DeskCardId>>;
    }

    archiveCards(deskId: number, cardIds: Array<DeskCardId>): Observable<Array<DeskCardId>> {
        return observableForkJoin(
            cardIds.map(cardId => this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/archive`, {})),
        ) as Observable<Array<DeskCardId>>;
    }

    newLabel(deskId: number, name: string, color: string): Observable<DeskLabelModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/label`, {
                name: name,
                color: color,
            }),
        );
    }

    editLabel(deskId: number, labelId: number, name: string, color: string): Observable<DeskLabelModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/label/${labelId}`, {
                name: name,
                color: color,
            }),
        );
    }

    deleteLabel(deskId: number, labelId: number): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/label/${labelId}`),
        );
    }

    includeLabelToCard(deskId: number, cardId: number, labelId: number): Observable<DeskLabelModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/label/${labelId}`, { label: labelId, card: cardId }),
        );
    }

    excludeLabelFromCard(deskId: number, cardId: number, labelId: number): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/label/${labelId}`),
        );
    }

    newChecklist(deskId: number, cardId: number, request: DeskNewChecklistRequest): Observable<DeskCardChecklistModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/checklist`, {
                card: cardId,
                changedField: 'name',
                completedItemsCount: 0,
                items: [],
                name: request.title,
            }),
        );
    }

    updateChecklist(deskId: number, cardId: number, request: UpdateChecklistRequest): Observable<DeskCardChecklistModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/checklist/${request.id}`, request),
        );
    }

    deleteChecklist(deskId: number, cardId: number, request: DeleteChecklistRequest): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/checklist/${request.checklistId}`),
        );
    }

    newChecklistItem(deskId: number, cardId: number, request: DeskNewChecklistItemRequest): Observable<DeskCardChecklistItemModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/checklist/${request.checklistId}/item`, request),
        );
    }

    updateChecklistItem(deskId: number, cardId: number, checklistId: number, request: DeskUpdateChecklistItemRequest): Observable<DeskCardChecklistItemModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/checklist/${checklistId}/item/${request.id}`, request),
        );
    }

    deleteChecklistItem(deskId: number, cardId: number, request: DeskDeleteChecklistItemRequest): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/checklist/${request.checklistId}/item/${request.checklistItemId}`),
        );
    }

    attachLinkToCard(deskId: number, cardId: number, request: DeskAttachLinkToCardRequest): Observable<DeskCardAttachmentModel> {
        const formData: FormData = new FormData();

        formData.append('file', null);
        formData.append('name', request.title);
        formData.append('link', request.link);

        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/attachment/upload`, formData),
        );
    }

    uploadFileAndAttachToCard(deskId: number, cardId: number, request: UploadFileAndAttachToCardRequest): Observable<HttpProgressEvent | HttpResponse<DeskCardAttachmentModel>> {
        const formData: FormData = new FormData();

        formData.append('file', request.file);

        const uploadRequest: HttpRequest<any> = new HttpRequest('POST', `${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/attachment/upload`, formData, {
            reportProgress: true,
        });

        return httpPublishReplay(this.http.request(uploadRequest));
    }

    deleteAttachment(deskId: number, cardId: number, attachmentId: number): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/attachment/${attachmentId}`),
        );
    }

    addComment(deskId: number, cardId: number, text: string): Observable<DeskCardCommentModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/comment`, {
                card: cardId,
                text: text,
            }),
        );
    }

    updateComment(deskId: number, comment: DeskCardCommentModel, changedField: DeskUpdateCardCommentChangedField): Observable<DeskCardCommentModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${comment.card}/comment/${comment.id}`, {
                ...comment,
                changedField: changedField,
            }),
        );
    }

    deleteComment(deskId: number, cardId: number, commentId: number): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/comment/${commentId}`),
        );
    }

    checkForChanges(deskId: number, userId: number): Observable<boolean> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/checkChanges`, { userId: userId }, {
                responseType: 'text',
            }).pipe(map(r => r === 'true')),
        );
    }

    getProjectMembers(deskId: number, projectId: number): Observable<Array<DeskParticipantModel>> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/project/${projectId}`),
        );
    }

    getRestoredArchive(deskId: number, startDate: Date, endDate: Date): Observable<Array<DeskArchiveCardModel>> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/getArchieveCards`, {
                deskId: deskId,
                start: startDate,
                end: endDate,
            }, {}),
        );
    }

    findUser(searchStr: string, page?: number): Observable<Array<ProfileModel>> {
        const offset: number = page > 0 ? DESK_FIND_PARTICIPANT_LIMIT * page + 1 : 0;

        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/user/find?limit=${DESK_FIND_PARTICIPANT_LIMIT}&offset=${offset}`, {
                query: searchStr,
            }, {}),
        );
    }

    addMemberToProject(deskId: number, projectId: number, user: { active: boolean, id: number }): Observable<DeskAddMemberToProjectResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/project/member`, {
                active: user.active,
                deskId: deskId,
                projectId: projectId,
                userId: user.id,
            }, {}),
        );
    }

    deleteMemberFromProject(deskId: number, projectId: DeskProjectId, memberId: DeskMemberId): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/project/${projectId}/member/${memberId}`),
        );
    }

    addMemberToDesk(deskId: number, user: { active: boolean, email: string }): Observable<DeskAddMemberToDeskResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/member`, {
                active: user.active,
                email: user.email,
            }, {}),
        );
    }

    deleteMemberFromDesk(deskId: number, memberId: DeskMemberId): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/member/${memberId}`),
        );
    }

    sendInviteToDesk(deskId: number, email: string): Observable<DeskSendInviteResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/multidesk/${deskId}/member`, {
                active: true,
                email: email,
            }),
        );
    }

    addResponsibleToCard(deskId: number, cardId: number, responsibleId: number): Observable<DeskCardModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/responsible`, {
                action: 'add',
                responsible: responsibleId,
            }),
        );
    }

    deleteResponsibleFromCard(deskId: number, cardId: number, responsibleId: number): Observable<void> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/${deskId}/card/${cardId}/responsible`, {
                action: 'remove',
                responsible: responsibleId,
            }),
        );
    }

    restoreArchivedCard(cardId: number): Observable<DeskCardModel> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/desk/archieveToCard`, {
                id: cardId.toString(),
            }),
        );
    }
}
