import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {EvodeskNotification, NotificationId} from '../models/Notification.model';

export interface GetNotificationsResponse200 {
    notifications: Array<EvodeskNotification>;
    count_new: number;
    pagination: {
        current_page: number;
        total_pages: number;
    };
}

@Injectable()
export class NotificationRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    pull(options: { limit: number, page: number }): Observable<GetNotificationsResponse200> {
        return httpPublishReplay(
            this.http.get<any>(`${this.moduleConfig.apiEndpoint}/notification/all`, {
                params: <any>options,
            }).pipe(map(httpResponse => {
                return {
                    ...httpResponse,
                    count_new: parseInt(httpResponse.count_new, 10),
                    pagination: {
                        total_pages: parseInt(httpResponse.pagination.total_pages, 10),
                        current_page: parseInt(httpResponse.pagination.count_new, 10),
                    },
                    notifications: httpResponse.notifications.map(n => {
                        return {
                            kind: n.notificationType.name,
                            model: n,
                        };
                    }),
                };
            })),
        );
    }

    setAsViewed(notificationIds: Array<NotificationId>): Observable<void> {
        return httpPublishReplay(
            this.http.post<void>(`${this.moduleConfig.apiEndpoint}/notification/setViewed`, {
                id: notificationIds,
            }),
        );
    }
}
