import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {LeaderEntryModel} from '../models/leaders/LeaderEntry.model';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {ProfileId} from '../models/profile/Profile.model';

export enum LeadersRange {
    Week = 'Week',
    Month = 'Month',
    All = 'All',
}

export interface LeadersResponse200
{
    data: Array<LeaderEntryModel>;
    this: LeaderEntryModel;
}

@Injectable()
export class LeadersRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    leaders(options: { forProfileId: ProfileId; range: LeadersRange; offset: number; limit: number }): Observable<LeadersResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/profile/getLeaders${<any>options.range}`, {
                params: <any>{
                    forProfileId: options.forProfileId,
                    offset: options.offset,
                    limit: options.limit,
                },
            }),
        );
    }
}
