import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {UserModel} from '../models/user/User.model';

import {BackendError} from '../../../../functions/backendErrorToString.function';
import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {UserActivityModel} from '../models/user/UserActivity.model';

export interface SignInRequest {
    email: string;
    password: string;
}

export interface SignInResponse200 {
    user: UserModel;
    access_token: string;
    refresh_token: string;
}

export interface RefreshTokenResponse200 {
    access_token: string;
    refresh_token: string;
}

export interface SignUpRequest {
    checked: true;
    email: string;
    password: string;
    promo: string;
}

export interface SignUpResponse200 {
    activated: boolean;
    newPassword: string;
    user: UserModel;
}

export interface ResetPasswordRequest {
    email: string;
}

export interface ResetPasswordResponse200 {
}

export interface ActivateUserRequest {
    email: string;
    code: string;
}

export type ResetPasswordError = BackendError;
export type CurrentUserResponse200 = UserModel;
export type ActivateUserResponse = any;

export interface GetUserActivityRequest {
    offset: number;
    limit: number;
}

export type OAuthServices = 'vk' | 'fb' | 'gl' | 'ln';

@Injectable()
export class UserRESTService {
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    goOAuth(service: OAuthServices): void {
        location.href = `${this.moduleConfig.apiEndpoint}/user/login/${service}?redirect=${this.moduleConfig.serverHostName}/auth/oauth`;
    }

    signIn(request: SignInRequest): Observable<SignInResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/user/getToken`, request),
        );
    }

    signOut(): Observable<void> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/user/logout`, {}),
        );
    }

    signUp(request: SignUpRequest): Observable<SignUpResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/user/registration`, request),
        );
    }

    refreshToken(refreshToken: string): Observable<RefreshTokenResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/user/refreshToken`, {}, {
                headers: {
                    Token: refreshToken,
                },
            }),
        );
    }

    resetPassword(request: ResetPasswordRequest): Observable<ResetPasswordResponse200 | ResetPasswordError> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/user/password/reset`, request),
        );
    }

    activateUser(request: ActivateUserRequest): Observable<ActivateUserResponse> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/user/active`, {
                params: {
                    email: request.email,
                    code: request.code,
                },
            }),
        );
    }

    current(): Observable<CurrentUserResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/user`),
        );
    }

    getActivity(request: GetUserActivityRequest): Observable<Array<UserActivityModel>> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/activity`, {
                params: <any>{
                    offset: request.offset,
                    limit: request.limit,
                },
            }),
        );
    }
}
