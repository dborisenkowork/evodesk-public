import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {AttachmentId, AttachmentLinkModel, AttachmentPhotoModel, AttachmentVideoModel} from '../models/post/Post.model';
import {PostId} from '../models/post/Post.model';

export type AttachPhotoResponse200 = AttachmentPhotoModel;
export type AttachVideoResponse200 = AttachmentVideoModel;
export type AttachLinkResponse200 = AttachmentLinkModel;

@Injectable()
export class AttachmentRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    deletePostAttachment(postId: PostId, attachmentId: AttachmentId): Observable<void> {
        return httpPublishReplay(
            this.http.delete(`${this.moduleConfig.apiEndpoint}/post/${postId}/attachment/${attachmentId}`),
        );
    }

    attachPhoto(file: File): Observable<AttachPhotoResponse200> {
        const fd: FormData = new FormData();

        fd.append('file', file);

        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/attach/photo`, fd),
        );
    }

    attachVideo(url: string): Observable<AttachVideoResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/attach/video`, {
                link: url,
            }),
        );
    }

    attachLink(url: string): Observable<AttachLinkResponse200> {
        return httpPublishReplay(
            this.http.post(`${this.moduleConfig.apiEndpoint}/attach/link`, {
                link: url,
            }),
        );
    }
}
