
import {map} from 'rxjs/operators';
import {Injectable, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {EVODESK_REST_API_MODULE_CONFIG, EvoDeskRESTApiEnvironmentConfig} from '../configs/EvodeskRESTApiEnvironment.config';

import {httpPublishReplay} from '../functions/httpPublishReplay.function';

import {FaqEntry, FaqGroup} from '../../../features/EvodeskFaq/models/Faq.model';

export interface FaqListResponse200 {
    groups: Array<FaqGroup>;
    entries: Array<FaqEntry>;
}

@Injectable()
export class FaqRESTService
{
    constructor(
        private http: HttpClient,
        @Inject(EVODESK_REST_API_MODULE_CONFIG) private moduleConfig: EvoDeskRESTApiEnvironmentConfig,
    ) {}

    faqList(): Observable<FaqListResponse200> {
        return httpPublishReplay(
            this.http.get(`${this.moduleConfig.apiEndpoint}/faq/data`).pipe(
                map(response => {
                    const groups: Array<FaqGroup> = [];
                    const entries: Array<FaqEntry> = [];

                    for (const key in response) {
                        if (response.hasOwnProperty(key)) {
                            groups.push({
                                title: key,
                            });

                            for (const entry of response[key]) {
                                entries.push(entry);
                            }
                        }
                    }

                    return {
                        groups: groups,
                        entries: entries,
                    };
                })),
        );
    }
}
