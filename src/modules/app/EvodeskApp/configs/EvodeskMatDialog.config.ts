import {MatDialogConfig} from '@angular/material';

export const defaultMatDialogConfig: MatDialogConfig = {
    panelClass: '__evodesk-mat-dialog-default',
    backdropClass: '__evodesk-mat-dialog-default-backdrop',
};

export const defaultBottomMatDialogConfig: MatDialogConfig = {
    panelClass: '__evodesk-mat-dialog-bottom-420px',
    backdropClass: '__evodesk-mat-dialog-default-backdrop',
};

export const defaultBottomMatDialog600pxConfig: MatDialogConfig = {
    panelClass: '__evodesk-mat-dialog-bottom-600px',
    backdropClass: '__evodesk-mat-dialog-default-backdrop',
};

export const defaultBottomMatDialog720pxConfig: MatDialogConfig = {
    panelClass: '__evodesk-mat-dialog-bottom-720px',
    backdropClass: '__evodesk-mat-dialog-default-backdrop',
};

export const defaultBottomMatDialog800pxConfig: MatDialogConfig = {
    panelClass: '__evodesk-mat-dialog-bottom-800px',
    backdropClass: '__evodesk-mat-dialog-default-backdrop',
};

export const defaultDisablePaddingsMatDialogConfig: MatDialogConfig = {
    panelClass: '__evodesk-mat-dialog-disable-paddings',
    backdropClass: '__evodesk-mat-dialog-default-backdrop',
};

export const defaultFullscreenMatDialogConfig: MatDialogConfig = {
    panelClass: '__evodesk-mat-dialog-fullscreen',
    backdropClass: '__evodesk-mat-dialog-fullscreen',
};

export const v3Bottom420pxMatDialogConfig: MatDialogConfig = {
    panelClass: '__evodesk-v3-mat-dialog-bottom-420px',
    backdropClass: '__evodesk-mat-dialog-v3-backdrop',
};

export const v3Bottom600pxMatDialogConfig: MatDialogConfig = {
    panelClass: '__evodesk-v3-mat-dialog-bottom-600px',
    backdropClass: '__evodesk-mat-dialog-v3-backdrop',
};

export const v3Bottom800pxMatDialogConfig: MatDialogConfig = {
    panelClass: '__evodesk-v3-mat-dialog-bottom-800px',
    backdropClass: '__evodesk-mat-dialog-v3-backdrop',
};

export const v3Bottom710pxMatDialogConfig: MatDialogConfig = {
    panelClass: '__evodesk-v3-mat-dialog-bottom-710px',
    backdropClass: '__evodesk-mat-dialog-v3-backdrop',
};

export const v3Bottom317pxMatDialogConfig: MatDialogConfig = {
    panelClass: '__evodesk-v3-mat-dialog-bottom-317px',
    backdropClass: '__evodesk-mat-dialog-v3-backdrop',
};

export const v3Bottom349pxMatDialogConfig: MatDialogConfig = {
    panelClass: '__evodesk-v3-mat-dialog-bottom-349px',
    backdropClass: '__evodesk-mat-dialog-v3-backdrop',
};

export const v3Bottom294pxMatDialogConfig: MatDialogConfig = {
    panelClass: '__evodesk-v3-mat-dialog-bottom-294px',
    backdropClass: '__evodesk-mat-dialog-v3-backdrop',
};

export const v3BottomDeskCardModalMatDialogConfig: MatDialogConfig = {
    panelClass: '__evodesk-v3-mat-dialog-desk-card-modal',
    backdropClass: '__evodesk-mat-dialog-v3-backdrop',
};

