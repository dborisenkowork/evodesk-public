import {Route, Routes} from '@angular/router';
import {Provider} from '@angular/core';

import {NotFoundRouteComponent} from '../routes/NotFoundRoute/NotFoundRoute.component';

import {AutoRedirectToDashboardGuard} from '../guards/AutoRedirectToDashboard.guard';

import {IndexRouteComponent} from '../routes/IndexRoute/IndexRoute.component';

const routeNotFound: Route = {
    path: '**',
    component: NotFoundRouteComponent,
};

export const EvodeskAppRouting: {
    routes: Routes,
    declarations: Array<Function>,
    providers: Array<Provider>,
} = {
    routes: [
        {
            path: '',
            pathMatch: 'full',
            component: IndexRouteComponent,
            canActivate: [
                AutoRedirectToDashboardGuard,
            ],
        },
        {
            path: 'coin',
            redirectTo: '/landing/coin',
        },
        {
            path: 'auth',
            loadChildren: 'modules/features/EvodeskAuth/EvodeskAuth.module#EvodeskAuthModule',
        },
        {
            path: 'billing',
            loadChildren: 'modules/features/EvodeskBilling/EvodeskBilling.module#EvodeskBillingModule',
        },
        {
            path: 'landing',
            loadChildren: 'modules/features/EvodeskLanding/EvodeskLanding.module#EvodeskLandingModule',
        },
        {
            path: 'desk',
            loadChildren: 'modules/features/EvodeskDesk/EvodeskDesk.module#EvodeskDeskModule',
        },
        {
            path: 'profile',
            loadChildren: 'modules/features/EvodeskProfile/EvodeskProfile.module#EvodeskProfileModule',
        },
        {
            path: 'desk',
            loadChildren: 'modules/features/EvodeskDesk/EvodeskDesk.module#EvodeskDeskModule',
        },
        {
            path: 'shop',
            loadChildren: 'modules/features/EvodeskShop/EvodeskShop.module#EvodeskShopModule',
        },
        {
            path: 'rating',
            loadChildren: 'modules/features/EvodeskAchievements/EvodeskAchievements.module#EvodeskAchievementsModule',
        },
        {
            path: 'leaders',
            loadChildren: 'modules/features/EvodeskLeaders/EvodeskLeaders.module#EvodeskLeadersModule',
        },
        {
            path: 'exchange',
            loadChildren: 'modules/features/EvodeskExchange/EvodeskExchange.module#EvodeskExchangeModule',
        },
        {
            path: 'people',
            loadChildren: 'modules/features/EvodeskPeople/EvodeskPeople.module#EvodeskPeopleModule',
        },
        {
            path: 'faq',
            loadChildren: 'modules/features/EvodeskFaq/EvodeskFaq.module#EvodeskFaqModule',
        },
        {
            path: 'partners',
            loadChildren: 'modules/features/EvodeskPartners/EvodeskPartners.module#EvodeskPartnersModule',
        },
        {
            path: 'news',
            loadChildren: 'modules/features/EvodeskNews/EvodeskNews.module#EvodeskNewsModule',
        },
        {
            path: 'im',
            loadChildren: 'modules/features/EvodeskIM/EvodeskIM.module#EvodeskIMModule',
        },
        {
            path: 'user-settings',
            loadChildren: 'modules/features/EvodeskUserSettings/EvodeskUserSettings.module#EvodeskUserSettingsModule',
        },
        {
            path: 'desks',
            loadChildren: 'modules/features/EvodeskDeskDashboard/EvodeskDeskDashboard.module#EvodeskDeskDashboardModule',
        },
        routeNotFound,
    ],
    declarations: [
        NotFoundRouteComponent,
    ],
    providers: [],
};
