import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {NavigationStart, Router, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpErrorResponse} from '@angular/common/http';

import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {LocalStorageModule} from 'angular-2-local-storage';
import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';

import {filter} from 'rxjs/operators';

import * as moment from 'moment';

import {ru_RU} from './translations/ru_RU';
import {en_GB} from './translations/en_GB';

import {api, environment} from '../../../environments/environment.config';

import {EvodeskAppRouting} from './configs/EvodeskAppRouting.config';

import {tsToTranslations} from './functions/ts-to-translations.function';

import {EVODESK_REST_API_MODULE_CONFIG} from '../EvodeskRESTApi/configs/EvodeskRESTApiEnvironment.config';

import {EvodeskAppSharedModule} from './EvodeskAppShared.module';
import {EvodeskHeaderModule} from '../../features/EvodeskHeader/EvodeskHeader.module';
import {EvodeskBillingModule} from '../../features/EvodeskBilling/EvodeskBilling.module';

import {AppRootComponent} from './components/module/AppRoot/AppRoot.component';
import {EmptyComponentComponent} from './components/module/EmptyComponent/EmptyComponent.component';
import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';

import {AppEvent} from '../EvodeskAppMessageBus/models/app-events.model';
import {PHONE_CODES, phoneCodes} from '../../../models/phoneCodes.models';

import {JWTHttpInterceptor} from './http-interceptors/JWT.http-interceptor';

import {AuthTokenService} from '../../features/EvodeskAuth/services/AuthToken.service';
import {EvodeskAppLoadingStatusService} from './services/EvodeskAppLoadingStatus.service';
import {EvodeskAppMessageBusService} from '../EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';
import {EvodeskExchangeRatesService} from '../../features/EvodeskBilling/services/EvodeskExchangeRates.service';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule.withServerTransition({appId: 'evodesk-app'}),
        BrowserAnimationsModule,
        TranslateModule.forRoot(),
        ScrollToModule.forRoot(),
        LocalStorageModule.withConfig({
            prefix: 'evodesk-app',
            storageType: 'localStorage',
        }),
        RouterModule.forRoot(EvodeskAppRouting.routes, {
            useHash: environment.angular.useHashStrategy,
            enableTracing: environment.angular.enableRouterTracing,
        }),

        EvodeskAppSharedModule.forRoot(),

        EvodeskHeaderModule,
        EvodeskBillingModule,
    ],
    exports: [
        EvodeskAppSharedModule,
    ],
    declarations: [
        AppRootComponent,
        EmptyComponentComponent,
        IndexRouteComponent,

        EvodeskAppRouting.declarations,
    ],
    providers: [
        api,
        EvodeskAppRouting.providers,
        EvodeskAppLoadingStatusService,
        EvodeskExchangeRatesService,
        { provide: EVODESK_REST_API_MODULE_CONFIG, useValue: environment.modules.EvoDeskRESTApi },
        { provide: PHONE_CODES, useValue: phoneCodes },
        { provide: HTTP_INTERCEPTORS, useClass: JWTHttpInterceptor, multi: true },
    ],
    bootstrap: [
        AppRootComponent,
    ],
})
export class EvodeskAppModule
{
    constructor(
        private router: Router,
        private loading: EvodeskAppLoadingStatusService,
        private translate: TranslateService,
        private authToken: AuthTokenService,
        private appBus: EvodeskAppMessageBusService,
    ) {
        this.initTranslationModule();
        this.initAuthToken();
        this.initDefaultLocale();
        this.initLoadingReset();
    }

    private initTranslationModule(): void {
        this.translate.setTranslation('ru_RU', tsToTranslations(ru_RU), false);
        this.translate.setTranslation('en_GB', tsToTranslations(en_GB), false);
    }

    private initAuthToken(): void {
        this.authToken.bootstrapFromLocalStorage();

        this.appBus.stream$.subscribe((e) => {
            switch (e.type) {
                case AppEvent.HttpError: {
                    const httpError: HttpErrorResponse = e.payload.reason;

                    if (httpError && typeof httpError === 'object' && httpError.status) {
                        if (httpError.status === 400) {
                            this.authToken.clear();
                            this.router.navigate(['/']);
                        }
                    }

                    break;
                }
            }
        });
    }

    private initDefaultLocale(): void {
        moment.locale('ru');
        this.translate.setDefaultLang('ru_RU');
    }

    private initLoadingReset(): void {
        this.router.events.pipe(
            filter(e => <any>e instanceof NavigationStart),
        ).subscribe(() => {
            this.loading.clearAllLoadings();
        });
    }
}
