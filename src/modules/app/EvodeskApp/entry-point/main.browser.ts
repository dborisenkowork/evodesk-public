import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {environment} from '../../../../environments/environment.config';

import {IS_NGZONE_DISABLED} from '../configs/EvodeskApp.config';

import {EvodeskAppModule} from '../EvodeskApp.module';

(window as any).global = window;

if (environment.angular.enableProductionMode) {
    enableProdMode();
}

import './integrations';

/* tslint:disable:no-console */
platformBrowserDynamic()
    .bootstrapModule(EvodeskAppModule, {
        ngZone: IS_NGZONE_DISABLED ? 'noop' : 'zone.js',
    })
    .catch(err => console.log(err));
/* tslint:enable:no-console */
