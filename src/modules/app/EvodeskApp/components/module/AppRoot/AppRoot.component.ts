import {ApplicationRef, ChangeDetectorRef, Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, RouteConfigLoadEnd, RouteConfigLoadStart, Router} from '@angular/router';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {EvodeskMatIconsCardService} from '../../../services/EvodeskMatIconsCard.service';
import {EvodeskAppLoadingStatusService} from '../../../services/EvodeskAppLoadingStatus.service';
import {EvodeskAppGlobalScrollService} from '../../../services/EvodeskAppGlobalScroll.service';

import {HTTP_PUBLISH_REPLAY_REF} from '../../../../EvodeskRESTApi/functions/httpPublishReplay.function';

interface State {
    isLoading: boolean;
    isLazyModuleLoading: boolean;
}

@Component({
    selector: 'evodesk-app-root',
    templateUrl: './AppRoot.component.pug',
    styleUrls: [
        './AppRoot.component.scss',
    ],
})
export class AppRootComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        isLoading: false,
        isLazyModuleLoading: false,
    };

    constructor(
        private ngZone: NgZone,
        private router: Router,
        private cdRef: ChangeDetectorRef,
        private appRef: ApplicationRef,
        private loading: EvodeskAppLoadingStatusService,
        private matIconsCard: EvodeskMatIconsCardService,
        private appGlobalScroll: EvodeskAppGlobalScrollService,
    ) {}

    ngOnInit(): void {
        HTTP_PUBLISH_REPLAY_REF.appRef = this.appRef;

        this.subscribeToScrollTop();
        this.subscribeToLoadingService();
        this.subscribeToAutoDestroyLoadingsOnNavigations();
        this.subscribeToLazyLoadingModules();

        this.matIconsCard.init();
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onScroll($event): void {
        this.ngZone.runOutsideAngular(() => {
            this.appGlobalScroll.emit($event);
        });
    }

    private subscribeToScrollTop(): void {
        this.router.events.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(e => <any>e instanceof NavigationEnd),
        ).subscribe(() => window.scrollTo(0, 0));
    }

    private subscribeToLoadingService(): void {
        this.loading.isLoading$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(isLoading => {
            this.state = {
                ...this.state,
                isLoading: isLoading,
            };

            this.cdRef.detectChanges();
        });
    }

    private subscribeToAutoDestroyLoadingsOnNavigations(): void {
        this.router.events.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(e => <any>e instanceof NavigationEnd),
        ).subscribe(() => {
            this.loading.clearAllLoadings();
        });
    }

    private subscribeToLazyLoadingModules(): void {
        this.router.events.pipe(takeUntil(this.ngOnDestroy$)).subscribe((event) => {
            if (event instanceof RouteConfigLoadStart) {
                this.state = {
                    ...this.state,
                    isLazyModuleLoading: true,
                };

                this.cdRef.detectChanges();
            } else if (event instanceof RouteConfigLoadEnd) {
                this.state = {
                    ...this.state,
                    isLazyModuleLoading: false,
                };

                this.cdRef.detectChanges();
            }
        });
    }
}
