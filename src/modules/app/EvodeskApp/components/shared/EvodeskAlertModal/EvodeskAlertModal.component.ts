import {Component, ElementRef, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

export interface EvodeskAlertModalComponentProps
{
    title: string;
    text: string;
    textAsHtml: boolean;
    ok: string;
    copyLink: string | undefined;
}

@Component({
    templateUrl: './EvodeskAlertModal.component.pug',
    styleUrls: [
        './EvodeskAlertModal.component.scss',
    ],
})
export class EvodeskAlertModalComponent
{
    @ViewChild('input') inputRef: ElementRef;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: EvodeskAlertModalComponentProps,
    ) {}

    selectAndCopy(): void {
        (this.inputRef.nativeElement as HTMLInputElement).focus();
        (this.inputRef.nativeElement as HTMLInputElement).setSelectionRange(0, this.data.copyLink.length);

        document.execCommand('Copy');
    }
}
