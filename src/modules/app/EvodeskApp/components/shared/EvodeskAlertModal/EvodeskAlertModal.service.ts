
import {of as observableOf, forkJoin as observableForkJoin, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {sprintf} from 'sprintf-js';

import {EvodeskAlertModalComponent, EvodeskAlertModalComponentProps} from './EvodeskAlertModal.component';

export interface EvodeskAlertModalOptions
{
    title: { text: string, translate?: boolean, replaces?: Array<any> };
    text: { text: string, translate?: boolean, replaces?: Array<any>, asHtml?: boolean };
    ok: { text: string, translate?: boolean, replaces?: Array<any> };
    copyLink?: string | undefined;
}

@Injectable()
export class EvodeskAlertModalService
{
    constructor(
        private matDialog: MatDialog,
        private translate: TranslateService,
    ) {}

    open(withOptions: EvodeskAlertModalOptions): Observable<void> {
        return Observable.create(close => {
            observableForkJoin([
                withOptions.title.translate ? this.translate.get(withOptions.title.text) : observableOf(withOptions.title.text),
                withOptions.text.translate ? this.translate.get(withOptions.text.text) : observableOf(withOptions.text.text),
                withOptions.ok.translate ? this.translate.get(withOptions.ok.text) : observableOf(withOptions.ok.text),
            ]).subscribe((results) => {
                const matDialogRef: MatDialogRef<EvodeskAlertModalComponent> = this.matDialog.open(EvodeskAlertModalComponent, {
                    panelClass: '__evodesk-mat-dialog-default',
                    backdropClass: '__evodesk-mat-dialog-default-backdrop',
                    closeOnNavigation: true,
                    data: <EvodeskAlertModalComponentProps>{
                        title: sprintf(results[0], withOptions.title.replaces || []),
                        text: sprintf(results[1], withOptions.text.replaces || []),
                        textAsHtml: !!withOptions.text.asHtml,
                        ok: sprintf(results[2], withOptions.ok.replaces || []),
                        copyLink: withOptions.copyLink,
                    },
                });

                matDialogRef.afterClosed().subscribe(() => {
                    close.next(undefined);
                    close.complete();
                });
            });
        });
    }

    openTranslated(prefix: string): Observable<void> {
        return this.open({
            title: {
                text: `${prefix}.Title`,
                translate: true,
            },
            text: {
                text: `${prefix}.Text`,
                translate: true,
            },
            ok: {
                text: `${prefix}.Ok`,
                translate: true,
            },
        });
    }

    httpError(): Observable<void> {
        return this.openTranslated('EvodeskApp.services.EvodeskAlertModal.httpError');
    }
}

