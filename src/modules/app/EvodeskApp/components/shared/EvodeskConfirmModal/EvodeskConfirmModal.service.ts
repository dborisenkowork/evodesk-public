import {Injectable, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';

import {of as observableOf, forkJoin as observableForkJoin, Subject, Observable, BehaviorSubject} from 'rxjs';

import {sprintf} from 'sprintf-js';
import {takeUntil} from 'rxjs/operators';

import {EvodeskConfirmModalComponent, EvodeskConfirmModalComponentProps} from './EvodeskConfirmModal.component';

export interface EvodeskConfirmModalOptions {
    title: { text: string, translate?: boolean, replaces?: Array<any> };
    text: { text: string, translate?: boolean, replaces?: Array<any>, asHtml?: boolean };
    confirmButton: { text: string, translate?: boolean, replaces?: Array<any> };
    cancelButton: { text: string, translate?: boolean, replaces?: Array<any> };
    confirm?: Function;
    cancel?: Function;
    viewContainerRef?: ViewContainerRef;
    closeOnNavigation?: boolean;
}

export interface EvodeskConfirmModalServiceCurrentDialog {
    options: EvodeskConfirmModalOptions;
    matDialogRef: MatDialogRef<EvodeskConfirmModalComponent>;
}

@Injectable()
export class EvodeskConfirmModalService
{
    private _current$: BehaviorSubject<EvodeskConfirmModalServiceCurrentDialog | undefined> = new BehaviorSubject<EvodeskConfirmModalServiceCurrentDialog|undefined>(undefined);

    constructor(
        private matDialog: MatDialog,
        private translate: TranslateService,
    ) {}

    get current$(): Observable<EvodeskConfirmModalServiceCurrentDialog | undefined> {
        return this._current$.asObservable();
    }

    open(withOptions: EvodeskConfirmModalOptions): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create(confirmed => {
            observableForkJoin([
                withOptions.title.translate ? this.translate.get(withOptions.title.text) : observableOf(withOptions.title.text),
                withOptions.text.translate ? this.translate.get(withOptions.text.text) : observableOf(withOptions.text.text),
                withOptions.confirmButton.translate ? this.translate.get(withOptions.confirmButton.text) : observableOf(withOptions.confirmButton.text),
                withOptions.cancelButton.translate ? this.translate.get(withOptions.cancelButton.text) : observableOf(withOptions.cancelButton.text),
            ]).subscribe((results) => {
                const config: MatDialogConfig = {
                    disableClose: true,
                    panelClass: '__evodesk-v3-mat-dialog-confirm',
                    backdropClass: '__evodesk-mat-dialog-v3-backdrop',
                    closeOnNavigation: true,
                    data: <EvodeskConfirmModalComponentProps>{
                        title: sprintf(results[0], ...(withOptions.title.replaces || [])),
                        text: sprintf(results[1], ...(withOptions.text.replaces || [])),
                        textAsHtml: !!withOptions.text.asHtml,
                        confirmButton: sprintf(results[2], ...(withOptions.confirmButton.replaces || [])),
                        cancelButton: sprintf(results[3], ...(withOptions.cancelButton.replaces || [])),
                    },
                };

                if (withOptions.viewContainerRef) {
                    config.viewContainerRef = withOptions.viewContainerRef;
                }

                if (withOptions.closeOnNavigation) {
                    config.closeOnNavigation = withOptions.closeOnNavigation;
                }

                const matDialogRef: MatDialogRef<EvodeskConfirmModalComponent> = this.matDialog.open(EvodeskConfirmModalComponent, config);


                matDialogRef.afterOpened().pipe(takeUntil(unsubscribe$)).subscribe(() => {
                    matDialogRef.componentInstance.confirmEvent.pipe(
                        takeUntil(unsubscribe$))
                        .subscribe(() => {
                            confirmed.next(true);
                            confirmed.complete();

                            this.close();

                            matDialogRef.close();

                            if (withOptions.confirm) {
                                withOptions.confirm();
                            }
                        });

                    matDialogRef.componentInstance.cancelEvent.pipe(
                        takeUntil(unsubscribe$))
                        .subscribe(() => {
                            confirmed.next(false);
                            confirmed.complete();

                            this.close();

                            matDialogRef.close();

                            if (withOptions.cancel) {
                                withOptions.cancel();
                            }
                        });

                    this._current$.pipe(takeUntil(unsubscribe$)).subscribe((n) => {
                        if (! n || n.options !== withOptions) {
                            matDialogRef.close();

                            confirmed.next(false);
                            confirmed.complete();
                        }
                    });
                });

                this._current$.next({
                    matDialogRef: matDialogRef,
                    options: withOptions,
                });
            });

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    close(): void {
        this._current$.next(undefined);
    }

    isOpened(): boolean {
        return this._current$.getValue() !== undefined;
    }
}
