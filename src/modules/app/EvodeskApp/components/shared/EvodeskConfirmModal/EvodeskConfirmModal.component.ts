import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

export interface EvodeskConfirmModalComponentProps {
    title: string;
    text: string;
    textAsHtml: boolean;
    confirmButton: string;
    cancelButton: string;
}

@Component({
    templateUrl: './EvodeskConfirmModal.component.pug',
    styleUrls: [
        './EvodeskConfirmModal.component.scss',
    ],
})
export class EvodeskConfirmModalComponent
{
    @Output() cancelEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output() confirmEvent: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: EvodeskConfirmModalComponentProps,
    ) {}
}
