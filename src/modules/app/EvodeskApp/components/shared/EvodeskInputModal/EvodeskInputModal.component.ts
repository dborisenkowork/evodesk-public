import {ChangeDetectionStrategy, Component, EventEmitter, Inject, Output} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {AbstractControl} from '@angular/forms';

export interface EvodeskInputModalProps {
    title: string;
    text: string;
    textAsHtml: boolean;
    confirmButton: string;
    cancelButton: string;
    formControl: AbstractControl;
}

@Component({
    selector: 'evodesk-input-modal',
    templateUrl: './EvodeskInputModal.component.pug',
    styleUrls: ['./EvodeskInputModal.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskInputModalComponent
{
    @Output() cancelEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output() confirmEvent: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: EvodeskInputModalProps,
    ) {}

    confirm(): void {
        if (this.data.formControl.valid) {
            this.confirmEvent.emit(undefined);
        }
    }
}
