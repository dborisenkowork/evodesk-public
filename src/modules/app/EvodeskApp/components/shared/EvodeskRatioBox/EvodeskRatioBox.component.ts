import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';

@Component({
    selector: 'evodesk-app-common-ratio-box',
    template: `<div #element [ngStyle]="styles"><ng-content></ng-content></div>`,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskRatioBoxComponent implements OnInit, OnDestroy
{
    @ViewChild('element') elementRef: ElementRef;

    @Input() aspectW: number = 16;
    @Input() aspectH: number = 9;

    private lastWidth: number;
    private nextFrame: any;

    public styles: any = {
        width: '100%',
        height: 'auto',
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private ngZone: NgZone,
    ) {}

    ngOnInit(): void {
        const update: FrameRequestCallback = () => {
            this.ngZone.runOutsideAngular(() => {
                this.recalculate();

                this.nextFrame = window.requestAnimationFrame(update);
            });
        };

        this.nextFrame = window.requestAnimationFrame(update);
    }

    ngOnDestroy(): void {
        if (this.nextFrame) {
            window.cancelAnimationFrame(this.nextFrame);
        }
    }

    recalculate(): void {
        if (this.elementRef) {
            const clientRect: ClientRect = (this.elementRef.nativeElement as HTMLElement).getBoundingClientRect();
            const width: number = clientRect.width;

            if (width > 0 && this.lastWidth !== width) {
                this.lastWidth = width;

                this.styles = {
                    width: '100%',
                    height: `${(width / this.aspectW) * this.aspectH}px`,
                };

                this.cdr.detectChanges();
                this.cdr.detectChanges();
            }
        }
    }
}
