import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'evodesk-app-common-sub-menu',
    templateUrl: './EvodeskAppSubMenu.component.pug',
    styleUrls: [
        './EvodeskAppSubMenu.component.scss',
    ],
})
export class EvodeskAppSubMenuComponent
{
    @Input() isOpened: boolean = false;
    @Input() deskTitle: string = '';

    @Output('toggle') toggleEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    toggle(): void {
        this.toggleEvent.emit(this.isOpened);
    }

    hasDeskTitle(): boolean {
      return this.deskTitle.length > 0;
    }
}
