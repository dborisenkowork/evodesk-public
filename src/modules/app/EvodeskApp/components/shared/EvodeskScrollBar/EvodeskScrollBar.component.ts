import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, HostListener, Input, NgZone, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';

export type ScrollBarDirection = 'x' | 'y';

export type EvodeskScrollBarComponentTheme = 'default' | 'blue';

interface State {
    canScroll: boolean;
    containerBasis: number;
    containerNgStyle: any;
    scrollBlockNgStyle: any;
    isDragging: boolean;
    initialX: number;
    initialY: number;
    initialScrollLeft: number;
    initialScrollTop: number;
    visible: boolean;
    forceVisible: boolean;
}

function bound(input: number, min: number, max: number): number {
    return Math.min(max, Math.max(input, min));
}

function percents(input: number): number {
    return bound(input, 0, 100);
}

export interface ScrollInfo {
    x: number;
    y: number;
}

@Component({
    selector: 'evodesk-app-common-scroll-bar',
    templateUrl: './EvodeskScrollBar.component.pug',
    styleUrls: [
        './EvodeskScrollBar.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskScrollBarComponent implements OnChanges, OnInit, OnDestroy
{
    public static MIN_SIZE: number = 10;

    @Input() enabled: boolean = true;
    @Input() elementRef: HTMLElement;
    @Input() scrollBarDirection: ScrollBarDirection = 'y';
    @Input() theme: EvodeskScrollBarComponentTheme = 'default';
    @Input() autoHide: boolean = false;

    @Output() scrollBlockMove: EventEmitter<ScrollInfo> = new EventEmitter<ScrollInfo>();

    private eventListeners: Array<any> = [];

    private destroyed: boolean = false;

    public state: State = {
        canScroll: false,
        containerBasis: 0,
        containerNgStyle: {},
        scrollBlockNgStyle: {},
        isDragging: false,
        initialX: 0,
        initialY: 0,
        initialScrollLeft: 0,
        initialScrollTop: 0,
        visible: false,
        forceVisible: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private ngZone: NgZone,
        private changeDetectionRef: ChangeDetectorRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['elementRef'] || changes['autoHide']) {
            this.upListeners();
        }
    }

    ngOnInit(): void {
        const nextFrame: Function = () => {
            if (! this.enabled) {
                return;
            }

            this.ngZone.runOutsideAngular(() => {
                if (this.elementRef) {
                    if (this.scrollBarDirection === 'x') {
                        const canScroll: boolean = this.elementRef.scrollWidth > this.elementRef.clientWidth;

                        if (this.state.canScroll !== canScroll) {
                            this.state = {
                                ...this.state,
                                canScroll: canScroll,
                            };
                        }

                        this.updateScrollBlockSize();
                    } else if (this.scrollBarDirection === 'y') {
                        const canScroll: boolean = this.elementRef.scrollHeight > this.elementRef.clientHeight;

                        if (this.state.canScroll !== canScroll) {
                            this.state = {
                                ...this.state,
                                canScroll: canScroll,
                            };
                        }

                        this.updateScrollBlockSize();
                    }

                    if (this.state.canScroll) {
                        if (this.scrollBarDirection === 'x') {
                            const basis: number = this.elementRef.clientWidth;

                            if (this.state.containerBasis !== basis) {
                                this.state = {
                                    ...this.state,
                                    containerBasis: basis,
                                    containerNgStyle: {
                                        ...this.state.containerNgStyle,
                                        width: `${basis}px`,
                                    },
                                };

                                if (! this.destroyed) { this.changeDetectionRef.detectChanges(); }
                            }
                        } else if (this.scrollBarDirection === 'y') {
                            const basis: number = this.elementRef.clientHeight;

                            if (this.state.containerBasis !== basis) {
                                this.state = {
                                    ...this.state,
                                    containerBasis: basis,
                                    containerNgStyle: {
                                        ...this.state.containerNgStyle,
                                        height: `${basis}px`,
                                    },
                                };

                                if (! this.destroyed) { this.changeDetectionRef.detectChanges(); }
                            }
                        }
                    }

                    if (! this.destroyed) {
                        this.ngZone.runOutsideAngular(() => {
                            window.requestAnimationFrame(() => {
                                nextFrame();
                            });
                        });
                    }
                }
            });
        };

        window.requestAnimationFrame(() => {
            nextFrame();
        });
    }

    ngOnDestroy(): void {
        this.changeDetectionRef.detach();
        this.destroyed = true;
        this.downListeners();
    }

    @HostListener('window:resize.out-zone')
    updateScrollBlockSizeOnWindowResize(): void {
        this.ngZone.runOutsideAngular(() => {
            if (! this.enabled) {
                return;
            }

            this.updateScrollBlockSize();
        });
    }

    @HostListener('document:touchmove.out-zone', ['$event'])
    onScrollBlockTouchMove($event: TouchEvent) {
        this.ngZone.runOutsideAngular(() => {
            if (! this.enabled) {
                return;
            }

            if (this.state.isDragging) {
                $event.preventDefault();
                $event.stopPropagation();

                this.onScrollBlockMove($event.touches[0].pageX, $event.touches[0].pageY);
            }
        });
    }

    @HostListener('document:touchend.out-zone', ['$event'])
    onScrollBlockTouchEnd($event: TouchEvent) {
        this.ngZone.runOutsideAngular(() => {
            if (! this.enabled) {
                return;
            }

            if (this.state.isDragging) {
                $event.stopPropagation();

                this.onScrollBlockEnd();
            }
        });
    }

    @HostListener('document:mousemove.out-zone', ['$event'])
    onScrollBlockMouseMove($event: MouseEvent) {
        this.ngZone.runOutsideAngular(() => {
            if (! this.enabled) {
                return;
            }

            if (this.state.isDragging) {
                $event.stopPropagation();

                this.onScrollBlockMove($event.clientX, $event.clientY);
            }
        });
    }

    @HostListener('document:mouseup.out-zone', ['$event'])
    onScrollBlockMouseUp($event: MouseEvent) {
        this.ngZone.runOutsideAngular(() => {
            if (! this.enabled) {
                return;
            }

            if (this.state.isDragging) {
                $event.stopPropagation();

                this.onScrollBlockEnd();
            }
        });
    }

    get ngClasses(): any {
        return {
            'c-direction-x': this.scrollBarDirection === 'x',
            'c-direction-y': this.scrollBarDirection === 'y',
            'visible': this.state.visible,
            'force-visible': this.state.forceVisible,
            'auto-hide': this.autoHide,
        };
    }

    upListeners(): void {
        this.downListeners();

        if (this.autoHide) {
            this.eventListeners.push(this.elementRef.addEventListener('mouseenter', () => {
                this.ngZone.runOutsideAngular(() => {
                    if (! this.enabled) {
                        return;
                    }

                    if (! this.state.visible) {
                        this.state = {
                            ...this.state,
                            visible: true,
                        };

                        this.cdr.detectChanges();
                    }
                });
            }));

            this.eventListeners.push(this.elementRef.addEventListener('mouseleave', () => {
                this.ngZone.runOutsideAngular(() => {
                    if (! this.enabled) {
                        return;
                    }

                    if (this.state.visible) {
                        this.state = {
                            ...this.state,
                            visible: false,
                        };

                        this.cdr.detectChanges();
                    }
                });
            }));

            this.eventListeners.push(this.elementRef.addEventListener('mousedown', () => {
                this.ngZone.runOutsideAngular(() => {
                    if (! this.enabled) {
                        return;
                    }

                    if (! this.state.visible || ! this.state.forceVisible) {
                        this.state = {
                            ...this.state,
                            visible: true,
                            forceVisible: true,
                        };

                        this.cdr.detectChanges();
                    }
                });
            }));

            this.eventListeners.push(this.elementRef.addEventListener('touchstart', () => {
                this.ngZone.runOutsideAngular(() => {
                    if (! this.enabled) {
                        return;
                    }

                    if (! this.state.visible || ! this.state.forceVisible) {
                        this.state = {
                            ...this.state,
                            visible: true,
                            forceVisible: true,
                        };

                        this.cdr.detectChanges();
                    }
                });
            }));

            this.eventListeners.push(this.elementRef.addEventListener('mouseup', () => {
                this.ngZone.runOutsideAngular(() => {
                    if (! this.enabled) {
                        return;
                    }

                    if (this.state.visible || this.state.forceVisible) {
                        this.state = {
                            ...this.state,
                            visible: false,
                            forceVisible: false,
                        };

                        this.cdr.detectChanges();
                    }
                });
            }));

            this.eventListeners.push(this.elementRef.addEventListener('click', () => {
                this.ngZone.runOutsideAngular(() => {
                    if (! this.state.visible) {
                        this.state = {
                            ...this.state,
                            visible: true,
                        };

                        this.cdr.detectChanges();
                    }
                });
            }));

            this.eventListeners.push(this.elementRef.addEventListener('touchend', () => {
                this.ngZone.runOutsideAngular(() => {
                    if (this.state.visible) {
                        this.state = {
                            ...this.state,
                            visible: false,
                        };

                        this.cdr.detectChanges();
                    }
                });
            }));
        } else {
            this.state = {
                ...this.state,
                visible: true,
                forceVisible: false,
            };
        }
    }

    downListeners(): void {
    }

    updateScrollBlockSize(): void {
        this.ngZone.runOutsideAngular(() => {
            if (this.elementRef) {
                const rect: ClientRect = this.elementRef.getBoundingClientRect();

                const scrollTop: number = this.elementRef.scrollTop;
                const scrollLeft: number = this.elementRef.scrollLeft;
                const scrollWidth: number = this.elementRef.scrollWidth;
                const scrollHeight: number = this.elementRef.scrollHeight;

                if (this.scrollBarDirection === 'x') {
                    const size: number = Math.max(EvodeskScrollBarComponent.MIN_SIZE, (rect.width / scrollWidth) * rect.width);

                    const scrollRange: number = rect.width - size;

                    const minLeft: number = 0;
                    const maxLeft: number = scrollRange;

                    const currentScrolledInPercents: number = percents((100 / (scrollWidth - rect.width)) * bound(scrollLeft, 0, scrollWidth));
                    const cssLeft: number = Math.max(0, (bound(((scrollRange) / 100) * currentScrolledInPercents, minLeft, maxLeft)));

                    if (this.state.scrollBlockNgStyle.width !== `${size}px` || this.state.scrollBlockNgStyle.left !== `${cssLeft}px`) {
                        this.state = {
                            ...this.state,
                            scrollBlockNgStyle: {
                                ...this.state,
                                width: `${size}px`,
                                left: `${cssLeft}px`,
                            },
                        };

                        if (! this.destroyed) { this.changeDetectionRef.detectChanges(); }
                    }
                } else if (this.scrollBarDirection === 'y') {
                    const size: number = Math.max(EvodeskScrollBarComponent.MIN_SIZE, (rect.height / scrollHeight) * rect.height);

                    const scrollRange: number = rect.height - size;

                    const minTop: number = 0;
                    const maxTop: number = scrollRange;

                    const currentScrolledInPercents: number = percents((100 / (scrollHeight - rect.height)) * bound(scrollTop, 0, scrollHeight));
                    const cssTop: number = Math.max(0, (bound(((scrollRange) / 100) * currentScrolledInPercents, minTop, maxTop)));

                    if (this.state.scrollBlockNgStyle.top !== `${cssTop}px` || this.state.scrollBlockNgStyle.height !== `${size}px`) {
                        this.state = {
                            ...this.state,
                            scrollBlockNgStyle: {
                                ...this.state,
                                top: `${cssTop}px`,
                                height: `${size}px`,
                            },
                        };

                        if (! this.destroyed) { this.changeDetectionRef.detectChanges(); }
                    }
                }
            }
        });
    }

    onScrollBlockMouseDown($event: MouseEvent) {
        this.ngZone.runOutsideAngular(() => {
            $event.stopPropagation();

            this.onScrollBlockStart($event.clientX, $event.clientY);
        });
    }

    onScrollBlockTouchStart($event: TouchEvent) {
        $event.preventDefault();
        $event.stopPropagation();

        this.ngZone.runOutsideAngular(() => {
            this.onScrollBlockStart($event.touches[0].pageX, $event.touches[0].pageY);
        });
    }

    onScrollBlockStart(cursorX: number, cursorY: number): void {
        this.ngZone.runOutsideAngular(() => {
            this.state = {
                ...this.state,
                isDragging: true,
                initialX: cursorX,
                initialY: cursorY,
                initialScrollTop: this.elementRef.scrollTop,
                initialScrollLeft: this.elementRef.scrollLeft,
                forceVisible: true,
            };
        });
    }

    onScrollBlockEnd(): void {
        this.ngZone.runOutsideAngular(() => {
            this.state = {
                ...this.state,
                isDragging: false,
                forceVisible: false,
            };
        });
    }

    onScrollBlockMove(cursorX: number, cursorY: number): void {
        this.ngZone.runOutsideAngular(() => {
            const rect: ClientRect = this.elementRef.getBoundingClientRect();

            const deltaX: number = cursorX - this.state.initialX;
            const deltaY: number = cursorY - this.state.initialY;
            const newPosition: ScrollInfo = { x: this.state.initialScrollLeft, y: this.state.initialScrollTop};

            if (this.scrollBarDirection === 'x') {
                const scrollWidth: number = this.elementRef.scrollWidth;

                const q: number = scrollWidth / (rect.width);

                this.elementRef.scrollLeft = this.state.initialScrollLeft + (deltaX * q);
                newPosition.x = this.elementRef.scrollLeft;
            } else if (this.scrollBarDirection === 'y') {
                const scrollHeight: number = this.elementRef.scrollHeight;

                const q: number = scrollHeight / (rect.height);

                this.elementRef.scrollTop = this.state.initialScrollTop + (deltaY * q);
                newPosition.y = this.elementRef.scrollTop;
            }
            this.scrollBlockMove.emit(newPosition);
        });
    }
}
