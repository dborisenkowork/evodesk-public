import {ChangeDetectorRef, Component, DoCheck, OnInit} from '@angular/core';

import {environment} from '../../../../../../environments/environment.config';

let isDisabled: boolean = false;
let isForcedEnabled: boolean = false;

@Component({
    selector: 'evodesk-app-common-dev-ng-do-check-counter',
    templateUrl: './EvodeskNgDoCheckCounter.component.pug',
    styleUrls: [
        './EvodeskNgDoCheckCounter.component.scss',
    ],
})
export class EvodeskNgDoCheckCounterComponent implements DoCheck, OnInit
{
    public isProduction: boolean = environment.angular.enableProductionMode;
    public counter: number = 0;

    constructor(
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnInit(): void {
        if (window) {
            window['evodeskDisableNgCounter'] = () => {
                isDisabled = true;

                this.cdr.detectChanges();
            };

            window['evodeskEnableNgCounter'] = () => {
                isForcedEnabled = true;
                isDisabled = false;

                this.cdr.detectChanges();
            };
        }
    }

    ngDoCheck(): void {
        this.counter++;
    }

    get isEnabled(): boolean {
        return (environment.angular.enableProductionMode && isForcedEnabled) || (! isDisabled);
    }
}
