import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

import {Observable} from 'rxjs';

import {AuthTokenService} from '../../../features/EvodeskAuth/services/AuthToken.service';

@Injectable()
export class JWTHttpInterceptor implements HttpInterceptor
{
    constructor(
        private authToken: AuthTokenService,
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.authToken.has() && ! req.headers.has('Token')) {
            req = req.clone({
                setHeaders: {
                    Token: `${this.authToken.token.access_token}`,
                },
            });
        }

        return next.handle(req);
    }
}
