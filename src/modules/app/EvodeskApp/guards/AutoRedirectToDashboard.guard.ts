
import {of as observableOf, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';



import {AuthTokenService} from '../../../features/EvodeskAuth/services/AuthToken.service';

@Injectable()
export class AutoRedirectToDashboardGuard implements CanActivate
{
    constructor(
        private router: Router,
        private authToken: AuthTokenService,
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.authToken.has() && this.router.url === '/') {
            this.router.navigate(['/profile']);
        }

        return observableOf(true);
    }
}
