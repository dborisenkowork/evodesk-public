import {Component} from '@angular/core';

@Component({
    templateUrl: './NotFoundRoute.component.pug',
    styleUrls: [
        './NotFoundRoute.component.scss',
    ],
})
export class NotFoundRouteComponent
{}
