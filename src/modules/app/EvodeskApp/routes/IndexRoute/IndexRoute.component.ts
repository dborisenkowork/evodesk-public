import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {AuthTokenService} from '../../../../features/EvodeskAuth/services/AuthToken.service';

@Component({
    template: ``,
})
export class IndexRouteComponent implements OnInit
{
    constructor(
        private router: Router,
        private authToken: AuthTokenService,
    ) {}

    ngOnInit(): void {
        if (this.authToken.has()) {
            this.router.navigate(['/profile']);
        } else {
            this.router.navigate(['/landing']);
        }
    }
}
