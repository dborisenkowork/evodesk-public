import {NgModule} from '@angular/core';
import {ServerModule} from '@angular/platform-server';
import {ModuleMapLoaderModule} from '@nguniversal/module-map-ngfactory-loader';

import {EvodeskAppModule} from './EvodeskApp.module';

import {AppRootComponent} from './components/module/AppRoot/AppRoot.component';

@NgModule({
    imports: [
        EvodeskAppModule,
        ServerModule,
        ModuleMapLoaderModule,
    ],
    bootstrap: [
        AppRootComponent,
    ],
})
export class EvodeskAppServerModule
{}
