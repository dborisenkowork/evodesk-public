import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

export enum SocialNetworks
{
    Facebook = <any>'facebook',
    Twitter = <any>'twitter',
    GooglePlus = <any>'google-plus',
    Vk = <any>'vk',
    Ok = <any>'ok',
    MailRu = <any>'mail.ru',
    LinkedIn = <any>'linkedin',
}

export interface ShareWithSocialNetworkRequest {
    name: string;
    description: string;
    image: string;
    link: string;
}

@Injectable()
export class EvodeskShareWithSocialNetworkService
{
    constructor(
        private translate: TranslateService,
    ) {}

    share(withNetwork: SocialNetworks, request: ShareWithSocialNetworkRequest): void {
        this.shareWindowStrategy(withNetwork, request);
    }

    shareWindowStrategy(withNetwork: SocialNetworks, request: ShareWithSocialNetworkRequest): void {
        this.translate.get([
            'EvodeskApp.services.EvodeskShareWithSocialNetworkService.defaultName',
            'EvodeskApp.services.EvodeskShareWithSocialNetworkService.defaultDescription',
        ]).subscribe((messages) => {
            const cRequest: ShareWithSocialNetworkRequest = { ...request };

            if (!cRequest.name || cRequest.name.length === 0) {
                cRequest.name = messages['EvodeskApp.services.EvodeskShareWithSocialNetworkService.defaultName'];
            }

            if (!cRequest.description || cRequest.description.length === 0) {
                cRequest.description = messages['EvodeskApp.services.EvodeskShareWithSocialNetworkService.defaultDescription'];
            }

            switch (withNetwork) {
                case SocialNetworks.Vk: {
                    window.open('http://vk.com/share.php?url=' + encodeURIComponent(cRequest.link + 'vk'), '', `toolbar=0,status=0,width=626,height=436`);

                    break;
                }

                case SocialNetworks.Ok: {
                    window.open('http://ok.ru/dk?st.cmd=addShare&st.s=1&st.comments=' + encodeURIComponent(cRequest.description) + '&st._surl=' + encodeURIComponent(cRequest.link), '', `toolbar=0,status=0,width=626,height=436`);

                    break;
                }

                case SocialNetworks.Facebook: {
                    window.open('http://www.facebook.com/sharer.php?s=100'
                        + '&p[title]=' + encodeURIComponent(cRequest.name)
                        + '&p[summary]=' + encodeURIComponent(cRequest.description)
                        + '&p[url]=' + encodeURIComponent(cRequest.link)
                        + '&p[images][0]=' + encodeURIComponent(cRequest.image), '', `toolbar=0,status=0,width=626,height=436`);

                    break;
                }

                case SocialNetworks.Twitter: {
                    window.open('http://twitter.com/share?'
                        + 'text=' + encodeURIComponent(cRequest.name)
                        + '&url=' + encodeURIComponent(cRequest.link)
                        + '&counturl=' + encodeURIComponent(cRequest.link), '', `toolbar=0,status=0,width=626,height=436`);

                    break;
                }

                case SocialNetworks.MailRu: {
                    window.open('http://connect.mail.ru/share?'
                        + 'url=' + encodeURIComponent(cRequest.link)
                        + '&title=' + encodeURIComponent(cRequest.name)
                        + '&description=' + encodeURIComponent(cRequest.description)
                        + '&imageurl='    + encodeURIComponent(cRequest.image), '', `toolbar=0,status=0,width=626,height=800`);

                    break;
                }

                case SocialNetworks.GooglePlus: {
                    window.open('http://plus.google.com/share?url=' + encodeURIComponent(cRequest.link), '', `toolbar=0,status=0,width=626,height=800`);

                    break;
                }

                case SocialNetworks.LinkedIn: {
                    window.open('https://www.linkedin.com/shareArticle?'
                        + 'url=' + encodeURIComponent(cRequest.link)
                        + '&title=' + encodeURIComponent(cRequest.name)
                        + '&share_to=http%3A%2F%2Fwww.linkedin.com%2FshareArticle', '', `toolbar=0,status=0,width=626,height=436`);

                    break;
                }
            }
        });
    }
}
