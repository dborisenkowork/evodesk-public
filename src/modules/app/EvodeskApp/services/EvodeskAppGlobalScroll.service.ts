import {Injectable} from '@angular/core';

import {Observable, Subject} from 'rxjs';

export interface EvodeskAppGlobalScrollEvent {
    areaHeight: number;
    scrollTop: number;
    scrollHeight: number;
}

@Injectable()
export class EvodeskAppGlobalScrollService
{
    private _scroll$: Subject<EvodeskAppGlobalScrollEvent> = new Subject<EvodeskAppGlobalScrollEvent>();

    public get scroll$(): Observable<EvodeskAppGlobalScrollEvent> {
        return this._scroll$.asObservable();
    }

    public emit($event: Event): void {
        const element: HTMLElement = <any>$event.target;

        this._scroll$.next({
            areaHeight: element.clientHeight,
            scrollTop: element.scrollTop,
            scrollHeight: element.scrollHeight,
        });
    }
}
