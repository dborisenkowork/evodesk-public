import {Injectable, NgZone} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {Observable, Subject, interval} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import * as moment from 'moment';
import {sprintf} from 'sprintf-js';
import {Moment} from 'moment';

export interface EvodeskDateDiffFromNowOptions {
    updateInMs?: number;
    futureAsNow?: boolean;
}

const defaultDateDiffFromNowOptions: EvodeskDateDiffFromNowOptions = {
    updateInMs: 1000,
    futureAsNow: false,
};

function getMinutesDiff(a: Moment, b: Moment) {
    const diffA: Moment = moment(new Date(a.toDate().getTime()));
    const diffB: Moment = moment(new Date(b.toDate().getTime()));

    [diffA, diffB].forEach((m) => {
        m.milliseconds(0);
    });

    return diffA.diff(diffB, 'minutes');
}

function getDayDiff(a: Moment, b: Moment) {
    const diffA: Moment = moment(new Date(a.toDate().getTime()));
    const diffB: Moment = moment(new Date(b.toDate().getTime()));

    [diffA, diffB].forEach((m) => {
        m.hours(0).minutes(0).seconds(0).milliseconds(0);
    });

    return diffA.diff(diffB, 'days');
}

function getYearDiff(a: Moment, b: Moment) {
    const diffA: Moment = moment(new Date(a.toDate().getTime()));
    const diffB: Moment = moment(new Date(b.toDate().getTime()));

    [diffA, diffB].forEach((m) => {
        m.month(1).days(1).hours(0).minutes(0).seconds(0).milliseconds(0);
    });

    return diffA.diff(diffB, 'years');
}

/**
 * [!] THIS SERVICE IS ALLOWED TO BE USED WITHIN VIEW-COMPONENTS.
 */

@Injectable()
export class EvodeskDateService
{
    private _now: Date;
    private _localNow: Date;

    constructor(
        private ngZone: NgZone,
        private translate: TranslateService,
    ) { this.now = new Date(); }

    set now(fromNow: Date) {
        this._now = fromNow;
        this._localNow = new Date();
    }

    get now(): Date {
        const diff: number = (new Date).getTime() - this._localNow.getTime();

        return new Date(this._now.getTime() + diff);
    }

    diffFromNowV1(input: Date, withOptions: EvodeskDateDiffFromNowOptions = {}): Observable<string> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((next) => {
            const options: EvodeskDateDiffFromNowOptions = {
                ...defaultDateDiffFromNowOptions,
                ...withOptions,
            };

            const pass: Function = (date: string) => {
                next.next(date);
            };

            const translate: Function = (translateTo: string, replaces: Array<string>) => {
                this.translate.get(translateTo).pipe(takeUntil(unsubscribe$)).subscribe((translated) => {
                    pass(sprintf(translated, ...replaces));
                });
            };

            const tick: Function = () => {
                const now: Moment = moment(this.now);
                const diffWith: Moment = moment(input);

                if (now.diff(diffWith) < 0 && options.futureAsNow) {
                    translate('EvodeskApp.services.EvodeskDateService.diffFromNowV1.Today', moment(input).format('HH:mm'));
                } else if (now.diff(diffWith) > 0) {
                    const diffDays: number = getDayDiff(now, diffWith);
                    const diffYears: number = getYearDiff(now, diffWith);

                    if (diffYears === 0) {
                        if (diffDays === 0) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV1.Today');
                        } else if (diffDays === 1) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV1.Yesterday');
                        } else if (diffDays === 2) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV1.DayBeforeYesterday');
                        } else {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV1.SameYear', [moment(input).format('Do MMM'), moment(input).format('HH:mm')]);
                        }
                    } else {
                        pass(moment(input).format('LLL'));
                    }
                } else {
                    pass(moment(input).format('LLL'));
                }
            };

            this.ngZone.runOutsideAngular(() => {
                interval(options.updateInMs).pipe(takeUntil(unsubscribe$)).subscribe(() => {
                    this.ngZone.runOutsideAngular(() => {
                        tick();
                    });
                });

                tick();
            });

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    diffFromNowV2(input: Date, withOptions: EvodeskDateDiffFromNowOptions = {}): Observable<string> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((next) => {
            const options: EvodeskDateDiffFromNowOptions = {
                ...defaultDateDiffFromNowOptions,
                ...withOptions,
            };

            const pass: Function = (date: string) => {
                next.next(date);
            };

            const translate: Function = (translateTo: string, replaces: Array<string>) => {
                this.translate.get(translateTo).pipe(takeUntil(unsubscribe$)).subscribe((translated) => {
                    pass(sprintf(translated, ...replaces));
                });
            };

            const tick: Function = () => {
                const now: Moment = moment(this.now);
                const diffWith: Moment = moment(input);

                if (now.diff(diffWith) < 0 && options.futureAsNow) {
                    translate('EvodeskApp.services.EvodeskDateService.diffFromNowV2.SameYear', moment(input).format('HH:mm'));
                } else if (now.diff(diffWith) > 0) {
                    const diffYears: number = getYearDiff(now, diffWith);

                    if (diffYears === 0) {
                        translate('EvodeskApp.services.EvodeskDateService.diffFromNowV2.SameYear', [moment(input).format('Do MMM'), moment(input).format('HH:mm')]);
                    } else {
                        pass(moment(input).format('LLL'));
                    }
                } else {
                    pass(moment(input).format('LLL'));
                }
            };

            this.ngZone.runOutsideAngular(() => {
                interval(options.updateInMs).pipe(takeUntil(unsubscribe$)).subscribe(() => {
                    this.ngZone.runOutsideAngular(() => {
                        tick();
                    });
                });

                tick();
            });

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    diffFromNowV3(input: Date, withOptions: EvodeskDateDiffFromNowOptions = {}): Observable<string> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((next) => {
            const options: EvodeskDateDiffFromNowOptions = {
                ...defaultDateDiffFromNowOptions,
                ...withOptions,
            };

            const pass: Function = (date: string) => {
                next.next(date);
            };

            const translate: Function = (translateTo: string, replaces: Array<string>) => {
                this.translate.get(translateTo).pipe(takeUntil(unsubscribe$)).subscribe((translated) => {
                    pass(sprintf(translated, ...replaces));
                });
            };

            const tick: Function = () => {
                const now: Moment = moment(this.now);
                const diffWith: Moment = moment(input);

                if (now.diff(diffWith) < 0 && options.futureAsNow) {
                    translate('EvodeskApp.services.EvodeskDateService.diffFromNowV3.Now', moment(input).format('HH:mm'));
                } else if (now.diff(diffWith) > 0) {
                    const diffMinutes: number = getMinutesDiff(now, diffWith);
                    const diffDays: number = getDayDiff(now, diffWith);
                    const diffYears: number = getYearDiff(now, diffWith);

                    if (diffYears === 0) {
                        if (diffDays === 0) {
                            if (diffMinutes < 5) {
                                translate('EvodeskApp.services.EvodeskDateService.diffFromNowV3.Now', moment(input).format('HH:mm'));
                            } else if (diffMinutes >= 5 && diffMinutes < 10) {
                                translate('EvodeskApp.services.EvodeskDateService.diffFromNowV3.Min5Ago', moment(input).format('HH:mm'));
                            } else if (diffMinutes >= 10 && diffMinutes < 15) {
                                translate('EvodeskApp.services.EvodeskDateService.diffFromNowV3.Min10Ago', moment(input).format('HH:mm'));
                            } else if (diffMinutes >= 15 && diffMinutes < 20) {
                                translate('EvodeskApp.services.EvodeskDateService.diffFromNowV3.Min15Ago', moment(input).format('HH:mm'));
                            } else {
                                translate('EvodeskApp.services.EvodeskDateService.diffFromNowV3.Today', moment(input).format('HH:mm'));
                            }
                        } else if (diffDays === 1) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV3.Yesterday', moment(input).format('HH:mm'));
                        } else if (diffDays === 2) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV3.DayBeforeYesterday', moment(input).format('HH:mm'));
                        } else {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV3.SameYear', [moment(input).format('LL'), moment(input).format('HH:mm')]);
                        }
                    } else {
                        pass(moment(input).format('LLL'));
                    }
                } else {
                    pass(moment(input).format('LLL'));
                }
            };

            this.ngZone.runOutsideAngular(() => {
                interval(options.updateInMs).pipe(takeUntil(unsubscribe$)).subscribe(() => {
                    this.ngZone.runOutsideAngular(() => {
                        tick();
                    });
                });

                tick();
            });

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    diffFromNowV4(input: Date, withOptions: EvodeskDateDiffFromNowOptions = {}): Observable<string> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((next) => {
            const options: EvodeskDateDiffFromNowOptions = {
                ...defaultDateDiffFromNowOptions,
                ...withOptions,
            };

            const pass: Function = (date: string) => {
                next.next(date);
            };

            const translate: Function = (translateTo: string, replaces: Array<string>) => {
                this.translate.get(translateTo).pipe(takeUntil(unsubscribe$)).subscribe((translated) => {
                    pass(sprintf(translated, ...replaces));
                });
            };

            const tick: Function = () => {
                const now: Moment = moment(this.now);
                const diffWith: Moment = moment(input);

                if (now.diff(diffWith) < 0 && options.futureAsNow) {
                    translate('EvodeskApp.services.EvodeskDateService.diffFromNowV4.Now', moment(input).format('HH:mm'));
                } else if (now.diff(diffWith) > 0) {
                    const diffMinutes: number = getMinutesDiff(now, diffWith);
                    const diffDays: number = getDayDiff(now, diffWith);
                    const diffYears: number = getYearDiff(now, diffWith);

                    if (diffYears === 0) {
                        if (diffDays === 0) {
                            if (diffMinutes < 5) {
                                translate('EvodeskApp.services.EvodeskDateService.diffFromNowV4.Now', moment(input).format('HH:mm'));
                            } else if (diffMinutes >= 5 && diffMinutes < 10) {
                                translate('EvodeskApp.services.EvodeskDateService.diffFromNowV4.Min5Ago', moment(input).format('HH:mm'));
                            } else if (diffMinutes >= 10 && diffMinutes < 15) {
                                translate('EvodeskApp.services.EvodeskDateService.diffFromNowV4.Min10Ago', moment(input).format('HH:mm'));
                            } else if (diffMinutes >= 15 && diffMinutes < 20) {
                                translate('EvodeskApp.services.EvodeskDateService.diffFromNowV4.Min15Ago', moment(input).format('HH:mm'));
                            } else {
                                translate('EvodeskApp.services.EvodeskDateService.diffFromNowV4.Today', moment(input).format('HH:mm'));
                            }
                        } else if (diffDays === 1) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV4.Yesterday', moment(input).format('HH:mm'));
                        } else if (diffDays === 2) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV4.DayBeforeYesterday', moment(input).format('HH:mm'));
                        } else {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV4.SameYear', [moment(input).format('Do MMM')]);
                        }
                    } else {
                        pass(moment(input).format('LLL'));
                    }
                } else {
                    pass(moment(input).format('LLL'));
                }
            };

            this.ngZone.runOutsideAngular(() => {
                interval(options.updateInMs).pipe(takeUntil(unsubscribe$)).subscribe(() => {
                    this.ngZone.runOutsideAngular(() => {
                        tick();
                    });
                });

                tick();
            });

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    diffFromNowV5(input: Date, withOptions: EvodeskDateDiffFromNowOptions = {}): Observable<string> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((next) => {
            const options: EvodeskDateDiffFromNowOptions = {
                ...defaultDateDiffFromNowOptions,
                ...withOptions,
            };

            const pass: Function = (date: string) => {
                next.next(date);
            };

            const translate: Function = (translateTo: string, replaces: Array<string>) => {
                this.translate.get(translateTo).pipe(takeUntil(unsubscribe$)).subscribe((translated) => {
                    pass(sprintf(translated, ...replaces));
                });
            };

            const tick: Function = () => {
                const now: Moment = moment(this.now);
                const diffWith: Moment = moment(input);

                if (now.diff(diffWith) < 0 && options.futureAsNow) {
                    translate('EvodeskApp.services.EvodeskDateService.diffFromNowV5.Today', moment(input).format('HH:mm'));
                } else if (now.diff(diffWith) > 0) {
                    const diffDays: number = getDayDiff(now, diffWith);
                    const diffYears: number = getYearDiff(now, diffWith);

                    if (diffYears === 0) {
                        if (diffDays === 0) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV5.Today');
                        } else if (diffDays === 1) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV5.Yesterday');
                        } else if (diffDays === 2) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV5.DayBeforeYesterday');
                        } else {
                            pass(moment(input).format('LL'));
                        }
                    } else {
                        pass(moment(input).format('LL'));
                    }
                } else {
                    pass(moment(input).format('LL'));
                }
            };

            this.ngZone.runOutsideAngular(() => {
                interval(options.updateInMs).pipe(takeUntil(unsubscribe$)).subscribe(() => {
                    this.ngZone.runOutsideAngular(() => {
                        tick();
                    });
                });

                tick();
            });

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    diffFromNowV6(input: Date, withOptions: EvodeskDateDiffFromNowOptions = {}): Observable<string> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((next) => {
            const options: EvodeskDateDiffFromNowOptions = {
                ...defaultDateDiffFromNowOptions,
                ...withOptions,
            };

            const pass: Function = (date: string) => {
                next.next(date);
            };

            const translate: Function = (translateTo: string, replaces: Array<string>) => {
                this.translate.get(translateTo).pipe(takeUntil(unsubscribe$)).subscribe((translated) => {
                    pass(sprintf(translated, ...replaces));
                });
            };

            const tick: Function = () => {
                const now: Moment = moment(this.now);
                const diffWith: Moment = moment(input);

                if (now.diff(diffWith) < 0 && options.futureAsNow) {
                    translate('EvodeskApp.services.EvodeskDateService.diffFromNowV5.Today', moment(input).format('HH:mm'));
                } else if (now.diff(diffWith) > 0) {
                    const diffDays: number = getDayDiff(now, diffWith);
                    const diffYears: number = getYearDiff(now, diffWith);

                    if (diffYears === 0) {
                        if (diffDays === 0) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV5.Today');
                        } else if (diffDays === 1) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV5.Yesterday');
                        } else if (diffDays === 2) {
                            translate('EvodeskApp.services.EvodeskDateService.diffFromNowV5.DayBeforeYesterday');
                        } else {
                            pass(moment(input).format('Do MMMM'));
                        }
                    } else {
                        pass(moment(input).format('LL'));
                    }
                } else {
                    pass(moment(input).format('LL'));
                }
            };

            this.ngZone.runOutsideAngular(() => {
                interval(options.updateInMs).pipe(takeUntil(unsubscribe$)).subscribe(() => {
                    this.ngZone.runOutsideAngular(() => {
                        tick();
                    });
                });

                tick();
            });

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
