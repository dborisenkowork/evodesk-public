import {Injectable} from '@angular/core';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Injectable()
export class EvodeskMatIconsCardService {
    constructor(
        private iconRegistry: MatIconRegistry,
        private sanitizer: DomSanitizer,
    ) {}

    init() {
        this.iconRegistry.addSvgIcon('ic-cross', this.sanitizer.bypassSecurityTrustResourceUrl('assets//components/evodesk-sprite-icons/sprite/ic-cross.svg'));
        this.iconRegistry.addSvgIcon('ic-plus-blue', this.sanitizer.bypassSecurityTrustResourceUrl('assets//components/evodesk-sprite-icons/sprite/ic-plus-blue.svg'));
    }
}
