import {inject, TestBed} from '@angular/core/testing';
import {NgZone} from '@angular/core';

import {TranslateService} from '@ngx-translate/core';

import {Subject, Observable} from 'rxjs';
import {take, takeUntil} from 'rxjs/operators';

import * as moment from 'moment';

import {EvodeskDateService} from './EvodeskDate.service';

const translateFixture: { [key: string]: string } = {
    'EvodeskApp.services.EvodeskDateService.diffFromNowV3.Now': 'Now',
    'EvodeskApp.services.EvodeskDateService.diffFromNowV3.Min5Ago': '5 minutes ago',
    'EvodeskApp.services.EvodeskDateService.diffFromNowV3.Min10Ago': '10 minutes ago',
    'EvodeskApp.services.EvodeskDateService.diffFromNowV3.Min15Ago': '15 minutes ago',
    'EvodeskApp.services.EvodeskDateService.diffFromNowV3.Today': 'Today at %s',
    'EvodeskApp.services.EvodeskDateService.diffFromNowV3.Yesterday': 'Yesterday at %s',
    'EvodeskApp.services.EvodeskDateService.diffFromNowV3.DayBeforeYesterday': 'Two days ago at %s',
    'EvodeskApp.services.EvodeskDateService.diffFromNowV3.SameYear': '%s at %s',
};

export class MockNgZone extends NgZone {
    constructor() {
        super({ enableLongStackTrace: false });
    }
}

class TranslateServiceMock
{
    // noinspection JSUnusedGlobalSymbols
    get(values: string | Array<string>): Observable<string | Array<string>> {
        return Observable.create(observer => {
            setTimeout(() => {
                if (Array.isArray(values)) {
                    observer.next(values.map(v => translateFixture[v] || v));
                    observer.complete();
                } else {
                    observer.next(translateFixture[values] || values);
                    observer.complete();
                }
            });
        });
    }
}

describe('EvodeskApp/services/EvodeskDateService', () => {
    const afterEach$: Subject<void> = new Subject<void>();


    beforeEach(() => {
        moment.locale('en');

        TestBed.configureTestingModule({
            providers: [
                { provide: NgZone, useClass: MockNgZone },
                {
                    provide: EvodeskDateService,
                    useFactory: (ngZone: NgZone, translate: TranslateService) => {
                        return new EvodeskDateService(ngZone, translate);
                    },
                    deps: [
                        NgZone,
                        TranslateService,
                    ],
                },
                {
                    provide: TranslateService,
                    useValue: new TranslateServiceMock(),
                },
            ],
        });

        (TestBed.get(EvodeskDateService) as EvodeskDateService).now = moment('10.01.2018 1:30:00', 'DD.MM.YYYY HH:mm:ss').toDate();

        return TestBed.compileComponents();
    });

    afterEach(() => {
        afterEach$.next(undefined);
    });

    it ('displays now (0 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:30:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('Now');
                done();
            });
        })();
    });

    it ('displays now (1 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:29:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('Now');
                done();
            });
        })();
    });

    it ('displays now (4 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:26:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('Now');
                done();
            });
        })();
    });

    it ('displays now (4 minutes 59 seconds diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:25:01', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('Now');
                done();
            });
        })();
    });

    it ('displays now (5 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:25:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('5 minutes ago');
                done();
            });
        })();
    });

    it ('displays 5 minutes ago (5 minutes 1 seconds diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:24:59', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('5 minutes ago');
                done();
            });
        })();
    });

    it ('displays 5 minutes ago (9:59 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:20:01', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('5 minutes ago');
                done();
            });
        })();
    });

    it ('displays 5 minutes ago (10:00 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:20:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('10 minutes ago');
                done();
            });
        })();
    });

    it ('displays 10 minutes ago (10:01 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:19:59', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('10 minutes ago');
                done();
            });
        })();
    });

    it ('displays 10 minutes ago (14:59 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:15:01', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('10 minutes ago');
                done();
            });
        })();
    });

    it ('displays 15 minutes ago (15:00 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:15:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('15 minutes ago');
                done();
            });
        })();
    });

    it ('displays 15 minutes ago (15:01 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:14:59', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('15 minutes ago');
                done();
            });
        })();
    });

    it ('displays 15 minutes ago (19:59 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:10:01', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('15 minutes ago');
                done();
            });
        })();
    });

    it ('displays today (20 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 01:10:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('Today at 01:10');
                done();
            });
        })();
    });

    it ('displays today (60 minutes diff)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 00:30:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('Today at 00:30');
                done();
            });
        })();
    });

    it ('displays today (00:00:01)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('10.01.2018 00:00:01', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('Today at 00:00');
                done();
            });
        })();
    });

    it ('displays yesterday (yesterday, 01:00)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('09.01.2018 00:01:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('Yesterday at 00:01');
                done();
            });
        })();
    });

    it ('displays yesterday (yesterday, 23:59)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('09.01.2018 23:59:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('Yesterday at 23:59');
                done();
            });
        })();
    });

    it ('displays yesterday (two days ago, 01:00)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('08.01.2018 00:01:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('Two days ago at 00:01');
                done();
            });
        })();
    });

    it ('displays same year (two days ago, 02.01.2018)', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('02.01.2018 00:01:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('January 2, 2018 at 00:01');
                done();
            });
        })();
    });

    it ('displays future', (done) => {
        inject([EvodeskDateService], (service: EvodeskDateService) => {
            service.diffFromNowV3(moment('11.01.2018 00:01:00', 'DD.MM.YYYY HH:mm:ss').toDate()).pipe(takeUntil(afterEach$), take(1)).subscribe((next) => {
                expect(next).toBe('January 11, 2018 12:01 AM');
                done();
            });
        })();
    });
});
