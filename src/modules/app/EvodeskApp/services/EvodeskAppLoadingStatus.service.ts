import {Injectable} from '@angular/core';

import {randomSID} from '../functions/random-sid.function';
import {BehaviorSubject, Observable} from 'rxjs';

export interface EvodeskAppLoading {
    done(): void;
}

type SID = string;

@Injectable()
export class EvodeskAppLoadingStatusService
{
    private _isLoading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    private _loadings: Array<SID> = [];

    get isLoading$(): Observable<boolean> {
        return <any>this._isLoading$.asObservable();
    }

    addLoading(): EvodeskAppLoading {
        const sid: SID = randomSID(32);

        this._loadings.push(sid);
        this.emit();

        return {
            done: () => {
                this._loadings = this._loadings.filter(s => s !== sid);
                this.emit();
            },
        };
    }

    clearAllLoadings(): void {
        this._loadings = [];
        this.emit();
    }

    emit(): void {
        this._isLoading$.next(this._loadings.length > 0);
    }
}
