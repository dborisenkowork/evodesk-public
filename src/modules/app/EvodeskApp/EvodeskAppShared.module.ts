import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EventManager} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {Platform, PlatformModule} from '@angular/cdk/platform';
import {DragDropModule} from '@angular/cdk/drag-drop';

import {DateAdapter, MatSelectModule, MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule, MatProgressBarModule, MatRippleModule, MatTabsModule, MatProgressSpinnerModule, MAT_DATE_LOCALE, MatTooltipModule} from '@angular/material';

import {StoreModule} from '@ngrx/store';
import {TranslateModule} from '@ngx-translate/core';
import {ClickOutsideModule} from 'ng-click-outside';
import {ColorPickerModule} from 'ngx-color-picker';
import {SWIPER_CONFIG, SwiperModule} from 'ngx-swiper-wrapper';
import {TextMaskModule} from 'angular2-text-mask';
import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';
import {AutosizeModule} from 'ngx-autosize';

import {EvodeskAppStateModule} from '../EvodeskAppState/EvodeskAppState.module';

import {EvodeskAppStateService} from '../EvodeskAppState/services/EvodeskAppState.service';

import {EvodeskEdgeScrollDirective} from './directives/EvodeskEdgeScroll.directive';
import {EvodeskMouseWheelScrollDirective} from './directives/EvodeskMouseWheelScroll.directive';
import {EvodeskTouchScrollDirective} from './directives/EvodeskTouchScroll.directive';
import {EvodeskKeyboardScrollDirective} from './directives/EvodeskKeyboardScroll.directive';
import {EvodeskBindDirective} from './directives/EvodeskBind.directive';
import {EvodeskFilesDropAreaDirective} from './directives/EvodeskFilesDropArea.directive';

import {EvodeskEventManager} from './angular/event-manager/Evodesk.event-manager';

import {EvodeskAppSubMenuComponent} from './components/shared/EvodeskAppSubMenu/EvodeskAppSubMenu.component';
import {EvodeskConfirmModalComponent} from './components/shared/EvodeskConfirmModal/EvodeskConfirmModal.component';
import {EvodeskScrollBarComponent} from './components/shared/EvodeskScrollBar/EvodeskScrollBar.component';
import {EvodeskNgDoCheckCounterComponent} from './components/shared-dev/EvodeskNgDoCheckCounter/EvodeskNgDoCheckCounter.component';
import {EvodeskRatioBoxComponent} from './components/shared/EvodeskRatioBox/EvodeskRatioBox.component';
import {EvodeskInputModalComponent} from './components/shared/EvodeskInputModal/EvodeskInputModal.component';

import {HasAuthTokenGuard} from '../../features/EvodeskAuth/guards/HasAuthToken.guard';
import {CurrentUserGuard} from '../../features/EvodeskAuth/guards/CurrentUser.guard';
import {AutoRedirectToDashboardGuard} from './guards/AutoRedirectToDashboard.guard';
import {EvodeskSetCommonHeaderGuard} from '../../features/EvodeskHeader/guards/EvodeskSetCommonHeader.guard';

import {EvodeskAlertModalComponent} from './components/shared/EvodeskAlertModal/EvodeskAlertModal.component';
import {EvodeskProfileAvatarComponent} from '../../features/EvodeskProfile/shared/components/EvodeskProfileAvatar/EvodeskProfileAvatar.component';
import {EvodeskProfileSubscribeButtonContainerComponent} from '../../features/EvodeskProfile/shared/components/EvodeskProfileSubscribeButton/EvodeskProfileSubscribeButtonContainer.component';
import {EvodeskProfileSubscribeButtonComponent} from '../../features/EvodeskProfile/shared/components/EvodeskProfileSubscribeButton/EvodeskProfileSubscribeButton.component';
import {EvodeskProfileAvatarCurrentUserComponent} from '../../features/EvodeskProfile/shared/components/EvodeskProfileAvatar/EvodeskProfileAvatarCurrentUser.component';

import {IMStartDialogContainerComponent} from '../../features/EvodeskIM/shared/components/IMStartDialog/IMStartDialogContainer.component';
import {IMStartDialogComponent} from '../../features/EvodeskIM/shared/components/IMStartDialog/IMStartDialog.component';

import {AuthTokenService} from '../../features/EvodeskAuth/services/AuthToken.service';
import {CurrentUserService} from '../../features/EvodeskAuth/services/CurrentUser.service';
import {EvodeskAppMessageBusService} from '../EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';
import {EvodeskAlertModalService} from './components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {EvodeskConfirmModalService} from './components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {EvodeskHeaderStateService} from '../../features/EvodeskHeader/services/EvodeskHeaderState.service';
import {SignOutService} from '../../features/EvodeskAuth/services/SignOut.service';
import {EvodeskBillingBalanceService} from '../../features/EvodeskBilling/services/EvodeskBillingBalance.service';
import {EvodeskBillingStateService} from '../../features/EvodeskBilling/state/EvodeskBillingState.service';
import {EvodeskEvoChecksService} from '../../features/EvodeskBilling/services/EvodeskEvoChecks.service';
import {CustomDateAdapter} from './services/CustomDateAdapter.service';
import {EvodeskDateService} from './services/EvodeskDate.service';
import {EvodeskProfileRepositoryService} from '../../features/EvodeskProfile/services/EvodeskProfileRepository.service';
import {EvodeskIMContextService} from '../../features/EvodeskIM/state/EvodeskIMContext.service';
import {EvodeskNewsContextService} from '../../features/EvodeskNews/services/EvodeskNewsContext.service';
import {EvodeskMatIconsCardService} from './services/EvodeskMatIconsCard.service';
import {EvodeskProfileUrlService} from '../../features/EvodeskProfile/shared/services/EvodeskProfileUrl.service';
import {EvodeskAppGlobalScrollService} from './services/EvodeskAppGlobalScroll.service';
import {EvodeskShareWithSocialNetworkService} from './services/EvodeskShareWithSocialNetworks.service';
import {EvodeskHeaderConfigurationService} from '../../features/EvodeskHeader/services/EvodeskHeaderConfiguration.service';
import {EvodeskInputModalService} from './components/shared/EvodeskInputModal/EvodeskInputModal.service';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule,
        PlatformModule,

        StoreModule.forRoot({}),
        TranslateModule.forChild({}),
        ScrollToModule.forRoot(),
        ClickOutsideModule,
        ColorPickerModule,
        SwiperModule,
        TextMaskModule,

        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatListModule,
        MatButtonModule,
        MatTabsModule,
        MatMenuModule,
        MatDialogModule,
        MatCheckboxModule,
        MatIconModule,
        MatProgressBarModule,
        MatRippleModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        MatTooltipModule,

        EvodeskAppStateModule,
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        PlatformModule,

        StoreModule,
        TranslateModule,
        ScrollToModule,
        ClickOutsideModule,
        ColorPickerModule,
        SwiperModule,
        TextMaskModule,
        AutosizeModule,

        EvodeskAppStateModule,

        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatListModule,
        MatButtonModule,
        MatTabsModule,
        MatMenuModule,
        MatDialogModule,
        MatCheckboxModule,
        MatIconModule,
        MatProgressBarModule,
        MatRippleModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        DragDropModule,

        EvodeskEdgeScrollDirective,
        EvodeskMouseWheelScrollDirective,
        EvodeskTouchScrollDirective,
        EvodeskKeyboardScrollDirective,
        EvodeskBindDirective,
        EvodeskFilesDropAreaDirective,

        EvodeskAppSubMenuComponent,
        EvodeskConfirmModalComponent,
        EvodeskScrollBarComponent,
        EvodeskNgDoCheckCounterComponent,
        EvodeskRatioBoxComponent,

        EvodeskProfileSubscribeButtonComponent,
        EvodeskProfileSubscribeButtonContainerComponent,
        EvodeskProfileAvatarComponent,
        EvodeskProfileAvatarCurrentUserComponent,

        IMStartDialogComponent,
        IMStartDialogContainerComponent,
    ],
    declarations: [
        EvodeskEdgeScrollDirective,
        EvodeskMouseWheelScrollDirective,
        EvodeskTouchScrollDirective,
        EvodeskKeyboardScrollDirective,
        EvodeskBindDirective,
        EvodeskFilesDropAreaDirective,

        EvodeskAppSubMenuComponent,
        EvodeskConfirmModalComponent,
        EvodeskScrollBarComponent,
        EvodeskNgDoCheckCounterComponent,
        EvodeskAlertModalComponent,
        EvodeskRatioBoxComponent,
        EvodeskInputModalComponent,

        EvodeskProfileSubscribeButtonComponent,
        EvodeskProfileSubscribeButtonContainerComponent,
        EvodeskProfileAvatarComponent,
        EvodeskProfileAvatarCurrentUserComponent,

        IMStartDialogComponent,
        IMStartDialogContainerComponent,
    ],
    entryComponents: [
        EvodeskAlertModalComponent,
        EvodeskConfirmModalComponent,
        EvodeskInputModalComponent,
        IMStartDialogContainerComponent,
    ],
    providers: [
        {
            provide: DateAdapter,
            useClass: CustomDateAdapter,
            deps: [MAT_DATE_LOCALE, Platform],
        },
        {
            provide: MAT_DATE_LOCALE, useValue: 'ru-RU',
        },
        {
            provide: SWIPER_CONFIG,
            useValue: {
                direction: 'horizontal',
                slidesPerView: 'auto',
            },
        },
        {
            provide: EventManager,
            useClass: EvodeskEventManager,
        },
    ],
})
export class EvodeskAppSharedModule
{
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: EvodeskAppSharedModule,
            providers: [
                EvodeskAppStateService,
                EvodeskAppMessageBusService,
                EvodeskDateService,
                EvodeskMatIconsCardService,
                EvodeskAppGlobalScrollService,
                EvodeskShareWithSocialNetworkService,

                AuthTokenService,
                CurrentUserService,
                SignOutService,

                EvodeskAlertModalService,
                EvodeskConfirmModalService,
                EvodeskInputModalService,

                EvodeskHeaderStateService,
                EvodeskHeaderConfigurationService,

                EvodeskBillingBalanceService,
                EvodeskBillingStateService,
                EvodeskEvoChecksService,

                HasAuthTokenGuard,
                CurrentUserGuard,
                AutoRedirectToDashboardGuard,
                EvodeskSetCommonHeaderGuard,

                EvodeskProfileRepositoryService,
                EvodeskIMContextService,
                EvodeskNewsContextService,

                EvodeskProfileUrlService,
            ],
        };
    }
}
