import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
    selector: '[evodeskTouchScroll]',
})
export class EvodeskTouchScrollDirective
{
    @Input('evodeskTouchScroll') direction: 'x' | 'y' = 'x';
    @Input('evodeskTouchPreventDefaults') preventDefault: boolean = true;
    @Input('evodeskTouchScrollEnabled') enabled: boolean = true;
    @Input('evodeskTouchScrollDisableScrollPass') disableScrollPass: boolean = true;

    private initX: number;
    private initY: number;
    private initScrollLeft: number;
    private initScrollTop: number;

    constructor(
        private el: ElementRef,
    ) {}

    @HostListener('touchstart.out-zone', ['$event'])
    onTouchStart(event: TouchEvent): void {
        if (! this.enabled) {
            return;
        }

        this.initX = event.touches[0].pageX;
        this.initY = event.touches[0].pageY;
        this.initScrollLeft = (this.el.nativeElement as HTMLElement).scrollLeft;
        this.initScrollTop = (this.el.nativeElement as HTMLElement).scrollTop;
    }

    @HostListener('touchmove.out-zone', ['$event'])
    onTouchMove(event: TouchEvent): void {
        if (! this.enabled) {
            return;
        }

        if (this.disableScrollPass) {
            event.preventDefault();
            event.stopImmediatePropagation();
        }

        const elem: HTMLElement = this.el.nativeElement as HTMLElement;

        const cursorX: number = event.touches[0].pageX;
        const cursorY: number = event.touches[0].pageY;

        if (this.direction === 'x') {
            const diffX: number = cursorX - this.initX;

            const isScrolledToStart: boolean = elem.scrollLeft === 0;
            const isScrolledToEnd: boolean = elem.offsetWidth + elem.scrollLeft === elem.scrollWidth;

            if (diffX === 0) {
                return;
            } else if (diffX > 0 && isScrolledToStart) {
                return;
            } else if (diffX < 0 && isScrolledToEnd) {
                return;
            } else if (elem.scrollWidth > elem.clientWidth) {
                if (this.preventDefault && ! this.disableScrollPass) {
                    event.preventDefault();
                }

                elem.scrollLeft = Math.max(0, this.initScrollLeft - diffX);
            }
        } else if (this.direction === 'y') {
            const diffY: number = cursorY - this.initY;

            const isScrolledToStart: boolean = elem.scrollTop === 0;
            const isScrolledToEnd: boolean = elem.offsetHeight + elem.scrollTop === elem.scrollHeight;

            if (diffY === 0) {
                return;
            } else if (diffY > 0 && isScrolledToStart) {
                return;
            } else if (diffY < 0 && isScrolledToEnd) {
                return;
            } else if (elem.scrollHeight > elem.clientHeight) {
                if (this.preventDefault || this.disableScrollPass) {
                    event.preventDefault();
                }

                elem.scrollTop = Math.max(0, this.initScrollTop - diffY);
            }
        } else {
            throw new Error(`Unknown direction "${this.direction}"`);
        }
    }

    @HostListener('touchend.out-zone', ['$event'])
    onTouchEnd(): void {
        if (! this.enabled) {
            return;
        }
    }
}
