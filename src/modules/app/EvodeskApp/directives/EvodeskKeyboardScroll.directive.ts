import {Directive, ElementRef, EventEmitter, HostListener, Input, NgZone, Output, AfterViewInit} from '@angular/core';
import * as keycodes from '@angular/cdk/keycodes';

import {ScrollInfo} from '../components/shared/EvodeskScrollBar/EvodeskScrollBar.component';

@Directive({
    selector: '[evodeskKeyboardScroll]',
})
export class EvodeskKeyboardScrollDirective implements AfterViewInit
{
    @Input('evodeskKeyboardScrollDirection') direction: 'x' | 'y' = 'y';

    @Input('evodeskKeyboardScrollEnabled') enabled: boolean = true;

    @Input('evodeskKeyUpScrollEnabled') keyUp: boolean = true;
    @Input('evodeskKeyDownScrollEnabled') keyDown: boolean = true;
    @Input('evodeskPageUpScrollEnabled') pageUp: boolean = true;
    @Input('evodeskPageDownScrollEnabled') pageDown: boolean = true;
    @Input('evodeskEndScrollEnabled') end: boolean = true;
    @Input('evodeskHomeScrollEnabled') home: boolean = true;
    @Input('evodeskKeyLeftScrollEnabled') keyLeft: boolean = false;
    @Input('evodeskKeyRightScrollEnabled') keyRight: boolean = false;

    @Output() keyScroll: EventEmitter<ScrollInfo> = new EventEmitter<ScrollInfo>();

    private element: HTMLElement;

    private ARROW_SCROLL_DELTA: number = 100;
    private isInFocus: boolean = false;

    constructor(
        private elementRef: ElementRef,
        private ngZone: NgZone,
    ) {}

    ngAfterViewInit() {
        this.element = this.elementRef.nativeElement;
    }

    @HostListener('scroll.out-zone')
    onScroll(): void {
        this.keyScroll.emit({
            x: this.element.scrollLeft,
            y: this.element.scrollTop,
        });
    }

    @HostListener('window:keyup.out-zone', ['$event'])
    onKeyUp(event: KeyboardEvent): void {
        this.ngZone.runOutsideAngular(() => {
            if (this.isInFocus) {
                this.ngZone.runOutsideAngular(() => {
                    switch (event.keyCode) {
                        case keycodes.UP_ARROW: {
                            this.scrollUp();
                            break;
                        }
                        case keycodes.DOWN_ARROW: {
                            this.scrollDown();
                            break;
                        }
                        case keycodes.LEFT_ARROW: {
                            this.scrollLeft();
                            break;
                        }
                        case keycodes.RIGHT_ARROW: {
                            this.scrollRight();
                            break;
                        }
                        case keycodes.PAGE_UP: {
                            this.scrollPageUp();
                            break;
                        }
                        case keycodes.PAGE_DOWN: {
                            this.scrollPageDown();
                            break;
                        }
                        case keycodes.HOME: {
                            this.scrollToStart();
                            break;
                        }
                        case keycodes.END: {
                            this.scrollToEnd();
                            break;
                        }
                    }
                });
            }
        });
    }

    @HostListener('mouseleave.out-zone', ['$event'])
    onMouseLeave(): void {
        this.ngZone.runOutsideAngular(() => {
            if (! this.enabled) {
                return;
            }

            this.isInFocus = false;
        });
    }

    @HostListener('mouseenter.out-zone', ['$event'])
    onMouseEnter(): void {
        this.ngZone.runOutsideAngular(() => {
            if (! this.enabled) {
                return;
            }

            this.isInFocus = true;
        });
    }

    private scrollUp() {
        if (this.keyUp) {
            this.element.scrollBy({ top: -this.ARROW_SCROLL_DELTA, behavior: 'smooth'});
        }
    }

    private scrollDown() {
        if (this.keyDown) {
            this.element.scrollBy({top: this.ARROW_SCROLL_DELTA, behavior: 'smooth'});
        }
    }

    private scrollPageUp() {
        if (this.pageUp) {
            this.element.scrollBy({top: -this.element.clientHeight, behavior: 'smooth'});
        }
    }

    private scrollPageDown() {
        if (this.pageDown) {
            this.element.scrollBy({top: this.element.clientHeight, behavior: 'smooth'});
        }
    }

    private scrollToStart() {
        if (this.home) {
            if (this.direction === 'y') {
                this.element.scrollTo({top: 0, behavior: 'smooth'});
            } else if (this.direction === 'x') {
                this.element.scrollTo({left: 0, behavior: 'smooth'});
            }
        }
    }

    private scrollToEnd() {
        if (this.end) {
            if (this.direction === 'y') {
                this.element.scrollTo({top: this.element.scrollHeight, behavior: 'smooth'});
            } else if (this.direction === 'x') {
                this.element.scrollTo({left: this.element.scrollWidth, behavior: 'smooth'});
            }
        }
    }

    private scrollLeft() {
        if (this.keyLeft) {
            this.element.scrollBy({ left: -this.ARROW_SCROLL_DELTA, behavior: 'smooth'});
        }
    }

    private scrollRight() {
        if (this.keyRight) {
            this.element.scrollBy({ left: this.ARROW_SCROLL_DELTA, behavior: 'smooth'});
        }
    }
}
