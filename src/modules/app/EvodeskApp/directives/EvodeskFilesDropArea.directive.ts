import {Directive, EventEmitter, HostListener, NgZone, OnDestroy, Output} from '@angular/core';

import {forkJoin, Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Directive({
    selector: '[evodeskFormFilesDropArea]',
})
export class EvodeskFilesDropAreaDirective implements OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    private dragEnterCounter: number = 0;

    @Output('onFileDropAreaNext') onFileDropAreaNext: EventEmitter<Array<File>> = new EventEmitter();

    @Output('onFileDropAreaDragEnter') onFileDropAreaDragEnterEvent: EventEmitter<DragEvent> = new EventEmitter();
    @Output('onFileDropAreaDragLeave') onFileDropAreaDragLeaveEvent: EventEmitter<DragEvent> = new EventEmitter();

    constructor(
        private zone: NgZone,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    @HostListener('dragenter', ['$event'])
    onDragEnter(e: DragEvent): void {
        e.stopPropagation();
        e.preventDefault();

        this.dragEnterCounter++;
        this.onFileDropAreaDragEnterEvent.emit(e);
    }

    @HostListener('dragover', ['$event'])
    onDragOver(e: DragEvent): void {
        e.stopPropagation();
        e.preventDefault();
    }

    @HostListener('dragleave', ['$event'])
    onDragLeave(e: DragEvent): void {
        e.preventDefault();
        e.stopPropagation();

        this.dragEnterCounter--;

        if (this.dragEnterCounter === 0) {
            this.onFileDropAreaDragLeaveEvent.emit(e);
        }
    }

    @HostListener('drop', ['$event'])
    onDrop(e: DragEvent): void {
        this.dragEnterCounter = 0;

        e.preventDefault();
        e.stopPropagation();

        const files: FileList = e.dataTransfer.files;

        if (files.length > 0) {
            const total: Array<Observable<Array<File>>> = [];

            for (let i: number = 0; i < e.dataTransfer.files.length; i++) {
                if (e.dataTransfer.items && e.dataTransfer.items[0].webkitGetAsEntry) {
                    total.push(this.withWebKit(e.dataTransfer, i));
                } else {
                    total.push(Observable.create(observer => {
                        observer.next(this.withGenericBrowser(e.dataTransfer, i));
                        observer.complete();
                    }));
                }
            }

            forkJoin(total).pipe(takeUntil(this.ngOnDestroy$)).subscribe(next => {
                const flatMapResults: Array<File> = [];

                next.forEach(n => {
                    n.forEach(f => flatMapResults.push(f));
                });

                this.zone.run(() => {
                    this.onFileDropAreaNext.emit(flatMapResults);
                });
            });
        }
    }

    private withGenericBrowser(dataTransfer: DataTransfer, index: number): Array<File> {
        return [dataTransfer.files[index]];
    }

    private withWebKit(dataTransfer: DataTransfer, index: number): Observable<Array<File>> {
        return Observable.create(observer => {
            const entry: any = dataTransfer.items[index].webkitGetAsEntry();
            const queue: Array<Observable<File>> = [];

            if (entry.isFile) {
                queue.push(Observable.create(fileObserver => {
                    entry.file(single => {
                        fileObserver.next(single);
                        fileObserver.complete();
                    });
                }));
            } else if (entry.isDirectory) {
                const reader: any = entry.createReader();

                queue.push(Observable.create(fileObserver => {
                    reader.readEntries((event: any) => {
                        let readComplete: number = 0;
                        let total: number = 0;

                        event.forEach(fileEntry => {
                            if (fileEntry.isFile) {
                                total++;
                            }
                        });

                        event.forEach(fileEntry => {
                            if (fileEntry.isFile) {
                                fileEntry.file(f => {
                                    readComplete++;

                                    fileObserver.next(f);

                                    if (readComplete === total) {
                                        fileObserver.complete();
                                    }
                                });
                            }
                        });
                    });
                }));
            }

            let processed: number = 0;
            const totalFiles: Array<File> = [];

            queue.forEach(q => q.pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                next => totalFiles.push(next),
                undefined,
                () => {
                    processed++;

                    if (processed === queue.length) {
                        observer.next(totalFiles);
                        observer.complete();
                    }
                },
            ));
        });
    }
}
