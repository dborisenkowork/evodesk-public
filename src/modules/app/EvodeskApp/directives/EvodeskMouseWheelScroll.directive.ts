import {Directive, ElementRef, EventEmitter, HostListener, Input, NgZone, Output} from '@angular/core';

import {ScrollInfo} from '../components/shared/EvodeskScrollBar/EvodeskScrollBar.component';

@Directive({
    selector: '[evodeskMouseWheelScroll]',
})
export class EvodeskMouseWheelScrollDirective
{
    @Input('evodeskMouseWheelScrollEnabled') enabled: boolean = true;
    @Input('evodeskMouseWheelScrollX') enableX: boolean = false;
    @Input('evodeskMouseWheelScrollY') enableY: boolean = true;
    @Input('evodeskMouseWheelPreventDefaults') preventDefaults: boolean = true;
    @Input('evodeskMouseWheelDisableScrollPass') disableScrollPass: boolean = false;

    @Output() mouseWheelScroll: EventEmitter<ScrollInfo> = new EventEmitter<ScrollInfo>();

    private static SCROLL_DELTA: number = 100;

    constructor(
        private el: ElementRef,
        private ngZone: NgZone,
    ) {}

    @HostListener('wheel.out-zone', ['$event'])
    onMouseWheel($event: WheelEvent): void {
        this.ngZone.runOutsideAngular(() => {
            const element: HTMLElement = this.el.nativeElement as HTMLElement;

            if (this.disableScrollPass) {
                $event.stopImmediatePropagation();
            }

            if (! this.enabled) {
                return;
            }

            if (this.enableX) {
                const isScrolledToStart: boolean = element.scrollLeft === 0;
                const isScrolledToEnd: boolean = element.offsetWidth + element.scrollLeft === element.scrollWidth;

                if ($event.deltaY === 0) {
                    return;
                } else if ($event.deltaY < 0 && isScrolledToStart) {
                    return;
                } else if ($event.deltaY > 0 && isScrolledToEnd) {
                    return;
                } else if (element.scrollWidth > element.clientWidth) {
                    if (this.preventDefaults && ! this.disableScrollPass) {
                        $event.preventDefault();
                        $event.stopPropagation();
                    }
                    if (navigator.userAgent.indexOf('Firefox') !== -1) {
                        element.scrollBy({left: $event.deltaY * EvodeskMouseWheelScrollDirective.SCROLL_DELTA / Math.abs($event.deltaY)});
                    } else {
                        element.scrollBy({left: $event.deltaY});
                    }
                }
            }

            if (this.enableY) {
                const isScrolledToStart: boolean = element.scrollTop === 0;
                const isScrolledToEnd: boolean = element.offsetHeight + element.scrollTop === element.scrollHeight;

                if ($event.deltaY === 0) {
                    return;
                } else if ($event.deltaY < 0 && isScrolledToStart) {
                    return;
                } else if ($event.deltaY > 0 && isScrolledToEnd) {
                    return;
                } else if (element.scrollHeight > element.clientHeight) {
                    if (this.preventDefaults && ! this.disableScrollPass) {
                        $event.preventDefault();
                        $event.stopPropagation();
                    }
                    if (navigator.userAgent.indexOf('Firefox') !== -1) {
                        element.scrollBy({top: $event.deltaY * EvodeskMouseWheelScrollDirective.SCROLL_DELTA / Math.abs($event.deltaY)});
                    } else {
                        element.scrollBy({top: $event.deltaY});
                    }
                }
            }

            this.mouseWheelScroll.emit({ x: element.scrollLeft, y: element.scrollTop });
        });
    }
}
