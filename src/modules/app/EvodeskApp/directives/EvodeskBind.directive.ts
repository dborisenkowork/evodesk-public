import {Directive, ElementRef, Input, NgZone, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Directive({
    'selector': '[evodeskBind]',
})
export class EvodeskBindDirective implements OnChanges, OnDestroy
{
    @Input('evodeskBind') bind: Observable<string>;

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private el: ElementRef,
        private ngZone: NgZone,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (this.bind) {
            this.bind.pipe(takeUntil(this.ngOnChanges$)).subscribe((next) => {
                this.ngZone.runOutsideAngular(() => {
                    (this.el.nativeElement as HTMLElement).textContent = next;
                });
            });
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }
}

