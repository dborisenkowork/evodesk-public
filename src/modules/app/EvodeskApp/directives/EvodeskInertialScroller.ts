export type Direction = 'x' | 'y';

const options: { p1: number, p2: number } = {
    p1: 0.02,
    p2: 0.2,
};

window['options'] = options;

export class EvodeskInertialScroller
{
    private scrollTrackX: Array<number> = [];
    private scrollTrackY: Array<number> = [];
    private previousX: number;
    private previousY: number;

    private interruptCurrentScroll: boolean = false;
    private firstStep: boolean = true;
    private currentScrollDirection: Direction;

    public reset() {
        this.scrollTrackX = [];
        this.scrollTrackY = [];
        this.currentScrollDirection = null;

        this.interruptCurrentScroll = true;
        this.firstStep = true;
    }

    public trackVelocity(velocity: number, direction: Direction) {
        if (direction === 'x') {
            this.scrollTrackX.push(velocity);
        } else if (direction === 'y') {
            this.scrollTrackY.push(velocity);
        }
    }

    public trackPosition(cursorX: number, cursorY: number) {
        if (!this.firstStep) {
            this.scrollTrackX.push(this.updateVelocity(this.previousX, cursorX));
            this.scrollTrackY.push(this.updateVelocity(this.previousY, cursorY));
        }
        this.firstStep = false;
        this.previousX = cursorX;
        this.previousY = cursorY;
    }

    public scroll(element: Element, direction: Direction) {
        if (direction === 'x' && this.scrollTrackX.length > 1 || direction === 'y' && this.scrollTrackY.length > 1) {
            this.interruptCurrentScroll = false;
            this.currentScrollDirection = direction;
            this.dragInertia(0, element, direction);
        } else {
            this.reset();
        }
    }

    public getCurrentScrollDirection(): Direction {
        return this.currentScrollDirection;
    }

    protected inertiaVelocity(velocity: number, iteration: number): number {
        return velocity * Math.acos(iteration * options.p1) * options.p2;
    }

    get velocityX(): number {
        return this.scrollTrackX.length > 1 ?
            this.scrollTrackX.reduce(((previousValue, currentValue) => previousValue + currentValue)) / this.scrollTrackX.length : 0;
    }

    get velocityY(): number {
        return this.scrollTrackY.length > 1 ?
            this.scrollTrackY.reduce(((previousValue, currentValue) => previousValue + currentValue)) / this.scrollTrackY.length : 0;
    }

    private updateVelocity(prevPosition: number, curPosition: number) {
        const velocity: number = prevPosition - curPosition;
        return (Math.abs(velocity) > 1) ? velocity : 0;
    }

    protected dragInertia(iteration: number, element: Element, direction: Direction) {
        if (direction === 'x') {
            element.scrollBy({left: this.inertiaVelocity(this.velocityX, iteration)});
        } else if (direction === 'y') {
            element.scrollBy({top: this.inertiaVelocity(this.velocityY, iteration)});
        }

        if (iteration < 50 && !this.interruptCurrentScroll) {
            // this.ngZone.runOutsideAngular(() => {
            //     window.requestAnimationFrame(() => {
            //         this.dragInertia(iteration + 1, element, direction);
            //     });
            // });
        }
    }
}
