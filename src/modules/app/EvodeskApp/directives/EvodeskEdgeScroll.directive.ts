import {Directive, ElementRef, HostListener, Input, NgZone, OnDestroy} from '@angular/core';

import {Moment} from 'moment';
import * as moment from 'moment';

interface State {
    isScrolling: boolean;
    moveInterval: any;
    speed: number;
    drag: {
        isEnabled: boolean;
        initX?: number;
        initY?: number;
        initScrollLeft?: number;
        initScrollTop?: number;
        startTime?: Moment;
        setPriority?: 'x' | 'y' | 'none' | undefined;
        dragMoveX?: number;
        dragMoveY?: number;
    };
}

export interface EvodeskEdgeScrollDirectiveOptions {
    edgeSize: number;
    minEdgeSize: number;
    enabled: 'x' | 'y' | 'xy';
    maxSpeed: number;
    preventDefault: boolean;
    stopPropagation: boolean;
    dragDelayAndPrioritize: boolean;
    dragDelayAndPrioritizeTimeoutMs: number;
    dragDelayAndPrioritizeTresholdPx: number;
    dragDelayAndPrioritizeBreakPx: number;
}

export const defaultOptions: EvodeskEdgeScrollDirectiveOptions = {
    edgeSize: 1 / 4,
    minEdgeSize: 100,
    enabled: 'x',
    maxSpeed: 25,
    preventDefault: false,
    stopPropagation: false,
    dragDelayAndPrioritize: false,
    dragDelayAndPrioritizeTimeoutMs: 60,
    dragDelayAndPrioritizeTresholdPx: 30,
    dragDelayAndPrioritizeBreakPx: 80,
};

@Directive({
    selector: '[evodeskEdgeScroll]',
})
export class EvodeskEdgeScrollDirective implements OnDestroy
{
    @Input('evodeskEdgeScrollScrollEnabled') isScrollEnabled: boolean;
    @Input('evodeskEdgeScrollDragEnabled') isDragEnabled: boolean;
    @Input('evodeskEdgeScrollOptions') options: EvodeskEdgeScrollDirectiveOptions = { ...defaultOptions };

    public state: State = {
        isScrolling: true,
        moveInterval: undefined,
        speed: 0,
        drag: {
            isEnabled: false,
        },
    };

    constructor(
        private el: ElementRef,
        private ngZone: NgZone,
    ) {}

    @HostListener('touchstart.out-zone', ['$event'])
    onTouchStart(event: TouchEvent): void {
        this.onStart(event.touches[0].pageX, event.touches[0].pageY);
    }

    @HostListener('mousedown.out-zone', ['$event'])
    onMouseStart(event: MouseEvent): void {
        this.onStart(event.clientX, event.clientY);
    }

    @HostListener('touchmove.out-zone', ['$event'])
    onTouchMove(event: TouchEvent): void {
        if (this.options.preventDefault) {
            event.preventDefault();
        }

        if (this.options.stopPropagation) {
            event.stopPropagation();
        }

        this.onMove(event.touches[0].pageX, event.touches[0].pageY, event);
    }

    @HostListener('document:mousemove.out-zone', ['$event'])
    onMouseMove(event: MouseEvent): void {
        if (this.options.preventDefault) {
            event.preventDefault();
        }

        if (this.options.stopPropagation) {
            event.stopPropagation();
        }

        this.onMove(event.clientX, event.clientY);
    }

    @HostListener('document:mouseup.out-zone')
    onTouchEnd(): void {
        this.ngZone.runOutsideAngular(() => {
            this.onEnd();
        });
    }

    @HostListener('touchend.out-zone')
    onMouseUp(): void {
        this.onEnd();
    }

    ngOnDestroy(): void {
        this.stopAnimations();
    }

    private startAnimations(): void {
        this.ngZone.runOutsideAngular(() => {
            const scroll: FrameRequestCallback = () => {
                this.ngZone.runOutsideAngular(() => {
                    if (this.isScrollEnabled && this.state.speed !== 0) {
                        if (this.options.enabled === 'x') {
                            if (this.isScrollEnabled) {
                                this.el.nativeElement.scrollLeft += Math.min(1, this.state.speed) * this.options.maxSpeed;
                            }
                        } else if (this.options.enabled === 'y') {
                            if (this.isScrollEnabled) {
                                this.el.nativeElement.scrollTop += Math.min(1, this.state.speed) * this.options.maxSpeed;
                            }
                        }
                    }

                    this.state.moveInterval = window.requestAnimationFrame(scroll);
                });
            };

            this.state.moveInterval = window.requestAnimationFrame(scroll);
        });
    }

    private stopAnimations(): void {
        this.ngZone.runOutsideAngular(() => {
            if (this.state.moveInterval) {
                window.cancelAnimationFrame(this.state.moveInterval);

                this.state.moveInterval = undefined;
            }
        });
    }

    private onStart(cursorX: number, cursorY: number): void {
        this.ngZone.runOutsideAngular(() => {
            this.state = {
                ...this.state,
                drag: {
                    ...this.state.drag,
                    isEnabled: true,
                    initX: cursorX,
                    initY: cursorY,
                    initScrollLeft: (this.el.nativeElement as HTMLElement).scrollLeft,
                    initScrollTop: (this.el.nativeElement as HTMLElement).scrollTop,
                    startTime: moment(new Date()),
                    setPriority: undefined,
                    dragMoveX: 0,
                    dragMoveY: 0,
                },
            };

            if (this.isScrollEnabled || this.state.drag) {
                this.startAnimations();
            }
        });
    }

    private onMove(cursorX: number, cursorY: number, touchEvent?: TouchEvent): void {
        if (touchEvent) {
            touchEvent.preventDefault();
        }

        this.ngZone.runOutsideAngular(() => {
            if (this.isScrollEnabled) {
                if (! this.state.moveInterval) {
                    this.startAnimations();
                }

                this.onMoveToScroll(cursorX, cursorY);
            } else if (this.isDragEnabled && this.state.drag.isEnabled) {
                if (! this.state.moveInterval) {
                    this.startAnimations();
                }

                this.onMoveToDrag(cursorX, cursorY, touchEvent);
            }
        });
    }

    private onMoveToScroll(cursorX: number, cursorY: number): void {
        this.ngZone.runOutsideAngular(() => {
            const rect: any = this.el.nativeElement.getBoundingClientRect();

            const startFromLeftEdge: number = rect.left;
            const startFromRightEdge: number = rect.left + rect.width;
            const startFromTopEdge: number = rect.top;
            const startFromBottomEdge: number = rect.top + rect.height;

            if (this.options.enabled === 'x') {
                const edgeSize: number = Math.max(this.options.minEdgeSize, (this.el.nativeElement as HTMLElement).clientWidth * this.options.edgeSize);

                if (cursorX <= (startFromLeftEdge + edgeSize)) {
                    if (cursorY >= startFromTopEdge && cursorY <= startFromBottomEdge) {
                        this.state = {
                            ...this.state,
                            speed: -1 * (edgeSize + startFromLeftEdge - cursorX) / (edgeSize),
                        };
                    } else {
                        this.state = {
                            ...this.state,
                            speed: 0,
                        };
                    }
                } else if (cursorX >= (startFromRightEdge - edgeSize)) {
                    if (cursorY >= startFromTopEdge && cursorY <= startFromBottomEdge) {
                        this.state = {
                            ...this.state,
                            speed: ((100 / edgeSize) * (cursorX - (startFromRightEdge - edgeSize))) / 100,
                        };
                    } else {
                        this.state = {
                            ...this.state,
                            speed: 0,
                        };
                    }
                } else {
                    this.state = {
                        ...this.state,
                        speed: 0,
                    };
                }
            } else if (this.options.enabled === 'y') {
                if (cursorX >= startFromLeftEdge && cursorX <= startFromRightEdge) {
                    const edgeSize: number = Math.max(this.options.minEdgeSize, (this.el.nativeElement as HTMLElement).clientHeight * this.options.edgeSize);

                    if (cursorY <= (startFromTopEdge + edgeSize)) {
                        this.state = {
                            ...this.state,
                            speed: -1 * (edgeSize + startFromTopEdge - cursorY) / (edgeSize),
                        };
                    } else if (cursorY >= (startFromBottomEdge - edgeSize)) {
                        this.state = {
                            ...this.state,
                            speed: ((100 / edgeSize) * (cursorY - (startFromBottomEdge - edgeSize))) / 100,
                        };
                    } else {
                        this.state = {
                            ...this.state,
                            speed: 0,
                        };
                    }
                } else {
                    this.state = {
                        ...this.state,
                        speed: 0,
                    };
                }
            } else {
                this.state = {
                    ...this.state,
                    speed: 0,
                };
            }
        });
    }

    private onMoveToDrag(cursorX: number, cursorY: number, $event: TouchEvent): void {
        if ($event) {
            $event.preventDefault();
        }

        this.ngZone.runOutsideAngular(() => {
            const canScroll: boolean = (() => {
                const canScrollX: boolean = (this.el.nativeElement as HTMLElement).scrollWidth > (this.el.nativeElement as HTMLElement).clientWidth;
                const canScrollY: boolean = (this.el.nativeElement as HTMLElement).scrollHeight > (this.el.nativeElement as HTMLElement).clientHeight;

                if (this.options.enabled === 'x') {
                    return canScrollX;
                } else if (this.options.enabled === 'y') {
                    return canScrollY;
                } else {
                    return canScrollX || canScrollY;
                }
            })();

            const dragX: Function = () => {
                const diffX: number = cursorX - this.state.drag.initX;

                window.requestAnimationFrame(() => {
                    this.ngZone.runOutsideAngular(() => {
                        if (this.state.drag.initScrollLeft !== undefined) {
                            (this.el.nativeElement as HTMLElement).scrollLeft = Math.max(0, this.state.drag.initScrollLeft - diffX);
                        }
                    });
                });
            };

            const dragY: Function = () => {
                const diffY: number = cursorY - this.state.drag.initY;

                window.requestAnimationFrame(() => {
                    this.ngZone.runOutsideAngular(() => {
                        if (this.state.drag.initScrollTop !== undefined) {
                            (this.el.nativeElement as HTMLElement).scrollTop = Math.max(0, this.state.drag.initScrollTop - diffY);
                        }
                    });
                });
            };

            const setPriority: Function = (priorty: 'x' | 'y' | 'none' | undefined) => {
                this.state = {
                    ...this.state,
                    drag: {
                        ...this.state.drag,
                        setPriority: priorty,
                    },
                };
            };

            if (canScroll && this.options.dragDelayAndPrioritize) {
                if (this.state.drag.setPriority) {
                    if (this.state.drag.setPriority === 'x' && this.options.enabled === 'x') {
                        dragX();

                        if ($event) {
                            $event.stopPropagation();
                        }
                    } else if (this.state.drag.setPriority === 'y' && this.options.enabled === 'y') {
                        dragY();

                        if ($event) {
                            $event.stopPropagation();
                        }
                    }
                } else {
                    const diffDragMoveX: number = Math.abs(this.state.drag.dragMoveX) - Math.abs(this.state.drag.dragMoveY);
                    const diffDragMoveY: number = Math.abs(this.state.drag.dragMoveY) - Math.abs(this.state.drag.dragMoveX);

                    if (this.options.enabled === 'y' && diffDragMoveX > this.options.dragDelayAndPrioritizeBreakPx) {
                        setPriority('none');
                    } else if (this.options.enabled === 'x' && diffDragMoveY > this.options.dragDelayAndPrioritizeBreakPx) {
                        setPriority('none');
                    } else {
                        if ($event) {
                            $event.stopPropagation();
                        }

                        if (moment(new Date()).diff(this.state.drag.startTime, 'milliseconds') > this.options.dragDelayAndPrioritizeTimeoutMs) {
                            if (diffDragMoveX > this.options.dragDelayAndPrioritizeTresholdPx) {
                                setPriority('x');
                            } else if (diffDragMoveY > this.options.dragDelayAndPrioritizeTresholdPx) {
                                setPriority('y');
                            } else {
                                setPriority('none');
                            }
                        } else {
                            const diffX: number = cursorX - this.state.drag.initX;
                            const diffY: number = cursorY - this.state.drag.initY;

                            this.state.drag.dragMoveX += diffX;
                            this.state.drag.dragMoveY += diffY;
                        }
                    }
                }
            } else if (canScroll) {
                if (this.options.enabled === 'xy' || this.options.enabled === 'x') {
                    dragX();
                }

                if (this.options.enabled === 'xy' || this.options.enabled === 'y') {
                    dragY();
                }
            }
        });
    }

    private onEnd(): void {
        this.ngZone.runOutsideAngular(() => {
            this.state = {
                ...this.state,
                drag: {
                    isEnabled: false,
                },
            };

            this.stopAnimations();
        });
    }
}
