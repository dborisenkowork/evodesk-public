import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable} from 'rxjs';

import {EvodeskAppStateModel} from '../models/EvodeskAppState.model';

export type AppState = EvodeskAppStateModel;

function initialState(): AppState {
    return {
        isDesksSubmenuOpened: false,
        isAppHeaderDisabled: false,
    };
}

type PrevAppState = AppState | undefined;
type NextAppState = AppState;
type States = { current: NextAppState, prev?: PrevAppState };

@Injectable()
export class EvodeskAppStateService
{
    private _snapshot: AppState;
    private _current$: BehaviorSubject<States> = new BehaviorSubject<States>({ current: initialState() });

    constructor() {
        this._current$.subscribe(states => this._snapshot = states.current);
    }

    get current$(): Observable<States> {
        return this._current$.asObservable();
    }

    get snapshot(): AppState {
        return this._snapshot;
    }

    setState(query: (orig: AppState) => AppState) {
        const prev: AppState = this._current$.getValue().current;

        this._current$.next({
            prev: prev,
            current: query(prev),
        });
    }

    reset(): void {
        this.setState(() => {
            return initialState();
        });
    }
}
