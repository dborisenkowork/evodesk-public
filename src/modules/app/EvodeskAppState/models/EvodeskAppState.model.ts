import {UserBalanceModel, UserModel} from '../../EvodeskRESTApi/models/user/User.model';

import {DeskModel} from '../../EvodeskRESTApi/models/Desk.model';
import {DeskInviteModel} from '../../EvodeskRESTApi/models/DeskInvite.model';

export interface EvodeskAppStateModel /* avoid to import it. User AppState shortcut */
{
    currentUser?: UserModel;
    currentUserBalance?: UserBalanceModel;
    personalDesk?: DeskModel;
    multidesks?: Array<DeskModel>;
    invites?: Array<DeskInviteModel>;
    isDesksSubmenuOpened: boolean;
    isAppHeaderDisabled: boolean;
}
