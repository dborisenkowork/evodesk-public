import {ProfileId} from '../../EvodeskRESTApi/models/profile/Profile.model';
import {EvodeskHeaderMenuItem} from '../../../features/EvodeskHeader/models/EvodeskHeader.models';

import {EvodeskDeskSyncState} from '../../../features/EvodeskDesk/state/EvodeskDeskSyncState.service';

export enum AppEvent {
    HttpError = 'http-error',

    SignOut = 'sign-out',

    HeaderMobileClickItem = 'header-mobile-click-item',

    DeskForceReload = 'personal-desk-force-reload',
    DeskChangesDetected = 'personal-desk-changes-detected',

    IMForceReload = 'im-force-reload',
    IMChangesDetected = 'im-changes-detected',

    InvalidateProfileImage = 'invalidate-profile-image',
}

export type AppBusAction  =
      { type: AppEvent.HttpError, payload: { reason: any } }
    | { type: AppEvent.SignOut }
    | { type: AppEvent.HeaderMobileClickItem, payload: EvodeskHeaderMenuItem }
    | { type: AppEvent.DeskForceReload, payload: { reason: EvodeskDeskSyncState; } }
    | { type: AppEvent.DeskChangesDetected }
    | { type: AppEvent.IMForceReload }
    | { type: AppEvent.IMChangesDetected }
    | { type: AppEvent.InvalidateProfileImage, payload: { profileId: ProfileId; } }
;
