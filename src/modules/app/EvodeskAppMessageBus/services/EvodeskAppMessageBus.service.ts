import {Injectable} from '@angular/core';

import {Subject, Observable} from 'rxjs';

import {AppBusAction} from '../models/app-events.model';

@Injectable()
export class EvodeskAppMessageBusService
{
    private _stream$: Subject<AppBusAction> = new Subject<AppBusAction>();

    get stream$(): Observable<AppBusAction> {
        return this._stream$.asObservable();
    }

    dispatch(action: AppBusAction): void {
        this._stream$.next(action);
    }
}
