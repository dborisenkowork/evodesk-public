import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {Observable} from 'rxjs';

import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

export interface EvodeskFeedPostRepostedComponentInputEntity {
    authorProfileId: ProfileId;
    authorProfileImage: string | null;
    authorProfileName: string;
    authorProfileUrl: Array<any>;
    originalPostText$: Observable<string>;
    repostDate$: Observable<string>;
}

export interface EvodeskFeedPostRepostedComponentProps {
    entity: EvodeskFeedPostRepostedComponentInputEntity;
}

type Props = EvodeskFeedPostRepostedComponentProps;

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-feed-post-reposted',
    templateUrl: './EvodeskFeedPostReposted.component.pug',
    styleUrls: [
        './EvodeskFeedPostReposted.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostRepostedComponent implements OnChanges
{
    @Input() props: Props;

    @Output('openRepost') openRepostEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    openRepost(): void {
        this.openRepostEvent.emit(undefined);
    }
}
