import {Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewContainerRef} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import * as moment from 'moment';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../types/Backend.types';

import {EvodeskFeedPostRepostedComponentProps} from './EvodeskFeedPostReposted.component';

import {FeedPostReposted} from '../../state/EvodeskFeedState.service';
import {EvodeskFeedBodyParserService} from '../../services/EvodeskFeedBodyParser.service';
import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';
import {EvodeskFeedOpenPostService} from '../../services/EvodeskFeedOpenPost.service';
import {EvodeskProfileUrlService} from '../../../EvodeskProfile/shared/services/EvodeskProfileUrl.service';

type Props = EvodeskFeedPostRepostedComponentProps;

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-feed-post-reposted-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-feed-post-reposted [props]="state.props" (openRepost)="openRepost()"></evodesk-feed-post-reposted>
      </ng-container>
    `,
})
export class EvodeskFeedPostRepostedContainerComponent implements OnChanges, OnDestroy
{
    @Input() entity: FeedPostReposted;

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private dates: EvodeskDateService,
        private bodyParser: EvodeskFeedBodyParserService,
        private profileUrl: EvodeskProfileUrlService,
        private openPostService: EvodeskFeedOpenPostService,
        private viewContainerRef: ViewContainerRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (changes['entity']) {
            if (this.entity) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        entity: {
                            originalPostText$: this.bodyParser.parseToHtmlPreview(this.entity.post.text),
                            repostDate$: this.dates.diffFromNowV1(moment(this.entity.repost.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate()),
                            authorProfileName: this.entity.repostedBy.name || this.entity.repostedBy.email,
                            authorProfileId: this.entity.repostedBy.id,
                            authorProfileImage: this.entity.repostedBy.images[0],
                            authorProfileUrl: this.profileUrl.getRouterLinkByIdAndUrlAlias(this.entity.repostedBy.id, this.entity.repostedBy.url_alias),
                        },
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
        this.ngOnChanges$.next(undefined);
    }

    openRepost(): void {
        this.openPostService.openPost(this.viewContainerRef, this.entity.repost.id).pipe(takeUntil(this.ngOnChanges$)).subscribe();
    }
}
