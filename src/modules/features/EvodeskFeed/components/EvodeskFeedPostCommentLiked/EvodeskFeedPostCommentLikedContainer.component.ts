import {Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewContainerRef} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskFeedPostCommentLikedComponentProps} from './EvodeskFeedPostCommentLiked.component';

import {FeedPostCommentLikedEntry} from '../../state/EvodeskFeedState.service';
import {EvodeskFeedBodyParserService} from '../../services/EvodeskFeedBodyParser.service';
import {EvodeskProfileUrlService} from '../../../EvodeskProfile/shared/services/EvodeskProfileUrl.service';
import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';
import {EvodeskFeedOpenPostService} from '../../services/EvodeskFeedOpenPost.service';

type Props = EvodeskFeedPostCommentLikedComponentProps;

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-feed-post-comment-liked-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-feed-post-comment-liked [props]="state.props" (openPost)="openPost()"></evodesk-feed-post-comment-liked>
      </ng-container>
    `,
})
export class EvodeskFeedPostCommentLikedContainerComponent implements OnChanges, OnDestroy
{
    @Input() entity: FeedPostCommentLikedEntry;

    public state: State = {
        ready: false,
    };

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private dates: EvodeskDateService,
        private bodyParser: EvodeskFeedBodyParserService,
        private profileUrl: EvodeskProfileUrlService,
        private openPostService: EvodeskFeedOpenPostService,
        private viewContainerRef: ViewContainerRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (changes['entity']) {
            if (this.entity) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        entity: {
                            authorProfileName: this.entity.likedBy.name || this.entity.likedBy.email,
                            authorProfileId: this.entity.likedBy.id,
                            authorProfileImage: this.entity.likedBy.images[0],
                            authorProfileUrl: this.profileUrl.getRouterLinkByIdAndUrlAlias(this.entity.likedBy.id, this.entity.likedBy.url_alias),
                            title$: this.bodyParser.parseCommentTextToHtmlPreview(this.bodyParser.parseCommentToJSOBNNBody(this.entity.comment.text).text, { maxLength: 32 }),
                            date$: this.dates.diffFromNowV1(this.entity.date),
                        },
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    openPost(): void {
        this.openPostService.openPost(this.viewContainerRef, this.entity.comment.post_id.id).pipe(takeUntil(this.ngOnChanges$)).subscribe();
    }
}
