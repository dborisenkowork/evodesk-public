import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {Observable} from 'rxjs';

import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

export interface EvodeskFeedPostCommentLikedComponentInputEntity {
    authorProfileId: ProfileId;
    authorProfileImage: string | null;
    authorProfileName: string;
    authorProfileUrl: Array<any>;
    title$: Observable<string>;
    date$: Observable<string>;
}

export interface EvodeskFeedPostCommentLikedComponentProps {
    entity: EvodeskFeedPostCommentLikedComponentInputEntity;
}

type Props = EvodeskFeedPostCommentLikedComponentProps;

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-feed-post-comment-liked',
    templateUrl: './EvodeskFeedPostCommentLiked.component.pug',
    styleUrls: [
        './EvodeskFeedPostCommentLiked.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostCommentLikedComponent implements OnChanges
{
    @Input() props: Props;

    @Output('openPost') openPostEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    openPost(): void {
        this.openPostEvent.emit(undefined);
    }
}
