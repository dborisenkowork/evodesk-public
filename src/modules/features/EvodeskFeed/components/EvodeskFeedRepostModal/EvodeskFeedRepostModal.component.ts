import {Component, ElementRef, EventEmitter, Inject, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../types/Backend.types';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import * as moment from 'moment';

import {PostModel, AttachmentListItem} from '../../../../app/EvodeskRESTApi/models/post/Post.model';
import {ProfileId, ProfileModel} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {PostRESTService} from '../../../../app/EvodeskRESTApi/services/PostREST.service';
import {EvodeskFeedBodyParserService} from '../../services/EvodeskFeedBodyParser.service';

export interface EvodeskFeedRepostModalComponentProps {
    post: PostModel;
    authorProfile: ProfileModel;
    reportToProfile: ProfileId;
}

interface State {
    ready: boolean;
    form: FormGroup;
    isTextareaFocused: boolean;
    parsed: string;
}

export function evodeskFeedRepostModalComponentFormBuilder(fb: FormBuilder): FormGroup {
    return fb.group({
        text: ['', [Validators.maxLength(65535)]],
    });
}

@Component({
    selector: 'evodesk-feed-repost-modal',
    templateUrl: './EvodeskFeedRepostModal.component.pug',
    styleUrls: [
        './EvodeskFeedRepostModal.component.scss',
    ],
})
export class EvodeskFeedRepostModalComponent implements OnInit, OnDestroy
{
    @ViewChild('textAreaInput') textAreaInputRef: ElementRef;

    @Output('success') successEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        form: evodeskFeedRepostModalComponentFormBuilder(this.fb),
        isTextareaFocused: false,
        parsed: '',
    };

    constructor(
        private fb: FormBuilder,
        private matDialogRef: MatDialogRef<EvodeskFeedRepostModalComponent>,
        @Inject(MAT_DIALOG_DATA) public props: EvodeskFeedRepostModalComponentProps,
        private postRESTService: PostRESTService,
        private parser: EvodeskFeedBodyParserService,
    ) {}

    ngOnInit(): void {
        this.parser.parseToHtml(this.props.post.text).pipe(takeUntil(this.ngOnDestroy$)).subscribe((result) => {
            this.state = {
                ...this.state,
                parsed: result,
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get textareaPlaceholder(): string {
        return this.state.isTextareaFocused ? '' : 'EvodeskFeed.components.EvodeskFeedRepostModal.TextPlaceholder';
    }

    onTextKeyDown($event: KeyboardEvent): void {
        if ($event.ctrlKey && $event.keyCode === 13) {
            this.submit();
        }
    }

    onTextareaFocus(): void {
        this.state = {
            ...this.state,
            isTextareaFocused: true,
        };
    }

    onTextareBlur(): void {
        this.state = {
            ...this.state,
            isTextareaFocused: false,
        };
    }

    get attachments(): Array<AttachmentListItem> {
        return this.props.post.attachments.map(a => {
            return <AttachmentListItem>{
                type: a.type,
                attachment: a,
            };
        });
    }

    get createdAt(): string {
        return moment(this.props.post.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).format('LL');
    }

    get hasText(): boolean {
        return !! this.props.post.text;
    }

    cancel(): void {
        this.matDialogRef.close();
    }

    submit(): void {
        if (this.state.form.valid) {
            this.state.form.disable();

            this.postRESTService.repost({
                profileId: this.props.reportToProfile,
                postId: this.props.post.id,
                text: this.state.form.value.text,
            }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                () => {
                    this.successEvent.emit(undefined);

                    this.matDialogRef.close();
                },
                () => {
                    this.state.form.enable();
                },
            );
        }
    }
}
