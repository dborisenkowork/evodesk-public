import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

import {AttachmentPhotoModel} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

import {environment} from '../../../../../environments/environment.config';
import {AuthTokenService} from '../../../EvodeskAuth/services/AuthToken.service';

@Component({
    selector: 'evodesk-feed-post-attachment-photo',
    templateUrl: './EvodeskFeedPostAttachmentPhoto.component.pug',
    styleUrls: [
        './EvodeskFeedPostAttachmentPhoto.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostAttachmentPhotoComponent
{
    @Input() attachment: AttachmentPhotoModel;
    @Input() controls: boolean = false;

    @Output('remove') removeEvent: EventEmitter<AttachmentPhotoModel> = new EventEmitter<AttachmentPhotoModel>();

    constructor(
        private authToken: AuthTokenService,
    ) {}

    get url(): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/post/social/${this.attachment.id}?access_token=${this.authToken.token}`;
    }

    get preview(): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/post/social/${this.attachment.id}`;
    }

    remove($event: MouseEvent): void {
        $event.preventDefault();
        $event.stopPropagation();

        this.removeEvent.emit(this.attachment);
    }
}
