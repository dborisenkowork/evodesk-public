import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

interface State {
    ready: boolean;
}

export interface EvodeskFeedPostRepostsFloatComponentProps {
    loading: boolean;
    numProfiles: number;
    profiles: Array<{
        id: ProfileId;
        name: string;
        avatar: string | undefined | null;
        routerLink: Array<any>;
    }>;
}

type Props = EvodeskFeedPostRepostsFloatComponentProps;

@Component({
    selector: 'evodesk-feed-post-reposts-float',
    templateUrl: './EvodeskFeedPostRepostsFloat.component.pug',
    styleUrls: [
        './EvodeskFeedPostRepostsFloat.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostRepostsFloatComponent implements OnChanges
{
    @Input() props: Props;

    @Output('details') detailsEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get hasDetails(): boolean {
        return this.props.numProfiles > this.props.profiles.length;
    }

    get detailsParam(): any {
        return {
            count: this.props.numProfiles - this.props.profiles.length,
        };
    }

    details(): void {
        this.detailsEvent.emit(undefined);
    }
}
