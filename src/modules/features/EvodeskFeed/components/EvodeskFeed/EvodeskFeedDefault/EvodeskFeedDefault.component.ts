import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild} from '@angular/core';

import {Subject} from 'rxjs';

import {isScrolledIntoView} from '../../../../../../functions/isScrolledInroView.function';

import {PostId} from '../../../../../app/EvodeskRESTApi/models/post/Post.model';

import {EvodeskFeedSharedProps} from '../EvodeskFeedContainer.component';

import {EvodeskFeedStateService, FeedEntry} from '../../../state/EvodeskFeedState.service';
import {EvodeskFeedService} from '../../../services/EvodeskFeed.service';

export interface EvodeskFeedDefaultComponentProps extends EvodeskFeedSharedProps {}

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-feed-default',
    templateUrl: './EvodeskFeedDefault.component.pug',
    styleUrls: [
        './EvodeskFeedDefault.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedDefaultComponent implements OnChanges, AfterViewInit, OnDestroy
{
    @Input() props: EvodeskFeedDefaultComponentProps;

    @Output('loadMore') loadMoreEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('stopEditPost') stopEditPostEvent: EventEmitter<PostId> = new EventEmitter<PostId>();

    @ViewChild('loadMoreTrigger') loadMoreTrigger: ElementRef;

    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private requestAnimationFrameId: any;

    public state: State = {
        ready: false,
    };

    constructor(
        private feed: EvodeskFeedService,
        private feedState: EvodeskFeedStateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngAfterViewInit(): void {
        this.feedState.checkLoadMore$.subscribe(() => {
            if (this.props && this.props.hasMoreToLoad) {
                const trigger: Element = document.querySelector('#evodeskFeedLoadMore');
                if (!!trigger && isScrolledIntoView(trigger)) {
                    this.loadMore();
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);

        if (this.requestAnimationFrameId) {
            window.cancelAnimationFrame(this.requestAnimationFrameId);
        }
    }

    get containerCSSClasses(): any {
        return {
            [`__context-${<any>this.props.context}`]: true,
        };
    }

    get entries(): Array<FeedEntry> {
        return this.feed.getEntries();
    }

    trackBy(index: number, entry: FeedEntry): any {
        return entry.sid;
    }

    isEditModeEnabled(postId: PostId): boolean {
        return !!~this.props.editing.indexOf(postId);
    }

    loadMore(): void {
        this.loadMoreEvent.emit(undefined);
    }

    stopEditPost(postId: PostId): void {
        this.stopEditPostEvent.emit(postId);
    }
}
