import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild} from '@angular/core';

import {Observable, Subject} from 'rxjs';

import {isScrolledIntoView} from '../../../../../../functions/isScrolledInroView.function';

import {PostId} from '../../../../../app/EvodeskRESTApi/models/post/Post.model';

import {EvodeskFeedSharedProps} from '../EvodeskFeedContainer.component';

import {EvodeskFeedStateService, FeedEntry} from '../../../state/EvodeskFeedState.service';
import {EvodeskFeedService} from '../../../services/EvodeskFeed.service';
import {EvodeskDateService} from '../../../../../app/EvodeskApp/services/EvodeskDate.service';

interface FeedEntriesGroup {
    trackBy: string;
    timestamp: Date;
    timestamp$?: Observable<string>;
    entries: Array<FeedEntry>;
}

export interface EvodeskFeedGroupedByDateComponentProps extends EvodeskFeedSharedProps {}

interface State {
    ready: boolean;
    groups: Array<FeedEntriesGroup>;
}

@Component({
    selector: 'evodesk-feed-grouped-by-date',
    templateUrl: './EvodeskFeedGroupedByDate.component.pug',
    styleUrls: [
        './EvodeskFeedGroupedByDate.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedGroupedByDateComponent implements OnChanges, AfterViewInit, OnDestroy
{
    @Input() props: EvodeskFeedGroupedByDateComponentProps;

    @Output('loadMore') loadMoreEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('stopEditPost') stopEditPostEvent: EventEmitter<PostId> = new EventEmitter<PostId>();

    @ViewChild('loadMoreTrigger') loadMoreTrigger: ElementRef;

    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private requestAnimationFrameId: any;

    public state: State = {
        ready: false,
        groups: [],
    };

    constructor(
        private feed: EvodeskFeedService,
        private feedState: EvodeskFeedStateService,
        private evodeskDateService: EvodeskDateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                if (changes['props'].isFirstChange() || (changes['props'].currentValue as Array<FeedEntry>) !== (changes['props'].previousValue as Array<FeedEntry>)) {
                    this.state = {
                        ...this.state,
                        ready: true,
                        groups: (() => {
                            if (this.props.entries.length) {
                                const entriesGroupMap: { [timestamp: string]: FeedEntriesGroup } = {};

                                this.props.entries.forEach((m) => {
                                    const messageTimestamp: string = (new Date(
                                        m.date.getFullYear(),
                                        m.date.getMonth(),
                                        m.date.getDate(),
                                        0, 0, 0,
                                    )).getTime().toString();

                                    const day: Date = new Date(
                                        m.date.getFullYear(),
                                        m.date.getMonth(),
                                        m.date.getDate(),
                                        0, 0, 0,
                                    );

                                    if (! entriesGroupMap[messageTimestamp]) {
                                        entriesGroupMap[messageTimestamp] = {
                                            trackBy: 'does-not-matter-at-this-point',
                                            timestamp: day,
                                            entries: [],
                                        };
                                    }

                                    entriesGroupMap[messageTimestamp].entries.push(m);
                                });

                                const result: Array<FeedEntriesGroup> = [];
                                const allTimestamps: Array<number> = Object.keys(entriesGroupMap).map(k => parseInt(k, 10));

                                allTimestamps.sort((a, b) => {
                                    return b - a;
                                });

                                allTimestamps.forEach((timestamp) => {
                                    const tDate: Date = new Date();

                                    tDate.setTime(timestamp);

                                    entriesGroupMap[timestamp.toString()].entries.sort((a, b) => {
                                        return a.date.getTime() - b.date.getTime();
                                    });

                                    result.push({
                                        trackBy: timestamp.toString(),
                                        timestamp: new Date(timestamp),
                                        timestamp$: this.evodeskDateService.diffFromNowV6(tDate),
                                        entries: entriesGroupMap[timestamp.toString()].entries,
                                    });
                                });

                                return result;
                            } else {
                                return [];
                            }
                        })(),
                    };
                }
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngAfterViewInit(): void {
        this.feedState.checkLoadMore$.subscribe(() => {
            if (this.props && this.props.hasMoreToLoad) {
                const trigger: Element = document.querySelector('#evodeskFeedLoadMore');

                if (!!trigger && isScrolledIntoView(trigger)) {
                    this.loadMore();
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);

        if (this.requestAnimationFrameId) {
            window.cancelAnimationFrame(this.requestAnimationFrameId);
        }
    }

    get containerCSSClasses(): any {
        return {
            [`__context-${<any>this.props.context}`]: true,
        };
    }

    get entries(): Array<FeedEntry> {
        return this.feed.getEntries();
    }

    trackBy(index: number, entry: FeedEntry): any {
        return entry.sid;
    }

    trackByGroup(index: number, group: FeedEntriesGroup): any {
        return group.trackBy;
    }

    isEditModeEnabled(postId: PostId): boolean {
        return !!~this.props.editing.indexOf(postId);
    }

    loadMore(): void {
        this.loadMoreEvent.emit(undefined);
    }

    stopEditPost(postId: PostId): void {
        this.stopEditPostEvent.emit(postId);
    }
}
