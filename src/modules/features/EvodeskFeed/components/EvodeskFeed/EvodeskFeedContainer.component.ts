import {ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, takeUntil} from 'rxjs/operators';

import {PostId} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

import {EvodeskFeedContext} from '../../models/EvodeskFeed.model';

import {EvodeskFeedStateAction, EvodeskFeedStateService, FeedEntry} from '../../state/EvodeskFeedState.service';
import {EvodeskFeedService} from '../../services/EvodeskFeed.service';
import {EvodeskFeedBreakpointsService} from '../../services/EvodeskFeedBreakpoint.service';

interface State {
    ready: boolean;
    props?: EvodeskFeedSharedProps;
}

export interface EvodeskFeedSharedProps {
    hasMoreToLoad: boolean;
    loadingMore: boolean;
    entries: Array<FeedEntry>;
    editing: Array<PostId>;
    context: EvodeskFeedContext;
}

export enum EvodeskFeedViewMode
{
    Default = 'default',
    GroupedByDate = 'grouped-by-date',
}

@Component({
    selector: 'evodesk-feed-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <ng-container *ngIf="viewMode === 'default'">
          <evodesk-feed-default
            [props]="state.props"
            (loadMore)="loadMore()"
            (stopEditPost)="stopEditPost($event)"
          ></evodesk-feed-default>
        </ng-container>
        <ng-container *ngIf="viewMode === 'grouped-by-date'">
          <evodesk-feed-grouped-by-date
            [props]="state.props"
            (loadMore)="loadMore()"
            (stopEditPost)="stopEditPost($event)"
          ></evodesk-feed-grouped-by-date>
        </ng-container>
      </ng-container>
    `,
    providers: [
        EvodeskFeedBreakpointsService,
    ],
})
export class EvodeskFeedContainerComponent implements OnInit, OnDestroy, OnChanges
{
    @Input() context: EvodeskFeedContext = EvodeskFeedContext.Default;
    @Input() viewMode: EvodeskFeedViewMode = EvodeskFeedViewMode.Default;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private feedState: EvodeskFeedStateService,
        private feed: EvodeskFeedService,
        private breakpoints: EvodeskFeedBreakpointsService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.feedState.snapshot.context !== this.context) {
            this.feedState.dispatch({
                type: EvodeskFeedStateAction.SetContext,
                payload: {
                    context: this.context,
                },
            });
        }
    }

    ngOnInit(): void {
        this.breakpoints.init();

        this.breakpoints.current$.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            this.feedState.dispatch({
                type: EvodeskFeedStateAction.SetViewMode,
                payload: {
                    viewMode: next,
                },
            });
        });

        this.feedState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !!s.current),
            distinctUntilChanged(),
        ).subscribe(s => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    loadingMore: s.current.loadingMore,
                    hasMoreToLoad: s.current.hasPostsToLoad,
                    entries: s.current.posts,
                    editing: s.current.editing,
                    context: s.current.context,
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    loadMore(): void {
        this.feed.config.source.loadMore().pipe(takeUntil(this.ngOnDestroy$)).subscribe(
            () => {
                this.cdr.detectChanges();
            },
            () => {
                this.cdr.detectChanges();
            },
        );
    }

    stopEditPost(postId: PostId): void {
        this.feedState.dispatch({
            type: EvodeskFeedStateAction.StopEditPost,
            payload: {
                postId: postId,
            },
        });

        this.cdr.detectChanges();
    }
}
