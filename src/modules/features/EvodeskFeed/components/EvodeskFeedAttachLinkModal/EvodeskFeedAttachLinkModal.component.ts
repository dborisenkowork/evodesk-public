import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

export interface EvodeskFeedAttachLinkModalComponentData {
    url: string;
    doSubmit: boolean;
}

@Component({
    selector: 'evodesk-feed-attach-link-modal',
    templateUrl: './EvodeskFeedAttachLinkModal.component.pug',
    styleUrls: [
        './EvodeskFeedAttachLinkModal.component.scss',
    ],
})
export class EvodeskFeedAttachLinkModalComponent
{
    constructor(
        private matDialogRef: MatDialogRef<EvodeskFeedAttachLinkModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: EvodeskFeedAttachLinkModalComponentData,
    ) {}

    cancel(): void {
        this.data.doSubmit = false;

        this.matDialogRef.close();
    }

    submit(): void {
        this.data.doSubmit = true;

        this.matDialogRef.close();
    }
}
