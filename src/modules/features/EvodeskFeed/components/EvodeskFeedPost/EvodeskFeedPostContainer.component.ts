import {ChangeDetectorRef, Component, ComponentRef, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChange, SimpleChanges, ViewChild, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ComponentPortal} from '@angular/cdk/portal';
import {FlexibleConnectedPositionStrategy, Overlay, OverlayRef} from '@angular/cdk/overlay';

import {forkJoin as observableForkJoin, Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {proxy} from '../../../../../functions/proxy.function';

import {PostModel} from '../../../../app/EvodeskRESTApi/models/post/Post.model';
import {ProfileModel} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {FeedViewMode} from '../../models/EvodeskFeed.model';

import {EvodeskFeedPostComponent, EvodeskFeedPostComponentProps, EvodeskFeedPostViewMode, ToggleCommentsEvent} from './EvodeskFeedPost.component';
import {EvodeskFeedRepostModalComponent, EvodeskFeedRepostModalComponentProps} from '../EvodeskFeedRepostModal/EvodeskFeedRepostModal.component';
import {EvodeskFeedPostLikesFloatContainerComponent} from '../EvodeskFeedPostLikesFloat/EvodeskFeedPostLikesFloatContainer.component';
import {EvodeskFeedPostRepostsFloatContainerComponent} from '../EvodeskFeedPostRepostsFloat/EvodeskFeedPostRepostsFloatContainer.component';
import {EvodeskFeedPostLikesModalContainerComponent} from '../EvodeskFeedPostLikesModal/EvodeskFeedPostLikesModalContainer.component';
import {EvodeskFeedPostRepostsModalContainerComponent} from '../EvodeskFeedPostRepostsModal/EvodeskFeedPostRepostsModalContainer.component';

import {defaultBottomMatDialogConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {EvodeskFeedBodyParserService} from '../../services/EvodeskFeedBodyParser.service';
import {PostRESTService} from '../../../../app/EvodeskRESTApi/services/PostREST.service';
import {EvodeskConfirmModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {EvodeskFeedStateAction, EvodeskFeedStateService} from '../../state/EvodeskFeedState.service';
import {EvodeskProfileRepositoryService} from '../../../EvodeskProfile/services/EvodeskProfileRepository.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';

interface State {
    ready: boolean;
    props?: EvodeskFeedPostComponentProps;
    profile?: ProfileModel;
    matDialogRef?: MatDialogRef<any> | undefined;
    overlayRef?: OverlayRef;
}

@Component({
    selector: 'evodesk-feed-post-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-feed-post
          [props]="state.props"
          (toggleLike)="onToggleLike()"
          (edit)="onEdit()"
          (pin)="onPin()"
          (unpin)="onUnpin()"
          (remove)="onRemove()"
          (repost)="onRepost()"
          (toggleComments)="onToggleComments($event)"
          (close)="onClose()"
        ></evodesk-feed-post>
      </ng-container>
    `,
})
export class EvodeskFeedPostContainerComponent implements OnChanges, OnDestroy
{
    @Input() post: PostModel;
    @Input() viewMode: EvodeskFeedPostViewMode = EvodeskFeedPostViewMode.Default;

    @Output() removed: EventEmitter<PostModel> = new EventEmitter<PostModel>();
    @Output() removedError: EventEmitter<PostModel> = new EventEmitter<PostModel>();
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild(EvodeskFeedPostComponent) view: EvodeskFeedPostComponent;

    private destroyFloat$: Subject<void> = new Subject<void>();
    private destroyModal$: Subject<void> = new Subject<void>();
    private nextLikeAction$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private matDialog: MatDialog,
        private uiConfirmService: EvodeskConfirmModalService,
        private feedStateService: EvodeskFeedStateService,
        private bodyParser: EvodeskFeedBodyParserService,
        private profileRepository: EvodeskProfileRepositoryService,
        private postRESTService: PostRESTService,
        private currentUserService: CurrentUserService,
        private overlay: Overlay,
        private viewContainerRef: ViewContainerRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['post']) {
            if (this.post) {
                const queries: Array<Observable<any>> = [];

                this.state = {
                    ...this.state,
                    ready: false,
                    props: {
                        ...this.state.props,
                        post: this.post,
                        isOwnPost: this.isOwn,
                        hasComments: this.post.comments > 0,
                        viewMode: this.viewMode,
                        context: this.feedStateService.snapshot.context,
                    },
                };

                queries.push(Observable.create(q => {
                    const unsubscribe$: Subject<void> = new Subject<void>();

                    this.bodyParser.parseToHtml(this.post.text).pipe(takeUntil(unsubscribe$)).subscribe(
                        (parsed) => {
                            this.state = {
                                ...this.state,
                                ready: true,
                                props: {
                                    ...this.state.props,
                                    parsed: parsed,
                                },
                            };

                            q.next(undefined);
                            q.complete();
                        },
                        (error) => {
                            q.error(error);
                        },
                    );
                }));

                queries.push(Observable.create(q => {
                    const unsubscribe$: Subject<void> = new Subject<void>();

                    this.profileRepository.getProfile(this.post.user_id).pipe(takeUntil(unsubscribe$)).subscribe(
                        (profile) => {
                            this.state = {
                                ...this.state,
                                profile: profile,
                                props: {
                                    ...this.state.props,
                                    profile: profile,
                                },
                            };

                            q.next(undefined);
                            q.complete();
                        },
                        (error) => {
                            q.error(error);
                        },
                    );
                }));

                observableForkJoin(queries).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                    this.state = {
                        ...this.state,
                        ready: true,
                    };

                    this.cdr.detectChanges();
                });
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }

            this.cdr.detectChanges();
        }

        if (changes['viewMode'] && this.state.props) {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    viewMode: this.viewMode,
                },
            };
        }
    }

    ngOnDestroy(): void {
        if (this.state.matDialogRef) {
            this.state.matDialogRef.close();
        }

        this.destroyFloats();

        this.destroyFloat$.next(undefined);
        this.destroyModal$.next(undefined);

        this.ngOnDestroy$.next(undefined);
    }

    get isOwn(): boolean {
        return this.post.user_id.toString() === this.currentUserService.impersonatedAs.id.toString();
    }

    get isMobile(): boolean {
        return this.feedStateService.snapshot.viewMode === FeedViewMode.Mobile;
    }

    get isDesktop(): boolean {
        return this.feedStateService.snapshot.viewMode === FeedViewMode.Tablet || this.feedStateService.snapshot.viewMode === FeedViewMode.Desktop;
    }

    destroyFloats(): void {
        this.destroyFloat$.next(undefined);

        if (this.state.overlayRef) {
            this.state.overlayRef.dispose();

            this.state = {
                ...this.state,
                overlayRef: undefined,
            };
        }
    }

    openLikesFloat(): void {
        const scrollStrategy: any = this.overlay.scrollStrategies.close();
        const positionStrategy: FlexibleConnectedPositionStrategy = this.overlay.position().flexibleConnectedTo(this.view.likesIconRef);

        positionStrategy.withPositions([
            {
                offsetX: 0,
                offsetY: 15,
                originX: 'center',
                originY: 'bottom',
                overlayX: 'center',
                overlayY: 'top',
            },
            {
                offsetX: 0,
                offsetY: -15,
                originX: 'center',
                originY: 'top',
                overlayX: 'center',
                overlayY: 'bottom',
            },
        ]);

        const overlayRef: OverlayRef = this.overlay.create({
            positionStrategy: positionStrategy,
            scrollStrategy: scrollStrategy,
            hasBackdrop: true,
            backdropClass: '__evodesk-mat-dialog-transparent-backdrop',
        });

        const portal: ComponentPortal<EvodeskFeedPostLikesFloatContainerComponent> = new ComponentPortal(EvodeskFeedPostLikesFloatContainerComponent, this.viewContainerRef);
        const componentRef: ComponentRef<EvodeskFeedPostLikesFloatContainerComponent> = overlayRef.attach(portal);

        componentRef.instance.post = this.post;
        componentRef.instance.ngOnChanges({ ['post']: new SimpleChange(undefined, this.post.id, true) });

        componentRef.instance.detailsEvent.pipe(takeUntil(this.destroyFloat$)).subscribe(() => {
            this.destroyFloats();
            this.openLikesModal();
        });

        overlayRef.backdropClick().pipe(takeUntil(this.destroyFloat$)).subscribe(() => {
            this.destroyFloats();
        });

        overlayRef.backdropElement.addEventListener('mousewheel', () => {
            this.destroyFloats();
        });

        overlayRef.backdropElement.addEventListener('touchmove', () => {
            this.destroyFloats();
        });

        this.state = {
            ...this.state,
            overlayRef: overlayRef,
        };
    }

    openRepostsFloat(): void {
        const scrollStrategy: any = this.overlay.scrollStrategies.close();
        const positionStrategy: FlexibleConnectedPositionStrategy = this.overlay.position().flexibleConnectedTo(this.view.repostIconRef);

        positionStrategy.withPositions([
            {
                offsetX: 0,
                offsetY: 15,
                originX: 'center',
                originY: 'bottom',
                overlayX: 'center',
                overlayY: 'top',
            },
            {
                offsetX: 0,
                offsetY: -15,
                originX: 'center',
                originY: 'top',
                overlayX: 'center',
                overlayY: 'bottom',
            },
        ]);

        const overlayRef: OverlayRef = this.overlay.create({
            positionStrategy: positionStrategy,
            scrollStrategy: scrollStrategy,
            hasBackdrop: true,
            backdropClass: '__evodesk-mat-dialog-transparent-backdrop',
        });

        const portal: ComponentPortal<EvodeskFeedPostRepostsFloatContainerComponent> = new ComponentPortal(EvodeskFeedPostRepostsFloatContainerComponent, this.viewContainerRef);
        const componentRef: ComponentRef<EvodeskFeedPostRepostsFloatContainerComponent> = overlayRef.attach(portal);

        componentRef.instance.post = this.post;
        componentRef.instance.ngOnChanges({ ['post']: new SimpleChange(undefined, this.post.id, true) });

        componentRef.instance.detailsEvent.pipe(takeUntil(this.destroyFloat$)).subscribe(() => {
            this.destroyFloats();
            this.openRepostsModal();
        });

        overlayRef.backdropClick().pipe(takeUntil(this.destroyFloat$)).subscribe(() => {
            this.destroyFloats();
        });

        overlayRef.backdropElement.addEventListener('mousewheel', () => {
            this.destroyFloats();
        });

        overlayRef.backdropElement.addEventListener('touchmove', () => {
            this.destroyFloats();
        });

        this.state = {
            ...this.state,
            overlayRef: overlayRef,
        };
    }

    openLikesModal(): void {
        const matDialogRef: MatDialogRef<EvodeskFeedPostLikesModalContainerComponent> = this.matDialog.open(EvodeskFeedPostLikesModalContainerComponent, {
            ...defaultBottomMatDialogConfig,
        });

        this.state = {
            ...this.state,
            matDialogRef: matDialogRef,
        };

        matDialogRef.componentInstance.post = this.post;
        matDialogRef.componentInstance.ngOnChanges({ 'post': new SimpleChange(undefined, this.post, true) });

        matDialogRef.componentInstance.closeEvent.pipe(takeUntil(this.destroyModal$)).subscribe(() => {
            matDialogRef.close();
        });

        matDialogRef.afterClosed().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.destroyModal$.next(undefined);

            this.state = {
                ...this.state,
                matDialogRef: undefined,
            };
        });
    }

    openRepostsModal(): void {
        const matDialogRef: MatDialogRef<EvodeskFeedPostRepostsModalContainerComponent> = this.matDialog.open(EvodeskFeedPostRepostsModalContainerComponent, {
            ...defaultBottomMatDialogConfig,
        });

        this.state = {
            ...this.state,
            matDialogRef: matDialogRef,
        };

        matDialogRef.componentInstance.post = this.post;
        matDialogRef.componentInstance.ngOnChanges({ 'post': new SimpleChange(undefined, this.post, true) });

        matDialogRef.componentInstance.closeEvent.pipe(takeUntil(this.destroyModal$)).subscribe(() => {
            matDialogRef.close();
        });

        matDialogRef.afterClosed().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.destroyModal$.next(undefined);

            this.state = {
                ...this.state,
                matDialogRef: undefined,
            };
        });
    }

    onRemove(): void {
        this.uiConfirmService.open({
            title: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.ConfirmRemove.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.ConfirmRemove.Text',
                translate: true,
            },
            cancelButton: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.ConfirmRemove.Cancel',
                translate: true,
            },
            confirmButton: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.ConfirmRemove.Confirm',
                translate: true,
            },
            confirm: () => {
                this.removed.emit(this.post);

                this.feedStateService.dispatch({
                    type: EvodeskFeedStateAction.RemovePost,
                    payload: {
                        post: this.post,
                    },
                });

                proxy(this.postRESTService.deletePost(this.post.id)).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                    undefined,
                    () => {
                        this.removedError.emit(this.post);
                    },
                );
            },
        }).subscribe();
    }

    onEdit(): void {
        this.feedStateService.dispatch({
            type: EvodeskFeedStateAction.MarkPostAsEditing,
            payload: {
                post: this.post,
            },
        });
    }

    onPin(): void {
        this.feedStateService.dispatch({
            type: EvodeskFeedStateAction.PinPost,
            payload: {
                post: this.post,
            },
        });

        proxy(this.postRESTService.savePost({
            ...this.post,
            pinned: '1',
        })).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }

    onUnpin(): void {
        this.feedStateService.dispatch({
            type: EvodeskFeedStateAction.UnpinPost,
            payload: {
                post: this.post,
            },
        });

        proxy(this.postRESTService.savePost({
            ...this.post,
            pinned: '0',
        })).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }

    onToggleLike(): void {
        if (this.isOwn) {
            if (this.post.likes > 0) {
                if (this.state.overlayRef) {
                    this.destroyFloats();
                } else {
                    if (this.isDesktop) {
                        this.openLikesFloat();
                    } else {
                        this.destroyFloats();
                        this.openLikesModal();
                    }
                }
            }
        } else {
            this.nextLikeAction$.next(undefined);

            if (this.post.isLiked) {
                this.feedStateService.dispatch({
                    type: EvodeskFeedStateAction.PostDislike,
                    payload: {
                        post: this.post,
                    },
                });

                proxy(
                    this.postRESTService.unlikePost(this.post.id).pipe(takeUntil(this.nextLikeAction$)),
                ).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
            } else {
                this.feedStateService.dispatch({
                    type: EvodeskFeedStateAction.LikePost,
                    payload: {
                        post: this.post,
                    },
                });

                proxy(
                    this.postRESTService.likePost(this.post.id).pipe(takeUntil(this.nextLikeAction$)),
                ).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
            }
        }
    }

    onRepost(): void {
        if (this.isOwn) {
            if (this.post.reposts > 0) {
                if (this.state.overlayRef) {
                    this.destroyFloats();
                } else {
                    if (this.isDesktop) {
                        this.openRepostsFloat();
                    } else {
                        this.destroyFloats();
                        this.openRepostsModal();
                    }
                }
            }
        } else {
            const deepPost: Function = (input: PostModel) => {
                if (input.post) {
                    return deepPost(input.post);
                } else {
                    return input;
                }
            };

            const matDialogRef: MatDialogRef<EvodeskFeedRepostModalComponent> = this.matDialog.open(EvodeskFeedRepostModalComponent, {
                ...defaultBottomMatDialogConfig,
                data: <EvodeskFeedRepostModalComponentProps>{
                    post: deepPost(this.post),
                    authorProfile: this.state.profile,
                    reportToProfile: this.currentUserService.impersonatedAs.id,
                },
            });

            matDialogRef.componentInstance.successEvent.pipe(takeUntil(this.destroyModal$)).subscribe(() => {
                this.feedStateService.dispatch({
                    type: EvodeskFeedStateAction.MarkAsReposted,
                    payload: {
                        postId: this.post.id,
                    },
                });
            });

            matDialogRef.afterClosed().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                this.destroyModal$.next(undefined);

                this.state = {
                    ...this.state,
                    matDialogRef: undefined,
                };
            });

            this.state = {
                ...this.state,
                matDialogRef: matDialogRef,
            };
        }
    }

    onToggleComments($event: ToggleCommentsEvent): void {
        if ($event.isEnabled) {
            this.feedStateService.dispatch({
                type: EvodeskFeedStateAction.ClearPostComments,
                payload: {
                    postId: this.post.id,
                },
            });
        }
    }

    onClose(): void {
        this.closeEvent.emit(undefined);
    }
}
