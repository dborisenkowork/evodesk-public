import {AfterViewChecked, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild} from '@angular/core';

import {Subject, Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import * as moment from 'moment';

import {AttachmentListItem, PostModel} from '../../../../app/EvodeskRESTApi/models/post/Post.model';
import {ProfileModel} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {EvodeskFeedCommentEntity} from '../../models/EvodeskFeedCommentEntity.model';

import {getAttachmentsOfPost} from '../../functions/getAttachmentsOfPost.function';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../types/Backend.types';

import {EvodeskFeedCommentFormPostEntity, SupportedCommentType} from '../EvodeskFeedCommentForm/EvodeskFeedCommentFormContainer.component';
import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';
import {EvodeskFeedContext, evodeskFeedNewsContexts} from '../../models/EvodeskFeed.model';

export enum EvodeskFeedPostViewMode {
    Default = 'default',
    Modal = 'modal',
    Notification = 'notification',
}

export interface ToggleCommentsEvent {
    isEnabled: boolean;
}

export interface EvodeskFeedPostComponentProps {
    post: PostModel;
    parsed: string;
    isOwnPost: boolean;
    profile: ProfileModel;
    hasComments: boolean;
    viewMode: EvodeskFeedPostViewMode;
    context: EvodeskFeedContext;
}

interface State {
    ready: boolean;
    expanded: boolean;
    shouldTruncate: boolean;
    isCommentsSectionOpened: boolean;
    commentEntity?: EvodeskFeedCommentFormPostEntity;
    commentsListEntity?: EvodeskFeedCommentEntity;
    createdAt?: Observable<string>;
}

@Component({
    selector: 'evodesk-feed-post',
    templateUrl: './EvodeskFeedPost.component.pug',
    styleUrls: [
        './EvodeskFeedPost.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostComponent implements OnChanges, AfterViewChecked, OnDestroy
{
    @Input() props: EvodeskFeedPostComponentProps;

    @Output('edit') editEvent: EventEmitter<PostModel> = new EventEmitter<PostModel>();
    @Output('pin') pinEvent: EventEmitter<PostModel> = new EventEmitter<PostModel>();
    @Output('unpin') unpinEvent: EventEmitter<PostModel> = new EventEmitter<PostModel>();
    @Output('remove') removeEvent: EventEmitter<PostModel> = new EventEmitter<PostModel>();
    @Output('toggleLike') toggleLikeEvent: EventEmitter<PostModel> = new EventEmitter<PostModel>();
    @Output('repost') repostEvent: EventEmitter<PostModel> = new EventEmitter<PostModel>();
    @Output('toggleComments') toggleCommentsEvent: EventEmitter<ToggleCommentsEvent> = new EventEmitter<ToggleCommentsEvent>();
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('likesIcon') likesIconRef: ElementRef;
    @ViewChild('repostIcon') repostIconRef: ElementRef;
    @ViewChild('textSpan') textSpan: ElementRef;

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        expanded: false,
        shouldTruncate: false,
        isCommentsSectionOpened: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private evodeskDateService: EvodeskDateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    createdAt: this.evodeskDateService.diffFromNowV3(moment(this.props.post.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate()).pipe(takeUntil(this.ngOnChanges$)),
                    commentEntity: {
                        type: SupportedCommentType.Post,
                        post: this.props.post,
                    },
                    commentsListEntity: {
                        type: SupportedCommentType.Post,
                        post: this.props.post,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngAfterViewChecked(): void {
        if (this.textSpan !== undefined && this.textSpan.nativeElement.offsetHeight > 390) {
            this.state.shouldTruncate = true;
            this.cdr.detectChanges();
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    get containerCSSClasses(): any {
        return {
            '__view-mode-default': this.props.viewMode === EvodeskFeedPostViewMode.Default,
            '__view-mode-modal': this.props.viewMode === EvodeskFeedPostViewMode.Modal,
            '__view-mode-notification': this.props.viewMode === EvodeskFeedPostViewMode.Notification,
            'is-comments-section-opened': this.state.isCommentsSectionOpened,
            'is-pinned': this.props.post.pinned === '1',
            'is-profile-post': this.props.context === EvodeskFeedContext.Profile,
            'is-news-post': !!~evodeskFeedNewsContexts.indexOf(this.props.context),
        };
    }

    get isRepost(): boolean {
        return !! this.props.post.post;
    }

    get isPinned(): boolean {
        return this.props.post.pinned.toString() === '1';
    }

    get hasText(): boolean {
        return this.props.post.text && this.props.post.text.length > 0;
    }

    get attachments(): Array<AttachmentListItem> {
        return getAttachmentsOfPost(this.props.post);
    }

    get isContextMenuAvailable(): boolean {
        return this.props.isOwnPost && this.props.viewMode !== EvodeskFeedPostViewMode.Notification;
    }

    get shouldDisplayPinnedShortHeader(): boolean {
        return this.props.post.pinned === '1' && this.props.context === EvodeskFeedContext.Profile;
    }

    expand(): void {
        this.state.expanded = true;
    }

    edit(): void {
        this.editEvent.emit(undefined);
    }

    pin(): void {
        this.pinEvent.emit(undefined);
    }

    unpin(): void {
        this.unpinEvent.emit(undefined);
    }

    remove(): void {
        this.removeEvent.emit(this.props.post);
    }

    toggleLike(): void {
        this.toggleLikeEvent.emit(this.props.post);
    }

    repost(): void {
        this.repostEvent.emit(this.props.post);
    }

    toggleCommentsSection(): void {
        this.state = {
            ...this.state,
            isCommentsSectionOpened: ! this.state.isCommentsSectionOpened,
        };

        this.toggleCommentsEvent.emit({
            isEnabled: this.state.isCommentsSectionOpened,
        });

        this.cdr.detectChanges();
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
