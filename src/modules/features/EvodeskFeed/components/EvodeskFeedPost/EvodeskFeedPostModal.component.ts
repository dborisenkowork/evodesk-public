import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {PostModel} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

@Component({
    selector: 'evodesk-feed-post-modal',
    template: `
      <mat-dialog-content>
        <evodesk-feed-post-container
          [post]="post"
          [viewMode]="'modal'"
          (removed)="removed.emit($event)"
          (removedError)="removedError.emit($event)"
          (close)="closeEvent.emit(undefined)"
        ></evodesk-feed-post-container>
      </mat-dialog-content>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostModalComponent implements OnChanges
{
    @Input() post: PostModel;

    @Output() removed: EventEmitter<PostModel> = new EventEmitter<PostModel>();
    @Output() removedError: EventEmitter<PostModel> = new EventEmitter<PostModel>();
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.cdr.detectChanges();
    }
}
