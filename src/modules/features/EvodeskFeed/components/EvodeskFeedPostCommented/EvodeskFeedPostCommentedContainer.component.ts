import {Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewContainerRef} from '@angular/core';

import * as moment from 'moment';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskFeedPostCommentedComponentProps} from './EvodeskFeedPostCommented.component';

import {FeedPostCommentedEntry} from '../../state/EvodeskFeedState.service';
import {EvodeskFeedBodyParserService} from '../../services/EvodeskFeedBodyParser.service';
import {EvodeskFeedOpenPostService} from '../../services/EvodeskFeedOpenPost.service';
import {EvodeskProfileUrlService} from '../../../EvodeskProfile/shared/services/EvodeskProfileUrl.service';
import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';
import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../types/Backend.types';

type Props = EvodeskFeedPostCommentedComponentProps;

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-feed-post-commented-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-feed-post-commented [props]="state.props" (openPost)="openPost()"></evodesk-feed-post-commented>
      </ng-container>
    `,
})
export class EvodeskFeedPostCommentedContainerComponent implements OnChanges, OnDestroy
{
    @Input() entity: FeedPostCommentedEntry;

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private bodyParser: EvodeskFeedBodyParserService,
        private profileUrl: EvodeskProfileUrlService,
        private openPostService: EvodeskFeedOpenPostService,
        private viewContainerRef: ViewContainerRef,
        private dates: EvodeskDateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (changes['entity']) {
            if (this.entity) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        entity: {
                            authorProfileName: this.entity.commentedBy.name || this.entity.commentedBy.email,
                            authorProfileId: this.entity.commentedBy.id,
                            authorProfileImage: this.entity.commentedBy.images[0],
                            authorProfileUrl: this.profileUrl.getRouterLinkByIdAndUrlAlias(this.entity.commentedBy.id, this.entity.commentedBy.url_alias),
                            comment$: this.bodyParser.parseToHtmlPreview(this.bodyParser.parseCommentToJSOBNNBody(this.entity.comment.text).text),
                            date$: this.dates.diffFromNowV2(moment(this.entity.comment.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate()),
                        },
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    openPost(): void {
        this.openPostService.openPost(this.viewContainerRef, this.entity.comment.post_id.id).pipe(takeUntil(this.ngOnChanges$)).subscribe();
    }
}
