import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {proxy} from '../../../../../functions/proxy.function';

import {EvodeskFeedPostLikesModalComponentProps, EvodeskFeedPostLikesModalComponentPropsEntry} from './EvodeskFeedPostLikesModal.component';

import {PostModel} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

import {PostRESTService} from '../../../../app/EvodeskRESTApi/services/PostREST.service';
import {ProfileRESTService} from '../../../../app/EvodeskRESTApi/services/ProfileREST.service';

type Props = EvodeskFeedPostLikesModalComponentProps;

interface State {
    ready: boolean;
    props?: Props;
    lastLikeId: number | undefined;
}

const numLikesPerPage: number = 15;

@Component({
    selector: 'evodesk-feed-post-likes-modal-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-feed-post-likes-modal [props]="state.props" (next)="next()" (follow)="follow($event)" (close)="close()"></evodesk-feed-post-likes-modal>
      </ng-container>
    `,
})
export class EvodeskFeedPostLikesModalContainerComponent implements OnChanges, OnDestroy
{
    @Input() post: PostModel;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter();

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        lastLikeId: undefined,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private rest: PostRESTService,
        private profileRest: ProfileRESTService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['post']) {
            if (this.post) {
                if (changes['post'].isFirstChange() || (changes['post'].previousValue as PostModel).id !== (changes['post'].currentValue as PostModel).id) {
                    this.ngOnChanges$.next(undefined);

                    this.state = {
                        ...this.state,
                        ready: true,
                        props: {
                            loading: false,
                            numLikes: this.post.likes,
                            entries: [],
                            hasMoreToLoad: false,
                            following: [],
                        },
                    };

                    this.fetch();
                }
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    fetch(beforeId?: number): void {
        if (this.state.props.loading) {
            return;
        }

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                loading: true,
            },
        };

        this.rest.likedBy(this.post.id, beforeId ? { num: numLikesPerPage + 1, before: beforeId } : { num: numLikesPerPage + 1 }).pipe(takeUntil(this.ngOnChanges$)).subscribe(
            (httpResponse) => {
                this.state = {
                    ...this.state,
                    lastLikeId: Math.min.apply(Math, httpResponse.slice(0, numLikesPerPage).map(e => e.like_id)),
                    props: {
                        ...this.state.props,
                        loading: false,
                        hasMoreToLoad: httpResponse.length > numLikesPerPage,
                        entries: [...this.state.props.entries, ...httpResponse.slice(0, numLikesPerPage).map(e => {
                            return <EvodeskFeedPostLikesModalComponentPropsEntry>{
                                profileId: e.id,
                                profileName: e.name || e.email,
                                profileImage: e.avatar,
                                profileStatus: 'Статус',
                                isFollowed: e.is_followed,
                            };
                        })],
                    },
                };

                this.cdr.detectChanges();
            },
            () => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        loading: false,
                    },
                };

                this.cdr.detectChanges();
            },
        );
    }

    next(): void {
        if (this.state.props.hasMoreToLoad) {
            this.fetch(this.state.lastLikeId);
        }

        this.cdr.detectChanges();
    }

    follow(entry: EvodeskFeedPostLikesModalComponentPropsEntry): void {
        if (this.state.props.following.indexOf(entry.profileId) > -1) {
            return;
        }

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                following: [...this.state.props.following, entry.profileId],
            },
        };

        proxy(this.profileRest.followOn(entry.profileId)).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
            () => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        following: this.state.props.following.filter(f => f !== entry.profileId),
                        entries: this.state.props.entries.map(e => {
                            if (e.profileId === entry.profileId) {
                                return {
                                    ...e,
                                    isFollowed: true,
                                };
                            } else {
                                return e;
                            }
                        }),
                    },
                };
            },
            () => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        following: this.state.props.following.filter(f => f !== entry.profileId),
                    },
                };
            },
        );
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
