import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';

import {ProfileAvatar, ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

export interface EvodeskFeedPostRepostsModalComponentPropsEntry {
    profileId: ProfileId;
    profileName: string;
    profileImage: ProfileAvatar;
    profileStatus: string;
    isFollowed: boolean;
}

type Entry = EvodeskFeedPostRepostsModalComponentPropsEntry;

interface State {
    ready: boolean;
}

export interface EvodeskFeedPostRepostsModalComponentProps {
    loading: boolean;
    numReposts: number;
    entries: Array<Entry>;
    hasMoreToLoad: boolean;
    following: Array<ProfileId>;
}

type Props = EvodeskFeedPostRepostsModalComponentProps;

const trigger: number = 100;

@Component({
    selector: 'evodesk-feed-post-reposts-modal',
    templateUrl: './EvodeskFeedPostRepostsModal.component.pug',
    styleUrls: [
        './EvodeskFeedPostRepostsModal.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostRepostsModalComponent implements OnChanges
{
    @Input() props: Props;

    @Output('next') nextEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('follow') followEvent: EventEmitter<Entry> = new EventEmitter<Entry>();
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('area') areaRef: ElementRef;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get entries(): Array<Entry> {
        return this.props.entries;
    }

    isFollowButtonDisabled(entry: Entry): boolean {
        return this.props.following.indexOf(entry.profileId) > -1;
    }

    trackByEntry(index: number, entry: Entry): any {
        return entry.profileId;
    }

    onScroll(): void {
        const elem: HTMLElement = this.areaRef.nativeElement as HTMLElement;

        if ((elem.scrollTop + elem.clientHeight) >= (elem.scrollHeight - trigger)) {
            this.next();
        }
    }

    follow(entry: Entry): void {
        this.followEvent.emit(entry);
    }

    next(): void {
        this.nextEvent.emit(undefined);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
