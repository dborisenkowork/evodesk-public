import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import {EvodeskFeedCommentJSONBody} from '../../models/EvodeskFeed.model';

import {EvodeskFeedCommentBodyComponentProps} from './EvodeskFeedCommentBody.component';

import {EvodeskFeedBodyParserService} from '../../services/EvodeskFeedBodyParser.service';
import {EvodeskProfileUrlService} from '../../../EvodeskProfile/shared/services/EvodeskProfileUrl.service';

type Props = EvodeskFeedCommentBodyComponentProps;

interface State {
    ready: boolean;
    props?: Props | undefined;
}

@Component({
    selector: 'evodesk-feed-comment-body-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-feed-comment-body [props]="state.props"></evodesk-feed-comment-body>
      </ng-container>
    `,
})
export class EvodeskFeedCommentBodyContainerComponent implements OnChanges, OnDestroy
{
    @Input() body: string | EvodeskFeedCommentJSONBody;
    @Input() depth: number = 1;

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private bodyParser: EvodeskFeedBodyParserService,
        private profileUrlService: EvodeskProfileUrlService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (this.body) {
            const body: EvodeskFeedCommentJSONBody = typeof this.body === 'string' ? this.bodyParser.parseCommentToJSOBNNBody(this.body) : this.body;

            this.state = {
                ...this.state,
                ready: true,
                props: {
                    depth: this.depth,
                    body: body,
                    parsed$: this.bodyParser.parseCommentTextToHtml(body.text),
                    replyProfileUrl: body.reply ? this.profileUrlService.getRouterLinkByProfileId(body.reply.authorProfileId) : undefined,
                },
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
                props: undefined,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }
}
