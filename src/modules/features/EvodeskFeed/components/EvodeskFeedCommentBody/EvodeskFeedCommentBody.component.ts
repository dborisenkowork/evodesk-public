import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

import {Observable} from 'rxjs';

import {EvodeskFeedCommentJSONBody} from '../../models/EvodeskFeed.model';

import {evodeskFeedMaxReplyDepth} from '../../configs/EvodeskFeed.constants';

export interface EvodeskFeedCommentBodyComponentProps {
    depth: number;
    body: EvodeskFeedCommentJSONBody;
    parsed$: Observable<string>;
    replyProfileUrl?: Array<any>;
}

type Props = EvodeskFeedCommentBodyComponentProps;

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-feed-comment-body',
    templateUrl: './EvodeskFeedCommentBody.component.pug',
    styleUrls: [
        './EvodeskFeedCommentBody.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedCommentBodyComponent implements OnChanges
{
    @Input() props: Props;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get hasReply(): boolean {
        return !! this.props.body.reply;
    }

    get shouldGoDeeper(): boolean {
        return this.props.depth < evodeskFeedMaxReplyDepth;
    }
}
