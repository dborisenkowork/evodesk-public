import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

import {SwiperConfigInterface} from 'ngx-swiper-wrapper';

import {AnyAttachmentModel, AttachmentListItem} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

@Component({
    selector: 'evodesk-feed-post-list-attachments',
    templateUrl: './EvodeskFeedPostListAttachments.component.pug',
    styleUrls: [
        './EvodeskFeedPostListAttachments.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostListAttachmentsComponent
{
    @Input() attachments: Array<AttachmentListItem> = [];
    @Input() controls: boolean = false;

    @Output('remove') removeEvent: EventEmitter<AnyAttachmentModel> = new EventEmitter<AnyAttachmentModel>();

    get swiperConfig(): SwiperConfigInterface {
        return {
            slidesPerView: 'auto',
            autoHeight: true,
        };
    }

    remove($event: AnyAttachmentModel): void {
        this.removeEvent.emit($event);
    }

    trackBy(index: number, item: AttachmentListItem): any {
        return item.attachment.id;
    }
}
