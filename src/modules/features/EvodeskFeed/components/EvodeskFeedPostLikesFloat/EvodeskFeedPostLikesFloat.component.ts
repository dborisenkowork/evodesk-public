import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

interface State {
    ready: boolean;
}

export interface EvodeskFeedPostLikesFloatComponentProps {
    loading: boolean;
    numProfiles: number;
    profiles: Array<{
        id: ProfileId;
        name: string;
        avatar: string | undefined | null;
        routerLink: Array<any>;
    }>;
}

type Props = EvodeskFeedPostLikesFloatComponentProps;

@Component({
    selector: 'evodesk-feed-post-likes-float',
    templateUrl: './EvodeskFeedPostLikesFloat.component.pug',
    styleUrls: [
        './EvodeskFeedPostLikesFloat.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostLikesFloatComponent implements OnChanges
{
    @Input() props: Props;

    @Output('details') detailsEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get hasDetails(): boolean {
        return this.props.numProfiles > this.props.profiles.length;
    }

    get detailsParam(): any {
        return {
            count: this.props.numProfiles - this.props.profiles.length,
        };
    }

    details(): void {
        this.detailsEvent.emit(undefined);
    }
}
