import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskFeedPostLikesFloatComponentProps} from './EvodeskFeedPostLikesFloat.component';

import {PostModel} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

import {EvodeskProfileUrlService} from '../../../EvodeskProfile/shared/services/EvodeskProfileUrl.service';
import {PostRESTService} from '../../../../app/EvodeskRESTApi/services/PostREST.service';

type Props = EvodeskFeedPostLikesFloatComponentProps;

interface State {
    ready: boolean;
    props: Props;
}

const numProfiles: number = 3;

@Component({
    selector: 'evodesk-feed-post-likes-float-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-feed-post-likes-float [props]="state.props" (details)="details()"></evodesk-feed-post-likes-float>
      </ng-container>
    `,
})
export class EvodeskFeedPostLikesFloatContainerComponent implements OnChanges, OnDestroy
{
    @Input() post: PostModel;

    @Output('details') detailsEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: {
            loading: false,
            profiles: [],
            numProfiles: 0,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private rest: PostRESTService,
        private urlFactory: EvodeskProfileUrlService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (this.post) {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    loading: true,
                },
            };

            this.rest.likedBy(this.post.id, { num: numProfiles }).pipe(takeUntil(this.ngOnChanges$)).subscribe(
                (httpResponse) => {
                    this.state = {
                        ...this.state,
                        props: {
                            loading: false,
                            numProfiles: this.post.likes,
                            profiles: httpResponse.map((p) => {
                                return {
                                    id: p.id,
                                    name: p.name || p.email,
                                    avatar: p.avatar,
                                    routerLink: this.urlFactory.getRouterLinkByProfileId(p.id),
                                };
                            }),
                        },
                    };

                    this.cdr.detectChanges();
                },
                () => {
                    this.state = {
                        ...this.state,
                        props: {
                            loading: false,
                            numProfiles: 0,
                            profiles: [],
                        },
                    };

                    this.cdr.detectChanges();
                },
            );
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    details(): void {
        this.detailsEvent.emit(undefined);
    }
}
