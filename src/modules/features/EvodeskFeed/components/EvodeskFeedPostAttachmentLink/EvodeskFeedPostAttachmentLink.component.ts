import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

import {AttachmentLinkModel} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

import * as parseURL from 'url-parse';

@Component({
    selector: 'evodesk-feed-post-attachment-link',
    templateUrl: './EvodeskFeedPostAttachmentLink.component.pug',
    styleUrls: [
        './EvodeskFeedPostAttachmentLink.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostAttachmentLinkComponent
{
    @Input() attachment: AttachmentLinkModel;
    @Input() controls: boolean = false;

    @Output('remove') removeEvent: EventEmitter<AttachmentLinkModel> = new EventEmitter<AttachmentLinkModel>();

    get name(): string {
        return this.attachment.name || this.attachment.path;
    }

    get url(): string {
        return this.attachment.path;
    }

    get domainUrl(): string {
        return `${parseURL(this.attachment.path).protocol}//${parseURL(this.attachment.path).hostname}`;
    }

    get hostname(): string {
        return parseURL(this.attachment.path).hostname;
    }

    get hasImage(): boolean {
        return !! this.attachment.preview;
    }

    get previewImage(): any {
        return this.attachment.preview;
    }

    remove($event: MouseEvent): void {
        $event.preventDefault();
        $event.stopPropagation();

        this.removeEvent.emit(this.attachment);
    }
}
