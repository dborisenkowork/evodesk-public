import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {Observable} from 'rxjs';

import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

export interface EvodeskFeedPostLikedComponentInputEntity {
    authorProfileId: ProfileId;
    authorProfileImage: string | null;
    authorProfileName: string;
    authorProfileUrl: Array<any>;
    title$: Observable<string>;
    date$: Observable<string>;
}

export interface EvodeskFeedPostLikedComponentProps {
    entity: EvodeskFeedPostLikedComponentInputEntity;
}

type Props = EvodeskFeedPostLikedComponentProps;

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-feed-post-liked',
    templateUrl: './EvodeskFeedPostLiked.component.pug',
    styleUrls: [
        './EvodeskFeedPostLiked.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostLikedComponent implements OnChanges
{
    @Input() props: Props;

    @Output('openPost') openPostEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    openPost(): void {
        this.openPostEvent.emit(undefined);
    }
}
