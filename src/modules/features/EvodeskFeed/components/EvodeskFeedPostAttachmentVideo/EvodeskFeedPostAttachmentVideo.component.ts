import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {AttachmentVideoModel} from '../../../../app/EvodeskRESTApi/models/post/Post.model';
import {DomSanitizer} from '@angular/platform-browser';

interface State {
    trustedUrl: any;
    preview: string;
    viewMode: 'preview' | 'play';
}

@Component({
    selector: 'evodesk-feed-post-attachment-video',
    templateUrl: './EvodeskFeedPostAttachmentVideo.component.pug',
    styleUrls: [
        './EvodeskFeedPostAttachmentVideo.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostAttachmentVideoComponent implements OnChanges
{
    @Input() attachment: AttachmentVideoModel;
    @Input() controls: boolean = false;

    @Output('remove') removeEvent: EventEmitter<AttachmentVideoModel> = new EventEmitter<AttachmentVideoModel>();

    public state: State = {
        trustedUrl: undefined,
        preview: undefined,
        viewMode: 'preview',
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private sanitize: DomSanitizer,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['attachment']) {
            const youtubeId: string | undefined = ((input: string) => {
                if (/embed/.test(input)) {
                    return input;
                } else {
                    const regExp: RegExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
                    const match: RegExpMatchArray = input.match(regExp);

                    return (match && match[7].length === 11) ? match[7] : undefined;
                }
            })(this.attachment.path);

            this.state = {
                ...this.state,
                preview: `https://img.youtube.com/vi/${youtubeId}/mqdefault.jpg`,
                trustedUrl: (() => {

                    if (youtubeId) {
                        return this.sanitize.bypassSecurityTrustResourceUrl(`https://www.youtube.com/embed/${youtubeId}`);
                    } else {
                        return undefined;
                    }
                })(),
            };
        }
    }

    enablePlayMode(): void {
        if (this.state.trustedUrl) {
            this.state = {
                ...this.state,
                viewMode: 'play',
            };

            this.cdr.detectChanges();
        }
    }

    remove($event: MouseEvent): void {
        $event.preventDefault();
        $event.stopPropagation();

        this.removeEvent.emit(this.attachment);
    }
}
