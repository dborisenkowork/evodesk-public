import {ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject, Observable, forkJoin} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {AttachmentPostModel} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

import {EvodeskFeedPostAttachmentPostComponentProps} from './EvodeskFeedPostAttachmentPost.component';

import {EvodeskProfileRepositoryService} from '../../../EvodeskProfile/services/EvodeskProfileRepository.service';
import {EvodeskFeedBodyParserService} from '../../services/EvodeskFeedBodyParser.service';

interface State {
    ready: boolean;
    props?: EvodeskFeedPostAttachmentPostComponentProps;
}

@Component({
    selector: 'evodesk-feed-post-attachment-post-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-feed-post-attachment-post [props]="state.props"></evodesk-feed-post-attachment-post>
      </ng-container>
    `,
})
export class EvodeskFeedPostAttachmentPostContainerComponent implements OnChanges, OnDestroy
{
    @Input() attachment: AttachmentPostModel;
    @Input() controls: boolean;

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private feedBodyParser: EvodeskFeedBodyParserService,
        private profileRepositoryService: EvodeskProfileRepositoryService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (changes['attachment']) {
            this.setUp();
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    setUp(): void {
        const queries: Array<Observable<any>> = [];

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                attachment: this.attachment,
                controls: this.controls,
                parsedBody: this.feedBodyParser.parseToHtml(this.attachment.origPost.text),
            },
        };

        queries.push(Observable.create(q => {
            this.profileRepositoryService.getProfile(this.attachment.origPost.user_id).pipe(takeUntil(this.ngOnChanges$)).subscribe(
                (profile) => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            authorProfile: profile,
                        },
                    };

                    q.next(undefined);
                    q.complete();
                },
                (httpError) => {
                    q.error(httpError);
                },
            );
        }));

        forkJoin(queries).pipe(takeUntil(this.ngOnChanges$)).subscribe(() => {
            this.state = {
                ...this.state,
                ready: true,
            };

            this.cdr.detectChanges();
        });
    }
}
