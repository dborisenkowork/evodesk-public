import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

import * as moment from 'moment';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../types/Backend.types';

import {Observable} from 'rxjs';

import {ProfileModel} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {AttachmentListItem, AttachmentPostModel} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

export interface EvodeskFeedPostAttachmentPostComponentProps {
    authorProfile: ProfileModel;
    attachment: AttachmentPostModel;
    controls: boolean;
    parsedBody: Observable<string>;
}

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-feed-post-attachment-post',
    templateUrl: './EvodeskFeedPostAttachmentPost.component.pug',
    styleUrls: [
        './EvodeskFeedPostAttachmentPost.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostAttachmentPostComponent implements OnChanges
{
    @Input() props: EvodeskFeedPostAttachmentPostComponentProps;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get attachments(): Array<AttachmentListItem> {
        const attachments: Array<AttachmentListItem> = [];

        this.props.attachment.origPost.attachments.forEach(a => {
            attachments.push(<AttachmentListItem>{
                type: a.type,
                attachment: a,
            });
        });

        return attachments;
    }

    get createdAt(): string {
        return moment(this.props.attachment.origPost.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).format('LL');
    }

    get hasText(): boolean {
        return this.props.attachment.origPost.text && this.props.attachment.origPost.text.length > 0;
    }
}
