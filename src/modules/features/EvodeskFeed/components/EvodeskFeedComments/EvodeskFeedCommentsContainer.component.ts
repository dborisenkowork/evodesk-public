import {ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewChild} from '@angular/core';

import {Observable, Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {EvodeskFeedCommentsComponent, EvodeskFeedCommentsComponentProps} from './EvodeskFeedComments.component';

import {PostId} from '../../../../app/EvodeskRESTApi/models/post/Post.model';
import {PostCommentModel} from '../../../../app/EvodeskRESTApi/models/post/PostComment.model';
import {EvodeskFeedCommentEntity, SupportedCommentType} from '../../models/EvodeskFeedCommentEntity.model';
import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {GetPostCommentsOptions, PostCommentRESTService} from '../../../../app/EvodeskRESTApi/services/PostCommentREST.service';
import {EvodeskFeedStateAction, EvodeskFeedStateService} from '../../state/EvodeskFeedState.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';

interface FetchOptions {
    useLoading: boolean;
    beforeId?: number;
}

interface State {
    ready: boolean;
    props?: EvodeskFeedCommentsComponentProps;
    isFirstFetch: boolean;
}

@Component({
    selector: 'evodesk-feed-comments-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-feed-comments #view [props]="state.props" (loadMore)="onLoadMore()"></evodesk-feed-comments>
      </ng-container>
    `,
})
export class EvodeskFeedCommentsContainerComponent implements OnDestroy, OnChanges
{
    public static PAGE_SIZE: number = 5;

    @Input() entity: EvodeskFeedCommentEntity;
    @Input() postAuthorId: ProfileId;

    @ViewChild('view') view: EvodeskFeedCommentsComponent;

    private nextFetch$: Subject<void> = new Subject<void>();
    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        isFirstFetch: true,
        props: {
            loading: true,
            comments: [],
            totalComments: 0,
            hasMoreCommentsToLoad: false,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private uiAlertService: EvodeskAlertModalService,
        private postCommentRESTService: PostCommentRESTService,
        private feedState: EvodeskFeedStateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        this.feedState.side$.pipe(takeUntil(this.ngOnChanges$), filter(s => s.type === EvodeskFeedStateAction.CreatePostComment)).subscribe(() => {
            setTimeout(() => {
                if (this.view) {
                    this.view.scrollToBottom();
                }
            });
        });

        if (this.entity) {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    totalComments: (() => {
                        switch (this.entity.type) {
                            default:
                                throw new Error('Unknown entity type');

                            case SupportedCommentType.Post:
                                return this.entity.post.comments;
                        }
                    })(),
                },
            };

            this.feedState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
                this.update();
            });


            this.fetch({
                useLoading: true,
            }).subscribe();
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.nextFetch$.next(undefined);
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    onLoadMore(): void {
        this.fetch({
            useLoading: true,
            beforeId: this.state.props.comments[0].id,
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }

    fetch(options: FetchOptions): Observable<void> {
        return Observable.create((done) => {
            this.nextFetch$.next(undefined);

            if (options.useLoading) {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        loading: true,
                    },
                };
            }

            switch (this.entity.type) {
                default:
                    throw new Error('Unknown entity type');

                case SupportedCommentType.Post:
                    const postId: PostId = this.entity.post.id;
                    const qo: GetPostCommentsOptions = {
                        num: EvodeskFeedCommentsContainerComponent.PAGE_SIZE + 1,
                    };

                    if (options.beforeId) {
                        qo.before = options.beforeId;
                    }

                    this.postCommentRESTService.getPostComments({ postId: postId, profileId: this.postAuthorId }, qo).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                        (httpResponse) => {
                            this.feedState.dispatch({
                                type: EvodeskFeedStateAction.PushPostComments,
                                payload: {
                                    postId: postId,
                                    comments: httpResponse.slice(0, EvodeskFeedCommentsContainerComponent.PAGE_SIZE),
                                },
                            });

                            if (options.useLoading) {
                                this.state = {
                                    ...this.state,
                                    props: {
                                        ...this.state.props,
                                        loading: false,
                                    },
                                };
                            }

                            this.state = {
                                ...this.state,
                                props: {
                                    ...this.state.props,
                                    hasMoreCommentsToLoad: httpResponse.length > EvodeskFeedCommentsContainerComponent.PAGE_SIZE,
                                },
                            };

                            this.cdr.detectChanges();

                            done.next(undefined);
                            done.complete();
                        },
                        (httpError) => {
                            this.uiAlertService.openTranslated('EvodeskFeed.components.EvodeskFeedCommentsContainer.FailedToFetch').pipe(takeUntil(this.ngOnDestroy$)).subscribe();

                            this.cdr.detectChanges();

                            done.error(httpError);
                        },
                    );

                    break;
            }
        });
    }

    update(): void {
        switch (this.entity.type) {
            default:
                throw new Error('Unknown entity type');

            case SupportedCommentType.Post:
                const comments: Array<PostCommentModel> = [...(this.feedState.snapshot.postComments[this.entity.post.id] || [])];

                if (comments.length) {
                    comments.sort((a, b) => a.id - b.id);

                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            comments: comments,

                        },
                    };

                    setTimeout(() => {
                        if (this.state.isFirstFetch) {
                            this.state = {
                                ...this.state,
                                isFirstFetch: false,
                            };

                            this.view.stickToBottom();
                        } else {
                            this.view.stickToTop();
                        }
                    });
                } else {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            comments: comments,

                        },
                    };
                }

                break;
        }

        this.cdr.detectChanges();
    }
}
