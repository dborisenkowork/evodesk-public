import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';

import {PostCommentModel} from '../../../../app/EvodeskRESTApi/models/post/PostComment.model';

import {interval, Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

export interface EvodeskFeedCommentsComponentProps {
    loading: boolean;
    comments: Array<PostCommentModel>;
    totalComments: number;
    hasMoreCommentsToLoad: boolean;
}

interface State {
    ready: boolean;
    stickToScrollEnabled?: Subscription;
}

@Component({
    selector: 'evodesk-feed-comments',
    templateUrl: './EvodeskFeedComments.component.pug',
    styleUrls: [
        './EvodeskFeedComments.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedCommentsComponent implements OnChanges
{
    @Input() props: EvodeskFeedCommentsComponentProps;

    @Output('loadMore') loadMoreEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('comments') commentsRef: ElementRef;

    private stopStickScroll$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private ngZone: NgZone,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            }
        }
    }

    trackBy(index: number, comment: PostCommentModel): any {
        return comment.id;
    }

    loadMore(): void {
        if (! this.props.loading) {
            this.loadMoreEvent.emit(undefined);
        }
    }

    stickToTop(): void {
        this.stopStickScroll$.next(undefined);

        this.scrollToTop();

        this.ngZone.runOutsideAngular(() => {
            this.state = {
                ...this.state,
                stickToScrollEnabled: interval(100).pipe(takeUntil(this.stopStickScroll$)).subscribe(() => {
                    this.scrollToTop();
                }),
            };
        });
    }

    stickToBottom(): void {
        this.stopStickScroll$.next(undefined);

        this.scrollToBottom();

        this.ngZone.runOutsideAngular(() => {
            this.state = {
                ...this.state,
                stickToScrollEnabled: interval(100).pipe(takeUntil(this.stopStickScroll$)).subscribe(() => {
                    this.scrollToBottom();
                }),
            };
        });
    }

    stopScrollStick() {
        if (this.state.stickToScrollEnabled) {
            this.stopStickScroll$.next(undefined);

            this.state = {
                ...this.state,
                stickToScrollEnabled: undefined,
            };
        }
    }

    scrollToTop(): void {
        (this.commentsRef.nativeElement as HTMLElement).scrollTop = 0;
    }

    scrollToBottom(): void {
        (this.commentsRef.nativeElement as HTMLElement).scrollTop = (this.commentsRef.nativeElement as HTMLElement).scrollHeight;
    }

    onWheel(): void {
        this.stopScrollStick();
    }

    onTouchMove(): void {
        this.stopScrollStick();
    }
}
