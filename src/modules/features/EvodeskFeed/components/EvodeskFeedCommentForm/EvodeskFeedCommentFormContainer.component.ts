import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder} from '@angular/forms';

import {Observable, Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {EvodeskFeedCommentFormComponent, evodeskFeedCommentFormComponentFormBuilder, EvodeskFeedCommentFormComponentFormValue, EvodeskFeedCommentFormComponentProps} from './EvodeskFeedCommentForm.component';

import {PostModel} from '../../../../app/EvodeskRESTApi/models/post/Post.model';
import {PostCommentModel} from '../../../../app/EvodeskRESTApi/models/post/PostComment.model';
import {EvodeskFeedCommentJSONBody} from '../../models/EvodeskFeed.model';

import {PostCommentRESTService} from '../../../../app/EvodeskRESTApi/services/PostCommentREST.service';
import {EvodeskFeedStateAction, EvodeskFeedStateService} from '../../state/EvodeskFeedState.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {EvodeskFeedBodyParserService} from '../../services/EvodeskFeedBodyParser.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';

export enum SupportedCommentType
{
    Post = 'post',
}

export interface EvodeskFeedCommentFormPostEntity {
    type: SupportedCommentType.Post;
    post: PostModel;
    useComment?: PostCommentModel;
}

type Entity = EvodeskFeedCommentFormPostEntity;

type FormValue = EvodeskFeedCommentFormComponentFormValue;

export interface FailEvent {
    formValue: FormValue;
}

export interface SuccessEvent {
    formValue: FormValue;
    postComment: PostCommentModel;
}

interface State {
    ready: boolean;
    props?: EvodeskFeedCommentFormComponentProps;
}

@Component({
    selector: 'evodesk-feed-comment-form-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-feed-comment-form [props]="state.props" (submit)="onSubmit($event)" (reset)="onReset()" (clearReply)="onReplyClear()"></evodesk-feed-comment-form>
      </ng-container>
    `,
})
export class EvodeskFeedCommentFormContainerComponent implements OnInit, OnChanges, OnDestroy
{
    @Input() entity: Entity;

    @Output('fail') failEvent:  EventEmitter<FailEvent> = new EventEmitter<FailEvent>();
    @Output('success') successEvent:  EventEmitter<SuccessEvent> = new EventEmitter<SuccessEvent>();

    @ViewChild(EvodeskFeedCommentFormComponent) view: EvodeskFeedCommentFormComponent;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private fb: FormBuilder,
        private cdr: ChangeDetectorRef,
        private postCommentRESTService: PostCommentRESTService,
        private feedStateService: EvodeskFeedStateService,
        private currentUserService: CurrentUserService,
        private uiAlertService: EvodeskAlertModalService,
        private bodyParser: EvodeskFeedBodyParserService,
    ) {}

    ngOnInit(): void {
        this.feedStateService.side$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((action) => {
            if (action.type === EvodeskFeedStateAction.ReplyPostComment) {
                this.view.focus();
            }
        });

        this.feedStateService.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !!s.current),
        ).subscribe(s => {
            if (s.current.reply) {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        reply: s.current.reply,
                        replyText$: this.bodyParser.parseCommentTextToHtml(
                            this.bodyParser.parseCommentToJSOBNNBody(s.current.reply.text).text,
                        ),
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        reply: undefined,
                    },
                };
            }

            this.cdr.detectChanges();
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.entity) {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    form: evodeskFeedCommentFormComponentFormBuilder(this.fb, { useComment: this.entity.useComment }),
                    isSubmitting: false,
                    isEditing: !! this.entity.useComment,
                },
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }

        this.cdr.detectChanges();
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get isEditing(): boolean {
        return this.state.props.isEditing;
    }

    onReset(): void {
        this.state.props.form.reset();
    }

    onSubmit(): void {
        const formValue: FormValue = this.state.props.form.value;

        this.state.props.form.disable();

        const fail: Function = () => {
            this.state.props.form.enable();

            this.failEvent.emit({
                formValue: formValue,
            });

            this.cdr.detectChanges();

            setTimeout(() => {
                if (this.view) {
                    this.view.focus();
                }
            });
        };

        const success: Function = (postComment: PostCommentModel) => {
            this.cdr.detectChanges();

            this.state.props.form.reset();
            this.state.props.form.enable();

            switch (this.entity.type) {
                default:
                    throw new Error('Unknown entity type');

                case SupportedCommentType.Post:
                    if (this.isEditing) {
                        this.feedStateService.dispatch({
                            type: EvodeskFeedStateAction.EditPostComment,
                            payload: {
                                postId: this.entity.post.id,
                                comment: postComment,
                            },
                        });
                    } else {
                        this.feedStateService.dispatch({
                            type: EvodeskFeedStateAction.CreatePostComment,
                            payload: {
                                postId: this.entity.post.id,
                                comment: postComment,
                            },
                        });
                    }

                    break;
            }

            this.successEvent.emit({
                formValue: formValue,
                postComment: postComment,
            });

            setTimeout(() => {
                if (this.view) {
                    this.view.focus();
                }
            });
        };

        Observable.create(response => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            const jsonBody: EvodeskFeedCommentJSONBody = {
                text: formValue.text,
                reply: ! this.state.props.reply ? undefined : {
                    authorProfileId: this.state.props.reply.user_id,
                    authorProfileName: this.state.props.reply.name || this.state.props.reply.email,
                    body: this.bodyParser.parseCommentToJSOBNNBody(this.state.props.reply.text),
                },
            };

            const httpQuery: Observable<PostCommentModel> = (() => {
                switch (this.entity.type) {
                    default:
                        throw new Error('Unknown entity type');

                    case SupportedCommentType.Post:
                        if (this.isEditing) {
                            return this.postCommentRESTService.editPostComment(
                                {
                                    profileId: this.currentUserService.impersonatedAs.id,
                                    postId: this.entity.post.id,
                                    commentId: this.entity.useComment.id,
                                },
                                {
                                    text: JSON.stringify(jsonBody),
                                },
                            );
                        } else {
                            return this.postCommentRESTService.createNewPostComment(
                                {
                                    profileId: this.currentUserService.impersonatedAs.id,
                                    postId: this.entity.post.id,
                                },
                                {
                                    text: JSON.stringify(jsonBody),
                                },
                            );
                        }
                }
            })();

            httpQuery.pipe(takeUntil(unsubscribe$)).subscribe(
                (httpResponse) => {
                    success(httpResponse);

                    this.feedStateService.dispatch({
                        type: EvodeskFeedStateAction.ClearReplyPostComment,
                    });

                    response.next(httpResponse);
                    response.complete();
                },
                (httpError) => {
                    fail();

                    this.uiAlertService.open({
                        title: {
                            text: 'EvodeskFeed.components.EvodeskFeedCommentForm.Fail.Title',
                            translate: true,
                        },
                        text: {
                            text: 'EvodeskFeed.components.EvodeskFeedCommentForm.Fail.Text',
                            translate: true,
                        },
                        ok: {
                            text: 'EvodeskFeed.components.EvodeskFeedCommentForm.Fail.Close',
                            translate: true,
                        },
                    }).subscribe();

                    response.error(httpError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }

    onReplyClear() {
        this.feedStateService.dispatch({
            type: EvodeskFeedStateAction.ClearReplyPostComment,
        });
    }
}
