import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Observable} from 'rxjs';

import {PostCommentModel} from '../../../../app/EvodeskRESTApi/models/post/PostComment.model';

export interface EvodeskFeedCommentFormComponentProps {
    form: FormGroup;
    isEditing: boolean;
    isSubmitting: boolean;
    reply?: PostCommentModel;
    replyText$?: Observable<string>;
}

interface State {
    ready: boolean;
    isTextareaFocused: boolean;
}

export function evodeskFeedCommentFormComponentFormBuilder(fb: FormBuilder, options: { useComment?: PostCommentModel | undefined } = {}): FormGroup {
    const form: FormGroup = fb.group({
        text: ['', [Validators.required, Validators.maxLength(65535)]],
    });

    if (options.useComment) {
        form.patchValue({
            text: options.useComment.text,
        });
    }

    return form;
}

export interface EvodeskFeedCommentFormComponentFormValue {
    text: string;
}

@Component({
    selector: 'evodesk-feed-comment-form',
    templateUrl: './EvodeskFeedCommentForm.component.pug',
    styleUrls: [
        './EvodeskFeedCommentForm.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedCommentFormComponent implements OnChanges, AfterViewInit
{
    @Input() props: EvodeskFeedCommentFormComponentProps;

    @Output('reset') resetEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('submit') submitEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('clearReply') clearReplyEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('textarea') textareaRef: ElementRef;

    public state: State = {
        ready: false,
        isTextareaFocused: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.focus();
        });
    }

    clearReply() {
        this.clearReplyEvent.emit();
    }

    get textareaPlaceholder(): string {
        return this.state.isTextareaFocused ? '' : 'EvodeskFeed.components.EvodeskFeedCommentForm.Text';
    }

    onTextKeyDown($event: KeyboardEvent): void {
        this.cdr.detectChanges();

        if ($event.ctrlKey && $event.keyCode === 13) {
            this.submit();
        }
    }

    onTextareaFocus(): void {
        this.state = {
            ...this.state,
            isTextareaFocused: true,
        };
    }

    onTextareBlur(): void {
        this.state = {
            ...this.state,
            isTextareaFocused: false,
        };
    }

    get hasText(): boolean {
        return this.props.form.get('text').value && this.props.form.get('text').value.length > 0;
    }

    get canSubmit(): boolean {
        const isFormValid: boolean = this.props.form.valid;
        const hasText: boolean = this.hasText;

        return isFormValid && hasText;
    }

    focus(): void {
        if (this.textareaRef && this.textareaRef.nativeElement) {
            (this.textareaRef.nativeElement as HTMLInputElement).focus();
        }
    }

    reset(): void {
        if (this.hasText) {
            this.resetEvent.emit(undefined);
        }
    }

    submit(): void {
        if (this.canSubmit) {
            this.submitEvent.emit(undefined);
            this.clearReply();
        }
    }
}
