import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import * as moment from 'moment';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../types/Backend.types';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {PostCommentModel} from '../../../../app/EvodeskRESTApi/models/post/PostComment.model';
import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';

export interface EvodeskFeedCommentComponentProps {
    postComment: PostCommentModel;
    isOwnComment: boolean;
}

interface State {
    ready: boolean;
    createdAt?: Observable<string>;
}

@Component({
    selector: 'evodesk-feed-comment',
    templateUrl: './EvodeskFeedComment.component.pug',
    styleUrls: [
        './EvodeskFeedComment.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedCommentComponent implements OnChanges, OnDestroy
{
    @Input() props: EvodeskFeedCommentComponentProps;

    @Output('toggleLike') toggleLikeEvent: EventEmitter<PostCommentModel> = new EventEmitter<PostCommentModel>();
    @Output('reply') replyEvent: EventEmitter<PostCommentModel> = new EventEmitter<PostCommentModel>();

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private evodeskDateService: EvodeskDateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    createdAt: this.evodeskDateService.diffFromNowV3(moment(this.props.postComment.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate()).pipe(takeUntil(this.ngOnChanges$)),
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    get name(): string {
        return this.props.postComment.name || this.props.postComment.email;
    }

    toggleLike(): void {
        if (! this.props.isOwnComment) {
            this.toggleLikeEvent.emit(this.props.postComment);
        }
    }

    reply() {
        this.replyEvent.emit(this.props.postComment);
    }
}
