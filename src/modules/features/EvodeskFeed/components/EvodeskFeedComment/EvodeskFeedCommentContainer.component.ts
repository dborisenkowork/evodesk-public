import {ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {proxy} from '../../../../../functions/proxy.function';

import {EvodeskFeedCommentComponentProps} from './EvodeskFeedComment.component';

import {PostCommentModel} from '../../../../app/EvodeskRESTApi/models/post/PostComment.model';

import {EvodeskFeedStateAction, EvodeskFeedStateService} from '../../state/EvodeskFeedState.service';
import {PostCommentRESTService} from '../../../../app/EvodeskRESTApi/services/PostCommentREST.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';

interface State {
    ready: boolean;
    props?: EvodeskFeedCommentComponentProps;
}

@Component({
    selector: 'evodesk-feed-comment-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-feed-comment [props]="state.props" (toggleLike)="onToggleLike()" (reply)="onReply()"></evodesk-feed-comment>
      </ng-container>
    `,
})
export class EvodeskFeedCommentContainerComponent implements OnChanges, OnDestroy
{
    @Input() postComment: PostCommentModel;

    private nextLikeAction$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private currentUserService: CurrentUserService,
        private feedStateService: EvodeskFeedStateService,
        private postCommentRESTService: PostCommentRESTService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['postComment']) {
            if (this.postComment) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        postComment: this.postComment,
                        isOwnComment: this.currentUserService.impersonatedAs.id.toString() === this.postComment.user_id.toString(),
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onToggleLike(): void {
        this.nextLikeAction$.next(undefined);

        if (this.postComment.isLiked) {
            this.feedStateService.dispatch({
                type: EvodeskFeedStateAction.DislikePostComment,
                payload: {
                    postId: this.postComment.post_id.id,
                    commentId: this.postComment.id,
                },
            });

            proxy(
                this.postCommentRESTService.dislikePostComment(this.postComment.id).pipe(takeUntil(this.nextLikeAction$)),
            ).pipe(takeUntil(this.ngOnDestroy$)).subscribe();

            this.cdr.detectChanges();
        } else {
            this.feedStateService.dispatch({
                type: EvodeskFeedStateAction.LikePostComment,
                payload: {
                    postId: this.postComment.post_id.id,
                    commentId: this.postComment.id,
                },
            });

            proxy(
                this.postCommentRESTService.likePostComment(this.postComment.id).pipe(takeUntil(this.nextLikeAction$)),
            ).pipe(takeUntil(this.ngOnDestroy$)).subscribe();

            this.cdr.detectChanges();
        }
    }

    onReply() {
        this.feedStateService.dispatch({
            type: EvodeskFeedStateAction.ReplyPostComment,
            payload: {
                comment: this.postComment,
            },
        });
    }
}
