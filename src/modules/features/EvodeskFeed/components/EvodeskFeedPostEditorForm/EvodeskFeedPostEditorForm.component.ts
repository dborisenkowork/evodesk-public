import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {PostModel, AttachmentListItem} from '../../../../app/EvodeskRESTApi/models/post/Post.model';
import {AnyAttachmentModel, AttachmentType} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

import {defaultMatDialogConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {EvodeskFeedAttachLinkModalComponent, EvodeskFeedAttachLinkModalComponentData} from '../EvodeskFeedAttachLinkModal/EvodeskFeedAttachLinkModal.component';

export enum Screen {
    TextPost = 'text-post',
    PostWithAttachments = 'post-with-attachments',
}

export enum ExpectingFileType {
    None,
    Photo,
}

export enum VideoHostingPattern {
    Youtube = 'youtu\.?be\.?(com)?\/',
}

export interface EvodeskFeedPostEditorNgSubmitEvent {
    formValue: EvodeskFeedPostEditorFormComponentFormValue;
    attachments: Array<AnyAttachmentModel>;
}

export interface EvodeskFeedPostEditorFormComponentFormValue {
    text: string;
    pinned: boolean;
}

export interface AttachPhotoRequest {
    files: FileList;
}

export interface AttachLinkRequest {
    url: string;
}

export interface AttachVideoRequest {
    url: string;
}

export enum EvodeskFeedPostEditorFormComponentViewMode {
    Default = 'default',
    Modal = 'modal',
}

export interface EvodeskFeedPostEditorFormComponentProps {
    profileId: number;
    profileImage: string | undefined;
    profileName: string;
    form: FormGroup;
    attachments: Array<AnyAttachmentModel>;
    attaching: boolean;
    currentAttachment: undefined | AttachmentType;
    isEditing: boolean;
    setupFromPost?: PostModel;
    enableAutoFocus: boolean;
    forceExpandedForm: boolean;
    forceDisplayCancelButton: boolean;
    forcePin: boolean;
    viewMode: EvodeskFeedPostEditorFormComponentViewMode;
}

export function evodeskFeedPostEditorFormComponentFormBuilder(fb: FormBuilder, options: { from?: PostModel } = {}): FormGroup {
    const form: FormGroup = fb.group({
        text: ['', [Validators.maxLength(65535)]],
        pinned: [false],
    });

    if (options.from) {
        form.patchValue({
            text: options.from.text,
            pinned: options.from.pinned.toString() === '1',
        });
    }

    return form;
}

interface State {
    ready: boolean;
    screen: Screen;
    expectingAttachment: ExpectingFileType;
    attachLinkRef?: MatDialogRef<EvodeskFeedAttachLinkModalComponent>;
    isTextareaFocused: boolean;
}

@Component({
    selector: 'evodesk-feed-post-editor-form',
    templateUrl: './EvodeskFeedPostEditorForm.component.pug',
    styleUrls: [
        './EvodeskFeedPostEditorForm.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskFeedPostEditorFormComponent implements OnChanges, AfterViewInit, OnDestroy
{
    @Input() props: EvodeskFeedPostEditorFormComponentProps;

    @Output('ngSubmit') submitEvent: EventEmitter<EvodeskFeedPostEditorNgSubmitEvent> = new EventEmitter<EvodeskFeedPostEditorNgSubmitEvent>();

    @Output('attachPhoto') attachPhotoEvent: EventEmitter<AttachPhotoRequest> = new EventEmitter<AttachPhotoRequest>();
    @Output('attachVideo') attachVideoEvent: EventEmitter<AttachVideoRequest> = new EventEmitter<AttachVideoRequest>();

    @Output('removeAttachment') removeAttachmentEvent: EventEmitter<AnyAttachmentModel> = new EventEmitter<AnyAttachmentModel>();

    @Output('cancel') cancelEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('closeModal') closeModalEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('fileInput') fileInputRef: ElementRef;
    @ViewChild('textAreaInput') textAreaInputRef: ElementRef;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        screen: Screen.TextPost,
        expectingAttachment: ExpectingFileType.None,
        isTextareaFocused: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private matDialog: MatDialog,
    ) {}

    ngAfterViewInit(): void {
        if (this.props.enableAutoFocus) {
            (this.textAreaInputRef.nativeElement as HTMLElement).focus();
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    screen: this.hasAttachments ? Screen.PostWithAttachments : Screen.TextPost,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.cdr.detach();

        this.ngOnDestroy$.next(undefined);

        if (this.state.attachLinkRef) {
            this.state.attachLinkRef.close();
        }
    }

    setScreen(screen: Screen): void {
        this.state = {
            ...this.state,
            screen: screen,
        };

        setTimeout(() => {
            if (this.textAreaInputRef) {
                (this.textAreaInputRef.nativeElement as HTMLElement).focus();
            }
        });
    }

    get containerCSSClasses(): any {
        return {
            '__vm-default': this.props.viewMode === EvodeskFeedPostEditorFormComponentViewMode.Default,
            '__vm-modal': this.props.viewMode === EvodeskFeedPostEditorFormComponentViewMode.Modal,
        };
    }

    get fileInput(): HTMLInputElement {
        return this.fileInputRef.nativeElement;
    }

    get isPinned(): boolean {
        return this.props.form.get('pinned').value;
    }

    get hasAttachments(): boolean {
        return (this.props.attachments.length > 0) || (this.props.setupFromPost && !! this.props.setupFromPost.post);
    }

    get attachments(): Array<AttachmentListItem> {
        if (this.props.setupFromPost && !! this.props.setupFromPost.post) {
            return [
                {
                    type: AttachmentType.Post,
                    attachment: {
                        id: this.props.setupFromPost.id,
                        origPost: this.props.setupFromPost.post,
                        repost: this.props.setupFromPost,
                    },
                },
            ];
        } else {
            return this.props.attachments.map((attachment) => {
                return <AttachmentListItem>{
                    attachment: attachment,
                    type: <any>this.props.currentAttachment,
                };
            });
        }
    }

    get textareaPlaceholder(): string {
        return this.state.isTextareaFocused ? '' : 'EvodeskFeed.components.EvodeskFeedPostEditorForm.Text';
    }

    onTextKeyDown($event: KeyboardEvent): void {
        if ($event.ctrlKey && $event.keyCode === 13) {
            this.submit();
        }
    }

    onFileInput(): void {
        switch (this.state.expectingAttachment) {
            default:
                break;

            case ExpectingFileType.Photo:
                this.attachPhotoEvent.emit({
                    files: this.fileInput.files,
                });

                break;
        }
    }

    togglePinned(): void {
        this.props.form.patchValue({
            pinned: ! this.isPinned,
        });
    }

    requestAttachPhoto(): void {
        this.state = {
            ...this.state,
            expectingAttachment: ExpectingFileType.Photo,
        };

        this.fileInput.click();
    }

    requestAttachLink(): void {
        const data: EvodeskFeedAttachLinkModalComponentData = {
            url: '',
            doSubmit: false,
        };

        this.state = {
            ...this.state,
            attachLinkRef: this.matDialog.open(EvodeskFeedAttachLinkModalComponent, {
                ...defaultMatDialogConfig,
                data: data,
            }),
        };

        this.state.attachLinkRef.afterClosed().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            if (data.doSubmit) {
                if (data.url && data.url.length) {
                    if (new RegExp(VideoHostingPattern.Youtube).test(data.url)) {
                        this.attachVideoEvent.emit({
                            url: data.url,
                        });
                    } else {
                    }
                }
            }
        });
    }

    requestRemoveAttachment($event: AnyAttachmentModel): void {
        this.removeAttachmentEvent.emit($event);
    }

    onTextareaFocus(): void {
        this.state = {
            ...this.state,
            isTextareaFocused: true,
        };
    }

    onTextareBlur(): void {
        this.state = {
            ...this.state,
            isTextareaFocused: false,
        };
    }

    get hasText(): boolean {
        return this.props.form.get('text').value && this.props.form.get('text').value.length > 0;
    }

    get canSubmit(): boolean {
        const isFormValid: boolean = this.props.form.valid;
        const hasText: boolean = this.hasText;
        const hasAttachments: boolean = this.props.attachments.length > 0;

        return isFormValid && (hasText || hasAttachments);
    }

    submit(): void {
        if (this.canSubmit) {
            if (this.props.forcePin) {
                this.props.form.patchValue({
                    pinned: true,
                });
            }

            this.submitEvent.emit({
                attachments: this.props.attachments,
                formValue: this.props.form.value,
            });
        }
    }

    cancel(): void {
        this.cancelEvent.emit();
    }

    closeModal(): void {
        this.closeModalEvent.emit(undefined);
    }
}
