import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {forkJoin as observableForkJoin, Subject, Observable, forkJoin} from 'rxjs';
import {takeUntil, filter} from 'rxjs/operators';

import {PostModel, PostType} from '../../../../app/EvodeskRESTApi/models/post/Post.model';
import {AnyAttachmentModel, AttachmentId, AttachmentType} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

import {AttachLinkRequest, AttachPhotoRequest, AttachVideoRequest, EvodeskFeedPostEditorFormComponent, evodeskFeedPostEditorFormComponentFormBuilder, EvodeskFeedPostEditorFormComponentFormValue, EvodeskFeedPostEditorFormComponentProps, EvodeskFeedPostEditorFormComponentViewMode, EvodeskFeedPostEditorNgSubmitEvent, Screen} from './EvodeskFeedPostEditorForm.component';

import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {PostRESTService} from '../../../../app/EvodeskRESTApi/services/PostREST.service';
import {AttachmentRESTService} from '../../../../app/EvodeskRESTApi/services/AttachmentREST.service';
import {EvodeskConfirmModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {EvodeskFeedStateAction, EvodeskFeedStateService} from '../../state/EvodeskFeedState.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';

export interface FailEvent {
    formValue: EvodeskFeedPostEditorFormComponentFormValue;
}

export interface SuccessEvent {
    post: PostModel;
    formValue: EvodeskFeedPostEditorFormComponentFormValue;
}

interface State {
    ready: boolean;
    props?: EvodeskFeedPostEditorFormComponentProps;
    queueRemoveAttachments: Array<AttachmentId>;
}

export interface EvodeskFeedPostEditorFormContainerComponentProps {
    profileId: number;
    profileImage: string;
    profileName: string;
}

type ViewMode = EvodeskFeedPostEditorFormComponentViewMode;

@Component({
    selector: 'evodesk-feed-post-editor-form-container',
    template: `
        <ng-container *ngIf="!!state && state.ready && !! state.props">
          <evodesk-feed-post-editor-form
            [props]="state.props"
            (ngSubmit)="onNgSubmit($event)"
            (cancel)="onCancel()"
            (closeModal)="onCloseModal()"
            (attachPhoto)="onAttachPhoto($event)"
            (attachLink)="onAttachLink($event)"
            (attachVideo)="onAttachVideo($event)"
            (removeAttachment)="onRemoveAttachment($event)"
          ></evodesk-feed-post-editor-form>
        </ng-container>
    `,
})
export class EvodeskFeedPostEditorFormContainerComponent implements OnChanges, OnInit, OnDestroy
{
    public static MAX_ATTACHMENTS: number = 10;

    @Input() setupFromPost: PostModel;
    @Input() enableAutoFocus: boolean = true;
    @Input() forceExpandedForm: boolean = false;
    @Input() forceDisplayCancelButton: boolean = false;
    @Input() forcePin: boolean = false;
    @Input() viewMode: ViewMode = EvodeskFeedPostEditorFormComponentViewMode.Default;
    @Input() doPushPostToState: boolean = true;

    @Output('fail') failEvent: EventEmitter<FailEvent> = new EventEmitter<FailEvent>();
    @Output('success') successEvent: EventEmitter<SuccessEvent> = new EventEmitter<SuccessEvent>();
    @Output('cancel') cancelEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output('closeModal') closeModalEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild(EvodeskFeedPostEditorFormComponent) view: EvodeskFeedPostEditorFormComponent;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        queueRemoveAttachments: [],
    };

    constructor(
        private fb: FormBuilder,
        private cdr: ChangeDetectorRef,
        private uiAlertService: EvodeskAlertModalService,
        private uiConfirmService: EvodeskConfirmModalService,
        private postRESTService: PostRESTService,
        private attachmentRESTService: AttachmentRESTService,
        private feedStateService: EvodeskFeedStateService,
        private currentUser: CurrentUserService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.setup();
    }

    ngOnInit(): void {
        this.feedStateService.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !!s.current && ((! this.state.props || ! this.state.props.profileId) || (s.current.currentProfile.profileId !== this.state.props.profileId))),
        ).subscribe(s => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    profileId: s.current.currentProfile.profileId,
                    profileImage: s.current.currentProfile.profileImage,
                    profileName: s.current.currentProfile.profileName,
                },
            };

            this.cdr.detectChanges();
        });

        if (! this.setupFromPost) {
            this.setup();
        }
    }

    ngOnDestroy(): void {
        this.cdr.detach();

        this.ngOnDestroy$.next(undefined);
    }

    reset(): void {
        this.state = {
            ready: false,
            queueRemoveAttachments: [],
        };

        this.setup();
    }

    setup(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                form: evodeskFeedPostEditorFormComponentFormBuilder(this.fb),
                attachments: [],
                attaching: false,
                currentAttachment: undefined,
                profileName: this.currentUser.impersonatedAs.name || this.currentUser.impersonatedAs.email,
                profileId: this.currentUser.impersonatedAs.id,
                profileImage: this.currentUser.impersonatedAs.avatar,
            },
        };

        if (this.setupFromPost) {
            const form: FormGroup = evodeskFeedPostEditorFormComponentFormBuilder(this.fb, { from: this.setupFromPost });

            this.state = {
                ...this.state,
                ready: true,
                queueRemoveAttachments: [],
                props: {
                    ...this.state.props,
                    form: form,
                    attachments: [...this.setupFromPost.attachments],
                    currentAttachment: (() => {
                        if (this.setupFromPost.attachments.length) {
                            return this.setupFromPost.attachments[0].type;
                        } else {
                            return undefined;
                        }
                    })(),
                    isEditing: true,
                    setupFromPost: this.setupFromPost,
                },
            };
        } else {
            this.state = {
                ...this.state,
                ready: true,
                queueRemoveAttachments: [],
                props: {
                    ...this.state.props,
                    attachments: [],
                    currentAttachment: undefined,
                    isEditing: false,
                    setupFromPost: undefined,
                },
            };
        }

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                enableAutoFocus: this.enableAutoFocus,
                forceExpandedForm: this.forceExpandedForm,
                forceDisplayCancelButton: this.forceDisplayCancelButton,
                forcePin: this.forcePin,
                viewMode: this.viewMode,
            },
        };

        this.cdr.detectChanges();
    }

    get isEditing(): boolean {
        return this.state.props.isEditing;
    }

    onAttachPhoto($event: AttachPhotoRequest): void {
        const toAttach: Array<File> = [];

        for (let i: number = 0; i < $event.files.length; i++) {
            toAttach.push($event.files[i]);
        }

        if (toAttach.length + this.state.props.attachments.length > EvodeskFeedPostEditorFormContainerComponent.MAX_ATTACHMENTS) {
            this.showTooManyAttachmentsDialog();
        } else {
            if (toAttach.length > 0) {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        attaching: false,
                    },
                };

                observableForkJoin(toAttach.map(f => this.attachmentRESTService.attachPhoto(f))).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                    (attachments) => {
                        this.state = {
                            ...this.state,
                            props: {
                                ...this.state.props,
                                attachments: [...this.state.props.attachments, ...attachments],
                                currentAttachment: AttachmentType.Photo,
                            },
                        };

                        this.view.setScreen(Screen.PostWithAttachments);

                        this.cdr.detectChanges();
                        this.cdr.detectChanges();
                    },
                    () => {
                        this.showFailedToAttachDialog();
                    },
                );
            }
        }
    }

    onAttachVideo($event: AttachVideoRequest): void {
        if (this.state.props.attachments.length >= EvodeskFeedPostEditorFormContainerComponent.MAX_ATTACHMENTS) {
            this.showTooManyAttachmentsDialog();
        } else {
            this.attachmentRESTService.attachVideo($event.url).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                (attachment) => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            attachments: [...this.state.props.attachments, attachment],
                            currentAttachment: AttachmentType.Video,
                        },
                    };

                    this.view.setScreen(Screen.PostWithAttachments);

                    this.cdr.detectChanges();
                },
                () => {
                    this.showFailedToAttachDialog();
                },
            );
        }
    }

    onAttachLink($event: AttachLinkRequest): void {
        if (this.state.props.attachments.length >= EvodeskFeedPostEditorFormContainerComponent.MAX_ATTACHMENTS) {
            this.showTooManyAttachmentsDialog();
        } else {
            this.attachmentRESTService.attachLink($event.url).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                (attachment) => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            attachments: [...this.state.props.attachments, attachment],
                            currentAttachment: AttachmentType.Link,
                        },
                    };

                    this.view.setScreen(Screen.PostWithAttachments);

                    this.cdr.detectChanges();
                },
                () => {
                    this.showFailedToAttachDialog();
                },
            );
        }
    }

    onRemoveAttachment(attachment: AnyAttachmentModel): void {
        this.uiConfirmService.open({
            title: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.ConfirmRemoveAttachment.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.ConfirmRemoveAttachment.Text',
                translate: true,
            },
            confirmButton: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.ConfirmRemoveAttachment.Confirm',
                translate: true,
            },
            cancelButton: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.ConfirmRemoveAttachment.Cancel',
                translate: true,
            },
            confirm: () => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        attachments: this.state.props.attachments.filter(a => a.id !== attachment.id),
                    },
                };

                if (this.isEditing) {
                    this.state = {
                        ...this.state,
                        queueRemoveAttachments: [...this.state.queueRemoveAttachments, attachment.id],
                    };
                }

                if (this.state.props.attachments.length === 0) {
                    this.view.setScreen(Screen.TextPost);
                }
            },
        }).subscribe();
    }

    onNgSubmit($event: EvodeskFeedPostEditorNgSubmitEvent): void {
        const formValue: EvodeskFeedPostEditorFormComponentFormValue = $event.formValue;
        const attachments: Array<AnyAttachmentModel> = $event.attachments;

        this.state.props.form.disable();

        const fail: Function = () => {
            this.state.props.form.enable();

            this.failEvent.emit({
                formValue: formValue,
            });
        };

        const success: Function = (post: PostModel) => {
            this.view.setScreen(Screen.TextPost);

            this.state.props.form.reset();
            this.state.props.form.enable();

            if (this.doPushPostToState) {
                if (this.isEditing) {
                    this.feedStateService.dispatch({
                        type: EvodeskFeedStateAction.UpdatePost,
                        payload: {
                            post: post,
                        },
                    });
                } else {
                    this.feedStateService.dispatch({
                        type: EvodeskFeedStateAction.CreatePost,
                        payload: {
                            post: post,
                        },
                    });
                }
            }

            this.successEvent.emit({
                formValue: formValue,
                post: post,
            });

            this.reset();
        };

        Observable.create(response => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            const subs: Array<Observable<any>> = [];
            if (this.isEditing) {
                this.state.queueRemoveAttachments.forEach((aId) => {
                    subs.push(this.attachmentRESTService.deletePostAttachment(this.setupFromPost.id, aId));
                });
            }
            forkJoin(...subs).subscribe(() => {}, () => {}, () => {
                const httpQuery: Observable<PostModel> = (() => {
                    if (this.isEditing) {
                        return this.postRESTService.updatePost(this.setupFromPost.id, {
                            text: formValue.text,
                            pinned: formValue.pinned,
                            type: PostType.Text,
                            attachments: attachments.map(a => a.id),
                        });
                    } else {
                        return this.postRESTService.createNewPost({
                            text: formValue.text,
                            pinned: formValue.pinned,
                            type: PostType.Text,
                            attachments: attachments.map(a => a.id),
                        });
                    }
                })();

                httpQuery.pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        success(httpResponse);

                        response.next(httpResponse);
                        response.complete();
                    },
                    (httpError) => {
                        fail();

                        this.uiAlertService.open({
                            title: {
                                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.Fail.Title',
                                translate: true,
                            },
                            text: {
                                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.Fail.Text',
                                translate: true,
                            },
                            ok: {
                                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.Fail.Close',
                                translate: true,
                            },
                        }).subscribe();

                        response.error(httpError);
                    },
                );

                return () => {
                    unsubscribe$.next(undefined);
                };
            });
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }

    onCancel(): void {
        this.reset();
        this.cancelEvent.emit();
    }

    onCloseModal(): void {
        this.closeModalEvent.emit(undefined);
    }

    private showFailedToAttachDialog(): void {
        this.uiAlertService.open({
            title: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.FailedToAttach.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.FailedToAttach.Text',
                translate: true,
                replaces: [
                    EvodeskFeedPostEditorFormContainerComponent.MAX_ATTACHMENTS,
                ],
            },
            ok: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.FailedToAttach.Close',
                translate: true,
            },
        }).subscribe();
    }

    private showTooManyAttachmentsDialog(): void {
        this.uiAlertService.open({
            title: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.TooManyAttachments.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.TooManyAttachments.Text',
                translate: true,
                replaces: [
                    EvodeskFeedPostEditorFormContainerComponent.MAX_ATTACHMENTS,
                ],
            },
            ok: {
                text: 'EvodeskFeed.components.EvodeskFeedPostEditorFormContainerComponent.TooManyAttachments.Close',
                translate: true,
            },
        }).subscribe();
    }
}
