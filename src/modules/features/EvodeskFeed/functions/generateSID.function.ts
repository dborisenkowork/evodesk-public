import {FeedEntryType} from '../state/EvodeskFeedState.service';

export function generateSID(type: FeedEntryType, ids: Array<string>): string {
    return (<string>type) + ':' + ids.join('.');
}
