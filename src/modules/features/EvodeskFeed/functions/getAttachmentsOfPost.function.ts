import {AttachmentType, AttachmentListItem, AttachmentListItemPost, PostModel} from '../../../app/EvodeskRESTApi/models/post/Post.model';

export function getAttachmentsOfPost(input: PostModel): Array<AttachmentListItem> {
    const attachments: Array<AttachmentListItem> = [];

    input.attachments.forEach(a => {
        attachments.push(<AttachmentListItem>{
            type: a.type,
            attachment: a,
        });
    });

    if (!! input.post) {
        attachments.push(<AttachmentListItemPost>{
            type: AttachmentType.Post,
            attachment: {
                id: input.id,
                origPost: input.post,
                repost: input,
            },
        });
    }

    return attachments;
}
