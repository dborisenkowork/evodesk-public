import {NgModule, Provider} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskFeedBodyParserService} from './services/EvodeskFeedBodyParser.service';
import {EvodeskFeedOpenPostService} from './services/EvodeskFeedOpenPost.service';
import {EvodeskFeedService} from './services/EvodeskFeed.service';
import {EvodeskFeedStateService} from './state/EvodeskFeedState.service';

import {EvodeskFeedContainerComponent} from './components/EvodeskFeed/EvodeskFeedContainer.component';
import {EvodeskFeedGroupedByDateComponent} from './components/EvodeskFeed/EvodeskFeedGroupedByDate/EvodeskFeedGroupedByDate.component';
import {EvodeskFeedDefaultComponent} from './components/EvodeskFeed/EvodeskFeedDefault/EvodeskFeedDefault.component';
import {EvodeskFeedPostComponent} from './components/EvodeskFeedPost/EvodeskFeedPost.component';
import {EvodeskFeedPostContainerComponent} from './components/EvodeskFeedPost/EvodeskFeedPostContainer.component';
import {EvodeskFeedPostEditorFormContainerComponent} from './components/EvodeskFeedPostEditorForm/EvodeskFeedPostEditorFormContainer.component';
import {EvodeskFeedPostEditorFormComponent} from './components/EvodeskFeedPostEditorForm/EvodeskFeedPostEditorForm.component';
import {EvodeskFeedAttachLinkModalComponent} from './components/EvodeskFeedAttachLinkModal/EvodeskFeedAttachLinkModal.component';
import {EvodeskFeedPostAttachmentLinkComponent} from './components/EvodeskFeedPostAttachmentLink/EvodeskFeedPostAttachmentLink.component';
import {EvodeskFeedPostAttachmentPhotoComponent} from './components/EvodeskFeedPostAttachmentPhoto/EvodeskFeedPostAttachmentPhoto.component';
import {EvodeskFeedPostAttachmentVideoComponent} from './components/EvodeskFeedPostAttachmentVideo/EvodeskFeedPostAttachmentVideo.component';
import {EvodeskFeedPostListAttachmentsComponent} from './components/EvodeskFeedPostListAttachments/EvodeskFeedPostListAttachments.component';
import {EvodeskFeedRepostModalComponent} from './components/EvodeskFeedRepostModal/EvodeskFeedRepostModal.component';
import {EvodeskFeedCommentComponent} from './components/EvodeskFeedComment/EvodeskFeedComment.component';
import {EvodeskFeedCommentContainerComponent} from './components/EvodeskFeedComment/EvodeskFeedCommentContainer.component';
import {EvodeskFeedCommentFormComponent} from './components/EvodeskFeedCommentForm/EvodeskFeedCommentForm.component';
import {EvodeskFeedCommentFormContainerComponent} from './components/EvodeskFeedCommentForm/EvodeskFeedCommentFormContainer.component';
import {EvodeskFeedCommentsComponent} from './components/EvodeskFeedComments/EvodeskFeedComments.component';
import {EvodeskFeedCommentsContainerComponent} from './components/EvodeskFeedComments/EvodeskFeedCommentsContainer.component';
import {EvodeskFeedPostAttachmentPostComponent} from './components/EvodeskFeedPostAttachmentPost/EvodeskFeedPostAttachmentPost.component';
import {EvodeskFeedPostAttachmentPostContainerComponent} from './components/EvodeskFeedPostAttachmentPost/EvodeskFeedPostAttachmentPostContainer.component';
import {EvodeskFeedPostRepostedComponent} from './components/EvodeskFeedPostReposted/EvodeskFeedPostReposted.component';
import {EvodeskFeedPostRepostedContainerComponent} from './components/EvodeskFeedPostReposted/EvodeskFeedPostRepostedContainer.component';
import {EvodeskFeedPostLikedComponent} from './components/EvodeskFeedPostLiked/EvodeskFeedPostLiked.component';
import {EvodeskFeedPostLikedContainerComponent} from './components/EvodeskFeedPostLiked/EvodeskFeedPostLikedContainer.component';
import {EvodeskFeedPostCommentedComponent} from './components/EvodeskFeedPostCommented/EvodeskFeedPostCommented.component';
import {EvodeskFeedPostCommentedContainerComponent} from './components/EvodeskFeedPostCommented/EvodeskFeedPostCommentedContainer.component';
import {EvodeskFeedPostCommentLikedComponent} from './components/EvodeskFeedPostCommentLiked/EvodeskFeedPostCommentLiked.component';
import {EvodeskFeedPostCommentLikedContainerComponent} from './components/EvodeskFeedPostCommentLiked/EvodeskFeedPostCommentLikedContainer.component';
import {EvodeskFeedPostLikesFloatComponent} from './components/EvodeskFeedPostLikesFloat/EvodeskFeedPostLikesFloat.component';
import {EvodeskFeedPostLikesFloatContainerComponent} from './components/EvodeskFeedPostLikesFloat/EvodeskFeedPostLikesFloatContainer.component';
import {EvodeskFeedPostRepostsFloatComponent} from './components/EvodeskFeedPostRepostsFloat/EvodeskFeedPostRepostsFloat.component';
import {EvodeskFeedPostRepostsFloatContainerComponent} from './components/EvodeskFeedPostRepostsFloat/EvodeskFeedPostRepostsFloatContainer.component';
import {EvodeskFeedPostLikesModalComponent} from './components/EvodeskFeedPostLikesModal/EvodeskFeedPostLikesModal.component';
import {EvodeskFeedPostLikesModalContainerComponent} from './components/EvodeskFeedPostLikesModal/EvodeskFeedPostLikesModalContainer.component';
import {EvodeskFeedPostRepostsModalComponent} from './components/EvodeskFeedPostRepostsModal/EvodeskFeedPostRepostsModal.component';
import {EvodeskFeedPostRepostsModalContainerComponent} from './components/EvodeskFeedPostRepostsModal/EvodeskFeedPostRepostsModalContainer.component';
import {EvodeskFeedCommentBodyComponent} from './components/EvodeskFeedCommentBody/EvodeskFeedCommentBody.component';
import {EvodeskFeedCommentBodyContainerComponent} from './components/EvodeskFeedCommentBody/EvodeskFeedCommentBodyContainer.component';
import {EvodeskFeedPostModalComponent} from './components/EvodeskFeedPost/EvodeskFeedPostModal.component';

export const EVODESK_FEED_INJECT_PROVIDERS: Array<Provider> = [
    EvodeskFeedService,
    EvodeskFeedStateService,
];

@NgModule({
    imports: [
        EvodeskAppSharedModule,
    ],
    exports: [
        EvodeskFeedContainerComponent,
        EvodeskFeedPostContainerComponent,
        EvodeskFeedPostEditorFormContainerComponent,
        EvodeskFeedPostModalComponent,
    ],
    declarations: [
        EvodeskFeedDefaultComponent,
        EvodeskFeedGroupedByDateComponent,
        EvodeskFeedContainerComponent,
        EvodeskFeedPostComponent,
        EvodeskFeedPostContainerComponent,
        EvodeskFeedPostEditorFormComponent,
        EvodeskFeedPostEditorFormContainerComponent,
        EvodeskFeedAttachLinkModalComponent,
        EvodeskFeedPostListAttachmentsComponent,
        EvodeskFeedPostAttachmentPostComponent,
        EvodeskFeedPostAttachmentPostContainerComponent,
        EvodeskFeedPostAttachmentLinkComponent,
        EvodeskFeedPostAttachmentPhotoComponent,
        EvodeskFeedPostAttachmentVideoComponent,
        EvodeskFeedRepostModalComponent,
        EvodeskFeedCommentComponent,
        EvodeskFeedCommentContainerComponent,
        EvodeskFeedCommentFormComponent,
        EvodeskFeedCommentFormContainerComponent,
        EvodeskFeedCommentsComponent,
        EvodeskFeedCommentsContainerComponent,
        EvodeskFeedPostRepostedComponent,
        EvodeskFeedPostRepostedContainerComponent,
        EvodeskFeedPostLikedComponent,
        EvodeskFeedPostLikedContainerComponent,
        EvodeskFeedPostCommentedComponent,
        EvodeskFeedPostCommentedContainerComponent,
        EvodeskFeedPostCommentLikedComponent,
        EvodeskFeedPostCommentLikedContainerComponent,
        EvodeskFeedPostLikesFloatComponent,
        EvodeskFeedPostLikesFloatContainerComponent,
        EvodeskFeedPostRepostsFloatComponent,
        EvodeskFeedPostRepostsFloatContainerComponent,
        EvodeskFeedPostLikesModalComponent,
        EvodeskFeedPostLikesModalContainerComponent,
        EvodeskFeedPostRepostsModalComponent,
        EvodeskFeedPostRepostsModalContainerComponent,
        EvodeskFeedCommentBodyComponent,
        EvodeskFeedCommentBodyContainerComponent,
        EvodeskFeedPostModalComponent,
    ],
    entryComponents: [
        EvodeskFeedAttachLinkModalComponent,
        EvodeskFeedRepostModalComponent,
        EvodeskFeedPostLikesFloatContainerComponent,
        EvodeskFeedPostRepostsFloatContainerComponent,
        EvodeskFeedPostLikesModalContainerComponent,
        EvodeskFeedPostRepostsModalContainerComponent,
        EvodeskFeedPostEditorFormContainerComponent,
        EvodeskFeedPostContainerComponent,
        EvodeskFeedPostModalComponent,
    ],
    providers: [
        EvodeskFeedBodyParserService,
        EvodeskFeedOpenPostService,
    ],
})
export class EvodeskFeedModule
{}
