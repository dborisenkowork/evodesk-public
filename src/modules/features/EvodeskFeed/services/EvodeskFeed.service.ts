import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';

import {FeedSourceAdapter} from '../source-adapters/FeedSourceAdapter.interface';

import {EvodeskFeedConfiguration} from '../models/EvodeskFeedConfiguration.model';

import {FeedEntry} from '../state/EvodeskFeedState.service';

@Injectable()
export class EvodeskFeedService
{
    private _config: EvodeskFeedConfiguration;

    get config(): EvodeskFeedConfiguration {
        if (! this._config) {
            throw new Error('No feedState configuration available');
        }

        return { ...this._config };
    }

    get source(): FeedSourceAdapter {
        return this._config.source;
    }

    setConfig(set$: (orig: EvodeskFeedConfiguration | undefined) => EvodeskFeedConfiguration): void {
        this._config = set$(this._config);
    }

    getEntries(): Array<FeedEntry> {
        return this.source.getEntries();
    }

    loadMore(): Observable<boolean> {
        return this.source.loadMore();
    }
}
