import {Injectable} from '@angular/core';

import * as ellipsize from 'ellipsize';

import {Observable} from 'rxjs';

import {EvodeskFeedCommentJSONBody} from '../models/EvodeskFeed.model';

export interface EvodeskFeedParseToHTMLPreviewOptions {
    maxLength?: number;
}

const defaultPreviewOptions: EvodeskFeedParseToHTMLPreviewOptions = {
    maxLength: 80,
};

@Injectable()
export class EvodeskFeedBodyParserService
{
    parseToHtml(input: string): Observable<string> {
        return Observable.create(observer => {
            if (!! input) {
                let result: string = input;

                result = result.replace(/(?:\r\n|\r|\n)/g, '<br/>');

                observer.next(result);
                observer.complete();
            } else {
                observer.next('');
                observer.complete();
            }
        });
    }

    parseToHtmlPreview(input: string, withOptions: EvodeskFeedParseToHTMLPreviewOptions = {}): Observable<string> {
        const options: EvodeskFeedParseToHTMLPreviewOptions = {
            ...defaultPreviewOptions,
            ...withOptions,
        };

        return Observable.create(observer => {
            observer.next(ellipsize(input, options.maxLength));
        });
    }

    parseCommentToJSOBNNBody(body: string): EvodeskFeedCommentJSONBody {
        try {
            return JSON.parse(body);
        } catch (e) {
            return {
                text: body,
            };
        }
    }

    parseCommentTextToHtml(input: string): Observable<string> {
        return Observable.create(observer => {
            if (!! input) {
                let result: string = input;

                result = result.replace(/(?:\r\n|\r|\n)/g, '<br/>');

                observer.next(result);
                observer.complete();
            } else {
                observer.next('');
                observer.complete();
            }
        });
    }

    parseCommentTextToHtmlPreview(input: string, withOptions: EvodeskFeedParseToHTMLPreviewOptions = {}): Observable<string> {
        const options: EvodeskFeedParseToHTMLPreviewOptions = {
            ...defaultPreviewOptions,
            ...withOptions,
        };

        return Observable.create(observer => {
            observer.next(ellipsize(input, options.maxLength));
        });
    }
}
