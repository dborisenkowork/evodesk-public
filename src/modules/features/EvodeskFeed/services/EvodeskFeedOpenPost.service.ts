import {Injectable, SimpleChange, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {filter, skip, takeUntil} from 'rxjs/operators';

import {PostId, PostModel} from '../../../app/EvodeskRESTApi/models/post/Post.model';

import {PostRESTService} from '../../../app/EvodeskRESTApi/services/PostREST.service';
import {EvodeskFeedPostModalComponent} from '../components/EvodeskFeedPost/EvodeskFeedPostModal.component';

@Injectable()
export class EvodeskFeedOpenPostService
{
    private inProgress$: BehaviorSubject<PostId | undefined> = new BehaviorSubject<PostId | undefined>(undefined);
    private current$: Observable<PostModel>;

    constructor(
        private matDialog: MatDialog,
        private postRESTService: PostRESTService,
    ) {}

    openPost(viewContainerRef: ViewContainerRef, postId: PostId): Observable<void> {
        return Observable.create((opened) => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            if (this.inProgress$.getValue() !== postId) {
                this.inProgress$.next(postId);

                this.current$ = this.postRESTService.getPost(postId).pipe(takeUntil(unsubscribe$), takeUntil(this.inProgress$.pipe(skip(1), filter(p => p !== postId))));

                this.current$.subscribe(
                    (httpResponse) => {
                        const matDialogRef: MatDialogRef<EvodeskFeedPostModalComponent> = this.matDialog.open(EvodeskFeedPostModalComponent, {
                            panelClass: '__evodesk-mat-dialog-feed-post-modal',
                            backdropClass: '__evodesk-mat-dialog-default-backdrop',
                            viewContainerRef: viewContainerRef,
                            autoFocus: false,
                            closeOnNavigation: true,
                        });

                        matDialogRef.componentInstance.post = httpResponse;

                        matDialogRef.componentInstance.ngOnChanges({
                            post: new SimpleChange(undefined, httpResponse, true),
                        });

                        matDialogRef.componentInstance.closeEvent.pipe(takeUntil(unsubscribe$)).subscribe(() => {
                            matDialogRef.close();

                            opened.complete();
                        });

                        matDialogRef.afterClosed().subscribe(() => {
                            this.inProgress$.next(undefined);
                        });

                        unsubscribe$.pipe(takeUntil(matDialogRef.afterClosed())).subscribe(() => {
                            matDialogRef.close();
                        });
                    },
                    (httpError) => {
                        opened.error(httpError);
                    },
                );
            }

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
