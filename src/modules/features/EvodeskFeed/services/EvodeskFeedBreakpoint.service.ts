import {Injectable} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';

import {filter, takeUntil} from 'rxjs/operators';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {FeedViewMode} from '../models/EvodeskFeed.model';

import {evodeskFeedLayoutDesktop, evodeskFeedLayoutMobile, evodeskFeedLayoutTablet} from '../configs/EvodeskFeed.constants';

@Injectable()
export class EvodeskFeedBreakpointsService
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private _current$: BehaviorSubject<FeedViewMode> = new BehaviorSubject<FeedViewMode>(FeedViewMode.Desktop);

    constructor(
        private breakpointObserver: BreakpointObserver,
    ) {}

    init(): Observable<FeedViewMode> {
        this.breakpointObserver.observe(`(max-width: ${evodeskFeedLayoutMobile.to}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe((e) => {
            this._current$.next(FeedViewMode.Mobile);
        });

        this.breakpointObserver.observe(`(min-width: ${evodeskFeedLayoutTablet.from}px) and (max-width: ${evodeskFeedLayoutTablet.to}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this._current$.next(FeedViewMode.Tablet);
        });

        this.breakpointObserver.observe(`(min-width: ${evodeskFeedLayoutDesktop.from}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this._current$.next(FeedViewMode.Desktop);
        });

        return this.current$;
    }

    destroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get current$(): Observable<FeedViewMode> {
        return this._current$.asObservable();
    }
}
