import {EvodeskFeedStateAction, EvodeskFeedStateService, FeedEntry, FeedEntryType, FeedPostCommentedEntry, FeedPostCommentLikedEntry, FeedPostEntry, FeedPostLikedEntry, FeedPostReposted} from '../state/EvodeskFeedState.service';
import {Injectable} from '@angular/core';

import * as moment from 'moment';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {NewsEntry, NewsEntryType} from '../../../app/EvodeskRESTApi/models/News.model';

import {generateSID} from '../functions/generateSID.function';

import {CurrentUserService} from '../../EvodeskAuth/services/CurrentUser.service';
import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../types/Backend.types';

export interface NewsHttpSourceOptions {
    offset: number;
    limit: number;
}

@Injectable()
export class AbstractNewsSourceAdapter
{
    constructor(
        private feedState: EvodeskFeedStateService,
        private currentUserService: CurrentUserService,
    ) {}

    getEntries(): Array<FeedEntry> {
        return this.feedState.snapshot.posts;
    }

    loadMore(httpSource: (options: NewsHttpSourceOptions) => Observable<Array<NewsEntry>>, feedEntriesPerPage: number): Observable<boolean> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create(done => {
            const nextLoadFrom: number = this.feedState.snapshot.nextLoadFrom;

            if (this.feedState.snapshot.hasPostsToLoad && ! this.feedState.snapshot.loadingMore) {
                this.feedState.dispatch({
                    type: EvodeskFeedStateAction.LoadingMore,
                });

                httpSource({
                    offset: nextLoadFrom,
                    limit: feedEntriesPerPage + 1,
                }).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        const result: Array<FeedEntry> = [];

                        httpResponse.forEach(p => {
                            switch (p.type) {
                                case NewsEntryType.Post:
                                    result.push(<FeedPostEntry>{
                                        type: FeedEntryType.Post,
                                        sid: generateSID(FeedEntryType.Post, [p.data.entity.id.toString()]),
                                        post: p.data.entity,
                                        isOwn: p.data.entity.user_id === this.currentUserService.impersonatedAs.id,
                                        date: moment(p.data.entity.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate(),
                                    });

                                    break;

                                case NewsEntryType.PostReposted:
                                    result.push(<FeedPostReposted>{
                                        type: FeedEntryType.PostReposted,
                                        sid: generateSID(FeedEntryType.PostReposted, [p.data.repost.id.toString()]),
                                        repostedBy: p.data.user,
                                        post: p.data.entity,
                                        repost: p.data.repost,
                                        date: moment(p.data.repost.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate(),
                                    });

                                    break;

                                case NewsEntryType.PostLiked:
                                    result.push(<FeedPostLikedEntry>{
                                        type: FeedEntryType.PostLiked,
                                        sid: generateSID(FeedEntryType.PostLiked, [p.data.like.id.toString()]),
                                        post: p.data.entity,
                                        likedBy: p.data.user,
                                        date: moment(p.data.like.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate(),
                                    });

                                    break;

                                case NewsEntryType.PostCommented:
                                    result.push(<FeedPostCommentedEntry>{
                                        type: FeedEntryType.PostCommented,
                                        sid: generateSID(FeedEntryType.PostCommented, [p.data.entity.id.toString()]),
                                        comment: p.data.entity,
                                        commentedBy: p.data.user,
                                        date: moment(p.data.entity.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate(),
                                    });

                                    break;

                                case NewsEntryType.PostCommentLiked:
                                    result.push(<FeedPostCommentLikedEntry>{
                                        type: FeedEntryType.PostCommentLiked,
                                        sid: generateSID(FeedEntryType.PostCommentLiked, [p.data.entity.id.toString()]),
                                        comment: p.data.comment,
                                        likedBy: p.data.user,
                                        date: moment(p.data.entity.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate(),
                                    });

                                    break;
                            }
                        });

                        this.feedState.dispatch({
                            type: EvodeskFeedStateAction.PushFeedEntries,
                            payload: {
                                feedEntries: result.slice(0, feedEntriesPerPage),
                                hasMoreToLoad: result.length > feedEntriesPerPage,
                            },
                        });

                        this.feedState.dispatch({
                            type: EvodeskFeedStateAction.LoadingMoreFinished,
                        });

                        done.next(undefined);
                        done.complete();
                    },
                    (httpError) => {
                        this.feedState.dispatch({
                            type: EvodeskFeedStateAction.LoadingMoreFinished,
                        });

                        done.error(httpError);
                    },
                );
            } else {
                this.feedState.dispatch({
                    type: EvodeskFeedStateAction.LoadingMoreFinished,
                });

                done.next(undefined);
                done.complete();
            }

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
