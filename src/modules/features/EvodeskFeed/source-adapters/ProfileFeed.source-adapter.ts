import {takeUntil} from 'rxjs/operators';
import {Injectable} from '@angular/core';

import * as moment from 'moment';

import {Observable, Subject} from 'rxjs';

import {FeedSourceAdapter} from './FeedSourceAdapter.interface';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../types/Backend.types';

import {generateSID} from '../functions/generateSID.function';

import {ProfileId} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {EvodeskFeedStateAction, EvodeskFeedStateService, FeedEntry, FeedEntryType} from '../state/EvodeskFeedState.service';
import {PostRESTService} from '../../../app/EvodeskRESTApi/services/PostREST.service';
import {CurrentUserService} from '../../EvodeskAuth/services/CurrentUser.service';

export interface ProfileFeedSourceAdapterConfig {
    profileId: ProfileId;
    postsPerPage: number;
}

type Config = ProfileFeedSourceAdapterConfig;

@Injectable()
export class ProfileFeedSourceAdapter implements FeedSourceAdapter
{
    private _config: Config;

    constructor(
        private feedState: EvodeskFeedStateService,
        private postRESTService: PostRESTService,
        private currentUserService: CurrentUserService,
    ) {}

    set config(config: Config) {
        this._config = config;
    }

    get config(): Config {
        return this._config;
    }

    getEntries(): Array<FeedEntry> {
        const entries: Array<FeedEntry> = [...this.feedState.snapshot.posts];

        const getSortBy: Function = ((input: FeedEntry) => {
            switch (input.type) {
                default:
                    throw new Error('No getSortBy strategy available');

                case FeedEntryType.Post:
                    return new Date(input.post.created_at);
            }
        });

        entries.sort((a, b) => {
            const aCriteria: any = getSortBy(a);
            const bCriteria: any = getSortBy(b);

            if (aCriteria < bCriteria) {
                return 1;
            } else if (aCriteria === bCriteria) {
                return 0;
            } else {
                return -1;
            }
        });

        return [
            ...entries.filter(p => p.type === FeedEntryType.Post && p.post.pinned.toString() === '1'),
            ...entries.filter(p => ! (p.type === FeedEntryType.Post && p.post.pinned.toString() === '1')),
        ];
    }

    loadMore(): Observable<boolean> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create(done => {
            const profileId: number = this.config.profileId;
            const nextLoadFrom: number = this.feedState.snapshot.nextLoadFrom;

            if (this.feedState.snapshot.hasPostsToLoad && ! this.feedState.snapshot.loadingMore) {
                this.feedState.dispatch({
                    type: EvodeskFeedStateAction.LoadingMore,
                });

                this.postRESTService.getPostsOfUser(profileId, {
                    from: nextLoadFrom,
                    limit: this.config.postsPerPage + 1,
                }).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        const result: Array<FeedEntry> = [];

                        httpResponse.posts.forEach(p => {
                            result.push({
                                type: FeedEntryType.Post,
                                sid: generateSID(FeedEntryType.Post, [p.id.toString()]),
                                post: p,
                                isOwn: p.user_id === this.currentUserService.impersonatedAs.id,
                                date: moment(p.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate(),
                            });
                        });

                        this.feedState.dispatch({
                            type: EvodeskFeedStateAction.PushFeedEntries,
                            payload: {
                                feedEntries: result.slice(0, this.config.postsPerPage),
                                hasMoreToLoad: result.length > this.config.postsPerPage,
                            },
                        });

                        this.feedState.dispatch({
                            type: EvodeskFeedStateAction.LoadingMoreFinished,
                        });

                        done.next(undefined);
                        done.complete();
                    },
                    (httpError) => {
                        this.feedState.dispatch({
                            type: EvodeskFeedStateAction.LoadingMoreFinished,
                        });

                        done.error(httpError);
                    },
                );
            } else {
                done.next(undefined);
                done.complete();
            }

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
