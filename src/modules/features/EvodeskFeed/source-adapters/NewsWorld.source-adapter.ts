import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';

import {FeedSourceAdapter} from './FeedSourceAdapter.interface';

import {FeedEntry} from '../state/EvodeskFeedState.service';
import {NewsRESTService} from '../../../app/EvodeskRESTApi/services/NewsREST.service';

import {AbstractNewsSourceAdapter} from './AbstractNews.source-adapter';

const feedEntriesPerPage: number = 15;

@Injectable()
export class NewsWorldSourceAdapter implements FeedSourceAdapter
{
    constructor(
        private commonNewsAdapter: AbstractNewsSourceAdapter,
        private newsRESTService: NewsRESTService,
    ) {}

    getEntries(): Array<FeedEntry> {
        return this.commonNewsAdapter.getEntries();
    }

    loadMore(): Observable<boolean> {
        return this.commonNewsAdapter.loadMore((options) => this.newsRESTService.newsWorld(options), feedEntriesPerPage);
    }
}

