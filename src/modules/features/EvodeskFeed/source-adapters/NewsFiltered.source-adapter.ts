import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';

import {FeedSourceAdapter} from './FeedSourceAdapter.interface';

import {AbstractNewsSourceAdapter} from './AbstractNews.source-adapter';

import {FeedEntry} from '../state/EvodeskFeedState.service';
import {NewsRESTService} from '../../../app/EvodeskRESTApi/services/NewsREST.service';

const feedEntriesPerPage: number = 15;

@Injectable()
export class NewsFilteredSourceAdapter implements FeedSourceAdapter
{
    constructor(
        private commonNewsAdapter: AbstractNewsSourceAdapter,
        private newsRESTService: NewsRESTService,
    ) {}

    getEntries(): Array<FeedEntry> {
        return this.commonNewsAdapter.getEntries();
    }

    loadMore(): Observable<boolean> {
        return this.commonNewsAdapter.loadMore((options) => this.newsRESTService.newsFiltered(options), feedEntriesPerPage);
    }
}
