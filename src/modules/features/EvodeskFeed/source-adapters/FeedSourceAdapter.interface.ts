import {Observable} from 'rxjs';

import {FeedEntry} from '../state/EvodeskFeedState.service';

export interface FeedSourceAdapter {
    getEntries(): Array<FeedEntry>;
    loadMore(): Observable<boolean>;
}
