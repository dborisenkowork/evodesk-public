import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable, Subject} from 'rxjs';

import * as R from 'ramda';
import * as moment from 'moment';

import {generateSID} from '../functions/generateSID.function';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../types/Backend.types';

import {PostId, PostModel} from '../../../app/EvodeskRESTApi/models/post/Post.model';
import {ProfileId, ProfileModel} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {PostCommentId, PostCommentModel} from '../../../app/EvodeskRESTApi/models/post/PostComment.model';
import {EvodeskFeedContext, FeedViewMode} from '../models/EvodeskFeed.model';

export enum FeedEntryType {
    Post = 'post',
    PostReposted = 'post-reposted',
    PostLiked = 'post-liked',
    PostCommented = 'post-commented',
    PostCommentLiked = 'post-comment-liked',
}

export type FeedEntry = FeedPostEntry | FeedPostReposted | FeedPostLikedEntry | FeedPostCommentedEntry | FeedPostCommentLikedEntry;

interface BaseFeedEntry {
    date: Date;
}

export interface FeedPostEntry extends BaseFeedEntry { type: FeedEntryType.Post; sid: string; post: PostModel; isOwn: boolean; }
export interface FeedPostLikedEntry extends BaseFeedEntry { type: FeedEntryType.PostLiked; sid: string; post: PostModel; likedBy: ProfileModel; }
export interface FeedPostCommentedEntry extends BaseFeedEntry { type: FeedEntryType.PostCommented; sid: string; comment: PostCommentModel; commentedBy: ProfileModel; }
export interface FeedPostCommentLikedEntry extends BaseFeedEntry { type: FeedEntryType.PostCommentLiked;  sid: string; comment: PostCommentModel; likedBy: ProfileModel; }
export interface FeedPostReposted extends BaseFeedEntry { type: FeedEntryType.PostReposted; sid: string; post: PostModel; repost: PostModel; repostedBy: ProfileModel; }

export interface EvodeskFeedState {
    context: EvodeskFeedContext;
    viewMode: FeedViewMode;
    isOwnProfile: boolean;
    currentProfile: {
        profileId: number;
        profileName: string;
        profileImage: string | null;
    };
    posts: Array<FeedEntry>;
    editing: Array<PostId>;
    commenting: Array<PostId>;
    postComments: { [postId: number]: Array<PostCommentModel> };
    hasPostsToLoad: boolean;
    loadingMore: boolean;
    nextLoadFrom: number;
    reply?: PostCommentModel;
}

type PrevState = EvodeskFeedState | undefined;
type CurrentState = EvodeskFeedState | undefined;

export enum EvodeskFeedStateAction {
    Setup,
    SetViewMode,
    SetContext,
    LoadingMore,
    LoadingMoreFinished,
    PushFeedEntries,
    CreatePost,
    PinPost,
    UnpinPost,
    RemovePost,
    UpdatePost,
    LikePost,
    PostDislike,
    MarkPostAsEditing,
    StopEditPost,
    CreatePostComment,
    EditPostComment,
    DeletePostComment,
    PushPostComments,
    ClearPostComments,
    LikePostComment,
    DislikePostComment,
    ReplyPostComment,
    ClearReplyPostComment,
    MarkAsReposted,
}

export type EvodeskFeedStateActions =
      { type: EvodeskFeedStateAction.Setup, payload: { profile: { id: ProfileId; avatar: string | null; name: string | null; email: string | null; }, isOwnProfile: boolean } }
    | { type: EvodeskFeedStateAction.SetViewMode, payload: { viewMode: FeedViewMode } }
    | { type: EvodeskFeedStateAction.LoadingMore }
    | { type: EvodeskFeedStateAction.LoadingMoreFinished }
    | { type: EvodeskFeedStateAction.PushFeedEntries, payload: { feedEntries: Array<FeedEntry>; hasMoreToLoad: boolean; } }
    | { type: EvodeskFeedStateAction.CreatePost, payload: { post: PostModel } }
    | { type: EvodeskFeedStateAction.UpdatePost, payload: { post: PostModel } }
    | { type: EvodeskFeedStateAction.PinPost, payload: { post: PostModel } }
    | { type: EvodeskFeedStateAction.UnpinPost, payload: { post: PostModel } }
    | { type: EvodeskFeedStateAction.RemovePost, payload: { post: PostModel } }
    | { type: EvodeskFeedStateAction.LikePost, payload: { post: PostModel } }
    | { type: EvodeskFeedStateAction.PostDislike, payload: { post: PostModel } }
    | { type: EvodeskFeedStateAction.MarkPostAsEditing, payload: { post: PostModel } }
    | { type: EvodeskFeedStateAction.StopEditPost, payload: { postId: PostId } }
    | { type: EvodeskFeedStateAction.CreatePostComment, payload: { postId: PostId; comment: PostCommentModel; } }
    | { type: EvodeskFeedStateAction.EditPostComment, payload: { postId: PostId; comment: PostCommentModel; } }
    | { type: EvodeskFeedStateAction.DeletePostComment, payload: { postId: PostId; commentId: PostCommentId; } }
    | { type: EvodeskFeedStateAction.PushPostComments, payload: { postId: PostId, comments: Array<PostCommentModel>; incrementPostCounter?: boolean } }
    | { type: EvodeskFeedStateAction.ClearPostComments, payload: { postId: PostId; } }
    | { type: EvodeskFeedStateAction.LikePostComment, payload: { postId: PostId, commentId: PostCommentId; } }
    | { type: EvodeskFeedStateAction.DislikePostComment, payload: { postId: PostId, commentId: PostCommentId; } }
    | { type: EvodeskFeedStateAction.ReplyPostComment, payload: { comment: PostCommentModel; } }
    | { type: EvodeskFeedStateAction.ReplyPostComment, payload: { comment: PostCommentModel; } }
    | { type: EvodeskFeedStateAction.ClearReplyPostComment }
    | { type: EvodeskFeedStateAction.SetContext, payload: { context: EvodeskFeedContext } }
    | { type: EvodeskFeedStateAction.MarkAsReposted, payload: { postId: PostId } }
;

interface States {
    previous: PrevState;
    current: CurrentState;
}

function initialState(): EvodeskFeedState {
    return {
        context: EvodeskFeedContext.Default,
        viewMode: FeedViewMode.Desktop,
        commenting: [],
        editing: [],
        postComments: {},
        hasPostsToLoad: true,
        loadingMore: false,
        nextLoadFrom: 0,
        isOwnProfile: false,
        posts: [],
        currentProfile: {
            profileId: -1,
            profileImage: null,
            profileName: '',
        },
    };
}

@Injectable()
export class EvodeskFeedStateService {
    private _checkLoadMore$: Subject<void> = new Subject<void>();

    private destroy$: Subject<void> = new Subject<void>();

    private _side$: BehaviorSubject<EvodeskFeedStateActions | undefined> = new BehaviorSubject<EvodeskFeedStateActions | undefined>(undefined);

    private _current$: BehaviorSubject<States> = new BehaviorSubject<States>({
        previous: undefined,
        current: initialState(),
    });

    get side$(): Observable<EvodeskFeedStateActions> {
        return this._side$.asObservable();
    }

    get current$(): Observable<States> {
        return this._current$.asObservable();
    }

    get snapshot(): EvodeskFeedState {
        return this._current$.getValue().current;
    }

    get checkLoadMore$(): Observable<void> {
      return this._checkLoadMore$.asObservable();
    }

    reset(): void {
        this._current$.next({
            previous: undefined,
            current: initialState(),
        });
    }

    destroy(): void {
        this.destroy$.next(undefined);
        this.reset();
    }

    dispatch(action: EvodeskFeedStateActions) {
        switch (action.type) {
            case EvodeskFeedStateAction.LoadingMore:
                this.setState((orig) => {
                    return {
                        ...orig,
                        loadingMore: true,
                    };
                });

                break;

            case EvodeskFeedStateAction.SetViewMode:
                this.setState((orig) => {
                    return {
                        ...orig,
                        viewMode: action.payload.viewMode,
                    };
                });

                break;

            case EvodeskFeedStateAction.LoadingMoreFinished:
                this.setState((orig) => {
                    return {
                        ...orig,
                        loadingMore: false,
                    };
                });

                break;

            case EvodeskFeedStateAction.PushFeedEntries:
                this.setState((orig) => {
                    return {
                        ...orig,
                        hasPostsToLoad: action.payload.hasMoreToLoad,
                        posts: R.uniqBy((input: FeedEntry) => input.sid, [...(orig && orig.posts ? orig.posts : []), ...action.payload.feedEntries]),
                        nextLoadFrom: orig.nextLoadFrom + action.payload.feedEntries.length,
                    };
                });

                break;

            case EvodeskFeedStateAction.Setup:
                this.setState((orig) => {
                    return {
                        ...orig,
                        isOwnProfile: action.payload.isOwnProfile,
                        currentProfile: {
                            profileId: action.payload.profile.id,
                            profileImage: action.payload.profile.avatar,
                            profileName: action.payload.profile.name ? action.payload.profile.name : action.payload.profile.email,
                        },
                        editing: [],
                        commenting: [],
                    };
                });

                break;

            case EvodeskFeedStateAction.CreatePost:
                this.setState((orig) => {
                    return {
                        ...orig,
                        posts: [{
                            type: FeedEntryType.Post,
                            sid: generateSID(FeedEntryType.Post, [action.payload.post.id.toString()]),
                            post: action.payload.post,
                            comments: [],
                            isOwn: true,
                            date: moment(action.payload.post.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate(),
                        }, ...orig.posts],
                    };
                });

                break;

            case EvodeskFeedStateAction.UpdatePost:
                this.setState((orig) => {
                    return {
                        ...orig,
                        posts: orig.posts.map((p) => {
                            if (p.type === FeedEntryType.Post && p.post.id === action.payload.post.id) {
                                return {
                                    ...p,
                                    post: action.payload.post,
                                };
                            } else {
                                return p;
                            }
                        }),
                    };
                });

                break;

            case EvodeskFeedStateAction.PinPost:
                this.setState((orig) => {
                    return {
                        ...orig,
                        posts: orig.posts.map(p => {
                            if (p.type === FeedEntryType.Post && p.post.id === action.payload.post.id) {
                                return { ...p, post: { ...p.post, pinned: <any>'1' } };
                            } else {
                                return p;
                            }
                        }),
                    };
                });

                break;

            case EvodeskFeedStateAction.UnpinPost:
                this.setState((orig) => {
                    return {
                        ...orig,
                        posts: orig.posts.map(p => {
                            if (p.type === FeedEntryType.Post && p.post.id === action.payload.post.id) {
                                return { ...p, post: { ...p.post, pinned: <any>'0' } };
                            } else {
                                return p;
                            }
                        }),
                    };
                });

                break;

            case EvodeskFeedStateAction.RemovePost:
                this.setState((orig) => {
                    return {
                        ...orig,
                        posts: orig.posts.filter(p => (p.type !== FeedEntryType.Post) || (p.type === FeedEntryType.Post && p.post.id !== action.payload.post.id)),
                    };
                });

                break;

            case EvodeskFeedStateAction.LikePost:
                this.setState((orig) => {
                    return {
                        ...orig,
                        posts: orig.posts.map((p) => {
                            if (p.type === FeedEntryType.Post && p.post.id === action.payload.post.id ) {
                                return {
                                    ...p,
                                    post: {
                                        ...p.post,
                                        isLiked: true,
                                        likes: p.post.likes + 1,
                                    },
                                };
                            } else {
                                return p;
                            }
                        }),
                    };
                });

                break;

            case EvodeskFeedStateAction.PostDislike:
                this.setState((orig) => {
                    return {
                        ...orig,
                        posts: orig.posts.map((p) => {
                            if (p.type === FeedEntryType.Post && p.post.id === action.payload.post.id ) {
                                return {
                                    ...p,
                                    post: {
                                        ...p.post,
                                        isLiked: false,
                                        likes: p.post.likes - 1,
                                    },
                                };
                            } else {
                                return p;
                            }
                        }),
                    };
                });

                break;

            case EvodeskFeedStateAction.MarkPostAsEditing:
                this.setState((orig) => {
                    return {
                        ...orig,
                        editing: [action.payload.post.id],
                    };
                });

                break;

            case EvodeskFeedStateAction.StopEditPost:
                this.setState((orig) => {
                    return {
                        ...orig,
                        editing: orig.editing.filter(p => p !== action.payload.postId),
                    };
                });

                break;

            case EvodeskFeedStateAction.CreatePostComment:
                this.setState((orig) => {
                    return {
                        ...orig,
                        postComments: {
                            ...orig.postComments,
                            [action.payload.postId]: [...(orig.postComments[action.payload.postId] || []), action.payload.comment],
                        },
                        posts: orig.posts.map((p) => {
                            if (p.type === FeedEntryType.Post && p.post.id === action.payload.postId) {
                                return {
                                    ...p,
                                    post: {
                                        ...p.post,
                                        comments: p.post.comments + 1,
                                    },
                                };
                            } else {
                                return p;
                            }
                        }),
                    };
                });

                break;

            case EvodeskFeedStateAction.EditPostComment:
                this.setState((orig) => {
                    return {
                        ...orig,
                        postComments: {
                            ...orig.postComments,
                            [action.payload.postId]: (orig.postComments[action.payload.postId] || []).map((c) => {
                                if (c.id === action.payload.comment.id) {
                                    return action.payload.comment;
                                } else {
                                    return c;
                                }
                            }),
                        },
                    };
                });

                break;

            case EvodeskFeedStateAction.DeletePostComment:
                this.setState((orig) => {
                    return {
                        ...orig,
                        postComments: {
                            ...orig.postComments,
                            [action.payload.postId]: (orig.postComments[action.payload.postId] || []).filter((c) => c.id !== action.payload.commentId),
                        },
                        posts: orig.posts.map((p) => {
                            if (p.type === FeedEntryType.Post && p.post.id === action.payload.postId) {
                                return {
                                    ...p,
                                    post: {
                                        ...p.post,
                                        comments: p.post.comments - 1,
                                    },
                                };
                            } else {
                                return p;
                            }
                        }),
                    };
                });

                break;

            case EvodeskFeedStateAction.PushPostComments:
                this.setState((orig) => {
                    return {
                        ...orig,
                        postComments: {
                            ...orig.postComments,
                            [action.payload.postId]: R.uniqBy((input: PostCommentModel) => {
                                return input.id;
                            }, [...(orig.postComments[action.payload.postId] || []), ...action.payload.comments]),
                        },
                    };
                });

                break;

            case EvodeskFeedStateAction.ClearPostComments:
                this.setState((orig) => {
                    return {
                        ...orig,
                        postComments: {
                            ...orig.postComments,
                            [action.payload.postId]: [],
                        },
                    };
                });

                break;

            case EvodeskFeedStateAction.LikePostComment:
                this.setState((orig) => {
                    return {
                        ...orig,
                        postComments: {
                            ...orig.postComments,
                            [action.payload.postId]: (orig.postComments[action.payload.postId] || []).map((c) => {
                                if (c.id === action.payload.commentId) {
                                    return { ...c, isLiked: true, likes: c.likes + 1 };
                                } else {
                                    return c;
                                }
                            }),
                        },
                    };
                });

                break;

            case EvodeskFeedStateAction.DislikePostComment:
                this.setState((orig) => {
                    return {
                        ...orig,
                        postComments: {
                            ...orig.postComments,
                            [action.payload.postId]: (orig.postComments[action.payload.postId] || []).map((c) => {
                                if (c.id === action.payload.commentId) {
                                    return { ...c, isLiked: false, likes: Math.max(0, c.likes - 1) };
                                } else {
                                    return c;
                                }
                            }),
                        },
                    };
                });

                break;

            case EvodeskFeedStateAction.ReplyPostComment:
                this.setState((orig) => {
                    return {
                        ...orig,
                        reply: action.payload.comment,
                    };
                });

                break;

            case EvodeskFeedStateAction.ClearReplyPostComment:
                this.setState((orig) => {
                    return {
                        ...orig,
                        reply: undefined,
                    };
                  });

                break;

            case EvodeskFeedStateAction.SetContext:
                this.setState((orig) => {
                    return {
                        ...orig,
                        context: action.payload.context,
                    };
                });

                break;

            case EvodeskFeedStateAction.MarkAsReposted:
                this.setState((orig) => {
                    return {
                        ...orig,
                        posts: orig.posts.map((p) => {
                            if (p.type === FeedEntryType.Post && p.post.id === action.payload.postId ) {
                                return {
                                    ...p,
                                    post: {
                                        ...p.post,
                                        isReposted: true,
                                        reposts: p.post.reposts + 1,
                                    },
                                };
                            } else {
                                return p;
                            }
                        }),
                    };
                });

                break;
        }

        this._side$.next(action);
    }

    public checkLoadMore() {
        this._checkLoadMore$.next();
    }

    private setState(set$: (orig: EvodeskFeedState) => EvodeskFeedState) {
        const orig: EvodeskFeedState = this._current$.getValue().current;

        this._current$.next({
            previous: orig,
            current: set$(orig),
        });
    }
}
