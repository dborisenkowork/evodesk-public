export const evodeskFeedLayoutMobile: { to: number } = { to: 420 };
export const evodeskFeedLayoutTablet: { from: number, to: number } = { from: 421, to: 1160 };
export const evodeskFeedLayoutDesktop: { from: number } = { from: 1161 };

export const evodeskFeedMaxReplyDepth: number = 3;
