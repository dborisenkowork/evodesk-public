import {FeedSourceAdapter} from '../source-adapters/FeedSourceAdapter.interface';

export interface EvodeskFeedConfiguration {
    source: FeedSourceAdapter;
}
