import {ViewMode} from '../../../../types/ViewMode.types';
import {ProfileId} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';

export enum EvodeskFeedContext
{
    Default = 'default',
    Profile = 'profile',
    NewsIndex = 'news-index',
    NewsAdverts = 'news-adverts',
    NewsFiltered = 'news-filtered',
    NewsNotifications = 'news-notifications',
    NewsWorld = 'news-world',
    NewsSubscribers = 'news-subcribers',
}

export const evodeskFeedNewsContexts: Array<EvodeskFeedContext> = [
    EvodeskFeedContext.NewsIndex,
    EvodeskFeedContext.NewsAdverts,
    EvodeskFeedContext.NewsFiltered,
    EvodeskFeedContext.NewsNotifications,
    EvodeskFeedContext.NewsWorld,
    EvodeskFeedContext.NewsSubscribers,
];

export {ViewMode as FeedViewMode};

export interface EvodeskFeedCommentJSONBody {
    text: string;
    reply?: {
        authorProfileId: ProfileId;
        authorProfileName: string;
        body: EvodeskFeedCommentJSONBody;
    };
}
