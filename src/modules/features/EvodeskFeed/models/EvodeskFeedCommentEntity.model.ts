import {PostModel} from '../../../app/EvodeskRESTApi/models/post/Post.model';

export enum SupportedCommentType
{
    Post = 'post',
}

export interface EvodeskFeedCommentsEntityPost {
    type: SupportedCommentType.Post;
    post: PostModel;
}

export type EvodeskFeedCommentEntity = EvodeskFeedCommentsEntityPost;
