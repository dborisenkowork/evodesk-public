import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskPartnersRoutingModule} from './EvodeskPartnersRouting.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';

import {EvodeskPartnersComponent} from './components/EvodeskPartners/EvodeskPartners.component';
import {EvodeskPartnersContainerComponent} from './components/EvodeskPartners/EvodeskPartnersContainer.component';
import {EvodeskPartnersHowEarnEvoCoinComponent} from './components/EvodeskPartnersHowEarnEvoCoin/EvodeskPartnersHowEarnEvoCoin.component';
import {EvodeskPartnersHowEarnEvoCoinContainerComponent} from './components/EvodeskPartnersHowEarnEvoCoin/EvodeskPartnersHowEarnEvoCoinContainer.component';
import {EvodeskPartnersHowEarnEvoPointsContainerComponent} from './components/EvodeskPartnersHowEarnEvoPoints/EvodeskPartnersHowEarnEvoPointsContainer.component';
import {EvodeskPartnersHowEarnEvoPointsComponent} from './components/EvodeskPartnersHowEarnEvoPoints/EvodeskPartnersHowEarnEvoPoints.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskPartnersRoutingModule,
    ],
    declarations: [
        IndexRouteComponent,

        EvodeskPartnersComponent,
        EvodeskPartnersContainerComponent,
        EvodeskPartnersHowEarnEvoCoinComponent,
        EvodeskPartnersHowEarnEvoCoinContainerComponent,
        EvodeskPartnersHowEarnEvoPointsComponent,
        EvodeskPartnersHowEarnEvoPointsContainerComponent,
    ],
    entryComponents: [
        EvodeskPartnersHowEarnEvoCoinContainerComponent,
        EvodeskPartnersHowEarnEvoPointsContainerComponent,
    ],
})
export class EvodeskPartnersModule
{}
