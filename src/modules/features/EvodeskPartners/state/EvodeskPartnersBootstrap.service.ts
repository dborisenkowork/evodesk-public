import {Injectable} from '@angular/core';

import {forkJoin, Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {ProfileId} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {EvodeskAppLoading, EvodeskAppLoadingStatusService} from '../../../app/EvodeskApp/services/EvodeskAppLoadingStatus.service';
import {EvodeskPartnersStateService, PartnersAction, PartnersBootstrapQuery} from './EvodeskPartnersState.service';
import {CurrentUserService} from '../../EvodeskAuth/services/CurrentUser.service';
import {PartnersRESTService} from '../../../app/EvodeskRESTApi/services/PartnersREST.service';

@Injectable()
export class EvodeskPartnersBootstrapService
{
    constructor(
        private appLoading: EvodeskAppLoadingStatusService,
        private currentUser: CurrentUserService,
        private partnersState: EvodeskPartnersStateService,
        private partnersRESTService: PartnersRESTService,
    ) {}

    bootstrap(withProfileId: ProfileId | undefined): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        const profileId: ProfileId = withProfileId || this.currentUser.impersonatedAs.id;
        const appLoading: EvodeskAppLoading = this.appLoading.addLoading();

        const bootstrapQuery: PartnersBootstrapQuery = {
            code: '',
            statistic: {
                totalAllPoints: 0,
                totalAllUsers: 0,
                totalTodayPoints: 0,
                totalTodayUsers: 0,
            },
            top: [],
        };

        return Observable.create((done) => {
            const queries: Array<Observable<any>> = [];

            queries.push(Observable.create(q => {
                this.partnersRESTService.getDataForPartners(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.top = httpResponse.top;
                        bootstrapQuery.statistic = httpResponse.statistic;
                        bootstrapQuery.code = httpResponse.code;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            forkJoin(queries).pipe(takeUntil(unsubscribe$)).subscribe(
                () => {
                    this.partnersState.dispatch({
                        type: PartnersAction.Bootstrap,
                        payload: {
                            bootstrap: bootstrapQuery,
                        },
                    });

                    appLoading.done();

                    done.next(undefined);
                    done.complete();
                },
                (queryError) => {
                    appLoading.done();

                    done.error(queryError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}


