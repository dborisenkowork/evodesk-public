import {Injectable} from '@angular/core';

import {PartnerModel} from '../../../app/EvodeskRESTApi/models/Partner.model';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

interface PartnersState {
    code: string;
    top: Array<PartnerModel>;
    statistic: {
        totalTodayUsers: number;
        totalTodayPoints: number;
        totalAllUsers: number;
        totalAllPoints: number;
    };
}

export type PartnersPrevState = PartnersState | undefined;
export type PartnersNextState = PartnersState;
export type PartnersStates = { previous: PartnersPrevState; current: PartnersNextState; };

export enum PartnersAction {
    Reset,
    Bootstrap,
}

export interface PartnersBootstrapQuery {
    code: string;
    top: Array<PartnerModel>;
    statistic: {
        totalTodayUsers: number;
        totalTodayPoints: number;
        totalAllUsers: number;
        totalAllPoints: number;
    };
}

function initialState(): PartnersState {
    return {
        code: '<NONE>',
        top: [],
        statistic: {
            totalTodayPoints: 0,
            totalTodayUsers: 0,
            totalAllPoints: 0,
            totalAllUsers: 0,
        },
    };
}

type Actions =
      { type: PartnersAction.Reset }
    | { type: PartnersAction.Bootstrap; payload: { bootstrap: PartnersBootstrapQuery } }
;

@Injectable()
export class EvodeskPartnersStateService
{
    private _current$: BehaviorSubject<PartnersStates> = new BehaviorSubject<PartnersStates>({ previous: undefined, current: initialState() });
    private _side$: Subject<Actions> = new Subject<Actions>();

    get current$(): Observable<PartnersStates> {
        return this._current$.asObservable();
    }

    get side$(): Observable<Actions> {
        return this._side$.asObservable();
    }

    get snapshot(): PartnersState {
        return this._current$.getValue().current;
    }

    reset(): void {
        this._current$.next({
            previous: undefined,
            current: initialState(),
        });
    }

    dispatch(action: Actions): void {
        this._side$.next(action);

        switch (action.type) {
            case PartnersAction.Reset: {
                this.reset();

                break;
            }

            case PartnersAction.Bootstrap: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        ...action.payload.bootstrap,
                    };
                });

                break;
            }
        }
    }

    private setState(set$: (orig: PartnersPrevState) => PartnersNextState): void {
        this._current$.next({
            previous: this.snapshot,
            current: set$(this.snapshot),
        });
    }
}
