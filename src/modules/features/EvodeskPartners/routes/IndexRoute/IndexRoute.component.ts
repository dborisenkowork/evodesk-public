import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {EvodeskPartnersBootstrapService} from '../../state/EvodeskPartnersBootstrap.service';
import {EvodeskPartnersStateService} from '../../state/EvodeskPartnersState.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';


interface State {
    ready: boolean;
    profileId: ProfileId | undefined;
}

@Component({
    templateUrl: './IndexRoute.component.pug',
    styleUrls: [
        './IndexRoute.component.scss',
    ],
    providers: [
        EvodeskPartnersStateService,
        EvodeskPartnersBootstrapService,
    ],
})
export class IndexRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        profileId: undefined,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private activatedRoute: ActivatedRoute,
        private currentUser: CurrentUserService,
    ) {}

    ngOnInit(): void {
        this.activatedRoute.params.pipe(takeUntil(this.ngOnDestroy$)).subscribe((params) => {
            if (params['profileId']) {
                this.state = {
                    ...this.state,
                    ready: true,
                    profileId: parseInt(params['profileId'], 10),
                };

                this.cdr.detectChanges();
            } else {
                this.state = {
                    ...this.state,
                    ready: true,
                    profileId: this.currentUser.impersonatedAs.id,
                };

                this.cdr.detectChanges();
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
