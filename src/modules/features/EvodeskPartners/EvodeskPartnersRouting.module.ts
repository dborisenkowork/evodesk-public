import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskPartnersRouting} from './configs/EvodeskPartnersRouting.module';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskPartnersRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskPartnersRoutingModule
{}
