import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

interface State {
    ready: boolean;
}

interface Props {
}

export {Props as EvodeskPartnersHowEarnEvoPointsComponentProps};

@Component({
    selector: 'evodesk-partners-how-earn-evopoints',
    templateUrl: './EvodeskPartnersHowEarnEvoPoints.component.pug',
    styleUrls: [
        './EvodeskPartnersHowEarnEvoPoints.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskPartnersHowEarnEvoPointsComponent implements OnChanges
{
    @Input() props: Props;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }
}
