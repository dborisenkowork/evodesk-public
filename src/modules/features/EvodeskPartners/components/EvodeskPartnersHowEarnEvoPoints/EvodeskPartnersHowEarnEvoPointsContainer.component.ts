import {ChangeDetectionStrategy, Component} from '@angular/core';

import {EvodeskPartnersHowEarnEvoPointsComponentProps as Props} from './EvodeskPartnersHowEarnEvoPoints.component';

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-partners-how-earn-evopoints-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-partners-how-earn-evopoints [props]="state.props"></evodesk-partners-how-earn-evopoints>
      </ng-container>
    `,
})
export class EvodeskPartnersHowEarnEvoPointsContainerComponent
{
    public state: State = {
        ready: true,
        props: {},
    };
}
