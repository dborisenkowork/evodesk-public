import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewContainerRef} from '@angular/core';
import {MatDialog} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskPartnersComponentProps as Props} from './EvodeskPartners.component';

import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {defaultMatDialogConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {EvodeskPartnersHowEarnEvoCoinContainerComponent} from '../EvodeskPartnersHowEarnEvoCoin/EvodeskPartnersHowEarnEvoCoinContainer.component';
import {EvodeskPartnersHowEarnEvoPointsContainerComponent} from '../EvodeskPartnersHowEarnEvoPoints/EvodeskPartnersHowEarnEvoPointsContainer.component';

import {EvodeskPartnersStateService} from '../../state/EvodeskPartnersState.service';
import {EvodeskPartnersBootstrapService} from '../../state/EvodeskPartnersBootstrap.service';

interface State {
    ready: boolean;
    loading: boolean;
    props?: Props | undefined;
}

@Component({
    selector: 'evodesk-partners-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-partners [props]="state.props" (howEarnPoints)="howEarnPoints()" (howEarnEvoCoin)="howEarnEvoCoin()"></evodesk-partners>
        </ng-container>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskPartnersContainerComponent implements OnDestroy, OnChanges
{
    @Input() profileId: ProfileId;

    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private nextProfileId$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        loading: false,
        props: undefined,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private partnersState: EvodeskPartnersStateService,
        private bootstrap: EvodeskPartnersBootstrapService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['profileId']) {
            this.nextProfileId$.next(undefined);

            this.state = {
                ...this.state,
                loading: true,
                props: undefined,
            };

            this.cdr.detectChanges();

            this.bootstrap.bootstrap(this.profileId).pipe(takeUntil(this.nextProfileId$)).subscribe(() => {
                this.state = {
                    ...this.state,
                    loading: false,
                };

                this.partnersState.current$.pipe(takeUntil(this.nextProfileId$)).subscribe((s) => {
                    this.state = {
                        ...this.state,
                        ready: true,
                        props: {
                            ...this.state.props,
                            code: s.current.code,
                            statistic: s.current.statistic,
                            top: s.current.top,
                        },
                    };

                    this.cdr.detectChanges();
                });
            });
        }
    }

    ngOnDestroy(): void {
        this.nextProfileId$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    howEarnPoints(): void {
        this.matDialog.open(EvodeskPartnersHowEarnEvoPointsContainerComponent, {
            ...defaultMatDialogConfig,
            viewContainerRef: this.viewContainerRef,
            closeOnNavigation: true,
            autoFocus: false,
        });
    }

    howEarnEvoCoin(): void {
        this.matDialog.open(EvodeskPartnersHowEarnEvoCoinContainerComponent, {
            ...defaultMatDialogConfig,
            viewContainerRef: this.viewContainerRef,
            closeOnNavigation: true,
            autoFocus: false,
        });
    }
}
