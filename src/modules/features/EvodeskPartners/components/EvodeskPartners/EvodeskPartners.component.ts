import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {PartnerModel} from '../../../../app/EvodeskRESTApi/models/Partner.model';

type StatsTab = 'today' | 'total';

interface Props {
    code: string;
    top: Array<PartnerModel>;
    statistic: {
        totalTodayUsers: number;
        totalTodayPoints: number;
        totalAllUsers: number;
        totalAllPoints: number;
    };
}

interface State {
    ready: boolean;
    statsTab: StatsTab;
}

export {Props as EvodeskPartnersComponentProps};

@Component({
    selector: 'evodesk-partners',
    templateUrl: './EvodeskPartners.component.pug',
    styleUrls: [
        './EvodeskPartners.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskPartnersComponent implements OnChanges
{
    @Input() props: Props;

    @Output('howEarnPoints') howEarnPointsEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('howEarnEvoCoin') howEarnEvoCoinEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        statsTab: 'today',
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    clickCode(codeInputRef: HTMLInputElement): void {
        codeInputRef.focus();
        codeInputRef.setSelectionRange(0, codeInputRef.value.length);
    }

    copyCode(codeInputRef: HTMLInputElement): void {
        codeInputRef.focus();
        codeInputRef.setSelectionRange(0, codeInputRef.value.length);

        document.execCommand('Copy');
    }

    howEarnPoints(): void {
        this.howEarnPointsEvent.emit(undefined);
    }

    howEarnEvoCoin(): void {
        this.howEarnEvoCoinEvent.emit(undefined);
    }

    setStatsTab(statsTab: StatsTab): void {
        this.state = {
            ...this.state,
            statsTab: statsTab,
        };
    }
}
