import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

interface State {
    ready: boolean;
}

interface Props {
}

export {Props as EvodeskPartnersHowEarnEvoCoinComponentProps};

@Component({
    selector: 'evodesk-partners-how-earn-evocoin',
    templateUrl: './EvodeskPartnersHowEarnEvoCoin.component.pug',
    styleUrls: [
        './EvodeskPartnersHowEarnEvoCoin.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskPartnersHowEarnEvoCoinComponent implements OnChanges
{
    @Input() props: Props;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }
}
