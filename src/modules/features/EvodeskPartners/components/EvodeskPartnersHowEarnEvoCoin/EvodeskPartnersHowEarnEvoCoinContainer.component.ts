import {ChangeDetectionStrategy, Component} from '@angular/core';

import {EvodeskPartnersHowEarnEvoCoinComponentProps as Props} from './EvodeskPartnersHowEarnEvoCoin.component';

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-partners-how-earn-evocoin-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-partners-how-earn-evocoin [props]="state.props"></evodesk-partners-how-earn-evocoin>
      </ng-container>
    `,
})
export class EvodeskPartnersHowEarnEvoCoinContainerComponent
{
    public state: State = {
        ready: true,
        props: {},
    };
}
