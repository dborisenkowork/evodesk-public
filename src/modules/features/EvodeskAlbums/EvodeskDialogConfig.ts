import {MatDialogConfig} from '@angular/material';

export const defaultDialogConfig: MatDialogConfig = {
  width: '100%',
  maxWidth: '768px',
  disableClose: true,
  autoFocus: false,
  panelClass: '__evodesk-albums-dialog',
  hasBackdrop: false,
};

export const confirmDialogConfig: MatDialogConfig = {
  width: '100%', maxWidth: 'unset', autoFocus: false, panelClass: '__evodesk-albums-confirm-dialog',
};
