import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskEditAlbumComponent} from './components/EvodeskEditAlbum/EvodeskEditAlbum.component';
import {EvodeskAddPhotosToAlbumComponent} from './components/EvodeskAddPhotosToAlbum/EvodeskAddPhotosToAlbum.component';
import {EvodeskViewAlbumComponent} from './components/EvodeskViewAlbum/EvodeskViewAlbum.component';
import {EvodeskViewAllAlbumsComponent} from './components/EvodeskViewAllAlbums/EvodeskViewAllAlbums.component';
import {EvodeskDeleteAlbumConfirmComponent} from './components/EvodeskDeleteAlbumConfirm/EvodeskDeleteAlbumConfirm.component';
import {EvodeskUploadPhotosComponent} from './components/EvodeskUploadPhotos/EvodeskUploadPhotos.component';
import {EvodeskViewPhotoComponent} from './components/EvodeskViewPhoto/EvodeskViewPhoto.component';
import {EvodeskBaseAlbumDialogComponent} from './components/EvodeskBaseAlbumDialog';

import {EvodeskBootstrapAlbumsService} from './services/EvodeskBootstrapAlbums.service';
import {EvodeskAlbumsStateService} from './services/EvodeskAlbumsState.service';

@NgModule({
    imports: [
        EvodeskAppSharedModule,
    ],
    exports: [
        EvodeskEditAlbumComponent,
        EvodeskAddPhotosToAlbumComponent,
        EvodeskViewAlbumComponent,
        EvodeskViewAllAlbumsComponent,
        EvodeskDeleteAlbumConfirmComponent,
        EvodeskUploadPhotosComponent,
        EvodeskViewPhotoComponent,
    ],
    declarations: [
        EvodeskEditAlbumComponent,
        EvodeskAddPhotosToAlbumComponent,
        EvodeskViewAlbumComponent,
        EvodeskViewAllAlbumsComponent,
        EvodeskDeleteAlbumConfirmComponent,
        EvodeskUploadPhotosComponent,
        EvodeskViewPhotoComponent,
        EvodeskBaseAlbumDialogComponent,
    ],
    entryComponents: [
        EvodeskEditAlbumComponent,
        EvodeskAddPhotosToAlbumComponent,
        EvodeskViewAlbumComponent,
        EvodeskViewAllAlbumsComponent,
        EvodeskDeleteAlbumConfirmComponent,
        EvodeskUploadPhotosComponent,
        EvodeskViewPhotoComponent,
    ],
    providers: [
        EvodeskAlbumsStateService,
        EvodeskBootstrapAlbumsService,
    ],
})
export class EvodeskAlbumsModule {}
