import {Injectable} from '@angular/core';

import {Subject, Observable, BehaviorSubject} from 'rxjs';

import {Album, Photo} from '../../../app/EvodeskRESTApi/models/album/Album.model';

export interface AlbumsStateListener {
    subscribe(albumsState: EvodeskAlbumsStateService): void;
    unsubscribe(albumsState: EvodeskAlbumsStateService): void;
}

export interface AlbumsState {
    albums: Array<Album> | undefined;
    photos: Array<Photo> | undefined;
}


export type PrevAlbumsState = AlbumsState | undefined;
export type NextAlbumsState = AlbumsState;
export type AlbumsStates = { current: NextAlbumsState, prev?: PrevAlbumsState };

function initialState(): AlbumsState {
    return {
        albums: [],
        photos: [],
    };
}

@Injectable()
export class EvodeskAlbumsStateService {
    childDialogClosed: Subject<boolean> = new Subject<boolean>();
    closeAllDialogs: Subject<boolean> = new Subject<boolean>();

    constructor() {
        this.subscribeToListeners();
    }

    private _current$: BehaviorSubject<AlbumsStates> = new BehaviorSubject<AlbumsStates>({current: initialState()});

    get current$(): Observable<AlbumsStates> {
        return this._current$.asObservable();
    }

    get listeners(): Array<AlbumsStateListener> {
        return [];
    }

    get snapshot(): AlbumsStates {
        return this._current$.getValue();
    }

    addAlbum(album: Album) {
        setTimeout(() => {
            const prev: AlbumsState = this._current$.getValue().current;
            const current: AlbumsState = prev;

            current.albums.push(album);

            this._current$.next({
                prev: prev,
                current: current,
            });
        });
    }

    addPhotosInAlbum(newAlbum: Album, newPhotos: Array<Photo>) {
        setTimeout(() => {
            const prev: AlbumsState = this._current$.getValue().current;
            const current: AlbumsState = prev;

            current.albums.map((album: Album) => {
                if (album.id === newAlbum.id) {
                    album.photos = newAlbum.photos;
                }
            });

            current.photos.push(...newPhotos);
            current.photos.sort((a, b) => a.id - b.id);

            this._current$.next({
                prev: prev,
                current: current,
            });
        });
    }

    setState(reason: string, query: (orig: AlbumsState) => AlbumsState) {
        setTimeout(() => {
            const prev: AlbumsState = this._current$.getValue().current;

            this._current$.next({
                prev: prev,
                current: query(prev),
            });
        });
    }

    reset(): void {
        this.unsubscribeFromListeners();

        this.setState('AlbumsStateService.reset', () => {
            return initialState();
        });

        this.subscribeToListeners();
    }

    handleBackdrop() {
        this.closeAllDialogs.next(true);
    }

    private subscribeToListeners(): void {
        this.listeners.forEach(l => l.subscribe(this));
    }

    private unsubscribeFromListeners(): void {
        this.listeners.forEach(l => l.unsubscribe(this));
    }
}
