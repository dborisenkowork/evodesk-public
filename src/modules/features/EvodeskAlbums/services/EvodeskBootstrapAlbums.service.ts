import {Injectable} from '@angular/core';

import {forkJoin as observableForkJoin, Observable, Subject} from 'rxjs';
import {publishLast, takeUntil} from 'rxjs/operators';

import {AlbumsRESTService} from '../../../app/EvodeskRESTApi/services/AlbumsREST.service';
import {AlbumsState, EvodeskAlbumsStateService} from './EvodeskAlbumsState.service';
import {CurrentUserService} from '../../EvodeskAuth/services/CurrentUser.service';

@Injectable()
export class EvodeskBootstrapAlbumsService
{
    constructor(
        private state: EvodeskAlbumsStateService,
        private rest: AlbumsRESTService,
        private currentUserService: CurrentUserService,
    ) {}

    bootstrap(id: number): Observable<any> {
        const profileId: number = id === undefined ? this.currentUserService.current.id : id;

        return Observable.create(observer => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            observableForkJoin([
                this._fetchAlbums(profileId, unsubscribe$),
            ]).subscribe(
                () => {
                    observer.next(true);
                    observer.complete();
                },
                (error) => {
                    observer.error(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        }).pipe(publishLast()).refCount();
    }

    private _fetchAlbums(id: number, takeUntil$: Subject<any>): Observable<any> {
        const profileId: number = id === undefined ? this.currentUserService.current.id : id;
        const observable: Observable<any> = this.rest.getAlbums(profileId).pipe(takeUntil(takeUntil$));

        observable.subscribe(next => {
            this.state.setState('BootstrapAlbumsScript._fetchAlbums', (orig: AlbumsState) => {
                return {
                    ...orig,
                    albums: next,
                };
            });
        });
        return observable;
    }
}
