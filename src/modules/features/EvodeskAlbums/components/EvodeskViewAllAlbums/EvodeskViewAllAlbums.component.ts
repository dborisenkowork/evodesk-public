import {takeUntil} from 'rxjs/operators';
import {AfterViewInit, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {EvodeskAlbumsStateService} from '../../services/EvodeskAlbumsState.service';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';
import {EvodeskViewAlbumComponent} from '../EvodeskViewAlbum/EvodeskViewAlbum.component';
import {Album, Photo} from '../../../../app/EvodeskRESTApi/models/album/Album.model';
import {confirmDialogConfig, defaultDialogConfig} from '../../EvodeskDialogConfig';
import {EvodeskProfileStateService} from '../../../EvodeskProfile/services/EvodeskProfileState.service';
import {EvodeskUploadPhotosComponent} from '../EvodeskUploadPhotos/EvodeskUploadPhotos.component';
import {environment} from '../../../../../environments/environment.config';
import {dialogConfig, EvodeskViewPhotoComponent} from '../EvodeskViewPhoto/EvodeskViewPhoto.component';
import {EvodeskBaseAlbumDialogComponent} from '../EvodeskBaseAlbumDialog';

@Component({
  selector: 'evodesk-profile-view-all-albums',
  templateUrl: './EvodeskViewAllAlbums.component.pug',
  styleUrls: [
    './EvodeskViewAllAlbums.component.scss',
  ],
})
export class EvodeskViewAllAlbumsComponent extends EvodeskBaseAlbumDialogComponent implements AfterViewInit, OnDestroy, OnInit {

  canAddAlbum: boolean;
  allPhotos: Array<Photo> = [];

  constructor(
    public state: EvodeskAlbumsStateService,
    public dialogRef: MatDialogRef<EvodeskViewAllAlbumsComponent>,
    public dialog: MatDialog,
    private profileState: EvodeskProfileStateService,
    public viewContainerRef: ViewContainerRef,
  ) {
    super(state, dialogRef, dialog);
  }

  ngOnInit(): void {
    this.state.current$.subscribe(next => {
      this.allPhotos = [];
      next.current.albums.map(album => {
        this.allPhotos.push(...album.photos);
        this.allPhotos.sort((a, b) => b.id - a.id);
      });
    });

    this.profileState.current$.pipe(
      takeUntil(this.ngOnDestroy$))
      .subscribe((s) => {
        this.canAddAlbum = s.current.isOwnProfile;
      });
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.dialogRef.afterClosed().subscribe(() => {
      EvodeskBaseAlbumDialogComponent.dialogScrollPosition = 0;
    });
  }

  openAlbumDialog(album: Album) {
    const config: MatDialogConfig = {
        ...defaultDialogConfig,
        viewContainerRef: this.viewContainerRef,
        data: album,
    };

    this.openChildDialog(config, EvodeskViewAlbumComponent);
  }

  createAlbum() {
    const config: MatDialogConfig = {
        ...confirmDialogConfig,
        viewContainerRef: this.viewContainerRef,
        backdropClass: 'transparent-backdrop',
    };

    this.openChildDialog(config, EvodeskUploadPhotosComponent);
  }

  openViewPhoto(index: number) {
    const config: MatDialogConfig = {
        ...dialogConfig,
        viewContainerRef: this.viewContainerRef,
        data: { photos: this.allPhotos, index: index },
    };

    this.openChildDialog(config, EvodeskViewPhotoComponent);
  }

  getPhotoPreview(photo: Photo): string {
    return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/albums/preview/${photo.id}`;
  }
}
