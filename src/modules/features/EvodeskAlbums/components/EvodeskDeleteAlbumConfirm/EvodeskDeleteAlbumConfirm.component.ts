import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'evodesk-delete-album-confirm',
  templateUrl: './EvodeskDeleteAlbumConfirm.component.pug',
  styleUrls: [
    './EvodeskDeleteAlbumConfirm.component.scss',
  ],
})
export class EvodeskDeleteAlbumConfirmComponent {

  constructor(public dialogRef: MatDialogRef<EvodeskDeleteAlbumConfirmComponent>) {
  }

  confirm() {
    this.dialogRef.close(true);
  }

}
