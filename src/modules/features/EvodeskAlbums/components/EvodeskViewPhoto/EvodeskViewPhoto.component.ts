import {AfterViewInit, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogConfig, MatDialogRef} from '@angular/material';
import {Photo} from '../../../../app/EvodeskRESTApi/models/album/Album.model';
import {environment} from '../../../../../environments/environment.config';
import {EvodeskAlbumsStateService} from '../../services/EvodeskAlbumsState.service';

interface PhotosInfo {
  photos: Array<Photo>;
  albumName?: string;
  index: number;
}

export const dialogConfig: MatDialogConfig = {
  width: '100%',
  maxWidth: '768px',
  disableClose: true,
  autoFocus: false,
  panelClass: '__evodesk-albums-dialog-photo-view',
  hasBackdrop: true,
  backdropClass: 'transparent-backdrop',
};

@Component({
  selector: 'evodesk-profile-view-photo',
  templateUrl: './EvodeskViewPhoto.component.pug',
  styleUrls: [
    './EvodeskViewPhoto.component.scss',
  ],
})
export class EvodeskViewPhotoComponent implements AfterViewInit {
  index: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public photosInfo: PhotosInfo,
    public state: EvodeskAlbumsStateService,
    public dialogRef: MatDialogRef<EvodeskViewPhotoComponent>,
  ) {
    this.index = this.photosInfo.index;
  }

  ngAfterViewInit() {
    this.dialogRef.backdropClick().subscribe(result => {
      this.state.handleBackdrop();
    });
    this.state.closeAllDialogs.subscribe(next => {
      this.dialogRef.close();
    });
  }

  goToPrev() {
    if (this.index > 0) {
      this.index--;
    }
  }

  getPhoto(): string {
    return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/albums/photo/${this.photosInfo.photos[this.index].id}`;
  }

  goToNext() {
    if (this.index < this.photosInfo.photos.length - 1) {
      this.index++;
    }
  }
}
