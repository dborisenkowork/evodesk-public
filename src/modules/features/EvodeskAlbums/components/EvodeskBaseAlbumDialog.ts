import {AfterViewInit, Component, ElementRef, OnDestroy, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';

import {EvodeskAlbumsStateService} from '../services/EvodeskAlbumsState.service';
import {Subject} from 'rxjs/internal/Subject';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'evodesk-profile-edit-photo-album',
    template: `
      <div class='sticky-wrapper' #sticky='' (click)="state.handleBackdrop()">
        <div class='dialog' (click)="$event.stopPropagation()"></div>
      </div>`,
})
export class EvodeskBaseAlbumDialogComponent implements AfterViewInit, OnDestroy {
    public static dialogScrollPosition: number;

    @ViewChild('sticky') sticky: ElementRef;

    public scrollable: boolean = true;

    private maxScrollValue: number;
    private isClosed: boolean = false;
    protected ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        public state: EvodeskAlbumsStateService,
        public dialogRef: MatDialogRef<EvodeskBaseAlbumDialogComponent>,
        public dialog: MatDialog,
    ) {
        this.maxScrollValue = document.documentElement.clientHeight * 0.3;
    }

    ngAfterViewInit() {
        this.restoreScroll();

        this.dialogRef.backdropClick().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.state.handleBackdrop();
        });

        this.dialogRef.beforeClose().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.calculateScrollTop();
            this.state.childDialogClosed.next(true);
        });

        this.state.childDialogClosed.pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.restoreScroll();
        });

        this.state.closeAllDialogs.pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.isClosed = true;
            this.dialogRef.close();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    restoreScroll() {
        if (this.scrollable && this.sticky.nativeElement.scrollTop <= this.maxScrollValue) {
            this.sticky.nativeElement.scrollTop = EvodeskBaseAlbumDialogComponent.dialogScrollPosition;
        }
    }

    calculateScrollTop() {
        if (this.scrollable) {
            EvodeskBaseAlbumDialogComponent.dialogScrollPosition = Math.floor(Math.min(this.maxScrollValue,
                this.sticky.nativeElement.scrollTop));
        }
    }

    openChildDialog(config: MatDialogConfig, component: any) {
        if (this.dialog) {
            this.calculateScrollTop();

            const savedWidth: string = this.dialogRef._containerInstance._config.width;

            this.dialog.open(component, config).beforeClose().subscribe(() => {
                if (!this.isClosed) {
                    this.dialogRef.updateSize(savedWidth);
                    this.dialogRef._containerInstance._state = 'enter';
                }
            });

            this.dialogRef._containerInstance._state = 'void';
            this.dialogRef.updateSize('0');
        }
    }
}
