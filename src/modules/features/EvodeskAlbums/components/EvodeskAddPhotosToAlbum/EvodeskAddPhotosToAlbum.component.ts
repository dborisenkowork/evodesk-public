import {AfterViewInit, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

import {takeUntil} from 'rxjs/operators';

import {environment} from '../../../../../environments/environment.config';

import {AlbumsRESTService} from '../../../../app/EvodeskRESTApi/services/AlbumsREST.service';
import {EvodeskAlbumsStateService} from '../../services/EvodeskAlbumsState.service';
import {EvodeskProfileStateService} from '../../../EvodeskProfile/services/EvodeskProfileState.service';

import {EvodeskBaseAlbumDialogComponent} from '../EvodeskBaseAlbumDialog';

import {Album, Photo} from '../../../../app/EvodeskRESTApi/models/album/Album.model';

@Component({
    selector: 'evodesk-profile-add-photos-to-album',
    templateUrl: './EvodeskAddPhotosToAlbum.component.pug',
    styleUrls: [
        './EvodeskAddPhotosToAlbum.component.scss',
    ],
})
export class EvodeskAddPhotosToAlbumComponent extends EvodeskBaseAlbumDialogComponent implements OnInit, AfterViewInit, OnDestroy {

    filesToLoad: Array<any> = [];

    constructor(
        @Inject(MAT_DIALOG_DATA) public data,
        private rest: AlbumsRESTService,
        public state: EvodeskAlbumsStateService,
        private profileState: EvodeskProfileStateService,
        public dialogRef: MatDialogRef<EvodeskAddPhotosToAlbumComponent>,
    ) {
        super(state, dialogRef, undefined);
    }

    ngOnInit() {
        for (let i: number = 0; i < this.data.files.length; i++) {
            this.filesToLoad.push(this.data.files[i]);
            this.filesToLoad[i].progressBar = 0;
            this.filesToLoad[i].description = '';
            this.rest.addPhoto({
                file: this.filesToLoad[i],
                description: '',
            }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(next => {
                this.filesToLoad[i].photo = next;
                this.filesToLoad[i].progressBar = 100;
            });
        }
    }

    moveListToAlbum() {
        const photos: Array<any> = [];

        for (let i: number = 0; i < this.filesToLoad.length; i++) {
            photos.push({
                id: this.filesToLoad[i].photo.id,
                description: this.filesToLoad[i].description,
            });
        }

        if (!! (this.data.album)) {
            this.rest.moveListToAlbum({
                album_id: this.data.album.id,
                photos: photos,
            }).pipe(takeUntil(this.ngOnDestroy$)).subscribe((next: Album) => {
                this.state.addPhotosInAlbum(next, photos);
                this.dialogRef.close(true);
            });
        } else {
            this.rest.addAlbum(this.profileState.snapshot.profile.id, {name: 'Новый альбом'}).pipe(takeUntil(this.ngOnDestroy$)).subscribe(next => {
                this.rest.moveListToAlbum({
                    album_id: next.id,
                    photos: photos,
                }).pipe(takeUntil(this.ngOnDestroy$)).subscribe((mvnext: Album) => {
                    this.state.addPhotosInAlbum(mvnext, photos);
                    this.dialogRef.close(true);
                });
                this.state.addAlbum(next);
            });
        }
    }

    deletePhoto(photo: Photo) {
        const deleteIndex: number = this.filesToLoad.findIndex((value) => value.photo.id === photo.id);

        if (deleteIndex !== -1) {
            this.filesToLoad.splice(deleteIndex, 1);
        }
    }

    getPhotoPreview(photo: Photo): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/albums/preview/${photo.id}`;
    }
}

