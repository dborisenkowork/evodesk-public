import {Component, Inject, ViewContainerRef} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';

import {EvodeskAddPhotosToAlbumComponent} from '../EvodeskAddPhotosToAlbum/EvodeskAddPhotosToAlbum.component';

import {defaultDialogConfig} from '../../EvodeskDialogConfig';

import {Album} from '../../../../app/EvodeskRESTApi/models/album/Album.model';

@Component({
  selector: 'evodesk-upload-photos',
  templateUrl: './EvodeskUploadPhotos.component.pug',
  styleUrls: [
    './EvodeskUploadPhotos.component.scss',
  ],
})
export class EvodeskUploadPhotosComponent {

  constructor(
      @Inject(MAT_DIALOG_DATA) public album: Album,
      public dialog: MatDialog,
      public dialogRef: MatDialogRef<EvodeskUploadPhotosComponent>,
      public viewContainerRef: ViewContainerRef,
  ) {}

  selectPhotos(fileList: FileList) {
    const config: MatDialogConfig = {
        ...defaultDialogConfig,
        data: { album: this.album, files: fileList },
        viewContainerRef: this.viewContainerRef,
    };

    this.dialog.open(EvodeskAddPhotosToAlbumComponent, config).beforeClosed().subscribe(() => {
      this.dialogRef.close(true);
    });

    this.dialogRef._containerInstance._state = 'void';
    this.dialogRef.updateSize('0');
  }
}
