import {AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';

import {confirmDialogConfig, defaultDialogConfig} from '../../EvodeskDialogConfig';

import {environment} from '../../../../../environments/environment.config';

import {Album, Photo} from '../../../../app/EvodeskRESTApi/models/album/Album.model';

import {EvodeskEditAlbumComponent} from '../EvodeskEditAlbum/EvodeskEditAlbum.component';
import {EvodeskDeleteAlbumConfirmComponent} from '../EvodeskDeleteAlbumConfirm/EvodeskDeleteAlbumConfirm.component';
import {EvodeskUploadPhotosComponent} from '../EvodeskUploadPhotos/EvodeskUploadPhotos.component';
import {dialogConfig, EvodeskViewPhotoComponent} from '../EvodeskViewPhoto/EvodeskViewPhoto.component';
import {EvodeskBaseAlbumDialogComponent} from '../EvodeskBaseAlbumDialog';

import {AlbumsState, EvodeskAlbumsStateService} from '../../services/EvodeskAlbumsState.service';
import {AlbumsRESTService} from '../../../../app/EvodeskRESTApi/services/AlbumsREST.service';
import {EvodeskProfileStateService} from '../../../EvodeskProfile/services/EvodeskProfileState.service';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'evodesk-profile-view-photos-album',
    templateUrl: './EvodeskViewAlbum.component.pug',
    styleUrls: [
        './EvodeskViewAlbum.component.scss',
    ],
})
export class EvodeskViewAlbumComponent extends EvodeskBaseAlbumDialogComponent implements AfterViewInit, OnInit, OnDestroy {

    albumInfo: Album;
    canAdmin: boolean;

    constructor(
        @Inject(MAT_DIALOG_DATA) public album: Album,
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<EvodeskViewAlbumComponent>,
        public state: EvodeskAlbumsStateService,
        private rest: AlbumsRESTService,
        private profileState: EvodeskProfileStateService,
        public viewContainerRef: ViewContainerRef,
    ) {
        super(state, dialogRef, dialog);
        this.albumInfo = this.album;
    }

    ngOnInit(): void {
        this.profileState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.canAdmin = s.current.isOwnProfile;
        });
    }

    openEditAlbum() {
        const config: MatDialogConfig = {
            ...defaultDialogConfig,
            viewContainerRef: this.viewContainerRef,
            data: this.album,
        };
        this.openChildDialog(config, EvodeskEditAlbumComponent);
    }

    openViewPhoto(index: number) {
        const config: MatDialogConfig = {
            ...dialogConfig,
            viewContainerRef: this.viewContainerRef,
            data: { photos: this.album.photos, albumName: this.album.name, index: index },
        };
        this.openChildDialog(config, EvodeskViewPhotoComponent);
    }

    requestDeleteConfirm() {
        const config: MatDialogConfig = {
            ...confirmDialogConfig,
            viewContainerRef: this.viewContainerRef,
        };
        this.dialog.open(EvodeskDeleteAlbumConfirmComponent, config).afterClosed().subscribe(result => {
            if (result) {
                this.deleteAlbum();
            }
        });
    }

    uploadPhotos() {
        const config: MatDialogConfig = {
            ...confirmDialogConfig,
            data: this.album,
            viewContainerRef: this.viewContainerRef,
            backdropClass: 'transparent-backdrop',
        };
        this.openChildDialog(config, EvodeskUploadPhotosComponent);
    }

    getPhotoPreview(photo: Photo): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/albums/preview/${photo.id}`;
    }

    private deleteAlbum() {
        this.rest.deleteAlbum(this.album.id).subscribe(() => {
            this.state.setState('albumDeleted', (orig: AlbumsState) => {
                return {
                    ...orig,
                    albums: orig.albums.filter(album => album.id !== this.album.id),
                };
            });
            this.dialogRef.close();
        });
    }
}
