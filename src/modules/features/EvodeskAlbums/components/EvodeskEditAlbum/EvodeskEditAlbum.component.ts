import {AfterViewInit, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl} from '@angular/forms';

import {Observable, forkJoin} from 'rxjs';
import {tap} from 'rxjs/operators';

import {environment} from '../../../../../environments/environment.config';

import {Album, Photo} from '../../../../app/EvodeskRESTApi/models/album/Album.model';

import {AlbumsState, EvodeskAlbumsStateService} from '../../services/EvodeskAlbumsState.service';

import {AlbumsRESTService} from '../../../../app/EvodeskRESTApi/services/AlbumsREST.service';
import {EvodeskProfileStateService} from '../../../EvodeskProfile/services/EvodeskProfileState.service';

import {EvodeskBaseAlbumDialogComponent} from '../EvodeskBaseAlbumDialog';

@Component({
    selector: 'evodesk-profile-edit-photo-album',
    templateUrl: './EvodeskEditAlbum.component.pug',
    styleUrls: [
        './EvodeskEditAlbum.component.scss',
    ],
})
export class EvodeskEditAlbumComponent extends EvodeskBaseAlbumDialogComponent implements AfterViewInit {

    photos: Array<Photo> = [];
    selectedPhotos: Array<Photo> = [];
    photosToDelete: Array<Photo> = [];

    albumName: FormControl = new FormControl(this.album.name);

    constructor(@Inject(MAT_DIALOG_DATA) public album: Album,
                public dialogRef: MatDialogRef<EvodeskEditAlbumComponent>,
                public state: EvodeskAlbumsStateService,
                private rest: AlbumsRESTService,
                private profileState: EvodeskProfileStateService,
    ) {
        super(state, dialogRef, undefined);
        this.photos = album.photos;
    }

    togglePhoto(photo: Photo) {
        const selectedIndex: number = this.selectedPhotos.findIndex((value) => value.id === photo.id);

        if (selectedIndex !== -1) {
            this.selectedPhotos.splice(selectedIndex, 1);
        } else {
            this.selectedPhotos.push(photo);
        }
    }

    selectAll() {
        this.selectedPhotos = [];
        this.selectedPhotos.push(...this.photos);
    }

    isSelected(photo: Photo): boolean {
        return this.selectedPhotos.find((value) => value.id === photo.id) != null;
    }

    deletePhotos() {
        this.photos = this.photos.filter((value) => {
            if (this.selectedPhotos.includes(value)) {
                this.photosToDelete.push(value);
                return false;
            }
            return true;
        });
        this.selectedPhotos = [];
    }

    saveChanges() {
        const observables: Array<Observable<any>> = [];
        const photos: Array<number> = [];
        if (this.photosToDelete.length > 0) {
            this.photosToDelete.forEach((value) => {
                photos.push(value.id);
            });

            observables.push(this.rest.deletePhotosList(photos).pipe(tap(() => {
                this.state.setState('albumUpdated', (orig: AlbumsState) => {
                    return {
                        ...orig,
                        albums: orig.albums.map((album: Album) => {
                            if (album.id === this.album.id) {
                                album.photos = this.photos;
                            }
                            return album;
                        }),
                    };
                });
            })));
        }

        observables.push(this.rest.updateAlbum(this.profileState.snapshot.profile.id, this.album.id, {
            name: this.albumName.value,
            description: '',
        }).pipe(tap(next => {
            this.state.setState('albumUpdated', (orig: AlbumsState) => {
                return {
                    ...orig,
                    albums: orig.albums.map(album => {
                        if (album.id === next.id) {
                            album.name = next.name;
                        }
                        return album;
                    }),
                };
            });
        })));

        forkJoin(observables).subscribe(undefined, undefined, () => {
            this.dialogRef.close();
        });
    }

    getPhotoPreview(photo: Photo): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/albums/preview/${photo.id}`;
    }
}
