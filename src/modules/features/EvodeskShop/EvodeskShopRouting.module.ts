import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskShopRouting} from './configs/EvodeskShopRouting.module';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskShopRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskShopRoutingModule
{}
