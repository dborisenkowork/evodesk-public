import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskShopRoutingModule} from './EvodeskShopRouting.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';
import {CatalogRouteComponent} from './routes/CatalogRoute/CatalogRoute.component';
import {OrdersRouteComponent} from './routes/OrdersRoute/OrdersRoute.component';
import {ProductRouteComponent} from './routes/ProductRoute/ProductRoute.component';
import {FavoritesRouteComponent} from './routes/FavoritesRoute/FavoritesRoute.component';
import {ServiceRouteComponent} from './routes/ServiceRoute/ServiceRoute.component';

import {EvodeskShopStarsComponent} from './components-util/EvodeskShopStars/EvodeskShopStars.component';

import {EvodeskShopNavigationDesktopComponent} from './components/EvodeskShopNavigationDesktop/EvodeskShopNavigationDesktop.component';
import {EvodeskShopNavigationDesktopContainerComponent} from './components/EvodeskShopNavigationDesktop/EvodeskShopNavigationDesktopContainer.component';
import {EvodeskShopNavigationMobileComponent} from './components/EvodeskShopNavigationMobile/EvodeskShopNavigationMobile.component';
import {EvodeskShopNavigationMobileContainerComponent} from './components/EvodeskShopNavigationMobile/EvodeskShopNavigationMobileContainer.component';
import {EvodeskShopProductCardComponent} from './components/EvodeskShopProductCard/EvodeskShopProductCard.component';
import {EvodeskShopProductCardContainerComponent} from './components/EvodeskShopProductCard/EvodeskShopProductCardContainer.component';
import {EvodeskShopCatalogFeedComponent} from './components/EvodeskShopCatalogFeed/EvodeskShopCatalogFeed.component';
import {EvodeskShopCatalogFeedContainerComponent} from './components/EvodeskShopCatalogFeed/EvodeskShopCatalogFeedContainer.component';
import {EvodeskShopProductViewComponent} from './components/EvodeskShopProductView/EvodeskShopProductView.component';
import {EvodeskShopProductViewContainerComponent} from './components/EvodeskShopProductView/EvodeskShopProductViewContainer.component';
import {EvodeskShopReviewFormComponent} from './components/EvodeskShopReviewForm/EvodeskShopReviewForm.component';
import {EvodeskShopReviewFormContainerComponent} from './components/EvodeskShopReviewForm/EvodeskShopReviewFormContainer.component';
import {EvodeskShopReviewContainerComponent} from './components/EvodeskShopReview/EvodeskShopReviewContainer.component';
import {EvodeskShopReviewComponent} from './components/EvodeskShopReview/EvodeskShopReview.component';
import {EvodeskShopOrderCardComponent} from './components/EvodeskShopOrderCard/EvodeskShopOrderCard.component';
import {EvodeskShopOrderCardContainerComponent} from './components/EvodeskShopOrderCard/EvodeskShopOrderCardContainer.component';
import {EvodeskShopOrdersFeedComponent} from './components/EvodeskShopOrdersFeed/EvodeskShopOrdersFeed.component';
import {EvodeskShopOrdersFeedContainerComponent} from './components/EvodeskShopOrdersFeed/EvodeskShopOrdersFeedContainer.component';
import {EvodeskShopNavigationModalComponent} from './components/EvodeskShopNavigationModal/EvodeskShopNavigationModal.component';
import {EvodeskShopNavigationModalContainerComponent} from './components/EvodeskShopNavigationModal/EvodeskShopNavigationModalContainer.component';
import {EvodeskShopFavoriteCardComponent} from './components/EvodeskShopFavoriteCard/EvodeskShopFavoriteCard.component';
import {EvodeskShopFavoriteCardContainerComponent} from './components/EvodeskShopFavoriteCard/EvodeskShopFavoriteCardContainer.component';
import {EvodeskShopFavoritesFeedComponent} from './components/EvodeskShopFavoritesFeed/EvodeskShopFavoritesFeed.component';
import {EvodeskShopFavoritesFeedContainerComponent} from './components/EvodeskShopFavoritesFeed/EvodeskShopFavoritesFeedContainer.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskShopRoutingModule,
    ],
    declarations: [
        IndexRouteComponent,
        CatalogRouteComponent,
        OrdersRouteComponent,
        ProductRouteComponent,
        FavoritesRouteComponent,
        ServiceRouteComponent,

        EvodeskShopStarsComponent,

        EvodeskShopNavigationDesktopComponent,
        EvodeskShopNavigationDesktopContainerComponent,
        EvodeskShopNavigationMobileComponent,
        EvodeskShopNavigationMobileContainerComponent,
        EvodeskShopProductCardComponent,
        EvodeskShopProductCardContainerComponent,
        EvodeskShopCatalogFeedComponent,
        EvodeskShopCatalogFeedContainerComponent,
        EvodeskShopProductViewComponent,
        EvodeskShopProductViewContainerComponent,
        EvodeskShopReviewFormComponent,
        EvodeskShopReviewFormContainerComponent,
        EvodeskShopReviewComponent,
        EvodeskShopReviewContainerComponent,
        EvodeskShopOrderCardComponent,
        EvodeskShopOrderCardContainerComponent,
        EvodeskShopOrdersFeedComponent,
        EvodeskShopOrdersFeedContainerComponent,
        EvodeskShopNavigationModalComponent,
        EvodeskShopNavigationModalContainerComponent,
        EvodeskShopFavoriteCardComponent,
        EvodeskShopFavoriteCardContainerComponent,
        EvodeskShopFavoritesFeedComponent,
        EvodeskShopFavoritesFeedContainerComponent,
    ],
    entryComponents: [
        EvodeskShopProductViewContainerComponent,
        EvodeskShopNavigationModalContainerComponent,
    ],
})
export class EvodeskShopModule
{}
