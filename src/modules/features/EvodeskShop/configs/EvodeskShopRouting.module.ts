import {Routes} from '@angular/router';

import {HasAuthTokenGuard} from '../../EvodeskAuth/guards/HasAuthToken.guard';
import {CurrentUserGuard} from '../../EvodeskAuth/guards/CurrentUser.guard';
import {EvodeskSetCommonHeaderGuard} from '../../EvodeskHeader/guards/EvodeskSetCommonHeader.guard';

import {IndexRouteComponent} from '../routes/IndexRoute/IndexRoute.component';
import {CatalogRouteComponent} from '../routes/CatalogRoute/CatalogRoute.component';
import {OrdersRouteComponent} from '../routes/OrdersRoute/OrdersRoute.component';
import {ProductRouteComponent} from '../routes/ProductRoute/ProductRoute.component';
import {FavoritesRouteComponent} from '../routes/FavoritesRoute/FavoritesRoute.component';
import {ServiceRouteComponent} from '../routes/ServiceRoute/ServiceRoute.component';

export const EvodeskShopRouting: Routes = [
    {
        path: '',
        pathMatch: 'prefix',
        component: IndexRouteComponent,
        canActivate: [
            HasAuthTokenGuard,
            CurrentUserGuard,
            EvodeskSetCommonHeaderGuard,
        ],
        children: [
            {
                path: '',
                pathMatch: 'full',
                component: CatalogRouteComponent,
            },
            {
                path: 'orders',
                pathMatch: 'full',
                component: OrdersRouteComponent,
            },
            {
                path: 'favorites',
                pathMatch: 'full',
                component: FavoritesRouteComponent,
            },
            {
                path: 'service/:serviceId/:fileType',
                pathMatch: 'full',
                component: ServiceRouteComponent,
            },
            {
                path: ':category',
                pathMatch: 'full',
                component: CatalogRouteComponent,
            },
            {
                path: ':category/:product',
                pathMatch: 'full',
                component: ProductRouteComponent,
            },
        ],
    },
];
