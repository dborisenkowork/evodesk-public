export const evodeskShopLayoutMobile: { to: number } = { to: 1131 };
export const evodeskShopLayoutDesktop: { from: number; minHeight: number } = { from: 1132, minHeight: 750 };

export const evodeskShopDefaultLimit: number = 21;
export const evodeskShopMaxReviewLength: number = 1000;
