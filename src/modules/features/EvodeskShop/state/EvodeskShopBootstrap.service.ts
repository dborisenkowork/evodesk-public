import {Injectable} from '@angular/core';

import {forkJoin, Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskAppLoading, EvodeskAppLoadingStatusService} from '../../../app/EvodeskApp/services/EvodeskAppLoadingStatus.service';
import {ShopRESTService} from '../../../app/EvodeskRESTApi/services/ShopREST.service';
import {EvodeskShopStateService, ShopAction, ShopBootstrapQuery} from './EvodeskShopState.service';

@Injectable()
export class EvodeskShopBootstrapService
{
    constructor(
        private appLoading: EvodeskAppLoadingStatusService,
        private shopRESTService: ShopRESTService,
        private shopState: EvodeskShopStateService,
    ) {}

    bootstrap(): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        const appLoading: EvodeskAppLoading = this.appLoading.addLoading();

        const bootstrapQuery: ShopBootstrapQuery = {
            categories: [],
            orders: [],
        };

        return Observable.create((done) => {
            const queries: Array<Observable<any>> = [];

            queries.push(Observable.create(q => {
                this.shopRESTService.getCategories().pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.categories = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.shopRESTService.getOrders().pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.orders = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            forkJoin(queries).pipe(takeUntil(unsubscribe$)).subscribe(
                () => {
                    this.shopState.dispatch({
                        type: ShopAction.Bootstrap,
                        payload: {
                            bootstrap: bootstrapQuery,
                        },
                    });

                    appLoading.done();

                    done.next(undefined);
                    done.complete();
                },
                (queryError) => {
                    appLoading.done();

                    done.error(queryError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
