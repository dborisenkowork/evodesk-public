import {Injectable} from '@angular/core';

import * as R from 'ramda';

import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {evodeskShopDefaultLimit} from '../configs/EvodeskShop.constants';

import {ShopCategoryModel} from '../../../app/EvodeskRESTApi/models/ShopCategory.model';
import {ShopProductId, ShopProductModel} from '../../../app/EvodeskRESTApi/models/ShopProduct.model';
import {ShopOrderModel} from '../../../app/EvodeskRESTApi/models/ShopOrder.model';
import {ShopProductReviewModel} from '../../../app/EvodeskRESTApi/models/ShopProductReview.model';
import {ShopFavoriteModel} from '../../../app/EvodeskRESTApi/models/ShopFavorite.model';
import {CurrentUserService} from '../../EvodeskAuth/services/CurrentUser.service';

export interface ShopState {
    ready: boolean;
    route: ShopRoute;
    layoutVariant: ShopLayoutVariant;
    products: Array<ShopProductModel>;
    categories: Array<ShopCategoryModel>;
    orderBy: ShopCatalogViewOrderBy;
    productInFeed: Array<ShopProductId>;
    page: number;
    limit: number;
    hasMoreToLoad: boolean;
    search: string | undefined;
    orderDirection: ShopCatalogViewOrderDirection;
    currentCategory: undefined | ShopCategoryModel;
    currentProduct: undefined | ShopProductModel;
    orders: Array<ShopOrderModel>;
    orderedProducts: Array<ShopProductId>;
    reviews: Array<ShopProductReviewModel>;
    favorites: Array<ShopFavoriteModel>;
}

export type ShopPreviousState = undefined | ShopState;
export type ShopNextState = ShopState;
export type ShopStates = { current: ShopNextState; previous: ShopPreviousState };

export enum ShopAction {
    Reset = 'Reset',
    Bootstrap = 'Bootstrap',
    SetLayoutVariant = 'SetLayoutVariant',
    SetOrder = 'SetOrder',
    SetCurrentCategory = 'SetCurrentCategory',
    SetCurrentProduct = 'SetCurrentProduct',
    PushCategories = 'PushCategories',
    PushProducts = 'PushProducts',
    SetPage = 'SetPage',
    SetLimit = 'SetLimit',
    SetCanLoadMore = 'SetCanLoadMore',
    DisplayProductsToFeed = 'DisplayProductsToFeed',
    UpdateProduct = 'UpdateProduct',
    AddOrder = 'AddOrder',
    SetProductAsOrdered = 'SetProductAsOrdered',
    PushReviews = 'PushReviews',
    ClearReviews = 'ClearReviews',
    SetRoute = 'SetRoute',
    SetOrders = 'SetOrders',
    ClearFavorites = 'ClearFavorites',
    PushFavorites = 'PushFavorites',
    AddFavorite = 'AddFavorite',
    DeleteFavorite = 'DeleteFavorite',
}

type Actions =
      { type: ShopAction.Reset }
    | { type: ShopAction.Bootstrap, payload: { bootstrap: ShopBootstrapQuery } }
    | { type: ShopAction.SetLayoutVariant, payload: { layoutVariant: ShopLayoutVariant } }
    | { type: ShopAction.SetOrder, payload: { orderBy: ShopCatalogViewOrderBy; orderDirection: ShopCatalogViewOrderDirection; } }
    | { type: ShopAction.SetCurrentCategory, payload: { category: ShopCategoryModel; } }
    | { type: ShopAction.SetCurrentProduct, payload: { product: ShopProductModel; } }
    | { type: ShopAction.PushCategories, payload: { categories: Array<ShopCategoryModel>; } }
    | { type: ShopAction.PushProducts, payload: { products: Array<ShopProductModel>; } }
    | { type: ShopAction.SetPage, payload: { page: number; } }
    | { type: ShopAction.SetLimit, payload: { limit: number; } }
    | { type: ShopAction.SetCanLoadMore, payload: { canLoadMore: boolean; } }
    | { type: ShopAction.DisplayProductsToFeed, payload: { productIds: Array<ShopProductId>; } }
    | { type: ShopAction.UpdateProduct, payload: { product: ShopProductModel; } }
    | { type: ShopAction.AddOrder, payload: { order: ShopOrderModel; } }
    | { type: ShopAction.SetProductAsOrdered, payload: { productId: ShopProductId; } }
    | { type: ShopAction.PushReviews, payload: { reviews: Array<ShopProductReviewModel>; } }
    | { type: ShopAction.ClearReviews }
    | { type: ShopAction.SetRoute, payload: { route: ShopRoute } }
    | { type: ShopAction.SetOrders, payload: { orders: Array<ShopOrderModel> } }
    | { type: ShopAction.ClearFavorites }
    | { type: ShopAction.PushFavorites, payload: { favorites: Array<ShopFavoriteModel> } }
    | { type: ShopAction.AddFavorite, payload: { product: ShopProductModel } }
    | { type: ShopAction.DeleteFavorite, payload: { productId: ShopProductId } }
;

export interface ShopBootstrapQuery {
    categories: Array<ShopCategoryModel>;
    orders: Array<ShopOrderModel>;
}

export enum ShopCatalogViewOrderBy {
    Popularity = 'popularity',
    CreatedAt = 'createdAt',
    Rating = 'rating',
    Feedback = 'feedback',
}

export enum ShopCatalogViewOrderDirection {
    Asc = 'asc',
    Desc = 'desc',
}

export enum ShopLayoutVariant {
    Mobile,
    Desktop,
}

export enum ShopRoute {
    None = 'none',
    Catalog = 'catalog',
    Orders = 'orders',
    Favorites = 'favorites',
}

export const shopViewOrderToBackendMap: Array<{ from: ShopCatalogViewOrderBy, to: string }> = [
    { from: ShopCatalogViewOrderBy.CreatedAt, to: 'created_at' },
    { from: ShopCatalogViewOrderBy.Popularity, to: 'orders' },
    { from: ShopCatalogViewOrderBy.Rating, to: 'stars' },
    { from: ShopCatalogViewOrderBy.Feedback, to: 'reviews' },
];

function initialState(): ShopState {
    return {
        ready: false,
        route: ShopRoute.None,
        layoutVariant: ShopLayoutVariant.Mobile,
        products: [],
        categories: [],
        productInFeed: [],
        orderBy: ShopCatalogViewOrderBy.CreatedAt,
        orderDirection: ShopCatalogViewOrderDirection.Desc,
        currentProduct: undefined,
        currentCategory: undefined,
        page: 0,
        limit: evodeskShopDefaultLimit,
        search: undefined,
        hasMoreToLoad: false,
        orders: [],
        orderedProducts: [],
        reviews: [],
        favorites: [],
    };
}

@Injectable()
export class EvodeskShopStateService {
    private _current$: BehaviorSubject<ShopStates> = new BehaviorSubject<ShopStates>({
        previous: undefined,
        current: initialState(),
    });

    private _side$: Subject<Actions> = new Subject<Actions>();

    constructor(
        private currentUser: CurrentUserService,
    ) {}

    get current$(): Observable<ShopStates> {
        return this._current$.asObservable();
    }

    get side$(): Observable<Actions> {
        return this._side$.asObservable();
    }

    get snapshot(): ShopState {
        return this._current$.getValue().current;
    }

    reset(): void {
        this._current$.next({
            previous: undefined,
            current: initialState(),
        });
    }

    dispatch(action: Actions): void {
        this._side$.next(action);

        switch (action.type) {
            case ShopAction.Reset: {
                this.reset();

                break;
            }

            case ShopAction.Bootstrap: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        ...action.payload.bootstrap,
                        orderedProducts: action.payload.bootstrap.orders.map(o => o.service.id),
                        ready: true,
                    };
                });

                break;
            }

            case ShopAction.SetLayoutVariant: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        layoutVariant: action.payload.layoutVariant,
                    };
                });

                break;
            }

            case ShopAction.SetOrder: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        orderBy: action.payload.orderBy,
                        orderDirection: action.payload.orderDirection,
                        page: 0,
                        hasMoreToLoad: false,
                        products: [],
                        productInFeed: [],
                    };
                });

                break;
            }

            case ShopAction.SetCurrentCategory: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        currentCategory: action.payload.category,
                        page: 0,
                        hasMoreToLoad: false,
                        products: [],
                        productInFeed: [],
                    };
                });

                break;
            }

            case ShopAction.SetCurrentProduct: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        currentProduct: action.payload.product,
                    };
                });

                break;
            }

            case ShopAction.PushCategories: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        categories: R.uniqBy((input: ShopCategoryModel) => input.id, [...orig.categories, ...action.payload.categories]),
                    };
                });

                break;
            }

            case ShopAction.PushProducts: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        products: R.uniqBy((input: ShopProductModel) => input.id, [...orig.products, ...action.payload.products]),
                    };
                });

                break;
            }

            case ShopAction.SetPage: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        page: action.payload.page,
                    };
                });

                break;
            }

            case ShopAction.SetLimit: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        limit: action.payload.limit,
                    };
                });

                break;
            }

            case ShopAction.SetCanLoadMore: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        hasMoreToLoad: action.payload.canLoadMore,
                    };
                });

                break;
            }

            case ShopAction.DisplayProductsToFeed: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        productInFeed: R.uniqBy((id) => id, [...orig.productInFeed, ...action.payload.productIds]),
                    };
                });

                break;
            }

            case ShopAction.UpdateProduct: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        products: orig.products.map((p) => {
                            if (p.id === action.payload.product.id) {
                                return action.payload.product;
                            } else {
                                return p;
                            }
                        }),
                    };
                });

                break;
            }

            case ShopAction.AddOrder: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        orders: [...orig.orders, action.payload.order],
                        orderedProducts: R.uniqBy(id => id, [...orig.orderedProducts, action.payload.order.service.id]),
                    };
                });

                break;
            }

            case ShopAction.SetProductAsOrdered: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        orderedProducts: R.uniqBy(id => id, [...orig.orderedProducts, action.payload.productId]),
                    };
                });

                break;
            }

            case ShopAction.ClearReviews: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        reviews: [],
                    };
                });

                break;
            }

            case ShopAction.PushReviews: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        reviews: R.uniqBy(r => r.id, [...orig.reviews, ...action.payload.reviews.filter(r => {
                            return !! r.service && ! (r.stars === null && r.review === null) && r.review_at !== null;
                        })]),
                    };
                });

                break;
            }

            case ShopAction.SetRoute: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        route: action.payload.route,
                    };
                });

                break;
            }

            case ShopAction.SetOrders: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        orders: action.payload.orders,
                    };
                });

                break;
            }

            case ShopAction.ClearFavorites: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        favorites: [],
                    };
                });

                break;
            }

            case ShopAction.PushFavorites: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        favorites: [...orig.favorites, ...action.payload.favorites],
                    };
                });

                break;
            }

            case ShopAction.AddFavorite: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        favorites: [...orig.favorites, {
                            id: action.payload.product.id,
                            service: action.payload.product,
                            user: this.currentUser.impersonatedAs,
                        }],
                        products: orig.products.map(p => {
                            if (p.id === action.payload.product.id) {
                                return {
                                    ...p,
                                    isFavorite: true,
                                };
                            } else {
                                return p;
                            }
                        }),
                        orders: orig.orders.map(o => {
                            if (o.service.id === action.payload.product.id) {
                                return {
                                    ...o,
                                    service: {
                                        ...o.service,
                                        isFavorite: true,
                                    },
                                };
                            } else {
                                return o;
                            }
                        }),
                    };
                });

                break;
            }

            case ShopAction.DeleteFavorite: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        favorites: orig.favorites.filter(f => f.service.id !== action.payload.productId),
                        products: orig.products.map(p => {
                            if (p.id === action.payload.productId) {
                                return {
                                    ...p,
                                    isFavorite: false,
                                };
                            } else {
                                return p;
                            }
                        }),
                        orders: orig.orders.map(o => {
                            if (o.service.id === action.payload.productId) {
                                return {
                                    ...o,
                                    service: {
                                        ...o.service,
                                        isFavorite: false,
                                    },
                                };
                            } else {
                                return o;
                            }
                        }),
                    };
                });

                break;
            }
        }
    }

    private setState(set$: (orig: ShopPreviousState) => ShopNextState): void {
        this._current$.next({
            previous: this.snapshot,
            current: set$(this.snapshot),
        });
    }
}
