import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {defaultBottomMatDialog600pxConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {EvodeskShopNavigationMobileComponentProps} from './EvodeskShopNavigationMobile.component';
import {EvodeskShopNavigationModalContainerComponent as Modal} from '../EvodeskShopNavigationModal/EvodeskShopNavigationModalContainer.component';

import {EvodeskShopStateService} from '../../state/EvodeskShopState.service';

interface State {
    ready: boolean;
    props?: EvodeskShopNavigationMobileComponentProps;
}

@Component({
    selector: 'evodesk-shop-navigation-mobile-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-shop-navigation-mobile [props]="state.props" (openModal)="openModal()"></evodesk-shop-navigation-mobile>
      </ng-container>
    `,
})
export class EvodeskShopNavigationMobileContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private viewContainerRef: ViewContainerRef,
        private matDialog: MatDialog,
        private shopState: EvodeskShopStateService,
    ) {}

    ngOnInit(): void {
        this.shopState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    categories: s.current.categories,
                    orderBy: s.current.orderBy,
                    orderDirection: s.current.orderDirection,
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    openModal(): void {
        const close$: Subject<void> = new Subject<void>();

        const matDialogRef: MatDialogRef<Modal> = this.matDialog.open(Modal, {
            ...defaultBottomMatDialog600pxConfig,
            viewContainerRef: this.viewContainerRef,
            closeOnNavigation: true,
        });

        matDialogRef.componentInstance.closeEvent.pipe(takeUntil(close$)).subscribe(() => {
            matDialogRef.close();
        });

        matDialogRef.afterClosed().subscribe(() => {
            close$.next(undefined);
        });
    }
}
