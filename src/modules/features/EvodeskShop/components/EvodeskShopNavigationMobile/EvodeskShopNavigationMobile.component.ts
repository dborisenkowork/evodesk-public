import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {ShopCategoryModel} from '../../../../app/EvodeskRESTApi/models/ShopCategory.model';

import {ShopCatalogViewOrderBy, ShopCatalogViewOrderDirection} from '../../state/EvodeskShopState.service';

interface Props {
    categories: Array<ShopCategoryModel>;
    orderBy: ShopCatalogViewOrderBy;
    orderDirection: ShopCatalogViewOrderDirection;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskShopNavigationMobileComponentProps};

@Component({
    selector: 'evodesk-shop-navigation-mobile',
    templateUrl: './EvodeskShopNavigationMobile.component.pug',
    styleUrls: [
        './EvodeskShopNavigationMobile.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskShopNavigationMobileComponent implements OnChanges
{
    @Input() props: Props;

    @Output('openModal') openModalEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    open(): void {
        this.openModalEvent.emit(undefined);
    }
}
