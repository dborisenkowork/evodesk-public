import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {ShopProductId, ShopProductModel} from '../../../../app/EvodeskRESTApi/models/ShopProduct.model';
import {ShopCategoryModel} from '../../../../app/EvodeskRESTApi/models/ShopCategory.model';
import {ShopOrderModel} from '../../../../app/EvodeskRESTApi/models/ShopOrder.model';

interface Props {
    loading: boolean;
    category: ShopCategoryModel | undefined;
    products: Array<ShopProductModel>;
    productUrls: Array<{ id: ShopProductId; url: any; }>;
    canLoadMore: boolean;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskShopCatalogFeedComponentProps};

@Component({
    selector: 'evodesk-shop-catalog-feed',
    templateUrl: './EvodeskShopCatalogFeed.component.pug',
    styleUrls: [
        './EvodeskShopCatalogFeed.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskShopCatalogFeedComponent implements OnChanges
{
    @Input() props: Props;

    @Output('loadMore') loadMoreEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('open') openEvent: EventEmitter<ShopOrderModel> = new EventEmitter<ShopOrderModel>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    loadMore(): void {
        if (! this.props.loading) {
            this.loadMoreEvent.emit(undefined);
        }
    }

    open(order: ShopOrderModel): void {
        this.openEvent.emit(order);
    }

    productUrl(product: ShopProductModel): any {
        return this.props.productUrls.filter(u => u.id === product.id)[0].url;
    }

    preventDefault($event: MouseEvent): void {
        if (! ($event.metaKey || $event.ctrlKey || $event.altKey)) {
            $event.preventDefault();
            $event.stopImmediatePropagation();
        }
    }
}
