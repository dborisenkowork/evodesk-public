import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {EvodeskShopCatalogFeedComponentProps} from './EvodeskShopCatalogFeed.component';

import {ShopCategoryId} from '../../../../app/EvodeskRESTApi/models/ShopCategory.model';
import {ShopProductModel} from '../../../../app/EvodeskRESTApi/models/ShopProduct.model';

import {ShopRESTService} from '../../../../app/EvodeskRESTApi/services/ShopREST.service';
import {EvodeskShopStateService, ShopAction, ShopCatalogViewOrderBy, ShopCatalogViewOrderDirection, shopViewOrderToBackendMap} from '../../state/EvodeskShopState.service';
import {EvodeskShopOpenProductService} from '../../services/EvodeskShopOpenProduct.service';
import {EvodeskShopUrlService} from '../../services/EvodeskShopUrl.service';
import {EvodeskAppGlobalScrollService} from '../../../../app/EvodeskApp/services/EvodeskAppGlobalScroll.service';

const scrollTrigger: number = 100;

interface State {
    ready: boolean;
    destroyed: boolean;
    props?: EvodeskShopCatalogFeedComponentProps;
}

@Component({
    selector: 'evodesk-shop-catalog-feed-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <ng-container *ngIf="state.ready">
          <evodesk-shop-catalog-feed [props]="state.props" (loadMore)="loadMore()" (open)="open($event)"></evodesk-shop-catalog-feed>
        </ng-container>
    `,
})
export class EvodeskShopCatalogFeedContainerComponent implements OnInit, OnDestroy
{
    private nextFetch$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        destroyed: false,
        props: {
            loading: true,
            category: undefined,
            products: [],
            productUrls: [],
            canLoadMore: false,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private viewContainerRef: ViewContainerRef,
        private appGlobalScroll: EvodeskAppGlobalScrollService,
        private shopState: EvodeskShopStateService,
        private shopREST: ShopRESTService,
        private shopOpen: EvodeskShopOpenProductService,
        private shopUrl: EvodeskShopUrlService,
    ) {}

    ngOnInit(): void {
        this.shopState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.productInFeed),
            distinctUntilChanged(),
        ).subscribe((productIds) => {
            const mapProducts: { [productId: number]: ShopProductModel } = {};

            this.shopState.snapshot.products.forEach(p => {
                mapProducts[p.id] = p;
            });

            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    products: productIds
                        .map(productId => mapProducts[productId])
                        .filter(p => !! p),
                },
            };

            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    productUrls: this.state.props.products.map(p => {
                        return {
                            id: p.id,
                            url: this.shopUrl.getShopProductUrl(p),
                        };
                    }),
                },
            };

            this.cdr.detectChanges();
        });

        this.shopState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.hasMoreToLoad),
            distinctUntilChanged(),
        ).subscribe((canLoadMore) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    canLoadMore: canLoadMore,
                },
            };

            this.cdr.detectChanges();
        });

        this.shopState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.currentCategory),
            distinctUntilChanged(),
        ).subscribe((category) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    category: category,
                },
            };

            this.cdr.detectChanges();
        });

        this.shopState.side$.pipe(
            filter(a => !!~[
                ShopAction.SetCurrentCategory,
                ShopAction.SetOrder,
            ].indexOf(a.type)),
        ).subscribe(() => {
            setTimeout(() => {
                if (! this.state.destroyed) {
                    this.fetch();
                }
            });
        });

        this.appGlobalScroll.scroll$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((e) => {
            if ((e.scrollTop + e.areaHeight) > (e.scrollHeight - scrollTrigger)) {
                if (this.state.props.canLoadMore) {
                    this.fetch();
                }
            }
        });

        this.fetch();
    }

    ngOnDestroy(): void {
        this.state = {
            ...this.state,
            destroyed: true,
        };

        this.nextFetch$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    fetch(): void {
        this.loadingUp();

        this.nextFetch$.next(undefined);

        const currentCategory: ShopCategoryId | undefined = this.shopState.snapshot.currentCategory ? this.shopState.snapshot.currentCategory.id : undefined;
        const currentLimit: number = this.shopState.snapshot.limit;
        const currentPage: number = this.shopState.snapshot.page;
        const currentSearch: string | undefined = this.shopState.snapshot.search;
        const currentOrderBy: ShopCatalogViewOrderBy = this.shopState.snapshot.orderBy;
        const currentOrderDirection: ShopCatalogViewOrderDirection = this.shopState.snapshot.orderDirection;

        this.shopREST.getProducts({
            limit: currentLimit + 1,
            page: currentPage,
            search: currentSearch || undefined,
            category: currentCategory || undefined,
            orderBy: currentOrderBy ? shopViewOrderToBackendMap.filter(e => e.from === currentOrderBy)[0].to : undefined,
            orderDirection: <any>currentOrderDirection,
        }).pipe(takeUntil(this.nextFetch$)).subscribe(
            (httpResponse) => {
                this.shopState.dispatch({
                    type: ShopAction.PushProducts,
                    payload: {
                        products: httpResponse.products.slice(0, currentLimit),
                    },
                });

                this.shopState.dispatch({
                    type: ShopAction.SetCanLoadMore,
                    payload: {
                        canLoadMore: httpResponse.products.length > currentLimit,
                    },
                });

                this.shopState.dispatch({
                    type: ShopAction.DisplayProductsToFeed,
                    payload: {
                        productIds: httpResponse.products.map(p => p.id),
                    },
                });

                this.shopState.dispatch({
                    type: ShopAction.SetPage,
                    payload: {
                        page: currentPage + 1,
                    },
                });

                this.loadingDown();
            },
            () => {
                this.loadingDown();
            },
        );
    }

    loadMore(): void {
        this.fetch();
    }

    open(product: ShopProductModel): void {
        this.shopOpen.open({
            product: product.slug,
            viewContainerRef: this.viewContainerRef,
            replaceUrl: product,
        });
    }

    private loadingUp(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                loading: true,
            },
        };

        this.cdr.detectChanges();
    }

    private loadingDown(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                loading: false,
            },
        };

        this.cdr.detectChanges();
    }
}
