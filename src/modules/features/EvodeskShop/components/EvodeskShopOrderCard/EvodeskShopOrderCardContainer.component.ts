import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {EvodeskShopOrderCardComponentProps} from './EvodeskShopOrderCard.component';

import {ShopOrderModel} from '../../../../app/EvodeskRESTApi/models/ShopOrder.model';

interface State {
    ready: boolean;
    props?: EvodeskShopOrderCardComponentProps;
}

@Component({
    selector: 'evodesk-shop-order-card-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-shop-order-card [props]="state.props" (open)="open($event)"></evodesk-shop-order-card>
      </ng-container>
    `,
})
export class EvodeskShopOrderCardContainerComponent implements OnChanges
{
    @Input() order: ShopOrderModel;

    @Output('open') openEvent: EventEmitter<ShopOrderModel> = new EventEmitter<ShopOrderModel>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['order']) {
            if (this.order) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        order: this.order,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: {
                        ...this.state.props,
                        order: undefined,
                    },
                };
            }
        }
    }

    open(order: ShopOrderModel): void {
        this.openEvent.emit(order);
    }
}
