import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';

import {evodeskShopMaxReviewLength} from '../../configs/EvodeskShop.constants';

import {proxy} from '../../../../../functions/proxy.function';

import {EvodeskShopReviewFormComponentFormValue, EvodeskShopReviewFormComponentProps} from './EvodeskShopReviewForm.component';

import {ShopRESTService} from '../../../../app/EvodeskRESTApi/services/ShopREST.service';
import {EvodeskShopStateService} from '../../state/EvodeskShopState.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';

import {ShopOrderId} from '../../../../app/EvodeskRESTApi/models/ShopOrder.model';
import {ShopProductModel} from '../../../../app/EvodeskRESTApi/models/ShopProduct.model';

interface State {
    ready: boolean;
    props?: EvodeskShopReviewFormComponentProps;
}

@Component({
    selector: 'evodesk-shop-review-form-container',
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-shop-review-form [props]="state.props" (doSubmit)="submit($event)"></evodesk-shop-review-form>
      </ng-container>
    `,
})
export class EvodeskShopReviewFormContainerComponent implements OnChanges
{
    @Input() product: ShopProductModel;

    @Output('success') successEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private fb: FormBuilder,
        private cdr: ChangeDetectorRef,
        private alert: EvodeskAlertModalService,
        private shopREST: ShopRESTService,
        private shopState: EvodeskShopStateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['product']) {
            if (this.product) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        submitting: false,
                        product: this.product,
                        form: this.fb.group({
                            stars: [0],
                            review: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(evodeskShopMaxReviewLength)]],
                        }),
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    submit(formValue: EvodeskShopReviewFormComponentFormValue): void {
        if (formValue.stars < 1 || formValue.stars > 5) {
            this.alert.openTranslated('EvodeskShop.components.EvodeskShopReviewForm.SetStars').subscribe();
        } else {
            this.state.props.form.disable();

            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    submitting: true,
                },
            };

            const orderId: ShopOrderId = this.shopState.snapshot.orders.filter(o => o.service.id === this.product.id)[0].id;

            proxy(this.shopREST.postReview(orderId, { review: formValue.review, stars: formValue.stars })).subscribe(
                () => {
                    this.state.props.form.enable();

                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            submitting: false,
                        },
                    };

                    this.alert.openTranslated('EvodeskShop.components.EvodeskShopReviewForm.Success').subscribe();

                    this.cdr.detectChanges();

                    this.successEvent.emit(undefined);
                },
                () => {
                    this.state.props.form.enable();

                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            submitting: false,
                        },
                    };

                    this.alert.httpError().subscribe();

                    this.cdr.detectChanges();
                },
            );

            this.cdr.detectChanges();
        }
    }
}
