import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormGroup} from '@angular/forms';

import {ShopProductModel} from '../../../../app/EvodeskRESTApi/models/ShopProduct.model';

interface Props {
    product: ShopProductModel;
    form: FormGroup;
    submitting: boolean;
}

interface State {
    ready: boolean;
    hoveredStars: number;
}

export {Props as EvodeskShopReviewFormComponentProps};

interface FormValue {
    stars: number;
    review: string;
}

export {FormValue as EvodeskShopReviewFormComponentFormValue};

@Component({
    selector: 'evodesk-shop-review-form',
    templateUrl: './EvodeskShopReviewForm.component.pug',
    styleUrls: [
        './EvodeskShopReviewForm.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskShopReviewFormComponent implements OnChanges
{
    @Input() props: Props;

    @Output('doSubmit') submitEvent: EventEmitter<FormValue> = new EventEmitter<FormValue>();

    public state: State = {
        ready: false,
        hoveredStars: 0,
    };

    constructor(
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get stars(): number {
        return (this.props.form.value as FormValue).stars;
    }

    setStars(stars: number): void {
        this.props.form.patchValue({
            stars: stars,
        });
    }

    hoverStars(stars: number): void {
        this.state = {
            ...this.state,
            hoveredStars: stars,
        };

        this.cdr.detectChanges();
    }

    overStars(): void {
        this.state = {
            ...this.state,
            hoveredStars: 0,
        };

        this.cdr.detectChanges();
    }

    submit(): void {
        if (this.props.form.valid && ! this.props.submitting) {
            this.submitEvent.emit(this.props.form.value);
        }
    }
}
