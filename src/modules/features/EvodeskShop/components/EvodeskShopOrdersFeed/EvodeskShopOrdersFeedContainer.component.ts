import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import {ShopOrderModel} from '../../../../app/EvodeskRESTApi/models/ShopOrder.model';

import {EvodeskShopOrdersFeedComponentProps} from './EvodeskShopOrdersFeed.component';

import {ShopRESTService} from '../../../../app/EvodeskRESTApi/services/ShopREST.service';
import {EvodeskShopStateService, ShopAction} from '../../state/EvodeskShopState.service';
import {EvodeskShopOpenProductService} from '../../services/EvodeskShopOpenProduct.service';
import {EvodeskShopUrlService} from '../../services/EvodeskShopUrl.service';
import {EvodeskAppGlobalScrollService} from '../../../../app/EvodeskApp/services/EvodeskAppGlobalScroll.service';

interface State {
    ready: boolean;
    destroyed: boolean;
    props?: EvodeskShopOrdersFeedComponentProps;
}

const scrollTrigger: number = 100;

@Component({
    selector: 'evodesk-shop-orders-feed-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <ng-container *ngIf="state.ready">
          <evodesk-shop-orders-feed [props]="state.props" (loadMore)="loadMore()" (open)="open($event)"></evodesk-shop-orders-feed>
        </ng-container>
    `,
})
export class EvodeskShopOrdersFeedContainerComponent implements OnInit, OnDestroy
{
    private nextFetch$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        destroyed: false,
        props: {
            loading: true,
            orders: [],
            productUrls: [],
            canLoadMore: false,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private viewContainerRef: ViewContainerRef,
        private appGlobalScroll: EvodeskAppGlobalScrollService,
        private shopState: EvodeskShopStateService,
        private shopREST: ShopRESTService,
        private shopOpen: EvodeskShopOpenProductService,
        private shopUrl: EvodeskShopUrlService,
    ) {}

    ngOnInit(): void {
        this.shopState.dispatch({
            type: ShopAction.SetOrders,
            payload: {
                orders: [],
            },
        });

        this.shopState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.orders),
            distinctUntilChanged(),
        ).subscribe((orders) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    orders: orders,
                },
            };

            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    productUrls: this.state.props.orders.map(o => {
                        return {
                            id: o.service.id,
                            url: this.shopUrl.getShopProductUrl(o.service),
                        };
                    }),
                },
            };

            this.cdr.detectChanges();
        });

        this.appGlobalScroll.scroll$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((e) => {
            if ((e.scrollTop + e.areaHeight) > (e.scrollHeight - scrollTrigger)) {
                if (this.state.props.canLoadMore) {
                    this.fetch();
                }
            }
        });

        this.fetch();
    }

    ngOnDestroy(): void {
        this.state = {
            ...this.state,
            destroyed: true,
        };

        this.nextFetch$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    fetch(): void {
        this.loadingUp();

        this.nextFetch$.next(undefined);

        this.shopREST.getOrders().pipe(takeUntil(this.nextFetch$)).subscribe(
            (httpResponse) => {
                this.shopState.dispatch({
                    type: ShopAction.SetOrders,
                    payload: {
                        orders: httpResponse,
                    },
                });

                this.shopState.dispatch({
                    type: ShopAction.SetCanLoadMore,
                    payload: {
                        canLoadMore: false,
                    },
                });

                this.loadingDown();
            },
            () => {
                this.loadingDown();
            },
        );
    }

    loadMore(): void {
        this.fetch();
    }

    open(order: ShopOrderModel): void {
        this.shopOpen.open({
            product: order.service,
            viewContainerRef: this.viewContainerRef,
            replaceUrl: order.service,
        });
    }

    private loadingUp(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                loading: true,
            },
        };

        this.cdr.detectChanges();
    }

    private loadingDown(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                loading: false,
            },
        };

        this.cdr.detectChanges();
    }
}
