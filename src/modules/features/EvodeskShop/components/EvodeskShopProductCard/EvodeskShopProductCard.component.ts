import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import * as ellipsize from 'ellipsize';

import {ShopProductModel} from '../../../../app/EvodeskRESTApi/models/ShopProduct.model';

import {environment} from '../../../../../environments/environment.config';

interface State {
    ready: boolean;
}

interface Props {
    product: ShopProductModel;
}

export {Props as EvodeskShopProductCardComponentProps};

@Component({
    selector: 'evodesk-shop-product-card',
    templateUrl: './EvodeskShopProductCard.component.pug',
    styleUrls: [
        './EvodeskShopProductCard.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskShopProductCardComponent implements OnChanges
{
    @Input() props: Props;

    @Output('open') openEvent: EventEmitter<ShopProductModel> = new EventEmitter<ShopProductModel>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get price(): string {
        return Math.floor(parseInt(this.props.product.price, 10)).toString();
    }

    get imageUrl(): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/shop/service/image?name=${this.props.product.image}`;
    }

    get imageCss(): any {
        return {
            'background-image': `url(${this.imageUrl})`,
        };
    }

    get title(): string {
        return ellipsize(this.props.product.title, 64);
    }

    open($event: MouseEvent): void {
        if (! ($event.metaKey || $event.ctrlKey || $event.altKey)) {
            this.openEvent.emit(this.props.product);
        }
    }
}
