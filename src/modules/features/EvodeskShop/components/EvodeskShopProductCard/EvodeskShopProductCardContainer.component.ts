import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {EvodeskShopProductCardComponentProps} from './EvodeskShopProductCard.component';

import {ShopProductModel} from '../../../../app/EvodeskRESTApi/models/ShopProduct.model';

interface State {
    ready: boolean;
    props?: EvodeskShopProductCardComponentProps;
}

@Component({
    selector: 'evodesk-shop-product-card-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-shop-product-card [props]="state.props" (open)="open($event)"></evodesk-shop-product-card>
      </ng-container>
    `,
})
export class EvodeskShopProductCardContainerComponent implements OnChanges
{
    @Input() product: ShopProductModel;

    @Output('open') openEvent: EventEmitter<ShopProductModel> = new EventEmitter<ShopProductModel>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['product']) {
            if (this.product) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        product: this.product,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: {
                        ...this.state.props,
                        product: undefined,
                    },
                };
            }
        }
    }

    open(product: ShopProductModel): void {
        this.openEvent.emit(product);
    }
}
