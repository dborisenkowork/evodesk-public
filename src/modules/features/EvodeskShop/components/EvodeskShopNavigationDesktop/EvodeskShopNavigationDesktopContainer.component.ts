import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskShopNavigationDesktopComponentProps} from './EvodeskShopNavigationDesktop.component';

import {EvodeskShopStateService, ShopAction, ShopCatalogViewOrderBy, ShopCatalogViewOrderDirection} from '../../state/EvodeskShopState.service';

interface State {
    ready: boolean;
    props?: EvodeskShopNavigationDesktopComponentProps;
}

@Component({
    selector: 'evodesk-shop-navigation-desktop-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-shop-navigation-desktop
          [props]="state.props"
          (orderBy)="orderBy($event)"
          (orderDirection)="orderDirection($event)"
        ></evodesk-shop-navigation-desktop>
      </ng-container>
    `,
})
export class EvodeskShopNavigationDesktopContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private shopState: EvodeskShopStateService,
    ) {}

    ngOnInit(): void {
        this.shopState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    route: s.current.route,
                    categories: s.current.categories,
                    currentCategoryId: s.current.currentCategory ? s.current.currentCategory.id : undefined,
                    orderBy: s.current.orderBy,
                    orderDirection: s.current.orderDirection,
                },
            };

            this.cdr.markForCheck();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    orderBy(orderBy: ShopCatalogViewOrderBy): void {
        this.shopState.dispatch({
            type: ShopAction.SetOrder,
            payload: {
                orderBy: orderBy,
                orderDirection: this.shopState.snapshot.orderDirection,
            },
        });
    }

    orderDirection(orderDirection: ShopCatalogViewOrderDirection): void {
        this.shopState.dispatch({
            type: ShopAction.SetOrder,
            payload: {
                orderBy: this.shopState.snapshot.orderBy,
                orderDirection: orderDirection,
            },
        });
    }
}
