import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnDestroy, Output, SimpleChanges, TemplateRef, ViewChild} from '@angular/core';
import {MatMenuTrigger} from '@angular/material';

import {interval, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {ShopProductModel} from '../../../../app/EvodeskRESTApi/models/ShopProduct.model';
import {ShopProductReviewModel} from '../../../../app/EvodeskRESTApi/models/ShopProductReview.model';
import {ShopCategoryModel} from '../../../../app/EvodeskRESTApi/models/ShopCategory.model';

import {environment} from '../../../../../environments/environment.config';

import {SocialNetworks} from '../../../../app/EvodeskApp/services/EvodeskShareWithSocialNetworks.service';

interface State {
    ready: boolean;
    descriptionExpanded: boolean;
    shouldDisplayExpandButton: boolean;
    disableClose: boolean;
}

interface Props {
    loading: boolean;
    product?: ShopProductModel;
    wasOrdered: boolean;
    buying: boolean;
    context: EvodeskShopProductViewComponentContext;
    reviews: Array<ShopProductReviewModel>;
    loadingReviews: boolean;
    category: ShopCategoryModel;
    categoryRouterLink: any;
    isMobile: boolean;
    isFavoriting: boolean;
    canReview: boolean;
}

export type EvodeskShopProductViewComponentContext = 'modal' | 'page';

export {Props as EvodeskShopProductViewComponentProps};

@Component({
    selector: 'evodesk-shop-product-view',
    templateUrl: './EvodeskShopProductView.component.pug',
    styleUrls: [
        './EvodeskShopProductView.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskShopProductViewComponent implements OnChanges, AfterViewInit, OnDestroy
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('buy') buyEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('buyRubles') buyRublesEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openShareModal') openShareModalEvent: EventEmitter<TemplateRef<any>> = new EventEmitter<TemplateRef<any>>();
    @Output('openBuyModal') openBuyModalEvent: EventEmitter<TemplateRef<any>> = new EventEmitter<TemplateRef<any>>();
    @Output('favorite') favoriteEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('share') shareEvent: EventEmitter<SocialNetworks> = new EventEmitter<SocialNetworks>();

    @Output('successReview') successReviewEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('contents') contents: ElementRef;

    @ViewChild('shareTrigger', { read: MatMenuTrigger }) shareMenuTrigger: MatMenuTrigger;
    @ViewChild('shareMenuTemplate') shareMenuTemplate: TemplateRef<any>;

    @ViewChild('buyTrigger', { read: MatMenuTrigger }) buyMenuTrigger: MatMenuTrigger;
    @ViewChild('buyMenuTemplate') buyMenuTemplate: TemplateRef<any>;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        descriptionExpanded: false,
        shouldDisplayExpandButton: false,
        disableClose: false,
    };

    constructor(
        private ngZone: NgZone,
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngAfterViewInit(): void {
        this.ngZone.runOutsideAngular(() => {
            interval(50).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                if (this.contents) {
                    const shouldDisplay: boolean = (this.contents.nativeElement as HTMLElement).scrollHeight > (this.contents.nativeElement as HTMLElement).clientHeight;

                    if (shouldDisplay !== this.state.shouldDisplayExpandButton) {
                        this.state = {
                            ...this.state,
                            shouldDisplayExpandButton: shouldDisplay,
                        };

                        this.cdr.detectChanges();
                    }
                }
            });
        });

        [this.shareMenuTrigger, this.buyMenuTrigger].forEach(m => {
            m.menuClosed.pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                this.state = {
                    ...this.state,
                    disableClose: false,
                };
            });
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    expandDescription(): void {
        this.state = {
            ...this.state,
            descriptionExpanded: true,
        };
    }

    favorite(): void {
        if (! this.props.isFavoriting) {
            this.favoriteEvent.emit(undefined);
        }
    }

    buyVariants(): void {
        if (this.props.isMobile) {
            this.openBuyModalEvent.emit(this.buyMenuTemplate);
        } else {
            this.state = {
                ...this.state,
                disableClose: true,
            };

            this.buyMenuTrigger.openMenu();
        }
    }

    buyRubles(): void {
        this.buyRublesEvent.emit(undefined);
    }

    buy(): void {
        this.buyEvent.emit(undefined);
    }

    openShareMenu(): void {
        if (this.props.isMobile) {
            this.openShareModalEvent.emit(this.shareMenuTemplate);
        } else {
            this.state = {
                ...this.state,
                disableClose: true,
            };

            this.shareMenuTrigger.openMenu();
        }
    }

    share(network: SocialNetworks): void {
        this.shareEvent.emit(network);
    }

    close(): void {
        if (! this.state.disableClose && this.props.context === 'modal') {
            this.closeEvent.emit(undefined);
        }
    }

    trackByReviewId(index: number, item: ShopProductReviewModel): any {
        return item.id;
    }

    get price(): string {
        return Math.floor(parseInt(this.props.product.price, 10)).toString();
    }

    get imageUrl(): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/shop/service/image?name=${this.props.product.image}`;
    }

    get wasOrdered(): boolean {
        return this.props.wasOrdered;
    }


    get willBeSentFile(): boolean {
        return !! this.props.product.file;
    }

    get willBeSentLink(): boolean {
        return !! this.props.product.link;
    }

    get willBeSentPromo(): boolean {
        return this.props.product.haspromocode;
    }

    get hasAuthor(): boolean {
        return !! this.props.product.owner;
    }

    get hasReviews(): boolean {
        return this.props.reviews.length > 0;
    }
}
