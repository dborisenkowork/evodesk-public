import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, TemplateRef, ViewContainerRef} from '@angular/core';
import {Meta} from '@angular/platform-browser';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil, tap} from 'rxjs/operators';

import {environment} from '../../../../../environments/environment.config';
import {defaultBottomMatDialogConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {proxy} from '../../../../../functions/proxy.function';

import {EvodeskShopProductViewComponentContext, EvodeskShopProductViewComponentProps} from './EvodeskShopProductView.component';

import {ShopProductId, ShopProductModel, ShopProductSlug} from '../../../../app/EvodeskRESTApi/models/ShopProduct.model';
import {ShopCategoryModel} from '../../../../app/EvodeskRESTApi/models/ShopCategory.model';

import {EvodeskShopStateService, ShopAction, ShopLayoutVariant} from '../../state/EvodeskShopState.service';
import {ShopRESTService} from '../../../../app/EvodeskRESTApi/services/ShopREST.service';
import {EvodeskConfirmModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {EvodeskShopUrlService} from '../../services/EvodeskShopUrl.service';
import {EvodeskShopBodyParserService} from '../../services/EvodeskShopBodyParser.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';
import {EvodeskShareWithSocialNetworkService, SocialNetworks} from '../../../../app/EvodeskApp/services/EvodeskShareWithSocialNetworks.service';

interface State {
    ready: boolean;
    props?: EvodeskShopProductViewComponentProps;
    subModalsOpened: boolean;
    matDialogRef?: MatDialogRef<any>;
}

@Component({
    selector: 'evodesk-shop-product-view-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-shop-product-view
          [props]="state.props"
          (close)="close()"
          (buy)="buy({ withRubles: false })"
          (buyRubles)="buy({ withRubles: true })"
          (openShareModal)="openShareModal($event)"
          (openBuyModal)="openBuyModal($event)"
          (favorite)="favorite()"
          (successReview)="onSuccessReview()"
          (share)="share($event)"
        ></evodesk-shop-product-view>
      </ng-container>
    `,
})
export class EvodeskShopProductViewContainerComponent implements OnChanges, OnDestroy
{
    @Input() product: ShopProductSlug | ShopProductModel;
    @Input() context: EvodeskShopProductViewComponentContext = 'modal';
    @Input() setupMetaTags: boolean = false;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        subModalsOpened: false,
        props: {
            loading: true,
            buying: false,
            wasOrdered: false,
            reviews: [],
            loadingReviews: false,
            context: this.context,
            category: undefined,
            categoryRouterLink: [],
            isMobile: false,
            isFavoriting: false,
            canReview: false,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private meta: Meta,
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private alert: EvodeskAlertModalService,
        private confirm: EvodeskConfirmModalService,
        private currentUser: CurrentUserService,
        private shopState: EvodeskShopStateService,
        private shopREST: ShopRESTService,
        private shopUrl: EvodeskShopUrlService,
        private shopBodyParser: EvodeskShopBodyParserService,
        private social: EvodeskShareWithSocialNetworkService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (changes['product'] && this.product) {
            this.shopState.current$.pipe(
                takeUntil(this.ngOnChanges$),
                filter(() => !! this.state.props && !! this.state.props.product),
                map(s => s.current.reviews.filter(r => !! r.service && r.service.id.toString() === this.state.props.product.id.toString())),
                distinctUntilChanged(),
            ).subscribe((reviews) => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        reviews: reviews,
                    },
                };

                this.cdr.detectChanges();
            });

            this.shopState.current$.pipe(
                takeUntil(this.ngOnChanges$),
                map(s => s.current.layoutVariant),
                distinctUntilChanged(),
            ).subscribe(() => {
                this.updateMobileFlag();
            });

            this.shopState.current$.pipe(
                takeUntil(this.ngOnChanges$),
                filter(() => !! this.state.props && !! this.state.props.product),
                map(s => !!~s.current.orderedProducts.indexOf(this.state.props.product.id)),
                distinctUntilChanged(),
            ).subscribe((wasOrdered) => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        wasOrdered: wasOrdered,
                    },
                };

                this.cdr.detectChanges();
            });

            if (typeof this.product === 'string') {
                const productSlug: string = this.product;

                let commentsLoaded: boolean = false;

                this.shopState.current$.pipe(
                    takeUntil(this.ngOnChanges$),
                    map(s => s.current.products.filter(p => p.slug === productSlug)[0]),
                    distinctUntilChanged(),
                    tap((product) => {
                        if (! product) {
                            this.shopREST.getProduct(productSlug).pipe(takeUntil(this.ngOnChanges$)).subscribe(
                                (p) => {
                                    this.shopState.dispatch({
                                        type: ShopAction.PushProducts,
                                        payload: {
                                            products: [p],
                                        },
                                    });
                                },
                            );
                        }
                    }),
                ).subscribe((product) => {
                    if (product) {
                        this.enable(product, ! commentsLoaded);

                        commentsLoaded = true;
                    } else {
                        this.disable();
                    }
                });
            } else {
                const productChanged: boolean = ! changes['product'].isFirstChange()
                    && !! changes['product'].previousValue
                    && !! changes['product'].currentValue
                    && (changes['product'].previousValue as ShopProductModel).id !== (changes['product'].currentValue as ShopProductModel).id;
                const shouldLoadComments: boolean = !! this.product && (changes['product'].isFirstChange() || productChanged);

                this.enable(this.product as ShopProductModel, shouldLoadComments);
            }
        } else {
            this.disable();
        }
    }

    ngOnDestroy(): void {
        if (this.setupMetaTags) {
            this.meta.removeTag('property="og:title"');
            this.meta.removeTag('property="og:type"');
            this.meta.removeTag('property="og:image"');
            this.meta.removeTag('property="og:description"');
        }

        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    enable(product: ShopProductModel, loadComments: boolean = true): void {
        const category: ShopCategoryModel = this.shopState.snapshot.categories.filter(c => c.id === product.category)[0];
        const wasOrdered: boolean = !!~this.shopState.snapshot.orderedProducts.indexOf(product.id);

        this.state = {
            ...this.state,
            ready: true,
            props: {
                ...this.state.props,
                loading: false,
                product: product,
                wasOrdered: wasOrdered,
                context: this.context,
                category: category,
                categoryRouterLink: this.shopUrl.getShopCategoryUrl(
                    this.shopState.snapshot.categories.filter(c => c.id === product.category)[0].slug,
                ),
                canReview: false,
            },
        };

        if (this.setupMetaTags) {
            this.meta.addTags([
                {
                    property: 'og:title',
                    content: product.title,
                },
                {
                    property: 'og:type',
                    content: `website`,
                },
                {
                    property: 'og:image',
                    content: `${environment.modules.EvoDeskRESTApi.apiEndpoint}/shop/service/image?name=${product.image}`,
                },
                {
                    property: 'og:description',
                    content: this.shopBodyParser.parseToMetaTag(product.description),
                },
            ]);
        }

        this.updateMobileFlag();

        if (loadComments) {
            this.fetchReviews();
        }
    }

    disable(): void {
        this.state = {
            ...this.state,
            ready: false,
            props: {
                category: undefined,
                categoryRouterLink: undefined,
                reviews: [],
                loadingReviews: false,
                context: 'modal',
                wasOrdered: false,
                product: undefined,
                loading: true,
                buying: false,
                isMobile: false,
                isFavoriting: false,
                canReview: false,
            },
        };

        this.cdr.detectChanges();
    }

    fetchReviews(): void {
        const p: ShopProductModel = this.state.props.product;

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                loadingReviews: true,
            },
        };

        this.shopState.dispatch({
            type: ShopAction.ClearReviews,
        });

        this.shopREST.getReviews(p.id).pipe(takeUntil(this.ngOnChanges$)).subscribe((reviews) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    loadingReviews: false,
                    canReview: this.state.props.wasOrdered && reviews.filter(r => r.user.id === this.currentUser.impersonatedAs.id).length === 0,
                },
            };

            this.shopState.dispatch({
                type: ShopAction.PushReviews,
                payload: {
                    reviews: reviews,
                },
            });
        });

        this.cdr.detectChanges();
    }

    updateMobileFlag(): void {
        if (this.state.ready) {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    isMobile: !!~[
                        ShopLayoutVariant.Mobile,
                    ].indexOf(this.shopState.snapshot.layoutVariant),
                },
            };
        }

        this.cdr.detectChanges();
    }

    close(): void {
        if (! this.state.props.buying && ! this.state.subModalsOpened) {
            this.closeEvent.emit(undefined);
        }
    }

    favorite(): void {
        if (this.state.props.isFavoriting) {
            return;
        }

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                isFavoriting: true,
            },
        };

        if (this.state.props.product.isFavorite) {
            proxy(this.shopREST.deleteFavorite(this.state.props.product.id)).pipe(takeUntil(this.ngOnChanges$)).subscribe(
                () => {
                    this.shopState.dispatch({
                        type: ShopAction.DeleteFavorite,
                        payload: {
                            productId: this.state.props.product.id,
                        },
                    });

                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            isFavoriting: false,
                            product: {
                                ...this.state.props.product,
                                isFavorite: false,
                            },
                        },
                    };

                    this.cdr.detectChanges();
                },
                () => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            isFavoriting: false,
                        },
                    };

                    this.cdr.detectChanges();
                },
            );
        } else {
            proxy(this.shopREST.addFavorite(this.state.props.product.id)).pipe(takeUntil(this.ngOnChanges$)).subscribe(
                () => {
                    this.shopState.dispatch({
                        type: ShopAction.AddFavorite,
                        payload: {
                            product: this.state.props.product,
                        },
                    });

                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            isFavoriting: false,
                            product: {
                                ...this.state.props.product,
                                isFavorite: true,
                            },
                        },
                    };

                    this.cdr.detectChanges();
                },
                () => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            isFavoriting: false,
                        },
                    };

                    this.cdr.detectChanges();
                },
            );
        }

        this.cdr.detectChanges();
    }

    onSuccessReview(): void {
        this.fetchReviews();
    }

    openShareModal(templateRef: TemplateRef<any>): void {
        this.state = {
            ...this.state,
            subModalsOpened: true,
        };

        const close$: Subject<void> = new Subject<void>();

        const dialogRef: MatDialogRef<any> = this.matDialog.open(templateRef, {
            ...defaultBottomMatDialogConfig,
            closeOnNavigation: true,
            viewContainerRef: this.viewContainerRef,
        });

        dialogRef.afterClosed().pipe(takeUntil(close$)).subscribe(() => {
            close$.next(undefined);

            this.state = {
                ...this.state,
                subModalsOpened: false,
            };
        });

        this.state = { ...this.state, matDialogRef: dialogRef };
    }

    openBuyModal(templateRef: TemplateRef<any>): void {
        this.state = {
            ...this.state,
            subModalsOpened: true,
        };

        const close$: Subject<void> = new Subject<void>();

        const dialogRef: MatDialogRef<any> = this.matDialog.open(templateRef, {
            ...defaultBottomMatDialogConfig,
            closeOnNavigation: true,
            viewContainerRef: this.viewContainerRef,
        });

        dialogRef.afterClosed().pipe(takeUntil(close$)).subscribe(() => {
            close$.next(undefined);

            this.state = {
                ...this.state,
                subModalsOpened: false,
            };
        });

        this.state = { ...this.state, matDialogRef: dialogRef };
    }

    buy(options: { withRubles?: boolean } = {}): void {
        const base: string = 'EvodeskShop.components.EvodeskShopProductView.BuyConfirm';
        const productId: ShopProductId = this.state.props.product.id;

        this.state = {
            ...this.state,
            subModalsOpened: true,
        };

        this.confirm.open({
            title: {
                text: `${base}.Title`,
                translate: true,
            },
            text: {
                text: `${base}.Text`,
                translate: true,
                replaces: [
                    this.state.props.product.title,
                ],
            },
            cancelButton: {
                text: `${base}.Cancel`,
                translate: true,
            },
            confirmButton: {
                text: `${base}.Confirm`,
                translate: true,
            },
            confirm: () => {
                const prefix: string = 'EvodeskShop.components.EvodeskShopProductView';

                this.state = {
                    ...this.state,
                    subModalsOpened: false,
                };

                (options.withRubles
                    ? this.shopREST.orderProductWithRubles(productId)
                    : this.shopREST.orderProduct(productId)
                ).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                    () => {
                        this.shopState.dispatch({
                            type: ShopAction.SetProductAsOrdered,
                            payload: {
                                productId: productId,
                            },
                        });

                        this.state = {
                            ...this.state,
                            props: {
                                ...this.state.props,
                                buying: false,
                            },
                        };

                        if (this.state.props.product.file) {
                            this.alert.openTranslated(`${prefix}.OrderFile`)
                                .pipe(takeUntil(this.ngOnDestroy$))
                                .subscribe();

                        } else if (this.state.props.product.link) {
                            this.alert.openTranslated(`${prefix}.OrderLink`)
                                .pipe(takeUntil(this.ngOnDestroy$))
                                .subscribe();
                        } else if (this.state.props.product.haspromocode) {
                            this.alert.openTranslated(`${prefix}.OrderPromo`)
                                .pipe(takeUntil(this.ngOnDestroy$))
                                .subscribe();
                        } else {
                            this.alert.openTranslated(`${prefix}.OrderDefault`)
                                .pipe(takeUntil(this.ngOnDestroy$))
                                .subscribe();
                        }

                        this.cdr.detectChanges();
                    },
                    () => {
                        this.alert.openTranslated(`${prefix}.OrderFail`)
                            .pipe(takeUntil(this.ngOnDestroy$))
                            .subscribe();

                        this.state = {
                            ...this.state,
                            props: {
                                ...this.state.props,
                                buying: false,
                            },
                        };

                        this.cdr.detectChanges();
                    },
                );

                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        buying: true,
                    },
                };

                this.cdr.detectChanges();
            },
            cancel: () => {
                this.state = {
                    ...this.state,
                    subModalsOpened: false,
                };
            },
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }

    share(network: SocialNetworks): void {
        const product: ShopProductModel = this.state.props.product;
        const category: ShopCategoryModel = this.shopState.snapshot.categories.filter(c => c.id === product.category)[0];

        this.social.share(network, {
            name: product.title,
            description: product.description,
            link: `${environment.serverHostName}/shop/${category.slug}/${product.slug}`,
            image: `${environment.modules.EvoDeskRESTApi.apiEndpoint}/shop/service/image?name=${product.image}`,
        });

        if (this.state.matDialogRef) {
            this.state.matDialogRef.close();
        }
    }
}
