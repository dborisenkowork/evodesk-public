import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {ShopFavoriteModel} from '../../../../app/EvodeskRESTApi/models/ShopFavorite.model';
import {ShopProductId, ShopProductModel} from '../../../../app/EvodeskRESTApi/models/ShopProduct.model';

interface Props {
    loading: boolean;
    favorites: Array<ShopFavoriteModel>;
    productUrls: Array<{ id: ShopProductId; url: any; }>;
    canLoadMore: boolean;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskShopFavoritesFeedComponentProps};

@Component({
    selector: 'evodesk-shop-favorites-feed',
    templateUrl: './EvodeskShopFavoritesFeed.component.pug',
    styleUrls: [
        './EvodeskShopFavoritesFeed.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskShopFavoritesFeedComponent implements OnChanges
{
    @Input() props: Props;

    @Output('loadMore') loadMoreEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('open') openEvent: EventEmitter<ShopProductModel> = new EventEmitter<ShopProductModel>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    loadMore(): void {
        if (! this.props.loading) {
            this.loadMoreEvent.emit(undefined);
        }
    }

    open(product: ShopProductModel): void {
        this.openEvent.emit(product);
    }

    productUrl(product: ShopProductModel): any {
        return this.props.productUrls.filter(u => u.id === product.id)[0].url;
    }

    preventDefault($event: MouseEvent): void {
        if (! ($event.metaKey || $event.ctrlKey || $event.altKey)) {
            $event.preventDefault();
            $event.stopImmediatePropagation();
        }
    }
}
