import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import {evodeskShopDefaultLimit} from '../../configs/EvodeskShop.constants';

import {ShopFavoriteModel} from '../../../../app/EvodeskRESTApi/models/ShopFavorite.model';

import {EvodeskShopFavoritesFeedComponentProps} from './EvodeskShopFavoritesFeed.component';

import {ShopRESTService} from '../../../../app/EvodeskRESTApi/services/ShopREST.service';
import {EvodeskShopStateService, ShopAction} from '../../state/EvodeskShopState.service';
import {EvodeskShopOpenProductService} from '../../services/EvodeskShopOpenProduct.service';
import {EvodeskShopUrlService} from '../../services/EvodeskShopUrl.service';
import {EvodeskAppGlobalScrollService} from '../../../../app/EvodeskApp/services/EvodeskAppGlobalScroll.service';

interface State {
    ready: boolean;
    destroyed: boolean;
    page: number;
    props?: EvodeskShopFavoritesFeedComponentProps;
}

const scrollTrigger: number = 100;

@Component({
    selector: 'evodesk-shop-favorites-feed-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <ng-container *ngIf="state.ready">
          <evodesk-shop-favorites-feed [props]="state.props" (loadMore)="loadMore()" (open)="open($event)"></evodesk-shop-favorites-feed>
        </ng-container>
    `,
})
export class EvodeskShopFavoritesFeedContainerComponent implements OnInit, OnDestroy
{
    private nextFetch$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        destroyed: false,
        page: 0,
        props: {
            loading: true,
            favorites: [],
            productUrls: [],
            canLoadMore: false,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private viewContainerRef: ViewContainerRef,
        private appGlobalScroll: EvodeskAppGlobalScrollService,
        private shopState: EvodeskShopStateService,
        private shopREST: ShopRESTService,
        private shopOpen: EvodeskShopOpenProductService,
        private shopUrl: EvodeskShopUrlService,
    ) {}

    ngOnInit(): void {
        this.shopState.dispatch({
            type: ShopAction.ClearFavorites,
        });

        this.shopState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.favorites),
            distinctUntilChanged(),
        ).subscribe((favorites) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    favorites: favorites,
                },
            };

            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    productUrls: this.state.props.favorites.map(o => {
                        return {
                            id: o.service.id,
                            url: this.shopUrl.getShopProductUrl(o.service),
                        };
                    }),
                },
            };

            this.cdr.detectChanges();
        });

        this.appGlobalScroll.scroll$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((e) => {
            if ((e.scrollTop + e.areaHeight) > (e.scrollHeight - scrollTrigger)) {
                if (this.state.props.canLoadMore) {
                    this.fetch();
                }
            }
        });

        this.fetch();
    }

    ngOnDestroy(): void {
        this.state = {
            ...this.state,
            destroyed: true,
        };

        this.nextFetch$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    fetch(): void {
        this.loadingUp();

        this.nextFetch$.next(undefined);

        this.shopREST.getFavorites({ page: this.state.page + 1, limit: evodeskShopDefaultLimit + 1 }).pipe(takeUntil(this.nextFetch$)).subscribe(
            (httpResponse) => {
                this.state = {
                    ...this.state,
                    page: this.state.page + 1,
                    props: {
                        ...this.state.props,
                        canLoadMore: httpResponse.length > evodeskShopDefaultLimit,
                    },
                };

                this.shopState.dispatch({
                    type: ShopAction.PushFavorites,
                    payload: {
                        favorites: httpResponse.slice(0, evodeskShopDefaultLimit),
                    },
                });

                this.loadingDown();
            },
            () => {
                this.loadingDown();
            },
        );
    }

    loadMore(): void {
        this.fetch();
    }

    open(favorite: ShopFavoriteModel): void {
        this.shopOpen.open({
            product: favorite.service,
            viewContainerRef: this.viewContainerRef,
            replaceUrl: favorite.service,
        });
    }

    private loadingUp(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                loading: true,
            },
        };

        this.cdr.detectChanges();
    }

    private loadingDown(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                loading: false,
            },
        };

        this.cdr.detectChanges();
    }
}
