import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';

import {EvodeskShopReviewComponentProps} from './EvodeskShopReview.component';

import {ShopProductReviewModel} from '../../../../app/EvodeskRESTApi/models/ShopProductReview.model';

interface State {
    ready: boolean;
    props?: EvodeskShopReviewComponentProps;
}

@Component({
    selector: 'evodesk-shop-review-container',
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-shop-review [props]="state.props"></evodesk-shop-review>
      </ng-container>
    `,
})
export class EvodeskShopReviewContainerComponent implements OnChanges
{
    @Input() review: ShopProductReviewModel;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['review']) {
            if (this.review) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        review: this.review,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }
}
