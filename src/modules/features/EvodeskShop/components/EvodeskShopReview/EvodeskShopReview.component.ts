import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

import {Observable} from 'rxjs';

import * as moment from 'moment';

import {ShopProductReviewModel} from '../../../../app/EvodeskRESTApi/models/ShopProductReview.model';

import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';
import {EvodeskShopBodyParserService} from '../../services/EvodeskShopBodyParser.service';

interface Props {
    review: ShopProductReviewModel;
}

interface State {
    ready: boolean;
    variant: undefined | 'review' | 'mark';
    createdAt$?: Observable<string> | undefined;
    review$?: Observable<string> | undefined;
}

export {Props as EvodeskShopReviewComponentProps};

@Component({
    selector: 'evodesk-shop-review',
    templateUrl: './EvodeskShopReview.component.pug',
    styleUrls: [
        './EvodeskShopReview.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskShopReviewComponent implements OnChanges
{
    @Input() props: Props;

    public state: State = {
        ready: false,
        variant: undefined,
    };

    constructor(
        private dates: EvodeskDateService,
        private bodyParser: EvodeskShopBodyParserService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    variant: (() => {
                        if (!! this.props.review.review) {
                            return 'review';
                        } else if (!! this.props.review.stars) {
                            return 'mark';
                        } else {
                            throw new Error('Invalid review');
                        }
                    })(),
                    createdAt$: this.dates.diffFromNowV3(moment(this.props.review.creation_at.date.substr(0, 19), 'YYYY-MM-DD HH:ii:ss').toDate()),
                    review$: this.props.review.review ? this.bodyParser.parseToHtml(this.props.review.review) : undefined,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }
}
