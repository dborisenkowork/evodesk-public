import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';

import {ShopCategoryId, ShopCategoryModel} from '../../../../app/EvodeskRESTApi/models/ShopCategory.model';

import {ShopCatalogViewOrderBy, ShopCatalogViewOrderDirection, ShopRoute} from '../../state/EvodeskShopState.service';

interface Props {
    route: ShopRoute;
    categories: Array<ShopCategoryModel>;
    currentCategoryId: ShopCategoryId | undefined;
    orderBy: ShopCatalogViewOrderBy;
    orderDirection: ShopCatalogViewOrderDirection;
}

interface State {
    ready: boolean;
    firstTime: boolean;
}

export {Props as EvodeskShopNavigationModalComponentProps};

@Component({
    selector: 'evodesk-shop-navigation-modal',
    templateUrl: './EvodeskShopNavigationModal.component.pug',
    styleUrls: [
        './EvodeskShopNavigationModal.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskShopNavigationModalComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('orderBy') orderByEvent: EventEmitter<ShopCatalogViewOrderBy> = new EventEmitter<ShopCatalogViewOrderBy>();
    @Output('orderDirection') orderDirectionEvent: EventEmitter<ShopCatalogViewOrderDirection> = new EventEmitter<ShopCatalogViewOrderDirection>();

    @ViewChild('area') area: ElementRef;

    public state: State = {
        ready: false,
        firstTime: true,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    get allRouterLink(): any {
        return ['/shop'];
    }

    get ordersRouterLink(): any {
        return ['/shop/orders'];
    }

    get isAllActive(): boolean {
        return this.props.route === ShopRoute.Catalog && this.props.currentCategoryId === undefined;
    }

    get isOrdersActive(): boolean {
        return this.props.route === ShopRoute.Orders;
    }

    categoryRouterLink(category: ShopCategoryModel): any {
        return ['/shop/', category.slug];
    }

    isCategoryActive(category: ShopCategoryModel): boolean {
        return this.props.route === ShopRoute.Catalog && category.id === this.props.currentCategoryId;
    }

    isOrderByActive(orderBy: string): boolean {
        return this.props.orderBy === <any>orderBy;
    }

    isOrderDirectionActive(orderDirection: string): boolean {
        return this.props.orderDirection === <any>orderDirection;
    }

    orderBy(orderBy: string): void {
        if (this.props.orderBy !== <any>orderBy) {
            this.orderByEvent.emit(<any>orderBy);
        }
    }

    orderDirection(orderDirection: string): void {
        if (this.props.orderDirection !== <any>orderDirection) {
            this.orderDirectionEvent.emit(<any>orderDirection);
        }
    }
}
