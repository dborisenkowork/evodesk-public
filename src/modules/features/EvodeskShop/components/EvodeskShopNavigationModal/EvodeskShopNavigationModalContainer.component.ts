import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskShopNavigationModalComponentProps} from './EvodeskShopNavigationModal.component';

import {EvodeskShopStateService, ShopAction, ShopCatalogViewOrderBy, ShopCatalogViewOrderDirection} from '../../state/EvodeskShopState.service';

interface State {
    ready: boolean;
    props?: EvodeskShopNavigationModalComponentProps;
}

@Component({
    selector: 'evodesk-shop-navigation-modal-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-shop-navigation-modal
          [props]="state.props"
          (close)="close()"
          (orderBy)="orderBy($event)"
          (orderDirection)="orderDirection($event)"
        ></evodesk-shop-navigation-modal>
      </ng-container>
    `,
})
export class EvodeskShopNavigationModalContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private shopState: EvodeskShopStateService,
    ) {}

    ngOnInit(): void {
        this.shopState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    route: s.current.route,
                    categories: s.current.categories,
                    currentCategoryId: s.current.currentCategory ? s.current.currentCategory.id : undefined,
                    orderBy: s.current.orderBy,
                    orderDirection: s.current.orderDirection,
                },
            };

            this.cdr.markForCheck();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    orderBy(orderBy: ShopCatalogViewOrderBy): void {
        this.shopState.dispatch({
            type: ShopAction.SetOrder,
            payload: {
                orderBy: orderBy,
                orderDirection: this.shopState.snapshot.orderDirection,
            },
        });

        this.close();
    }

    orderDirection(orderDirection: ShopCatalogViewOrderDirection): void {
        this.shopState.dispatch({
            type: ShopAction.SetOrder,
            payload: {
                orderBy: this.shopState.snapshot.orderBy,
                orderDirection: orderDirection,
            },
        });

        this.close();
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
