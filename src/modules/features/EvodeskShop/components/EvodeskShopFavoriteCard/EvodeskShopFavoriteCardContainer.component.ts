import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {EvodeskShopFavoriteCardComponentProps} from './EvodeskShopFavoriteCard.component';

import {ShopFavoriteModel} from '../../../../app/EvodeskRESTApi/models/ShopFavorite.model';

interface State {
    ready: boolean;
    props?: EvodeskShopFavoriteCardComponentProps;
}

@Component({
    selector: 'evodesk-shop-favorite-card-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-shop-favorite-card [props]="state.props" (open)="open($event)"></evodesk-shop-favorite-card>
      </ng-container>
    `,
})
export class EvodeskShopFavoriteCardContainerComponent implements OnChanges
{
    @Input() favorite: ShopFavoriteModel;

    @Output('open') openEvent: EventEmitter<ShopFavoriteModel> = new EventEmitter<ShopFavoriteModel>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['favorite']) {
            if (this.favorite) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        favorite: this.favorite,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: {
                        ...this.state.props,
                        favorite: undefined,
                    },
                };
            }
        }
    }

    open(favorite: ShopFavoriteModel): void {
        this.openEvent.emit(favorite);
    }
}
