import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import * as ellipsize from 'ellipsize';

import {ShopProductModel} from '../../../../app/EvodeskRESTApi/models/ShopProduct.model';
import {ShopFavoriteModel} from '../../../../app/EvodeskRESTApi/models/ShopFavorite.model';

import {environment} from '../../../../../environments/environment.config';

interface State {
    ready: boolean;
}

interface Props {
    favorite: ShopFavoriteModel;
}

export {Props as EvodeskShopFavoriteCardComponentProps};

@Component({
    selector: 'evodesk-shop-favorite-card',
    templateUrl: './EvodeskShopFavoriteCard.component.pug',
    styleUrls: [
        './EvodeskShopFavoriteCard.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskShopFavoriteCardComponent implements OnChanges
{
    @Input() props: Props;

    @Output('open') openEvent: EventEmitter<ShopFavoriteModel> = new EventEmitter<ShopFavoriteModel>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get product(): ShopProductModel {
        return this.props.favorite.service;
    }

    get price(): string {
        return Math.floor(parseInt(this.product.price, 10)).toString();
    }

    get imageUrl(): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/shop/service/image?name=${this.product.image}`;
    }

    get imageCss(): any {
        return {
            'background-image': `url(${this.imageUrl})`,
        };
    }

    get title(): string {
        return ellipsize(this.product.title, 64);
    }

    open($event: MouseEvent): void {
        if (! ($event.metaKey || $event.ctrlKey || $event.altKey)) {
            if (this.props.favorite.service.slug !== undefined) {
                this.openEvent.emit(this.props.favorite);
            }
        }
    }
}
