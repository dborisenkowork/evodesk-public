import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
    selector: 'evodesk-shop-stars',
    templateUrl: './EvodeskShopStars.component.pug',
    styleUrls: [
        './EvodeskShopStars.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskShopStarsComponent
{
    @Input() stars: number;
}
