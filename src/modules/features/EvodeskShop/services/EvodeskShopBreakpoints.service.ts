import {Injectable} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';

import {filter, takeUntil} from 'rxjs/operators';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {evodeskShopLayoutDesktop, evodeskShopLayoutMobile} from '../configs/EvodeskShop.constants';

import {ShopLayoutVariant} from '../state/EvodeskShopState.service';

@Injectable()
export class EvodeskShopBreakpointsService
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private _current$: BehaviorSubject<ShopLayoutVariant> = new BehaviorSubject<ShopLayoutVariant>(ShopLayoutVariant.Desktop);

    constructor(
        private breakpointObserver: BreakpointObserver,
    ) {}

    init(): Observable<ShopLayoutVariant> {
        let hasEnoughWidth: boolean = false;
        let hasEnoughHeight: boolean = false;

        const update: Function = () => {
            if (hasEnoughWidth && hasEnoughHeight) {
                this._current$.next(ShopLayoutVariant.Desktop);
            } else {
                this._current$.next(ShopLayoutVariant.Mobile);
            }
        };

        this.breakpointObserver.observe(`(max-height: ${evodeskShopLayoutDesktop.minHeight - 1}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe((e) => {
            hasEnoughHeight = false;

            update();
        });

        this.breakpointObserver.observe(`(min-height: ${evodeskShopLayoutDesktop.minHeight}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe((e) => {
            hasEnoughHeight = true;

            update();
        });

        this.breakpointObserver.observe(`(max-width: ${evodeskShopLayoutMobile.to}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe((e) => {
            hasEnoughWidth = false;

            update();
        });

        this.breakpointObserver.observe(`(min-width: ${evodeskShopLayoutDesktop.from}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            hasEnoughWidth = true;

            update();
        });

        return this.current$;
    }

    destroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get current$(): Observable<ShopLayoutVariant> {
        return this._current$.asObservable();
    }
}
