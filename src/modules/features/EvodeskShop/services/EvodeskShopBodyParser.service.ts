import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import * as DOMPurify from 'dompurify';
import * as ellipsize from 'ellipsize';

@Injectable()
export class EvodeskShopBodyParserService
{
    parseToHtml(input: string): Observable<string> {
        return Observable.create(observer => {
            if (!! input) {
                let result: string = input;

                result = result.replace(/(?:\r\n|\r|\n)/g, '<br/>');
                result = DOMPurify.sanitize(result, { ALLOWED_TAGS: ['b', 'i', 'u', 's', 'br'] });

                observer.next(result);
                observer.complete();
            } else {
                observer.next('');
                observer.complete();
            }
        });
    }

    parseToMetaTag(input: string): string {
        let result: string = input;

        result = DOMPurify.sanitize(result);
        result = ellipsize(result, 32);

        return result;
    }
}
