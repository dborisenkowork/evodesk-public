import {Injectable, SimpleChange, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {Location} from '@angular/common';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskShopProductViewContainerComponent as ViewProduct} from '../components/EvodeskShopProductView/EvodeskShopProductViewContainer.component';

import {defaultBottomMatDialog800pxConfig} from '../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {ShopProductModel, ShopProductSlug} from '../../../app/EvodeskRESTApi/models/ShopProduct.model';

import {EvodeskShopUrlService} from './EvodeskShopUrl.service';

@Injectable()
export class EvodeskShopOpenProductService
{
    private nextProduct$: Subject<void> = new Subject<void>();

    constructor(
        private location: Location,
        private matDialog: MatDialog,
        private shopUrl: EvodeskShopUrlService,
    ) {}

    open(query: {
        product: ShopProductSlug | ShopProductModel;
        viewContainerRef: ViewContainerRef;
        replaceUrl?: ShopProductModel;
    }): void {
        this.nextProduct$.next(undefined);

        const wasUrl: string = this.location.path();

        if (query.replaceUrl) {
            this.location.go(this.shopUrl.getShopProductUrlAsString(query.replaceUrl));
        }

        const matDialogRef: MatDialogRef<ViewProduct> = this.matDialog.open(ViewProduct, {
            ...defaultBottomMatDialog800pxConfig,
            closeOnNavigation: true,
            viewContainerRef: query.viewContainerRef,
            autoFocus: false,
            disableClose: true,
        });

        this.nextProduct$.pipe(takeUntil(this.nextProduct$)).subscribe(() => {
            matDialogRef.close();
        });

        matDialogRef.afterClosed().pipe(takeUntil(this.nextProduct$)).subscribe(() => {
            this.nextProduct$.next(undefined);

            this.location.go(wasUrl);
        });

        matDialogRef.componentInstance.context = 'modal';
        matDialogRef.componentInstance.product = query.product;
        matDialogRef.componentInstance.setupMetaTags = false;

        matDialogRef.componentInstance.ngOnChanges({
            product: new SimpleChange(undefined, matDialogRef.componentInstance.product, true),
            context: new SimpleChange(undefined, matDialogRef.componentInstance.context, true),
            setupMetaTags: new SimpleChange(undefined, matDialogRef.componentInstance.setupMetaTags, true),
        });

        matDialogRef.componentInstance.closeEvent.pipe(takeUntil(this.nextProduct$)).subscribe(() => {
            matDialogRef.close();
        });
    }
}
