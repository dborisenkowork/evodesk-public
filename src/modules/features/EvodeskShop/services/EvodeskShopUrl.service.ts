import {Injectable} from '@angular/core';

import {EvodeskShopStateService} from '../state/EvodeskShopState.service';

import {ShopProductModel, ShopProductSlug} from '../../../app/EvodeskRESTApi/models/ShopProduct.model';

@Injectable()
export class EvodeskShopUrlService
{
    constructor(
        private shopState: EvodeskShopStateService,
    ) {}

    getShopCategoryUrl(input: ShopProductSlug): any {
        return ['/shop', input];
    }

    getShopProductUrl(input: ShopProductModel): any {
        return [
            '/shop',
            this.shopState.snapshot.categories.filter(c => c.id.toString() === input.category.toString())[0].slug,
            input.slug,
        ];
    }

    getShopProductUrlAsString(input: ShopProductModel): string {
        return [
            '/shop',
            this.shopState.snapshot.categories.filter(c => c.id.toString() === input.category.toString())[0].slug,
            input.slug,
        ].join('/');
    }
}
