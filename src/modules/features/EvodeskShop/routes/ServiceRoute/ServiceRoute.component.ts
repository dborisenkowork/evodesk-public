import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {ShopRESTService} from '../../../../app/EvodeskRESTApi/services/ShopREST.service';

interface State {
    message: string;
}

@Component({
    templateUrl: './ServiceRoute.component.pug',
    styleUrls: [
        './ServiceRoute.component.scss',
    ],
})
export class ServiceRouteComponent implements OnInit, OnDestroy
{
    public state: State = {
        message: undefined,
    };

    private fileType: string = 'file';
    private fileName: string;
    private serviceId: number;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private activatedRoute: ActivatedRoute,
        private shopRESTService: ShopRESTService,
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnInit(): void {
        this.activatedRoute.params.pipe(takeUntil(this.ngOnDestroy$)).subscribe(param => {
            this.serviceId = param.serviceId;
            this.fileType = param.fileType;
        });

        this.activatedRoute.queryParams.pipe(takeUntil(this.ngOnDestroy$)).subscribe(param => {
            this.fileName = param.name;
        });

        this.shopRESTService.downloadProduct(this.serviceId, this.fileType, this.fileName).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
            () => {
                this.state = {
                    ...this.state,
                    message: this.fileName,
                };

                this.cdr.detectChanges();
            },
            (error) => {
                this.state = {
                    ...this.state,
                    message: error.error.ErrorMsg,
                };
                this.cdr.detectChanges();
            });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
