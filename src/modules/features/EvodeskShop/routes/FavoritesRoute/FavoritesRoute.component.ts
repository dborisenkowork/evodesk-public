import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';

import {EvodeskShopStateService, ShopAction, ShopRoute} from '../../state/EvodeskShopState.service';

@Component({
    templateUrl: './FavoritesRoute.component.pug',
    styleUrls: [
        './FavoritesRoute.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FavoritesRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private shopState: EvodeskShopStateService,
    ) {}

    ngOnInit(): void {
        this.shopState.dispatch({
            type: ShopAction.SetRoute,
            payload: {
                route: ShopRoute.Favorites,
            },
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
