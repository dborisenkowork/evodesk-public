import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';

import {EvodeskShopStateService, ShopAction, ShopRoute} from '../../state/EvodeskShopState.service';

@Component({
    templateUrl: './OrdersRoute.component.pug',
    styleUrls: [
        './OrdersRoute.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrdersRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private shopState: EvodeskShopStateService,
    ) {}

    ngOnInit(): void {
        this.shopState.dispatch({
            type: ShopAction.SetRoute,
            payload: {
                route: ShopRoute.Orders,
            },
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
