import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {ShopCategoryModel} from '../../../../app/EvodeskRESTApi/models/ShopCategory.model';

import {EvodeskShopStateService, ShopAction, ShopRoute} from '../../state/EvodeskShopState.service';
import {EvodeskShopOpenProductService} from '../../services/EvodeskShopOpenProduct.service';

interface State {
    category: undefined | string;
}

@Component({
    templateUrl: './CatalogRoute.component.pug',
    styleUrls: [
        './CatalogRoute.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CatalogRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        category: undefined,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private router: Router,
        private viewContainerRef: ViewContainerRef,
        private activatedRoute: ActivatedRoute,
        private shopState: EvodeskShopStateService,
        private shopOpenProduct: EvodeskShopOpenProductService,
    ) {}

    ngOnInit(): void {
        this.shopState.dispatch({
            type: ShopAction.SetRoute,
            payload: {
                route: ShopRoute.Catalog,
            },
        });

        this.activatedRoute.params.pipe(takeUntil(this.ngOnDestroy$)).subscribe((params) => {
            if (params['category']) {
                const category: ShopCategoryModel = this.shopState.snapshot.categories.filter(c => c.slug === params['category'])[0];

                if (category) {
                    this.shopState.dispatch({
                        type: ShopAction.SetCurrentCategory,
                        payload: {
                            category: category,
                        },
                    });

                    this.state = {
                        ...this.state,
                        category: category.title,
                    };

                    this.cdr.detectChanges();
                } else {
                    this.router.navigate(['/shop']);
                }
            } else {
                this.shopState.dispatch({
                    type: ShopAction.SetCurrentCategory,
                    payload: {
                        category: undefined,
                    },
                });

                this.state = {
                    ...this.state,
                    category: undefined,
                };

                this.cdr.detectChanges();
            }

            if (params['product']) {
                this.shopOpenProduct.open({
                    product: params['product'],
                    viewContainerRef: this.viewContainerRef,
                });
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
