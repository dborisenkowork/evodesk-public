import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import {ShopProductSlug} from '../../../../app/EvodeskRESTApi/models/ShopProduct.model';
import {ShopCategoryModel} from '../../../../app/EvodeskRESTApi/models/ShopCategory.model';

import {EvodeskShopStateService, ShopAction, ShopLayoutVariant, ShopRoute} from '../../state/EvodeskShopState.service';

interface State {
    ready: boolean;
    product?: ShopProductSlug;
    isMobile: boolean;
}

@Component({
    templateUrl: './ProductRoute.component.pug',
    styleUrls: [
        './ProductRoute.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        isMobile: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private activatedRoute: ActivatedRoute,
        private shopState: EvodeskShopStateService,
    ) {}

    ngOnInit(): void {
        this.shopState.dispatch({
            type: ShopAction.SetRoute,
            payload: {
                route: ShopRoute.Catalog,
            },
        });

        this.shopState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.layoutVariant),
            distinctUntilChanged(),
        ).subscribe((layoutVariant) => {
            this.state = {
                ...this.state,
                isMobile: !!~[
                    ShopLayoutVariant.Mobile,
                ].indexOf(layoutVariant),
            };

            this.cdr.detectChanges();
        });

        this.activatedRoute.params.pipe(takeUntil(this.ngOnDestroy$)).subscribe((params) => {
            if (params['category']) {
                const category: ShopCategoryModel = this.shopState.snapshot.categories.filter(c => c.slug === params['category'])[0];

                if (category) {
                    this.shopState.dispatch({
                        type: ShopAction.SetCurrentCategory,
                        payload: {
                            category: category,
                        },
                    });
                }
            }

            if (params['product']) {
                this.state = {
                    ...this.state,
                    ready: true,
                    product: params['product'],
                };
            }

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
