import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {EvodeskShopStateService, ShopAction, ShopLayoutVariant} from '../../state/EvodeskShopState.service';
import {EvodeskShopBootstrapService} from '../../state/EvodeskShopBootstrap.service';
import {EvodeskShopBreakpointsService} from '../../services/EvodeskShopBreakpoints.service';
import {EvodeskShopUrlService} from '../../services/EvodeskShopUrl.service';
import {EvodeskShopOpenProductService} from '../../services/EvodeskShopOpenProduct.service';
import {EvodeskShopBodyParserService} from '../../services/EvodeskShopBodyParser.service';
import {EvodeskAppGlobalScrollService} from '../../../../app/EvodeskApp/services/EvodeskAppGlobalScroll.service';

interface State {
    ready: boolean;
    layoutVariant: ShopLayoutVariant;
}

@Component({
    templateUrl: './IndexRoute.component.pug',
    styleUrls: [
        './IndexRoute.component.scss',
    ],
    providers: [
        EvodeskShopStateService,
        EvodeskShopBootstrapService,
        EvodeskShopBreakpointsService,
        EvodeskShopUrlService,
        EvodeskShopOpenProductService,
        EvodeskShopBodyParserService,
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndexRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        layoutVariant: ShopLayoutVariant.Mobile,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private appGlobalScroll: EvodeskAppGlobalScrollService,
        private shopState: EvodeskShopStateService,
        private bootstrap: EvodeskShopBootstrapService,
        private breakpoints: EvodeskShopBreakpointsService,
    ) {}

    ngOnInit(): void {
        this.breakpoints.init();

        this.bootstrap.bootstrap().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.state = {
                ...this.state,
                ready: true,
            };

            this.cdr.markForCheck();
        });

        this.breakpoints.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((layoutVariant) => {
            this.shopState.dispatch({
                type: ShopAction.SetLayoutVariant,
                payload: {
                    layoutVariant: layoutVariant,
                },
            });
        });

        this.shopState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !! s.current),
            map(s => s.current.layoutVariant),
            distinctUntilChanged(),
        ).subscribe((layoutVariant) => {
            this.state = {
                ...this.state,
                layoutVariant: layoutVariant,
            };

            this.cdr.markForCheck();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onScroll($event: Event): void {
        this.appGlobalScroll.emit($event);
    }

    get containerNgClasses(): any {
        return {
            '__desktop': this.state.layoutVariant === ShopLayoutVariant.Desktop,
            '__mobile': this.state.layoutVariant === ShopLayoutVariant.Mobile,
        };
    }

    get shouldDisplayMobileNavigation(): boolean {
        return !!~[
            ShopLayoutVariant.Mobile,
        ].indexOf(this.state.layoutVariant);
    }

    get shouldDisplayDesktopNavigation(): boolean {
        return !!~[
            ShopLayoutVariant.Desktop,
        ].indexOf(this.state.layoutVariant);
    }
}

