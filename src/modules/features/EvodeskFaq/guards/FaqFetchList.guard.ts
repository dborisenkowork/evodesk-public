import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {FaqService} from '../services/Faq.service';


@Injectable()
export class FaqFetchListGuard implements CanActivate
{
    constructor(
        private service: FaqService,
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return Observable.create(response => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            this.service.fetch().pipe(takeUntil(unsubscribe$)).subscribe(
                () => {
                    response.next(true);
                    response.complete();
                },
                () => {
                    response.error();
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
