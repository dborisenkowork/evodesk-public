import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskFaqRoutingModule} from './EvodeskFaqRouting.module';

import {FaqService} from './services/Faq.service';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';

import {FAQComponent} from './components/FAQ/FAQ.component';
import {FAQContainerComponent} from './components/FAQ/FAQContainer.component';
import {FAQGroupComponent} from './components/FAQGroup/FAQGroup.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskFaqRoutingModule,
    ],
    declarations: [
        IndexRouteComponent,

        FAQComponent,
        FAQContainerComponent,
        FAQGroupComponent,
    ],
    providers: [
        FaqService,
    ],
})
export class EvodeskFaqModule
{}
