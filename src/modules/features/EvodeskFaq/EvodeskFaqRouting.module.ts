import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskFaqRouting} from './configs/EvodeskFaqRouting.module';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskFaqRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskFaqRoutingModule
{}
