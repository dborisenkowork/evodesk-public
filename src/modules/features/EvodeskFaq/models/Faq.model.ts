export interface FaqGroup {
    title: string;
}

export interface FaqEntry {
    id: number;
    name: string;
    question: string;
    answer: string;
}
