import {Injectable} from '@angular/core';

import {Subject, Observable, BehaviorSubject} from 'rxjs';
import {publishLast, refCount, takeUntil} from 'rxjs/operators';

import {FaqEntry, FaqGroup} from '../models/Faq.model';

import {FaqRESTService} from '../../../app/EvodeskRESTApi/services/FaqREST.service';

export interface CurrentFAQList {
    groups: Array<FaqGroup>;
    entries: Array<FaqEntry>;
}

@Injectable()
export class FaqService
{
    private _list$: BehaviorSubject<CurrentFAQList> = new BehaviorSubject<CurrentFAQList | undefined>(undefined);

    constructor(
        private rest: FaqRESTService,
    ) {}

    get list$(): Observable<CurrentFAQList> {
        return this._list$.asObservable();
    }

    fetch(): Observable<void> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            if (this._list$.getValue() === undefined) {
                this.rest.faqList().pipe(takeUntil(unsubscribe$)).subscribe(
                    response => {
                        this._list$.next({
                            groups: response.groups,
                            entries: response.entries,
                        });

                        observer.next(undefined);
                        observer.complete();
                    },
                    error => {
                        observer.error(error);
                    },
                );
            } else {
                observer.next(undefined);
                observer.complete();
            }

            return () => {
                unsubscribe$.next(undefined);
            };
        }).pipe(publishLast(), refCount());
    }
}
