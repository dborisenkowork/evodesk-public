import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

import {FaqService} from '../../services/Faq.service';

import {FaqEntry} from '../../models/Faq.model';

export interface FAQComponentProps {
    groupNames: Array<string>;
    entries: Array<Array<FaqEntry>>;
}

@Component({
    selector: 'evodesk-faq',
    templateUrl: './FAQ.component.pug',
    styleUrls: [
        './FAQ.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        FaqService,
    ],
})
export class FAQComponent
{
    @Input() props: FAQComponentProps;

    @Output('modalOpen') modalOpenEvent: EventEmitter<FaqEntry> = new EventEmitter<FaqEntry>();
}
