import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil, filter} from 'rxjs/operators';

import {FAQComponentProps} from './FAQ.component';

import {FaqService} from '../../services/Faq.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';

import {FaqEntry} from '../../models/Faq.model';

type State = {
    ready: boolean;
    props: FAQComponentProps,
};

@Component({
    selector: 'evodesk-faq-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-faq [props]="state.props" (modalOpen)="onModalOpen($event)"></evodesk-faq>
      </ng-container>
    `,
})
export class FAQContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: {
            entries: [],
            groupNames: [],
        },
    };

    constructor(
        private service: FaqService,
        private uiAlertService: EvodeskAlertModalService,
    ) {}

    ngOnInit(): void {
        this.service.fetch().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.service.list$.pipe(takeUntil(this.ngOnDestroy$), filter(n => !!n)).subscribe(next => {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        groupNames: next.groups.map(group => group.title),
                        entries: next.groups.map(group => {
                            return next.entries.filter(ee => ee.name === group.title);
                        }),
                    },
                };
            });
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onModalOpen(entry: FaqEntry): void {
        this.uiAlertService.open({
            title: {
                text: entry.name,
                translate: false,
            },
            text: {
                text: entry.answer,
                translate: false,
                asHtml: true,
            },
            ok: {
                text: 'EvodeskFaq.components.FAQ.Close',
                translate: true,
            },
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }
}
