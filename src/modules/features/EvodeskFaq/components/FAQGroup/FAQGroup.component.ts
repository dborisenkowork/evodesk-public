import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

import {FaqEntry} from '../../models/Faq.model';

@Component({
    selector: 'evodesk-faq-group',
    templateUrl: './FAQGroup.component.pug',
    styleUrls: [
        './FAQGroup.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FAQGroupComponent
{
    @Output('faqEntryClick') faqEntryClickEvent: EventEmitter<FaqEntry> = new EventEmitter<FaqEntry>();

    @Input() groupName: string;
    @Input() entries: Array<FaqEntry>;

    open(entry: FaqEntry): void {
        this.faqEntryClickEvent.emit(entry);
    }
}
