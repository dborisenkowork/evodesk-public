import {Routes} from '@angular/router';

import {HasAuthTokenGuard} from '../../EvodeskAuth/guards/HasAuthToken.guard';
import {CurrentUserGuard} from '../../EvodeskAuth/guards/CurrentUser.guard';

import {IndexRouteComponent} from '../routes/IndexRoute/IndexRoute.component';

import {EvodeskSetCommonHeaderGuard} from '../../EvodeskHeader/guards/EvodeskSetCommonHeader.guard';

export const EvodeskFaqRouting: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: IndexRouteComponent,
        canActivate: [
            HasAuthTokenGuard,
            CurrentUserGuard,
            EvodeskSetCommonHeaderGuard,
        ],
    },
];
