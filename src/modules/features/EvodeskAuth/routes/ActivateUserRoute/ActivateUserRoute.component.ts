import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {UserRESTService} from '../../../../app/EvodeskRESTApi/services/UserREST.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {AuthTokenService} from '../../services/AuthToken.service';

@Component({
    template: `
        <div class="__evodesk-app-circle-loader_fixed_container">
            <div class="__evodesk-app-circle-loader"></div>
        </div>
    `,
})
export class ActivateUserRouteComponent implements OnInit
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private authToken: AuthTokenService,
        private userRESTService: UserRESTService,
        private uiAlertService: EvodeskAlertModalService,
    ) {}

    ngOnInit(): void {
        this.activatedRoute.queryParams.pipe(
            takeUntil(this.ngOnDestroy$))
            .subscribe(qp => {
                if (qp && qp.email && qp.code) {
                    this.userRESTService.activateUser({ email: qp.email, code: qp.code }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                        () => {
                            this.authToken.clear();

                            this.uiAlertService.open({
                                title: {
                                    text: 'EvodeskAuth.routes.ActivateUserRoute.Title',
                                    translate: true,
                                },
                                text: {
                                    text: 'EvodeskAuth.routes.ActivateUserRoute.Success',
                                    translate: true,
                                },
                                ok: {
                                    text: 'EvodeskAuth.routes.ActivateUserRoute.Ok',
                                    translate: true,
                                },
                            }).subscribe(() => {
                                this.router.navigate(['/']);
                            });
                        },
                        () => {
                            this.uiAlertService.open({
                                title: {
                                    text: 'EvodeskAuth.routes.ActivateUserRoute.Title',
                                    translate: true,
                                },
                                text: {
                                    text: 'EvodeskAuth.routes.ActivateUserRoute.Fail',
                                    translate: true,
                                },
                                ok: {
                                    text: 'EvodeskAuth.routes.ActivateUserRoute.Ok',
                                    translate: true,
                                },
                            }).subscribe(() => {
                                this.router.navigate(['/']);
                            });
                        },
                    );
                } else {
                    this.router.navigate(['/']);
                }
            });
    }
}
