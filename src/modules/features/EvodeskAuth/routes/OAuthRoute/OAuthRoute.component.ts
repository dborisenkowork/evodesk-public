import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {take} from 'rxjs/operators';

import {AuthTokenService} from '../../services/AuthToken.service';

@Component({
    template: ``,
})
export class OAuthRouteComponent implements OnInit
{
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private authToken: AuthTokenService,
    ) {}

    ngOnInit(): void {
        this.activatedRoute.queryParams.pipe(take(1)).subscribe((qp) => {
            const id: number = parseInt(qp['user[id]'], 10);
            const accessToken: string = qp['access_token'];
            const refreshToken: string = qp['refresh_token'];

            if (id && accessToken && refreshToken) {
                this.authToken.setToken({
                    id: id,
                    access_token: accessToken,
                    refresh_token: refreshToken,
                    persist: true,
                });
            }

            this.router.navigate(['/']);
        });
    }
}
