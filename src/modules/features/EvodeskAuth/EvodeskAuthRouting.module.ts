import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskAuthRouting} from './configs/EvodeskAuthRouting.module';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskAuthRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskAuthRoutingModule
{}
