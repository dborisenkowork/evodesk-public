import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable} from 'rxjs';
import {filter} from 'rxjs/operators';

import {UserModel} from '../../../app/EvodeskRESTApi/models/user/User.model';
import {AppState, EvodeskAppStateService} from '../../../app/EvodeskAppState/services/EvodeskAppState.service';
import {ProfileModel} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';

export type User$ = UserModel | undefined;

@Injectable()
export class CurrentUserService
{
    private _current$: BehaviorSubject<User$> = new BehaviorSubject<User$>(undefined);

    constructor(
        private appState: EvodeskAppStateService,
    ) {
        this._current$.subscribe(next => {
            this.appState.setState((orig: AppState) => {
                return {
                    ...orig,
                    currentUser: !!next ? next : undefined,
                    currentUserBalance: !!next ? {
                        points: parseInt(<any>next.balance, 10),
                        evocoin: parseFloat(parseFloat(<any>next.balanceEvocoin).toFixed(2)),
                        rub: parseFloat(parseFloat(<any>next.balanceRub).toFixed(2)),
                    } : undefined,
                };
            });
        });
    }

    get current$(): Observable<UserModel> {
        return this._current$.pipe(filter(u => u !== undefined));
    }

    get current(): UserModel {
        return this._current$.getValue();
    }

    get impersonatedAs(): ProfileModel {
        return this._current$.getValue();
    }

    set(asCurrent: UserModel): void {
        this._current$.next(asCurrent);
    }

    clear(): void {
        this._current$.next(undefined);
    }

    has(): boolean {
        return !! this.current;
    }
}
