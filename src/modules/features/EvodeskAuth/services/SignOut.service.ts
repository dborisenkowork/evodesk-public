import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

import {AppEvent} from '../../../app/EvodeskAppMessageBus/models/app-events.model';

import {EvodeskAppMessageBusService} from '../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';
import {AuthTokenService} from './AuthToken.service';
import {CurrentUserService} from './CurrentUser.service';
import {UserRESTService} from '../../../app/EvodeskRESTApi/services/UserREST.service';

@Injectable()
export class SignOutService
{
    constructor(
        private router: Router,
        private appMessageBusService: EvodeskAppMessageBusService,
        private authTokenService: AuthTokenService,
        private currentUserService: CurrentUserService,
        private userRESTService: UserRESTService,
    ) {}

    signOut(): void {
        this.appMessageBusService.dispatch({ type: AppEvent.SignOut });
        this.authTokenService.clear();
        this.currentUserService.clear();
        this.userRESTService.signOut().subscribe();

        this.router.navigate(['/']);
    }
}
