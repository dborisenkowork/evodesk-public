import {Injectable, NgZone} from '@angular/core';
import {Router} from '@angular/router';

import {LocalStorageService} from 'angular-2-local-storage';

import {interval, Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {UserRESTService} from '../../../app/EvodeskRESTApi/services/UserREST.service';

import {UserId} from '../../../app/EvodeskRESTApi/models/user/User.model';

let signInThisSession: boolean = false;

export interface AuthToken
{
    id: UserId;
    persist: boolean;
    access_token: string;
    refresh_token: string;
    last_updated_on?: number | undefined;
}

interface SetTokenOptions {
    update?: boolean;
    persist?: boolean;
    refresh?: boolean;
    markSignedInFlag?: boolean;
}

@Injectable()
export class AuthTokenService
{
    static LOCAL_STORAGE_KEY: string = 'auth-token';

    private _token: AuthToken;
    private _nextRefreshingToken$: Subject<void> = new Subject<void>();
    private _stopRefreshingToken$: Subject<void> = new Subject<void>();

    constructor(
        private ngZone: NgZone,
        private localStorage: LocalStorageService,
        private userRESTService: UserRESTService,
        private router: Router,
    ) {}

    bootstrapFromLocalStorage(): void {
        if (this.localStorage.get(AuthTokenService.LOCAL_STORAGE_KEY)) {
            const token: AuthToken = JSON.parse(this.localStorage.get(AuthTokenService.LOCAL_STORAGE_KEY));

            this.setToken(token, {
                persist: token.persist,
                refresh: true,
            });
        }
    }

    get token(): AuthToken {
        return { ...this._token };
    }

    get canBeOutdated(): boolean {
        return ! signInThisSession;
    }

    has(): boolean {
        return this._token !== undefined;
    }

    clear(): void {
        signInThisSession = false;

        if (this.token.persist) {
            this.stopRefreshingToken();
        }

        this._token = undefined;
        this.localStorage.remove(AuthTokenService.LOCAL_STORAGE_KEY);
    }

    setToken(token: AuthToken, options: SetTokenOptions = {}): void {
        this._token = {
            ...token,
            last_updated_on: (new Date()).getTime(),
        };

        this.localStorage.set(AuthTokenService.LOCAL_STORAGE_KEY, JSON.stringify({
            ...token,
            last_updated_on: (new Date()).getTime(),
        }));

        if (options.markSignedInFlag) {
            signInThisSession = true;
        }

        if (options.persist && ! options.update) {
            this.startRefreshingToken();
        }

        if (options.persist && options.refresh) {
            this.refreshToken().pipe(takeUntil(this._nextRefreshingToken$)).subscribe(() => {
                this.startRefreshingToken();
            });
        }
    }

    refreshToken(): Observable<void> {
        return Observable.create(o => {
            if (this.has()) {
                this.userRESTService.refreshToken(this.token.refresh_token).pipe(takeUntil(this._nextRefreshingToken$)).subscribe(
                    (httpResponse) => {
                        if (this.localStorage.get(AuthTokenService.LOCAL_STORAGE_KEY)) {
                            this.setToken({
                                ...this.token,
                                access_token: httpResponse.access_token,
                                refresh_token: httpResponse.refresh_token,
                                last_updated_on: (new Date()).getTime(),
                            }, { update: true });

                            o.next(undefined);
                            o.complete();
                        }
                    },
                    (error) => {
                        this.clear();
                        this.router.navigate(['/']);

                        o.next(undefined);
                        o.complete();
                    },
                );
            }
        });
    }

    startRefreshingToken(): void {
        this.ngZone.runOutsideAngular(() => {
            interval(1000 /* ms */ * 30 /* seconds */).pipe(takeUntil(this._stopRefreshingToken$)).subscribe(() => {
                if (! this.localStorage.get(AuthTokenService.LOCAL_STORAGE_KEY)) {
                    this._stopRefreshingToken$.next(undefined);
                } else {
                    if (! this.token.last_updated_on || ((new Date).getTime() - this.token.last_updated_on > 1000 /*ms */  * 60 /* seconds */ * 15 /* minutes */)) {
                        this._nextRefreshingToken$.next(undefined);

                        this.refreshToken().subscribe();
                    }
                }
            });
        });
    }

    stopRefreshingToken(): void {
        this._nextRefreshingToken$.next(undefined);
        this._stopRefreshingToken$.next(undefined);
    }
}
