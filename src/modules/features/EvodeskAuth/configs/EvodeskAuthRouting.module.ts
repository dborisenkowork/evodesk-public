import {Routes} from '@angular/router';

import {ActivateUserRouteComponent} from '../routes/ActivateUserRoute/ActivateUserRoute.component';
import {OAuthRouteComponent} from '../routes/OAuthRoute/OAuthRoute.component';

export const EvodeskAuthRouting: Routes = [
    {
        path: 'login',
        pathMatch: 'full',
        component: ActivateUserRouteComponent,
    },
    {

        path: 'oauth',
        pathMatch: 'full',
        component: OAuthRouteComponent,
    },
];
