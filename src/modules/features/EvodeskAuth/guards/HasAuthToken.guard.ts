import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

import {of as observableOf, Observable} from 'rxjs';

import {AuthTokenService} from '../services/AuthToken.service';

@Injectable()
export class HasAuthTokenGuard implements CanActivate
{
    constructor(
        private router: Router,
        private authToken: AuthTokenService,
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.authToken.has()) {
            if (this.authToken.canBeOutdated) {
                return Observable.create(o => {
                    if (this.authToken.token.persist) {
                        this.authToken.stopRefreshingToken();
                    }

                    this.authToken.refreshToken().subscribe(
                        () => {
                            if (this.authToken.token.persist) {
                                this.authToken.startRefreshingToken();
                            }

                            o.next(true);
                            o.complete();
                        },
                        () => {
                            o.next(false);
                            o.complete();
                        },
                    );
                });
            } else {
                return observableOf(true);
            }
        } else {
            this.router.navigate(['/']);

            return observableOf(false);
        }
    }
}
