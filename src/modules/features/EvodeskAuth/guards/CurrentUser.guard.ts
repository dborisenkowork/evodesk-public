import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

import {Observable} from 'rxjs';

import {UserRESTService} from '../../../app/EvodeskRESTApi/services/UserREST.service';
import {CurrentUserService} from '../services/CurrentUser.service';
import {AuthTokenService} from '../services/AuthToken.service';

@Injectable()
export class CurrentUserGuard implements CanActivate
{
    constructor(
        private router: Router,
        private authToken: AuthTokenService,
        private currentUser: CurrentUserService,
        private userRESTService: UserRESTService,
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return Observable.create(observer => {
            if (this.currentUser.has()) {
                observer.next(true);
                observer.complete();
            } else {
                this.userRESTService.current()
                    .subscribe(
                        next => {
                            if (!! next) {
                                this.currentUser.set(next);

                                observer.next(true);
                                observer.complete();
                            } else {
                                this.authToken.clear();
                                this.currentUser.clear();

                                this.router.navigate(['/']);

                                observer.next(false);
                                observer.complete();
                            }
                        },
                        () => {
                            this.authToken.clear();
                            this.currentUser.clear();

                            this.router.navigate(['/']);

                            observer.next(false);
                            observer.complete();
                        },
                    );
            }
        });
    }
}
