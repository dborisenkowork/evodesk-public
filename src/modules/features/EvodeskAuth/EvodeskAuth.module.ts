import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskAuthRoutingModule} from './EvodeskAuthRouting.module';

import {ActivateUserRouteComponent} from './routes/ActivateUserRoute/ActivateUserRoute.component';
import {OAuthRouteComponent} from './routes/OAuthRoute/OAuthRoute.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskAuthRoutingModule,
    ],
    declarations: [
        ActivateUserRouteComponent,
        OAuthRouteComponent,
    ],
})
export class EvodeskAuthModule {
}
