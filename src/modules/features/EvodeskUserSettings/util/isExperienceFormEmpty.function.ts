import {EvodeskUserSettingsWorkExperienceComponentFormValue} from '../components/EvodeskUserSettingsWorkExperience/EvodeskUserSettingsWorkExperience.component';

export function isExperienceFormEmpty(input: EvodeskUserSettingsWorkExperienceComponentFormValue): boolean {
    return ! input.place && ! input.position && ! input.date_finish && ! input.date_finish && !input.site && ! input.text;
}
