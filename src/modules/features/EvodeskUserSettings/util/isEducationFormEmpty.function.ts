import {EvodeskUserSettingsEducationComponentFormValue} from '../components/EvodeskUserSettingsEducation/EvodeskUserSettingsEducation.component';

export function isEducationFormEmpty(input: EvodeskUserSettingsEducationComponentFormValue): boolean {
    return ! input.name && ! input.city && ! input.specialty && ! input.type && ! input.year;
}
