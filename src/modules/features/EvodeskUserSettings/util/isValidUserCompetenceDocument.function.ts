export function isValidUserCompetenceDocument(mimeType: string) {
    return !!~[
        'image/jpeg',
    ].indexOf(mimeType);
}
