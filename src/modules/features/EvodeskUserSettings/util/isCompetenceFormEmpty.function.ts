import {EvodeskUserSettingsCompetenceComponentFormValue} from '../components/EvodeskUserSettingsCompetence/EvodeskUserSettingsCompetence.component';

export function isCompetenceFormEmpty(input: EvodeskUserSettingsCompetenceComponentFormValue): boolean {
    return ! input.competenceCategoryId && ! input.competenceId;
}
