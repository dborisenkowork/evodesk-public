import {Injectable} from '@angular/core';
import {FormBuilder} from '@angular/forms';

import {takeUntil} from 'rxjs/operators';
import {forkJoin as observableForkJoin, Subject, Observable} from 'rxjs';

import * as moment from 'moment';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../types/Backend.types';

import {forkJoin} from 'rxjs';

import {ProfileModel} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {UserCompetenceModelId} from '../../../app/EvodeskRESTApi/models/UserCompetence.model';
import {UserCompetenceDocumentModel} from '../../../app/EvodeskRESTApi/models/UserCompetenceDocument.model';

import {defaultUserActivityPageSize} from '../components/EvodeskUserSettingsActivity/EvodeskUserSettingsActivityContainer.component';

import {EvodeskUserSettingsStateAction, EvodeskUserSettingsStateBootstrapQuery, EvodeskUserSettingsStateService} from './EvodeskUserSettingsState.service';
import {ProfileRESTService} from '../../../app/EvodeskRESTApi/services/ProfileREST.service';
import {CurrentUserService} from '../../EvodeskAuth/services/CurrentUser.service';
import {IndustryRESTService} from '../../../app/EvodeskRESTApi/services/IndustryREST.service';
import {ProfileEducationRESTService} from '../../../app/EvodeskRESTApi/services/ProfileEducationREST.service';
import {ProfileWorkExperienceRESTService} from '../../../app/EvodeskRESTApi/services/ProfileWorkExperienceREST.service';
import {EvodeskUserSettingsValidatorsFactoryService} from './EvodeskUserSettingsValidatorsFactory.service';
import {UserRESTService} from '../../../app/EvodeskRESTApi/services/UserREST.service';
import {CompetenceRESTService} from '../../../app/EvodeskRESTApi/services/CompetenceREST.service';
import {NotificationSettingsRESTService} from '../../../app/EvodeskRESTApi/services/NotificationSettingsREST.service';

import {evodeskUserSettingsCommonComponentFormBuilder} from '../components/EvodeskUserSettingsCommon/EvodeskUserSettingsCommon.component';
import {evodeskUserSettingsPasswordComponentFormBuilder} from '../components/EvodeskUserSettingsPassword/EvodeskUserSettingsPassword.component';
import {evodeskUserSettingsEducationComponentFormBuilder} from '../components/EvodeskUserSettingsEducation/EvodeskUserSettingsEducation.component';
import {evodeskUserSettingsWorkExperienceComponentFormBuilder} from '../components/EvodeskUserSettingsWorkExperience/EvodeskUserSettingsWorkExperience.component';
import {evodeskUserSettingsAccountComponentFormBuilder} from '../components/EvodeskUserSettingsAccount/EvodeskUserSettingsAccount.component';
import {evodeskUserSettingsPrivacySecurityComponentFormBuilder} from '../components/EvodeskUserSettingsPrivacySecurity/EvodeskUserSettingsPrivacySecurity.component';
import {evodeskUserSettingsCompetenceComponentFormBuilder} from '../components/EvodeskUserSettingsCompetence/EvodeskUserSettingsCompetence.component';
import {evodeskUserSettingsNotificationsComponentFormBuilder} from '../components/EvodeskUserSettingsNotifications/EvodeskUserSettingsNotifications.component';

@Injectable()
export class EvodeskUserSettingsBootstrapService
{
    constructor(
        private fb: FormBuilder,
        private state: EvodeskUserSettingsStateService,
        private currentUserService: CurrentUserService,
        private validatorsFactory: EvodeskUserSettingsValidatorsFactoryService,
        private profileRESTService: ProfileRESTService,
        private profileEducationRESTService: ProfileEducationRESTService,
        private profileExperienceRESTService: ProfileWorkExperienceRESTService,
        private industryRESTService: IndustryRESTService,
        private userRESTService: UserRESTService,
        private competenceRESTService: CompetenceRESTService,
        private notificationsRESTService: NotificationSettingsRESTService,
    ) {}

    bootstrap(): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((done) => {
            const queries: Array<Observable<any>> = [];

            const profile: ProfileModel = this.currentUserService.current;
            const profileId: number = profile.id;

            const bootstrapQuery: EvodeskUserSettingsStateBootstrapQuery = <any>{};

            bootstrapQuery.formCommon = evodeskUserSettingsCommonComponentFormBuilder(this.fb);
            bootstrapQuery.formPassword = evodeskUserSettingsPasswordComponentFormBuilder(this.fb);
            bootstrapQuery.formAccount = evodeskUserSettingsAccountComponentFormBuilder(this.fb, this.validatorsFactory, {
                initialValue: {
                    profileId: profile.id,
                    urlAlias: profile.url_alias,
                },
            });
            bootstrapQuery.formPrivacyAndSecurity = evodeskUserSettingsPrivacySecurityComponentFormBuilder(this.fb, {
                initialValue: {
                    pageVisibility: profile.page_visibality,
                    photoVisibility: profile.photo_visibality,
                    subscribeLevel: profile.subscribe_level,
                },
            });

            queries.push(Observable.create(q => {
                this.profileRESTService.getProfile(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.profile = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.industryRESTService.getIndustries().pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.availableIndustries = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.profileRESTService.getAvailableProfileLegalStatus().pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.availableLegalStatuses = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.competenceRESTService.getCompetenceCategories().pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.availableCompetenceCategories = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.competenceRESTService.getCompetenceOfProfile(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        const userCompetenceIds: Array<UserCompetenceModelId> = httpResponse.map(c => c.id);

                        const skills: { [userCompetenceId: number]: Array<string> } = {};
                        const documents: { [userCompetenceId: number]: Array<UserCompetenceDocumentModel> } = {};

                        const doBootstrap: Function = (() => {
                            bootstrapQuery.formsCompetence = (() => {
                                if (httpResponse.length > 0) {
                                    httpResponse.sort((a, b) => {
                                        if (a.id > b.id) {
                                            return 1;
                                        } else if (a.id === b.id) {
                                            return 0;
                                        } else {
                                            return -1;
                                        }
                                    });

                                    return httpResponse.map((c) => {
                                        return evodeskUserSettingsCompetenceComponentFormBuilder(this.fb, {
                                            withUserCompetence: c,
                                            withSkills: skills[c.id] || [],
                                            withDocuments: documents[c.id] || [],
                                        });
                                    });
                                } else {
                                    return [];
                                }
                            })();

                            q.next(undefined);
                            q.complete();
                        });

                        if (userCompetenceIds.length) {
                            forkJoin(
                                ...userCompetenceIds.map(ucId => Observable.create((s) => {
                                    this.competenceRESTService.getSkillsOfCurrentProfile(ucId).subscribe(
                                        (hResponse) => {
                                            skills[ucId] = hResponse.map(e => e.skill);

                                            s.next(undefined);
                                            s.complete();
                                        },
                                        (hError) => {
                                            s.error(hError);
                                        },
                                    );
                                })),
                                ...userCompetenceIds.map(ucId => Observable.create((s) => {
                                    this.competenceRESTService.getDocumentsOfCurrentProfile(ucId).subscribe(
                                        (hResponse) => {
                                            documents[ucId] = hResponse;

                                            s.next(undefined);
                                            s.complete();
                                        },
                                        (hError) => {
                                            s.error(hError);
                                        },
                                    );
                                })),
                            ).subscribe(() => {
                                doBootstrap();
                            });
                        } else {
                            doBootstrap();
                        }
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.profileEducationRESTService.getEducation(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.formsEducation = (() => {
                            if (httpResponse.length > 0) {
                                httpResponse.sort((a, b) => {
                                    if (a.id > b.id) {
                                        return 1;
                                    } else if (a.id === b.id) {
                                        return 0;
                                    } else {
                                        return -1;
                                    }
                                });

                                return httpResponse.map((e) => {
                                    return evodeskUserSettingsEducationComponentFormBuilder(this.fb, { education: e });
                                });
                            } else {
                                return [
                                    evodeskUserSettingsEducationComponentFormBuilder(this.fb),
                                ];
                            }
                        })();

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.profileExperienceRESTService.getExperienceOfProfile(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.formsExperience = (() => {
                            if (httpResponse.length > 0) {
                                httpResponse.sort((a, b) => {
                                    if (a.id > b.id) {
                                        return 1;
                                    } else if (a.id === b.id) {
                                        return 0;
                                    } else {
                                        return -1;
                                    }
                                });

                                return httpResponse.map((e) => {
                                    return evodeskUserSettingsWorkExperienceComponentFormBuilder(this.fb, { experience: e });
                                });
                            } else {
                                return [
                                    evodeskUserSettingsWorkExperienceComponentFormBuilder(this.fb),
                                ];
                            }
                        })();

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.userRESTService.getActivity({ offset: 0, limit: defaultUserActivityPageSize + 1 }).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.userActivities = httpResponse.slice(0, defaultUserActivityPageSize);
                        bootstrapQuery.hasMoreUserActivitiesToLoad = httpResponse.length > defaultUserActivityPageSize;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.notificationsRESTService.getSettings().pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.formsNotifications = httpResponse.map(settings => evodeskUserSettingsNotificationsComponentFormBuilder(this.fb, {
                            settings: settings,
                        }));

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            observableForkJoin(queries).pipe(takeUntil(unsubscribe$)).subscribe(
                () => {
                    const names: Array<string> = (bootstrapQuery.profile.name || '').split(' ');

                    bootstrapQuery.formCommon.patchValue({
                        firstName: names[1] || '',
                        lastName: names[0] || '',
                        middleName: names[2] || '',
                        birthday: bootstrapQuery.profile.birth_date ? moment(bootstrapQuery.profile.birth_date, BACKEND_DATE_FORMAT_AS_MOMENT).toDate() : null,
                        address: bootstrapQuery.profile.address,
                        industry: bootstrapQuery.profile.industry,
                        status: bootstrapQuery.profile.position,
                        phone: bootstrapQuery.profile.phone,
                    });

                    (() => {
                        if (bootstrapQuery.profile.phone) {
                            const phone: string = '+' + bootstrapQuery.profile.phone.toString().replace(/[^\d]/g, '');

                            if (phone) {
                                bootstrapQuery.formCommon.patchValue({
                                    phone: phone.slice(phone.length - 10),
                                    phoneCode: phone.slice(0, phone.length - 10),
                                });
                            } else {
                                bootstrapQuery.formCommon.patchValue({
                                    phone: '',
                                    phoneCode: '+7',
                                });
                            }
                        }
                    })();

                    this.state.dispatch({
                        type: EvodeskUserSettingsStateAction.Bootstrap,
                        payload: bootstrapQuery,
                    });

                    done.next(undefined);
                    done.complete();
                },
                (queryError) => {
                    done.error(queryError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
