import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {FormGroup} from '@angular/forms';

import {of as observableOf, forkJoin, Subject, Observable, concat} from 'rxjs';
import {publishLast, refCount, takeUntil, tap} from 'rxjs/operators';

import * as moment from 'moment';

import {environment} from '../../../../environments/environment.config';
import {ENVIRONMENT} from '../../../../environments/environments';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../types/Backend.types';

import {proxy} from '../../../../functions/proxy.function';

import {isEducationFormEmpty} from '../util/isEducationFormEmpty.function';
import {isExperienceFormEmpty} from '../util/isExperienceFormEmpty.function';
import {isCompetenceFormEmpty} from '../util/isCompetenceFormEmpty.function';

import {ProfileModel} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {ProfileExperienceJSON} from '../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';
import {ProfileEducationModel} from '../../../app/EvodeskRESTApi/models/profile/ProfileEducation.model';
import {UserCompetenceDocumentModel} from '../../../app/EvodeskRESTApi/models/UserCompetenceDocument.model';
import {CompetenceId} from '../../../app/EvodeskRESTApi/models/Competence.model';

import {EvodeskUserSettingsEducationComponentFormValue} from '../components/EvodeskUserSettingsEducation/EvodeskUserSettingsEducation.component';

import {EvodeskUserSettingsCommonComponentFormValue} from '../components/EvodeskUserSettingsCommon/EvodeskUserSettingsCommon.component';
import {EvodeskUserSettingsPasswordComponentFormValue} from '../components/EvodeskUserSettingsPassword/EvodeskUserSettingsPassword.component';
import {ApproveRequest, EvodeskUserSettingsWorkExperienceComponentFormValue} from '../components/EvodeskUserSettingsWorkExperience/EvodeskUserSettingsWorkExperience.component';
import {EvodeskUserSettingsAccountComponentFormValue} from '../components/EvodeskUserSettingsAccount/EvodeskUserSettingsAccount.component';
import {EvodeskUserSettingsPrivacySecurityComponentFormValue} from '../components/EvodeskUserSettingsPrivacySecurity/EvodeskUserSettingsPrivacySecurity.component';
import {EvodeskUserSettingsNotificationsComponentFormValue} from '../components/EvodeskUserSettingsNotifications/EvodeskUserSettingsNotifications.component';

import {EvodeskAlertModalService} from '../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {EvodeskUserSettingsState, EvodeskUserSettingsStateAction, EvodeskUserSettingsStateService} from './EvodeskUserSettingsState.service';
import {ProfileRESTService} from '../../../app/EvodeskRESTApi/services/ProfileREST.service';
import {ProfileEducationRESTService} from '../../../app/EvodeskRESTApi/services/ProfileEducationREST.service';
import {ProfileWorkExperienceRESTService} from '../../../app/EvodeskRESTApi/services/ProfileWorkExperienceREST.service';
import {EvodeskUserSettingsCompetenceComponentFormValue} from '../components/EvodeskUserSettingsCompetence/EvodeskUserSettingsCompetence.component';
import {SyncUserCompetencesRequestEntry} from '../../../app/EvodeskRESTApi/services/CompetenceREST.service';
import {SyncCompetenceScript, SyncUserCompetenceResponse} from '../../../app/EvodeskRESTApi/scripts/SyncCompetence.script';
import {NotificationSettingsRESTService, SetNotificationEntry} from '../../../app/EvodeskRESTApi/services/NotificationSettingsREST.service';

export enum SaveChangesErrorType {
    HttpError = 'http-error',
    Unknown = 'unknown',
    InvalidForm = 'invalid-form',
    FormIsNotValid = 'not-valid',
}

export type SaveChangesError =
      { type: SaveChangesErrorType.InvalidForm }
    | { type: SaveChangesErrorType.FormIsNotValid }
    | { type: SaveChangesErrorType.HttpError, httpError: HttpErrorResponse }
    | { type: SaveChangesErrorType.Unknown };

const DEV_SHOW_ERRORS: boolean = false && [
    ENVIRONMENT.Dev,
].indexOf(environment.env) > -1;

@Injectable()
export class EvodeskUserSaveChangesService
{
    private _saved$: Subject<void> = new Subject<void>();
    private _nextSaveRequest$: Subject<void> = new Subject<void>();

    constructor(
        private uiAlertService: EvodeskAlertModalService,
        private stateService: EvodeskUserSettingsStateService,
        private profileRESTService: ProfileRESTService,
        private profileEducationRESTService: ProfileEducationRESTService,
        private profileExperienceRESTService: ProfileWorkExperienceRESTService,
        private syncCompetenceScript: SyncCompetenceScript,
        private notificationRESTService: NotificationSettingsRESTService,
    ) {}

    private get profileId(): number {
        return this.stateService.snapshot.profile.id;
    }

    get saved$(): Observable<void> {
        return this._saved$.asObservable();
    }

    get canSaveChanges(): boolean {
        const testForms: boolean = [
            this.stateService.snapshot.forms.formCommon,
            this.stateService.snapshot.forms.formPassword,
            this.stateService.snapshot.forms.formAccount,
            this.stateService.snapshot.forms.formPrivacyAndSecurity,
        ].every(f => f.valid || ! f.dirty);

        const testEducationForms: boolean = this.stateService.snapshot.forms.formsEducation.every(f => isEducationFormEmpty(f.value) || f.valid || ! f.dirty);
        const testExperienceForms: boolean = this.stateService.snapshot.forms.formsExperience.every(f => isExperienceFormEmpty(f.value) || f.valid || ! f.dirty);
        const testCompetenceForms: boolean = this.stateService.snapshot.forms.formsCompetence.every(f => isCompetenceFormEmpty(f.value) || f.valid || ! f.dirty);

        const result: boolean = testForms && testEducationForms && testExperienceForms && testCompetenceForms;

        if (DEV_SHOW_ERRORS) {
            if (! result) {
                const formErrors: string = JSON.stringify([
                    this.stateService.snapshot.forms.formCommon,
                    this.stateService.snapshot.forms.formPassword,
                    this.stateService.snapshot.forms.formAccount,
                    this.stateService.snapshot.forms.formPrivacyAndSecurity,
                    ...this.stateService.snapshot.forms.formsEducation,
                    ...this.stateService.snapshot.forms.formsExperience,
                    ...this.stateService.snapshot.forms.formsCompetence,
                    ...this.stateService.snapshot.forms.formsNotifications,
                ].map(f => {
                    return {
                        valid: f.valid,
                        value: f.value,
                        errors: f.errors,
                    };
                }));

                this.uiAlertService.open({
                    title: {
                        text: 'FORM VALIDATION (DEVELOPMENT)',
                        translate: false,
                    },
                    text: {
                        text: formErrors,
                        translate: false,
                    },
                    ok: {
                        text: 'OK',
                        translate: false,
                    },
                }).subscribe();
            }
        }

        return result;
    }

    get canChangePassword(): boolean {
        const formValue: EvodeskUserSettingsPasswordComponentFormValue = this.stateService.snapshot.forms.formPassword.value;

        return this.stateService.snapshot.forms.formPassword.valid
            && formValue.newPassword === formValue.repeatPassword
            && ! this.arePasswordsSame;
    }

    get arePasswordsSame(): boolean {
        const formValue: EvodeskUserSettingsPasswordComponentFormValue = this.stateService.snapshot.forms.formPassword.value;

        return this.stateService.snapshot.forms.formPassword.valid
            && formValue.oldPassword
            && formValue.oldPassword.length > 0
            && formValue.oldPassword === formValue.newPassword;
    }

    hasChanges(): boolean {
        return this.stateService.snapshot.forms.formCommon.dirty
            || this.stateService.snapshot.forms.formAccount.dirty
            || this.stateService.snapshot.forms.formPrivacyAndSecurity.dirty
            || this.stateService.snapshot.forms.formsCompetence.some(f => f.dirty)
            || this.stateService.snapshot.forms.formsExperience.some(f => f.dirty)
            || this.stateService.snapshot.forms.formsEducation.some(f => f.dirty)
            || this.stateService.snapshot.forms.formsNotifications.some(f => f.dirty)
        ;
    }

    saveChanges(): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((done) => {
            this._nextSaveRequest$.next(undefined);

            if (this.canSaveChanges) {
                const queries: Array<Observable<any>> = [];

                queries.push(...[
                    this.saveProfileChanges(),
                    this.saveProfileEducationChanges(),
                    this.saveProfileExperienceChanges(),
                    this.saveProfileCompetenceChanges(),
                    this.saveNotificationSettingsChanges(),
                ]);

                forkJoin(queries).pipe(takeUntil(this._nextSaveRequest$)).subscribe(
                    () => {
                        this._saved$.next(undefined);

                        done.next(undefined);
                        done.complete();
                    },
                    (error) => {
                        this.fail(error);

                        done.error(error);
                    },
                );
            } else {
                this.fail({
                    type: SaveChangesErrorType.FormIsNotValid,
                });

                done.error(<SaveChangesError>{
                    type: SaveChangesErrorType.FormIsNotValid,
                });
            }

            return () => {
                unsubscribe$.next(undefined);
            };
        }).pipe(publishLast(), refCount());
    }

    savePasswordChanges(): Observable<boolean> {
        if (this.canChangePassword) {
            const unsubscribe$: Subject<void> = new Subject<void>();

            return Observable.create((done) => {
                const formValue: EvodeskUserSettingsPasswordComponentFormValue = this.stateService.snapshot.forms.formPassword.value;

                const alert: Function = (modal: string) => {
                    this.uiAlertService.openTranslated(`EvodeskUserSettings.services.EvodeskUserSaveChangesService.${modal}`).pipe(takeUntil(unsubscribe$)).subscribe();
                };

                proxy(this.profileRESTService.changePassword(this.profileId, {
                    old: formValue.oldPassword,
                    new: formValue.newPassword,
                    repeat: formValue.repeatPassword,
                })).pipe(takeUntil(unsubscribe$)).subscribe(
                    () => {
                        this.stateService.snapshot.forms.formPassword.reset();

                        alert('SuccessSavePassword');

                        done.next(undefined);
                        done.complete();
                    },
                    () => {
                        alert('FailSavePassword');

                        done.error();
                    },
                );

                return () => {
                    unsubscribe$.next(undefined);
                };
            });
        } else {
            return observableOf(true);
        }
    }

    private saveProfileChanges(): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((done) => {
            if (this.hasChanges) {
                const state: EvodeskUserSettingsState = this.stateService.snapshot;
                const formCommonValue: EvodeskUserSettingsCommonComponentFormValue = state.forms.formCommon.value;
                const formAccountValue: EvodeskUserSettingsAccountComponentFormValue = state.forms.formAccount.value;
                const formPrivacyAndSecurityValue: EvodeskUserSettingsPrivacySecurityComponentFormValue = state.forms.formPrivacyAndSecurity.value;

                const newProfile: ProfileModel = {
                    ...state.profile,
                    name: [formCommonValue.lastName, formCommonValue.firstName, formCommonValue.middleName].join(' '),
                    phone: (() => {
                        if (!! formCommonValue.phone) {
                            return '+' + (formCommonValue.phoneCode.toString() + formCommonValue.phone.toString()).replace(/[^\d]/g, '');
                        } else {
                            return null;
                        }
                    })(),
                    industry: formCommonValue.industry,
                    position: formCommonValue.status,
                    address: formCommonValue.address,
                    url_alias: (formAccountValue.urlAlias && formAccountValue.urlAlias.length) ? formAccountValue.urlAlias : null,
                    page_visibality: formPrivacyAndSecurityValue.pageVisibility,
                    photo_visibality: formPrivacyAndSecurityValue.photoVisibility,
                    subscribe_level: formPrivacyAndSecurityValue.subscribeLevel,
                };

                if (formCommonValue.birthday) {
                    newProfile.birth_date = moment(formCommonValue.birthday).format(BACKEND_DATE_FORMAT_AS_MOMENT);
                }

                this.profileRESTService.saveProfile(state.profile.id, newProfile).pipe(takeUntil(unsubscribe$)).subscribe(
                    () => {
                        done.next(undefined);
                        done.complete();
                    },
                    (httpError) => {
                        done.error(<SaveChangesError>{
                            type: SaveChangesErrorType.HttpError,
                            httpError: httpError,
                        });
                    },
                );
            } else {
                done.next(undefined);
                done.complete();
            }

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    private saveProfileCompetenceChanges(): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((done) => {
            const queries: Array<Observable<any>> = [];

            queries.push(this.syncCompetenceScript.run(this.profileId, {
                competences: (() => {
                    const competenceRequests: Array<SyncUserCompetencesRequestEntry> = [];

                    this.stateService.snapshot.forms.formsCompetence.forEach((ucForm) => {
                        const ucFormValue: EvodeskUserSettingsCompetenceComponentFormValue = ucForm.value;

                        if (ucFormValue.active) {
                            competenceRequests.push((() => {
                                return <SyncUserCompetencesRequestEntry>{
                                    competenceId: ucFormValue.competenceId,
                                    skills: ucFormValue.skills,
                                    documents: ucFormValue.documents,
                                    uploadDocuments: ucFormValue.uploadDocuments,
                                };
                            })());
                        }
                    });

                    return competenceRequests;
                })(),
            }).pipe(tap((syncResponse: SyncUserCompetenceResponse) => {
                if (syncResponse.uploadedDocuments.length) {
                    const competenceIds: Array<CompetenceId> = [];
                    const mapCompetencesToUploadedDocuments: { [competenceId: number]: Array<UserCompetenceDocumentModel> } = {};

                    syncResponse.uploadedDocuments.forEach((u) => {
                        if (! mapCompetencesToUploadedDocuments[u.competence_id.id]) {
                            competenceIds.push(u.competence_id.id);
                            mapCompetencesToUploadedDocuments[u.competence_id.id] = [];
                        }

                        mapCompetencesToUploadedDocuments[u.competence_id.id].push(u);
                    });

                    competenceIds.forEach((cId) => {
                        this.stateService.dispatch({
                            type: EvodeskUserSettingsStateAction.PushUserCompetenceDocuments,
                            payload: {
                                competenceId: cId,
                                documents: mapCompetencesToUploadedDocuments[cId],
                            },
                        });
                    });

                    competenceIds.forEach((cId) => {
                        this.stateService.dispatch({
                            type: EvodeskUserSettingsStateAction.ClearUploadUserCompetenceDocuments,
                        });
                    });
                }
            })));

            if (queries.length) {
                concat(...queries).pipe(takeUntil(unsubscribe$)).subscribe(
                    undefined,
                    (httpError) => {
                        done.error(httpError);
                    },
                    () => {
                        done.next(undefined);
                        done.complete();
                    },
                );
            } else {
                done.next(undefined);
                done.complete();
            }

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    private saveProfileEducationChanges(): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((done) => {
            const queries: Array<Observable<any>> = [];

            const allEducationForms: Array<FormGroup> =  this.stateService.snapshot.forms.formsEducation;

            allEducationForms
                .filter(f => (! isEducationFormEmpty(f.value) && f.dirty && f.valid))
                .map(form => {
                    const formValue: EvodeskUserSettingsEducationComponentFormValue = form.value;

                    if (formValue.id) {
                        return this.profileEducationRESTService.updateEducation(this.profileId, formValue.id, {
                            type: formValue.type,
                            city: formValue.city,
                            year: formValue.year.toString(),
                            name: formValue.name,
                            specialty: formValue.specialty,
                        });
                    } else {
                        return Observable.create((d) => {
                            const httpRequest$: Observable<ProfileEducationModel> = this.profileEducationRESTService.createEducation(this.profileId, {
                                type: formValue.type,
                                city: formValue.city,
                                year: formValue.year.toString(),
                                name: formValue.name,
                                specialty: formValue.specialty,
                            });

                            httpRequest$.pipe(takeUntil(unsubscribe$)).subscribe(
                                (httpResponse) => {
                                    form.patchValue({
                                        id: parseInt(<any>httpResponse.id, 10),
                                    });

                                    d.next(undefined);
                                    d.complete();
                                },
                                (httpError) => {
                                    d.error(httpError);
                                },
                            );
                        });
                    }
                })
                .forEach(f => queries.push(f));

            this.stateService.snapshot.requestDeleteEducations.forEach(eId => {
                queries.push(Observable.create(deleted => {
                    this.profileEducationRESTService.deleteEducation(this.profileId, eId).pipe(takeUntil(unsubscribe$)).subscribe(
                        () => {
                            this.stateService.dispatch({
                                type: EvodeskUserSettingsStateAction.EducationWasDeleted,
                                payload: {
                                    id: eId,
                                },
                            });

                            deleted.next(undefined);
                            deleted.complete();
                        },
                        (httpError) => {
                            deleted.error(httpError);
                        },
                    );
                }));
            });

            if (queries.length) {
                concat(...queries).pipe(takeUntil(unsubscribe$)).subscribe(
                    undefined,
                    (httpError) => {
                        done.error(httpError);
                    },
                    () => {
                        done.next(undefined);
                        done.complete();
                    },
                );
            } else {
                done.next(undefined);
                done.complete();
            }

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    private saveProfileExperienceChanges(): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((done) => {
            const queries: Array<Observable<any>> = [];

            const allExperienceForms: Array<FormGroup> =  this.stateService.snapshot.forms.formsExperience;

            allExperienceForms
                .filter(f => (! isExperienceFormEmpty(f.value) && f.dirty && f.valid))
                .map(form => {
                    const formValue: EvodeskUserSettingsWorkExperienceComponentFormValue = form.value;

                    const jsonModel: ProfileExperienceJSON = {
                        about: formValue.text,
                        tools: formValue.tools,
                        skills: formValue.skills,
                        responsibilities: formValue.responsibilities,
                    };

                    if (parseInt(<any>formValue.id, 10) > 0) {
                        return this.profileExperienceRESTService.updateExperience(this.profileId, formValue.id, {
                            place: formValue.place,
                            site: formValue.site,
                            date_start: formValue.date_start.format(BACKEND_DATE_FORMAT_AS_MOMENT),
                            date_finish: formValue.date_finish ? formValue.date_finish.format(BACKEND_DATE_FORMAT_AS_MOMENT) : null,
                            position: formValue.position,
                            text: JSON.stringify(jsonModel),
                            sphere: formValue.sphere,
                        });
                    } else {
                        return Observable.create((d) => {
                            const httpRequest$: Observable<any> = this.profileExperienceRESTService.addExperience(this.profileId, {
                                place: formValue.place,
                                site: formValue.site,
                                date_start: formValue.date_start.format(BACKEND_DATE_FORMAT_AS_MOMENT),
                                date_finish: formValue.date_finish ? formValue.date_finish.format(BACKEND_DATE_FORMAT_AS_MOMENT) : null,
                                position: formValue.position,
                                text: JSON.stringify(jsonModel),
                                sphere: formValue.sphere,
                            });

                            httpRequest$.pipe(takeUntil(unsubscribe$)).subscribe(
                                (httpResponse) => {
                                    const approvesToSend: Array<ApproveRequest> = formValue.approveRequests.filter(aR => !! aR.request.approve && ! aR.request.approve.id);

                                    form.patchValue({
                                        id: parseInt(<any>httpResponse.id, 10),
                                    });

                                    if (approvesToSend.length) {
                                        forkJoin(approvesToSend.map(a => this.profileExperienceRESTService.sendApproveRequest(
                                            httpResponse.id, a.request.approverProfileId,
                                        ))).subscribe(
                                            (approveHttpResponse) => {
                                                form.patchValue({
                                                    approveRequests: (form.value as EvodeskUserSettingsWorkExperienceComponentFormValue).approveRequests.map((aR) => {
                                                        if (aR.request.approve) {
                                                            return {
                                                                ...aR,
                                                                request: {
                                                                    ...aR.request,
                                                                    approve: {
                                                                        ...aR.request.approve,
                                                                        id: approveHttpResponse.filter(ah => ah.user_id === aR.request.approverProfileId)[0].id,
                                                                    },
                                                                },
                                                            };
                                                        } else {
                                                            return aR;
                                                        }
                                                    }),
                                                });

                                                d.next(undefined);
                                                d.complete();
                                            },
                                            (httpError) => {
                                                d.error(httpError);
                                            },
                                        );
                                    } else {
                                        d.next(undefined);
                                        d.complete();
                                    }
                                },
                                (httpError) => {
                                    d.error(httpError);
                                },
                            );
                        });
                    }
                })
                .forEach(f => queries.push(f));

            this.stateService.snapshot.requestDeleteExperiences.forEach(eId => {
                queries.push(Observable.create(deleted => {
                    if (eId > 0) {
                        this.profileExperienceRESTService.deleteExperience(this.profileId, eId).pipe(takeUntil(unsubscribe$)).subscribe(
                            () => {
                                this.stateService.dispatch({
                                    type: EvodeskUserSettingsStateAction.ExperienceWasDeleted,
                                    payload: {
                                        id: eId,
                                    },
                                });

                                deleted.next(undefined);
                                deleted.complete();
                            },
                            (httpError) => {
                                deleted.error(httpError);
                            },
                        );
                    }
                }));
            });

            if (queries.length) {
                concat(...queries).pipe(takeUntil(unsubscribe$)).subscribe(
                    undefined,
                    (httpError) => {
                        done.error(httpError);
                    },
                    () => {
                        done.next(undefined);
                        done.complete();
                    },
                );
            } else {
                done.next(undefined);
                done.complete();
            }

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    private saveNotificationSettingsChanges(): Observable<void> {
        if (this.stateService.snapshot.forms.formsNotifications.some(f => f.dirty || f.touched)) {
            return this.notificationRESTService.setSettings(this.stateService.snapshot.forms.formsNotifications.map(f => {
                const formValue: EvodeskUserSettingsNotificationsComponentFormValue = f.value;

                return <SetNotificationEntry>{
                    notificationTypeId: parseInt(<any>formValue.type.id, 10),
                    emailNotify: formValue.emailNotify,
                    mobileNotify: formValue.mobileNotify,
                    webNotify: formValue.webNotify,
                };
            }));
        } else {
            return observableOf(undefined);
        }
    }

    private fail(error: SaveChangesError): void {
        const reason: string = (() => {
            switch (error.type) {
                default:
                    return 'Unknown';

                case SaveChangesErrorType.InvalidForm:
                    return 'InvalidForm';

                case SaveChangesErrorType.FormIsNotValid:
                    return 'NotValid';
            }
        })();

        this.uiAlertService.openTranslated(`EvodeskUserSettings.services.EvodeskUserSaveChangesService.Fail.${reason}`).subscribe();
    }
}
