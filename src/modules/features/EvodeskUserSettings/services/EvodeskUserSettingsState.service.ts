import {Injectable} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {evodeskUserSettingsEducationComponentFormBuilder, EvodeskUserSettingsEducationComponentFormValue} from '../components/EvodeskUserSettingsEducation/EvodeskUserSettingsEducation.component';
import {EvodeskUserSettingsWorkExperienceComponentApproveRequestStatus as ApproveRequestStatus, EvodeskUserSettingsWorkExperienceComponentApproveRequest as ApproveRequest, evodeskUserSettingsWorkExperienceComponentFormBuilder, EvodeskUserSettingsWorkExperienceComponentFormValue} from '../components/EvodeskUserSettingsWorkExperience/EvodeskUserSettingsWorkExperience.component';
import {evodeskUserSettingsCompetenceComponentFormBuilder, EvodeskUserSettingsCompetenceComponentFormValue} from '../components/EvodeskUserSettingsCompetence/EvodeskUserSettingsCompetence.component';

import {EvodeskUserSettingsNavigation, EvodeskUserSettingsViewMode} from '../models/EvodeskUserSettings.model';
import {ProfileId, ProfileModel} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {IndustryModel} from '../../../app/EvodeskRESTApi/models/industry/Industry.model';
import {ProfileLegalStatusModel} from '../../../app/EvodeskRESTApi/models/profile/ProfileLegalStatus.model';
import {ProfileEducationId, ProfileEducationType} from '../../../app/EvodeskRESTApi/models/profile/ProfileEducation.model';
import {ProfileExperienceId} from '../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';
import {UserActivityModel} from '../../../app/EvodeskRESTApi/models/user/UserActivity.model';
import {WorkCompetenceApproveModel} from '../../../app/EvodeskRESTApi/models/WorkCompetenceApprove.model';
import {CompetenceCategoryModel} from '../../../app/EvodeskRESTApi/models/CompetenceCategory.model';
import {CompetenceId} from '../../../app/EvodeskRESTApi/models/Competence.model';
import {UserCompetenceDocumentModel} from '../../../app/EvodeskRESTApi/models/UserCompetenceDocument.model';

export interface EvodeskUserSettingsState {
    viewMode: EvodeskUserSettingsViewMode;
    currentNav: EvodeskUserSettingsNavigation;
    profile?: ProfileModel | undefined;
    forms?: {
        formCommon: FormGroup;
        formPassword: FormGroup;
        formAccount: FormGroup;
        formsEducation: Array<FormGroup>;
        formsExperience: Array<FormGroup>;
        formsCompetence: Array<FormGroup>;
        formPrivacyAndSecurity: FormGroup;
        formsNotifications: Array<FormGroup>;
    };
    availableIndustries?: Array<IndustryModel>;
    availableLegalStatuses?: Array<ProfileLegalStatusModel>;
    availableCompetenceCategories?: Array<CompetenceCategoryModel>;
    requestDeleteEducations: Array<ProfileEducationId>;
    requestDeleteExperiences: Array<ProfileExperienceId>;
    requestDeleteCompetences: Array<CompetenceId>;
    maxEducationsAllowed: number;
    maxExperiencesAllowed: number;
    maxCompetencesAllowed: number;
    userActivities: Array<UserActivityModel>;
    hasMoreUserActivitiesToLoad: boolean;
}

type PrevUserSettingsState = EvodeskUserSettingsState | undefined;
type CurrentUserSettingsState = EvodeskUserSettingsState;

interface States {
    previous: PrevUserSettingsState;
    current: CurrentUserSettingsState;
}

function initialState(): CurrentUserSettingsState {
    return {
        viewMode: EvodeskUserSettingsViewMode.Desktop,
        currentNav: EvodeskUserSettingsNavigation.Common,
        maxEducationsAllowed: 10,
        maxExperiencesAllowed: 10,
        maxCompetencesAllowed: 10,
        requestDeleteEducations: [],
        requestDeleteExperiences: [],
        requestDeleteCompetences: [],
        userActivities: [],
        hasMoreUserActivitiesToLoad: true,
    };
}

export enum EvodeskUserSettingsStateAction {
    SetViewMode = 'SetViewMode',
    Navigate = 'Navigate',
    Bootstrap = 'Bootstrap',
    AddEducationForm = 'AddEducationForm',
    MoveUpEducationForm = 'MoveUpEducationForm',
    MoveDownEducationForm = 'MoveDownEducationForm',
    RemoveEducationForm = 'RemoveEducationForm',
    EducationWasDeleted = 'EducationWasDeleted',
    AddExperienceForm = 'AddExperienceForm',
    RemoveExperienceForm = 'RemoveExperienceForm',
    MoveUpExperienceForm = 'MoveUpExperienceForm',
    MoveDownExperienceForm = 'MoveDownExperienceForm',
    ExperienceWasDeleted = 'ExperienceWasDeleted',
    PushUserActivities = 'PushUserActivities',
    WorkExperienceApproveSetApprove = 'WorkExperienceApproveSetApprove',
    WorkExperienceApproveSend = 'WorkExperienceApproveSend',
    WorkExperienceApproveSent = 'WorkExperienceApproveSent',
    WorkExperienceApproveFail = 'WorkExperienceApproveFail',
    WorkExperienceApproveSending = 'WorkExperienceApproveSending',
    WorkExperienceApproveCanceling = 'WorkExperienceApproveCanceling',
    WorkExperienceApproveCancel = 'WorkExperienceApproveCancel',
    AddCompetenceForm = 'AddCompetenceForm',
    RemoveCompetenceForm = 'RemoveCompetenceForm',
    CompetenceWasDeleted = 'CompetenceWasDeleted',
    ClearUploadUserCompetenceDocuments = 'ClearUploadUserCompetenceDocuments',
    PushUserCompetenceDocuments = 'PushUserCompetenceDocuments',
}

export interface EvodeskUserSettingsStateBootstrapQuery {
    formCommon: FormGroup;
    formPassword: FormGroup;
    formAccount: FormGroup;
    formsEducation: Array<FormGroup>;
    formsExperience: Array<FormGroup>;
    formsCompetence: Array<FormGroup>;
    formPrivacyAndSecurity: FormGroup;
    formsNotifications: Array<FormGroup>;
    profile: ProfileModel;
    availableIndustries: Array<IndustryModel>;
    availableLegalStatuses: Array<ProfileLegalStatusModel>;
    availableCompetenceCategories: Array<CompetenceCategoryModel>;
    userActivities: Array<UserActivityModel>;
    hasMoreUserActivitiesToLoad: boolean;
}

export type EvodeskUserSettingsStateActions =
      { type: EvodeskUserSettingsStateAction.SetViewMode, payload: EvodeskUserSettingsViewMode }
    | { type: EvodeskUserSettingsStateAction.Navigate, payload: EvodeskUserSettingsNavigation }
    | { type: EvodeskUserSettingsStateAction.Bootstrap, payload: EvodeskUserSettingsStateBootstrapQuery }
    | { type: EvodeskUserSettingsStateAction.AddEducationForm }
    | { type: EvodeskUserSettingsStateAction.MoveUpEducationForm, payload: { index: number } }
    | { type: EvodeskUserSettingsStateAction.MoveDownEducationForm, payload: { index: number } }
    | { type: EvodeskUserSettingsStateAction.RemoveEducationForm, payload: { index: number } }
    | { type: EvodeskUserSettingsStateAction.EducationWasDeleted, payload: { id: ProfileEducationId } }
    | { type: EvodeskUserSettingsStateAction.AddExperienceForm }
    | { type: EvodeskUserSettingsStateAction.MoveUpExperienceForm, payload: { index: number } }
    | { type: EvodeskUserSettingsStateAction.MoveDownExperienceForm, payload: { index: number } }
    | { type: EvodeskUserSettingsStateAction.RemoveExperienceForm, payload: { index: number } }
    | { type: EvodeskUserSettingsStateAction.ExperienceWasDeleted, payload: { id: ProfileExperienceId } }
    | { type: EvodeskUserSettingsStateAction.PushUserActivities, payload: { userActivities: Array<UserActivityModel>; hasMoreToLoad: boolean; } }
    | { type: EvodeskUserSettingsStateAction.WorkExperienceApproveSetApprove, payload: { workExperienceId: ProfileExperienceId; approverProfileId: ProfileId; approve: WorkCompetenceApproveModel; } }
    | { type: EvodeskUserSettingsStateAction.WorkExperienceApproveSend, payload: { workExperienceId: ProfileExperienceId; approverProfileId: ProfileId; approve: WorkCompetenceApproveModel; } }
    | { type: EvodeskUserSettingsStateAction.WorkExperienceApproveSent, payload: { workExperienceId: ProfileExperienceId; approverProfileId: ProfileId; } }
    | { type: EvodeskUserSettingsStateAction.WorkExperienceApproveSending, payload: { workExperienceId: ProfileExperienceId; approverProfileId: ProfileId; } }
    | { type: EvodeskUserSettingsStateAction.WorkExperienceApproveFail, payload: { workExperienceId: ProfileExperienceId; approverProfileId: ProfileId; } }
    | { type: EvodeskUserSettingsStateAction.WorkExperienceApproveCanceling, payload: { workExperienceId: ProfileExperienceId; approverProfileId: ProfileId; } }
    | { type: EvodeskUserSettingsStateAction.WorkExperienceApproveCancel, payload: { workExperienceId: ProfileExperienceId; approverProfileId: ProfileId; } }
    | { type: EvodeskUserSettingsStateAction.AddCompetenceForm, payload: { competenceId: CompetenceId } }
    | { type: EvodeskUserSettingsStateAction.RemoveCompetenceForm, payload: { index: number } }
    | { type: EvodeskUserSettingsStateAction.CompetenceWasDeleted, payload: { competenceId: CompetenceId } }
    | { type: EvodeskUserSettingsStateAction.ClearUploadUserCompetenceDocuments }
    | { type: EvodeskUserSettingsStateAction.PushUserCompetenceDocuments, payload: { competenceId: CompetenceId; documents: Array<UserCompetenceDocumentModel> } }
;

@Injectable()
export class EvodeskUserSettingsStateService
{
    private _destroy$: Subject<void> = new Subject<void>();

    private _current$: BehaviorSubject<States> = new BehaviorSubject<States>({
        previous: undefined,
        current: initialState(),
    });

    get current$(): Observable<States> {
        return this._current$.asObservable();
    }

    get destroy$(): Observable<void> {
        return this._destroy$.asObservable();
    }

    get snapshot(): CurrentUserSettingsState {
        return this._current$.getValue().current;
    }

    constructor(
        private fb: FormBuilder,
    ) {}

    reset(): void {
        this._current$.next({
            previous: undefined,
            current: initialState(),
        });
    }

    destroy(): void {
        this._destroy$.next(undefined);
    }

    dispatch(action: EvodeskUserSettingsStateActions): void {
        switch (action.type) {
            case EvodeskUserSettingsStateAction.Navigate:
                this.setState((orig) => {
                    return {
                        ...orig,
                        currentNav: action.payload,
                    };
                });

                break;

            case EvodeskUserSettingsStateAction.SetViewMode:
                this.setState((orig) => {
                    return {
                        ...orig,
                        viewMode: action.payload,
                    };
                });

                break;

            case EvodeskUserSettingsStateAction.Bootstrap:
                this.setState((orig) => {
                    return {
                        ...orig,
                        forms: {
                            ...orig.forms,
                            formCommon: action.payload.formCommon,
                            formPassword: action.payload.formPassword,
                            formAccount: action.payload.formAccount,
                            formsEducation: action.payload.formsEducation,
                            formsExperience: action.payload.formsExperience,
                            formPrivacyAndSecurity: action.payload.formPrivacyAndSecurity,
                            formsCompetence: action.payload.formsCompetence,
                            formsNotifications: action.payload.formsNotifications,
                        },
                        profile: action.payload.profile,
                        availableLegalStatuses: action.payload.availableLegalStatuses,
                        availableIndustries: action.payload.availableIndustries,
                        availableCompetenceCategories: action.payload.availableCompetenceCategories,
                        userActivities: action.payload.userActivities,
                        hasMoreUserActivitiesToLoad: action.payload.hasMoreUserActivitiesToLoad,
                    };
                });

                break;

            case EvodeskUserSettingsStateAction.AddEducationForm: {

                this.setState((orig) => {
                    if (orig.forms.formsEducation.length < orig.maxEducationsAllowed) {
                        return {
                            ...orig,
                            forms: {
                                ...orig.forms,
                                formsEducation: [...orig.forms.formsEducation, evodeskUserSettingsEducationComponentFormBuilder(this.fb, { initialType: ProfileEducationType.Secondary })],
                            },
                        };
                    } else {
                        return orig;
                    }
                });

                break;
            }

            case EvodeskUserSettingsStateAction.MoveUpEducationForm: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        forms: {
                            ...orig.forms,
                            formsEducation: (() => {
                                const forms: Array<FormGroup> = orig.forms.formsEducation;

                                const id1: ProfileEducationId = (forms[action.payload.index].value as EvodeskUserSettingsEducationComponentFormValue).id;
                                const id2: ProfileEducationId = (forms[action.payload.index - 1].value as EvodeskUserSettingsEducationComponentFormValue).id;
                                const swap: EvodeskUserSettingsEducationComponentFormValue = forms[action.payload.index].value;

                                forms[action.payload.index].patchValue({ ...forms[action.payload.index - 1].value, id: id1 });
                                forms[action.payload.index - 1].patchValue({ ...swap, id: id2 });

                                forms[action.payload.index].markAsDirty();
                                forms[action.payload.index - 1].markAsDirty();

                                return forms;
                            })(),
                        },
                    };
                });

                break;
            }

            case EvodeskUserSettingsStateAction.MoveDownEducationForm: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        forms: {
                            ...orig.forms,
                            formsEducation: (() => {
                                const forms: Array<FormGroup> = orig.forms.formsEducation;

                                const id1: ProfileEducationId = (forms[action.payload.index].value as EvodeskUserSettingsEducationComponentFormValue).id;
                                const id2: ProfileEducationId = (forms[action.payload.index + 1].value as EvodeskUserSettingsEducationComponentFormValue).id;
                                const swap: EvodeskUserSettingsEducationComponentFormValue = forms[action.payload.index].value;

                                forms[action.payload.index].patchValue({ ...forms[action.payload.index + 1].value, id: id1 });
                                forms[action.payload.index + 1].patchValue({ ...swap, id: id2 });

                                forms[action.payload.index].markAsDirty();
                                forms[action.payload.index + 1].markAsDirty();

                                return forms;
                            })(),
                        },
                    };
                });

                break;
            }

            case EvodeskUserSettingsStateAction.RemoveEducationForm: {
                this.setState((orig) => {
                    const origFormValue: EvodeskUserSettingsEducationComponentFormValue = orig.forms.formsEducation[action.payload.index].value;

                    if (orig.forms.formsEducation.length > 1) {
                        return {
                            ...orig,
                            requestDeleteEducations: origFormValue.id ? [...orig.requestDeleteEducations, origFormValue.id] : orig.requestDeleteEducations,
                            forms: {
                                ...orig.forms,
                                formsEducation: [...orig.forms.formsEducation.slice(0, action.payload.index), ...orig.forms.formsEducation.slice(action.payload.index + 1)],
                            },
                        };
                    } else {
                        return {
                            ...orig,
                            requestDeleteEducations: origFormValue.id ? [...orig.requestDeleteEducations, origFormValue.id] : orig.requestDeleteEducations,
                            forms: {
                                ...orig.forms,
                                formsEducation: [
                                    evodeskUserSettingsEducationComponentFormBuilder(this.fb),
                                ],
                            },
                        };
                    }
                });

                break;
            }

            case EvodeskUserSettingsStateAction.EducationWasDeleted: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        requestDeleteEducations: orig.requestDeleteEducations.filter(id => id !== action.payload.id),
                    };
                });

                break;
            }

            case EvodeskUserSettingsStateAction.AddExperienceForm: {
                this.setState((orig) => {
                    if (orig.forms.formsExperience.length < orig.maxExperiencesAllowed) {
                        return {
                            ...orig,
                            forms: {
                                ...orig.forms,
                                formsExperience: [...orig.forms.formsExperience, evodeskUserSettingsWorkExperienceComponentFormBuilder(this.fb)],
                            },
                        };
                    } else {
                        return orig;
                    }
                });

                break;
            }

            case EvodeskUserSettingsStateAction.MoveUpExperienceForm: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        forms: {
                            ...orig.forms,
                            formsExperience: (() => {
                                const forms: Array<FormGroup> = orig.forms.formsExperience;

                                const id1: ProfileExperienceId = (forms[action.payload.index].value as EvodeskUserSettingsWorkExperienceComponentFormValue).id;
                                const id2: ProfileExperienceId = (forms[action.payload.index - 1].value as EvodeskUserSettingsWorkExperienceComponentFormValue).id;
                                const swap: EvodeskUserSettingsWorkExperienceComponentFormValue = forms[action.payload.index].value;

                                forms[action.payload.index] = evodeskUserSettingsWorkExperienceComponentFormBuilder(this.fb, { form: { ...forms[action.payload.index - 1].value, id: id1 } });
                                forms[action.payload.index - 1] = evodeskUserSettingsWorkExperienceComponentFormBuilder(this.fb, { form: { ...swap, id: id2 } });

                                forms[action.payload.index].markAsDirty();
                                forms[action.payload.index - 1].markAsDirty();

                                return forms;
                            })(),
                        },
                    };
                });

                break;
            }

            case EvodeskUserSettingsStateAction.MoveDownExperienceForm: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        forms: {
                            ...orig.forms,
                            formsExperience: (() => {
                                const forms: Array<FormGroup> = orig.forms.formsExperience;

                                const id1: ProfileExperienceId = (forms[action.payload.index].value as EvodeskUserSettingsWorkExperienceComponentFormValue).id;
                                const id2: ProfileExperienceId = (forms[action.payload.index + 1].value as EvodeskUserSettingsWorkExperienceComponentFormValue).id;
                                const swap: EvodeskUserSettingsWorkExperienceComponentFormValue = forms[action.payload.index].value;

                                forms[action.payload.index] = evodeskUserSettingsWorkExperienceComponentFormBuilder(this.fb, { form: { ...forms[action.payload.index + 1].value, id: id1 } });
                                forms[action.payload.index + 1] = evodeskUserSettingsWorkExperienceComponentFormBuilder(this.fb, { form: { ...swap, id: id2 } });

                                forms[action.payload.index].markAsDirty();
                                forms[action.payload.index + 1].markAsDirty();

                                return forms;
                            })(),
                        },
                    };
                });

                break;
            }

            case EvodeskUserSettingsStateAction.RemoveExperienceForm: {
                this.setState((orig) => {
                    const origFormValue: EvodeskUserSettingsWorkExperienceComponentFormValue = orig.forms.formsExperience[action.payload.index].value;

                    if (orig.forms.formsExperience.length > 1) {
                        return {
                            ...orig,
                            requestDeleteExperiences: origFormValue.id ? [...orig.requestDeleteExperiences, origFormValue.id] : orig.requestDeleteExperiences,
                            forms: {
                                ...orig.forms,
                                formsExperience: [...orig.forms.formsExperience.slice(0, action.payload.index), ...orig.forms.formsExperience.slice(action.payload.index + 1)],
                            },
                        };
                    } else {
                        return {
                            ...orig,
                            requestDeleteExperiences: origFormValue.id ? [...orig.requestDeleteExperiences, origFormValue.id] : orig.requestDeleteExperiences,
                            forms: {
                                ...orig.forms,
                                formsExperience: [
                                    evodeskUserSettingsWorkExperienceComponentFormBuilder(this.fb),
                                ],
                            },
                        };
                    }
                });

                break;
            }

            case EvodeskUserSettingsStateAction.ExperienceWasDeleted: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        requestDeleteExperiences: orig.requestDeleteExperiences.filter(id => id !== action.payload.id),
                    };
                });

                break;
            }

            case EvodeskUserSettingsStateAction.PushUserActivities: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        userActivities: [...orig.userActivities, ...action.payload.userActivities],
                        hasMoreUserActivitiesToLoad: action.payload.hasMoreToLoad,
                    };
                });

                break;
            }

            case EvodeskUserSettingsStateAction.WorkExperienceApproveSetApprove: {
                const dForm: FormGroup = this.snapshot.forms.formsExperience.filter(f => f.get('id').value === action.payload.workExperienceId)[0];
                const dRequests: Array<ApproveRequest> = (dForm.get('approveRequests').value as Array<ApproveRequest>);

                if (dRequests.filter(r => r.request.approverProfileId === action.payload.approverProfileId).length === 0) {
                    dForm.patchValue({
                        approveRequests: [...dRequests, <ApproveRequest>{
                            status: ApproveRequestStatus.Sent,
                            request: {
                                approve: action.payload.approve,
                                approverProfileId: action.payload.workExperienceId,
                                workExperienceId: action.payload.approverProfileId,
                            },
                        }],
                    });
                } else {
                    dForm.patchValue({
                        approveRequests: dRequests.map(sR => {
                            if (sR.request.approverProfileId === action.payload.approverProfileId) {
                                return {
                                    ...sR,
                                    request: {
                                        ...sR.request,
                                        approve: action.payload.approve,
                                    },
                                };
                            } else {
                                return sR;
                            }
                        }),
                    });
                }

                this._current$.next({ current: this.snapshot, previous: this.snapshot });

                break;
            }

            case EvodeskUserSettingsStateAction.WorkExperienceApproveSend: {
                const sForm: FormGroup = this.snapshot.forms.formsExperience.filter(f => f.get('id').value === action.payload.workExperienceId)[0];
                const sRequests: Array<ApproveRequest> = (sForm.get('approveRequests').value as Array<ApproveRequest>);

                if (sRequests.filter(r => r.request.approverProfileId === action.payload.approverProfileId).length === 0) {
                    sForm.patchValue({
                        approveRequests: [...sRequests, <ApproveRequest>{
                            status: ApproveRequestStatus.Sent,
                            request: {
                                approve: action.payload.approve,
                                approverProfileId: action.payload.approverProfileId,
                                workExperienceId: action.payload.workExperienceId,
                            },
                        }],
                    });
                } else {
                    sForm.patchValue({
                        approveRequests: sRequests.map(sR => {
                            if (sR.request.approverProfileId === action.payload.approverProfileId) {
                                return {
                                    ...sR,
                                    status: ApproveRequestStatus.Sent,
                                };
                            } else {
                                return sR;
                            }
                        }),
                    });
                }

                this._current$.next({ current: this.snapshot, previous: this.snapshot });

                break;
            }

            case EvodeskUserSettingsStateAction.WorkExperienceApproveSent: {
                const sForm: FormGroup = this.snapshot.forms.formsExperience.filter(f => f.get('id').value === action.payload.workExperienceId)[0];
                const sRequests: Array<ApproveRequest> = (sForm.get('approveRequests').value as Array<ApproveRequest>);

                if (sRequests.filter(r => r.request.approverProfileId === action.payload.approverProfileId).length === 0) {
                    sForm.patchValue({
                        approveRequests: [...sRequests, <ApproveRequest>{
                            status: ApproveRequestStatus.Sent,
                            request: {
                                approve: undefined,
                                approverProfileId: action.payload.approverProfileId,
                                workExperienceId: action.payload.workExperienceId,
                            },
                        }],
                    });
                } else {
                    sForm.patchValue({
                        approveRequests: sRequests.map(sR => {
                            if (sR.request.approverProfileId === action.payload.approverProfileId) {
                                return {
                                    ...sR,
                                    status: ApproveRequestStatus.Sent,
                                };
                            } else {
                                return sR;
                            }
                        }),
                    });
                }

                this._current$.next({ current: this.snapshot, previous: this.snapshot });

                break;
            }

            case EvodeskUserSettingsStateAction.WorkExperienceApproveSending: {
                const sForm: FormGroup = this.snapshot.forms.formsExperience.filter(f => f.get('id').value === action.payload.workExperienceId)[0];
                const sRequests: Array<ApproveRequest> = (sForm.get('approveRequests').value as Array<ApproveRequest>);

                if (sRequests.filter(r => r.request.approverProfileId === action.payload.approverProfileId).length === 0) {
                    sForm.patchValue({
                        approveRequests: [...sRequests, <ApproveRequest>{
                            status: ApproveRequestStatus.Sent,
                            request: {
                                approve: undefined,
                                approverProfileId: action.payload.approverProfileId,
                                workExperienceId: action.payload.workExperienceId,
                            },
                        }],
                    });
                } else {
                    sForm.patchValue({
                        approveRequests: sRequests.map(sR => {
                            if (sR.request.approverProfileId === action.payload.approverProfileId) {
                                return {
                                    ...sR,
                                    status: ApproveRequestStatus.Sending,
                                };
                            } else {
                                return sR;
                            }
                        }),
                    });
                }

                this._current$.next({ current: this.snapshot, previous: this.snapshot });

                break;
            }

            case EvodeskUserSettingsStateAction.WorkExperienceApproveCancel: {
                const tForm: FormGroup = this.snapshot.forms.formsExperience.filter(f => f.get('id').value === action.payload.workExperienceId)[0];
                const tRequests: Array<ApproveRequest> = (tForm.get('approveRequests').value as Array<ApproveRequest>);

                if (tRequests.filter(r => r.request.approverProfileId === action.payload.approverProfileId).length === 0) {
                    tForm.patchValue({
                        approveRequests: [...tRequests, <ApproveRequest>{
                            status: ApproveRequestStatus.Canceled,
                            request: {
                                approve: undefined,
                                approverProfileId: action.payload.approverProfileId,
                                workExperienceId: action.payload.workExperienceId,
                            },
                        }],
                    });
                } else {
                    tForm.patchValue({
                        approveRequests: tRequests.map(sR => {
                            if (sR.request.approverProfileId === action.payload.approverProfileId) {
                                return {
                                    ...sR,
                                    status: ApproveRequestStatus.Canceled,
                                    request: {
                                        ...sR.request,
                                        approve: undefined,
                                    },
                                };
                            } else {
                                return sR;
                            }
                        }),
                    });
                }

                this._current$.next({ current: this.snapshot, previous: this.snapshot });

                break;
            }

            case EvodeskUserSettingsStateAction.WorkExperienceApproveCanceling: {
                const tForm: FormGroup = this.snapshot.forms.formsExperience.filter(f => f.get('id').value === action.payload.workExperienceId)[0];
                const tRequests: Array<ApproveRequest> = (tForm.get('approveRequests').value as Array<ApproveRequest>);

                if (tRequests.filter(r => r.request.approverProfileId === action.payload.approverProfileId).length === 0) {
                    tForm.patchValue({
                        approveRequests: [...tRequests, <ApproveRequest>{
                            status: ApproveRequestStatus.Canceled,
                            request: {
                                approve: undefined,
                                approverProfileId: action.payload.approverProfileId,
                                workExperienceId: action.payload.workExperienceId,
                            },
                        }],
                    });
                } else {
                    tForm.patchValue({
                        approveRequests: tRequests.map(sR => {
                            if (sR.request.approverProfileId === action.payload.approverProfileId) {
                                return {
                                    ...sR,
                                    status: ApproveRequestStatus.Canceling
                                    ,
                                };
                            } else {
                                return sR;
                            }
                        }),
                    });
                }

                this._current$.next({ current: this.snapshot, previous: this.snapshot });

                break;
            }

            case EvodeskUserSettingsStateAction.AddCompetenceForm: {
                this.setState((orig) => {
                    const notActiveForm: FormGroup = orig.forms.formsCompetence.filter(f => (f.value as EvodeskUserSettingsCompetenceComponentFormValue).competenceId === action.payload.competenceId)[0];

                    if (notActiveForm) {
                        notActiveForm.patchValue({
                            active: true,
                        });

                        return {
                            ...orig,
                            forms: {
                                ...orig.forms,
                                formsCompetence: [...orig.forms.formsCompetence],
                            },
                        };
                    } else {
                        return {
                            ...orig,
                            forms: {
                                ...orig.forms,
                                formsCompetence: [...orig.forms.formsCompetence, evodeskUserSettingsCompetenceComponentFormBuilder(this.fb, {
                                    withCompetenceId: action.payload.competenceId,
                                })],
                            },
                        };
                    }
                });

                break;
            }

            case EvodeskUserSettingsStateAction.RemoveCompetenceForm: {
                this.setState((orig) => {
                    const form: FormGroup = orig.forms.formsCompetence[action.payload.index];
                    const formValue: EvodeskUserSettingsCompetenceComponentFormValue = form.value;

                    if (formValue.id < 0) {
                        return {
                            ...orig,
                            forms: {
                                ...orig.forms,
                                formsCompetence: [...orig.forms.formsCompetence.slice(0, action.payload.index), ...orig.forms.formsCompetence.slice(action.payload.index + 1)],
                            },
                        };
                    } else {
                        form.patchValue({
                            active: false,
                        });

                        return {
                            ...orig,
                            forms: {
                                ...orig.forms,
                                formsCompetence: [...orig.forms.formsCompetence],
                            },
                        };
                    }
                });

                break;
            }

            case EvodeskUserSettingsStateAction.CompetenceWasDeleted: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        requestDeleteCompetences: orig.requestDeleteCompetences.filter(id => id !== action.payload.competenceId),
                    };
                });

                break;
            }

            case EvodeskUserSettingsStateAction.PushUserCompetenceDocuments: {
                this.setState((orig) => {
                    const form: FormGroup = orig.forms.formsCompetence.filter(f => (f.value as EvodeskUserSettingsCompetenceComponentFormValue).competenceId === action.payload.competenceId)[0];

                    form.patchValue({
                        documents: [...form.value.documents, ...action.payload.documents],
                    });

                    return {
                        ...orig,
                        forms: {
                            ...orig.forms,
                            formsCompetence: [...orig.forms.formsCompetence],
                        },
                    };
                });

                break;
            }

            case EvodeskUserSettingsStateAction.ClearUploadUserCompetenceDocuments: {
                this.setState((orig) => {
                    orig.forms.formsCompetence.forEach((f) => {
                        f.patchValue({
                            uploadDocuments: [],
                        });
                    });

                    return {
                        ...orig,
                        forms: {
                            ...orig.forms,
                            formsCompetence: [...orig.forms.formsCompetence],
                        },
                    };
                });

                break;
            }
        }
    }

    private setState(set$: (orig: EvodeskUserSettingsState) => EvodeskUserSettingsState) {
        const orig: EvodeskUserSettingsState = this._current$.getValue().current;

        this._current$.next({
            previous: orig,
            current: set$(orig),
        });
    }
}
