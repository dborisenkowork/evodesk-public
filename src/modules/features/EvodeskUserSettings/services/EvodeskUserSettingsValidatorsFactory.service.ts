import {ChangeDetectorRef, Injectable} from '@angular/core';
import {AbstractControl, AsyncValidatorFn} from '@angular/forms';

import {TranslateService} from '@ngx-translate/core';

import {Subject, Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {sprintf} from 'sprintf-js';

import {ProfileRESTService} from '../../../app/EvodeskRESTApi/services/ProfileREST.service';

import {MAX_ALIAS_LENGTH, MIN_ALIAS_LENGTH, ProfileId, validateUrlAlias} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';

interface InitialAlias {
    alias: string;
    profileId: ProfileId;
}

@Injectable()
export class EvodeskUserSettingsValidatorsFactoryService
{
    constructor(
        private cdr: ChangeDetectorRef,
        private translate: TranslateService,
        private profileRESTService: ProfileRESTService,
    ) {}

    factoryUrlAlias(initialAlias?: InitialAlias): AsyncValidatorFn {
        return (control: AbstractControl) => {
            const newAlias: string = control.value;

            return Observable.create((result) => {
                const unsubscribe$: Subject<void> = new Subject<void>();

                unsubscribe$.subscribe(() => {
                    result.next(null);
                    result.complete();
                });

                const accept: Function = () => {
                    result.next(null);
                    result.complete();

                    setTimeout(() => {
                        this.cdr.markForCheck();
                    });
                };

                const reject: Function = (error: string, replaces: Array<string> = []) => {
                    this.translate.get(`EvodeskUserSettings.services.EvodeskUserSettingsValidatorsFactoryService.factoryUrlAlias.${error}`).subscribe(
                        (translated) => {
                            result.next({
                                evodeskUrlAlias: sprintf(translated, replaces),
                            });
                            result.complete();

                            setTimeout(() => {
                                this.cdr.markForCheck();
                            });
                        },
                    );
                };

                if (newAlias && newAlias.length > 0) {
                    if (newAlias.length < MIN_ALIAS_LENGTH) {
                        reject('minLength', [MIN_ALIAS_LENGTH]);
                    } else if (newAlias.length > MAX_ALIAS_LENGTH) {
                        reject('maxLength', [MAX_ALIAS_LENGTH]);
                    } else if (!validateUrlAlias(newAlias)) {
                        reject('invalid');
                    } else {
                        this.profileRESTService.resolve(newAlias).pipe(takeUntil(unsubscribe$)).subscribe(
                            (resolvedId) => {
                                if (resolvedId && resolvedId > 0) {
                                    if (initialAlias.alias) {
                                        if (resolvedId === initialAlias.profileId) {
                                            accept();
                                        } else {
                                            reject('exists');
                                        }
                                    } else {
                                        reject('exists');
                                    }
                                } else {
                                    if (resolvedId) {
                                        reject('exists');
                                    } else {
                                        accept();
                                    }
                                }
                            },
                            () => {
                                reject('http');
                            },
                        );
                    }
                } else {
                    accept();
                }

                return () => {
                    unsubscribe$.next(undefined);
                };
            });
        };
    }
}
