import {Injectable} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';

import {filter, takeUntil} from 'rxjs/operators';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {EvodeskUserSettingsViewMode} from '../models/EvodeskUserSettings.model';

import {evodeskUserSettingsLayoutDesktop, evodeskUserSettingsLayoutMobile, evodeskUserSettingsLayoutTablet} from '../configs/EvodeskUserSettings.constants';

@Injectable()
export class EvodeskUserSettingsBreakpointsService
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private _current$: BehaviorSubject<EvodeskUserSettingsViewMode> = new BehaviorSubject<EvodeskUserSettingsViewMode>(EvodeskUserSettingsViewMode.Desktop);

    constructor(
        private breakpointObserver: BreakpointObserver,
    ) {}

    init(): Observable<EvodeskUserSettingsViewMode> {
        this.breakpointObserver.observe(`(max-width: ${evodeskUserSettingsLayoutMobile.to}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe((e) => {
            this._current$.next(EvodeskUserSettingsViewMode.Mobile);
        });

        this.breakpointObserver.observe(`(min-width: ${evodeskUserSettingsLayoutTablet.from}px) and (max-width: ${evodeskUserSettingsLayoutTablet.to}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this._current$.next(EvodeskUserSettingsViewMode.Tablet);
        });

        this.breakpointObserver.observe(`(min-width: ${evodeskUserSettingsLayoutDesktop.from}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this._current$.next(EvodeskUserSettingsViewMode.Desktop);
        });

        return this.current$;
    }

    destroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get current$(): Observable<EvodeskUserSettingsViewMode> {
        return this._current$.asObservable();
    }
}
