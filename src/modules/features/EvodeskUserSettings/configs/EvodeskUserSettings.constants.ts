export const evodeskUserSettingsLayoutMobile: { to: number } = { to: 500 };
export const evodeskUserSettingsLayoutTablet: { from: number, to: number } = { from: 501, to: 1160 };
export const evodeskUserSettingsLayoutDesktop: { from: number } = { from: 1161 };

export const evodeskUserSettingsMinSkillLength: number = 3;
export const evodeskUserSettingsMaxSkillLength: number = 32;


export const evodeskUserSettingsMinDocumentFileNameLength: number = 1;
export const evodeskUserSettingsMaxDocumentFileNameLength: number = 64;
