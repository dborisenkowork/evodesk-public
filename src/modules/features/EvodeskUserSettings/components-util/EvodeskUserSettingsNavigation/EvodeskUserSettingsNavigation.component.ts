import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {EvodeskUserSettingsNavigation} from '../../models/EvodeskUserSettings.model';

interface State {
    ready: boolean;
}

export interface EvodeskUserSettingsNavigationComponentProps {
    currentNav: EvodeskUserSettingsNavigation;
}

type Props = EvodeskUserSettingsNavigationComponentProps;

@Component({
    selector: 'evodesk-user-settings-navigation',
    templateUrl: './EvodeskUserSettingsNavigation.component.pug',
    styleUrls: [
        './EvodeskUserSettingsNavigation.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsNavigationComponent implements OnChanges
{
    @Input() props: Props;

    @Output('go') goEvent: EventEmitter<EvodeskUserSettingsNavigation> = new EventEmitter<EvodeskUserSettingsNavigation>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    go(navAsString: string): void {
        this.goEvent.emit(<any>navAsString);
    }
}
