import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {availableNavigationSections, EvodeskUserSettingsNavigation} from '../../models/EvodeskUserSettings.model';

interface State {
    ready: boolean;
    isOpened: boolean;
}

export interface EvodeskUserSettingsNavigationMobileComponentProps {
    currentNav: EvodeskUserSettingsNavigation;
}

type Props = EvodeskUserSettingsNavigationMobileComponentProps;

@Component({
    selector: 'evodesk-user-settings-navigation-mobile',
    templateUrl: './EvodeskUserSettingsNavigationMobile.component.pug',
    styleUrls: [
        './EvodeskUserSettingsNavigationMobile.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsNavigationMobileComponent implements OnChanges
{
    @Input() props: Props;

    @Output('go') goEvent: EventEmitter<EvodeskUserSettingsNavigation> = new EventEmitter<EvodeskUserSettingsNavigation>();

    public state: State = {
        ready: false,
        isOpened: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get containerCSSClasses(): any {
        return {
            'is-opened': this.state.isOpened,
        };
    }

    get currentNav(): string {
        return availableNavigationSections.filter(a => a.nav === this.props.currentNav)[0].translate;
    }

    go(navAsString: string): void {
        this.goEvent.emit(<any>navAsString);

        this.state = {
            ...this.state,
            isOpened: false,
        };
    }

    toggle(): void {
        this.state = {
            ...this.state,
            isOpened: ! this.state.isOpened,
        };
    }

    close(): void {
        this.state = {
            ...this.state,
            isOpened: false,
        };
    }
}
