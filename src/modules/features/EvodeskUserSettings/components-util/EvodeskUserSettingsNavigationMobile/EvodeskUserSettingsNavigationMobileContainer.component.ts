import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import {EvodeskUserSettingsNavigation} from '../../models/EvodeskUserSettings.model';

import {EvodeskUserSettingsNavigationMobileComponentProps} from './EvodeskUserSettingsNavigationMobile.component';

import {EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';

type Props = EvodeskUserSettingsNavigationMobileComponentProps;

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-user-settings-navigation-mobile-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-user-settings-navigation-mobile [props]="state.props" (go)="go($event)"></evodesk-user-settings-navigation-mobile>
        </ng-container>
    `,
})
export class EvodeskUserSettingsNavigationMobileContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private router: Router,
        private usState: EvodeskUserSettingsStateService,
    ) {}

    ngOnInit(): void {
        this.usState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.currentNav),
            distinctUntilChanged(),
        ).subscribe((current) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    currentNav: current,
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    go(newNav: EvodeskUserSettingsNavigation): void {
        this.router.navigate(['/user-settings'], {
            queryParams: {
                'section': newNav,
            },
        });
    }
}
