import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskUserSettingsRouting} from './configs/EvodeskUserSettingsRouting.config';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskUserSettingsRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskUserSettingsRoutingModule
{}
