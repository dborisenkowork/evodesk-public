import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {evodeskUserSettingsMaxDocumentFileNameLength, evodeskUserSettingsMinDocumentFileNameLength} from '../../configs/EvodeskUserSettings.constants';

interface State {
    ready: boolean;
    form: FormGroup;
}

interface FormValue {
    title: string;
}

interface EditEvent {
    title: string;
}

export {EditEvent as EvodeskUserSettingsCompetenceEditFileComponentEditEvent};

@Component({
    selector: 'evodesk-user-settings-competence-edit-file',
    templateUrl: './EvodeskUserSettingsCompetenceEditFile.component.pug',
    styleUrls: [
        './EvodeskUserSettingsCompetenceEditFile.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsCompetenceEditFileComponent
{
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('edit') editEvent: EventEmitter<EditEvent> = new EventEmitter<EditEvent>();

    public state: State = {
        ready: true,
        form: this.fb.group({ title: ['', [Validators.required, Validators.minLength(evodeskUserSettingsMinDocumentFileNameLength), Validators.maxLength(evodeskUserSettingsMaxDocumentFileNameLength)]] }),
    };

    constructor(
        private fb: FormBuilder,
        private cdr: ChangeDetectorRef,
    ) {}

    get formValue(): FormValue {
        return this.state.form.value;
    }

    setTitle(title: string): void {
        this.state.form.patchValue({
            title: title,
        });

        this.cdr.detectChanges();
    }

    get canSubmit(): boolean {
        return this.state.form.valid;
    }

    submit(): void {
        if (this.state.form.valid) {
            this.editEvent.emit({
                title: this.formValue.title,
            });
        }

        this.close();
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
