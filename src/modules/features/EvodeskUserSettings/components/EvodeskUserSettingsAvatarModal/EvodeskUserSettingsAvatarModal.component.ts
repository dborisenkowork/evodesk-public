import {AfterViewInit, Component, ElementRef, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

import * as Cropper from 'cropperjs/dist/cropper';

export interface EvodeskUserSettingsAvatarModalComponentProps {
    imageUrl: string;
    minWidth: number;
    minHeight: number;
    profileName: string;
    profileStatus: string;
}

interface State {
    trustedImageUrl: any;
    cropper?: any;
}

@Component({
    selector: 'evodesk-user-settings-avatar-modal',
    templateUrl: './EvodeskUserSettingsAvatarModal.component.pug',
    styleUrls: [
        './EvodeskUserSettingsAvatarModal.component.scss',
    ],
})
export class EvodeskUserSettingsAvatarModalComponent implements AfterViewInit
{
    @ViewChild('image') imageRef: ElementRef;
    @ViewChild('preview') previewRef: ElementRef;

    public state: State = {
        trustedImageUrl: this.sanitize.bypassSecurityTrustUrl(this.props.imageUrl),
    };

    constructor(
        private sanitize: DomSanitizer,
        private matDialogRef: MatDialogRef<EvodeskUserSettingsAvatarModalComponent>,
        @Inject(MAT_DIALOG_DATA) public props: EvodeskUserSettingsAvatarModalComponentProps,
    ) {}

    ngAfterViewInit(): void {
        if (this.imageRef) {
            this.state = {
                ...this.state,
                cropper: (() => {
                    return new Cropper(this.imageRef.nativeElement, {
                        viewMode: 1,
                        initialAspectRatio: 1,
                        aspectRatio: 1,
                        guides: true,
                        minCropBoxWidth: this.props.minWidth,
                        minCropBoxHeight: this.props.minHeight,
                        center: true,
                        preview: this.previewRef.nativeElement,
                    });
                })(),
            };
        }
    }

    cancel(): void {
        this.matDialogRef.close();
    }

    submit(): void {
        this.state.cropper.getCroppedCanvas();

        this.state.cropper.getCroppedCanvas({
            maxWidth: 4096,
            maxHeight: 4096,
        }).toBlob((blob) => {
            this.matDialogRef.close(blob);
        });
    }
}

