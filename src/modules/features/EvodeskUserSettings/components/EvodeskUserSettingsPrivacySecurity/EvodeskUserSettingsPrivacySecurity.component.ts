import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {PageVisibility, PhotoVisibility, SubscribeAccess} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

export interface EvodeskUserSettingsPrivacySecurityComponentProps {
    form: FormGroup;
}

interface State {
    ready: boolean;
    pageVisibilityOptions: Array<{ value: string; title: string; }>;
    photoVisibilityOptions: Array<{ value: string; title: string; }>;
    subscribeAccessOptions: Array<{ value: string; title: string; }>;
}

interface FormBuilderOptions {
    initialValue: {
        pageVisibility: PageVisibility;
        photoVisibility: PhotoVisibility;
        subscribeLevel: SubscribeAccess;
    };
}

export function evodeskUserSettingsPrivacySecurityComponentFormBuilder(fb: FormBuilder, options: FormBuilderOptions): FormGroup {
    const form: FormGroup = fb.group({
        pageVisibility: ['0', [Validators.required]],
        photoVisibility: ['0', [Validators.required]],
        subscribeAccess: ['0', [Validators.required]],
    });

    form.patchValue({
        pageVisibility: options.initialValue.pageVisibility,
        photoVisibility: options.initialValue.photoVisibility,
        subscribeLevel: options.initialValue.subscribeLevel,
    });

    return form;
}

export interface EvodeskUserSettingsPrivacySecurityComponentFormValue {
    pageVisibility: PageVisibility;
    photoVisibility: PhotoVisibility;
    subscribeLevel: SubscribeAccess;
}

@Component({
    selector: 'evodesk-user-settings-privacy-security',
    templateUrl: './EvodeskUserSettingsPrivacySecurity.component.pug',
    styleUrls: [
        './EvodeskUserSettingsPrivacySecurity.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsPrivacySecurityComponent implements OnChanges
{
    @Input() props: EvodeskUserSettingsPrivacySecurityComponentProps;

    public state: State = {
        ready: false,
        pageVisibilityOptions: [
            { value: <any>(PageVisibility.Everyone), title: 'EvodeskUserSettings.components.EvodeskUserSettingsPrivacyAndSecurity.PageVisibility.Options.Everyone' },
            { value: <any>(PageVisibility.MySubscribers), title: 'EvodeskUserSettings.components.EvodeskUserSettingsPrivacyAndSecurity.PageVisibility.Options.MySubscribers' },
            { value: <any>(PageVisibility.SubscribersRelated), title: 'EvodeskUserSettings.components.EvodeskUserSettingsPrivacyAndSecurity.PageVisibility.Options.SubscribersRelated' },
        ],
        photoVisibilityOptions: [
            { value: <any>(PhotoVisibility.Everyone), title: 'EvodeskUserSettings.components.EvodeskUserSettingsPrivacyAndSecurity.PhotoVisibility.Options.Everyone' },
            { value: <any>(PhotoVisibility.MySubscribers), title: 'EvodeskUserSettings.components.EvodeskUserSettingsPrivacyAndSecurity.PhotoVisibility.Options.MySubscribers' },
            { value: <any>(PhotoVisibility.SubscribersRelated), title: 'EvodeskUserSettings.components.EvodeskUserSettingsPrivacyAndSecurity.PhotoVisibility.Options.SubscribersRelated' },
        ],
        subscribeAccessOptions: [
            { value: <any>(SubscribeAccess.Everyone), title: 'EvodeskUserSettings.components.EvodeskUserSettingsPrivacyAndSecurity.SubscribeAccess.Options.Everyone' },
            { value: <any>(SubscribeAccess.MySubscriptions), title: 'EvodeskUserSettings.components.EvodeskUserSettingsPrivacyAndSecurity.SubscribeAccess.Options.MySubscriptions' },
            { value: <any>(SubscribeAccess.SubscribersRelated), title: 'EvodeskUserSettings.components.EvodeskUserSettingsPrivacyAndSecurity.SubscribeAccess.Options.SubscribersRelated' },
        ],
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }
}
