import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskUserSettingsPrivacySecurityComponentProps} from './EvodeskUserSettingsPrivacySecurity.component';

import {EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';

interface State {
    ready: boolean;
    props?: EvodeskUserSettingsPrivacySecurityComponentProps;
}

@Component({
    selector: 'evodesk-user-settings-privacy-security-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-user-settings-privacy-security [props]="state.props"></evodesk-user-settings-privacy-security>
      </ng-container>
    `,
})
export class EvodeskUserSettingsPrivacySecurityContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private stateService: EvodeskUserSettingsStateService,
    ) {}

    ngOnInit(): void {
        this.stateService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(s => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    form: s.current.forms.formPrivacyAndSecurity,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
