import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

export interface EvodeskUserSettingsPasswordComponentFormValue {
    oldPassword: string;
    newPassword: string;
    repeatPassword: string;
}

export interface EvodeskUserSettingsPasswordComponentProps {
    form: FormGroup;
}

interface State {
    ready: boolean;
}

export function evodeskUserSettingsPasswordComponentFormBuilder(fb: FormBuilder): FormGroup {
    return fb.group({
        oldPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(255)]],
        newPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(255)]],
        repeatPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(255)]],
    });
}

@Component({
    selector: 'evodesk-user-settings-password',
    templateUrl: './EvodeskUserSettingsPassword.component.pug',
    styleUrls: [
        './EvodeskUserSettingsPassword.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsPasswordComponent implements OnChanges
{
    @Input() props: EvodeskUserSettingsPasswordComponentProps;

    @Output('ngSubmit') ngSubmitEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: true,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }
}
