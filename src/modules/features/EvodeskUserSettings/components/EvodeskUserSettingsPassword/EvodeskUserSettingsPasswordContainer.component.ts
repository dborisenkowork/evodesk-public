import {ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskUserSettingsPasswordComponentProps} from './EvodeskUserSettingsPassword.component';
import {EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';

interface State {
    ready: boolean;
    props?: EvodeskUserSettingsPasswordComponentProps;
}

@Component({
    selector: 'evodesk-user-settings-password-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-user-settings-password [props]="state.props" (ngSubmit)="ngSubmitEvent.emit(undefined)"></evodesk-user-settings-password>
      </ng-container>
    `,
})
export class EvodeskUserSettingsPasswordContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    @Output('ngSubmit') ngSubmitEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private userSettingsState: EvodeskUserSettingsStateService,
    ) {}

    ngOnInit(): void {
        this.userSettingsState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(s => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    form: s.current.forms.formPassword,
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
