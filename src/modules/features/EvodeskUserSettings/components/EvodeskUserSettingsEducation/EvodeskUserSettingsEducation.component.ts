import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {ProfileEducationModel, ProfileEducationModelMetadataEntry, ProfileEducationType} from '../../../../app/EvodeskRESTApi/models/profile/ProfileEducation.model';

export interface EvodeskUserSettingsEducationComponentFormValue {
    id: number | undefined;
    type: string;
    city: string;
    year: number;
    name: string;
    specialty: string;
}

export interface EvodeskUserSettingsEducationComponentProps {
    forms: Array<FormGroup>;
    availableTypes: Array<ProfileEducationModelMetadataEntry>;
    maxEducationAllowed: number;
}

interface State {
    ready: boolean;
}

const DATES_RANGE: number = 100;

export function evodeskUserSettingsEducationComponentFormBuilder(fb: FormBuilder, options: {
    initialType?: ProfileEducationType;
    education?: ProfileEducationModel;
} = {}): FormGroup {
    const form: FormGroup = fb.group({
        id: [undefined],
        type: ['', [Validators.required, Validators.maxLength(255)]],
        city: ['', [Validators.required, Validators.maxLength(255)]],
        year: [null, [Validators.required, Validators.maxLength(255)]],
        name: ['', [Validators.required, Validators.maxLength(255)]],
        specialty: ['', [Validators.maxLength(255)]],
    });

    if (options.education) {
        form.patchValue({
            id: options.education.id,
            type: options.education.type,
            city: options.education.city,
            year: options.education.year,
            name: options.education.name,
            specialty: options.education.specialty,
        });
    }

    if (options.initialType && ! options.education) {
        form.patchValue({
            type: options.initialType,
            year: (new Date()).getFullYear(),
        });
    }

    return form;
}

@Component({
    selector: 'evodesk-user-settings-education',
    templateUrl: './EvodeskUserSettingsEducation.component.pug',
    styleUrls: [
        './EvodeskUserSettingsEducation.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsEducationComponent implements OnChanges, OnDestroy
{
    @Input() props: EvodeskUserSettingsEducationComponentProps;

    @Output('add') addEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('remove') removeEvent: EventEmitter<{ index: number }> = new EventEmitter<{ index: number }>();
    @Output('moveUp') moveUpEvent: EventEmitter<{ index: number }> = new EventEmitter<{ index: number }>();
    @Output('moveDown') moveDownEvent: EventEmitter<{ index: number }> = new EventEmitter<{ index: number }>();

    public state: State;

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }

            this.props.forms.forEach(f => {
                f.get('type').valueChanges.pipe(takeUntil(this.ngOnChanges$)).subscribe(() => {
                    if (! this.shouldDisplaySpecialty(f)) {
                        f.patchValue({
                            specialty: '',
                        });
                    }
                });
            });
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    get canAdd(): boolean {
        return (this.props.maxEducationAllowed < 0) || (this.props.forms.length < this.props.maxEducationAllowed);
    }

    get availableDates(): Array<number> {
        const options: Array<number> = [];

        for (let i: number = (new Date()).getFullYear(); i > (new Date()).getFullYear() - DATES_RANGE; i--) {
            options.push(i);
        }

        return options;
    }

    shouldDisplaySpecialty(form: FormGroup): boolean {
        const metadataEntry: ProfileEducationModelMetadataEntry = this.props.availableTypes.filter(aT => aT.type === <any>(form.value as EvodeskUserSettingsEducationComponentFormValue).type)[0];

        return metadataEntry ? metadataEntry.canContainsSpecialty : false;
    }

    add(): void {
        this.addEvent.emit(undefined);
    }

    remove(index: number): void {
        this.removeEvent.emit({
            index: index,
        });
    }

    moveUp(index: number): void {
        this.moveUpEvent.emit({
            index: index,
        });
    }

    moveDown(index: number): void {
        this.moveDownEvent.emit({
            index: index,
        });
    }
}
