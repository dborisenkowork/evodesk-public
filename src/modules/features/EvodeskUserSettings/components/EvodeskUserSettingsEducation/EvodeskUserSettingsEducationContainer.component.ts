import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {EvodeskUserSettingsEducationComponentProps} from './EvodeskUserSettingsEducation.component';

import {profileEducationTypeMetadata} from '../../../../app/EvodeskRESTApi/models/profile/ProfileEducation.model';

import {EvodeskUserSettingsStateAction, EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';

interface State {
    ready: boolean;
    props?: EvodeskUserSettingsEducationComponentProps;
}

@Component({
    selector: 'evodesk-user-settings-education-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-user-settings-education
          [props]="state.props"
          (add)="onAdd()"
          (remove)="onRemove($event.index)"
          (moveUp)="onMoveUp($event.index)"
          (moveDown)="onMoveDown($event.index)"
        ></evodesk-user-settings-education>
      </ng-container>
    `,
})
export class EvodeskUserSettingsEducationContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private userSettingsState: EvodeskUserSettingsStateService,
    ) {}

    ngOnInit(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                availableTypes: [...profileEducationTypeMetadata],
                maxEducationAllowed: this.userSettingsState.snapshot.maxEducationsAllowed,
            },
        };

        this.userSettingsState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => ! this.state.ready || s.current.forms.formsEducation !== s.previous.forms.formsEducation),
        ).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    forms: s.current.forms.formsEducation,
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onAdd(): void {
        this.userSettingsState.dispatch({
            type: EvodeskUserSettingsStateAction.AddEducationForm,
        });

        this.cdr.detectChanges();
    }

    onRemove(index: number): void {
        this.userSettingsState.dispatch({
            type: EvodeskUserSettingsStateAction.RemoveEducationForm,
            payload: {
                index: index,
            },
        });

        this.cdr.detectChanges();
    }

    onMoveUp(index: number): void {
        if (index < 1) {
            return;
        }

        this.userSettingsState.dispatch({
            type: EvodeskUserSettingsStateAction.MoveUpEducationForm,
            payload: {
                index: index,
            },
        });
    }

    onMoveDown(index: number): void {
        if (index > (this.state.props.forms.length - 1)) {
            return;
        }

        this.userSettingsState.dispatch({
            type: EvodeskUserSettingsStateAction.MoveDownEducationForm,
            payload: {
                index: index,
            },
        });
    }
}
