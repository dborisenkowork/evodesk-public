import {ChangeDetectorRef, Component, OnDestroy, OnInit, SimpleChange, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {defaultBottomMatDialog800pxConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {ProfileExperienceId} from '../../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';

import {EvodeskUserSettingsWorkExperienceComponentProps} from './EvodeskUserSettingsWorkExperience.component';
import {EvodeskUserSettingsWorkExperienceConfirmContainerComponent} from '../EvodeskUserSettingsWorkExperienceConfirm/EvodeskUserSettingsWorkExperienceConfirmContainer.component';

import {EvodeskUserSettingsStateAction, EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';

interface State {
    ready: boolean;
    props?: EvodeskUserSettingsWorkExperienceComponentProps;
    matDialogRef?: MatDialogRef<any>;
}

@Component({
    selector: 'evodesk-user-settings-work-experience-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-user-settings-work-experience
          [props]="state.props"
          (add)="onAdd()"
          (remove)="onRemove($event.index)"
          (moveUp)="onMoveUp($event.index)"
          (moveDown)="onMoveDown($event.index)"
          (openApproveModal)="openApproveModal($event)"
        ></evodesk-user-settings-work-experience>
      </ng-container>
    `,
})
export class EvodeskUserSettingsWorkExperienceContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private matDialogClose$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private matDialog: MatDialog,
        private userSettingsState: EvodeskUserSettingsStateService,
        private viewContainerRef: ViewContainerRef,
    ) {}

    ngOnInit(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                maxExperienceAllowed: this.userSettingsState.snapshot.maxExperiencesAllowed,
            },
        };

        this.userSettingsState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
        ).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    forms: s.current.forms.formsExperience,
                    availableSpheres: [...s.current.availableIndustries],
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.matDialogClose$.next(undefined);
        this.ngOnDestroy$.next(undefined);

        if (this.state.matDialogRef) {
            this.state.matDialogRef.close();
        }
    }

    onAdd(): void {
        this.userSettingsState.dispatch({
            type: EvodeskUserSettingsStateAction.AddExperienceForm,
        });

        this.cdr.detectChanges();
    }

    onRemove(index: number): void {
        this.userSettingsState.dispatch({
            type: EvodeskUserSettingsStateAction.RemoveExperienceForm,
            payload: {
                index: index,
            },
        });

        this.cdr.detectChanges();
    }

    onMoveUp(index: number): void {
        if (index < 1) {
            return;
        }

        this.userSettingsState.dispatch({
            type: EvodeskUserSettingsStateAction.MoveUpExperienceForm,
            payload: {
                index: index,
            },
        });
    }

    onMoveDown(index: number): void {
        if (index > (this.state.props.forms.length - 1)) {
            return;
        }

        this.userSettingsState.dispatch({
            type: EvodeskUserSettingsStateAction.MoveDownExperienceForm,
            payload: {
                index: index,
            },
        });
    }

    openApproveModal(id: ProfileExperienceId): void {
        const matDialogRef: MatDialogRef<EvodeskUserSettingsWorkExperienceConfirmContainerComponent> = this.matDialog.open(EvodeskUserSettingsWorkExperienceConfirmContainerComponent, {
            ...defaultBottomMatDialog800pxConfig,
            closeOnNavigation: true,
            viewContainerRef: this.viewContainerRef,
        });

        matDialogRef.componentInstance.experienceId = id;
        matDialogRef.componentInstance.ngOnChanges({ experienceId: new SimpleChange(undefined, matDialogRef.componentInstance.experienceId, true) });

        matDialogRef.componentInstance.closeEvent.pipe(takeUntil(this.matDialogClose$)).subscribe(() => {
            matDialogRef.close();
        });

        matDialogRef.afterClosed().pipe(takeUntil(this.matDialogClose$)).subscribe(() => {
            this.matDialogClose$.next(undefined);

            this.state = {
                ...this.state,
                matDialogRef: undefined,
            };
        });

        this.state = {
            ...this.state,
            matDialogRef: matDialogRef,
        };
    }
}
