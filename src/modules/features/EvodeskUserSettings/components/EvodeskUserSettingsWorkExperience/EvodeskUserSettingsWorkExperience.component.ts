import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, QueryList, SimpleChanges, ViewChildren} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDateFormats} from '@angular/material';

import {Moment} from 'moment';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import * as textMask from 'vanilla-text-mask/dist/vanillaTextMask.js';
import * as moment from 'moment';

import {IndustryModel} from '../../../../app/EvodeskRESTApi/models/industry/Industry.model';
import {parseWorkExperienceJSON, ProfileExperienceId, ProfileExperienceJSON, ProfileExperienceModel} from '../../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';
import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {WorkCompetenceApproveModel} from '../../../../app/EvodeskRESTApi/models/WorkCompetenceApprove.model';
import {EvodeskProfileUrlService} from '../../../EvodeskProfile/shared/services/EvodeskProfileUrl.service';

let nextMinusId: number = -1;

export interface ApproveRequest {
    status: ApproveRequestStatus;
    request: {
        approve?: WorkCompetenceApproveModel;
        workExperienceId: ProfileExperienceId;
        approverProfileId: ProfileId;
    };
}

enum ApproveRequestStatus {
    Sending = 'sending',
    WillBeSent = 'willBeSent',
    Sent = 'sent',
    Canceled = 'canceled',
    Canceling = 'canceling',
}

export function evodeskUserSettingsWorkExperienceComponentFormBuilder(fb: FormBuilder, options: {
    experience?: ProfileExperienceModel,
    form?: FormValue,
} = {}): FormGroup {
    const form: FormGroup = fb.group({
        id: [--nextMinusId],
        place: ['', [Validators.required, Validators.maxLength(255)]],
        site: ['', [Validators.maxLength(255)]],
        date_start: ['', [Validators.required]],
        date_finish: [''],
        position: ['', [Validators.required, Validators.maxLength(255)]],
        text: ['', [Validators.required, Validators.maxLength(65535)]],
        responsibilities: fb.array([]),
        tools: fb.array([]),
        skills: fb.array([]),
        untilToday: [false],
        sphere: [undefined],
        approveRequests: [[]],

    });

    if (options.experience) {
        const jsonModel: ProfileExperienceJSON = parseWorkExperienceJSON(options.experience.text);

        jsonModel.responsibilities.map(i => formBuilderResponsibility(fb, i.value)).forEach((f) => (form.get('responsibilities') as FormArray).push(f));
        jsonModel.tools.map(i => formBuilderTools(fb, i.value)).forEach((f) => (form.get('tools') as FormArray).push(f));
        jsonModel.skills.map(i => formBuilderSkills(fb, i.value)).forEach((f) => (form.get('skills') as FormArray).push(f));

        form.patchValue({
            id: options.experience.id,
            place: options.experience.place,
            site: options.experience.site,
            date_start: moment(new Date(options.experience.date_start)),
            date_finish: options.experience.date_finish ? moment(new Date(options.experience.date_finish)) : null,
            position: options.experience.position,
            text: jsonModel.about,
            untilToday: options.experience.date_finish === null,
            sphere: options.experience.sphere,
            approveRequests: options.experience.approves.map((approve) => {
                return <ApproveRequest>{
                    request: {
                        approve: approve,
                        workExperienceId: approve.experience_id,
                        approverProfileId: approve.user_id,
                    },
                    status: ApproveRequestStatus.Sent,
                };
            }),
        });

        if ((form.get('responsibilities') as FormArray).length === 0) {
            (form.get('responsibilities') as FormArray).push(formBuilderResponsibility(fb));
        }

        if ((form.get('tools') as FormArray).length === 0) {
            (form.get('tools') as FormArray).push(formBuilderTools(fb));
        }

        if ((form.get('skills') as FormArray).length === 0) {
            (form.get('skills') as FormArray).push(formBuilderSkills(fb));
        }

    } else if (options.form) {
        form.patchValue(options.form);

        options.form.responsibilities.forEach(v => (form.get('responsibilities') as FormArray).push(formBuilderResponsibility(fb, v.value)));
        options.form.tools.forEach(v => (form.get('tools') as FormArray).push(formBuilderTools(fb, v.value)));
        options.form.skills.forEach(v => (form.get('skills') as FormArray).push(formBuilderSkills(fb, v.value)));
    }

    return form;
}

export function formBuilderResponsibility(fb: FormBuilder, initialValue: string = ''): FormGroup {
    const form: FormGroup = fb.group({
        value: [initialValue, [Validators.maxLength(65535)]],
    });

    form.patchValue({
        value: initialValue,
    });

    return form;
}

export function formBuilderTools(fb: FormBuilder, initialValue: string = ''): FormGroup {
    const form: FormGroup = fb.group({
        value: [initialValue, [Validators.maxLength(65535)]],
    });

    form.patchValue({
        value: initialValue,
    });

    return form;
}

export function formBuilderSkills(fb: FormBuilder, initialValue: string = ''): FormGroup {
    const form: FormGroup = fb.group({
        value: [initialValue, [Validators.maxLength(65535)]],
    });

    form.patchValue({
        value: initialValue,
    });

    return form;
}


interface FormValue {
    id?: number | undefined;
    place: string;
    site: string;
    date_start: Moment;
    date_finish: Moment | null;
    position: string;
    text: string;
    responsibilities: Array<{ value: string }>;
    tools: Array<{ value: string }>;
    skills: Array<{ value: string }>;
    untilToday: boolean;
    sphere: number;
    approveRequests: Array<ApproveRequest>;
}

export interface Props {
    forms: Array<FormGroup>;
    maxExperienceAllowed: number;
    availableSpheres: Array<IndustryModel>;
}

interface State {
    ready: boolean;
    textMaskControllers: Array<any>;
}

const DATE_FORMATS: MatDateFormats = {
    parse: {
        dateInput: 'DD.MM.YYYY',
    },
    display: {
        dateInput: 'DD.MM.YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

export {FormValue as EvodeskUserSettingsWorkExperienceComponentFormValue};
export {Props as EvodeskUserSettingsWorkExperienceComponentProps};
export {ApproveRequestStatus as EvodeskUserSettingsWorkExperienceComponentApproveRequestStatus};
export {ApproveRequest as EvodeskUserSettingsWorkExperienceComponentApproveRequest};

@Component({
    selector: 'evodesk-user-settings-work-experience',
    templateUrl: './EvodeskUserSettingsWorkExperience.component.pug',
    styleUrls: [
        './EvodeskUserSettingsWorkExperience.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        { provide: MAT_DATE_FORMATS, useValue: DATE_FORMATS },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    ],
})
export class EvodeskUserSettingsWorkExperienceComponent implements OnChanges, AfterViewInit, OnDestroy
{
    @Input() props: Props;

    @Output('add') addEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('moveUp') moveUpEvent: EventEmitter<{ index: number }> = new EventEmitter<{ index: number }>();
    @Output('moveDown') moveDownEvent: EventEmitter<{ index: number }> = new EventEmitter<{ index: number }>();
    @Output('remove') removeEvent: EventEmitter<{ index: number }> = new EventEmitter<{ index: number }>();
    @Output('openApproveModal') openApproveModalEvent: EventEmitter<ProfileExperienceId> = new EventEmitter<ProfileExperienceId>();

    @ViewChildren('inputDateStart') inputDateStartRefs: QueryList<ElementRef>;
    @ViewChildren('inputDateFinish') inputDateFinishRefs: QueryList<ElementRef>;

    public state: State = {
        ready: false,
        textMaskControllers: [],
    };

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private fb: FormBuilder,
        private urlProfile: EvodeskProfileUrlService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }

            this.props.forms.forEach((form) => {
                if ((form.value as FormValue).date_finish === null) {
                    form.get('date_finish').disable();
                }

                form.valueChanges.pipe(takeUntil(this.ngOnChanges$)).subscribe(() => {
                    this.updateDates(form);
                });

                form.get('untilToday').valueChanges.pipe(takeUntil(this.ngOnChanges$)).subscribe((v) => {
                    if (v) {
                        form.get('date_finish').disable();
                        form.get('date_finish').setValue(null);
                    } else {
                        form.get('date_finish').enable();
                    }
               });

                this.updateDates(form);
            });

            this.setupMasks();
        }
    }

    ngAfterViewInit(): void {
        this.setupMasks();

    }

    ngOnDestroy(): void {
        this.state.textMaskControllers.forEach(c => c.destroy());

        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    get canAdd(): boolean {
        return (this.props.maxExperienceAllowed < 0) || (this.props.forms.length < this.props.maxExperienceAllowed);
    }

    add(): void {
        this.addEvent.emit(undefined);
    }

    remove(index: number): void {
        this.removeEvent.emit({
            index: index,
        });
    }

    moveUp(index: number): void {
        this.moveUpEvent.emit({
            index: index,
        });
    }

    moveDown(index: number): void {
        this.moveDownEvent.emit({
            index: index,
        });
    }

    addResponsibilities(form: FormGroup): void {
        (form.get('responsibilities') as FormArray).push(formBuilderResponsibility(this.fb));
    }

    removeResponsibilities(form: FormGroup, index: number): void {
        (form.get('responsibilities') as FormArray).removeAt(index);
    }

    addTools(form: FormGroup): void {
        (form.get('tools') as FormArray).push(formBuilderResponsibility(this.fb));
    }

    removeTools(form: FormGroup, index: number): void {
        (form.get('tools') as FormArray).removeAt(index);
    }

    addSkills(form: FormGroup): void {
        (form.get('skills') as FormArray).push(formBuilderResponsibility(this.fb));
    }

    removeSkills(form: FormGroup, index: number): void {
        (form.get('skills') as FormArray).removeAt(index);
    }

    updateDateStart(form: FormGroup): void {
        setTimeout(() => {
            if (! (form.value as FormValue).date_start || ! (form.value as FormValue).date_start.isValid()) {
                form.patchValue({
                    date_start: null,
                });
            }
        });
    }

    updateDates(form: FormGroup): void {
        const v: FormValue = form.value;

        if (v.date_start && v.date_start.toDate() > (new Date())) {
            form.patchValue({
                date_start: null,
            });
        }

        if (v.date_finish && v.date_finish.toDate() > (new Date())) {
            form.patchValue({
                date_finish: null,
            });
        }

        if (v.date_finish && v.date_start && v.date_start.toDate() > v.date_finish.toDate()) {
            form.patchValue({
                date_finish: null,
            });
        }
    }

    getDateStartFilter(form: FormGroup): (input: Moment) => boolean {
        return (input: Moment) => {
            const formValue: FormValue = form.value;

            if (formValue.date_finish) {
                return input.toDate() <= new Date() && input.toDate() <= formValue.date_finish.toDate();
            } else {
                return input.toDate() <= new Date();
            }
        };
    }

    getDateFinishFilter(form: FormGroup): (input: Moment) => boolean {
        return (input: Moment) => {
            const formValue: FormValue = form.value;

            if (formValue.date_start) {
                return input.toDate() <= new Date() && input.toDate() >= formValue.date_start.toDate();
            } else {
                return input.toDate() <= new Date();
            }
        };
    }

    isExistsOnServer(form: FormGroup): boolean {
        return !! (form.value as FormValue).id;
    }

    id(form: FormGroup): number {
        if (this.isExistsOnServer(form)) {
            return parseInt(<any>(form.value as FormValue).id, 10);
        } else {
            throw new Error('Experience does not exists yet');
        }
    }

    openApproveModal(id: ProfileExperienceId): void {
        this.openApproveModalEvent.emit(id);
    }

    approves(form: FormGroup): Array<WorkCompetenceApproveModel> {
        return (form.value as FormValue).approveRequests
            .filter(r => !! r.request.approve)
            .map(r => r.request.approve);
    }

    isConfirmed(approve: WorkCompetenceApproveModel): boolean {
        return approve.status.toString() === '1';
    }

    isNotConfirmed(approve: WorkCompetenceApproveModel): boolean {
        return approve.status.toString() === '0';
    }

    isRejected(approve: WorkCompetenceApproveModel): boolean {
        return approve.status.toString() === '2';
    }

    url(profileId: ProfileId): Array<any> {
        return this.urlProfile.getRouterLinkByProfileId(profileId);
    }

    private setupMasks(): void {
        this.state.textMaskControllers.forEach(c => c.destroy());

        this.state = {
            ...this.state,
            textMaskControllers: [],
        };

        setTimeout(() => {
            this.inputDateFinishRefs.forEach((elem) => {
                this.state.textMaskControllers.push(textMask.maskInput({
                    inputElement: elem.nativeElement,
                    mask: [/\d/, /\d/, '.', /\d/, /\d/, '.',  /\d/, /\d/, /\d/, /\d/],
                }));
            });

            this.inputDateStartRefs.forEach((elem) => {
                this.state.textMaskControllers.push(textMask.maskInput({
                    inputElement: elem.nativeElement,
                    mask: [/\d/, /\d/, '.', /\d/, /\d/, '.',  /\d/, /\d/, /\d/, /\d/],
                }));
            });
        });
    }
}
