import {ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskUserSettingsValidatorsFactoryService} from '../../services/EvodeskUserSettingsValidatorsFactory.service';

export function evodeskUserSettingsAccountComponentFormBuilder(
    fb: FormBuilder,
    validatorsFactory: EvodeskUserSettingsValidatorsFactoryService,
    options: { initialValue?: { profileId: number; urlAlias: string } } = {},
): FormGroup {
    const form: FormGroup = fb.group({
        urlAlias: ['', []],
    });

    if (options.initialValue) {
        form.get('urlAlias').setAsyncValidators([
            validatorsFactory.factoryUrlAlias({
                alias: options.initialValue.urlAlias,
                profileId: options.initialValue.profileId,
            }),
        ]);

        form.patchValue({
            urlAlias: options.initialValue.urlAlias,
        });
    } else {
        form.get('urlAlias').setAsyncValidators([
            validatorsFactory.factoryUrlAlias(),
        ]);
    }

    return form;
}

export interface EvodeskUserSettingsAccountComponentFormValue {
    urlAlias: string;
}

export interface EvodeskUserSettingsAccountComponentProps {
    form: FormGroup;
}

interface State {
    ready: boolean;
    wasBlured: boolean;
}

@Component({
    selector: 'evodesk-user-settings-account',
    templateUrl: './EvodeskUserSettingsAccount.component.pug',
    styleUrls: [
        './EvodeskUserSettingsAccount.component.scss',
    ],
})
export class EvodeskUserSettingsAccountComponent implements OnChanges, OnDestroy
{
    @Input() props: EvodeskUserSettingsAccountComponentProps;

    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private ngOnChanges$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        wasBlured: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            this.ngOnChanges$.next(undefined);

            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };

                this.props.form.valueChanges.pipe(takeUntil(this.ngOnChanges$)).subscribe(() => {
                    this.cdr.detectChanges();
                });
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }

        this.cdr.detectChanges();
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    onBlur(): void {
        this.state = {
            ...this.state,
            wasBlured: true,
        };
    }

    get isPending(): boolean {
        return false;
    }

    get hasError(): boolean {
        return ! this.props.form.valid && !! this.props.form.get('urlAlias').errors;
    }

    get error(): string {
        return this.props.form.get('urlAlias').errors['evodeskUrlAlias'];
    }
}
