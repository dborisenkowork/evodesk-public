import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskUserSettingsAccountComponentProps} from './EvodeskUserSettingsAccount.component';

import {EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';

interface State {
    ready: boolean;
    props?: EvodeskUserSettingsAccountComponentProps;
}

@Component({
    selector: 'evodesk-user-settings-account-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-user-settings-account [props]="state.props"></evodesk-user-settings-account>
      </ng-container>
    `,
})
export class EvodeskUserSettingsAccountContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private userSettingsStateService: EvodeskUserSettingsStateService,
    ) {}

    ngOnInit(): void {
        this.userSettingsStateService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    form: s.current.forms.formAccount,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
