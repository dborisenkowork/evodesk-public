import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject, Observable} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {proxy} from '../../../../../functions/proxy.function';

import {defaultBottomMatDialogConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {EvodeskUserSettingsAvatarModalComponent, EvodeskUserSettingsAvatarModalComponentProps} from '../EvodeskUserSettingsAvatarModal/EvodeskUserSettingsAvatarModal.component';
import {EvodeskUserSettingsAvatarComponentProps} from './EvodeskUserSettingsAvatar.component';

import {EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {ProfileRESTService} from '../../../../app/EvodeskRESTApi/services/ProfileREST.service';
import {EvodeskAppMessageBusService} from '../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';

import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {AppEvent} from '../../../../app/EvodeskAppMessageBus/models/app-events.model';

interface State {
    ready: boolean;
    props?: EvodeskUserSettingsAvatarComponentProps;
    profileId?: ProfileId;
    profileName?: string;
    profileStatus?: string;
    matDialogRef?: MatDialogRef<any>;
}

declare var Image: any;

@Component({
    selector: 'evodesk-user-settings-avatar-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-user-settings-avatar [props]="state.props" (selectFile)="onSelectFile($event)"></evodesk-user-settings-avatar>
      </ng-container>
    `,
})
export class EvodeskUserSettingsAvatarContainerComponent implements OnInit, OnDestroy
{
    public static MIN_IMAGE_WIDTH: number = 100;
    public static MIN_IMAGE_HEIGHT: number = 100;

    public static MAX_IMAGE_WIDTH: number = 5000;
    public static MAX_IMAGE_HEIGHT: number = 5000;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private matDialog: MatDialog,
        private appMessageBus: EvodeskAppMessageBusService,
        private uiAlertService: EvodeskAlertModalService,
        private userSettingsState: EvodeskUserSettingsStateService,
        private profileRESTService: ProfileRESTService,
    ) {}

    ngOnInit(): void {
        this.userSettingsState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    origAvatar: {
                        profileId: s.current.profile.id,
                        profileImage: s.current.profile.images[0],
                    },
                },
                profileId: s.current.profile.id,
                profileName: s.current.profile.name || s.current.profile.email,
                profileStatus: s.current.profile.position,
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);

        if (this.state.matDialogRef) {
            this.state.matDialogRef.close();
        }
    }

    onSelectFile(file: File): void {
        if (file) {
            const fr: FileReader = new FileReader();

            fr.onload = () => {
                const image: any = new Image();

                image.onload = () => {
                    if (image.width < EvodeskUserSettingsAvatarContainerComponent.MIN_IMAGE_WIDTH && image.height < EvodeskUserSettingsAvatarContainerComponent.MIN_IMAGE_HEIGHT) {
                        this.uiAlertService.openTranslated('EvodeskUserSettings.components.EvodeskUserSettingsAvatar.InvalidImageSize').pipe(takeUntil(this.ngOnDestroy$)).subscribe();
                    } else if (image.width > EvodeskUserSettingsAvatarContainerComponent.MAX_IMAGE_WIDTH && image.height > EvodeskUserSettingsAvatarContainerComponent.MAX_IMAGE_HEIGHT) {
                        this.uiAlertService.openTranslated('EvodeskUserSettings.components.EvodeskUserSettingsAvatar.TooBig').pipe(takeUntil(this.ngOnDestroy$)).subscribe();
                    } else {
                        this.openCropModal(fr.result.toString());
                    }
                };

                image.onerror = () => {
                    this.uiAlertService.openTranslated('EvodeskUserSettings.components.EvodeskUserSettingsAvatar.InvalidFile').pipe(takeUntil(this.ngOnDestroy$)).subscribe();
                };

                image.src = fr.result;
            };

            fr.onerror = () => {
                this.uiAlertService.openTranslated('EvodeskUserSettings.components.EvodeskUserSettingsAvatar.UnableToAccessFile').pipe(takeUntil(this.ngOnDestroy$)).subscribe();
            };

            fr.readAsDataURL(file);
        }
    }

    openCropModal(imageUrl: string): void {
        const matDialogRef: MatDialogRef<EvodeskUserSettingsAvatarModalComponent> = this.matDialog.open(EvodeskUserSettingsAvatarModalComponent, {
            ...defaultBottomMatDialogConfig,
            data: <EvodeskUserSettingsAvatarModalComponentProps>{
                imageUrl: imageUrl,
                minWidth: EvodeskUserSettingsAvatarContainerComponent.MIN_IMAGE_WIDTH,
                minHeight: EvodeskUserSettingsAvatarContainerComponent.MIN_IMAGE_HEIGHT,
                profileName: this.state.profileName,
                profileStatus: this.state.profileStatus,
            },
        });

        matDialogRef.afterClosed().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.state = {
                ...this.state,
                matDialogRef: undefined,
            };
        });

        matDialogRef.afterClosed().pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !!s),
        ).subscribe((file: Blob) => {
            const observable: Observable<void> = this.profileRESTService.uploadAvatar(this.state.profileId, file);

            observable.subscribe(
                () => {
                    this.appMessageBus.dispatch({ type: AppEvent.InvalidateProfileImage, payload: { profileId: this.state.profileId } });
                },
            );

            proxy(observable).pipe(takeUntil(this.ngOnDestroy$)).subscribe(undefined, () => {
                this.uiAlertService.openTranslated('EvodeskUserSettings.components.EvodeskUserSettingsAvatar.Fail').pipe(takeUntil(this.ngOnDestroy$)).subscribe();
            });
        });

        this.state = {
            ...this.state,
            matDialogRef: matDialogRef,
        };
    }
}
