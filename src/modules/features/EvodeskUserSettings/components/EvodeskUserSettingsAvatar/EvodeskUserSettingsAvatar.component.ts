import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';

import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

export interface EvodeskUserSettingsAvatarComponentProps {
    origAvatar: {
        profileId: ProfileId;
        profileImage: string | null;
    };
}

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-user-settings-avatar',
    templateUrl: './EvodeskUserSettingsAvatar.component.pug',
    styleUrls: [
        './EvodeskUserSettingsAvatar.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsAvatarComponent implements OnChanges
{
    @Input() props: EvodeskUserSettingsAvatarComponentProps;

    @ViewChild('fileInput') fileInputRef: ElementRef;

    @Output('selectFile') selectFileEvent: EventEmitter<File> = new EventEmitter<File>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    onFileInputChange(): void {
        this.selectFileEvent.emit((this.fileInputRef.nativeElement as HTMLInputElement).files[0]);
    }

    changeAvatar(): void {
        if (this.fileInputRef) {
            (this.fileInputRef.nativeElement as HTMLInputElement).click();
        }
    }
}
