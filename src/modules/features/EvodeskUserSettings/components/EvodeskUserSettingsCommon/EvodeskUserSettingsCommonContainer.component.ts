import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskUserSettingsCommonComponentProps} from './EvodeskUserSettingsCommon.component';
import {EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';

interface State {
    ready: boolean;
    props?: EvodeskUserSettingsCommonComponentProps;
}

@Component({
    selector: 'evodesk-user-settings-common-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-user-settings-common [props]="state.props"></evodesk-user-settings-common>
      </ng-container>
    `,
})
export class EvodeskUserSettingsCommonContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private stateService: EvodeskUserSettingsStateService,
    ) {}

    ngOnInit(): void {
        this.stateService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(s => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    form: s.current.forms.formCommon,
                    availableIndustries: s.current.availableIndustries,
                    availableLegalStatuses: s.current.availableLegalStatuses,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
