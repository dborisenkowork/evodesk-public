import {ChangeDetectionStrategy, Component, Inject, Input, OnChanges, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {TextMaskConfig} from 'angular2-text-mask';

import {IndustryModel} from '../../../../app/EvodeskRESTApi/models/industry/Industry.model';
import {ProfileLegalStatusModel} from '../../../../app/EvodeskRESTApi/models/profile/ProfileLegalStatus.model';
import {PHONE_CODES, PhoneCodeEntries} from '../../../../../models/phoneCodes.models';

export interface EvodeskUserSettingsCommonComponentProps {
    form: FormGroup;
    availableIndustries: Array<IndustryModel>;
    availableLegalStatuses: Array<ProfileLegalStatusModel>;
}

interface State {
    ready: boolean;
    phoneMask: TextMaskConfig;
    phoneCodeOptions: PhoneCodeEntries;
}

export interface EvodeskUserSettingsCommonComponentFormValue {
    phone: string;
    phoneCode: string;
    firstName: string;
    lastName: string;
    middleName: string;
    birthday: Date | null;
    address: string;
    industry: string;
    status: string;
}

export function evodeskUserSettingsCommonComponentFormBuilder(fb: FormBuilder): FormGroup {
    return fb.group({
        phone: ['', [Validators.minLength(6), Validators.maxLength(16)]],
        phoneCode: ['+7', [Validators.required]],
        firstName: ['', [Validators.maxLength(255)]],
        lastName: ['', [Validators.maxLength(255)]],
        middleName: ['', [Validators.maxLength(255)]],
        birthday: ['', []],
        address: ['', [Validators.maxLength(255)]],
        industry: ['', [Validators.maxLength(255)]],
        status: ['', [Validators.maxLength(255)]],
    });
}

@Component({
    selector: 'evodesk-user-settings-common',
    templateUrl: './EvodeskUserSettingsCommon.component.pug',
    styleUrls: [
        './EvodeskUserSettingsCommon.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsCommonComponent implements OnChanges
{
    @Input() props: EvodeskUserSettingsCommonComponentProps;

    public state: State = {
        ready: false,
        phoneMask: {
            mask: ['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
        },
        phoneCodeOptions: this.phoneCodes,
    };

    constructor(
        @Inject(PHONE_CODES) private phoneCodes: PhoneCodeEntries,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get birthdayFilter(): (input: Date) => boolean {
        return (input: Date) => {
            return input < new Date();
        };
    }
}
