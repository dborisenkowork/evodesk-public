import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';

import {Subject, interval} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import {EvodeskUserSettingsNotificationsComponentProps} from './EvodeskUserSettingsNotifications.component';
import {EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';
import {EvodeskUserSaveChangesService} from '../../services/EvodeskUserSaveChanges.service';

interface State {
    ready: boolean;
    props?: EvodeskUserSettingsNotificationsComponentProps;
}

@Component({
    selector: 'evodesk-user-settings-notifications-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-user-settings-notifications [props]="state.props" (enableDesktopNotifications)="enableDesktopNotifications()"></evodesk-user-settings-notifications>
      </ng-container>
    `,
})
export class EvodeskUserSettingsNotificationsContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private stateService: EvodeskUserSettingsStateService,
        private saveChanges: EvodeskUserSaveChangesService,
    ) {}

    ngOnInit(): void {
        this.stateService.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.forms.formsNotifications),
            distinctUntilChanged(),
        ).subscribe(forms => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    forms: forms,
                },
            };

            this.updateNotificationStatus();
        });

        this.saveChanges.saved$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.updateNotificationStatus();
        });

        interval(1000).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.updateNotificationStatus();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get notifications(): Notification {
        return window['Notification'];
    }

    enableDesktopNotifications(): void {
        Notification.requestPermission(() => {
            this.updateNotificationStatus();
        });
    }

    updateNotificationStatus(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                hasDesktopNotificationsSupport: 'Notification' in window,
                areDesktopNotificationsEnabled: (this.notifications as any).permission === 'granted',
                areDesktopNotificationsRestricted: (this.notifications as any).permission === 'denied',
            },
        };

        this.cdr.detectChanges();
    }
}
