import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {NotificationSettingsModel} from '../../../../app/EvodeskRESTApi/models/NotificationSettings.model';
import {NotificationType} from '../../../../app/EvodeskRESTApi/models/NotificationsSettings.model';

export interface EvodeskUserSettingsNotificationsComponentProps {
    hasDesktopNotificationsSupport: boolean;
    areDesktopNotificationsEnabled: boolean;
    areDesktopNotificationsRestricted: boolean;
    forms: Array<FormGroup>;
}

interface State {
    ready: boolean;
}

export interface EvodeskUserSettingsNotificationsComponentFormValue {
    type: NotificationType;
    emailNotify: boolean;
    mobileNotify: boolean;
    webNotify: boolean;
}

export function evodeskUserSettingsNotificationsComponentFormBuilder(fb: FormBuilder, withOptions: { settings: NotificationSettingsModel }): FormGroup {
    return fb.group({
        type: [withOptions.settings.notificationType],
        emailNotify: [withOptions.settings.emailNotify],
        mobileNotify: [withOptions.settings.mobileNotify],
        webNotify: [withOptions.settings.webNotify],
    });
}

@Component({
    selector: 'evodesk-user-settings-notifications',
    templateUrl: './EvodeskUserSettingsNotifications.component.pug',
    styleUrls: [
        './EvodeskUserSettingsNotifications.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsNotificationsComponent implements OnChanges
{
    @Input() props: EvodeskUserSettingsNotificationsComponentProps;

    @Output('enableDesktopNotifications') enableDesktopNotificationsEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    enableDesktopNotifications(): void {
        this.enableDesktopNotificationsEvent.emit(undefined);
    }
}
