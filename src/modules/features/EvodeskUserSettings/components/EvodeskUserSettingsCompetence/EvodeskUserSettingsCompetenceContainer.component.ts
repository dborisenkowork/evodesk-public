import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {defaultBottomMatDialog800pxConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {EvodeskUserSettingsCompetenceComponentProps} from './EvodeskUserSettingsCompetence.component';
import {EvodeskUserSettingsCompetenceAddContainerComponent as AddCompetenceModal} from '../EvodeskUserSettingsCompetenceAdd/EvodeskUserSettingsCompetenceAddContainer.component';

import {EvodeskUserSettingsStateAction, EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';

interface State {
    ready: boolean;
    props?: EvodeskUserSettingsCompetenceComponentProps;
}

@Component({
    selector: 'evodesk-user-settings-competence-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-user-settings-competence
          [props]="state.props"
          (add)="onAdd()"
          (remove)="onRemove($event.index)"
        ></evodesk-user-settings-competence>
      </ng-container>
    `,
})
export class EvodeskUserSettingsCompetenceContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private modalClose$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private usState: EvodeskUserSettingsStateService,
    ) {}

    ngOnInit(): void {
        const tryBootstrap: Function = () => {
            if (this.state.props && this.state.props.availableCompetenceCategories !== undefined && this.state.props.forms !== undefined && this.state.props.maxCompetencesAllowed !== undefined) {
                this.state = {
                    ...this.state,
                    ready: true,
                };

                this.cdr.detectChanges();
            }
        };

        this.usState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => s.current.maxCompetencesAllowed !== undefined),
            map(s => s.current.maxCompetencesAllowed),
            distinctUntilChanged(),
        ).subscribe((maxCompetencesAllowed) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    maxCompetencesAllowed: maxCompetencesAllowed,
                },
            };

            tryBootstrap();
        });

        this.usState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => s.current.availableCompetenceCategories !== undefined),
            map(s => s.current.availableCompetenceCategories),
            distinctUntilChanged(),
        ).subscribe((availableCompetenceCategories) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    availableCompetenceCategories: availableCompetenceCategories,
                },
            };

            tryBootstrap();
        });

        this.usState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => s.current.forms.formsCompetence !== undefined),
            map(s => s.current.forms.formsCompetence),
            distinctUntilChanged(),
        ).subscribe((formsCompetence) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    forms: formsCompetence,
                },
            };

            tryBootstrap();
        });
    }

    ngOnDestroy(): void {
        this.modalClose$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    onAdd(): void {
        const matDialogRef: MatDialogRef<AddCompetenceModal> = this.matDialog.open(AddCompetenceModal, {
            ...defaultBottomMatDialog800pxConfig,
            viewContainerRef: this.viewContainerRef,
            closeOnNavigation: true,
            autoFocus: false,
        });

        matDialogRef.componentInstance.closeEvent.pipe(takeUntil(this.modalClose$)).subscribe(() => {
            this.modalClose$.next(undefined);

            matDialogRef.close();
        });

        matDialogRef.afterClosed().pipe(takeUntil(this.modalClose$)).subscribe(() => {
            this.modalClose$.next(undefined);
        });

        this.cdr.detectChanges();
    }

    onRemove(index: number): void {
        this.usState.dispatch({
            type: EvodeskUserSettingsStateAction.RemoveCompetenceForm,
            payload: {
                index: index,
            },
        });

        this.cdr.detectChanges();
    }
}
