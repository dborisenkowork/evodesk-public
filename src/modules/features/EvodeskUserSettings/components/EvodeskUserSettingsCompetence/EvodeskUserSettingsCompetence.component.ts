import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, QueryList, SimpleChanges, ViewChildren, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import * as R from 'ramda';

import {defaultBottomMatDialogConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {evodeskUserSettingsMaxSkillLength, evodeskUserSettingsMinSkillLength} from '../../configs/EvodeskUserSettings.constants';

import {EvodeskUserSettingsCompetenceEditFileComponent as EditFileModal} from '../EvodeskUserSettingsCompetenceEditFile/EvodeskUserSettingsCompetenceEditFile.component';

import {CompetenceCategoryId, CompetenceCategoryModel} from '../../../../app/EvodeskRESTApi/models/CompetenceCategory.model';
import {CompetenceId, CompetenceModel} from '../../../../app/EvodeskRESTApi/models/Competence.model';
import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';
import {UserCompetenceDocumentModel, UserCompetenceDocumentStatus} from '../../../../app/EvodeskRESTApi/models/UserCompetenceDocument.model';
import {EvodeskUserSettingsDocumentCommentBodyParserService} from '../../services/EvodeskUserSettingsDocumentCommentBodyParser.service';

export interface Props {
    forms: Array<FormGroup>;
    availableCompetenceCategories: Array<CompetenceCategoryModel>;
    maxCompetencesAllowed: number;
}

interface State {
    ready: boolean;
    addSkillForm: FormGroup;
    isAddSkillFormOpened: number | undefined;
}

interface FormValueUploadDocumentEntry {
    file: Blob;
    title: string;
}

interface FormValue {
    id: number;
    competenceCategoryId: CompetenceCategoryId;
    competenceId: CompetenceId;
    skills: Array<string>;
    active: boolean;
    documents: Array<UserCompetenceDocumentModel>;
    uploadDocuments: Array<FormValueUploadDocumentEntry>;
}

interface AddSkillFormValue {
    title: string;
}

interface SetCompetenceEvent {
    index: number;
    competenceId: CompetenceId;
}

let nextMinusId: number = -1;

export {Props as EvodeskUserSettingsCompetenceComponentProps};
export {FormValue as EvodeskUserSettingsCompetenceComponentFormValue};

interface Options {
    withUserCompetence?: UserCompetenceModel;
    withCompetenceId?: CompetenceId;
    withSkills?: Array<string>;
    withDocuments?: Array<UserCompetenceDocumentModel>;
}

export function evodeskUserSettingsCompetenceComponentFormBuilder(fb: FormBuilder, options: Options  = {}): FormGroup {
    const form: FormGroup = fb.group({
        id: [--nextMinusId],
        competenceCategoryId: [undefined, [Validators.required]],
        competenceId: [undefined, [Validators.required]],
        active: [false],
        skills: [[]],
        documents: [[]],
        uploadDocuments: [[]],
    });

    if (options.withUserCompetence) {
        form.patchValue({
            id: options.withUserCompetence.id,
            competenceId: options.withUserCompetence.competence.id,
            active: options.withUserCompetence.active === 1,
            skills: [],
        });
    } else if (options.withCompetenceId) {
        form.patchValue({
            id: --nextMinusId,
            competenceId: options.withCompetenceId,
            active: true,
            skills: [],
        });
    }

    if (options.withSkills) {
        form.patchValue({
            skills: options.withSkills,
        });
    }

    if (options.withDocuments) {
        form.patchValue({
            documents: options.withDocuments,
        });
    }


    return form;
}

@Component({
    selector: 'evodesk-user-settings-competence',
    templateUrl: './EvodeskUserSettingsCompetence.component.pug',
    styleUrls: [
        './EvodeskUserSettingsCompetence.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsCompetenceComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    @Output('add') addEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('remove') removeEvent: EventEmitter<{ index: number }> = new EventEmitter<{ index: number }>();
    @Output('setCompetence') setCompetenceEvent: EventEmitter<SetCompetenceEvent> = new EventEmitter<SetCompetenceEvent>();

    @ViewChildren('addSkillFormInput') addSkillFormInputRefs: QueryList<ElementRef>;
    @ViewChildren('uploadDocumentsInput') uploadDocumentsInputRefs: QueryList<ElementRef>;

    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private modalClose$: Subject<void> = new Subject<void>();

    public state: State;

    constructor(
        private cdr: ChangeDetectorRef,
        private fb: FormBuilder,
        private viewContainerRef: ViewContainerRef,
        private matDialog: MatDialog,
        private documentCommentBodyParser: EvodeskUserSettingsDocumentCommentBodyParserService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    addSkillForm: this.fb.group({
                        title: ['', [Validators.required, Validators.minLength(evodeskUserSettingsMinSkillLength), Validators.maxLength(evodeskUserSettingsMaxSkillLength)]],
                    }),
                };

                this.props.forms.forEach((f) => {
                    const formValue: FormValue = f.value;

                    if (formValue.competenceId && ! formValue.competenceCategoryId) {
                        f.patchValue({
                            competenceCategoryId: (() => {
                                for (const cc of this.props.availableCompetenceCategories) {
                                    if (!!~cc.competences.map(c => c.id).indexOf(formValue.competenceId)) {
                                        return cc.id;
                                    }
                                }

                                return undefined;
                            })(),
                        });
                    }
                });
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.modalClose$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    get canAdd(): boolean {
        return (this.props.maxCompetencesAllowed < 0) || (this.props.forms.length < this.props.maxCompetencesAllowed);
    }

    category(form: FormGroup): CompetenceCategoryModel {
        return this.props.availableCompetenceCategories.filter(c => c.id === parseInt(<any>(form.value as FormValue).competenceCategoryId, 10))[0];
    }

    competence(form: FormGroup): CompetenceModel {
        return this.category(form).competences.filter(c => c.id === parseInt(<any>(form.value as FormValue).competenceId, 10))[0];
    }

    isFormEnabled(f: FormGroup): boolean {
        return (f.value as FormValue).active === true || (f.value as FormValue).id < 0;
    }

    add(): void {
        this.addEvent.emit(undefined);
    }

    remove(index: number): void {
        this.removeEvent.emit({
            index: index,
        });
    }

    skills(form: FormGroup): Array<string> {
        return (form.value as FormValue).skills;
    }

    addSkill(form: FormGroup, index: number): void {
        if (this.state.addSkillForm.valid) {
            form.patchValue({
                skills: R.uniqBy(input => input.toLocaleLowerCase(), [...(form.value.skills || []), (this.state.addSkillForm.value as AddSkillFormValue).title.trim()]),
            });

            this.state.addSkillForm.reset();
            this.openAddSkillForm(index);
            this.cdr.detectChanges();
        }
    }

    deleteSkill(form: FormGroup, skill: string): void {
        form.patchValue({
            skills: form.value.skills.filter(s => s !== skill),
        });
    }

    isAddFormOpened(form: FormGroup, index: number): boolean {
        return (this.state.isAddSkillFormOpened !== undefined && this.state.isAddSkillFormOpened === index) || ((form.value as FormValue).skills.length === 0);
    }

    openAddSkillForm(index: number): void {
        this.state = {
            ...this.state,
            isAddSkillFormOpened: index,
        };

        setTimeout(() => {
            this.focusAddSkillsForm(index);
        });
    }

    hasSkills(form: FormGroup): boolean {
        return (form.value as FormValue).skills.length > 0;
    }

    closeAddSkillForm(): void {
        this.state.addSkillForm.reset();

        this.state = {
            ...this.state,
            isAddSkillFormOpened: undefined,
        };
    }

    focusAddSkillsForm(index: number): void {
        setTimeout(() => {
            if (this.addSkillFormInputRefs[index]) {
                (this.addSkillFormInputRefs[index].nativeElement as HTMLElement).focus();
            }
        });
    }

    onFileInputChange(form: FormGroup, fileInputRef: HTMLInputElement): void {
        const files: FileList = fileInputRef.files;
        const requests: Array<FormValueUploadDocumentEntry> = [];

        for (let i: number = 0; i < files.length; i++) {
            requests.push({
                file: files[i],
                title: files[i].name,
            });
        }

        fileInputRef.value = '';

        form.patchValue({
            uploadDocuments: requests,
        });
    }

    hasDocuments(form: FormGroup): boolean {
        return (form.value as FormValue).documents.length > 0;
    }

    hasUploadDocuments(form: FormGroup): boolean {
        return !! (form.value as FormValue).uploadDocuments && (form.value as FormValue).uploadDocuments.length > 0;
    }

    uploadDocuments(form: FormGroup): Array<FormValueUploadDocumentEntry> {
        return (form.value as FormValue).uploadDocuments;
    }

    editUploadDocument(form: FormGroup, index: number): void {
        const uploadDocument: FormValueUploadDocumentEntry = (form.value as FormValue).uploadDocuments[index];

        const matDialogRef: MatDialogRef<EditFileModal> = this.matDialog.open(EditFileModal, {
            ...defaultBottomMatDialogConfig,
            viewContainerRef: this.viewContainerRef,
            closeOnNavigation: true,
            autoFocus: true,
        });

        matDialogRef.afterClosed().pipe(takeUntil(this.modalClose$)).subscribe(() => {
            this.modalClose$.next(undefined);
        });

        matDialogRef.componentInstance.setTitle(uploadDocument.title);

        matDialogRef.componentInstance.closeEvent.pipe(takeUntil(this.modalClose$)).subscribe(() => {
            this.modalClose$.next(undefined);

            matDialogRef.close(undefined);
        });

        matDialogRef.componentInstance.editEvent.pipe(takeUntil(this.modalClose$)).subscribe((editEvent) => {
            uploadDocument.title = editEvent.title;

            this.cdr.detectChanges();
        });
    }

    removeUploadDocument(form: FormGroup, index: number): void {
        const current: Array<FormValueUploadDocumentEntry> = (form.value as FormValue).uploadDocuments;

        form.patchValue({
            uploadDocuments: [...current.slice(0, index), ...current.slice(index + 1)],
        });
    }

    documents(form: FormGroup): Array<UserCompetenceDocumentModel> {
        return (form.value as FormValue).documents;
    }

    documentStatusCSSClasses(document: UserCompetenceDocumentModel): any {
        return {
            'status-pending': this.isDocumentPending(document),
            'status-accepted': this.isDocumentAccepted(document),
            'status-rejected': this.isDocumentRejected(document),
        };
    }

    isDocumentPending(document: UserCompetenceDocumentModel): boolean {
        return document.status === UserCompetenceDocumentStatus.Pending;
    }

    isDocumentRejected(document: UserCompetenceDocumentModel): boolean {
        return document.status === UserCompetenceDocumentStatus.Accepted;
    }

    isDocumentAccepted(document: UserCompetenceDocumentModel): boolean {
        return document.status === UserCompetenceDocumentStatus.Rejected;
    }

    editDocument(document: UserCompetenceDocumentModel): void {
        const matDialogRef: MatDialogRef<EditFileModal> = this.matDialog.open(EditFileModal, {
            ...defaultBottomMatDialogConfig,
            viewContainerRef: this.viewContainerRef,
            closeOnNavigation: true,
            autoFocus: true,
        });

        matDialogRef.afterClosed().pipe(takeUntil(this.modalClose$)).subscribe(() => {
            this.modalClose$.next(undefined);
        });

        matDialogRef.componentInstance.setTitle(document.title);

        matDialogRef.componentInstance.closeEvent.pipe(takeUntil(this.modalClose$)).subscribe(() => {
            this.modalClose$.next(undefined);

            matDialogRef.close(undefined);
        });

        matDialogRef.componentInstance.editEvent.pipe(takeUntil(this.modalClose$)).subscribe((editEvent) => {
            document.title = editEvent.title;

            this.cdr.detectChanges();
        });
    }

    removeDocument(form: FormGroup, index: number): void {
        const current: Array<UserCompetenceDocumentModel> = (form.value as FormValue).documents;

        form.patchValue({
            documents: [...current.slice(0, index), ...current.slice(index + 1)],
        });
    }

    documentAdminComment(document: UserCompetenceDocumentModel): Observable<string> {
        return this.documentCommentBodyParser.parseToHtml(document.admin_comment || '');
    }
}
