import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {CompetenceCategoryId, CompetenceCategoryModel} from '../../../../app/EvodeskRESTApi/models/CompetenceCategory.model';
import {CompetenceId, CompetenceModel} from '../../../../app/EvodeskRESTApi/models/Competence.model';

interface Props {
    form: FormGroup;
    usedCompetences: Array<CompetenceId>;
    availableCompetenceCategories: Array<CompetenceCategoryModel>;
}

interface State {
    ready: boolean;
    competenceCategoriesCompletelyUsed: Array<CompetenceId>;
    areAllCompetenceCategoriesCompletelyUsed: boolean;
}

interface FormValue {
    competenceCategoryId: CompetenceCategoryId;
    competenceId: CompetenceId;
}

export {Props as EvodeskUserSettingsCompetenceAddComponentProps};
export {FormValue as EvodeskUserSettingsCompetenceAddComponentFormValue};

export function evodeskUserSettingsCompetenceAddFormBuilder(fb: FormBuilder): FormGroup {
    return fb.group({
        competenceCategoryId: [undefined, [Validators.required]],
        competenceId: [undefined, [Validators.required]],
    });
}

@Component({
    selector: 'evodesk-user-settings-competence-add',
    templateUrl: './EvodeskUserSettingsCompetenceAdd.component.pug',
    styleUrls: [
        './EvodeskUserSettingsCompetenceAdd.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsCompetenceAddComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('ngSubmit') ngSubmitEvent: EventEmitter<FormValue> = new EventEmitter<FormValue>();

    public state: State = {
        ready: false,
        competenceCategoriesCompletelyUsed: [],
        areAllCompetenceCategoriesCompletelyUsed: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        const isCompetenceCategoryCompletelyUsed: Function = (category: CompetenceCategoryModel) => {
            return category.competences
                .map(c => c.id)
                .every(cId => this.isCompetenceUsed(cId));
        };

        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    competenceCategoriesCompletelyUsed: this.props.availableCompetenceCategories.filter(c => isCompetenceCategoryCompletelyUsed(c)).map(c => c.id),
                    areAllCompetenceCategoriesCompletelyUsed: this.props.availableCompetenceCategories.every(c => isCompetenceCategoryCompletelyUsed(c)),
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get hasCompetenceCategory(): boolean {
        return !! ((this.props.form.value as FormValue).competenceCategoryId);
    }

    get availableCompetences(): Array<CompetenceModel> {
        const ccId: CompetenceCategoryId = (this.props.form.value as FormValue).competenceCategoryId;

        if (!! ccId) {
            return this.props.availableCompetenceCategories.filter(c => c.id.toString() === ccId.toString())[0].competences;
        } else {
            return [];
        }
    }

    get canSubmit(): boolean {
        return this.props.form.valid;
    }

    isCompetenceUsed(competenceId: CompetenceId): boolean {
        return !!~this.props.usedCompetences.indexOf(competenceId);
    }

    isCompetenceCategoryCompletelyUsed(category: CompetenceCategoryModel): boolean {
        return !!~this.state.competenceCategoriesCompletelyUsed.indexOf(category.id);
    }

    ngSubmit(): void {
        if (this.props.form.valid) {
            this.ngSubmitEvent.emit(this.props.form.value);
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
