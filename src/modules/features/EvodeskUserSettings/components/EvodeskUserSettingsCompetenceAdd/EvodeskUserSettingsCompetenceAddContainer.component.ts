import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder} from '@angular/forms';

import {take, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

import {EvodeskUserSettingsCompetenceAddComponentFormValue, EvodeskUserSettingsCompetenceAddComponentProps, evodeskUserSettingsCompetenceAddFormBuilder} from './EvodeskUserSettingsCompetenceAdd.component';
import {EvodeskUserSettingsCompetenceComponentFormValue} from '../EvodeskUserSettingsCompetence/EvodeskUserSettingsCompetence.component';

import {EvodeskUserSettingsStateAction, EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';

type Props = EvodeskUserSettingsCompetenceAddComponentProps;

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-user-settings-competence-add-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-user-settings-competence-add [props]="state.props" (close)="close()" (ngSubmit)="ngSubmit($event)"></evodesk-user-settings-competence-add>
      </ng-container>
    `,
})
export class EvodeskUserSettingsCompetenceAddContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private fb: FormBuilder,
        private usState: EvodeskUserSettingsStateService,
    ) {}

    ngOnInit(): void {
        this.usState.current$.pipe(takeUntil(this.ngOnDestroy$), take(1)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    form: evodeskUserSettingsCompetenceAddFormBuilder(this.fb),
                    availableCompetenceCategories: s.current.availableCompetenceCategories,
                    usedCompetences: s.current.forms.formsCompetence
                        .filter((f) => (f.value as EvodeskUserSettingsCompetenceComponentFormValue).active)
                        .map((f) => (f.value as EvodeskUserSettingsCompetenceComponentFormValue).competenceId),
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    ngSubmit($event: EvodeskUserSettingsCompetenceAddComponentFormValue): void {
        this.usState.dispatch({
            type: EvodeskUserSettingsStateAction.AddCompetenceForm,
            payload: {
                competenceId: parseInt(<any>$event.competenceId, 10),
            },
        });

        this.close();
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
