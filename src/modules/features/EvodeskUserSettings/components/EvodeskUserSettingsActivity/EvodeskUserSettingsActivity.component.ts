import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';

import * as moment from 'moment';

import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';
import {UserActivityModel} from '../../../../app/EvodeskRESTApi/models/user/UserActivity.model';
import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../types/Backend.types';

export function backendActivityToFrontendActivity(input: UserActivityModel): Entry {
    return {
        date: moment(input.datetime, BACKEND_DATE_FORMAT_AS_MOMENT).toDate(),
        browser: input.browser,
        location: input.place,
        os: input.device,
    };
}

export interface EvodeskUserSettingsActivityComponentPropsEntry {
    os: string;
    browser: string;
    location: string;
    date: Date;
}

interface StateEntry {
    entry: Entry;
    isOnline: boolean;
    date: Observable<string>;
}

export interface EvodeskUserSettingsActivityComponentProps {
    entries: Array<EvodeskUserSettingsActivityComponentPropsEntry>;
    hasMoreEntries: boolean;
    isLoading: boolean;
}

interface State {
    ready: boolean;
    entries: Array<StateEntry>;
}

type Entry = EvodeskUserSettingsActivityComponentPropsEntry;

@Component({
    selector: 'evodesk-user-settings-activity',
    templateUrl: './EvodeskUserSettingsActivity.component.pug',
    styleUrls: [
        './EvodeskUserSettingsActivity.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsActivityComponent implements OnChanges
{
    @Input() props: EvodeskUserSettingsActivityComponentProps;

    @Output('loadMore') loadMoreEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State;

    constructor(
        private evodeskDateService: EvodeskDateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    entries: (() => {
                        const entries: Array<StateEntry> = this.props.entries.map((e) => {
                            return <StateEntry>{
                                entry: e,
                                date: this.evodeskDateService.diffFromNowV3(e.date),
                                isOnline: false,
                            };
                        });

                        entries.sort((a, b) => {
                            if (a.entry.date > b.entry.date) {
                                return -1;
                            } else if (a.entry.date.getTime() === b.entry.date.getTime()) {
                                return 0;
                            } else {
                                return 1;
                            }
                        });

                        if (entries.length) {
                            entries[0].isOnline = true;
                        }

                        return entries;
                    })(),
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    loadMore(): void {
        if (this.props.hasMoreEntries && ! this.props.isLoading) {
            this.loadMoreEvent.emit(undefined);
        }
    }
}
