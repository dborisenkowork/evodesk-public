import {ChangeDetectorRef, Component, Input, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {backendActivityToFrontendActivity, EvodeskUserSettingsActivityComponentProps} from './EvodeskUserSettingsActivity.component';

import {EvodeskUserSettingsStateAction, EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';
import {UserRESTService} from '../../../../app/EvodeskRESTApi/services/UserREST.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';

interface State {
    ready: boolean;
    page: number;
    isLoading: boolean;
    props?: EvodeskUserSettingsActivityComponentProps;
}

export const defaultUserActivityPageSize: number = 5;


@Component({
    selector: 'evodesk-user-settings-activity-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-user-settings-activity [props]="state.props" (loadMore)="onLoadMore()"></evodesk-user-settings-activity>
      </ng-container>
    `,
})
export class EvodeskUserSettingsActivityContainerComponent implements OnInit, OnDestroy
{
    @Input() entriesPerPage: number = defaultUserActivityPageSize;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        page: 0,
        isLoading: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private userSettingsState: EvodeskUserSettingsStateService,
        private userRESTService: UserRESTService,
        private uiAlertService: EvodeskAlertModalService,
    ) {}

    ngOnInit(): void {
        this.userSettingsState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    entries: s.current.userActivities.map(sE => backendActivityToFrontendActivity(sE)),
                    hasMoreEntries: s.current.hasMoreUserActivitiesToLoad,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onLoadMore(): void {
        this.state = {
            ...this.state,
            isLoading: true,
            page: this.state.page + 1,
            props: {
                ...this.state.props,
                isLoading: true,
            },
        };

        this.userRESTService.getActivity({
            offset: this.entriesPerPage * this.state.page,
            limit: this.entriesPerPage + 1,
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
            (httpResponse) => {
                this.state = {
                    ...this.state,
                    isLoading: false,
                    props: {
                        ...this.state.props,
                        isLoading: false,
                    },
                };

                this.userSettingsState.dispatch({
                    type: EvodeskUserSettingsStateAction.PushUserActivities,
                    payload: {
                        userActivities: httpResponse.slice(0, this.entriesPerPage),
                        hasMoreToLoad: httpResponse.length > this.entriesPerPage,
                    },
                });

                this.cdr.detectChanges();
            },
            () => {
                this.state = {
                    ...this.state,
                    isLoading: false,
                    page: this.state.page - 1,
                    props: {
                        ...this.state.props,
                        isLoading: false,
                    },
                };

                this.uiAlertService.httpError().pipe(takeUntil(this.ngOnDestroy$)).subscribe();

                this.cdr.detectChanges();
            },
        );
    }
}
