import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {EvodeskUserSettingsNavigation, EvodeskUserSettingsViewMode} from '../../models/EvodeskUserSettings.model';

export interface EvodeskUserSettingsComponentProps {
    currentNav: EvodeskUserSettingsNavigation;
    canChangePassword: boolean;
    arePasswordsSame: boolean;
    viewMode: EvodeskUserSettingsViewMode;
}

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-user-settings',
    templateUrl: './EvodeskUserSettings.component.pug',
    styleUrls: [
        './EvodeskUserSettings.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsComponent implements OnChanges
{
    @Input() props: EvodeskUserSettingsComponentProps;

    @Output('saveChanges') saveChangesEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('savePasswordChanges') savePasswordChangesEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State;

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props && this.props.currentNav && this.props.viewMode) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    saveChanges(): void {
        this.saveChangesEvent.emit(undefined);
    }

    savePasswordChanges(): void {
        this.savePasswordChangesEvent.emit(undefined);
    }

    get shouldDisplayMobileNavigation(): boolean {
        return this.props.viewMode !== EvodeskUserSettingsViewMode.Desktop;
    }
}
