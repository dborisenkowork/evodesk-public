import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import {EvodeskUserSettingsComponentProps} from './EvodeskUserSettings.component';

import {EvodeskUserSettingsNavigation} from '../../models/EvodeskUserSettings.model';

import {EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';
import {EvodeskUserSettingsBootstrapService} from '../../services/EvodeskUserSettingsBootstrap.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {EvodeskUserSaveChangesService} from '../../services/EvodeskUserSaveChanges.service';
import {EvodeskAppLoading, EvodeskAppLoadingStatusService} from '../../../../app/EvodeskApp/services/EvodeskAppLoadingStatus.service';

interface State {
    ready: boolean;
    props?: EvodeskUserSettingsComponentProps;
}

@Component({
    selector: 'evodesk-user-settings-container',
    template: `
      <ng-container *ngIf="!!state && state.ready && state.props">
        <evodesk-user-settings [props]="state.props" (go)="go($event)" (saveChanges)="saveChanges()" (savePasswordChanges)="savePasswordChanges()"></evodesk-user-settings>
      </ng-container>
    `,
})
export class EvodeskUserSettingsContainerComponent implements OnDestroy, OnInit
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private router: Router,
        private appLoading: EvodeskAppLoadingStatusService,
        private userSettingsState: EvodeskUserSettingsStateService,
        private userSettingsBootstrap: EvodeskUserSettingsBootstrapService,
        private userSettingsSaveChanges: EvodeskUserSaveChangesService,
        private uiAlertService: EvodeskAlertModalService,
    ) {}

    ngOnInit(): void {
        this.state = {
            ...this.state,
            ready: true,
            props: {
                ...this.state.props,
                canChangePassword: false,
            },
        };

        this.userSettingsBootstrap.bootstrap().pipe(takeUntil(this.ngOnDestroy$)).subscribe(
            () => {
                this.userSettingsState.current$.pipe(
                    takeUntil(this.ngOnDestroy$),
                    map(s => s.current.currentNav),
                    distinctUntilChanged(),
                ).subscribe((currentNav) => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            currentNav: currentNav,
                        },
                    };

                    this.cdr.detectChanges();
                });

                this.userSettingsState.current$.pipe(
                    takeUntil(this.ngOnDestroy$),
                    map(s => s.current.viewMode),
                    distinctUntilChanged(),
                ).subscribe((viewMode) => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            viewMode: viewMode,
                        },
                    };

                    this.cdr.detectChanges();
                });

                this.userSettingsState.snapshot.forms.formPassword.valueChanges.pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            canChangePassword: this.canChangePassword,
                            arePasswordsSame: this.arePasswordsSame,
                        },
                    };

                    this.cdr.detectChanges();
                });

                setTimeout(() => {
                    this.state = {
                        ...this.state,
                        ready: true,
                    };
                });
            },
            () => {
                this.uiAlertService.openTranslated(`EvodeskUserSettings.components.EvodeskUserSettingsContainer.FailedToBootstrap`).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
            },
        );
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    go(newNav: EvodeskUserSettingsNavigation): void {
        this.router.navigate(['/user-settings'], {
            queryParams: {
                'section': newNav,
            },
        });
    }

    saveChanges(): void {
        const loading: EvodeskAppLoading = this.appLoading.addLoading();

        this.userSettingsSaveChanges.saveChanges().subscribe(
            () => {
                loading.done();
            },
            () => {
                loading.done();
            },
        );
    }

    savePasswordChanges(): void {
        if (this.userSettingsSaveChanges.canChangePassword) {
            const loading: EvodeskAppLoading = this.appLoading.addLoading();

            this.userSettingsSaveChanges.savePasswordChanges().pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                () => {
                    loading.done();
                },
                () => {
                    loading.done();
                },
            );
        }
    }

    get canChangePassword(): boolean {
        return this.userSettingsSaveChanges.canChangePassword;
    }

    get arePasswordsSame(): boolean {
        return this.userSettingsSaveChanges.arePasswordsSame;
    }
}
