import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';
import {FormGroup} from '@angular/forms';

import {forkJoin, Observable, Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import * as R from 'ramda';
import * as moment from 'moment';

import {proxy} from '../../../../../functions/proxy.function';

import {ProfileExperienceId} from '../../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';
import {WorkCompetenceApproveModel} from '../../../../app/EvodeskRESTApi/models/WorkCompetenceApprove.model';

import {ApproveRequest, EvodeskUserSettingsWorkExperienceComponentApproveRequestStatus, EvodeskUserSettingsWorkExperienceComponentFormValue} from '../EvodeskUserSettingsWorkExperience/EvodeskUserSettingsWorkExperience.component';
import {EvodeskUserSettingsWorkExperienceConfirmComponentCancelRequest, EvodeskUserSettingsWorkExperienceConfirmComponentEntry, EvodeskUserSettingsWorkExperienceConfirmComponentProps, EvodeskUserSettingsWorkExperienceConfirmComponentSearchRequest, EvodeskUserSettingsWorkExperienceConfirmComponentSendRequest} from './EvodeskUserSettingsWorkExperienceConfirm.component';

import {GetRelationsResponse200Relation, ProfileRESTService} from '../../../../app/EvodeskRESTApi/services/ProfileREST.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';
import {SearchRESTService} from '../../../../app/EvodeskRESTApi/services/SearchREST.service';
import {ProfileWorkExperienceRESTService} from '../../../../app/EvodeskRESTApi/services/ProfileWorkExperienceREST.service';
import {EvodeskUserSettingsStateAction, EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';
import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../types/Backend.types';

type Props = EvodeskUserSettingsWorkExperienceConfirmComponentProps;
type FormValue = EvodeskUserSettingsWorkExperienceComponentFormValue;
type SearchRequest = EvodeskUserSettingsWorkExperienceConfirmComponentSearchRequest;
type Entry = EvodeskUserSettingsWorkExperienceConfirmComponentEntry;
type SendRequest = EvodeskUserSettingsWorkExperienceConfirmComponentSendRequest;
type CancelRequest = EvodeskUserSettingsWorkExperienceConfirmComponentCancelRequest;

interface State {
    ready: boolean;
    props?: Props;
    requests: Array<WorkCompetenceApproveModel>;
    relations: Array<GetRelationsResponse200Relation>;
    sending: Array<SendRequest>;
}

@Component({
    selector: 'evodesk-user-settings-work-experience-confirm-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-user-settings-work-experience-confirm
          [props]="state.props"
          (close)="close()"
          (search)="search($event)"
          (send)="send($event)"
          (cancel)="cancel($event)"
        ></evodesk-user-settings-work-experience-confirm>
      </ng-container>
    `,
})
export class EvodeskUserSettingsWorkExperienceConfirmContainerComponent implements OnChanges, OnDestroy
{
    @Input() experienceId: ProfileExperienceId;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private ngOnChanges$: Subject<void> = new Subject<void>();
    private nextSearch$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        relations: [],
        requests: [],
        sending: [],
        props: {
            experienceId: -1,
            loading: 0,
            searching: false,
            hasRelations: true,
            entries: [],
            workExperienceId: -1,
            hasRequestsFor: [],
            queue: [],
            requestsApproved: [],
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private usState: EvodeskUserSettingsStateService,
        private currentUser: CurrentUserService,
        private restProfile: ProfileRESTService,
        private restSearch: SearchRESTService,
        private restWorkExperience: ProfileWorkExperienceRESTService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                experienceId: this.experienceId,
                loading: 1,
            },
        };

        this.cdr.detectChanges();

        forkJoin(this.restProfile.getFollowers(this.currentUser.impersonatedAs.id), this.restProfile.getFollowing(this.currentUser.impersonatedAs.id)).pipe(takeUntil(this.ngOnChanges$)).subscribe(
            (httpResponse) => {
                const setUpEntries: Function = () => {
                    this.state = {
                        ...this.state,
                        relations: [...httpResponse[0].followers, ...httpResponse[1].following],
                    };

                    this.main();

                    this.cdr.detectChanges();
                };

                const setUpHasRequestsFor: Function = (approveRequests: Array<ApproveRequest>) => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            hasRequestsFor: approveRequests
                                .filter(a => !!~[
                                    EvodeskUserSettingsWorkExperienceComponentApproveRequestStatus.Sent,
                                    EvodeskUserSettingsWorkExperienceComponentApproveRequestStatus.WillBeSent,
                                ].indexOf(a.status))
                                .filter(a => ! a.request.approve || a.request.approve.status !== 2)
                                .map(a => a.request.approverProfileId),
                            requestsApproved: approveRequests
                                .filter(a => !! a.request.approve && a.request.approve.status === 1)
                                .map(a => a.request.approverProfileId),
                        },
                    };

                    this.cdr.detectChanges();
                };

                this.form.valueChanges.pipe(
                    takeUntil(this.ngOnChanges$),
                    map((fv: EvodeskUserSettingsWorkExperienceComponentFormValue) => fv.approveRequests),
                    distinctUntilChanged(),
                ).subscribe((approveRequests) => {
                    setUpHasRequestsFor(approveRequests);
                });

                setUpEntries();
                setUpHasRequestsFor((this.form.value as FormValue).approveRequests);
            },
            () => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        loading: 0,
                    },
                };

                this.cdr.detectChanges();
            },
        );
    }

    ngOnDestroy(): void {
        this.nextSearch$.next(undefined);
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    get form(): FormGroup {
        return this.usState.snapshot.forms.formsExperience.filter(f => f.get('id').value === this.experienceId)[0];
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    main(): void {
        const form: FormGroup = this.form;
        const mapRequests: { [profileId: number]: ApproveRequest } = {};

        (form.value as FormValue).approveRequests.forEach(r => mapRequests[r.request.approverProfileId] = r);

        const entries: Array<Entry> = R.uniqBy((input: Entry) => input.id, [
            ...this.state.relations.map((r) => {
                return {
                    ...r,
                    approve: mapRequests[r.id] ? mapRequests[r.id].request.approve : undefined,
                };
            }),
            ...(form.value as FormValue).approveRequests
                .filter(r => !! r.request.approve)
                .map(r => {
                    return <Entry>{
                        id: r.request.approve.user_id,
                        approve: r.request.approve,
                        position: r.request.approve.user_position,
                        email: r.request.approve.user_email,
                        name: r.request.approve.user_name,
                    };
                }),
        ]);

        entries.sort((a, b) => {
            return a.id - b.id;
        });

        this.state = {
            ...this.state,
            relations: entries,
            props: {
                ...this.state.props,
                loading: 0,
                hasRelations: entries.length > 0,
                entries: entries,
                workExperienceId: this.experienceId,
            },
        };
    }

    search(request: SearchRequest): void {
        this.nextSearch$.next(undefined);

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                searching: false,
            },
        };

        if (request.query) {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    searching: true,
                },
            };

            this.restSearch.search({ queryString: request.query }).subscribe(
                (httpResponse) => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            searching: false,
                            entries: httpResponse.profiles.map((entry) => {
                                return <Entry>{
                                    id: entry.id,
                                    name: entry.name,
                                    email: entry.email,
                                    position: entry.position,
                                };
                            }),
                        },
                    };

                    this.cdr.detectChanges();
                },
                () => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            searching: false,
                        },
                    };

                    this.cdr.detectChanges();
                },
            );
        } else {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    searching: false,
                },
            };

            this.main();
        }

        this.cdr.detectChanges();
    }

    send(request: SendRequest): void {
        if (this.state.sending.filter(e => e.workExperienceId === request.workExperienceId).length === 0) {
            if (request.workExperienceId > 0) {
                this.startLoading(request);

                this.usState.dispatch({
                    type: EvodeskUserSettingsStateAction.WorkExperienceApproveSending,
                    payload: {
                        workExperienceId: request.workExperienceId,
                        approverProfileId: request.entry.id,
                    },
                });

                const httpRequest$: Observable<WorkCompetenceApproveModel> = this.restWorkExperience.sendApproveRequest(request.workExperienceId, request.entry.id);

                httpRequest$.pipe(takeUntil(this.usState.destroy$)).subscribe(
                    (httpResponse) => {
                        this.usState.dispatch({
                            type: EvodeskUserSettingsStateAction.WorkExperienceApproveSetApprove,
                            payload: {
                                workExperienceId: request.workExperienceId,
                                approverProfileId: request.entry.id,
                                approve: httpResponse,
                            },
                        });

                        this.usState.dispatch({
                            type: EvodeskUserSettingsStateAction.WorkExperienceApproveSent,
                            payload: {
                                workExperienceId: request.workExperienceId,
                                approverProfileId: request.entry.id,
                            },
                        });
                    },
                    () => {
                        this.usState.dispatch({
                            type: EvodeskUserSettingsStateAction.WorkExperienceApproveFail,
                            payload: {
                                workExperienceId: request.workExperienceId,
                                approverProfileId: request.entry.id,
                            },
                        });
                    },
                );

                proxy(httpRequest$).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                    () => {
                        this.stopLoading(request);
                    },
                    () => {
                        this.stopLoading(request);
                    },
                );

                this.state = {
                    ...this.state,
                    sending: this.state.sending.filter(e => e.workExperienceId !== request.workExperienceId),
                };
            } else {
                this.usState.dispatch({
                    type: EvodeskUserSettingsStateAction.WorkExperienceApproveSend,
                    payload: {
                        approve: {
                            id: undefined,
                            status: 0,
                            created_at: moment(new Date()).format(BACKEND_DATE_FORMAT_AS_MOMENT),
                            experience_id: this.experienceId,
                            user_id: request.entry.id,
                            user_name: request.entry.name,
                            user_email: request.entry.email,
                            user_position: request.entry.position,
                        },
                        workExperienceId: request.workExperienceId,
                        approverProfileId: request.entry.id,
                    },
                });
            }
        }
    }

    cancel(request: CancelRequest): void {
        if (this.state.sending.filter(e => e.workExperienceId === request.workExperienceId).length === 0) {
            if (request.workExperienceId > 0) {
                this.startLoading(request);

                this.usState.dispatch({
                    type: EvodeskUserSettingsStateAction.WorkExperienceApproveCanceling,
                    payload: {
                        workExperienceId: request.workExperienceId,
                        approverProfileId: request.entry.id,
                    },
                });

                const form: FormGroup = this.form;

                const httpRequest$: Observable<WorkCompetenceApproveModel> = this.restWorkExperience.cancelApproveRequest(
                    request.workExperienceId,
                    (form.value as FormValue).approveRequests.filter(r => r.request.approverProfileId === request.entry.id)[0].request.approve.id,
                );

                httpRequest$.pipe(takeUntil(this.usState.destroy$)).subscribe(
                    () => {
                        this.usState.dispatch({
                            type: EvodeskUserSettingsStateAction.WorkExperienceApproveSetApprove,
                            payload: {
                                workExperienceId: request.workExperienceId,
                                approverProfileId: request.entry.id,
                                approve: undefined,
                            },
                        });

                        this.usState.dispatch({
                            type: EvodeskUserSettingsStateAction.WorkExperienceApproveCancel,
                            payload: {
                                workExperienceId: request.workExperienceId,
                                approverProfileId: request.entry.id,
                            },
                        });

                        this.cdr.detectChanges();
                    },
                    () => {
                        this.usState.dispatch({
                            type: EvodeskUserSettingsStateAction.WorkExperienceApproveFail,
                            payload: {
                                workExperienceId: request.workExperienceId,
                                approverProfileId: request.entry.id,
                            },
                        });

                        this.cdr.detectChanges();
                    },
                );

                proxy(httpRequest$).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                    () => {
                        this.stopLoading(request);
                    },
                    () => {
                        this.stopLoading(request);
                    },
                );

                this.state = {
                    ...this.state,
                    sending: this.state.sending.filter(e => e.workExperienceId !== request.workExperienceId),
                };
            } else {
                this.usState.dispatch({
                    type: EvodeskUserSettingsStateAction.WorkExperienceApproveCancel,
                    payload: {
                        workExperienceId: request.workExperienceId,
                        approverProfileId: request.entry.id,
                    },
                });
            }
        }
    }

    private startLoading(request: CancelRequest | SendRequest): void {
        this.state = {
            ...this.state,
            sending: [...this.state.sending, request],
            props: {
                ...this.state.props,
                loading: this.state.props.loading + 1,
                queue: [...this.state.props.queue, request.entry.id],
            },
        };

        this.cdr.detectChanges();
    }

    private stopLoading(request: CancelRequest | SendRequest): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                loading: this.state.props.loading - 1,
                queue: this.state.props.queue.filter(q => q !== request.entry.id),
            },
        };

        this.cdr.detectChanges();
    }
}
