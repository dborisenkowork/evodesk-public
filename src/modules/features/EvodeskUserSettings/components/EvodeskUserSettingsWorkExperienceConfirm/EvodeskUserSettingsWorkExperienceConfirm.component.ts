import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';

import {ProfileExperienceId} from '../../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';
import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {WorkCompetenceApproveModel} from '../../../../app/EvodeskRESTApi/models/WorkCompetenceApprove.model';

export interface EvodeskUserSettingsWorkExperienceConfirmComponentProps
{
    experienceId: ProfileExperienceId;
    loading: number;
    searching: boolean;
    hasRelations: boolean;
    entries: Array<Entry>;
    workExperienceId: ProfileExperienceId;
    hasRequestsFor: Array<ProfileId>;
    requestsApproved: Array<ProfileId>;
    queue: Array<ProfileId>;
}

type Props = EvodeskUserSettingsWorkExperienceConfirmComponentProps;

interface State {
    ready: boolean;
    isSearchBoxActive: boolean;
    searchForm: FormGroup;
}

interface Entry {
    id: number;
    email: string;
    name: string | null;
    position: string | null;
    approve?: WorkCompetenceApproveModel;
}

interface SearchRequest {
    query: string;
}

interface FormValue {
    query: string;
}

export interface SendRequest {
    workExperienceId: ProfileExperienceId;
    entry: Entry;
}

export interface CancelRequest {
    workExperienceId: ProfileExperienceId;
    entry: Entry;
}

export {Entry as EvodeskUserSettingsWorkExperienceConfirmComponentEntry};
export {SearchRequest as EvodeskUserSettingsWorkExperienceConfirmComponentSearchRequest};
export {SendRequest as EvodeskUserSettingsWorkExperienceConfirmComponentSendRequest};
export {CancelRequest as EvodeskUserSettingsWorkExperienceConfirmComponentCancelRequest};

@Component({
    selector: 'evodesk-user-settings-work-experience-confirm',
    templateUrl: './EvodeskUserSettingsWorkExperienceConfirm.component.pug',
    styleUrls: [
        './EvodeskUserSettingsWorkExperienceConfirm.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskUserSettingsWorkExperienceConfirmComponent implements OnInit, OnChanges, OnDestroy
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('search') searchEvent: EventEmitter<SearchRequest> = new EventEmitter<SearchRequest>();
    @Output('send') sendEvent: EventEmitter<SendRequest> = new EventEmitter<SendRequest>();
    @Output('cancel') cancelEvent: EventEmitter<CancelRequest> = new EventEmitter<CancelRequest>();

    @ViewChild('searchInput') searchInputRef: ElementRef;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        isSearchBoxActive: false,
        searchForm: this.fb.group({
            query: [''],
        }),
    };

    constructor(
        private fb: FormBuilder,
    ) {}

    ngOnInit(): void {
        this.state.searchForm.valueChanges.pipe(
            takeUntil(this.ngOnDestroy$),
            debounceTime(500),
            distinctUntilChanged(),
        ).subscribe((formValue: FormValue) => {
            this.searchEvent.emit(formValue);
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    onSearchBoxClick(): void {
        if (this.searchInputRef) {
            (this.searchInputRef.nativeElement as HTMLInputElement).focus();
        }
    }

    onSearchBoxFocus(): void {
        this.state = {
            ...this.state,
            isSearchBoxActive: true,
        };
    }

    onSearchBoxBlur(): void {
        this.state = {
            ...this.state,
            isSearchBoxActive: false,
        };
    }

    get hasSearchResults(): boolean {
        return this.props.entries.length > 0;
    }

    get hasNoSearchResults(): boolean {
        return !! this.state.searchForm.value.query && this.props.entries.length === 0;
    }

    isSending(entry: Entry): boolean {
        return !!~this.props.queue.indexOf(entry.id);
    }

    isApproveRequestSent(entry: Entry): boolean {
        return !!~this.props.hasRequestsFor.indexOf(entry.id) && ! this.isApproved(entry);
    }

    isApproveRequestNotSent(entry: Entry): boolean {
        return ! this.isApproveRequestSent(entry) && ! this.isApproved(entry);
    }

    isApproved(entry: Entry): boolean {
        return !!~this.props.requestsApproved.indexOf(entry.id);
    }

    send(entry: Entry): void {
        this.sendEvent.emit({
            workExperienceId: this.props.workExperienceId,
            entry: entry,
        });
    }

    cancel(entry: Entry): void {
        this.cancelEvent.emit({
            workExperienceId: this.props.workExperienceId,
            entry: entry,
        });
    }
}
