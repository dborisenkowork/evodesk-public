import {ViewMode} from '../../../../types/ViewMode.types';

export enum EvodeskUserSettingsNavigation {
    Common = 'common',
    Account = 'account',
    Competence = 'competence',
    WorkExperience = 'work-experience',
    PrivacyAndSecurity = 'privacy-and-security',
    Education = 'education',
    Notifications = 'notifications',
}

export const availableNavigationSections: Array<{ nav: EvodeskUserSettingsNavigation, translate: string }> = [
    { nav: EvodeskUserSettingsNavigation.Common, translate: 'EvodeskUserSettings.models.EvodeskUserSettingsNavigation.Common' },
    { nav: EvodeskUserSettingsNavigation.Account, translate: 'EvodeskUserSettings.models.EvodeskUserSettingsNavigation.Account' },
    { nav: EvodeskUserSettingsNavigation.Competence, translate: 'EvodeskUserSettings.models.EvodeskUserSettingsNavigation.Competence' },
    { nav: EvodeskUserSettingsNavigation.WorkExperience, translate: 'EvodeskUserSettings.models.EvodeskUserSettingsNavigation.WorkExperience' },
    { nav: EvodeskUserSettingsNavigation.PrivacyAndSecurity, translate: 'EvodeskUserSettings.models.EvodeskUserSettingsNavigation.PrivacyAndSecurity' },
    { nav: EvodeskUserSettingsNavigation.Education, translate: 'EvodeskUserSettings.models.EvodeskUserSettingsNavigation.Education' },
    { nav: EvodeskUserSettingsNavigation.Notifications, translate: 'EvodeskUserSettings.models.EvodeskUserSettingsNavigation.Notifications' },
];

export {ViewMode as EvodeskUserSettingsViewMode};
