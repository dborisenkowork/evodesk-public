import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Subject} from 'rxjs';
import {distinctUntilChanged, takeUntil} from 'rxjs/operators';

import {availableNavigationSections} from '../../models/EvodeskUserSettings.model';

import {EvodeskUserSettingsStateAction, EvodeskUserSettingsStateService} from '../../services/EvodeskUserSettingsState.service';
import {EvodeskUserSettingsBootstrapService} from '../../services/EvodeskUserSettingsBootstrap.service';
import {EvodeskUserSaveChangesService} from '../../services/EvodeskUserSaveChanges.service';
import {EvodeskUserSettingsValidatorsFactoryService} from '../../services/EvodeskUserSettingsValidatorsFactory.service';
import {EvodeskUserSettingsBreakpointsService} from '../../services/EvodeskUserSettingsBreakpoints.service';
import {EvodeskUserSettingsDocumentCommentBodyParserService} from '../../services/EvodeskUserSettingsDocumentCommentBodyParser.service';

@Component({
    templateUrl: './IndexRoute.component.pug',
    styleUrls: [
        './IndexRoute.component.scss',
    ],
    providers: [
        EvodeskUserSettingsStateService,
        EvodeskUserSettingsBootstrapService,
        EvodeskUserSaveChangesService,
        EvodeskUserSettingsValidatorsFactoryService,
        EvodeskUserSettingsBreakpointsService,
        EvodeskUserSettingsDocumentCommentBodyParserService,
    ],
})
export class IndexRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private activeRoute: ActivatedRoute,
        private usState: EvodeskUserSettingsStateService,
        private breakpoints: EvodeskUserSettingsBreakpointsService,
    ) {}

    ngOnInit(): void {
        this.activeRoute.queryParams.pipe(
            takeUntil(this.ngOnDestroy$),
            distinctUntilChanged(),
        ).subscribe(qp => {
            if (qp['section'] && !!~availableNavigationSections.map(a => a.nav).indexOf(qp['section'])) {
                this.usState.dispatch({
                    type: EvodeskUserSettingsStateAction.Navigate,
                    payload: <any>qp['section'],
                });
            }

            setTimeout(() => {
                document.body.scrollTop = 0;
            });
        });

        this.breakpoints.current$.pipe(takeUntil(this.ngOnDestroy$), distinctUntilChanged()).subscribe((next) => {
            this.usState.dispatch({
                type: EvodeskUserSettingsStateAction.SetViewMode,
                payload: next,
            });
        });

        this.breakpoints.init();
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);

        this.usState.destroy();
        this.breakpoints.destroy();
    }
}
