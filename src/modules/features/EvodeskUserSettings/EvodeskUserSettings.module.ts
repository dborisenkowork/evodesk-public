import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskUserSettingsRoutingModule} from './EvodeskUserSettingsRouting.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';

import {EvodeskUserSettingsComponent} from './components/EvodeskUserSettings/EvodeskUserSettings.component';
import {EvodeskUserSettingsContainerComponent} from './components/EvodeskUserSettings/EvodeskUserSettingsContainer.component';
import {EvodeskUserSettingsCommonComponent} from './components/EvodeskUserSettingsCommon/EvodeskUserSettingsCommon.component';
import {EvodeskUserSettingsCommonContainerComponent} from './components/EvodeskUserSettingsCommon/EvodeskUserSettingsCommonContainer.component';
import {EvodeskUserSettingsAccountComponent} from './components/EvodeskUserSettingsAccount/EvodeskUserSettingsAccount.component';
import {EvodeskUserSettingsAccountContainerComponent} from './components/EvodeskUserSettingsAccount/EvodeskUserSettingsAccountContainer.component';
import {EvodeskUserSettingsEducationComponent} from './components/EvodeskUserSettingsEducation/EvodeskUserSettingsEducation.component';
import {EvodeskUserSettingsEducationContainerComponent} from './components/EvodeskUserSettingsEducation/EvodeskUserSettingsEducationContainer.component';
import {EvodeskUserSettingsWorkExperienceComponent} from './components/EvodeskUserSettingsWorkExperience/EvodeskUserSettingsWorkExperience.component';
import {EvodeskUserSettingsWorkExperienceContainerComponent} from './components/EvodeskUserSettingsWorkExperience/EvodeskUserSettingsWorkExperienceContainer.component';
import {EvodeskUserSettingsPrivacySecurityComponent} from './components/EvodeskUserSettingsPrivacySecurity/EvodeskUserSettingsPrivacySecurity.component';
import {EvodeskUserSettingsPrivacySecurityContainerComponent} from './components/EvodeskUserSettingsPrivacySecurity/EvodeskUserSettingsPrivacySecurityContainer.component';
import {EvodeskUserSettingsPasswordComponent} from './components/EvodeskUserSettingsPassword/EvodeskUserSettingsPassword.component';
import {EvodeskUserSettingsPasswordContainerComponent} from './components/EvodeskUserSettingsPassword/EvodeskUserSettingsPasswordContainer.component';
import {EvodeskUserSettingsAvatarComponent} from './components/EvodeskUserSettingsAvatar/EvodeskUserSettingsAvatar.component';
import {EvodeskUserSettingsAvatarContainerComponent} from './components/EvodeskUserSettingsAvatar/EvodeskUserSettingsAvatarContainer.component';
import {EvodeskUserSettingsAvatarModalComponent} from './components/EvodeskUserSettingsAvatarModal/EvodeskUserSettingsAvatarModal.component';
import {EvodeskUserSettingsActivityComponent} from './components/EvodeskUserSettingsActivity/EvodeskUserSettingsActivity.component';
import {EvodeskUserSettingsActivityContainerComponent} from './components/EvodeskUserSettingsActivity/EvodeskUserSettingsActivityContainer.component';
import {EvodeskUserSettingsNavigationComponent} from './components-util/EvodeskUserSettingsNavigation/EvodeskUserSettingsNavigation.component';
import {EvodeskUserSettingsNavigationContainerComponent} from './components-util/EvodeskUserSettingsNavigation/EvodeskUserSettingsNavigationContainer.component';
import {EvodeskUserSettingsNavigationMobileComponent} from './components-util/EvodeskUserSettingsNavigationMobile/EvodeskUserSettingsNavigationMobile.component';
import {EvodeskUserSettingsNavigationMobileContainerComponent} from './components-util/EvodeskUserSettingsNavigationMobile/EvodeskUserSettingsNavigationMobileContainer.component';
import {EvodeskUserSettingsWorkExperienceConfirmComponent} from './components/EvodeskUserSettingsWorkExperienceConfirm/EvodeskUserSettingsWorkExperienceConfirm.component';
import {EvodeskUserSettingsWorkExperienceConfirmContainerComponent} from './components/EvodeskUserSettingsWorkExperienceConfirm/EvodeskUserSettingsWorkExperienceConfirmContainer.component';
import {EvodeskUserSettingsCompetenceComponent} from './components/EvodeskUserSettingsCompetence/EvodeskUserSettingsCompetence.component';
import {EvodeskUserSettingsCompetenceContainerComponent} from './components/EvodeskUserSettingsCompetence/EvodeskUserSettingsCompetenceContainer.component';
import {EvodeskUserSettingsCompetenceAddComponent} from './components/EvodeskUserSettingsCompetenceAdd/EvodeskUserSettingsCompetenceAdd.component';
import {EvodeskUserSettingsCompetenceAddContainerComponent} from './components/EvodeskUserSettingsCompetenceAdd/EvodeskUserSettingsCompetenceAddContainer.component';
import {EvodeskUserSettingsCompetenceEditFileComponent} from './components/EvodeskUserSettingsCompetenceEditFile/EvodeskUserSettingsCompetenceEditFile.component';
import {EvodeskUserSettingsNotificationsComponent} from './components/EvodeskUserSettingsNotifications/EvodeskUserSettingsNotifications.component';
import {EvodeskUserSettingsNotificationsContainerComponent} from './components/EvodeskUserSettingsNotifications/EvodeskUserSettingsNotificationsContainer.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskUserSettingsRoutingModule,
    ],
    declarations: [
        IndexRouteComponent,

        EvodeskUserSettingsNavigationComponent,
        EvodeskUserSettingsNavigationContainerComponent,
        EvodeskUserSettingsNavigationMobileComponent,
        EvodeskUserSettingsNavigationMobileContainerComponent,

        EvodeskUserSettingsComponent,
        EvodeskUserSettingsContainerComponent,
        EvodeskUserSettingsCommonComponent,
        EvodeskUserSettingsCommonContainerComponent,
        EvodeskUserSettingsAccountComponent,
        EvodeskUserSettingsAccountContainerComponent,
        EvodeskUserSettingsEducationComponent,
        EvodeskUserSettingsEducationContainerComponent,
        EvodeskUserSettingsWorkExperienceComponent,
        EvodeskUserSettingsWorkExperienceContainerComponent,
        EvodeskUserSettingsPrivacySecurityComponent,
        EvodeskUserSettingsPrivacySecurityContainerComponent,
        EvodeskUserSettingsPasswordComponent,
        EvodeskUserSettingsPasswordContainerComponent,
        EvodeskUserSettingsAvatarComponent,
        EvodeskUserSettingsAvatarContainerComponent,
        EvodeskUserSettingsAvatarModalComponent,
        EvodeskUserSettingsActivityComponent,
        EvodeskUserSettingsActivityContainerComponent,
        EvodeskUserSettingsWorkExperienceConfirmComponent,
        EvodeskUserSettingsWorkExperienceConfirmContainerComponent,
        EvodeskUserSettingsCompetenceComponent,
        EvodeskUserSettingsCompetenceContainerComponent,
        EvodeskUserSettingsCompetenceAddComponent,
        EvodeskUserSettingsCompetenceAddContainerComponent,
        EvodeskUserSettingsCompetenceEditFileComponent,
        EvodeskUserSettingsNotificationsComponent,
        EvodeskUserSettingsNotificationsContainerComponent,
    ],
    entryComponents: [
        EvodeskUserSettingsAvatarModalComponent,
        EvodeskUserSettingsWorkExperienceConfirmContainerComponent,
        EvodeskUserSettingsCompetenceAddContainerComponent,
        EvodeskUserSettingsCompetenceEditFileComponent,
    ],
})
export class EvodeskUserSettingsModule
{
}
