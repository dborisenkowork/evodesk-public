import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskAchievementsRoutingModule} from './EvodeskAchievementsRouting.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';

import {EvodeskAchievementsComponent} from './components/EvodeskAchievements/EvodeskAchievements.component';
import {EvodeskAchievementsContainerComponent} from './components/EvodeskAchievements/EvodeskAchievementsContainer.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskAchievementsRoutingModule,
    ],
    declarations: [
        IndexRouteComponent,

        EvodeskAchievementsComponent,
        EvodeskAchievementsContainerComponent,
    ],
})
export class EvodeskAchievementsModule
{}
