import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {Platform} from '@angular/cdk/platform';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskAchievementsComponentProps as Props} from './EvodeskAchievements.component';

import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {AchievementsAction, EvodeskAchievementsStateService, LeadersSection} from '../../state/EvodeskAchievementsState.service';
import {EvodeskAchievementsBootstrapService} from '../../state/EvodeskAchievementsBootstrap.service';
import {LeadersRange, LeadersRESTService} from '../../../../app/EvodeskRESTApi/services/LeadersREST.service';

import {leaderEntriesPerPage} from '../../configs/EvodeskAchievements.config';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';

interface State {
    ready: boolean;
    loading: boolean;
    offsetLeadersWeek: number;
    offsetLeadersMonth: number;
    offsetLeadersTotal: number;
    props?: Props | undefined;
}

@Component({
    selector: 'evodesk-achievements-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-achievements
            *ngIf="state.props"
            [props]="state.props"
            (helpCardInfo)="helpCardInfo()"
            (loadMoreWeek)="loadMoreWeek()"
            (loadMoreMonth)="loadMoreMonth()"
            (loadMoreTotal)="loadMoreTotal()"
          ></evodesk-achievements>
        </ng-container>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskAchievementsContainerComponent implements OnDestroy, OnChanges
{
    @Input() profileId: ProfileId;

    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private nextProfileId$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        loading: false,
        offsetLeadersWeek: 0,
        offsetLeadersMonth: 0,
        offsetLeadersTotal: 0,
        props: undefined,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private aState: EvodeskAchievementsStateService,
        private bootstrap: EvodeskAchievementsBootstrapService,
        private leadersREST: LeadersRESTService,
        private platform: Platform,
        private uiAlertService: EvodeskAlertModalService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['profileId']) {
            this.nextProfileId$.next(undefined);

            this.state = {
                ...this.state,
                loading: true,
                props: undefined,
            };

            this.cdr.detectChanges();

            this.bootstrap.bootstrap(this.profileId).pipe(takeUntil(this.nextProfileId$)).subscribe(() => {
                this.state = {
                    ...this.state,
                    ready: true,
                    loading: false,
                };

                this.aState.current$.pipe(takeUntil(this.nextProfileId$)).subscribe((s) => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            isMobileDevice: this.platform.ANDROID || this.platform.IOS,
                            profile: s.current.profile,
                            current: s.current.current,
                            leaders: s.current.leaders,
                        },
                    };

                    this.cdr.detectChanges();
                });
            });
        }
    }

    ngOnDestroy(): void {
        this.nextProfileId$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    helpCardInfo(): void {
        this.uiAlertService.openTranslated('EvodeskAchievements.components.EvodeskAchievements.HelpTooltipDialog')
            .pipe(takeUntil(this.nextProfileId$))
            .subscribe();
    }

    loadMoreWeek(): void {
        this.aState.dispatch({
            type: AchievementsAction.EnableLeadersLoading,
            payload: LeadersSection.Week,
        });

        this.leadersREST.leaders({
            limit: leaderEntriesPerPage,
            offset: this.state.offsetLeadersWeek + leaderEntriesPerPage,
            forProfileId: this.profileId,
            range: LeadersRange.All,
        }).subscribe(
            (httpResponse) => {
                this.aState.dispatch({
                    type: AchievementsAction.DisableLeadersLoading,
                    payload: LeadersSection.Week,
                });

                this.aState.dispatch({
                    type: AchievementsAction.PushLeaders,
                    payload: {
                        entries: httpResponse.data,
                        section: LeadersSection.Week,
                    },
                });

                this.state = {
                    ...this.state,
                    offsetLeadersWeek: this.state.offsetLeadersWeek + leaderEntriesPerPage,
                };
            },
            () => {
                this.aState.dispatch({
                    type: AchievementsAction.DisableLeadersLoading,
                    payload: LeadersSection.Week,
                });
            },
        );
    }

    loadMoreMonth(): void {
        this.aState.dispatch({
            type: AchievementsAction.EnableLeadersLoading,
            payload: LeadersSection.Month,
        });

        this.leadersREST.leaders({
            limit: leaderEntriesPerPage,
            offset: this.state.offsetLeadersMonth + leaderEntriesPerPage,
            forProfileId: this.profileId,
            range: LeadersRange.All,
        }).subscribe(
            (httpResponse) => {
                this.aState.dispatch({
                    type: AchievementsAction.DisableLeadersLoading,
                    payload: LeadersSection.Month,
                });

                this.aState.dispatch({
                    type: AchievementsAction.PushLeaders,
                    payload: {
                        entries: httpResponse.data,
                        section: LeadersSection.Month,
                    },
                });

                this.state = {
                    ...this.state,
                    offsetLeadersMonth: this.state.offsetLeadersMonth + leaderEntriesPerPage,
                };
            },
            () => {
                this.aState.dispatch({
                    type: AchievementsAction.DisableLeadersLoading,
                    payload: LeadersSection.Month,
                });
            },
        );
    }

    loadMoreTotal(): void {
        this.aState.dispatch({
            type: AchievementsAction.EnableLeadersLoading,
            payload: LeadersSection.Total,
        });

        this.leadersREST.leaders({
            limit: leaderEntriesPerPage,
            offset: this.state.offsetLeadersTotal + leaderEntriesPerPage,
            forProfileId: this.profileId,
            range: LeadersRange.All,
        }).subscribe(
            (httpResponse) => {
                this.aState.dispatch({
                    type: AchievementsAction.DisableLeadersLoading,
                    payload: LeadersSection.Total,
                });

                this.aState.dispatch({
                    type: AchievementsAction.PushLeaders,
                    payload: {
                        entries: httpResponse.data,
                        section: LeadersSection.Total,
                    },
                });

                this.state = {
                    ...this.state,
                    offsetLeadersTotal: this.state.offsetLeadersTotal + leaderEntriesPerPage,
                };
            },
            () => {
                this.aState.dispatch({
                    type: AchievementsAction.DisableLeadersLoading,
                    payload: LeadersSection.Total,
                });
            },
        );
    }
}
