import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {SwiperConfigInterface} from 'ngx-swiper-wrapper';

import {environment} from '../../../../../environments/environment.config';

import {ProfileModel} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {LeaderEntryModel} from '../../../../app/EvodeskRESTApi/models/leaders/LeaderEntry.model';
import {AchievementModel} from '../../../../app/EvodeskRESTApi/models/achievements/Achievement.model';

interface Props {
    profile: ProfileModel;
    isMobileDevice: boolean;
    leaders: {
        week: {
            loading: boolean;
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
        };
        month: {
            loading: boolean;
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
        };
        total: {
            loading: boolean;
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
        };
    };
    current: {
        achievements: Array<AchievementModel>;
        positions: {
            week: {
                name: string;
                position: number;
                experience: number;
            };
            month: {
                name: string;
                position: number;
                experience: number;
            };
            total: {
                name: string;
                position: number;
                experience: number;
            };
        };
    };
}

interface State {
    ready: boolean;
}

export {Props as EvodeskAchievementsComponentProps};

@Component({
    selector: 'evodesk-achievements',
    templateUrl: './EvodeskAchievements.component.pug',
    styleUrls: [
        './EvodeskAchievements.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskAchievementsComponent implements OnChanges
{
    @Input() props: Props;

    @Output('helpCardInfo') helpCardInfoEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output('loadMoreWeek') loadMoreWeekEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('loadMoreMonth') loadMoreMonthEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('loadMoreTotal') loadMoreTotalEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    helpCardInfo(): void {
        if (this.props.isMobileDevice) {
            this.helpCardInfoEvent.emit(undefined);
        }
    }

    get pointsPlural(): string {
        const points: number = Math.floor(parseInt(this.props.profile.balance, 10));
        const lastDigit: number = parseInt(points.toString()[points.toString().length - 1], 10);

        if (points >= 10 && points <= 19) {
            return 'EvodeskAchievements.components.EvodeskAchievements.Points56789D';
        } else if (lastDigit === 1) {
            return 'EvodeskAchievements.components.EvodeskAchievements.Points1';
        } else if (!!~[0, 5, 6, 7, 8, 9].indexOf(lastDigit)) {
            return 'EvodeskAchievements.components.EvodeskAchievements.Points56789D';
        } else {
            return 'EvodeskAchievements.components.EvodeskAchievements.Points234';
        }
    }

    get progressBarNgStyle(): any {
        return {
            width: `${this.props.profile.currentExpPercent}%`,
        };
    }

    get swiperConfig(): SwiperConfigInterface {
        return {
        };
    }

    getAchievementsImageUrl(input: string): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/${input}`;
    }

    loadMoreWeek(): void {
        if (this.props.leaders.week.hasMoreToLoad && ! this.props.leaders.week.loading) {
            this.loadMoreWeekEvent.emit(undefined);
        }
    }

    loadMoreMonth(): void {
        if (this.props.leaders.month.hasMoreToLoad && ! this.props.leaders.month.loading) {
            this.loadMoreMonthEvent.emit(undefined);
        }
    }

    loadMoreTotal(): void {
        if (this.props.leaders.total.hasMoreToLoad && ! this.props.leaders.total.loading) {
            this.loadMoreTotalEvent.emit(undefined);
        }
    }

    onLeaderWeekScroll($event: Event): void {
        const elem: HTMLElement = <any>$event.target;

        const scrollTop: number = elem.scrollTop;
        const scrollHeight: number = elem.scrollHeight;
        const clientHeight: number = elem.clientHeight;

        if (scrollHeight > clientHeight) {
            if ((scrollTop + clientHeight) === scrollHeight) {
                this.loadMoreWeek();
            }
        }
    }

    onLeaderMonthScroll($event: Event): void {
        const elem: HTMLElement = <any>$event.target;

        const scrollTop: number = elem.scrollTop;
        const scrollHeight: number = elem.scrollHeight;
        const clientHeight: number = elem.clientHeight;

        if (scrollHeight > clientHeight) {
            if ((scrollTop + clientHeight) === scrollHeight) {
                this.loadMoreMonth();
            }
        }
    }

    onLeaderTotalScroll($event: Event): void {
        const elem: HTMLElement = <any>$event.target;

        const scrollTop: number = elem.scrollTop;
        const scrollHeight: number = elem.scrollHeight;
        const clientHeight: number = elem.clientHeight;

        if (scrollHeight > clientHeight) {
            if ((scrollTop + clientHeight) === scrollHeight) {
                this.loadMoreTotal();
            }
        }
    }
}
