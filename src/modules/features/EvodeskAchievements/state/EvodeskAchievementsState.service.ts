import {Injectable} from '@angular/core';

import * as R from 'ramda';

import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {AchievementModel} from '../../../app/EvodeskRESTApi/models/achievements/Achievement.model';
import {LeaderEntryModel} from '../../../app/EvodeskRESTApi/models/leaders/LeaderEntry.model';
import {ProfileModel} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {LeadersRange} from '../../../app/EvodeskRESTApi/services/LeadersREST.service';

export interface AchievementsState {
    profile: ProfileModel;
    leaders: {
        week: {
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
            loading: boolean;
        };
        month: {
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
            loading: boolean;
        };
        total: {
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
            loading: boolean;
        };
    };
    current: {
        achievements: Array<AchievementModel>;
        positions: {
            week: {
                name: string;
                position: number;
                experience: number;
            };
            month: {
                name: string;
                position: number;
                experience: number;
            };
            total: {
                name: string;
                position: number;
                experience: number;
            };
        };
    };
}

export type AchievementsPrevState = AchievementsState | undefined;
export type AchievementsNextState = AchievementsState | undefined;
export type AchievementsStates = { current: AchievementsNextState, prev?: AchievementsPrevState };

export enum LeadersSection {
    Week = 'Week',
    Month = 'Month',
    Total = 'Total',
}

export enum AchievementsAction {
    Reset = 'Reset',
    Bootstrap = 'Bootstrap',
    EnableLeadersLoading = 'EnableLeadersLoading',
    DisableLeadersLoading = 'DisableLeadersLoading',
    PushLeaders = 'PushLeaders',
}

function initialState(): AchievementsState | undefined {
    return undefined;
}

type Actions =
      { type: AchievementsAction.Reset }
    | { type: AchievementsAction.Bootstrap, payload: AchievementsBootstrap }
    | { type: AchievementsAction.EnableLeadersLoading, payload: LeadersSection }
    | { type: AchievementsAction.DisableLeadersLoading, payload: LeadersSection }
    | { type: AchievementsAction.PushLeaders, payload: { section: LeadersSection; entries: Array<LeaderEntryModel>; } }
;

export interface AchievementsBootstrap {
    profile: ProfileModel;
    leaders: {
        week: {
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
            loading: boolean;
        };
        month: {
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
            loading: boolean;
        };
        total: {
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
            loading: boolean;
        };
    };
    current: {
        achievements: Array<AchievementModel>;
        positions: {
            week: {
                name: string;
                position: number;
                experience: number;
            };
            month: {
                name: string;
                position: number;
                experience: number;
            };
            total: {
                name: string;
                position: number;
                experience: number;
            };
        };
    };
}

export const mapLeadersSectionToLeadersREST: Array<{ from: LeadersRange, to: LeadersSection }> = [
    { from: LeadersRange.Week, to: LeadersSection.Week },
    { from: LeadersRange.Month, to: LeadersSection.Month },
    { from: LeadersRange.All, to: LeadersSection.Total },
];

export const mapLeadersSectionToFields: Array<{ from: LeadersSection, to: string }> = [
    { from: LeadersSection.Week, to: 'week' },
    { from: LeadersSection.Month, to: 'month' },
    { from: LeadersSection.Total, to: 'total' },
];

@Injectable()
export class EvodeskAchievementsStateService
{
    private _current$: BehaviorSubject<AchievementsStates> = new BehaviorSubject<AchievementsStates>({ prev: undefined, current: initialState() });
    private _side$: Subject<Actions> = new Subject<Actions>();

    get current$(): Observable<AchievementsStates> {
        return this._current$.asObservable();
    }

    get side$(): Observable<Actions> {
        return this._side$.asObservable();
    }

    get snapshot(): AchievementsState {
        return this._current$.getValue().current;
    }

    reset(): void {
        this._current$.next({
            prev: undefined,
            current: initialState(),
        });
    }

    dispatch(action: Actions): void {
        this._side$.next(action);

        switch (action.type) {
            case AchievementsAction.Reset: {
                this.reset();

                break;
            }

            case AchievementsAction.Bootstrap: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        ...action.payload,
                    };
                });

                break;
            }

            case AchievementsAction.EnableLeadersLoading: {
                const field: string = mapLeadersSectionToFields.filter(m => m.from === action.payload)[0].to;

                this.setState((orig) => {
                    return {
                        ...orig,
                        leaders: {
                            ...orig.leaders,
                            [field]: {
                                ...orig.leaders[field],
                                loading: true,
                            },
                        },
                    };
                });

                break;
            }

            case AchievementsAction.DisableLeadersLoading: {
                const field: string = mapLeadersSectionToFields.filter(m => m.from === action.payload)[0].to;

                this.setState((orig) => {
                    return {
                        ...orig,
                        leaders: {
                            ...orig.leaders,
                            [field]: {
                                ...orig.leaders[field],
                                loading: false,
                            },
                        },
                    };
                });

                break;
            }

            case AchievementsAction.PushLeaders: {
                const field: string = mapLeadersSectionToFields.filter(m => m.from === action.payload.section)[0].to;

                this.setState((orig) => {
                    return {
                        ...orig,
                        leaders: {
                            ...orig.leaders,
                            [field]: {
                                ...orig.leaders[field],
                                entries: R.uniqBy((e: LeaderEntryModel) => e.id, [...orig.leaders[field].entries, ...action.payload.entries]),
                            },
                        },
                    };
                });

                break;
            }
        }
    }

    private setState(set$: (orig: AchievementsPrevState) => AchievementsNextState): void {
        this._current$.next({
            prev: this.snapshot,
            current: set$(this.snapshot),
        });
    }
}

