import {Injectable} from '@angular/core';

import {forkJoin, Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {leaderEntriesPerPage} from '../configs/EvodeskAchievements.config';

import {LeadersRange, LeadersRESTService} from '../../../app/EvodeskRESTApi/services/LeadersREST.service';
import {ProfileRESTService} from '../../../app/EvodeskRESTApi/services/ProfileREST.service';
import {AchievementsAction, AchievementsBootstrap, EvodeskAchievementsStateService} from './EvodeskAchievementsState.service';
import {EvodeskAppLoading, EvodeskAppLoadingStatusService} from '../../../app/EvodeskApp/services/EvodeskAppLoadingStatus.service';

@Injectable()
export class EvodeskAchievementsBootstrapService
{
    constructor(
        private appLoading: EvodeskAppLoadingStatusService,
        private achievementsState: EvodeskAchievementsStateService,
        private leadersRESTService: LeadersRESTService,
        private profileRESTService: ProfileRESTService,
    ) {}

    bootstrap(profileId): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        const appLoading: EvodeskAppLoading = this.appLoading.addLoading();

        const bootstrapQuery: AchievementsBootstrap = {
            profile: <any>undefined,
            current: {
                achievements: [],
                positions: {
                    month: {
                        name: '',
                        experience: 0,
                        position: 0,
                    },
                    total: {
                        name: '',
                        experience: 0,
                        position: 0,
                    },
                    week: {
                        name: '',
                        experience: 0,
                        position: 0,
                    },
                },
            },
            leaders: {
                month: {
                    entries: [],
                    hasMoreToLoad: false,
                    loading: false,
                },
                total: {
                    entries: [],
                    hasMoreToLoad: false,
                    loading: false,
                },
                week: {
                    entries: [],
                    hasMoreToLoad: false,
                    loading: false,
                },
            },
        };

        return Observable.create((done) => {
            const queries: Array<Observable<any>> = [];

            queries.push(Observable.create(q => {
                this.profileRESTService.getProfile(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.profile = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.profileRESTService.getProfileAchievements(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.current.achievements = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.leadersRESTService.leaders({
                    range: LeadersRange.Week,
                    forProfileId: profileId,
                    offset: 0,
                    limit: leaderEntriesPerPage + 1,
                }).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.leaders.week = {
                            ...bootstrapQuery.leaders.week,
                            entries: httpResponse.data.slice(0, leaderEntriesPerPage),
                            hasMoreToLoad: httpResponse.data.length > leaderEntriesPerPage,
                        };

                        bootstrapQuery.current.positions.week = {
                            name: httpResponse.this.name || httpResponse.this.email,
                            experience: parseInt(<any>httpResponse.this.experience, leaderEntriesPerPage),
                            position: httpResponse.this.position,
                        };

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.leadersRESTService.leaders({
                    range: LeadersRange.Month,
                    forProfileId: profileId,
                    offset: 0,
                    limit: leaderEntriesPerPage + 1,
                }).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.leaders.month = {
                            ...bootstrapQuery.leaders.month,
                            entries: httpResponse.data.slice(0, leaderEntriesPerPage),
                            hasMoreToLoad: httpResponse.data.length > leaderEntriesPerPage,
                        };

                        bootstrapQuery.current.positions.month = {
                            name: httpResponse.this.name || httpResponse.this.email,
                            experience: parseInt(<any>httpResponse.this.experience, leaderEntriesPerPage),
                            position: httpResponse.this.position,
                        };

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.leadersRESTService.leaders({
                    range: LeadersRange.All,
                    forProfileId: profileId,
                    offset: 0,
                    limit: leaderEntriesPerPage + 1,
                }).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.leaders.total = {
                            ...bootstrapQuery.leaders.total,
                            entries: httpResponse.data.slice(0, leaderEntriesPerPage),
                            hasMoreToLoad: httpResponse.data.length > leaderEntriesPerPage,
                        };

                        bootstrapQuery.current.positions.total = {
                            name: httpResponse.this.name || httpResponse.this.email,
                            experience: parseInt(<any>httpResponse.this.experience, leaderEntriesPerPage),
                            position: httpResponse.this.position,
                        };

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            forkJoin(queries).pipe(takeUntil(unsubscribe$)).subscribe(
                () => {
                    this.achievementsState.dispatch({
                        type: AchievementsAction.Bootstrap,
                        payload: bootstrapQuery,
                    });

                    appLoading.done();

                    done.next(undefined);
                    done.complete();
                },
                (queryError) => {
                    appLoading.done();

                    done.error(queryError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
