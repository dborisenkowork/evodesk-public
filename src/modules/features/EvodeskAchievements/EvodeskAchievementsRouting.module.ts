import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskAchievementsRouting} from './configs/EvodeskAchievementsRouting.module';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskAchievementsRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskAchievementsRoutingModule
{}
