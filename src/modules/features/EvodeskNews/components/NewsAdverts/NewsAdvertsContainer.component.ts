import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import {EVODESK_FEED_INJECT_PROVIDERS} from '../../../EvodeskFeed/EvodeskFeed.module';

import {NewsAdvertsComponentProps} from './NewsAdverts.component';

import {ViewMode} from '../../../../../types/ViewMode.types';

import {EvodeskNewsNavigation} from '../../models/EvodeskNews.model';
import {EvodeskFeedContext} from '../../../EvodeskFeed/models/EvodeskFeed.model';

import {NewsAdvertsSourceAdapter} from '../../../EvodeskFeed/source-adapters/NewsAdverts.source-adapter';
import {AbstractNewsSourceAdapter} from '../../../EvodeskFeed/source-adapters/AbstractNews.source-adapter';

import {EvodeskNewsContextService} from '../../services/EvodeskNewsContext.service';
import {EvodeskFeedService} from '../../../EvodeskFeed/services/EvodeskFeed.service';
import {EvodeskFeedStateAction, EvodeskFeedStateService} from '../../../EvodeskFeed/state/EvodeskFeedState.service';
import {EvodeskNewsStateAction, EvodeskNewsStateService} from '../../services/EvodeskNewsState.service';

interface State {
    ready: boolean;
    props?: ComponentProps;
}

type ComponentProps = NewsAdvertsComponentProps;

@Component({
    selector: 'evodesk-news-adverts-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-news-adverts [props]="state.props"></evodesk-news-adverts>
      </ng-container>
    `,
    providers: [
        ...EVODESK_FEED_INJECT_PROVIDERS,
        NewsAdvertsSourceAdapter,
        AbstractNewsSourceAdapter,
    ],
})
export class NewsAdvertsContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        props: {
            hasEntries: true,
            viewMode: ViewMode.Desktop,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private viewContainerRef: ViewContainerRef,
        private context: EvodeskNewsContextService,
        private feedState: EvodeskFeedStateService,
        private feedService: EvodeskFeedService,
        private feedSource: NewsAdvertsSourceAdapter,
        private newsState: EvodeskNewsStateService,
    ) {}

    ngOnInit(): void {
        this.context.setContext({
            feedState: this.feedState,
            feedService: this.feedService,
            feedSource: this.feedSource,
            viewContainerRef: this.viewContainerRef,
        });

        this.newsState.dispatch({
            type: EvodeskNewsStateAction.Navigate,
            payload: {
                navigateTo: EvodeskNewsNavigation.Adverts,
            },
        });

        this.feedService.setConfig((orig) => {
            return {
                ...orig,
                source: this.feedSource,
            };
        });

        this.feedState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.viewMode),
            distinctUntilChanged(),
        ).subscribe((viewMode) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    viewMode: viewMode,
                },
            };

            this.cdr.detectChanges();
        });

        this.feedService.loadMore().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    hasEntries: this.feedState.snapshot.posts.length > 0,
                },
            };

            this.cdr.detectChanges();
        });

        this.feedState.dispatch({
            type: EvodeskFeedStateAction.SetContext,
            payload: {
                context: EvodeskFeedContext.NewsAdverts,
            },
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
