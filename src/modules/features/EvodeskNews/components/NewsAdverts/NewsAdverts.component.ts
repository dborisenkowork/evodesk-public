import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import {EvodeskNewsViewMode} from '../../models/EvodeskNews.model';

export interface NewsAdvertsComponentProps {
    hasEntries: boolean;
    viewMode: EvodeskNewsViewMode;
}

interface State {
    ready: boolean;
}

type Props = NewsAdvertsComponentProps;

@Component({
    selector: 'evodesk-news-adverts',
    templateUrl: './NewsAdverts.component.pug',
    styleUrls: [
        './NewsAdverts.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsAdvertsComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            this.state = {
                ...this.state,
                ready: !! this.props,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get shouldDisplayPublishPostForm(): boolean {
        return this.props.viewMode === EvodeskNewsViewMode.Desktop;
    }
}
