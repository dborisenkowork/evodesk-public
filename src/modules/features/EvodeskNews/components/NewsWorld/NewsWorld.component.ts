import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

export interface NewsWorldComponentProps {
    hasEntries: boolean;
}

interface State {
    ready: boolean;
}

type Props = NewsWorldComponentProps;

@Component({
    selector: 'evodesk-news-world',
    templateUrl: './NewsWorld.component.pug',
    styleUrls: [
        './NewsWorld.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsWorldComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            this.state = {
                ...this.state,
                ready: !! this.props,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
