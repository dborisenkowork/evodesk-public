import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EVODESK_FEED_INJECT_PROVIDERS} from '../../../EvodeskFeed/EvodeskFeed.module';

import {NewsWorldComponentProps} from './NewsWorld.component';

import {EvodeskNewsNavigation} from '../../models/EvodeskNews.model';
import {EvodeskFeedContext} from '../../../EvodeskFeed/models/EvodeskFeed.model';

import {NewsWorldSourceAdapter} from '../../../EvodeskFeed/source-adapters/NewsWorld.source-adapter';
import {AbstractNewsSourceAdapter} from '../../../EvodeskFeed/source-adapters/AbstractNews.source-adapter';

import {EvodeskNewsContextService} from '../../services/EvodeskNewsContext.service';
import {EvodeskFeedService} from '../../../EvodeskFeed/services/EvodeskFeed.service';
import {EvodeskFeedStateAction, EvodeskFeedStateService} from '../../../EvodeskFeed/state/EvodeskFeedState.service';
import {EvodeskNewsStateAction, EvodeskNewsStateService} from '../../services/EvodeskNewsState.service';


interface State {
    ready: boolean;
    props?: ComponentProps;
}

type ComponentProps = NewsWorldComponentProps;

@Component({
    selector: 'evodesk-news-world-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-news-world [props]="state.props"></evodesk-news-world>
      </ng-container>
    `,
    providers: [
        ...EVODESK_FEED_INJECT_PROVIDERS,
        NewsWorldSourceAdapter,
        AbstractNewsSourceAdapter,
    ],
})
export class NewsWorldContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        props: {
            hasEntries: true,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private viewContainerRef: ViewContainerRef,
        private context: EvodeskNewsContextService,
        private feedState: EvodeskFeedStateService,
        private feedService: EvodeskFeedService,
        private feedSource: NewsWorldSourceAdapter,
        private newsState: EvodeskNewsStateService,
    ) {}

    ngOnInit(): void {
        this.context.setContext({
            feedState: this.feedState,
            feedService: this.feedService,
            feedSource: this.feedSource,
            viewContainerRef: this.viewContainerRef,
        });

        this.newsState.dispatch({
            type: EvodeskNewsStateAction.Navigate,
            payload: {
                navigateTo: EvodeskNewsNavigation.World,
            },
        });

        this.feedService.setConfig((orig) => {
            return {
                ...orig,
                source: this.feedSource,
            };
        });

        this.feedService.loadMore().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    hasEntries: this.feedState.snapshot.posts.length > 0,
                },
            };

            this.cdr.detectChanges();
        });

        this.feedState.dispatch({
            type: EvodeskFeedStateAction.SetContext,
            payload: {
                context: EvodeskFeedContext.NewsWorld,
            },
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
