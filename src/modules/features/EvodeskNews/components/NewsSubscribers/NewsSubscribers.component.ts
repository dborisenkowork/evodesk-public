import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

export interface NewsSubscribersComponentProps {
    hasEntries: boolean;
}

interface State {
    ready: boolean;
}

type Props = NewsSubscribersComponentProps;

@Component({
    selector: 'evodesk-news-subscribers',
    templateUrl: './NewsSubscribers.component.pug',
    styleUrls: [
        './NewsSubscribers.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsSubscribersComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            this.state = {
                ...this.state,
                ready: !! this.props,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
