import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, takeUntil} from 'rxjs/operators';

import {EvodeskNewsViewMode} from '../../models/EvodeskNews.model';

import {EvodeskNewsContextService} from '../../services/EvodeskNewsContext.service';
import {EvodeskNewsStateAction, EvodeskNewsStateService} from '../../services/EvodeskNewsState.service';
import {EvodeskNewsBreakpointsService} from '../../services/EvodeskNewsBreakpoints.service';

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-news',
    templateUrl: './News.component.pug',
    styleUrls: [
        './News.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        EvodeskNewsStateService,
        EvodeskNewsBreakpointsService,
    ],
})
export class NewsComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private feedState: EvodeskNewsStateService,
        private context: EvodeskNewsContextService,
        private breakpoints: EvodeskNewsBreakpointsService,
    ) {}

    ngOnInit(): void {
        this.breakpoints.init();

        this.breakpoints.current$.pipe(takeUntil(this.ngOnDestroy$), distinctUntilChanged()).subscribe((next) => {
            this.feedState.dispatch({
                type: EvodeskNewsStateAction.SetViewMode,
                payload: {
                    viewMode: next,
                },
            });

            this.cdr.detectChanges();
        });

        this.context.setContext({
            state: this.feedState,
        });
    }

    ngOnDestroy(): void {
        this.breakpoints.destroy();

        this.ngOnDestroy$.next(undefined);
    }

    get containerCSSClasses(): any {
        return {
            '__mobile': this.feedState.snapshot.viewMode === EvodeskNewsViewMode.Mobile,
            '__tablet': this.feedState.snapshot.viewMode === EvodeskNewsViewMode.Tablet,
            '__desktop': this.feedState.snapshot.viewMode === EvodeskNewsViewMode.Desktop,
        };
    }

    get shouldDisplayDesktopNavigation(): boolean {
        return this.feedState.snapshot.viewMode === EvodeskNewsViewMode.Desktop;
    }

    get shouldDisplayMobileNavigation(): boolean {
        return this.feedState.snapshot.viewMode === EvodeskNewsViewMode.Tablet
            || this.feedState.snapshot.viewMode === EvodeskNewsViewMode.Mobile;
    }
}
