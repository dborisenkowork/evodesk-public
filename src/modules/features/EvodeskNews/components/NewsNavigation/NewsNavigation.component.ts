import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

export interface NewsNavigationComponentProps {
}

interface State {
    ready: boolean;
}

type Props = NewsNavigationComponentProps;

@Component({
    selector: 'evodesk-news-navigation',
    templateUrl: './NewsNavigation.component.pug',
    styleUrls: [
        './NewsNavigation.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsNavigationComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            this.state = {
                ...this.state,
                ready: !! this.props,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
