import {Component, OnDestroy} from '@angular/core';

import {Subject} from 'rxjs';

import {NewsNavigationComponentProps} from './NewsNavigation.component';

interface State {
    ready: boolean;
    props?: ComponentProps;
}

type ComponentProps = NewsNavigationComponentProps;

@Component({
    selector: 'evodesk-news-navigation-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-news-navigation [props]="state.props"></evodesk-news-navigation>
      </ng-container>
    `,
})
export class NewsNavigationContainerComponent implements OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        props: {},
    };

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
