import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {availableNewsNavigations, EvodeskNewsNavigation, EvodeskNewsViewMode} from '../../models/EvodeskNews.model';

export interface NewsNavigationMobileComponentProps {
    viewMode: EvodeskNewsViewMode;
    currentNav: EvodeskNewsNavigation;
}

type Props = NewsNavigationMobileComponentProps;

interface State {
    ready: boolean;
    isOpened: boolean;
}

@Component({
    selector: 'evodesk-news-navigation-mobile',
    templateUrl: './NewsNavigationMobile.component.pug',
    styleUrls: [
        './NewsNavigationMobile.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsNavigationMobileComponent implements OnChanges
{
    @Input() props: Props;

    @Output('newPost') newPostEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        isOpened: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get containerCSSClasses(): any {
        return {
            '__mobile': this.props.viewMode === EvodeskNewsViewMode.Mobile,
            '__tablet': this.props.viewMode === EvodeskNewsViewMode.Tablet,
            '__desktop': this.props.viewMode === EvodeskNewsViewMode.Desktop,
            'is-opened': this.state.isOpened,
        };
    }

    get currentNav(): string {
        return availableNewsNavigations.filter(a => a.type === this.props.currentNav)[0].title;
    }

    toggle(): void {
        this.state = {
            ...this.state,
            isOpened: ! this.state.isOpened,
        };
    }

    close(): void {
        this.state = {
            ...this.state,
            isOpened: false,
        };
    }

    newPost(): void {
        this.newPostEvent.emit(undefined);
    }
}
