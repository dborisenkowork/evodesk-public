import {Component, OnDestroy, OnInit, SimpleChange} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {EvodeskNewsNavigation} from '../../models/EvodeskNews.model';

import {NewsNavigationMobileComponentProps} from './NewsNavigationMobile.component';
import {EvodeskFeedPostEditorFormContainerComponent} from '../../../EvodeskFeed/components/EvodeskFeedPostEditorForm/EvodeskFeedPostEditorFormContainer.component';

import {EvodeskNewsStateService} from '../../services/EvodeskNewsState.service';
import {EvodeskNewsContextService} from '../../services/EvodeskNewsContext.service';
import {EvodeskFeedPostEditorFormComponentViewMode} from '../../../EvodeskFeed/components/EvodeskFeedPostEditorForm/EvodeskFeedPostEditorForm.component';
import {NavigationStart, Router} from '@angular/router';

type Props = NewsNavigationMobileComponentProps;

interface State {
    ready: boolean;
    matDialogRef?: MatDialogRef<any>;
    props?: Props;
}

@Component({
    selector: 'evodesk-news-navigation-mobile-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-news-navigation-mobile [props]="state.props" (newPost)="newPost()"></evodesk-news-navigation-mobile>
      </ng-container>
    `,
})
export class NewsNavigationMobileContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private router: Router,
        private matDialog: MatDialog,
        private context: EvodeskNewsContextService,
        private newsState: EvodeskNewsStateService,
    ) {}

    ngOnInit(): void {
        this.newsState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.viewMode),
            distinctUntilChanged(),
        ).subscribe((viewMode) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    viewMode: viewMode,
                },
            };
        });

        this.newsState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.currentNavigation),
            distinctUntilChanged(),
        ).subscribe((currentNav) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    currentNav: currentNav,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);

        if (this.state.matDialogRef) {
            this.state.matDialogRef.close();
        }
    }

    newPost(): void {
        const modalClosed$: Subject<void> = new Subject<void>();

        const matDialogRef: MatDialogRef<EvodeskFeedPostEditorFormContainerComponent> = this.matDialog.open(EvodeskFeedPostEditorFormContainerComponent, {
            panelClass: '__evodesk-mat-dialog-feed-post-editor-container',
            backdropClass: '__evodesk-mat-dialog-default-backdrop',
            viewContainerRef: this.context.context.viewContainerRef,
        });

        matDialogRef.afterClosed().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            modalClosed$.next(undefined);

            this.state = {
                ...this.state,
                matDialogRef: undefined,
            };
        });

        matDialogRef.componentInstance.viewMode = EvodeskFeedPostEditorFormComponentViewMode.Modal;
        matDialogRef.componentInstance.forceExpandedForm = true;
        matDialogRef.componentInstance.forcePin = !!~[EvodeskNewsNavigation.Adverts].indexOf(this.newsState.snapshot.currentNavigation);
        matDialogRef.componentInstance.doPushPostToState = !!~[EvodeskNewsNavigation.Index, EvodeskNewsNavigation.Adverts].indexOf(this.newsState.snapshot.currentNavigation);

        matDialogRef.componentInstance.ngOnChanges({
            viewMode: new SimpleChange(undefined, matDialogRef.componentInstance.viewMode, true),
            forceExpandedForm: new SimpleChange(undefined, matDialogRef.componentInstance.forceExpandedForm, true),
            forcePin: new SimpleChange(undefined, matDialogRef.componentInstance.forcePin, true),
            doPushPostToState: new SimpleChange(undefined, matDialogRef.componentInstance.doPushPostToState, true),
        });

        matDialogRef.componentInstance.cancelEvent.pipe(takeUntil(modalClosed$)).subscribe(() => {
            matDialogRef.close();
        });

        matDialogRef.componentInstance.successEvent.pipe(takeUntil(modalClosed$)).subscribe(() => {
            matDialogRef.close();
        });

        matDialogRef.componentInstance.closeModalEvent.pipe(takeUntil(modalClosed$)).subscribe(() => {
            modalClosed$.next(undefined);
            matDialogRef.close();
        });

        this.router.events.pipe(takeUntil(modalClosed$), filter(e => <any>e instanceof NavigationStart)).subscribe(() => {
            modalClosed$.next(undefined);
        });

        this.state = {
            ...this.state,
            matDialogRef: matDialogRef,
        };
    }
}
