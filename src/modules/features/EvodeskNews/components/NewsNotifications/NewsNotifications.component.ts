import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

export interface NewsNotificationsComponentProps {
    hasEntries: boolean;
}

interface State {
    ready: boolean;
}

type Props = NewsNotificationsComponentProps;

@Component({
    selector: 'evodesk-news-notifications',
    templateUrl: './NewsNotifications.component.pug',
    styleUrls: [
        './NewsNotifications.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsNotificationsComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            this.state = {
                ...this.state,
                ready: !! this.props,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
