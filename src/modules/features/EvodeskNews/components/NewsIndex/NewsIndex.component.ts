import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import {EvodeskNewsViewMode} from '../../models/EvodeskNews.model';

export interface NewsIndexComponentProps {
    hasEntries: boolean;
    viewMode: EvodeskNewsViewMode;
}

interface State {
    ready: boolean;
}

type Props = NewsIndexComponentProps;

@Component({
    selector: 'evodesk-news-index',
    templateUrl: './NewsIndex.component.pug',
    styleUrls: [
        './NewsIndex.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsIndexComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            this.state = {
                ...this.state,
                ready: !! this.props,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get shouldDisplayPublishPostForm(): boolean {
        return this.props.viewMode === EvodeskNewsViewMode.Desktop;
    }
}
