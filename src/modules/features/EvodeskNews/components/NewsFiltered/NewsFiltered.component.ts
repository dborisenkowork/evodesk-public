import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

export interface NewsFilteredComponentProps {
    hasEntries: boolean;
}

interface State {
    ready: boolean;
}

type Props = NewsFilteredComponentProps;

@Component({
    selector: 'evodesk-news-filtered',
    templateUrl: './NewsFiltered.component.pug',
    styleUrls: [
        './NewsFiltered.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewsFilteredComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            this.state = {
                ...this.state,
                ready: !! this.props,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
