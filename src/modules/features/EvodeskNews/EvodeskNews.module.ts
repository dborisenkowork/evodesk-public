import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskNewsRoutingModule} from './EvodeskNewsRouting.module';

import {EvodeskFeedModule} from '../EvodeskFeed/EvodeskFeed.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';
import {FilteredRouteComponent} from './routes/FilteredRoute/FilteredRoute.component';
import {NotificationsRouteComponent} from './routes/NotificationsRoute/NotificationsRoute.component';
import {SubscribersRouteComponent} from './routes/SubscribersRoute/SubscribersRoute.component';
import {WorldRouteComponent} from './routes/WorldRoute/WorldRoute.component';
import {AdvertsRouteComponent} from './routes/AdvertsRoute/AdvertsRoute.component';

import {NewsComponent} from './components/News/News.component';
import {NewsNavigationComponent} from './components/NewsNavigation/NewsNavigation.component';
import {NewsNavigationContainerComponent} from './components/NewsNavigation/NewsNavigationContainer.component';
import {NewsFilteredComponent} from './components/NewsFiltered/NewsFiltered.component';
import {NewsFilteredContainerComponent} from './components/NewsFiltered/NewsFilteredContainer.component';
import {NewsIndexComponent} from './components/NewsIndex/NewsIndex.component';
import {NewsIndexContainerComponent} from './components/NewsIndex/NewsIndexContainer.component';
import {NewsNotificationsComponent} from './components/NewsNotifications/NewsNotifications.component';
import {NewsNotificationsContainerComponent} from './components/NewsNotifications/NewsNotificationsContainer.component';
import {NewsSubscribersComponent} from './components/NewsSubscribers/NewsSubscribers.component';
import {NewsSubscribersContainerComponent} from './components/NewsSubscribers/NewsSubscribersContainer.component';
import {NewsWorldComponent} from './components/NewsWorld/NewsWorld.component';
import {NewsWorldContainerComponent} from './components/NewsWorld/NewsWorldContainer.component';
import {NewsAdvertsComponent} from './components/NewsAdverts/NewsAdverts.component';
import {NewsAdvertsContainerComponent} from './components/NewsAdverts/NewsAdvertsContainer.component';
import {NewsNavigationMobileComponent} from './components/NewsNavigationMobile/NewsNavigationMobile.component';
import {NewsNavigationMobileContainerComponent} from './components/NewsNavigationMobile/NewsNavigationMobileContainer.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskNewsRoutingModule,
        EvodeskFeedModule,
    ],
    declarations: [
        IndexRouteComponent,
        FilteredRouteComponent,
        NotificationsRouteComponent,
        SubscribersRouteComponent,
        WorldRouteComponent,
        AdvertsRouteComponent,

        NewsComponent,
        NewsNavigationComponent,
        NewsNavigationContainerComponent,
        NewsFilteredComponent,
        NewsFilteredContainerComponent,
        NewsIndexComponent,
        NewsIndexContainerComponent,
        NewsNotificationsComponent,
        NewsNotificationsContainerComponent,
        NewsSubscribersComponent,
        NewsSubscribersContainerComponent,
        NewsWorldComponent,
        NewsWorldContainerComponent,
        NewsAdvertsComponent,
        NewsAdvertsContainerComponent,
        NewsNavigationMobileComponent,
        NewsNavigationMobileContainerComponent,
    ],
})
export class EvodeskNewsModule
{
}
