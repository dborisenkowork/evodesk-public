import {Routes} from '@angular/router';

import {HasAuthTokenGuard} from '../../EvodeskAuth/guards/HasAuthToken.guard';
import {CurrentUserGuard} from '../../EvodeskAuth/guards/CurrentUser.guard';
import {EvodeskSetCommonHeaderGuard} from '../../EvodeskHeader/guards/EvodeskSetCommonHeader.guard';

import {NewsComponent} from '../components/News/News.component';
import {IndexRouteComponent} from '../routes/IndexRoute/IndexRoute.component';
import {NotificationsRouteComponent} from '../routes/NotificationsRoute/NotificationsRoute.component';
import {SubscribersRouteComponent} from '../routes/SubscribersRoute/SubscribersRoute.component';
import {WorldRouteComponent} from '../routes/WorldRoute/WorldRoute.component';
import {FilteredRouteComponent} from '../routes/FilteredRoute/FilteredRoute.component';
import {AdvertsRouteComponent} from '../routes/AdvertsRoute/AdvertsRoute.component';

export const EvodeskNewsRouting: Routes = [
    {
        path: '',
        pathMatch: 'prefix',
        component: NewsComponent,
        canActivate: [
            HasAuthTokenGuard,
            CurrentUserGuard,
            EvodeskSetCommonHeaderGuard,
        ],
        children: [
            {
                path: '',
                redirectTo: 'index',
            },
            {
                path: 'index',
                component: IndexRouteComponent,
            },
            {
                path: 'notifications',
                component: NotificationsRouteComponent,
            },
            {
                path: 'subscribers',
                component: SubscribersRouteComponent,
            },
            {
                path: 'world',
                component: WorldRouteComponent,
            },
            {
                path: 'filtered',
                component: FilteredRouteComponent,
            },
            {
                path: 'adverts',
                component: AdvertsRouteComponent,
            },
        ],
    },
];
