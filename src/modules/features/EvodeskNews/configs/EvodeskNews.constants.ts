export const evodeskNewsLayoutMobile: { to: number } = { to: 500 };
export const evodeskNewsLayoutTablet: { from: number, to: number } = { from: 501, to: 1160 };
export const evodeskNewsLayoutDesktop: { from: number } = { from: 1161 };
