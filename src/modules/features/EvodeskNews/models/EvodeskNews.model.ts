import {ViewMode} from '../../../../types/ViewMode.types';

export enum EvodeskNewsNavigation
{
    Index = 'index',
    World = 'world',
    Subscribers = 'subscribers',
    Pinned = 'pinned',
    Notifications = 'notifications',
    Adverts = 'adverts',
    Filtered = 'filtered',
}

export const availableNewsNavigations: Array<{ type: EvodeskNewsNavigation, title: string }> = [
    { type: EvodeskNewsNavigation.Index, title: 'EvodeskNews.components.NewsNavigation.Nav.Index' },
    { type: EvodeskNewsNavigation.World, title: 'EvodeskNews.components.NewsNavigation.Nav.World' },
    { type: EvodeskNewsNavigation.Subscribers, title: 'EvodeskNews.components.NewsNavigation.Nav.Subscribers' },
    { type: EvodeskNewsNavigation.Pinned, title: 'EvodeskNews.components.NewsNavigation.Nav.Pinned' },
    { type: EvodeskNewsNavigation.Notifications, title: 'EvodeskNews.components.NewsNavigation.Nav.Notifications' },
    { type: EvodeskNewsNavigation.Adverts, title: 'EvodeskNews.components.NewsNavigation.Nav.Adverts' },
];

export {ViewMode as EvodeskNewsViewMode};
