import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskNewsRouting} from './configs/EvodeskNewsRouting.config';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskNewsRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskNewsRoutingModule
{}
