import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {EvodeskNewsNavigation, EvodeskNewsViewMode} from '../models/EvodeskNews.model';

export enum EvodeskNewsStateAction {
    SetViewMode,
    Navigate,
}

type EvodeskNewsStateActions =
      { type: EvodeskNewsStateAction.SetViewMode, payload: { viewMode: EvodeskNewsViewMode; } }
    | { type: EvodeskNewsStateAction.Navigate, payload: { navigateTo: EvodeskNewsNavigation; } }
;


export interface EvodeskNewsState {
    viewMode: EvodeskNewsViewMode;
    currentNavigation: EvodeskNewsNavigation;
}

type State = EvodeskNewsState;
type PrevState = State | undefined;
type CurrentState = State;

interface States {
    previous: PrevState;
    current: CurrentState;
}

export function initialState(): EvodeskNewsState {
    return {
        viewMode: EvodeskNewsViewMode.Desktop,
        currentNavigation: EvodeskNewsNavigation.Index,
    };
}

@Injectable()
export class EvodeskNewsStateService
{
    private destroy$: Subject<void> = new Subject<void>();

    private _current$: BehaviorSubject<States> = new BehaviorSubject<States>({
        previous: undefined,
        current: initialState(),
    });

    get current$(): Observable<States> {
        return this._current$.asObservable();
    }

    get snapshot(): CurrentState {
        return this._current$.getValue().current;
    }

    reset(): void {
        this._current$.next({
            previous: undefined,
            current: initialState(),
        });
    }

    destroy(): void {
        this.destroy$.next(undefined);
        this.reset();
    }

    dispatch(action: EvodeskNewsStateActions): void {
        switch (action.type) {
            case EvodeskNewsStateAction.SetViewMode:
                this.setState((orig) => {
                    return {
                        ...orig,
                        viewMode: action.payload.viewMode,
                    };
                });

                break;

            case EvodeskNewsStateAction.Navigate:
                this.setState((orig) => {
                    return {
                        ...orig,
                        currentNavigation: action.payload.navigateTo,
                    };
                });

                break;
        }
    }

    private setState(set$: (orig: State) => State) {
        const orig: State = this._current$.getValue().current;

        this._current$.next({
            previous: orig,
            current: set$(orig),
        });
    }
}
