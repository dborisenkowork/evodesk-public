import {Injectable} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';

import {filter, takeUntil} from 'rxjs/operators';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {EvodeskNewsViewMode} from '../models/EvodeskNews.model';

import {evodeskNewsLayoutDesktop, evodeskNewsLayoutMobile, evodeskNewsLayoutTablet} from '../configs/EvodeskNews.constants';

@Injectable()
export class EvodeskNewsBreakpointsService
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private _current$: BehaviorSubject<EvodeskNewsViewMode> = new BehaviorSubject<EvodeskNewsViewMode>(EvodeskNewsViewMode.Desktop);

    constructor(
        private breakpointObserver: BreakpointObserver,
    ) {}

    init(): Observable<EvodeskNewsViewMode> {
        this.breakpointObserver.observe(`(max-width: ${evodeskNewsLayoutMobile.to}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe((e) => {
            this._current$.next(EvodeskNewsViewMode.Mobile);
        });

        this.breakpointObserver.observe(`(min-width: ${evodeskNewsLayoutTablet.from}px) and (max-width: ${evodeskNewsLayoutTablet.to}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this._current$.next(EvodeskNewsViewMode.Tablet);
        });

        this.breakpointObserver.observe(`(min-width: ${evodeskNewsLayoutDesktop.from}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this._current$.next(EvodeskNewsViewMode.Desktop);
        });

        return this.current$;
    }

    destroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get current$(): Observable<EvodeskNewsViewMode> {
        return this._current$.asObservable();
    }
}
