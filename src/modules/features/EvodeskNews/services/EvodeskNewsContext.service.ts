import {Injectable, ViewContainerRef} from '@angular/core';

import {EvodeskFeedStateService} from '../../EvodeskFeed/state/EvodeskFeedState.service';
import {EvodeskNewsStateService} from './EvodeskNewsState.service';
import {EvodeskFeedService} from '../../EvodeskFeed/services/EvodeskFeed.service';

import {FeedSourceAdapter} from '../../EvodeskFeed/source-adapters/FeedSourceAdapter.interface';

interface Context {
    state: EvodeskNewsStateService;
    feedState: EvodeskFeedStateService;
    feedService: EvodeskFeedService;
    feedSource: FeedSourceAdapter;
    viewContainerRef: ViewContainerRef;
}

interface Set$ {
    state?: EvodeskNewsStateService;
    feedState?: EvodeskFeedStateService;
    feedService?: EvodeskFeedService;
    feedSource?: FeedSourceAdapter;
    viewContainerRef?: ViewContainerRef;
}

@Injectable()
export class EvodeskNewsContextService
{
    private _context: Context;

    setContext(set$: Set$): void {
        this._context = { ...this._context, ...set$ };
    }

    get context(): Context {
        return this._context;
    }
}
