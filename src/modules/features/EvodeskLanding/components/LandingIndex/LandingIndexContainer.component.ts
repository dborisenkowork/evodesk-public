import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskHeaderLayoutVariant, EvodeskHeaderMenuItem} from '../../../EvodeskHeader/models/EvodeskHeader.models';
import {AppEvent} from '../../../../app/EvodeskAppMessageBus/models/app-events.model';

import {EvodeskHeaderConfigurationService} from '../../../EvodeskHeader/services/EvodeskHeaderConfiguration.service';
import {EvodeskAppMessageBusService} from '../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';
import {EvodeskConditionsModalService} from '../../services/EvodeskConditionsModal.service';

@Component({
    selector: 'evodesk-landing-index-container',
    template: `<evodesk-landing-index></evodesk-landing-index>`,
})
export class LandingIndexContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private appBus: EvodeskAppMessageBusService,
        private evodeskHeaderConfigurationService: EvodeskHeaderConfigurationService,
        private evodeskLandingConditionsModalService: EvodeskConditionsModalService,
    ) {}

    ngOnInit(): void {
        this.evodeskHeaderConfigurationService.lockPanels(this.ngOnDestroy$);
        this.evodeskHeaderConfigurationService.useAbsolutePositions(this.ngOnDestroy$);
        this.evodeskHeaderConfigurationService.setVariant(EvodeskHeaderLayoutVariant.LandingNoBg);

        this.appBus.stream$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((e) => {
            switch (e.type) {
                case AppEvent.HeaderMobileClickItem: {
                    const menuItem: EvodeskHeaderMenuItem = e.payload;

                    if (menuItem && menuItem.id === 'conditions') {
                        this.evodeskLandingConditionsModalService.showConditions();
                    }

                    break;
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
