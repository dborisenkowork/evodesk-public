import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, ViewChild } from '@angular/core';

import * as objectFitVideos from 'object-fit-videos';

interface State {
    isVideoIntroOpened: boolean;
}

@Component({
    selector: 'evodesk-landing-index',
    templateUrl: './LandingIndex.component.pug',
    styleUrls: [
        './LandingIndex.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingIndexComponent implements AfterViewInit
{

    @ViewChild('evodeskLandingBackgroundVideo') backgroundVideo: ElementRef;

    public state: State = {
        isVideoIntroOpened: false,
    };

    ngAfterViewInit(): void {
        this.backgroundVideo.nativeElement.muted = true;
        this.backgroundVideo.nativeElement.oncanplaythrough = () => {
          this.backgroundVideo.nativeElement.play();
          objectFitVideos();
        };
    }

    openVideoIntro(): void {
        const windowWidth: number = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

        if (windowWidth < 440) {
            window.open('https://www.youtube.com/embed/Llres1dSBrs');
        } else {
            this.state = {
                ...this.state,
                isVideoIntroOpened: true,
            };
        }
    }

    closeVideoIntro(): void {
        this.state = {
            ...this.state,
            isVideoIntroOpened: false,
        };
    }
}
