import {ChangeDetectionStrategy, Component} from '@angular/core';

import {AuthTokenService} from '../../../EvodeskAuth/services/AuthToken.service';

@Component({
    selector: 'evodesk-landing-auth-panel',
    templateUrl: './LandingAuthPanel.component.pug',
    styleUrls: [
        './LandingAuthPanel.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingAuthPanelComponent
{
    constructor(
        private authToken: AuthTokenService,
    ) {}

    get hasAuthToken(): boolean {
        return this.authToken.has();
    }
}
