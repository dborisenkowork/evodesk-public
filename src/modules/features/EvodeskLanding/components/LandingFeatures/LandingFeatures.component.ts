import {AfterViewInit, ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Subject} from 'rxjs';
import {take, takeUntil} from 'rxjs/operators';

import {ScrollToService} from '@nicky-lenaers/ngx-scroll-to';

export enum TabType {
    Text = 'text',
    YouTube = 'youtube',
}

export type LandingFeaturesComponentTab = LandingFeaturesComponentTextTab | LandingFeaturesComponentYouTubeTab;

export interface LandingFeaturesComponentTextTab {
    type: TabType.Text;
    tabTitle: string;
    slides: Array<{
        title: string;
        htmlText: string;
        img: string;
    }>;
}

export interface LandingFeaturesComponentYouTubeTab {
    type: TabType.YouTube;
    tabTitle: string;
    youtubeLink: string;
}

export interface LandingFeaturesComponentPropsFunction {
    img: string;
    title: string;
    anchorDOMId: string;
    enabled: boolean;
}

export interface LandingFeaturesComponentPropsBlock {
    title: string;
    subTitle: string;
    anchorDOMId: string;
    tabs: Array<LandingFeaturesComponentTab>;
}

export interface LandingFeaturesComponentProps {
    functions: Array<LandingFeaturesComponentPropsFunction>;
    blocks: Array<LandingFeaturesComponentPropsBlock>;
}

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-landing-features',
    templateUrl: './LandingFeatures.component.pug',
    styleUrls: [
        './LandingFeatures.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingFeaturesComponent implements OnChanges, AfterViewInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    @Input() props: LandingFeaturesComponentProps;

    public state: State = {
        ready: false,
    };

    constructor(
        private route: ActivatedRoute,
        private scrollToService: ScrollToService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngAfterViewInit(): void {
        this.route.fragment.pipe(take(1)).subscribe(f => {
              this.scrollTo(f);
          });
    }

    scrollTo(f: string) {
        const desktopElement: HTMLElement = document.getElementById(f);
        const mobileElement: HTMLElement = document.getElementById(`mobile-${f}`);

        if (desktopElement.offsetTop === 0) {
            this.scrollToService.scrollTo({
              target: `mobile-${f}`,
            }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(next => {
                document.body.scrollTop = next - 80;
            });
        } else if (mobileElement.offsetTop === 0) {
            this.scrollToService.scrollTo({
              target: f,
            }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(next => {
                document.body.scrollTop = next - 80;
            });
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get images(): Array<string> {
        const images: Array<string> = [
            '/assets/landing/features/swiper/left.svg',
            '/assets/landing/features/swiper/left-active.svg',
            '/assets/landing/features/swiper/right.svg',
            '/assets/landing/features/swiper/right-active.svg',
        ];

        this.props.functions.forEach(f => {
            images.push(f.img);
        });

        this.props.blocks.forEach(block => {
            block.tabs.forEach(tab => {
                if (tab.type === TabType.Text) {
                    tab.slides.forEach(slide => {
                        images.push(slide.img);
                    });
                }
            });
        });

        return images;
    }

    get enabledFunctions(): Array<LandingFeaturesComponentPropsFunction> {
        return this.props.functions.filter(f => f.enabled);
    }
}
