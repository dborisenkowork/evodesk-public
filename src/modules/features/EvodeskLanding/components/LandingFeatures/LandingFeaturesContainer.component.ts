import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {LandingFeaturesComponentProps, TabType} from './LandingFeatures.component';

import {AppEvent} from '../../../../app/EvodeskAppMessageBus/models/app-events.model';
import {EvodeskHeaderMenuItem} from '../../../EvodeskHeader/models/EvodeskHeader.models';

import {EvodeskAppMessageBusService} from '../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';
import {EvodeskConditionsModalService} from '../../services/EvodeskConditionsModal.service';

interface State {
    ready: boolean;
    props?: LandingFeaturesComponentProps;
}

@Component({
    selector: 'evodesk-landing-features-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-landing-features [props]="state.props"></evodesk-landing-features>
      </ng-container>
    `,
})
export class LandingFeaturesContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        props: {
            functions: [
                {
                    img: '/assets/landing/features/functions/tasks.svg',
                    title: 'EvodeskLanding.components.LandingFeatures.Functions.Tasks.AnchorLink',
                    anchorDOMId: 'Tasks',
                    enabled: true,
                },
                {
                    img: '/assets/landing/features/functions/your-competence.svg',
                    title: 'EvodeskLanding.components.LandingFeatures.Functions.WorkWithYourCompetence.AnchorLink',
                    anchorDOMId: 'WorkWithYourCompetence',
                    enabled: false,
                },
                {
                    img: '/assets/landing/features/functions/your-competence.svg',
                    title: 'EvodeskLanding.components.LandingFeatures.Functions.Rewards.AnchorLink',
                    anchorDOMId: 'Rewards',
                    enabled: true,
                },
                {
                    img: '/assets/landing/features/functions/teamwork.svg',
                    title: 'EvodeskLanding.components.LandingFeatures.Functions.TeamWork.AnchorLink',
                    anchorDOMId: 'TeamWork',
                    enabled: true,
                },
                {
                    img: '/assets/landing/features/functions/work-with-yourself.svg',
                    title: 'EvodeskLanding.components.LandingFeatures.Functions.WothWithYourself.AnchorLink',
                    anchorDOMId: 'WothWithYourself',
                    enabled: true,
                },
                {
                    img: '/assets/landing/features/functions/experts.svg',
                    title: 'EvodeskLanding.components.LandingFeatures.Functions.Experts.AnchorLink',
                    anchorDOMId: 'Experts',
                    enabled: true,
                },
                {
                    img: '/assets/landing/features/functions/gangplank-moneymaking.svg',
                    title: 'EvodeskLanding.components.LandingFeatures.Functions.Moneymaking.AnchorLink',
                    anchorDOMId: 'Moneymaking',
                    enabled: true,
                },
            ],
            blocks: [
                {
                    title: 'EvodeskLanding.components.LandingFeatures.Functions.Tasks.Title',
                    subTitle: 'EvodeskLanding.components.LandingFeatures.Functions.Tasks.SubTitle',
                    anchorDOMId: 'Tasks',
                    tabs: [
                        {
                            type: TabType.Text,
                            tabTitle: 'EvodeskLanding.components.LandingFeatures.Functions.Tasks.Tabs.Description',
                            slides: (() => {
                                return [1, 2, 3, 4].map(i => {
                                    return {
                                        title: `EvodeskLanding.components.LandingFeatures.Functions.Tasks.Blocks.B${i}.Title`,
                                        htmlText: `EvodeskLanding.components.LandingFeatures.Functions.Tasks.Blocks.B${i}.Text`,
                                        img: `/assets/landing/features/slides/tasks/${i}.png`,
                                    };
                                });
                            })(),
                        },
                    ],
                },
                {
                    title: 'EvodeskLanding.components.LandingFeatures.Functions.Rewards.Title',
                    subTitle: 'EvodeskLanding.components.LandingFeatures.Functions.Rewards.SubTitle',
                    anchorDOMId: 'Rewards',
                    tabs: [
                        {
                            type: TabType.Text,
                            tabTitle: 'EvodeskLanding.components.LandingFeatures.Functions.Rewards.Tabs.Description',
                            slides: (() => {
                                return [1, 2, 3, 4, 5, 6].map(i => {
                                    return {
                                        title: `EvodeskLanding.components.LandingFeatures.Functions.Rewards.Blocks.B${i}.Title`,
                                        htmlText: `EvodeskLanding.components.LandingFeatures.Functions.Rewards.Blocks.B${i}.Text`,
                                        img: `/assets/landing/features/slides/rewards/${i}.png`,
                                    };
                                });
                            })(),
                        },
                    ],
                },
                {
                    title: 'EvodeskLanding.components.LandingFeatures.Functions.TeamWork.Title',
                    subTitle: 'EvodeskLanding.components.LandingFeatures.Functions.TeamWork.SubTitle',
                    anchorDOMId: 'TeamWork',
                    tabs: [
                        {
                            type: TabType.Text,
                            tabTitle: 'EvodeskLanding.components.LandingFeatures.Functions.TeamWork.Tabs.Description',
                            slides: (() => {
                                return [1, 2, 3, 4].map(i => {
                                    return {
                                        title: `EvodeskLanding.components.LandingFeatures.Functions.TeamWork.Blocks.B${i}.Title`,
                                        htmlText: `EvodeskLanding.components.LandingFeatures.Functions.TeamWork.Blocks.B${i}.Text`,
                                        img: `/assets/landing/features/slides/teamwork/${i}.png`,
                                    };
                                });
                            })(),
                        },
                    ],
                },
                {
                    title: 'EvodeskLanding.components.LandingFeatures.Functions.Moneymaking.Title',
                    subTitle: 'EvodeskLanding.components.LandingFeatures.Functions.Moneymaking.SubTitle',
                    anchorDOMId: 'Moneymaking',
                    tabs: [
                        {
                            type: TabType.Text,
                            tabTitle: 'EvodeskLanding.components.LandingFeatures.Functions.Moneymaking.Tabs.Description',
                            slides: (() => {
                                return [1, 2, 3, 4, 5].map(i => {
                                    return {
                                        title: `EvodeskLanding.components.LandingFeatures.Functions.Moneymaking.Blocks.B${i}.Title`,
                                        htmlText: `EvodeskLanding.components.LandingFeatures.Functions.Moneymaking.Blocks.B${i}.Text`,
                                        img: `/assets/landing/features/slides/moneymaking/${i}.png`,
                                    };
                                });
                            })(),
                        },
                    ],
                },
                {
                    title: 'EvodeskLanding.components.LandingFeatures.Functions.Experts.Title',
                    subTitle: 'EvodeskLanding.components.LandingFeatures.Functions.Experts.SubTitle',
                    anchorDOMId: 'Tasks',
                    tabs: [
                        {
                            type: TabType.Text,
                            tabTitle: 'EvodeskLanding.components.LandingFeatures.Functions.Experts.Tabs.Description',
                            slides: (() => {
                                return [1, 2].map(i => {
                                    return {
                                        title: `EvodeskLanding.components.LandingFeatures.Functions.Experts.Blocks.B${i}.Title`,
                                        htmlText: `EvodeskLanding.components.LandingFeatures.Functions.Experts.Blocks.B${i}.Text`,
                                        img: `/assets/landing/features/slides/experts/${i}.png`,
                                    };
                                });
                            })(),
                        },
                    ],
                },
            ],
        },
    };

    constructor(
        private appBus: EvodeskAppMessageBusService,
        private evodeskLandingConditionsModalService: EvodeskConditionsModalService,
    ) {}

    ngOnInit(): void {
        this.appBus.stream$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((e) => {
            switch (e.type) {
                case AppEvent.HeaderMobileClickItem: {
                    const menuItem: EvodeskHeaderMenuItem = e.payload;

                    if (menuItem && menuItem.id === 'conditions') {
                        this.evodeskLandingConditionsModalService.showConditions();
                    }

                    break;
                }
            }
        });

        this.appBus.stream$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((e) => {
            switch (e.type) {
                case AppEvent.HeaderMobileClickItem: {
                    const menuItem: EvodeskHeaderMenuItem = e.payload;

                    if (menuItem && menuItem.id === 'conditions') {
                        this.evodeskLandingConditionsModalService.showConditions();
                    }

                    break;
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
