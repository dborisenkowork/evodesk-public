import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';

import {LandingDevelopersComponentProps} from './LandingDevelopers.component';

import {AppEvent} from '../../../../app/EvodeskAppMessageBus/models/app-events.model';
import {EvodeskHeaderMenuItem} from '../../../EvodeskHeader/models/EvodeskHeader.models';

import {EvodeskConditionsModalService} from '../../services/EvodeskConditionsModal.service';
import {EvodeskAppMessageBusService} from '../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';
import {takeUntil} from 'rxjs/operators';

interface State {
    ready: boolean;
    props?: LandingDevelopersComponentProps;
}

@Component({
    selector: 'evodesk-landing-developers-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-landing-developers [props]="state.props"></evodesk-landing-developers>
      </ng-container>
    `,
})
export class LandingDevelopersContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        props: {},
    };

    constructor(
        private appBus: EvodeskAppMessageBusService,
        private evodeskLandingConditionsModalService: EvodeskConditionsModalService,
    ) {}

    ngOnInit(): void {
        this.appBus.stream$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((e) => {
            switch (e.type) {
                case AppEvent.HeaderMobileClickItem: {
                    const menuItem: EvodeskHeaderMenuItem = e.payload;

                    if (menuItem && menuItem.id === 'conditions') {
                        this.evodeskLandingConditionsModalService.showConditions();
                    }

                    break;
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
