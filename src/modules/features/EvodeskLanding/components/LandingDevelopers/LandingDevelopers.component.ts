import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

export interface LandingDevelopersComponentProps {
}

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-landing-developers',
    templateUrl: './LandingDevelopers.component.pug',
    styleUrls: [
        './LandingDevelopers.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingDevelopersComponent implements OnChanges
{
    @Input() props: LandingDevelopersComponentProps;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }
}
