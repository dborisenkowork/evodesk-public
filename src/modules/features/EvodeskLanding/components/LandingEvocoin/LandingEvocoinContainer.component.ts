import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {LandingEvocoinComponentProps} from './LandingEvocoin.component';

import {AppEvent} from '../../../../app/EvodeskAppMessageBus/models/app-events.model';
import {EvodeskHeaderMenuItem} from '../../../EvodeskHeader/models/EvodeskHeader.models';

import {EvodeskConditionsModalService} from '../../services/EvodeskConditionsModal.service';
import {EvodeskAppMessageBusService} from '../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';

interface State {
    ready: boolean;
    props?: LandingEvocoinComponentProps;
}

@Component({
    selector: 'evodesk-landing-evocoin-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-landing-evocoin [props]="state.props"></evodesk-landing-evocoin>
      </ng-container>
    `,
})
export class LandingEvocoinContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        props: {
            facts: [1, 2, 3, 4].map(index => {
                return {
                    id: index,
                    title: `EvodeskLanding.components.LandingEvocoin.Facts.F${index}.Title`,
                    htmlText: `EvodeskLanding.components.LandingEvocoin.Facts.F${index}.Text`,
                };
            }),
        },
    };

    constructor(
        private appBus: EvodeskAppMessageBusService,
        private evodeskLandingConditionsModalService: EvodeskConditionsModalService,
    ) {}

    ngOnInit(): void {
        this.appBus.stream$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((e) => {
            switch (e.type) {
                case AppEvent.HeaderMobileClickItem: {
                    const menuItem: EvodeskHeaderMenuItem = e.payload;

                    if (menuItem && menuItem.id === 'conditions') {
                        this.evodeskLandingConditionsModalService.showConditions();
                    }

                    break;
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
