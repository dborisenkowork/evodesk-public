import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

type FactId = number;

export interface LandingEvocoinComponentProps {
    facts: Array<{
        id: FactId;
        title: string;
        htmlText: string;
    }>;
}

interface State {
    ready: boolean;
    opened: Array<FactId>;
}

@Component({
    selector: 'evodesk-landing-evocoin',
    templateUrl: './LandingEvocoin.component.pug',
    styleUrls: [
        './LandingEvocoin.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingEvocoinComponent implements OnChanges
{
    @Input() props: LandingEvocoinComponentProps;

    public state: State = {
        ready: false,
        opened: [],
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    isOpened(id: number): boolean {
        return !!~this.state.opened.indexOf(id);
    }

    toggle(id: number): void {
        if (this.isOpened(id)) {
            this.close(id);
        } else {
            this.open(id);
        }
    }

    open(id: number): void {
        this.state = {
            ...this.state,
            opened: [id],
        };
    }

    close(id: number): void {
        this.state = {
            ...this.state,
            opened: this.state.opened.filter(s => s !== id),
        };
    }
}
