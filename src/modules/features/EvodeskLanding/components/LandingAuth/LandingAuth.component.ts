import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatCheckboxChange, MatTabChangeEvent, MatTabGroup} from '@angular/material';

import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

type CurrentTab = 'sign-in' | 'register';

export type LandingAuthThemes = 'dark' | 'light';

interface State {
    ready: boolean;
    agree: boolean;
    currentTab: CurrentTab;
    formRegister: FormGroup;
    formSignIn: FormGroup;
    lastUsedEmail: string | undefined;
    tabAnimationInProgress: boolean;
}

export interface LandingAuthComponentProps {
    submitting: boolean;
    formSignInError: string |  undefined;
    formRegisterError: string |  undefined;
    theme: LandingAuthThemes;
    currentProfileId?: ProfileId;
    currentProfileName?: string;
    currentProfileStatus?: string;
}

type FormValue = LandingAuthComponentRegisterFormValue;

export interface LandingAuthComponentRegisterFormValue {
    email: string;
    password: string;
    promo: string;
}

export interface LandingAuthComponentSignInFormValue {
    email: string;
    password: string;
    persist: boolean;
}

@Component({
    selector: 'evodesk-landing-auth',
    templateUrl: './LandingAuth.component.pug',
    styleUrls: [
        './LandingAuth.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingAuthComponent implements OnChanges, AfterViewInit
{
    @Input() props: LandingAuthComponentProps;
    @Input() initialAutoFocus: boolean = false;

    @Output('formSignInSubmit') formSignInSubmitEvent: EventEmitter<LandingAuthComponentSignInFormValue> = new EventEmitter<LandingAuthComponentSignInFormValue>();
    @Output('formRegisterSubmit') formRegisterSubmitEvent: EventEmitter<LandingAuthComponentRegisterFormValue> = new EventEmitter<LandingAuthComponentRegisterFormValue>();

    @Output('forgotPassword') forgotPasswordEvent: EventEmitter<string> = new EventEmitter<string>();

    @Output('showConditions') showConditionsEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('noAgreement') noAgreementEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output('signInSocial') signInSocialEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('matTabGroup') matTabGroup: MatTabGroup;
    @ViewChild('signInEmailRef') signInEmailRef: ElementRef;
    @ViewChild('signInPasswordRef') signInPasswordRef: ElementRef;
    @ViewChild('registerEmail') registerEmailRef: ElementRef;

    public state: State = {
        ready: false,
        agree: false,
        currentTab: 'sign-in',
        lastUsedEmail: undefined,
        tabAnimationInProgress: false,
        formRegister: this.fb.group({
            email: ['', [Validators.email, Validators.required]],
            password: ['', [Validators.required]],
            promo: ['', []],
        }),
        formSignIn: this.fb.group({
            email: ['', [Validators.email, Validators.required]],
            password: ['', [Validators.required, Validators.minLength(1)]],
            persist: [true],
        }),
    };

    constructor(
        private fb: FormBuilder,
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };

                if (this.props.submitting) {
                    this.state.formSignIn.disable();
                    this.state.formRegister.disable();
                } else {
                    this.state.formSignIn.enable();
                    this.state.formRegister.enable();
                }
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngAfterViewInit(): void {
        if (this.initialAutoFocus) {
            this.doAutoFocus();
        }
    }

    doAutoFocus(options: { isSignInError: boolean } = { isSignInError: false }): void {
        window.setTimeout(() => {
            if (this.state.currentTab === 'sign-in') {
                if (options.isSignInError) {
                    if (this.signInPasswordRef) {
                        (this.signInPasswordRef.nativeElement as HTMLElement).focus();
                    }
                } else {
                    if (this.signInEmailRef) {
                        (this.signInEmailRef.nativeElement as HTMLElement).focus();
                    }
                }
            } else if (this.state.currentTab === 'register') {
                if (this.registerEmailRef) {
                    (this.registerEmailRef.nativeElement as HTMLElement).focus();
                }
            }
        });
    }

    onSelectedTabChange($event: MatTabChangeEvent): void {
        const tab: CurrentTab = $event.index === 0 ? 'sign-in' : 'register';

        this.state = {
            ...this.state,
            currentTab: tab,
            tabAnimationInProgress: true,
        };
    }

    onTabAnimationDone() {
      if (this.state.tabAnimationInProgress) {
        this.doAutoFocus();
      }

      this.state = {
          ...this.state,
          tabAnimationInProgress: false,
      };
    }

    onFormSignInSubmit(): void {
        if (this.state.formSignIn.valid) {
            this.state = {
                ...this.state,
                lastUsedEmail: (this.state.formSignIn.value as FormValue).email,
            };

            this.formSignInSubmitEvent.emit(this.state.formSignIn.value);
        }
    }

    onFormRegisterSubmit(): void {
        if (this.state.formRegister.valid) {
            if (this.state.agree) {
                this.formRegisterSubmitEvent.emit(this.state.formRegister.value);
            } else {
                this.noAgreementEvent.emit(undefined);
            }
        }
    }

    goSignInWithParams(params: { email: string, password: string }): void {
        this.state.formSignIn.enable();
        this.state.formRegister.enable();
        this.state.formRegister.reset();

        this.matTabGroup.selectedIndex = 0;

        this.state.formSignIn.patchValue({
            email: params.email,
            password: params.password,
        });

        this.state = {
            ...this.state,
            currentTab: 'sign-in',
        };

        setTimeout(() => {
            this.cdr.detectChanges();

            this.doAutoFocus();
        });
    }

    showConditions(): void {
        this.showConditionsEvent.emit(undefined);
    }

    agree($event: MatCheckboxChange): void {
        this.state = {
            ...this.state,
            agree: $event.checked,
        };
    }

    forgotPassword(): void {
        this.forgotPasswordEvent.emit(this.state.lastUsedEmail);
    }

    signInSocial(): void {
        this.signInSocialEvent.emit(undefined);
    }
}
