import {ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewChild, ViewContainerRef} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {defaultBottomMatDialogConfig, defaultMatDialogConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {ProfileModel} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {LandingAuthComponent, LandingAuthComponentProps, LandingAuthComponentRegisterFormValue, LandingAuthComponentSignInFormValue, LandingAuthThemes} from './LandingAuth.component';
import {LandingModalsSubmittingComponent, LandingModalsSubmittingComponentProps} from '../LandingModalSubmitting/LandingModalsSubmitting.component';
import {LandingOAuthContainerComponent as OAuthModal} from '../LandingOAuth/LandingOAuthContainer.component';

import {AuthToken, AuthTokenService} from '../../../EvodeskAuth/services/AuthToken.service';
import {SignUpResponse200, UserRESTService} from '../../../../app/EvodeskRESTApi/services/UserREST.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {EvodeskConditionsModalService} from '../../services/EvodeskConditionsModal.service';
import {ProfileRESTService} from '../../../../app/EvodeskRESTApi/services/ProfileREST.service';
import {EvodeskConfirmModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';

interface State {
    ready: boolean;
    props?: LandingAuthComponentProps;
    registeredWith: Array<{ email: string; password: string; }>;
}

@Component({
    selector: 'evodesk-landing-auth-container',
    template: `
      <ng-container *ngIf="!!state && state.ready && state.props">
        <evodesk-landing-auth #view [props]="state.props"
          [initialAutoFocus]="initialAutoFocus"
          (formSignInSubmit)="onFormSignInSubmit($event)"
          (formRegisterSubmit)="onFormRegisterSubmit($event)"
          (forgotPassword)="onForgotPassword($event)"
          (showConditions)="onShowConditions()"
          (noAgreement)="onNoAgreement()"
          (signInSocial)="onSignInSocial()"
        ></evodesk-landing-auth>
      </ng-container>
    `,
})
export class LandingAuthContainerComponent implements OnDestroy, OnChanges
{
    private nextSubmit$: Subject<void> = new Subject<void>();

    @ViewChild('view') view: LandingAuthComponent;

    @Input() theme: LandingAuthThemes = 'dark';
    @Input() initialAutoFocus: boolean = false;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        registeredWith: [],
        props: {
            submitting: false,
            formSignInError: undefined,
            formRegisterError: undefined,
            theme: this.theme,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private router: Router,
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private authToken: AuthTokenService,
        private userRESTService: UserRESTService,
        private uiAlertService: EvodeskAlertModalService,
        private uiConfirmService: EvodeskConfirmModalService,
        private conditionsModalService: EvodeskConditionsModalService,
        private profileRESTService: ProfileRESTService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.state = {
            ...this.state,
            ready: true,
            props: {
                ...this.state.props,
                theme: this.theme,
            },
        };

        if (this.authToken.token) {
            this.state = {
                ...this.state,
                ready: false,
            };

            if (window.sessionStorage.getItem('evodeskLandingCurrentProfile')) {
                const profile: ProfileModel = JSON.parse(<any>window.sessionStorage.getItem('evodeskLandingCurrentProfile'));

                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        currentProfileId: this.authToken.token.id,
                        currentProfileName: profile.name,
                        currentProfileStatus: profile.position,
                    },
                };
            } else {
                this.profileRESTService.getProfile(this.authToken.token.id).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                    (profile) => {
                        window.sessionStorage.setItem('evodeskLandingCurrentProfile', JSON.stringify({
                            name: profile.name || profile.email,
                            position: profile.position,
                        }));

                        this.state = {
                            ...this.state,
                            ready: true,
                            props: {
                                ...this.state.props,
                                currentProfileId: this.authToken.token.id,
                                currentProfileName: profile.name || profile.email,
                                currentProfileStatus: profile.position,
                            },
                        };

                        this.cdr.detectChanges();
                    },
                    () => {
                        this.state = {
                            ...this.state,
                            ready: true,
                        };

                        this.cdr.detectChanges();
                    },
                );
            }
        }

        this.cdr.detectChanges();
    }

    ngOnDestroy(): void {
        this.nextSubmit$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    onFormSignInSubmit(formValue: LandingAuthComponentSignInFormValue): void {
        this.nextSubmit$.next(undefined);

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                submitting: true,
                formSignInError: undefined,
                formRegisterError: undefined,
            },
        };

        const success: Function = (token: AuthToken) => {
            this.authToken.setToken(token, {
                persist: formValue.persist,
                refresh: false,
                markSignedInFlag: true,
            });

            this.router.navigate(['/profile']);

            this.cdr.markForCheck();
        };

        const fail: Function = () => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    submitting: false,
                    formSignInError: (() => {
                        if (this.state.registeredWith.filter(rw => rw.email === formValue.email && rw.password === formValue.password).length > 0) {
                            return 'EvodeskLanding.components.LandingAuth.ErrorSignInNotActivated';
                        } else {
                            return 'EvodeskLanding.components.LandingAuth.ErrorSignIn';
                        }
                    })(),
                },
            };

            this.view.doAutoFocus({
                isSignInError: true,
            });

            this.cdr.markForCheck();
        };

        this.userRESTService.signIn({
            email: formValue.email,
            password: formValue.password,
        }).pipe(takeUntil(this.nextSubmit$)).subscribe(
            (httpResponse) => {
                if (httpResponse && (typeof httpResponse === 'object')) {
                    success(<AuthToken>{
                        id: httpResponse.user.id,
                        access_token: httpResponse.access_token,
                        refresh_token: httpResponse.refresh_token,
                        persist: formValue.persist,
                    });
                } else {
                    fail();
                }
            },
            () => {
                fail();
            },
        );
    }

    onFormRegisterSubmit(formValue: LandingAuthComponentRegisterFormValue): void {
        this.nextSubmit$.next(undefined);

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                submitting: true,
                formSignInError: undefined,
                formRegisterError: undefined,
            },
        };

        const success: Function = () => {
            this.state = {
                ...this.state,
                registeredWith: [...this.state.registeredWith, {
                    email: formValue.email,
                    password: formValue.password,
                }],
            };

            this.uiAlertService.openTranslated('EvodeskLanding.components.LandingAuth.RegisterAlert').pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                setTimeout(() => {
                    this.view.goSignInWithParams({
                        email: formValue.email,
                        password: formValue.password,
                    });
                });
            });

            this.cdr.detectChanges();
        };

        const fail: Function = () => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    submitting: false,
                    formRegisterError: 'EvodeskLanding.components.LandingAuth.ErrorRegister',
                },
            };

            this.view.doAutoFocus();

            this.cdr.markForCheck();
        };

        this.userRESTService.signUp({
            email: formValue.email,
            password: formValue.password,
            checked: true,
            promo: formValue.promo,
        }).pipe(takeUntil(this.nextSubmit$)).subscribe(
            (httpResponse) => {
                if (httpResponse && (typeof httpResponse === 'object') && ((<SignUpResponse200>httpResponse).activated !== undefined)) {
                    success(httpResponse);
                } else {
                    fail();
                }
            },
            () => {
                fail();
            },
        );
    }

    onForgotPassword(email: string): void {
        this.uiConfirmService.open({
            title: {
                text: 'EvodeskLanding.components.LandingAuth.ForgotPasswordConfirm.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskLanding.components.LandingAuth.ForgotPasswordConfirm.Text',
                translate: true,
                replaces: [
                    email,
                ],
            },
            cancelButton: {
                text: 'EvodeskLanding.components.LandingAuth.ForgotPasswordConfirm.Cancel',
                translate: true,
            },
            confirmButton: {
                text: 'EvodeskLanding.components.LandingAuth.ForgotPasswordConfirm.Confirm',
                translate: true,
            },
            confirm: () => {
                const submitting: MatDialogRef<LandingModalsSubmittingComponent> = this.matDialog.open(LandingModalsSubmittingComponent, {
                    ...defaultMatDialogConfig,
                    disableClose: true,
                    viewContainerRef: this.viewContainerRef,
                    data: <LandingModalsSubmittingComponentProps>{
                        text: 'EvodeskLanding.components.LandingAuth.SubmittingForgotPassword',
                    },
                });

                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        submitting: true,
                    },
                };

                this.cdr.markForCheck();

                this.userRESTService.resetPassword({ email: email }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                    () => {
                        submitting.close();

                        this.uiAlertService.openTranslated('EvodeskLanding.components.LandingAuth.ForgotPasswordSent').pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                            this.state = {
                                ...this.state,
                                props: {
                                    ...this.state.props,
                                    submitting: false,
                                    formSignInError: undefined,
                                },
                            };

                            this.cdr.markForCheck();
                        });
                    },
                    () => {
                        submitting.close();

                        this.uiAlertService.openTranslated('EvodeskLanding.components.LandingAuth.ForgotPasswordFail').pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                            this.state = {
                                ...this.state,
                                props: {
                                    ...this.state.props,
                                    submitting: false,
                                },
                            };

                            this.cdr.markForCheck();
                        });
                    },
                );
            },
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }

    onShowConditions(): void {
        this.conditionsModalService.showConditions();
    }

    onNoAgreement(): void {
        this.uiAlertService.openTranslated('EvodeskLanding.components.LandingAuth.DoAcceptAgreement').pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }

    onSignInSocial(): void {
        const closed$: Subject<void> = new Subject<void>();

        const dialogRef: MatDialogRef<OAuthModal> = this.matDialog.open(OAuthModal, {
            ...defaultBottomMatDialogConfig,
            viewContainerRef: this.viewContainerRef,
            closeOnNavigation: true,
        });

        dialogRef.afterClosed().pipe(takeUntil(closed$)).subscribe(() => {
            closed$.next(undefined);
        });

        dialogRef.componentInstance.closeEvent.pipe(takeUntil(closed$)).subscribe(() => {
            dialogRef.close();
        });
    }
}
