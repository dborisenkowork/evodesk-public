import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {LandingPartnersComponentProps} from './LandingPartners.component';

import {AppEvent} from '../../../../app/EvodeskAppMessageBus/models/app-events.model';
import {EvodeskHeaderMenuItem} from '../../../EvodeskHeader/models/EvodeskHeader.models';

import {EvodeskConditionsModalService} from '../../services/EvodeskConditionsModal.service';
import {EvodeskAppMessageBusService} from '../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';

interface State {
    ready: boolean;
    props?: LandingPartnersComponentProps;
}

@Component({
    selector: 'evodesk-landing-partners-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-landing-partners [props]="state.props"></evodesk-landing-partners>
      </ng-container>
    `,
})
export class LandingPartnersContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        props: {
            partners: [
                {
                    img: `/assets/landing/partners/p-1.jpg`,
                    url: 'http://sbs.edu.ru',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.Synergy',
                },
                {
                    img: `/assets/landing/partners/p-2.jpg`,
                    url: 'http://www.mbastrategy.ru/',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.MBA',
                },
                {
                    img: `/assets/landing/partners/p-3.jpg`,
                    url: 'https://bitjournal.media',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.BitJournal',
                },
                {
                    img: `/assets/landing/partners/p-4.png`,
                    url: 'https://websarafan.ru',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.WebSarafan',
                },
                {
                    img: `/assets/landing/partners/p-5.png`,
                    url: 'https://vk.com/cryptovestnik',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.Cryptovestnik',
                },
                {
                    img: `/assets/landing/partners/p-6.png`,
                    url: 'https://криптовалюты.рф/',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.CryptocurrenciesRU',
                },
                {
                    img: `/assets/landing/partners/p-7.jpg`,
                    url: 'https://t.me/best_cryptonews',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.BestCryptonews',
                },
                {
                    img: `/assets/landing/partners/p-8.png`,
                    url: 'https://dailybit.io',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.DailybitIO',
                },
                {
                    img: `/assets/landing/partners/p-9.jpg`,
                    url: 'https://t.me/Crypto_grapher',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.CryptoGrapher',
                },
                {
                    img: `/assets/landing/partners/p-10.jpg`,
                    url: 'https://rucrypto.com/',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.RyCrypto',
                },
                {
                    img: `/assets/landing/partners/p-11.jpg`,
                    url: 'https://bitminecorp.ru/',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.BitmineCorp',
                },
                {
                    img: `/assets/landing/partners/p-12.jpg`,
                    url: 'https://bitcoinnews.ru/',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.BitcoinNews',
                },
                {
                    img: `/assets/landing/partners/p-13.jpg`,
                    url: 'http://plusoneradio.com',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.Plusoneradio',
                },
                {
                    img: `/assets/landing/partners/p-14.png`,
                    url: 'http://cryptonomica.info',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.Cryptonomica',
                },
                {
                    img: `/assets/landing/partners/p-15.png`,
                    url: 'http://coinrace.ru',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.Coinrace',
                },
                {
                    img: `/assets/landing/partners/p-16.png`,
                    url: 'https://btcnovosti.com',
                    htmlText: 'EvodeskLanding.components.LandingPartners.Partners.BtcNovosti',
                },
            ],
        },
    };

    constructor(
        private appBus: EvodeskAppMessageBusService,
        private evodeskLandingConditionsModalService: EvodeskConditionsModalService,
    ) {}

    ngOnInit(): void {
        this.appBus.stream$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((e) => {
            switch (e.type) {
                case AppEvent.HeaderMobileClickItem: {
                    const menuItem: EvodeskHeaderMenuItem = e.payload;

                    if (menuItem && menuItem.id === 'conditions') {
                        this.evodeskLandingConditionsModalService.showConditions();
                    }

                    break;
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
