import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

export interface LandingPartnersComponentPropsPartner {
    img: string;
    url: string;
    htmlText: string;
}

export interface LandingPartnersComponentProps {
    partners: Array<LandingPartnersComponentPropsPartner>;
}

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-landing-partners',
    templateUrl: './LandingPartners.component.pug',
    styleUrls: [
        './LandingPartners.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingPartnersComponent implements OnChanges
{
    @Input() props: LandingPartnersComponentProps;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get images(): Array<string> {
        return this.props ? this.props.partners.map(p => p.img) : [];
    }
}
