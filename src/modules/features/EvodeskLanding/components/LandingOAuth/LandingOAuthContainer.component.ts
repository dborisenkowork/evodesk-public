import {ChangeDetectionStrategy, Component, EventEmitter, Output} from '@angular/core';

import {LandingOAuthComponentProps as Props} from './LandingOAuth.component';

import {SocialNetworks} from '../../../../app/EvodeskApp/services/EvodeskShareWithSocialNetworks.service';
import {OAuthServices, UserRESTService} from '../../../../app/EvodeskRESTApi/services/UserREST.service';

interface State {
    ready: boolean;
    props: Props;
}

const mapNetworks: Array<{ network: SocialNetworks, oauth: OAuthServices }> = [
    { network: SocialNetworks.Facebook, oauth: 'fb' },
    { network: SocialNetworks.GooglePlus, oauth: 'gl' },
    { network: SocialNetworks.Vk, oauth: 'vk' },
    { network: SocialNetworks.LinkedIn, oauth: 'ln' },
];

@Component({
    selector: 'evodesk-landing-oauth-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-landing-oauth [props]="state.props" (close)="close()" (use)="use($event)"></evodesk-landing-oauth>
      </ng-container>
    `,
})
export class LandingOAuthContainerComponent
{
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: true,
        props: {},
    };

    constructor(
        private userREST: UserRESTService,
    ) {}

    close(): void {
        this.closeEvent.emit(undefined);
    }

    use(socialNetwork: SocialNetworks): void {
        this.userREST.goOAuth(mapNetworks.filter(e => e.network === socialNetwork)[0].oauth);
    }
}
