import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {SocialNetworks} from '../../../../app/EvodeskApp/services/EvodeskShareWithSocialNetworks.service';

interface Props {
}

interface State {
    ready: boolean;
    blocked: boolean;
}

export {Props as LandingOAuthComponentProps};

@Component({
    selector: 'evodesk-landing-oauth',
    templateUrl: './LandingOAuth.component.pug',
    styleUrls: [
        './LandingOAuth.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingOAuthComponent implements OnChanges
{
    @Input() props: Props;

    @Output('use') useEvent: EventEmitter<SocialNetworks> = new EventEmitter<SocialNetworks>();
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        blocked: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    use(network: string): void {
        this.state = {
            ...this.state,
            blocked: true,
        };

        this.useEvent.emit(<any>network);
    }

    close(): void {
        if (! this.state.blocked) {
            this.closeEvent.emit(undefined);
        }
    }
}
