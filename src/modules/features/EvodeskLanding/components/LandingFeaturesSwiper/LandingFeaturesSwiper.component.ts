import {ChangeDetectionStrategy, Component, Input, ViewChild} from '@angular/core';
import {SwiperConfigInterface, SwiperDirective} from 'ngx-swiper-wrapper';

export interface LandingFeaturesSwiperComponentPropsSlide {
    title: string;
    htmlText: string;
    img: string;
}

interface State {
    index: number;
    swiperConfig: SwiperConfigInterface;
}

@Component({
    selector: 'evodesk-landing-features-swiper',
    templateUrl: './LandingFeaturesSwiper.component.pug',
    styleUrls: [
        './LandingFeaturesSwiper.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingFeaturesSwiperComponent
{
    public state: State = {
        index: 0,
        swiperConfig: {},
    };

    @Input() slides: Array<LandingFeaturesSwiperComponentPropsSlide> = [];

    @ViewChild(SwiperDirective) swiper: SwiperDirective;

    prev(): void {
        this.swiper.prevSlide();
    }

    next(): void {
        this.swiper.nextSlide();
    }
}
