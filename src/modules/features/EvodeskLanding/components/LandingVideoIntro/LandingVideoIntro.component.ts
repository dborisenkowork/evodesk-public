import {Component} from '@angular/core';

@Component({
    selector: 'evodesk-landing-video-intro',
    templateUrl: './LandingVideoIntro.component.pug',
    styleUrls: [
        './LandingVideoIntro.component.scss',
    ],
})
export class LandingVideoIntroComponent
{}
