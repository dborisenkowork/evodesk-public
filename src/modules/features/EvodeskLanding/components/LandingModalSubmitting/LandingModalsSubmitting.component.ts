import {Component, Inject} from '@angular/core';

import {MAT_DIALOG_DATA} from '@angular/material';

export interface LandingModalsSubmittingComponentProps {
    text: string;
}

@Component({
    templateUrl: './LandingModalsSubmitting.component.pug',
    styleUrls: [
        './LandingModalsSubmitting.component.scss',
    ],
})
export class LandingModalsSubmittingComponent
{
    constructor(
        @Inject(MAT_DIALOG_DATA) public props: LandingModalsSubmittingComponentProps,
    ) {}
}
