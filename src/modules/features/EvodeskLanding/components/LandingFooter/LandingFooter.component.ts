import {ChangeDetectionStrategy, Component} from '@angular/core';

import {environment} from '../../../../../environments/environment.config';

import {EvodeskConditionsModalService} from '../../services/EvodeskConditionsModal.service';

interface State {
    mobile: {
        [platform: string]: string;
    };
    social: {
        [network: string]: string;
    };
}

@Component({
    selector: 'evodesk-landing-footer',
    templateUrl: './LandingFooter.component.pug',
    styleUrls: [
        './LandingFooter.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingFooterComponent
{
    public state: State = {
        mobile: {
            ...environment.mobileAppLinks,
        },
        social: {
            ...environment.socialNetworkLinks,
        },
    };

    constructor(
        private conditionsModalService: EvodeskConditionsModalService,
    ) {}

    showConditions(): void {
        this.conditionsModalService.showConditions();
    }
}
