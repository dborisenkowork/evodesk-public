import {ChangeDetectionStrategy, Component} from '@angular/core';

import {AuthTokenService} from '../../../EvodeskAuth/services/AuthToken.service';

@Component({
    selector: 'evodesk-landing-auth-panel-mobile',
    templateUrl: './LandingAuthPanelMobile.component.pug',
    styleUrls: [
        './LandingAuthPanelMobile.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingAuthPanelMobileComponent
{
    constructor(
        private authToken: AuthTokenService,
    ) {}

    get hasAuthToken(): boolean {
        return this.authToken.has();
    }
}
