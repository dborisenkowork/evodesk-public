import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

export interface LandingInvestorFormComponentProps {
    form: FormGroup;
}

export function landingInvestorFormComponentPropsFormBuilder(fb: FormBuilder): FormGroup {
    return fb.group({
        name: ['', [Validators.required]],
        phone: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        specialAgree: [false],
        personalInfoAgree: [false],
    });
}

export interface LandingInvestorFormComponentFormValue {
    name: string;
    phone: string;
    email: string;
    specialAgree: boolean;
    personalInfoAgree: boolean;
}

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-landing-investor-form',
    templateUrl: './LandingInvestorForm.component.pug',
    styleUrls: [
        './LandingInvestorForm.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingInvestorFormComponent implements OnChanges
{
    @Input() props: LandingInvestorFormComponentProps;

    @Output('ngSubmit') submitEvent: EventEmitter<LandingInvestorFormComponentFormValue> = new EventEmitter<LandingInvestorFormComponentFormValue>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get isAgreeWithSpecial(): boolean {
        return !! this.props.form.get('specialAgree').value;
    }

    get isAgreeWithPersonalInfoConditionsAccepted(): boolean {
        return !! this.props.form.get('personalInfoAgree').value;
    }

    toggleAgreeWithSpecial(): void {
        this.props.form.patchValue({
            specialAgree: ! this.props.form.get('specialAgree').value,
        });
    }

    toggleAgreeWithPersonalInfoConditionsAccepted(): void {
        this.props.form.patchValue({
            personalInfoAgree: ! this.props.form.get('personalInfoAgree').value,
        });
    }

    submit(): void {
        if (this.props.form.valid) {
            this.submitEvent.emit(this.props.form.value);
        }
    }
}
