import {Component, OnDestroy} from '@angular/core';
import {FormBuilder} from '@angular/forms';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {LandingInvestorFormComponentFormValue, LandingInvestorFormComponentProps, landingInvestorFormComponentPropsFormBuilder} from './LandingInvestorForm.component';

import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {LandingRESTService} from '../../../../app/EvodeskRESTApi/services/LandingREST.service';

interface State {
    ready: boolean;
    props?: LandingInvestorFormComponentProps;
}

@Component({
    selector: 'evodesk-landing-investor-form-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-landing-investor-form [props]="state.props" (ngSubmit)="onSubmit($event)"></evodesk-landing-investor-form>
      </ng-container>
    `,
})
export class LandingInvestorFormContainerComponent implements OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        props: {
            form: landingInvestorFormComponentPropsFormBuilder(this.fb),
        },
    };

    constructor(
        private fb: FormBuilder,
        private landingRESTService: LandingRESTService,
        private uiAlertService: EvodeskAlertModalService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onSubmit(formValue: LandingInvestorFormComponentFormValue): void {
        if (formValue.personalInfoAgree) {
            this.state.props.form.disable();

            const success: Function = () => {
                this.uiAlertService.open({
                    title: {
                        text: 'EvodeskLanding.components.LandingInvestorFormContainer.Success.Title',
                        translate: true,
                    },
                    text: {
                        text: 'EvodeskLanding.components.LandingInvestorFormContainer.Success.Text',
                        translate: true,
                    },
                    ok: {
                        text: 'EvodeskLanding.components.LandingInvestorFormContainer.Success.Close',
                        translate: true,
                    },
                }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                    this.state.props.form.reset();
                    this.state.props.form.enable();
                });
            };

            const fail: Function = () => {
                this.uiAlertService.open({
                    title: {
                        text: 'EvodeskLanding.components.LandingInvestorFormContainer.Fail.Title',
                        translate: true,
                    },
                    text: {
                        text: 'EvodeskLanding.components.LandingInvestorFormContainer.Fail.Text',
                        translate: true,
                    },
                    ok: {
                        text: 'EvodeskLanding.components.LandingInvestorFormContainer.Fail.Close',
                        translate: true,
                    },
                }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                    this.state.props.form.enable();
                });
            };

            this.landingRESTService.registerConference({
                name: formValue.name,
                email: formValue.email,
                phone: formValue.phone,
                vklad: formValue.specialAgree,
            }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                () => {
                    success();
                },
                () => {
                    fail();
                },
            );
        } else {
            this.uiAlertService.open({
                title: {
                    text: 'EvodeskLanding.components.LandingInvestorFormContainer.NpPersonalInfoAgree.Title',
                    translate: true,
                },
                text: {
                    text: 'EvodeskLanding.components.LandingInvestorFormContainer.NpPersonalInfoAgree.Text',
                    translate: true,
                },
                ok: {
                    text: 'EvodeskLanding.components.LandingInvestorFormContainer.NpPersonalInfoAgree.Close',
                    translate: true,
                },
            }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
        }
    }
}
