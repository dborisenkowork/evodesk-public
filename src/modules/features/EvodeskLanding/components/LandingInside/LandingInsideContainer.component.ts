import {Component, OnDestroy} from '@angular/core';
import {FormBuilder} from '@angular/forms';

import {TranslateService} from '@ngx-translate/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {landingInsideComponentFormBuilder, LandingInsideComponentFormValue, LandingInsideComponentProps} from './LandingInside.component';

import {LandingRESTService} from '../../../../app/EvodeskRESTApi/services/LandingREST.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';

interface State {
    ready: boolean;
    props?: LandingInsideComponentProps;
}

@Component({
    selector: 'evodesk-landing-inside-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-landing-inside [props]="state.props" (ngSubmit)="ngSubmit($event)"></evodesk-landing-inside>
        </ng-container>
    `,
})
export class LandingInsideContainerComponent implements OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        props: {
            form: landingInsideComponentFormBuilder(this.fb),
        },
    };

    constructor(
        private fb: FormBuilder,
        private translate: TranslateService,
        private uiAlertService: EvodeskAlertModalService,
        private landingRESTService: LandingRESTService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    ngSubmit(formValue: LandingInsideComponentFormValue): void {
        this.state.props.form.disable();

        this.translate.get(formValue.money).pipe(takeUntil(this.ngOnDestroy$)).subscribe((money) => {
            this.landingRESTService.registerInside({ ...formValue, money: money }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                () => {
                    this.uiAlertService.openTranslated('EvodeskLanding.components.LandingInside.Success').pipe(
                        takeUntil(this.ngOnDestroy$))
                        .subscribe();

                    this.state.props.form.reset();
                    this.state.props.form.enable();

                    this.state.props.form.patchValue({
                        money: 'EvodeskLanding.components.LandingInside.Form.SumOptions.upTo500k',
                    });
                },
                () => {
                    this.uiAlertService.openTranslated('EvodeskLanding.components.LandingInside.Success').pipe(
                        takeUntil(this.ngOnDestroy$))
                        .subscribe();

                    this.state.props.form.enable();
                },
            );
        });
    }
}
