import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

export interface LandingInsideComponentProps {
    form: FormGroup;
}

interface State {
    ready: boolean;
}

export interface LandingInsideComponentFormValue {
    money: string;
    name: string;
    phone: string;
    email: string;
}

export function landingInsideComponentFormBuilder(fb: FormBuilder): FormGroup {
    return fb.group({
        money: ['EvodeskLanding.components.LandingInside.Form.SumOptions.upTo500k', [Validators.required]],
        name: ['', [Validators.required, Validators.maxLength(255)]],
        phone: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(255)]],
        email: ['', [Validators.required, Validators.email, Validators.maxLength(255)]],
    });
}

@Component({
    selector: 'evodesk-landing-inside',
    templateUrl: './LandingInside.component.pug',
    styleUrls: [
        './LandingInside.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingInsideComponent implements OnChanges
{
    @Input() props: LandingInsideComponentProps;

    @Output('ngSubmit') ngSubmitEvent: EventEmitter<LandingInsideComponentFormValue> = new EventEmitter<LandingInsideComponentFormValue>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            }
        }
    }

    get sumOptions(): Array<string> {
        return [
            'EvodeskLanding.components.LandingInside.Form.SumOptions.upTo500k',
            'EvodeskLanding.components.LandingInside.Form.SumOptions.from500kTo1M',
            'EvodeskLanding.components.LandingInside.Form.SumOptions.from1MUpTo3M',
            'EvodeskLanding.components.LandingInside.Form.SumOptions.from3M',
        ];
    }

    ngSubmit(): void {
        if (this.props.form.valid) {
            this.ngSubmitEvent.emit(this.props.form.value);
        }
    }
}
