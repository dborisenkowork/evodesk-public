import {Routes} from '@angular/router';

import {IndexRouteComponent} from '../routes/IndexRoute/IndexRoute.component';
import {DevelopersRouteComponent} from '../routes/DevelopersRoute/DevelopersRoute.component';
import {AboutRouteComponent} from '../routes/AboutRoute/AboutRoute.component';
import {FeaturesRouteComponent} from '../routes/FeaturesRoute/FeaturesRoute.component';
import {EvocoinRouteComponent} from '../routes/EvocoinRoute/EvocoinRoute.component';
import {PartnersRouteComponent} from '../routes/PartnersRoute/PartnersRoute.component';
import {InsideRouteComponent} from '../routes/InsideRoute/InsideRoute.component';

export const EvodeskLandingRouting: Routes = [
    {
        path: '',
        component: IndexRouteComponent,
    },
    {
        path: 'about',
        component: AboutRouteComponent,
    },
    {
        path: 'features',
        component: FeaturesRouteComponent,
    },
    {
        path: 'evocoin',
        component: EvocoinRouteComponent,
    },
    {
        path: 'developers',
        component: DevelopersRouteComponent,
    },
    {
        path: 'partners',
        component: PartnersRouteComponent,
    },
    {
        path: 'coin',
        component: InsideRouteComponent,
    },
];

