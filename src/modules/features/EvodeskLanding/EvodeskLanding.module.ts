import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';
import {EvodeskLandingRoutingModule} from './EvodeskLandingRouting.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';
import {InsideRouteComponent} from './routes/InsideRoute/InsideRoute.component';

import {LandingIndexComponent} from './components/LandingIndex/LandingIndex.component';
import {LandingIndexContainerComponent} from './components/LandingIndex/LandingIndexContainer.component';
import {AboutRouteComponent} from './routes/AboutRoute/AboutRoute.component';
import {DevelopersRouteComponent} from './routes/DevelopersRoute/DevelopersRoute.component';
import {EvocoinRouteComponent} from './routes/EvocoinRoute/EvocoinRoute.component';
import {FeaturesRouteComponent} from './routes/FeaturesRoute/FeaturesRoute.component';
import {PartnersRouteComponent} from './routes/PartnersRoute/PartnersRoute.component';
import {LandingFooterComponent} from './components/LandingFooter/LandingFooter.component';
import {LandingAuthComponent} from './components/LandingAuth/LandingAuth.component';
import {LandingAuthContainerComponent} from './components/LandingAuth/LandingAuthContainer.component';
import {LandingVideoIntroComponent} from './components/LandingVideoIntro/LandingVideoIntro.component';
import {LandingPartnersComponent} from './components/LandingPartners/LandingPartners.component';
import {LandingPartnersContainerComponent} from './components/LandingPartners/LandingPartnersContainer.component';
import {LandingEvocoinComponent} from './components/LandingEvocoin/LandingEvocoin.component';
import {LandingEvocoinContainerComponent} from './components/LandingEvocoin/LandingEvocoinContainer.component';
import {LandingFeaturesComponent} from './components/LandingFeatures/LandingFeatures.component';
import {LandingFeaturesContainerComponent} from './components/LandingFeatures/LandingFeaturesContainer.component';
import {LandingDevelopersComponent} from './components/LandingDevelopers/LandingDevelopers.component';
import {LandingDevelopersContainerComponent} from './components/LandingDevelopers/LandingDevelopersContainer.component';
import {LandingAuthPanelComponent} from './components/LandingAuthPanel/LandingAuthPanel.component';
import {LandingFeaturesSwiperComponent} from './components/LandingFeaturesSwiper/LandingFeaturesSwiper.component';
import {LandingAuthPanelMobileComponent} from './components/LandingAuthPanelMobile/LandingAuthPanelMobile.component';
import {LandingInvestorFormComponent} from './components/LandingInvestorForm/LandingInvestorForm.component';
import {LandingInvestorFormContainerComponent} from './components/LandingInvestorForm/LandingInvestorFormContainer.component';
import {LandingInsideComponent} from './components/LandingInside/LandingInside.component';
import {LandingInsideContainerComponent} from './components/LandingInside/LandingInsideContainer.component';
import {LandingModalsSubmittingComponent} from './components/LandingModalSubmitting/LandingModalsSubmitting.component';
import {LandingOAuthContainerComponent} from './components/LandingOAuth/LandingOAuthContainer.component';
import {LandingOAuthComponent} from './components/LandingOAuth/LandingOAuth.component';

import {EvodeskConditionsModalService} from './services/EvodeskConditionsModal.service';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskLandingRoutingModule,
    ],
    declarations: [
        IndexRouteComponent,
        AboutRouteComponent,
        DevelopersRouteComponent,
        EvocoinRouteComponent,
        FeaturesRouteComponent,
        PartnersRouteComponent,
        InsideRouteComponent,

        LandingFooterComponent,
        LandingAuthComponent,
        LandingAuthContainerComponent,
        LandingIndexComponent,
        LandingIndexContainerComponent,
        LandingVideoIntroComponent,
        LandingPartnersComponent,
        LandingPartnersContainerComponent,
        LandingEvocoinComponent,
        LandingEvocoinContainerComponent,
        LandingFeaturesComponent,
        LandingFeaturesContainerComponent,
        LandingDevelopersComponent,
        LandingDevelopersContainerComponent,
        LandingAuthPanelComponent,
        LandingFeaturesSwiperComponent,
        LandingAuthPanelMobileComponent,
        LandingInvestorFormComponent,
        LandingInvestorFormContainerComponent,
        LandingInsideComponent,
        LandingInsideContainerComponent,
        LandingModalsSubmittingComponent,
        LandingOAuthComponent,
        LandingOAuthContainerComponent,
    ],
    entryComponents: [
        LandingModalsSubmittingComponent,
        LandingOAuthContainerComponent,
    ],
    providers: [
        EvodeskConditionsModalService,
    ],
})
export class EvodeskLandingModule
{}
