import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskLandingRouting} from './configs/EvodeskLandingRouting.config';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskLandingRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskLandingRoutingModule
{}
