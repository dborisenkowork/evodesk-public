import {Component, OnInit} from '@angular/core';

import {EvodeskHeaderLayoutVariant} from '../../../EvodeskHeader/models/EvodeskHeader.models';

import {EvodeskHeaderConfigurationService} from '../../../EvodeskHeader/services/EvodeskHeaderConfiguration.service';

@Component({
    templateUrl: './FeaturesRoute.component.pug',
    styleUrls: [
        './FeaturesRoute.component.scss',
    ],
})
export class FeaturesRouteComponent implements OnInit
{
    constructor(
        private evodeskHeaderConfigurationService: EvodeskHeaderConfigurationService,
    ) {}

    ngOnInit(): void {
        this.evodeskHeaderConfigurationService.setVariant(EvodeskHeaderLayoutVariant.Landing);
    }
}
