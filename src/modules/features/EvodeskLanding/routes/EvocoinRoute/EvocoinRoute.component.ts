import {Component, OnInit} from '@angular/core';

import {EvodeskHeaderLayoutVariant} from '../../../EvodeskHeader/models/EvodeskHeader.models';

import {EvodeskHeaderConfigurationService} from '../../../EvodeskHeader/services/EvodeskHeaderConfiguration.service';

@Component({
    templateUrl: './EvocoinRoute.component.pug',
    styleUrls: [
        './EvocoinRoute.component.scss',
    ],
})
export class EvocoinRouteComponent implements OnInit
{
    constructor(
        private evodeskHeaderConfigurationService: EvodeskHeaderConfigurationService,
    ) {}

    ngOnInit(): void {
        this.evodeskHeaderConfigurationService.setVariant(EvodeskHeaderLayoutVariant.Landing);
    }
}
