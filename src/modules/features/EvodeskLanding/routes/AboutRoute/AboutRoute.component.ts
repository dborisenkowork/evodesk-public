import {Component, OnInit} from '@angular/core';

import {EvodeskHeaderLayoutVariant} from '../../../EvodeskHeader/models/EvodeskHeader.models';

import {EvodeskHeaderConfigurationService} from '../../../EvodeskHeader/services/EvodeskHeaderConfiguration.service';

@Component({
    templateUrl: './AboutRoute.component.pug',
    styleUrls: [
        './AboutRoute.component.scss',
    ],
})
export class AboutRouteComponent implements OnInit
{
    constructor(
        private evodeskHeaderConfigurationService: EvodeskHeaderConfigurationService,
    ) {}

    ngOnInit(): void {
        this.evodeskHeaderConfigurationService.setVariant(EvodeskHeaderLayoutVariant.Landing);
    }
}
