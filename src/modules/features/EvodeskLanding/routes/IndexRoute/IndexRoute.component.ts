import {Component, OnInit} from '@angular/core';

import {EvodeskHeaderLayoutVariant} from '../../../EvodeskHeader/models/EvodeskHeader.models';

import {EvodeskHeaderConfigurationService} from '../../../EvodeskHeader/services/EvodeskHeaderConfiguration.service';

@Component({
    templateUrl: './IndexRoute.component.pug',
    styleUrls: [
        './IndexRoute.component.scss',
    ],
})
export class IndexRouteComponent implements OnInit
{
    constructor(
        private evodeskHeaderConfigurationService: EvodeskHeaderConfigurationService,
    ) {}

    ngOnInit(): void {
        this.evodeskHeaderConfigurationService.setVariant(EvodeskHeaderLayoutVariant.LandingNoBg);
    }
}
