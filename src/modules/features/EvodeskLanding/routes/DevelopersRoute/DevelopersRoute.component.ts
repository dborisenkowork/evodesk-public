import {Component, OnInit} from '@angular/core';

import {EvodeskHeaderLayoutVariant} from '../../../EvodeskHeader/models/EvodeskHeader.models';

import {EvodeskHeaderConfigurationService} from '../../../EvodeskHeader/services/EvodeskHeaderConfiguration.service';

@Component({
    templateUrl: './DevelopersRoute.component.pug',
    styleUrls: [
        './DevelopersRoute.component.scss',
    ],
})
export class DevelopersRouteComponent implements OnInit
{
    constructor(
        private evodeskHeaderConfigurationService: EvodeskHeaderConfigurationService,
    ) {}

    ngOnInit(): void {
        this.evodeskHeaderConfigurationService.setVariant(EvodeskHeaderLayoutVariant.Landing);
    }
}
