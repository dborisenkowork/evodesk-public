import {Injectable} from '@angular/core';

import {EvodeskAlertModalService} from '../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';

@Injectable()
export class EvodeskConditionsModalService
{
    constructor(
        private uiAlertService: EvodeskAlertModalService,
    ) {}

    showConditions(): void {
        this.uiAlertService.open({
            title: {
                text: 'EvodeskLanding.__shared.conditions.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskLanding.__shared.conditions.TextInnerHTML',
                translate: true,
                asHtml: true,
            },
            ok: {
                text: 'EvodeskLanding.__shared.conditions.OK',
                translate: true,
            },
        }).subscribe();
    }
}
