import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskNotificationComponent} from './components/EvodeskNotification/EvodeskNotification.component';
import {EvodeskNotificationContainerComponent} from './components/EvodeskNotification/EvodeskNotificationContainer.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,
    ],
    declarations: [
        EvodeskNotificationComponent,
        EvodeskNotificationContainerComponent,
    ],
    exports: [
        EvodeskNotificationContainerComponent,
    ],
})
export class EvodeskNotificationsModule
{}
