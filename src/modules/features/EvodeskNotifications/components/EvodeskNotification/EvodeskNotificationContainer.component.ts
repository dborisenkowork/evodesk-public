import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';
import {Router} from '@angular/router';

import {Subject} from 'rxjs';

import {EvodeskNotificationComponentProps} from './EvodeskNotification.component';

import {AbstractNotificationCardEntity, EvodeskNotification, NotificationTypeKind} from '../../../../app/EvodeskRESTApi/models/Notification.model';
import {PostId} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

import {EvodeskFeedBodyParserService} from '../../../EvodeskFeed/services/EvodeskFeedBodyParser.service';
import {EvodeskIMBodyParserService} from '../../../EvodeskIM/services/EvodeskIMBodyParser.service';

type Props = EvodeskNotificationComponentProps;

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-notification-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <ng-container *ngIf="state.ready">
          <evodesk-notification [props]="state.props" (open)="onOpen()" (openPost)="onOpenPost($event)"></evodesk-notification>
        </ng-container>
    `,
    styles: [
        `:host { width: 100%; }`,
    ],
    providers: [
        EvodeskFeedBodyParserService,
        EvodeskIMBodyParserService,
    ],
})
export class EvodeskNotificationContainerComponent implements OnChanges, OnDestroy
{
    @Input() notification: EvodeskNotification;

    @Output('open') openEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openPost') openPostEvent: EventEmitter<PostId> = new EventEmitter<PostId>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private router: Router,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.notification) {
            this.state = {
                ready: true,
                props: {
                    notification: this.notification,
                },
            };

            this.cdr.markForCheck();
        } else {
            this.state = {
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onOpen(): void {
        const openDesk: Function = (entity: AbstractNotificationCardEntity) => {
            this.router.navigate([`/desk/${entity.deskId}`]);
        };

        switch (this.notification.kind) {
            case NotificationTypeKind.AchievementEarned:
                this.router.navigate(['/rating']);

                break;

            case NotificationTypeKind.CardDeleted:
            case NotificationTypeKind.CardParticipantDeleted:
            case NotificationTypeKind.CardTitleChanged:
            case NotificationTypeKind.CardDescriptionChanged:
            case NotificationTypeKind.CardLabelAdded:
            case NotificationTypeKind.CardLabelDeleted:
            case NotificationTypeKind.CardCommentAdded:
            case NotificationTypeKind.CardChecklistAdded:
            case NotificationTypeKind.CardMovedToAnotherColumn:
            case NotificationTypeKind.CardDeadlineUpdated:
            case NotificationTypeKind.CardDeadlineAdded:
            case NotificationTypeKind.CardDeadlineDeleted:
                openDesk(this.notification.model.entity);

                break;

            case NotificationTypeKind.IMReceivedMessage:
                this.router.navigate(['/im'], {
                    queryParams: <any>{
                        dialog: this.notification.model.entity.message.dialogId.toString(),
                    },
                });

                break;

            case NotificationTypeKind.EvoCoinReceived:
                this.router.navigate(['/exchange']);

                break;

            case NotificationTypeKind.EvoPointsReceived:
                this.router.navigate(['/exchange']);

                break;

            case NotificationTypeKind.Post:
                this.openPostEvent.emit(this.notification.model.entity.post.id);

                break;

            case NotificationTypeKind.PostLiked:
                this.openPostEvent.emit(this.notification.model.entity.post.id);

                break;

            case NotificationTypeKind.PostReposted:
                this.openPostEvent.emit(this.notification.model.entity.repost.id);

                break;

            case NotificationTypeKind.PostCommented:
                this.openPostEvent.emit(this.notification.model.entity.post.id);

                break;

            case NotificationTypeKind.PostCommentLiked:
                this.openPostEvent.emit(this.notification.model.entity.comment.post_id.id);

                break;
        }

        this.openEvent.emit(undefined);
    }

    onOpenPost(postId: PostId): void {
        this.openPostEvent.emit(postId);
    }
}
