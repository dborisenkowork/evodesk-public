import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {Observable} from 'rxjs';

import * as moment from 'moment';
import {Moment} from 'moment';

import {environment} from '../../../../../environments/environment.config';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../types/Backend.types';

import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';
import {EvodeskFeedBodyParserService} from '../../../EvodeskFeed/services/EvodeskFeedBodyParser.service';
import {EvodeskIMBodyParserService} from '../../../EvodeskIM/services/EvodeskIMBodyParser.service';

import {AttachmentType, PostId} from '../../../../app/EvodeskRESTApi/models/post/Post.model';
import {AbstractNotificationCardEntity, EvodeskNotification, NotificationAchievementEarnedEntity, NotificationCardCommentAddedEntity, NotificationCardCommentDeletedEntity, NotificationCardLabelAddedEntity, NotificationCardLabelDeletedEntity, NotificationIMReceivedMessageEarnedEntity, NotificationModel, NotificationPostCommentedEntity, NotificationPostCommentLikedEntity, NotificationPostEntity, NotificationPostLikedEntity, NotificationPostRepostedEntity} from '../../../../app/EvodeskRESTApi/models/Notification.model';
import {deskColumnTypeToTranslation, getDeskColumnTypeById} from '../../../../app/EvodeskRESTApi/models/DeskColumnDefinition.model';

interface Props {
    notification: EvodeskNotification;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskNotificationComponentProps};

@Component({
    selector: 'evodesk-notification',
    templateUrl: './EvodeskNotification.component.pug',
    styleUrls: [
        './EvodeskNotification.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskNotificationComponent implements OnChanges
{
    @Input() props: Props;

    @Output('open') openEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openPost') openPostEvent: EventEmitter<PostId> = new EventEmitter<PostId>();

    public state: State = {
        ready: false,
    };

    constructor(
        private dates: EvodeskDateService,
        private feedBodyParser: EvodeskFeedBodyParserService,
        private imBodyParser: EvodeskIMBodyParserService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get n(): EvodeskNotification {
        return this.props.notification;
    }

    get m(): NotificationModel<any> {
        return this.props.notification.model;
    }

    get type(): string {
        return <any>this.props.notification.kind;
    }


    get date(): Observable<string> {
        return this.dates.diffFromNowV4(new Date(this.props.notification.model.createdAt * 1000));
    }

    get cssClasses(): any {
        return {
            'is-viewed': this.props.notification.model.viewedAt !== null,
        };
    }

    open(): void {
        this.openEvent.emit(undefined);
    }

    openPost(postId: PostId): void {
        this.openPostEvent.emit(postId);
    }

    get achievementImageUrl(): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/${(<NotificationModel<NotificationAchievementEarnedEntity>>this.m).entity.achievement.image}`;
    }

    get deskEntityNgClass(): any {
        return {};
    }

    get deskEntityNgStyle(): any {
        const hasColor: boolean = (<AbstractNotificationCardEntity>this.m.entity).deskType === 'multidesk' && !! (<AbstractNotificationCardEntity>this.m.entity).deskColor;

        return {
            'background-color': hasColor ? (<AbstractNotificationCardEntity>this.m.entity).deskColor : '#1d2e48',
        };
    }

    get deskCardLabelNgStyle(): any {
        const entity: NotificationCardLabelAddedEntity | NotificationCardLabelDeletedEntity = this.m.entity;

        return {
            'background-color': entity.labelColor,
        };
    }

    get commentShort(): Observable<string> {
        const entity: NotificationCardCommentAddedEntity | NotificationCardCommentDeletedEntity = this.m.entity;

        return this.feedBodyParser.parseCommentTextToHtmlPreview(this.feedBodyParser.parseCommentToJSOBNNBody(entity.commentText).text, {
            maxLength: 32,
        });
    }

    deskColumnName(columnId, i): string {
        return deskColumnTypeToTranslation(getDeskColumnTypeById(columnId));
    }

    deadline(input: string): string {
        const date: Moment = moment(input, BACKEND_DATE_FORMAT_AS_MOMENT);

        if (date.hours() === 0 && date.minutes() === 0) {
            return date.format('LL');
        } else {
            return date.format('LLL');
        }
    }

    get imMessageAuthor(): string {
        const entity: NotificationIMReceivedMessageEarnedEntity = this.m.entity;

        return entity.author.name;
    }

    get imMessage(): Observable<string> {
        const entity: NotificationIMReceivedMessageEarnedEntity = this.m.entity;

        return this.imBodyParser.parseToHtml(entity.message.text, {
            maxSize: 32,
        });
    }

    get postText(): Observable<string> {
        const entity: NotificationPostEntity | NotificationPostRepostedEntity | NotificationPostLikedEntity | NotificationPostCommentedEntity = this.m.entity;

        return this.feedBodyParser.parseToHtmlPreview(entity.post.text, {
            maxLength: 32,
        });
    }

    get hasPostText(): boolean {
        const entity: NotificationPostEntity | NotificationPostRepostedEntity | NotificationPostLikedEntity | NotificationPostCommentedEntity = this.m.entity;

        return !! entity.post.text;
    }

    get isPostText(): boolean {
        const entity: NotificationPostEntity | NotificationPostRepostedEntity | NotificationPostLikedEntity | NotificationPostCommentedEntity = this.m.entity;

        return entity.post.attachments.length === 0;
    }

    get isPostPhoto(): boolean {
        const entity: NotificationPostEntity | NotificationPostRepostedEntity | NotificationPostLikedEntity | NotificationPostCommentedEntity = this.m.entity;

        return entity.post.attachments.length > 0 && entity.post.attachments[0].type === AttachmentType.Photo;
    }

    get isPostVideo(): boolean {
        const entity: NotificationPostEntity | NotificationPostRepostedEntity | NotificationPostLikedEntity | NotificationPostCommentedEntity = this.m.entity;

        return entity.post.attachments.length > 0 && entity.post.attachments[0].type === AttachmentType.Video;
    }

    get isPostLink(): boolean {
        const entity: NotificationPostEntity | NotificationPostRepostedEntity | NotificationPostLikedEntity | NotificationPostCommentedEntity = this.m.entity;

        return entity.post.attachments.length > 0 && entity.post.attachments[0].type === AttachmentType.Link;
    }

    get postCommentText(): Observable<string> {
        const entity: NotificationPostCommentLikedEntity = this.m.entity;

        return this.feedBodyParser.parseCommentTextToHtmlPreview(this.feedBodyParser.parseCommentToJSOBNNBody(entity.comment.text).text, {
            maxLength: 32,
        });
    }
}
