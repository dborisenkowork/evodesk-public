import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskPeopleRouting} from './configs/EvodeskPeopleRouting.module';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskPeopleRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskPeopleRoutingModule
{}
