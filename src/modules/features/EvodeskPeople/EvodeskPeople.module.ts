import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskPeopleRoutingModule} from './EvodeskPeopleRouting.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskPeopleRoutingModule,
    ],
    declarations: [
        IndexRouteComponent,
    ],
})
export class EvodeskPeopleModule
{}
