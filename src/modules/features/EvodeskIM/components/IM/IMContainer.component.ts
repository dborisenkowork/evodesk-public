import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {IMLayoutVariant} from '../../models/IM.models';

import {defaultMatDialogConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {IMComponentProps} from './IM.component';
import {IMStartDialogContainerComponent} from '../../shared/components/IMStartDialog/IMStartDialogContainer.component';

import {EvodeskIMStateService, IMAction} from '../../state/EvodeskIMState.service';

type Props = IMComponentProps;

interface State {
    ready: boolean;
    layout: IMLayoutVariant;
    props?: Props;
    dialogRef?: MatDialogRef<any>;
}

@Component({
    selector: 'evodesk-im-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-im [props]="state.props" [layout]="state.layout" (startDialog)="startDialog()"></evodesk-im>
        </ng-container>
    `,
})
export class IMContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        layout: IMLayoutVariant.Desktop,
    };

    constructor(
        private imState: EvodeskIMStateService,
        private matDialog: MatDialog,
    ) {}

    ngOnInit(): void {
        this.imState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                layout: s.current.layoutVariant,
                props: {
                    ...this.state.props,
                    currentDialog: s.current.currentDialogId,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);

        if (this.state.dialogRef) {
            this.state.dialogRef.close();
        }
    }

    startDialog(): void {
        const dialogRef: MatDialogRef<IMStartDialogContainerComponent> = this.matDialog.open(IMStartDialogContainerComponent, {
            ...defaultMatDialogConfig,
        });

        dialogRef.componentInstance.successEvent.pipe(takeUntil(this.ngOnDestroy$)).subscribe((message) => {
            this.imState.dispatch({
                type: IMAction.UseDialog,
                payload: {
                    dialogId: message.dialogId,
                },
            });

            dialogRef.close();
        });

        dialogRef.afterClosed().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.state = {
                ...this.state,
                dialogRef: undefined,
            };
        });

        this.state = {
            ...this.state,
            dialogRef: dialogRef,
        };
    }
}
