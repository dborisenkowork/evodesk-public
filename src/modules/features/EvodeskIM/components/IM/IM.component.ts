import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {IMLayoutVariant} from '../../models/IM.models';

export interface IMComponentProps
{
    currentDialog: number;
}

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-im',
    templateUrl: './IM.component.pug',
    styleUrls: [
        './IM.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IMComponent implements OnChanges
{
    @Input() props: IMComponentProps;
    @Input() layout: IMLayoutVariant;

    @Output('startDialog') startDialogEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get containerCSSClasses(): any {
        return {
            '___mobile': this.layout === IMLayoutVariant.Mobile || this.layout === IMLayoutVariant.Tablet,
            '___desktop': this.layout === IMLayoutVariant.Desktop,
        };
    }

    get shouldDisplayContacts(): boolean {
        return this.layout === IMLayoutVariant.Desktop || ! this.props.currentDialog;
    }

    get shouldDisplayDialog(): boolean {
        return this.layout === IMLayoutVariant.Desktop || !! this.props.currentDialog;
    }

    get hasCurrentDialog(): boolean {
        return !! this.props.currentDialog;
    }

    startDialog(): void {
        this.startDialogEvent.emit(undefined);
    }
}
