import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';

import {imFormComponentFormBuilder, IMFormComponentProps, IMFormComponentSendEvent} from './IMForm.component';

import {EvodeskIMSendMessageService} from '../../services/EvodeskIMSendMessage.service';
import {EvodeskIMStateService} from '../../state/EvodeskIMState.service';

type Props = IMFormComponentProps;

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-im-form-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-im-form [props]="state.props" (send)="send($event)"></evodesk-im-form>
        </ng-container>
    `,
})
export class IMFormContainerComponent
{
    public state: State = {
        ready: true,
        props: {
            form: imFormComponentFormBuilder(this.fb),
        },
    };

    constructor(
        private fb: FormBuilder,
        private imState: EvodeskIMStateService,
        private imSendMessage: EvodeskIMSendMessageService,
    ) {}

    send($event: IMFormComponentSendEvent): void {
        this.imSendMessage.sendMessage({
            dialogId: this.imState.currentDialog.id,
            text: $event.formValue.text,
        });

        this.state.props.form.reset();
    }
}
