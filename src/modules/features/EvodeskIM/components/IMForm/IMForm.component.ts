import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {evodeskIMMaxMessageLength, evodeskIMMinMessageLength} from '../../configs/EvodeskIM.constants';

export interface IMFormComponentProps
{
    form: FormGroup;
}

interface State {
    ready: boolean;
    maxTextLength: number;
}

export interface IMFormComponentSendEvent {
    formValue: FormValue;
}

export interface IMFormFormValue {
    text: string;
}

type FormValue = IMFormFormValue;

export function imFormComponentFormBuilder(fb: FormBuilder): FormGroup {
    return fb.group({
        text: [[], [Validators.required, Validators.minLength(evodeskIMMinMessageLength), Validators.maxLength(evodeskIMMaxMessageLength)]],
    });
}

@Component({
    selector: 'evodesk-im-form',
    templateUrl: './IMForm.component.pug',
    styleUrls: [
        './IMForm.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IMFormComponent implements OnChanges, AfterViewInit
{
    @Input() props: IMFormComponentProps;

    @Output('send') sendEvent: EventEmitter<IMFormComponentSendEvent> = new EventEmitter<IMFormComponentSendEvent>();

    @ViewChild('input') inputRef: ElementRef;

    public state: State = {
        ready: false,
        maxTextLength: evodeskIMMaxMessageLength,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngAfterViewInit(): void {
        (this.inputRef.nativeElement as HTMLElement).focus();
    }

    get textLength(): number {
        return !! this.props.form.value.text ? this.props.form.value.text.length : 0;
    }

    enter($event: KeyboardEvent): void {
        if ($event.ctrlKey && $event.keyCode === 13) {
            this.props.form.patchValue({
                text: this.props.form.value.text + '\n',
            });
        } else if ($event.keyCode === 13) {
            $event.preventDefault();
            $event.stopPropagation();

            this.send();
        }
    }

    send(): void {
        if (this.props.form.valid) {
            this.sendEvent.emit({
                formValue: this.props.form.value,
            });
        }
    }
}
