import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {evodeskIMMaxSearchLength, evodeskIMMinSearchLength} from '../../configs/EvodeskIM.constants';

export interface IMSearchComponentProps
{
    form: FormGroup;
}

interface State {
    ready: boolean;
    isSearchInputFocused: boolean;
}

export function imSearchComponentFormBuilder(fb: FormBuilder): FormGroup {
    return fb.group({
        queryString: ['', [Validators.required, Validators.minLength(evodeskIMMinSearchLength), Validators.maxLength(evodeskIMMaxSearchLength)]],
    });
}

export interface IMSearchFormValue {
    queryString: string;
}

type FormValue = IMSearchFormValue;

export interface IMSearchComponentSearchEvent {
    isValid: boolean;
    formValue: FormValue;
}

@Component({
    selector: 'evodesk-im-search',
    templateUrl: './IMSearch.component.pug',
    styleUrls: [
        './IMSearch.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IMSearchComponent implements OnChanges
{
    @ViewChild('input') inputRef: ElementRef;

    @Input() props: IMSearchComponentProps;

    @Output('reset') resetEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('search') searchEvent: EventEmitter<IMSearchComponentSearchEvent> = new EventEmitter<IMSearchComponentSearchEvent>();
    @Output('startDialog') startDialogEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        isSearchInputFocused: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    focusSearchInput(): void {
        this.inputRef.nativeElement.focus();
        this.onSearchInputFocus();
    }

    onSearchInputFocus(): void {
        this.state = {
            ...this.state,
            isSearchInputFocused: true,
        };
    }

    onSearchInputFocusOut(): void {
        this.state = {
            ...this.state,
            isSearchInputFocused: false,
        };
    }

    searchDialog(): void {
        this.searchEvent.emit({
            isValid: this.props.form.valid,
            formValue: this.props.form.value,
        });
    }

    resetSearchInput(): void {
        (this.inputRef.nativeElement as HTMLElement).blur();

        this.resetEvent.emit(undefined);
    }

    startDialog(): void {
        this.startDialogEvent.emit(undefined);
    }
}
