import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {defaultMatDialogConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {imSearchComponentFormBuilder, IMSearchComponentProps, IMSearchComponentSearchEvent} from './IMSearch.component';
import {IMStartDialogContainerComponent} from '../../shared/components/IMStartDialog/IMStartDialogContainer.component';

import {EvodeskIMStateService, IMAction} from '../../state/EvodeskIMState.service';

type Props = IMSearchComponentProps;

interface State {
    ready: boolean;
    props?: Props;
    dialogRef?: MatDialogRef<any>;
}

@Component({
    selector: 'evodesk-im-search-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-im-search [props]="state.props" (search)="onSearch($event)" (reset)="onReset()" (startDialog)="onDialogStart()"></evodesk-im-search>
        </ng-container>
    `,
})
export class IMSearchContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        props: {
            form: imSearchComponentFormBuilder(this.fb),
        },
    };

    constructor(
        private fb: FormBuilder,
        private cdr: ChangeDetectorRef,
        private imState: EvodeskIMStateService,
        private matDialog: MatDialog,
    ) {}

    ngOnInit(): void {
        this.imState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => ! s.prev || s.prev.lastSearchRequest !== s.current.lastSearchRequest),
        ).subscribe((s) => {
            const hasLastRequest: boolean = !! s.current.lastSearchRequest;

            this.state.props.form.patchValue({
                queryString: hasLastRequest ? s.current.lastSearchRequest.queryString : '',
            });

            this.cdr.markForCheck();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);

        if (this.state.dialogRef) {
            this.state.dialogRef.close();
        }
    }

    onSearch($event: IMSearchComponentSearchEvent): void {
        if ($event.isValid && !! $event.formValue.queryString) {
            this.imState.dispatch({
                type: IMAction.SearchContacts,
                payload: {
                    queryString: $event.formValue.queryString,
                },
            });
        } else {
            this.imState.dispatch({
                type: IMAction.CancelSearchContacts,
            });
        }
    }

    onReset(): void {
        this.state.props.form.reset();

        this.imState.dispatch({
            type: IMAction.CancelSearchContacts,
        });
    }

    onDialogStart(): void {
        const dialogRef: MatDialogRef<IMStartDialogContainerComponent> = this.matDialog.open(IMStartDialogContainerComponent, {
            ...defaultMatDialogConfig,
        });

        dialogRef.componentInstance.successEvent.pipe(takeUntil(this.ngOnDestroy$)).subscribe((message) => {
            this.imState.dispatch({
                type: IMAction.UseDialog,
                payload: {
                    dialogId: message.dialogId,
                },
            });

            dialogRef.close();
        });

        dialogRef.afterClosed().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.state = {
                ...this.state,
                dialogRef: undefined,
            };
        });

        this.state = {
            ...this.state,
            dialogRef: dialogRef,
        };
    }
}
