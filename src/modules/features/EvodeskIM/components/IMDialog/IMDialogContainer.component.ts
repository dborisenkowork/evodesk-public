import {ChangeDetectorRef, Component, Input, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {interval, Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {IMDialogComponent, IMDialogComponentProps} from './IMDialog.component';

import {IMDialogMessageState, IMDialogState, IMLayoutVariant, IMMessageStatus} from '../../models/IM.models';
import {imRESTMessageBackendDateStringToDate} from '../../../../app/EvodeskRESTApi/models/im/IM.model';

import {EvodeskIMStateService, IMAction} from '../../state/EvodeskIMState.service';
import {InstantMessagesRESTService} from '../../../../app/EvodeskRESTApi/services/InstantMessagesREST.service';
import {EvodeskIMSyncStateService} from '../../state/EvodeskIMSyncState.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';

type Props = IMDialogComponentProps;

enum UpdateStatus
{
    None,
    Updating,
    Updated,
    Error,
}

interface State {
    ready: boolean;
    updateStatus: UpdateStatus;
    deliveryFlagsStatus: UpdateStatus;
    dialog: IMDialogState | undefined;
    initialLoadBeforeFlagSet: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-im-dialog-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-im-dialog [layout]="layout" [props]="state.props" (loadBefore)="loadBefore()" (close)="close()" (blockUser)="blockUser()" (dropDialog)="dropDialog()"></evodesk-im-dialog>
        </ng-container>
    `,
})
export class IMDialogContainerComponent implements OnInit, OnDestroy {
    @Input() layout: IMLayoutVariant = IMLayoutVariant.Desktop;
    @Input() updateInterval: number = 2000;
    @Input() messagesPerPage: number = 10;

    @ViewChild(IMDialogComponent) view: IMDialogComponent;

    private isDestroyed: boolean = false;

    private changeDialog$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        dialog: undefined,
        updateStatus: UpdateStatus.None,
        deliveryFlagsStatus: UpdateStatus.None,
        initialLoadBeforeFlagSet: false,
    };

    constructor(
        private router: Router,
        private cdr: ChangeDetectorRef,
        private ngZone: NgZone,
        private imState: EvodeskIMStateService,
        private imSync: EvodeskIMSyncStateService,
        private imREST: InstantMessagesRESTService,
        private currentUserService: CurrentUserService,
    ) {
    }

    ngOnInit(): void {
        this.imState.side$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((action) => {
            switch (action.type) {
                case IMAction.AddMessageToQueue:
                case IMAction.UseContact:
                case IMAction.UseDialog:
                    this.ngZone.runOutsideAngular(() => {
                        this.view.stickToBottom();
                    });

                    break;
            }
        });

        this.imState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !this.state.props || s.prev.currentDialogId !== s.current.currentDialogId),
        ).subscribe((s) => {
            this.changeDialog$.next(undefined);

            if (s.current.currentDialogId) {
                const dialog: IMDialogState = this.imState.currentDialog;

                if (dialog) {
                    this.state = {
                        ...this.state,
                        ready: false,
                        dialog: dialog,
                        updateStatus: UpdateStatus.None,
                        initialLoadBeforeFlagSet: false,
                        props: {
                            ...this.state.props,
                            dialogName: dialog.name,
                            messages: [],
                            queuedMessages: [],
                            hasMoreToLoad: false,
                            loading: false,
                            preventLoadMoreTrigger: false,
                        },
                    };

                    this.subscribeDialog();
                } else {
                    this.imState.dispatch({
                        type: IMAction.CloseCurrentDialog,
                    });

                    this.router.navigate(['/im']);
                }
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }

            if (! this.isDestroyed) {
                this.cdr.detectChanges();
            }
        });

        this.imState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !this.state.props
                || s.prev.currentDialogId !== s.current.currentDialogId || !s.prev.dialogs.length
                || s.prev.dialogs.filter(d => d.id === s.current.currentDialogId)[0].messages !== s.current.dialogs.filter(d => d.id === s.current.currentDialogId)[0].messages
                || s.prev.dialogs.filter(d => d.id === s.current.currentDialogId)[0].queuedMessages !== s.current.dialogs.filter(d => d.id === s.current.currentDialogId)[0].queuedMessages
                || s.prev.dialogs.filter(d => d.id === s.current.currentDialogId)[0].hasMoreToLoad !== s.current.dialogs.filter(d => d.id === s.current.currentDialogId)[0].hasMoreToLoad,
            ),
        ).subscribe((s) => {
            if (s.current.currentDialogId) {
                const dialog: IMDialogState = this.imState.currentDialog;

                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        messages: dialog.messages,
                        queuedMessages: dialog.queuedMessages,
                        hasMoreToLoad: dialog.hasMoreToLoad,
                    },
                };
            }
        });
    }

    ngOnDestroy(): void {
        this.isDestroyed = true;

        this.changeDialog$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    subscribeDialog(): void {
        const tick: Function = () => {
            if (this.state.updateStatus !== UpdateStatus.Updating) {
                this.state = {
                    ...this.state,
                    updateStatus: UpdateStatus.Updating,
                };

                this.imREST.getMessagesOfDialog(this.state.dialog.id, {num: this.messagesPerPage + 1}).pipe(takeUntil(this.changeDialog$)).subscribe(
                    (messages) => {
                        this.imState.dispatch({
                            type: IMAction.PushMessages,
                            payload: {
                                dialogId: this.state.dialog.id,
                                messages: messages.slice(0, this.messagesPerPage).map(m => {
                                    return <IMDialogMessageState>{
                                        message: {
                                            id: m.id,
                                            createdAt: imRESTMessageBackendDateStringToDate(m.creation_at.date),
                                            dialogId: this.state.dialog.id,
                                            text: m.text,
                                            read: m.read,
                                            author: {
                                                id: m.author_id,
                                                name: m.author_name || m.author_email,
                                                avatar: m.author_avatar,
                                            },
                                        },
                                        messageState: IMMessageStatus.Sent,
                                    };
                                }),
                            },
                        });

                        const unreadMessages: Array<IMDialogMessageState> = this.imState.currentDialog.messages.filter(m => {
                            return m.message.read === 0 && m.message.author.id !== this.currentUserService.impersonatedAs.id;
                        } );

                        if (unreadMessages.length) {
                            if (this.state.deliveryFlagsStatus !== UpdateStatus.Updating) {
                                this.state = {
                                    ...this.state,
                                    deliveryFlagsStatus: UpdateStatus.Updating,
                                };

                                this.imREST.markDialogAsRead(this.state.dialog.id).subscribe(
                                    () => {
                                        if (! this.isDestroyed) {
                                            this.state = {
                                                ...this.state,
                                                deliveryFlagsStatus: UpdateStatus.Updated,
                                            };
                                        }
                                    },
                                    (httpError) => {
                                        this.state = {
                                            ...this.state,
                                            deliveryFlagsStatus: UpdateStatus.Error,
                                        };

                                        this.imSync.dispatchCommonHttpError(httpError);
                                    },
                                );
                            }
                        }

                        if (!this.state.initialLoadBeforeFlagSet) {
                            if (messages.length > this.messagesPerPage) {
                                this.imState.dispatch({
                                    type: IMAction.SetHasMoreToLoadFlag,
                                    payload: {
                                        dialogId: this.state.dialog.id,
                                    },
                                });
                            } else {
                                this.imState.dispatch({
                                    type: IMAction.UnsetHasMoreToLoadFlag,
                                    payload: {
                                        dialogId: this.state.dialog.id,
                                    },
                                });
                            }

                            this.state = {
                                ...this.state,
                                initialLoadBeforeFlagSet: true,
                            };
                        }

                        this.state = {
                            ...this.state,
                            ready: true,
                            updateStatus: UpdateStatus.Updated,
                        };
                    },
                    (httpError) => {
                        this.state = {
                            ...this.state,
                            updateStatus: UpdateStatus.Error,
                        };

                        this.imSync.dispatchCommonHttpError(httpError);
                    },
                );
            }
        };

        interval(this.updateInterval).pipe(takeUntil(this.changeDialog$)).subscribe(() => {
            tick();
        });

        tick();
    }

    loadBefore(): void {
        if (this.imState.currentDialog.hasMoreToLoad) {
            const minId: number = Math.min.apply(Math, this.imState.currentDialog.messages.map(m => m.message.id));

            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    loading: true,
                    preventLoadMoreTrigger: true,
                },
            };

            if (! this.isDestroyed) {
                this.cdr.detectChanges();
            }

            this.imREST.getMessagesOfDialog(this.state.dialog.id, {
                beforeId: minId,
                num: this.messagesPerPage + 1,
            }).pipe(takeUntil(this.changeDialog$)).subscribe((messages) => {
                    window.requestAnimationFrame(() => {
                        this.imState.dispatch({
                            type: IMAction.PushMessages,
                            payload: {
                                dialogId: this.state.dialog.id,
                                messages: messages.slice(0, this.messagesPerPage).map(m => {
                                    return <IMDialogMessageState>{
                                        message: {
                                            id: m.id,
                                            createdAt: imRESTMessageBackendDateStringToDate(m.creation_at.date),
                                            dialogId: this.state.dialog.id,
                                            text: m.text,
                                            read: m.read,
                                            author: {
                                                id: m.author_id,
                                                name: m.author_name || m.author_email,
                                                avatar: m.author_avatar,
                                            },
                                        },
                                        messageState: IMMessageStatus.Sent,
                                    };
                                }),
                            },
                        });

                        if (messages.length > this.messagesPerPage) {
                            this.imState.dispatch({
                                type: IMAction.SetHasMoreToLoadFlag,
                                payload: {
                                    dialogId: this.state.dialog.id,
                                },
                            });
                        } else {
                            this.imState.dispatch({
                                type: IMAction.UnsetHasMoreToLoadFlag,
                                payload: {
                                    dialogId: this.state.dialog.id,
                                },
                            });
                        }

                        window.setTimeout(() => {
                            window.setTimeout(() => {
                                this.state = {
                                    ...this.state,
                                    props: {
                                        ...this.state.props,
                                        loading: false,
                                        preventLoadMoreTrigger: false,
                                    },
                                };
                            });
                        });
                    });
                },
                (httpError) => {
                    this.imSync.dispatchCommonHttpError(httpError);

                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            loading: false,
                            preventLoadMoreTrigger: false,
                        },
                    };

                    if (! this.isDestroyed) {
                        this.cdr.detectChanges();
                    }
                },
            );
        }
    }

    close(): void {
        this.router.navigate(['/im']);
    }

    blockUser(): void {
        this.imREST.blockUser(this.imState.currentDialog.contact.id).subscribe();

        this.imState.dispatch({
            type: IMAction.DropDialog,
            payload: {
                dialogId: this.imState.currentDialog.id,
            },
        });
    }

    dropDialog(): void {
        this.imREST.dropDialog(this.imState.currentDialog.id).subscribe();

        this.imState.dispatch({
            type: IMAction.DropDialog,
            payload: {
                dialogId: this.imState.currentDialog.id,
            },
        });
    }
}
