import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild} from '@angular/core';

import {interval, Observable, Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {IMDialogMessageState, IMLayoutVariant} from '../../models/IM.models';

import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';

export interface IMDialogComponentProps
{
    dialogName: string;
    messages: Array<IMDialogMessageState>;
    queuedMessages: Array<IMDialogMessageState>;
    hasMoreToLoad: boolean;
    loading: boolean;
    preventLoadMoreTrigger: boolean;
}

interface State {
    ready: boolean;
    messages: Array<MessagesGroup>;
    stickToScrollEnabled?: Subscription;
}

interface MessagesGroup {
    timestamp: Date;
    timestamp$?: Observable<string>;
    messages: Array<IMDialogMessageState>;
}

const marginOnScrollTopTrigger: number = 0;

@Component({
    selector: 'evodesk-im-dialog',
    templateUrl: './IMDialog.component.pug',
    styleUrls: [
        './IMDialog.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IMDialogComponent implements OnChanges, OnDestroy
{
    @Input() props: IMDialogComponentProps;
    @Input() layout: IMLayoutVariant = IMLayoutVariant.Desktop;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('loadBefore') loadBeforeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('blockUser') blockUserEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('dropDialog') dropDialogEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('content') content: ElementRef;

    private stopStickScroll$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        messages: [],
    };

    constructor(
        private ngZone: NgZone,
        private evodeskDateService: EvodeskDateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    messages: (() => {
                        if (this.props.messages.length) {
                            const messageGroupsMap: { [timestamp: string]: MessagesGroup } = {};

                            this.props.messages.forEach((m) => {
                                const messageTimestamp: string = (new Date(
                                    m.message.createdAt.getFullYear(),
                                    m.message.createdAt.getMonth(),
                                    m.message.createdAt.getDate(),
                                    0, 0, 0,
                                )).getTime().toString();

                                if (! messageGroupsMap[messageTimestamp]) {
                                    messageGroupsMap[messageTimestamp] = {
                                        timestamp: new Date(
                                            m.message.createdAt.getFullYear(),
                                            m.message.createdAt.getMonth(),
                                            m.message.createdAt.getDate(),
                                            0, 0, 0,
                                        ),
                                        messages: [],
                                    };
                                }

                                messageGroupsMap[messageTimestamp].messages.push(m);
                            });

                            const result: Array<MessagesGroup> = [];
                            const allTimestamps: Array<number> = Object.keys(messageGroupsMap).map(k => parseInt(k, 10));

                            allTimestamps.sort((a, b) => {
                                return a - b;
                            });

                            allTimestamps.forEach((timestamp, index) => {
                                const tDate: Date = new Date();

                                tDate.setTime(timestamp);

                                messageGroupsMap[timestamp.toString()].messages.sort((a, b) => {
                                    if (a.message.createdAt.getTime() !== b.message.createdAt.getTime()) {
                                        return a.message.createdAt.getTime() - b.message.createdAt.getTime();
                                    }
                                    return a.message.id - b.message.id;
                                });

                                const groupMessages: Array<IMDialogMessageState> = (index === allTimestamps.length - 1) ?
                                    [ ...messageGroupsMap[timestamp.toString()].messages, ...this.props.queuedMessages ] :
                                    messageGroupsMap[timestamp.toString()].messages;

                                result.push({
                                    timestamp: new Date(timestamp),
                                    timestamp$: this.evodeskDateService.diffFromNowV5(tDate),
                                    messages: groupMessages,
                                });
                            });

                            return result;
                        } else {
                            return [];
                        }
                    })(),
                };

                if (changes['props'].isFirstChange()) {
                    this.stickToBottom();
                }
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.stopStickScroll$.next(undefined);
    }

    get containerCSSClasses(): any {
        return {
            '___mobile': this.layout === IMLayoutVariant.Mobile,
            '___tablet': this.layout === IMLayoutVariant.Tablet,
            '___desktop': this.layout === IMLayoutVariant.Desktop,
        };
    }

    onWheel($event: WheelEvent): void {
        this.stopScrollStick();

        if ($event.deltaY < 0) {
            if (! this.props.loading && this.props.hasMoreToLoad) {
                if ((this.content.nativeElement as HTMLElement).scrollTop <= marginOnScrollTopTrigger) {
                    this.loadBefore();
                }
            }
        }
    }

    onTouchMove(): void {
        this.stopScrollStick();

        if (! this.props.loading && this.props.hasMoreToLoad) {
            if ((this.content.nativeElement as HTMLElement).scrollTop <= marginOnScrollTopTrigger) {
                this.loadBefore();
            }
        }
    }

    onContentScroll(): void {
        if (! this.props.loading && this.props.hasMoreToLoad) {
            if ((this.content.nativeElement as HTMLElement).scrollTop <= marginOnScrollTopTrigger) {
                this.loadBefore();
            }
        }
    }

    loadBefore(): void {
        if (! this.props.preventLoadMoreTrigger) {
            this.loadBeforeEvent.emit(undefined);
        }
    }

    stickToBottom(): void {
        this.ngZone.runOutsideAngular(() => {
            this.state = {
                ...this.state,
                stickToScrollEnabled: interval(100).pipe(takeUntil(this.stopStickScroll$)).subscribe(() => {
                    (this.content.nativeElement as HTMLElement).scrollTop = (this.content.nativeElement as HTMLElement).scrollHeight;
                }),
            };
        });
    }

    stopScrollStick() {
        if (this.state.stickToScrollEnabled) {
            this.stopStickScroll$.next(undefined);

            this.state = {
                ...this.state,
                stickToScrollEnabled: undefined,
            };
        }
    }

    get isMobile(): boolean {
        return this.layout === IMLayoutVariant.Mobile;
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    blockUser(): void {
        this.blockUserEvent.emit(undefined);
    }

    dropDialog(): void {
        this.dropDialogEvent.emit(undefined);
    }
}
