import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {Observable} from 'rxjs';

import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';
import {IMDialogId, IMDialogState, IMLayoutVariant} from '../../models/IM.models';

export interface IMContactsComponentProps
{
    isInFilteredMode: boolean;
    currentDialog: IMDialogId;
    dialogs: Array<IMDialogState>;
}

interface State {
    ready: boolean;
    createdAt: { [dialogId: number]: Observable<string> };
}

@Component({
    selector: 'evodesk-im-contacts',
    templateUrl: './IMContacts.component.pug',
    styleUrls: [
        './IMContacts.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IMContactsComponent implements OnChanges
{
    @Input() props: IMContactsComponentProps;
    @Input() layout: IMLayoutVariant;

    @Output('useDialog') useDialogEvent: EventEmitter<IMDialogState> = new EventEmitter<IMDialogState>();

    public state: State = {
        ready: false,
        createdAt: {},
    };

    constructor(
        private evodeskDateService: EvodeskDateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                const dates: { [dialogId: number]: Observable<string> } = {};

                this.props.dialogs.forEach((d) => {
                    dates[d.id] = this.evodeskDateService.diffFromNowV4(d.lastMessageDate, { futureAsNow: true });
                });

                this.state = {
                    ...this.state,
                    ready: true,
                    createdAt: dates,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get isMobileLayout(): boolean {
        return this.layout === IMLayoutVariant.Mobile;
    }

    get containerCSSClasses(): any {
        return {
            'is-desktop': this.layout === IMLayoutVariant.Desktop || this.layout === IMLayoutVariant.Tablet,
            'is-mobile': this.layout === IMLayoutVariant.Mobile,
        };
    }

    dialogCSSClasses(dialog: IMDialogState): any {
        return {
            'is-active': this.props.currentDialog === dialog.id,
            'has-unreaded-messages': dialog.read === 0,
        };
    }

    get dialogs(): Array<IMDialogState> {
        return this.props.dialogs;
    }

    trackByDialogId(index: number, dialog: IMDialogState): any {
        return dialog.id;
    }

    getDate(d: IMDialogState): Observable<string> {
        return this.state.createdAt[d.id];
    }

    useDialog(dialog: IMDialogState): void {
        this.useDialogEvent.emit(dialog);
    }
}
