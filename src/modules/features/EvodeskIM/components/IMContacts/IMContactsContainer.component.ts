import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {IMContactsComponentProps} from './IMContacts.component';

import {IMDialogState, IMLayoutVariant} from '../../models/IM.models';

import {EvodeskIMStateService} from '../../state/EvodeskIMState.service';

type Props = IMContactsComponentProps;

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-im-contacts-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-im-contacts [layout]="layout" [props]="state.props" (useDialog)="useDialog($event)"></evodesk-im-contacts>
        </ng-container>
    `,
})
export class IMContactsContainerComponent implements OnInit, OnDestroy
{
    @Input() layout: IMLayoutVariant;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private router: Router,
        private imState: EvodeskIMStateService,
    ) {}

    ngOnInit(): void {
        this.imState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    isInFilteredMode: s.current.filteredDialogIds !== undefined,
                    dialogs: (() => {
                        if (s.current.filteredDialogIds === undefined) {
                            return s.current.dialogs;
                        } else {
                            return s.current.dialogs.filter(d => !!~s.current.filteredDialogIds.indexOf(d.id));
                        }
                    })(),
                    currentDialog: s.current.currentDialogId,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    useDialog(dialog: IMDialogState): void {
        this.router.navigate(['/im'], {
            queryParams: <any>{
                dialog: dialog.id,
            },
        });
    }
}
