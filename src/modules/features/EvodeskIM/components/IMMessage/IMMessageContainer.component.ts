import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import {IMMessageComponentProps} from './IMMessage.component';

import {IMDialogMessageState} from '../../models/IM.models';

import {EvodeskIMBodyParserService} from '../../services/EvodeskIMBodyParser.service';
import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';
import {EvodeskIMStateService, IMAction} from '../../state/EvodeskIMState.service';

type Props = IMMessageComponentProps;

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-im-message-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-im-message [props]="state.props" (delete)="deleteBtnClick()"></evodesk-im-message>
        </ng-container>
    `,
})
export class IMMessageContainerComponent implements OnChanges, OnDestroy
{
    @Input() message: IMDialogMessageState;

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private evodeskDateService: EvodeskDateService,
        private imBodyParsed: EvodeskIMBodyParserService,
        private currentUser: CurrentUserService,
        private imState: EvodeskIMStateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (changes['message']) {
            if (this.message) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        message: this.message,
                        parsed$: this.imBodyParsed.parseToHtml(this.message.message.text),
                        date$: this.evodeskDateService.diffFromNowV4(this.message.message.createdAt, { futureAsNow: true }),
                        isOwnMessage: this.message.message.author.id === this.currentUser.impersonatedAs.id,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    deleteBtnClick() {
        this.imState.dispatch({
            type: IMAction.RemoveMessageFromQueue,
            payload: {
                dialogId: this.message.message.dialogId,
                message: this.message,
            },
        });
    }
}
