import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {Observable} from 'rxjs';

import {IMDialogMessageState, IMMessageStatus} from '../../models/IM.models';

export interface IMMessageComponentProps {
    isOwnMessage: boolean;
    message: IMDialogMessageState;
    parsed$?: Observable<string>;
    date$?: Observable<string>;
}

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-im-message',
    templateUrl: './IMMessage.component.pug',
    styleUrls: [
        './IMMessage.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IMMessageComponent implements OnChanges
{
    @Input() props: IMMessageComponentProps;

    @Output('delete') deleteEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    deleteBtnClick() {
        this.deleteEvent.emit();
    }

    get containerCSSClasses(): any {
        return {
            'is-sent': this.props.message.messageState === IMMessageStatus.Sent,
            'is-error': this.props.message.messageState === IMMessageStatus.Error,
            'is-sending': this.props.message.messageState === IMMessageStatus.Sending,
        };
    }

    get isDelivered(): boolean {
        return this.props.message.messageState === IMMessageStatus.Sent && this.props.message.message.read === 0;
    }

    get isRead(): boolean {
        return this.props.message.messageState === IMMessageStatus.Sent && this.props.message.message.read === 1;
    }

    get isError(): boolean {
        return this.props.message.messageState === IMMessageStatus.Error;
    }
}
