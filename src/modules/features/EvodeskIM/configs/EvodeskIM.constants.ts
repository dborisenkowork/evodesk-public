export const evodeskIMLayoutMobile: { to: number } = { to: 599 };
export const evodeskIMLayoutTablet: { from: number, to: number } = { from: 600, to: 999 };
export const evodeskIMLayoutDesktop: { from: number } = { from: 1000 };

export const evodeskIMMinMessageLength: number = 0;
export const evodeskIMMaxMessageLength: number = 2000;

export const evodeskIMMinSearchLength: number = 0;
export const evodeskIMMaxSearchLength: number = 255;
