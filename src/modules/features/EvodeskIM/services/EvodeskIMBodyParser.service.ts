import {Injectable} from '@angular/core';

import * as ellipsize from 'ellipsize';

import {Observable} from 'rxjs';

export interface EvodeskIMBodyParseOptions {
    maxSize?: number;
}

@Injectable()
export class EvodeskIMBodyParserService
{
    parseToHtml(input: string, withOptions?: EvodeskIMBodyParseOptions): Observable<string> {
        const options: EvodeskIMBodyParseOptions = {
            maxSize: -1,
            ...withOptions,
        };

        return Observable.create(observer => {
            if (!! input) {
                let result: string = input;

                if (options.maxSize > 0) {
                    result = ellipsize(result, options.maxSize);
                }

                result = result.replace(/(?:\r\n|\r|\n)/g, '<br/>');

                observer.next(result);
                observer.complete();
            } else {
                observer.next('');
                observer.complete();
            }
        });
    }
}
