import {Injectable} from '@angular/core';

import {EvodeskIMStateService, IMAction} from '../state/EvodeskIMState.service';

export interface IMSearchRequest {
    queryString: string;
}

@Injectable()
export class EvodeskIMSearchService
{
    constructor(
        private imState: EvodeskIMStateService,
    ) {}

    doSearch(request: IMSearchRequest): void {
        this.imState.dispatch({
            type: IMAction.Search,
            payload: {
                request: request,
            },
        });
    }
}
