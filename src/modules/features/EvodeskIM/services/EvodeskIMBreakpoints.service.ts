import {Injectable} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';

import {filter, takeUntil} from 'rxjs/operators';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {IMLayoutVariant} from '../models/IM.models';

import {evodeskIMLayoutDesktop, evodeskIMLayoutMobile, evodeskIMLayoutTablet} from '../configs/EvodeskIM.constants';

@Injectable()
export class EvodeskIMBreakpointsService
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private _current$: BehaviorSubject<IMLayoutVariant> = new BehaviorSubject<IMLayoutVariant>(IMLayoutVariant.Desktop);

    constructor(
        private breakpointObserver: BreakpointObserver,
    ) {}

    init(): Observable<IMLayoutVariant> {
        this.breakpointObserver.observe(`(max-width: ${evodeskIMLayoutMobile.to}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe((e) => {
            this._current$.next(IMLayoutVariant.Mobile);
        });

        this.breakpointObserver.observe(`(min-width: ${evodeskIMLayoutTablet.from}px) and (max-width: ${evodeskIMLayoutTablet.to}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this._current$.next(IMLayoutVariant.Tablet);
        });

        this.breakpointObserver.observe(`(min-width: ${evodeskIMLayoutDesktop.from}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this._current$.next(IMLayoutVariant.Desktop);
        });

        return this.current$;
    }

    destroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get current$(): Observable<IMLayoutVariant> {
        return this._current$.asObservable();
    }
}
