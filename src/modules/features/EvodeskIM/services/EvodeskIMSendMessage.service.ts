import {Injectable} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {IMDialogId, IMMessageStatus} from '../models/IM.models';
import {imRESTMessageBackendDateStringToDate} from '../../../app/EvodeskRESTApi/models/im/IM.model';

import {EvodeskIMStateService, IMAction} from '../state/EvodeskIMState.service';
import {EvodeskIMSyncStateService} from '../state/EvodeskIMSyncState.service';
import {InstantMessagesRESTService} from '../../../app/EvodeskRESTApi/services/InstantMessagesREST.service';
import {CurrentUserService} from '../../EvodeskAuth/services/CurrentUser.service';

export interface SendToDialogRequest {
    dialogId: IMDialogId;
    text: string;
}

@Injectable()
export class EvodeskIMSendMessageService
{
    public static RETRY_MS: number = 1000;

    private destroy$: Subject<void> = new Subject<void>();

    constructor(
        private imState: EvodeskIMStateService,
        private imSync: EvodeskIMSyncStateService,
        private imRest: InstantMessagesRESTService,
        private currentUser: CurrentUserService,
    ) {}

    destroy(): void {
        this.destroy$.next(undefined);
    }

    sendMessage(request: SendToDialogRequest): void {
        this.pushIntoQueue(request.dialogId, request.text);
    }

    public pushIntoQueue(dialogId: number, message: string) {
        this.imState.dispatch({
            type: IMAction.AddMessageToQueue,
            payload: {
                dialogId: dialogId,
                message: {
                    message: {
                        id: 0,
                        createdAt: new Date(),
                        dialogId: dialogId,
                        text: message,
                        read: 0,
                        author: {
                            id: this.currentUser.impersonatedAs.id,
                            name: this.currentUser.impersonatedAs.name || this.currentUser.impersonatedAs.email,
                            avatar: this.currentUser.impersonatedAs.avatar,
                        },
                    },
                    messageState: IMMessageStatus.Sending,
                },
            },
        });
        if (this.imState.getDialog(dialogId).queuedMessages.length === 1) {
            this.handleMessage(dialogId, message);
        }
    }

    private handleMessage(dialogId: number, message: string) {
        this.imRest.sendMessage({
            from: this.currentUser.impersonatedAs.id,
            recipientIds: [
                this.imState.getDialog(dialogId).contact.id,
            ],
            message: message,
        }).pipe(takeUntil(this.destroy$)).subscribe(
            (m) => {

                this.imState.dispatch({
                    type: IMAction.SendMessage,
                    payload: {
                        dialogId: dialogId,
                        message: {
                            message: {
                                id: m.id,
                                createdAt: imRESTMessageBackendDateStringToDate(m.creation_at),
                                dialogId: dialogId,
                                text: m.text,
                                read: 0,
                                author: {
                                    id: m.user_from.id.id,
                                    name: m.user_from.id.name || m.user_from.id.email,
                                    avatar: m.user_from.id.avatar,
                                },
                            },
                            messageState: IMMessageStatus.Sent,
                        },
                    },
                });
                this.handleFirstQueuedMessage(dialogId);
            },
            (httpError) => {
                this.imSync.dispatchCommonHttpError(httpError);
                this.imState.dispatch({
                    type: IMAction.SendMessageError,
                    payload: { dialogId: dialogId },
                });
                setTimeout(() => {
                    this.handleFirstQueuedMessage(dialogId);
                }, EvodeskIMSendMessageService.RETRY_MS);
            },
        );
    }

    private handleFirstQueuedMessage(dialogId: number) {
        if (this.imState.getDialog(dialogId).queuedMessages.length > 0) {
            this.handleMessage(dialogId, this.imState.getDialog(dialogId).queuedMessages[0].message.text);
        }
    }
}
