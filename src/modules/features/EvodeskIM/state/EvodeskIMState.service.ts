import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable, Subject} from 'rxjs';

import * as R from 'ramda';

import {IMSearchRequest} from '../services/EvodeskIMSearch.service';
import {IMSyncStatus} from './EvodeskIMSyncState.service';

import {ProfileModel} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {IMContactId, IMDialogId, IMDialogMessageState, IMDialogState, IMLayoutVariant, IMMessage, IMMessageId, IMMessageStatus} from '../models/IM.models';

export type IMPrevState = IMState | undefined;
export type IMNextState = IMState;
export type IMStates = { current: IMNextState, prev?: IMPrevState };

export interface IMState {
    lastSearchRequest: IMSearchRequest | undefined;
    syncStatus: IMSyncStatus;
    layoutVariant: IMLayoutVariant;
    currentDialogId: number | undefined;
    dialogs: Array<IMDialogState>;
    filteredDialogIds: Array<IMDialogId> | undefined;
}

export enum IMAction {
    Reset = 'Reset',
    Bootstrap = 'Bootstrap',
    UpdateDialog = 'UpdateLastMessageDialog',
    SetLayout = 'SetLayout',
    SetSyncStatus = 'SetSyncStatus',
    UseDialog = 'UseDialog',
    UseContact = 'UseContact',
    Search = 'Search',
    ResetSearch = 'ResetSearch',
    PushMessages = 'PushMessages',
    SetMessageState = 'SetMessageState',
    SetMessageAndState = 'SetMessageAndState',
    StartDialog = 'StartDialog',
    RemoveMessage = 'RemoveMessage',
    SendMessage = 'SendMessage',
    SendMessageError = 'SendMessageError',
    SetHasMoreToLoadFlag = 'SetHasMoreToLoadFlag',
    UnsetHasMoreToLoadFlag = 'UnsetHasMoreToLoadFlag',
    CloseCurrentDialog = 'CloseCurrentDialog',
    DropDialog = 'DropDialog',
    MarkAllMessagesAsRead = 'MarkAllMessagesAsRead',
    SearchContacts = 'SearchContacts',
    CancelSearchContacts = 'CancelSearchContacts',
    AddMessageToQueue = 'AddMessageToQueue',
    RemoveMessageFromQueue = 'RemoveMessageFromQueue',
}

function initialState(): IMState {
    return {
        lastSearchRequest: undefined,
        layoutVariant: IMLayoutVariant.Desktop,
        syncStatus: IMSyncStatus.NotReady,
        currentDialogId: undefined,
        dialogs: [],
        filteredDialogIds: undefined,
    };
}

type Actions =
      { type: IMAction.Reset }
    | { type: IMAction.Bootstrap, payload: { bootstrap: IMBootstrap } }
    | { type: IMAction.UpdateDialog, payload: { dialogs:  Array<IMDialogState>} }
    | { type: IMAction.SetLayout, payload: { layout: IMLayoutVariant } }
    | { type: IMAction.SetSyncStatus, payload: { syncStatus: IMSyncStatus } }
    | { type: IMAction.UseDialog, payload: { dialogId: number } }
    | { type: IMAction.UseContact, payload: { contactId: IMContactId } }
    | { type: IMAction.Search, payload: { request: IMSearchRequest } }
    | { type: IMAction.ResetSearch }
    | { type: IMAction.PushMessages, payload: { dialogId: IMDialogId, messages: Array<IMDialogMessageState> } }
    | { type: IMAction.SetMessageState, payload: { messageId: IMMessageId, dialogId: IMDialogId, status: IMMessageStatus } }
    | { type: IMAction.SetMessageAndState, payload: { messageId: IMMessageId, setMessage: IMMessage, dialogId: IMDialogId, status: IMMessageStatus } }
    | { type: IMAction.StartDialog, payload: { recipient: ProfileModel } }
    | { type: IMAction.RemoveMessage, payload: { dialogId: IMDialogId, messageId: IMMessageId } }
    | { type: IMAction.SendMessage, payload: { dialogId: IMDialogId, message: IMDialogMessageState } }
    | { type: IMAction.SendMessageError, payload: { dialogId: IMDialogId } }
    | { type: IMAction.SetHasMoreToLoadFlag, payload: { dialogId: IMDialogId } }
    | { type: IMAction.UnsetHasMoreToLoadFlag, payload: { dialogId: IMDialogId } }
    | { type: IMAction.CloseCurrentDialog }
    | { type: IMAction.DropDialog, payload: { dialogId: IMDialogId } }
    | { type: IMAction.MarkAllMessagesAsRead, payload: { dialogId: IMDialogId } }
    | { type: IMAction.SearchContacts, payload: { queryString: string } }
    | { type: IMAction.CancelSearchContacts }
    | { type: IMAction.AddMessageToQueue, payload: { dialogId: IMDialogId, message: IMDialogMessageState } }
    | { type: IMAction.RemoveMessageFromQueue, payload: { dialogId: IMDialogId, message: IMDialogMessageState } }
;

export interface IMBootstrap {
    dialogs?: Array<IMDialogState>;
}

@Injectable()
export class EvodeskIMStateService
{
    private _current$: BehaviorSubject<IMStates> = new BehaviorSubject<IMStates>({ prev: undefined, current: initialState() });
    private _side$: Subject<Actions> = new Subject<Actions>();

    get current$(): Observable<IMStates> {
        return this._current$.asObservable();
    }

    get side$(): Observable<Actions> {
        return this._side$.asObservable();
    }

    get snapshot(): IMState {
        return this._current$.getValue().current;
    }

    get currentDialog(): IMDialogState {
        if (this.snapshot.currentDialogId) {
            return this.snapshot.dialogs.filter(d => d.id === this.snapshot.currentDialogId)[0];
        } else {
            throw new Error('No current dialog available');
        }
    }

    getDialog(dialogId: IMDialogId): IMDialogState {
        const result: IMDialogState = this.snapshot.dialogs.filter(d => d.id === dialogId)[0];

        if (! result) {
            throw new Error(`Dialog with ID "${dialogId}" not found`);
        }

        return result;
    }

    reset(): void {
        this.dispatch({
            type: IMAction.Reset,
        });
    }

     dispatch(action: Actions): void {
        this._side$.next(action);

        switch (action.type) {
            case IMAction.Reset:
                this.setState(() => {
                    return initialState();
                });

                break;

            case IMAction.Bootstrap:
                this.setState((orig) => {
                    return {
                        ...orig,
                        ...action.payload.bootstrap,
                    };
                });

                break;

            case IMAction.UpdateDialog:
                this.setState((orig) => {
                    const updatedOldDialogs: Array<IMDialogState> = [];

                    action.payload.dialogs.map(nd => {
                        const od: IMDialogState = orig.dialogs.find((value) => {
                            return value.id === nd.id;
                        });

                        if (! od) {
                            updatedOldDialogs.push(nd);
                        } else if (od.lastMessage !== nd.lastMessage) {
                            updatedOldDialogs.push({
                                ...od,
                                lastMessage: nd.lastMessage,
                                lastMessageDate: nd.lastMessageDate,
                            });
                        } else {
                            updatedOldDialogs.push(od);
                        }
                    });

                    return {
                        ...orig,
                        dialogs: updatedOldDialogs,
                    };
                });

                break;

            case IMAction.SetLayout:
                this.setState((orig) => {
                    return {
                        ...orig,
                        layoutVariant: action.payload.layout,
                    };
                });

                break;

            case IMAction.SetSyncStatus:
                this.setState((orig) => {
                    return {
                        ...orig,
                        syncStatus: action.payload.syncStatus,
                    };
                });

                break;

            case IMAction.UseContact:
                this.setState((orig) => {
                    return {
                        ...orig,
                        currentDialogId: action.payload.contactId,
                    };
                });

                break;

            case IMAction.UseDialog:
                this.setState((orig) => {
                    return {
                        ...orig,
                        currentDialogId: action.payload.dialogId,
                    };
                });

                break;

            case IMAction.Search:
                this.setState((orig) => {
                    return {
                        ...orig,
                        lastSearchRequest: action.payload.request,
                    };
                });

                break;

            case IMAction.ResetSearch:
                this.setState((orig) => {
                    return {
                        ...orig,
                        lastSearchRequest: undefined,
                    };
                });

                break;

            case IMAction.PushMessages:
                this.setState((orig) => {
                    const dialog: IMDialogState = orig.dialogs.filter(d => d.id === action.payload.dialogId)[0];
                    const messagesMap: { [messageId: number]: IMDialogMessageState } = {};

                    dialog.messages.forEach(m => messagesMap[m.message.id] = m);

                    const currentMessageIds: Array<IMMessageId> = dialog.messages
                        .filter(m => m.messageState === IMMessageStatus.Sent)
                        .map(m => m.message.id);

                    const updateIds: Array<IMMessageId> = action.payload.messages.filter(m => {
                        return !~currentMessageIds.indexOf(m.message.id) || messagesMap[m.message.id].message.read !== m.message.read;
                    }).map(m => m.message.id);

                    if (updateIds.length) {
                        return {
                            ...orig,
                            dialogs: orig.dialogs.map((d) => {
                                if (d.id === action.payload.dialogId) {
                                    const sortedMessages: Array<IMDialogMessageState> = (() => {
                                        if (action.payload.messages.length > 1) {
                                            return R.sort((a: IMDialogMessageState, b: IMDialogMessageState) => {
                                                return a.message.createdAt.getTime() - b.message.createdAt.getTime();
                                            }, action.payload.messages);
                                        } else {
                                            return action.payload.messages;
                                        }
                                    })();

                                    const uniqueMessages: Array<IMDialogMessageState> = R.uniqBy((input: IMDialogMessageState) => {
                                        return input.message.id;
                                    }, [...sortedMessages, ...d.messages]);

                                    if (uniqueMessages.length > 0) {
                                        return {
                                            ...d,
                                            lastMessage: sortedMessages[sortedMessages.length - 1].message.text,
                                            lastMessageDate: sortedMessages[sortedMessages.length - 1].message.createdAt,
                                            messages: uniqueMessages,
                                        };
                                    } else {
                                        return {
                                            ...d,
                                            lastMessage: undefined,
                                            lastMessageDate: undefined,
                                            messages: uniqueMessages,
                                        };
                                    }
                                } else {
                                    return d;
                                }
                            }),
                        };
                    } else {
                        return orig;
                    }
                });

                break;

            case IMAction.SetMessageState:
                this.setState((orig) => {
                    return {
                        ...orig,
                        dialogs: orig.dialogs.map((d) => {
                            if (d.id === action.payload.dialogId) {
                                return {
                                    ...d,
                                };
                            } else {
                                return d;
                            }
                        }),
                    };
                });

                break;

            case IMAction.SetMessageAndState:
                this.setState((orig) => {
                    return {
                        ...orig,
                        dialogs: orig.dialogs.map((d) => {
                            if (d.id === action.payload.dialogId) {
                                return {
                                    ...d,
                                };
                            } else {
                                return d;
                            }
                        }),
                    };
                });

                break;

            case IMAction.RemoveMessage:
                this.setState((orig) => {
                    return {
                        ...orig,
                        dialogs: orig.dialogs.map((d) => {
                            if (d.id === action.payload.dialogId) {
                                return {
                                    ...d,
                                    messages: d.messages.filter(m => m.message.id !== action.payload.messageId),
                                };
                            } else {
                                return d;
                            }
                        }),
                    };
                });

                break;

            case IMAction.SendMessage:
                this.setState((orig) => {
                    return {
                        ...orig,
                        dialogs: orig.dialogs.map((d) => {
                            if (d.id === action.payload.dialogId) {
                                return {
                                    ...d,
                                    messages: [...d.messages, action.payload.message],
                                    queuedMessages: d.queuedMessages.slice(1),
                                };
                            } else {
                                return d;
                            }
                        }),
                    };
                });

                break;

            case IMAction.SendMessageError:
                this.setState((orig) => {
                    return {
                        ...orig,
                        dialogs: orig.dialogs.map((d) => {
                            if (d.id === action.payload.dialogId) {
                                return {
                                    ...d,
                                    queuedMessages: d.queuedMessages.map(value => {
                                        return {
                                            ...value,
                                            messageState: IMMessageStatus.Error,
                                        };
                                    }),
                                };
                            } else {
                                return d;
                            }
                        }),
                    };
                });

                break;

            case IMAction.SetHasMoreToLoadFlag:
                this.setState((orig) => {
                    return {
                        ...orig,
                        dialogs: orig.dialogs.map((d) => {
                            if (d.id === action.payload.dialogId) {
                                return {
                                    ...d,
                                    hasMoreToLoad: true,
                                };
                            } else {
                                return d;
                            }
                        }),
                    };
                });

                break;

            case IMAction.UnsetHasMoreToLoadFlag:
                this.setState((orig) => {
                    return {
                        ...orig,
                        dialogs: orig.dialogs.map((d) => {
                            if (d.id === action.payload.dialogId) {
                                return {
                                    ...d,
                                    hasMoreToLoad: false,
                                };
                            } else {
                                return d;
                            }
                        }),
                    };
                });

                break;


            case IMAction.CloseCurrentDialog:
                this.setState((orig) => {
                    return {
                        ...orig,
                        currentDialogId: undefined,
                    };
                });

                break;

            case IMAction.DropDialog:
                this.setState((orig) => {
                    return {
                        ...orig,
                        currentDialogId: undefined,
                        dialogs: orig.dialogs.filter(d => d.id !== action.payload.dialogId),
                    };
                });

                break;

            case IMAction.MarkAllMessagesAsRead:
                this.setState((orig) => {
                    return {
                        ...orig,
                        dialogs: orig.dialogs.map((d) => {
                            if (d.id === action.payload.dialogId) {
                                return <IMDialogState>{
                                    ...d,
                                    read: 1,
                                    hasMoreToLoad: false,
                                    messages: d.messages.map(m => {
                                        if (m.message.read === 0) {
                                            return {
                                                ...m,
                                                message: {
                                                    ...m.message,
                                                    read: 1,
                                                },
                                            };
                                        } else {
                                            return m;
                                        }
                                    }),
                                };
                            } else {
                                return d;
                            }
                        }),
                    };
                });

                break;

            case IMAction.SearchContacts:
                this.setState((orig) => {
                    return {
                        ...orig,
                        filteredDialogIds: orig.dialogs.filter((d) => !!~d.contact.name.toLocaleLowerCase().indexOf(action.payload.queryString.toLocaleLowerCase())).map(d => d.id),
                    };
                });

                break;

            case IMAction.CancelSearchContacts:
                this.setState((orig) => {
                    return {
                        ...orig,
                        filteredDialogIds: undefined,
                    };
                });

                break;

            case IMAction.AddMessageToQueue:
                this.setState((orig) => {
                    return {
                        ...orig,
                        dialogs: orig.dialogs.map((d) => {
                            if (d.id === action.payload.dialogId) {
                                return {
                                    ...d,
                                    queuedMessages: [...d.queuedMessages, action.payload.message],
                                };
                            } else {
                                return d;
                            }
                        }),
                    };
                });

                break;

            case IMAction.RemoveMessageFromQueue:
                this.setState((orig) => {
                    return {
                        ...orig,
                        dialogs: orig.dialogs.map((d) => {
                            if (d.id === action.payload.dialogId) {
                                return {
                                    ...d,
                                    queuedMessages: d.queuedMessages.filter(value => value !== action.payload.message),
                                };
                            } else {
                                return d;
                            }
                        }),
                    };
                });

                break;
        }
    }

    private setState(set$: (orig: IMPrevState) => IMNextState): void {
        this._current$.next({
            prev: this.snapshot,
            current: set$(this.snapshot),
        });
    }
}
