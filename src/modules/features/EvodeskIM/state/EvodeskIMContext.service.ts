import {Injectable} from '@angular/core';

import {EvodeskIMStateService} from './EvodeskIMState.service';
import {EvodeskIMSyncStateService} from './EvodeskIMSyncState.service';
import {EvodeskIMSearchService} from '../services/EvodeskIMSearch.service';
import {EvodeskIMBreakpointsService} from '../services/EvodeskIMBreakpoints.service';
import {EvodeskIMBootstrapService} from '../scripts/EvodeskIMBootstrap.service';
import {EvodeskIMSendMessageService} from '../services/EvodeskIMSendMessage.service';
import {EvodeskIMBodyParserService} from '../services/EvodeskIMBodyParser.service';

interface Context {
    imState: EvodeskIMStateService;
    imSyncState: EvodeskIMSyncStateService;
    imSearch: EvodeskIMSearchService;
    imBreakpoints: EvodeskIMBreakpointsService;
    imBootstrap: EvodeskIMBootstrapService;
    imSendMessage: EvodeskIMSendMessageService;
    imBodyParser: EvodeskIMBodyParserService;
}

interface Set$ {
    imState?: EvodeskIMStateService;
    imSyncState?: EvodeskIMSyncStateService;
    imSearch?: EvodeskIMSearchService;
    imBreakpoints: EvodeskIMBreakpointsService;
    imBootstrap?: EvodeskIMBootstrapService;
    imSendMessage?: EvodeskIMSendMessageService;
    imBodyParser?: EvodeskIMBodyParserService;
}

@Injectable()
export class EvodeskIMContextService
{
    private _context: Context | undefined;

    setContext(set$: Set$): void {
        this._context = { ...this._context, ...set$ };
    }

    get context(): Context {
        return this._context;
    }

    destroy(): void {
        if (this._context.imBreakpoints) {
            this._context.imBreakpoints.destroy();
        }

        if (this._context.imSyncState) {
            this._context.imSyncState.destroy();
        }

        this._context = undefined;
    }
}
