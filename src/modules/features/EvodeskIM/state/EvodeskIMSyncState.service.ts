import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';

import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {Subject} from 'rxjs/internal/Subject';
import {Observable} from 'rxjs/internal/Observable';

import {EvodeskAppMessageBusService} from '../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';

import {AppEvent} from '../../../app/EvodeskAppMessageBus/models/app-events.model';

export enum IMSyncErrorReason {
    CommonHttpError,
}

export enum IMSyncStatus
{
    NotReady = 'not-ready',
    Ready = 'ready',
    Bootstraping = 'bootstraping',
    Failed = 'failed',
    SyncMismatch = 'sync-mismatch',
    ChangesDetected = 'changes-detected',
}

export type IMSyncError =
    { reason: IMSyncErrorReason.CommonHttpError, payload: { httpResponse: HttpErrorResponse } }
;

@Injectable()
export class EvodeskIMSyncStateService
{
    private destroy$: Subject<void> = new Subject<void>();
    private _current$: BehaviorSubject<IMSyncStatus> = new BehaviorSubject<IMSyncStatus>(IMSyncStatus.NotReady);

    constructor(
        private appMessageBusService: EvodeskAppMessageBusService,
    ) {}

    get current$(): Observable<IMSyncStatus> {
        return this._current$.asObservable();
    }

    setStatus(newSyncStatus: IMSyncStatus): void {
        this._current$.next(newSyncStatus);
    }

    destroy(): void {
        this.destroy$.next(undefined);
    }

    dispatchCommonHttpError(httpError: HttpErrorResponse): void {
        this.setStatus(IMSyncStatus.SyncMismatch);

        this.dispatchSyncError({
            reason: IMSyncErrorReason.CommonHttpError,
            payload: {
                httpResponse: httpError,
            },
        });
    }

    dispatchSyncError(reason: IMSyncError): void {
        this.setStatus(IMSyncStatus.SyncMismatch);

        this.appMessageBusService.dispatch({ type: AppEvent.IMForceReload });
    }

    dispatchChangesDetected(): void {
        this.setStatus(IMSyncStatus.ChangesDetected);

        this.appMessageBusService.dispatch({ type: AppEvent.IMChangesDetected });
    }
}
