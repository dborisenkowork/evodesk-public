import {ProfileId} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {ViewMode} from '../../../../types/ViewMode.types';

export type IMDialogId = number;
export type IMContactId = ProfileId;
export type IMMessageId = number;

export {ViewMode as IMLayoutVariant};

export enum IMMessageStatus {
    Sent,
    Sending,
    Error,
}

export interface IMDialogContact {
    id: ProfileId;
    name: string;
    avatar: string | null;
}

export interface IMMessage {
    id: IMMessageId;
    dialogId: IMDialogId;
    text: string;
    createdAt: Date;
    read: 0 | 1;
    author: {
        id: ProfileId;
        avatar: string;
        name: string;
    };
}

export interface IMDialogState {
    id: IMDialogId;
    read: 0 | 1;
    name: string;
    messages: Array<IMDialogMessageState>;
    queuedMessages: Array<IMDialogMessageState>;
    contact: IMDialogContact;
    avatar: string | null;
    lastMessage: string;
    lastMessageDate: Date | undefined;
    hasMoreToLoad: boolean;
}

export interface IMDialogMessageState {
    message: IMMessage;
    messageState: IMMessageStatus;
}
