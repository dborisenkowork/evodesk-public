import {Injectable} from '@angular/core';

import {forkJoin, interval, Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskIMStateService, IMAction, IMBootstrap} from '../state/EvodeskIMState.service';
import {InstantMessagesRESTService} from '../../../app/EvodeskRESTApi/services/InstantMessagesREST.service';

import {imRESTMessageBackendDateStringToDate} from '../../../app/EvodeskRESTApi/models/im/IM.model';

interface Payload {
    bootstrap: IMBootstrap;
}

@Injectable()
export class EvodeskIMBootstrapService
{
    constructor(
        private imState: EvodeskIMStateService,
        private imRESTService: InstantMessagesRESTService,
    ) {}

    private updateInterval: number = 2000;

    bootstrap(): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        const payload: Payload = {
            bootstrap: {},
        };

        return Observable.create((done) => {
            forkJoin([
                this._fetchContacts(payload),
            ]).pipe(takeUntil(unsubscribe$)).subscribe(
                () => {
                    this.imState.dispatch({
                        type: IMAction.Bootstrap,
                        payload: {
                            bootstrap: payload.bootstrap,
                        },
                    });

                    done.next(undefined);
                    done.complete();
                },
                (queryError) => {
                    done.error(queryError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    subscribeDialogs(): void {
        const unsubscribe$: Subject<void> = new Subject<void>();

        const tick: Function = () => {
            this.imRESTService.getDialogs().pipe(takeUntil(unsubscribe$)).subscribe(
                (response) => {
                    this.imState.dispatch({
                        type: IMAction.UpdateDialog,
                        payload: {
                            dialogs: response.map((r) => {
                                return {
                                    id: r.group,
                                    contact: {
                                        id: r.id,
                                        avatar: r.avatar || r.avatar2,
                                        name: r.name || r.email,
                                    },
                                    name: r.name || r.email,
                                    lastMessage: r.text,
                                    lastMessageDate: imRESTMessageBackendDateStringToDate(r.creation_at.date),
                                    avatar: r.avatar || r.avatar2,
                                    read: r.read,
                                    messages: [],
                                    queuedMessages: [],
                                    hasMoreToLoad: true,
                                };
                            }),
                        },
                    });
                });
        };

        interval(this.updateInterval).pipe(takeUntil(unsubscribe$)).subscribe(() => {
            tick();
        });

        tick();
    }

    private _fetchContacts(payload: Payload): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create(o => {
            this.imRESTService.getDialogs().pipe(takeUntil(unsubscribe$)).subscribe(
                (response) => {
                    payload.bootstrap = {
                        ...payload.bootstrap,
                        dialogs: response.map((r) => {
                            return {
                                id: r.group,
                                contact: {
                                    id: r.id,
                                    avatar: r.avatar || r.avatar2,
                                    name: r.name || r.email,
                                },
                                name: r.name || r.email,
                                lastMessage: r.text,
                                lastMessageDate: imRESTMessageBackendDateStringToDate(r.creation_at.date),
                                avatar: r.avatar || r.avatar2,
                                read: r.read,
                                messages: [],
                                queuedMessages: [],
                                hasMoreToLoad: true,
                            };
                        }),
                    };

                    o.next(undefined);
                    o.complete();
                },
                (httpError) => {
                    o.error(httpError);
                },
            );
        });
    }
}
