import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {IMLayoutVariant} from '../../models/IM.models';

import {EvodeskIMStateService, IMAction} from '../../state/EvodeskIMState.service';
import {EvodeskIMSyncStateService} from '../../state/EvodeskIMSyncState.service';
import {EvodeskIMContextService} from '../../state/EvodeskIMContext.service';
import {EvodeskIMSearchService} from '../../services/EvodeskIMSearch.service';
import {EvodeskHeaderConfigurationService} from '../../../EvodeskHeader/services/EvodeskHeaderConfiguration.service';
import {EvodeskIMBreakpointsService} from '../../services/EvodeskIMBreakpoints.service';
import {EvodeskIMBootstrapService} from '../../scripts/EvodeskIMBootstrap.service';
import {EvodeskIMSendMessageService} from '../../services/EvodeskIMSendMessage.service';
import {EvodeskIMBodyParserService} from '../../services/EvodeskIMBodyParser.service';

interface State {
    ready: boolean;
    layout: IMLayoutVariant;
}

@Component({
    templateUrl: './IndexRoute.component.pug',
    styleUrls: [
        './IndexRoute.component.scss',
    ],
    providers: [
        EvodeskIMStateService,
        EvodeskIMSyncStateService,
        EvodeskIMSearchService,
        EvodeskIMBreakpointsService,
        EvodeskIMBootstrapService,
        EvodeskIMSendMessageService,
        EvodeskIMBodyParserService,
    ],
})
export class IndexRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        layout: IMLayoutVariant.Desktop,
    };

    constructor(
        private activatedRoute: ActivatedRoute,
        private evodeskHeaderConfig: EvodeskHeaderConfigurationService,
        private context: EvodeskIMContextService,
        private imState: EvodeskIMStateService,
        private imSyncState: EvodeskIMSyncStateService,
        private imSearchService: EvodeskIMSearchService,
        private imBreakpoints: EvodeskIMBreakpointsService,
        private imBootstrap: EvodeskIMBootstrapService,
        private imSendMessage: EvodeskIMSendMessageService,
        private imBodyParser: EvodeskIMBodyParserService,
    ) {}

    ngOnInit(): void {
        this.evodeskHeaderConfig.lockPanels(this.ngOnDestroy$);

        this.imBootstrap.bootstrap().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            setTimeout(() => {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            }, 1000);
        });

        this.imState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => ! s.prev || s.current.layoutVariant !== s.prev.layoutVariant),
        ).subscribe((s) => {
            this.imBootstrap.subscribeDialogs();

            this.state = {
                ...this.state,
                layout: s.current.layoutVariant,
            };
        });

        this.imSyncState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((current) => {
            this.imState.dispatch({
                type: IMAction.SetSyncStatus,
                payload: {
                    syncStatus: current,
                },
            });
        });

        this.activatedRoute.queryParams.pipe(takeUntil(this.ngOnDestroy$)).subscribe((qp) => {
            if (qp['dialog'] && ! isNaN(parseInt(qp['dialog'], 10))) {
                this.imState.dispatch({
                    type: IMAction.UseDialog,
                    payload: {
                        dialogId: parseInt(qp['dialog'], 10),
                    },
                });
            } else {
                this.imState.dispatch({
                    type: IMAction.UseDialog,
                    payload: {
                        dialogId: undefined,
                    },
                });
            }
        });

        this.imBreakpoints.init().pipe(takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            this.imState.dispatch({
                type: IMAction.SetLayout,
                payload: {
                    layout: next,
                },
            });
        });

        this.context.setContext({
            imState: this.imState,
            imSyncState: this.imSyncState,
            imSearch: this.imSearchService,
            imBreakpoints: this.imBreakpoints,
            imBootstrap: this.imBootstrap,
            imSendMessage: this.imSendMessage,
            imBodyParser: this.imBodyParser,
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
        this.context.destroy();
    }

    get containerCSSClasses(): any {
        return {
            '___mobile': this.state.layout === IMLayoutVariant.Mobile,
            '___tablet': this.state.layout === IMLayoutVariant.Tablet,
            '___desktop': this.state.layout === IMLayoutVariant.Desktop,
        };
    }
}
