import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskIMRouting} from './configs/EvodeskIMRouting.config';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskIMRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskIMRoutingModule
{}
