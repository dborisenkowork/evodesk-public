import {Pipe, PipeTransform} from '@angular/core';

import {IMDialogState} from '../models/IM.models';

export enum SortType {
    Recents = 'Recents',
    Favorites = 'Favorites',
}

@Pipe({
    name: 'DialogSort',
})
export class DialogSortPipe implements PipeTransform {
    transform(dialogs: Array<IMDialogState>, sortType: SortType): any {
        if (sortType) {
            switch (sortType) {
                case SortType.Recents:
                    return dialogs.sort((a: IMDialogState, b: IMDialogState) => {
                        return b.lastMessageDate.getTime() - a.lastMessageDate.getTime();
                    });
                case SortType.Favorites:
                    return dialogs;
            }
        } else {
            return dialogs.sort((a: IMDialogState, b: IMDialogState) => {
                return b.lastMessageDate.getTime() - a.lastMessageDate.getTime();
            });
        }
    }
}
