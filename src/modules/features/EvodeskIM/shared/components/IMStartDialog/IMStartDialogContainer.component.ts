import {ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialogRef} from '@angular/material';

import {distinctUntilChanged, filter, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs/internal/Subject';

import {IMStartDialogComponentSendEvent, imStartDialogFormBuilder} from './IMStartDialog.component';

import {ProfileModel} from '../../../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {IMMessage} from '../../../models/IM.models';
import {imRESTMessageBackendDateStringToDate} from '../../../../../app/EvodeskRESTApi/models/im/IM.model';

import {EvodeskAlertModalService} from '../../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {CurrentUserService} from '../../../../EvodeskAuth/services/CurrentUser.service';
import {InstantMessagesRESTService} from '../../../../../app/EvodeskRESTApi/services/InstantMessagesREST.service';
import {SearchRESTService, SearchResults} from '../../../../../app/EvodeskRESTApi/services/SearchREST.service';

interface State {
    ready: boolean;
    searchOffset: number;
    searchQuery: string;
    props?: {
        form: FormGroup;
        searchResults?: SearchResults | undefined;
    };
}

export interface EvodeskIMStartDialogSearchFormValue {
    queryString: string;
}

@Component({
    selector: 'evodesk-im-start-dialog-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-im-start-dialog [props]="state.props" (send)="onSend($event)" (close)="onClose()" (showMore)="onShowMore()" (addRecipient)="addRecipient($event)" (removeRecipient)="removeRecipient($event)">
          </evodesk-im-start-dialog>
        </ng-container>
    `,
})
export class IMStartDialogContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private nextSearch$: Subject<void> = new Subject<void>();

    public static MIN_LENGTH_TRIGGER: number = 3;
    public static MAX_LENGTH_TRIGGER: number = 255;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('success') successEvent: EventEmitter<IMMessage> = new EventEmitter<IMMessage>();

    public state: State = {
        ready: true,
        searchOffset: 0,
        searchQuery: '',
        props: {
            form: imStartDialogFormBuilder(this.fb),
        },
    };

    constructor(
        public dialogRef: MatDialogRef<IMStartDialogContainerComponent>,
        private fb: FormBuilder,
        private cdr: ChangeDetectorRef,
        private imRest: InstantMessagesRESTService,
        private uiAlertService: EvodeskAlertModalService,
        private currentUserService: CurrentUserService,
        private searchRESTService: SearchRESTService,
    ) {}

    ngOnInit(): void {
        this.state.props.form.valueChanges.pipe(
            takeUntil(this.ngOnDestroy$),
            distinctUntilChanged(),
            filter((formValue: EvodeskIMStartDialogSearchFormValue) => {
                return formValue.queryString.length === 0 ||
                    (formValue.queryString.length >= IMStartDialogContainerComponent.MIN_LENGTH_TRIGGER
                    && formValue.queryString.length <= IMStartDialogContainerComponent.MAX_LENGTH_TRIGGER);
            }),
        ).subscribe((formValue: EvodeskIMStartDialogSearchFormValue) => {
            this.state = {
                ...this.state,
                searchOffset: 0,
                searchQuery: formValue.queryString,
            };

            this.search();
        });

        this.state.props.form.valueChanges.pipe(
            takeUntil(this.ngOnDestroy$),
            distinctUntilChanged(),
            filter((formValue: EvodeskIMStartDialogSearchFormValue) => {
                return formValue.queryString.length === 0;
            }),
        ).subscribe((formValue: EvodeskIMStartDialogSearchFormValue) => {
            this.nextSearch$.next(undefined);

            this.state = {
                ...this.state,
                searchOffset: 0,
                searchQuery: '',
            };

            this.setSearchResults({ profiles: [] });
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onSend($event: IMStartDialogComponentSendEvent): void {
        this.imRest.startDialog({
            from: this.currentUserService.impersonatedAs.id,
            recipientIds: $event.formValue.recipients.map(p => p.id),
            message: $event.formValue.text,
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
            (message) => {
                this.successEvent.emit({
                    id: message.id,
                    createdAt: imRESTMessageBackendDateStringToDate(message.creation_at),
                    text: message.text,
                    dialogId: message.group,
                    read: 0,
                    author: {
                        id: this.currentUserService.impersonatedAs.id,
                        name: this.currentUserService.impersonatedAs.name || this.currentUserService.impersonatedAs.email,
                        avatar: this.currentUserService.impersonatedAs.avatar,
                    },
                });
            },
            () => {
                this.uiAlertService.httpError().pipe(takeUntil(this.ngOnDestroy$)).subscribe();
            },
        );
    }

    addRecipient(profile: ProfileModel): void {
        this.state.props.form.patchValue({recipients: [profile]});
        this.state.props.form.patchValue({queryString: ''});
    }

    removeRecipient(profile: ProfileModel): void {
        this.state.props.form.patchValue({
            recipients: this.state.props.form.value.recipients.filter(value => {
                return value.id !== profile.id;
            }),
        });
    }

    setSearchResults(results: SearchResults): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                searchResults: results,
            },
        };

        this.cdr.detectChanges();
    }

    onClose(): void {
        this.dialogRef.close();
    }

    onShowMore(): void {
        this.state = {
            ...this.state,
            searchOffset: this.state.searchOffset + 1,
        };

        this.search();
    }

    search(): void {
        this.nextSearch$.next(undefined);

        this.searchRESTService.search({ queryString: this.state.searchQuery, page: this.state.searchOffset }).pipe(takeUntil(this.nextSearch$)).subscribe((results) => {
            this.setSearchResults(this.state.searchOffset === 0 ? results : { profiles: [ ...this.state.props.searchResults.profiles, ...results.profiles ] });
        });
    }
}
