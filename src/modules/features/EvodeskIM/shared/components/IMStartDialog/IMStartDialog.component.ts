import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {ProfileModel} from '../../../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {evodeskIMMaxMessageLength, evodeskIMMaxSearchLength, evodeskIMMinMessageLength, evodeskIMMinSearchLength} from '../../../configs/EvodeskIM.constants';

import {SearchResults} from '../../../../../app/EvodeskRESTApi/services/SearchREST.service';

export interface IMStartDialogComponentProps
{
    form: FormGroup;
    searchResults?: SearchResults | undefined;
}

interface State {
    ready: boolean;
    maxTextLength: number;
    isSearchInputFocused: boolean;
}

export interface IMStartDialogFormValue {
    recipients: Array<ProfileModel>;
    text: string;
}

export interface IMStartDialogComponentSendEvent {
    formValue: IMStartDialogFormValue;
}

export function imStartDialogFormBuilder(fb: FormBuilder): FormGroup {
    return fb.group({
        recipients: [[], [Validators.required]],
        text: [[], [Validators.required, Validators.minLength(evodeskIMMinMessageLength), Validators.maxLength(evodeskIMMaxMessageLength)]],
        queryString: ['', [Validators.required, Validators.minLength(evodeskIMMinSearchLength), Validators.maxLength(evodeskIMMaxSearchLength)]],
    });
}

@Component({
    selector: 'evodesk-im-start-dialog',
    templateUrl: './IMStartDialog.component.pug',
    styleUrls: [
        './IMStartDialog.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IMStartDialogComponent implements OnChanges
{
    @Input() props: IMStartDialogComponentProps;

    @Output('addRecipient') addRecipient: EventEmitter<ProfileModel> = new EventEmitter<ProfileModel>();
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('send') sendEvent: EventEmitter<IMStartDialogComponentSendEvent> = new EventEmitter<IMStartDialogComponentSendEvent>();
    @Output('showMore') showMoreEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('removeRecipient') removeRecipient: EventEmitter<ProfileModel> = new EventEmitter<ProfileModel>();

    @ViewChild('input') inputRef: ElementRef;

    public state: State = {
        ready: false,
        maxTextLength: evodeskIMMaxMessageLength,
        isSearchInputFocused: true,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get textLength(): number {
        return !! this.props.form.value.text ? this.props.form.value.text.length : 0;
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    enter($event: KeyboardEvent): void {
        if ($event.ctrlKey && $event.keyCode === 13) {
            this.send();
        }
    }

    send(): void {
        this.sendEvent.emit({
            formValue: this.props.form.value,
        });
    }

    focusSearchInput(): void {
        this.inputRef.nativeElement.focus();
        this.onSearchInputFocus();
    }

    onSearchInputFocus(): void {
        this.state = {
            ...this.state,
            isSearchInputFocused: true,
        };
    }

    onSearchInputFocusOut(): void {
        this.state = {
            ...this.state,
            isSearchInputFocused: false,
        };
    }

    resetSearchInput(): void {
        this.state = {
            ...this.state,
            isSearchInputFocused: false,
        };
    }

    get hasSearchResults(): boolean {
        return !! this.props.searchResults && this.props.searchResults.profiles.length > 0;
    }
}
