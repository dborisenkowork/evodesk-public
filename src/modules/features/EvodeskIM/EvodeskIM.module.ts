import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskIMRoutingModule} from './EvodeskIMRouting.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';

import {IMComponent} from './components/IM/IM.component';
import {IMContainerComponent} from './components/IM/IMContainer.component';
import {IMContactsComponent} from './components/IMContacts/IMContacts.component';
import {IMContactsContainerComponent} from './components/IMContacts/IMContactsContainer.component';
import {IMDialogComponent} from './components/IMDialog/IMDialog.component';
import {IMDialogContainerComponent} from './components/IMDialog/IMDialogContainer.component';
import {IMFormComponent} from './components/IMForm/IMForm.component';
import {IMFormContainerComponent} from './components/IMForm/IMFormContainer.component';
import {IMMessageComponent} from './components/IMMessage/IMMessage.component';
import {IMMessageContainerComponent} from './components/IMMessage/IMMessageContainer.component';
import {IMStartDialogComponent} from './shared/components/IMStartDialog/IMStartDialog.component';
import {IMSearchComponent} from './components/IMSearch/IMSearch.component';
import {IMSearchContainerComponent} from './components/IMSearch/IMSearchContainer.component';

import {DialogSortPipe} from './pipes/DialogSort.pipe';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskIMRoutingModule,
    ],
    exports: [
        IMStartDialogComponent,
    ],
    declarations: [
        IndexRouteComponent,

        IMComponent,
        IMContainerComponent,
        IMContactsComponent,
        IMContactsContainerComponent,
        IMDialogComponent,
        IMDialogContainerComponent,
        IMFormComponent,
        IMFormContainerComponent,
        IMMessageComponent,
        IMMessageContainerComponent,
        IMSearchComponent,
        IMSearchContainerComponent,

        DialogSortPipe,
    ],
})
export class EvodeskIMModule
{}
