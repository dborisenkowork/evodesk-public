
import {of as observableOf, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';

import {EvodeskHeaderLayoutVariant} from '../models/EvodeskHeader.models';

import {EvodeskHeaderConfigurationService} from '../services/EvodeskHeaderConfiguration.service';

@Injectable()
export class EvodeskSetCommonHeaderGuard implements CanActivate
{
    constructor(
        private evodeskHeaderConfigurationService: EvodeskHeaderConfigurationService,
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        this.evodeskHeaderConfigurationService.setVariant(EvodeskHeaderLayoutVariant.Common);

        return observableOf(true);
    }
}
