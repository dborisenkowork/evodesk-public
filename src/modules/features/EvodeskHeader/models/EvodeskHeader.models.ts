import {ActivatedRoute, Router} from '@angular/router';

export type EvodeskHeaderAvailableScreens = '*' | 'mobile' | 'tablet' | 'desktop' | 'wide';

export enum EvodeskHeaderLayoutVariant {
    None = 'none',
    Common = 'common',
    Landing = 'landing',
    LandingNoBg = 'landing-no-bg',
}

interface RouterLink { type: 'routerLink'; routerLink: Array<string | number>; }
interface UrlLink { type: 'url'; url: string; }

export enum EvodeskHeaderCounter {
    IMMessages = 'im-messages',
}

export interface EvodeskHeaderSubMenuCounter {
    text: string;
}

export interface EvodeskHeaderMenuItem {
    id?: string;
    title: {
        text: string;
        shouldTranslate: boolean;
    };
    link:  RouterLink | UrlLink | undefined;
    available: Array<EvodeskHeaderAvailableScreens>;
    disabled?: boolean;
    customActive?: (activatedRoute: ActivatedRoute) => boolean;
}

export interface EvodeskHeaderSubMenuItem {
    title: {
        text: string;
        shouldTranslate: boolean;
    };
    link:  RouterLink | UrlLink | undefined;
    position: number;
    disabled?: boolean;
    customActive?: (activatedRoute: ActivatedRoute, router: Router) => boolean;
    counter?: EvodeskHeaderCounter;
    routerLinkOptions?: { exact: boolean };
}
