import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, HostListener, Input, Output} from '@angular/core';

import {EvodeskHeaderLayoutVariant} from '../../models/EvodeskHeader.models';

type MobileMenuVariant = 'default' | 'user';

export interface EvodeskHeaderComponentProps {
    variant: EvodeskHeaderLayoutVariant;
    isLocked: boolean;
    isSearchFormOpened: boolean;
    isMobileOpened: boolean;
    useAbsolutePositions: boolean;
    imMessages: number;
}

interface State {
    isMinified: boolean;
    hadTouchInteraction: boolean;
    touchStartY: number;
    scrolledPixelsUp: number;
    scrolledPixelsDown: number;
    variant: MobileMenuVariant;
}

function fHasOverlays(): boolean {
    return [
        '.cdk-overlay-backdrop',
        '.is-any-modal-opened',
    ].some((q) => !! document.querySelector(q));
}

@Component({
    selector: 'evodesk-header',
    templateUrl: './EvodeskHeader.component.pug',
    styleUrls: [
        './EvodeskHeader.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderComponent {
    public static TRESHOLD_SCROLL_DOWN: number = 200;
    public static TRESHOLD_SCROLL_UP: number = 80;

    @Input() props: EvodeskHeaderComponentProps;

    @Output('doLockPanels') doLockPanelsEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('doUnlockPanels') doUnlockPanelsEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('doOpenMobileMenu') doOpenMobileMenuEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('doCloseMobileMenu') doCloseMobileMenuEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('setMinified') setMinifiedEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    public state: State = {
        isMinified: false,
        hadTouchInteraction: false,
        touchStartY: 0,
        scrolledPixelsUp: 0,
        scrolledPixelsDown: 0,
        variant: 'default',
    };

    constructor(
        private cdr: ChangeDetectorRef,
    ) {}

    onMenuBarClick(): void {
        if (this.state.isMinified) {
            this.disableMinifiedMode();
        }
    }

    onMenuBarMouseOver(): void {
        if (this.state.isMinified) {
            this.disableMinifiedMode();
        }
    }

    @HostListener('document:mousewheel.out-zone', ['$event'])
    onMouseWheel($event: MouseWheelEvent): void {
        if (this.props.isLocked) {
            return;
        }

        const isMinified: boolean = this.state.isMinified;
        const isDocumentScrolledToTop: boolean = ((document.scrollingElement || document.documentElement).scrollTop === 0);

        if (! fHasOverlays()) {
            if (isMinified && isDocumentScrolledToTop) {
                const deltaY: number = $event.deltaY;

                if (deltaY > 0) {
                    this.state.scrolledPixelsUp = 0;
                } else {
                    this.state.scrolledPixelsUp += -deltaY;
                }

                if (this.state.scrolledPixelsUp >= EvodeskHeaderComponent.TRESHOLD_SCROLL_UP) {
                    this.disableMinifiedMode();
                }
            } else {
                const deltaY: number = $event.deltaY;

                if (deltaY < 0) {
                    this.state.scrolledPixelsDown = 0;
                } else {
                    this.state.scrolledPixelsDown += deltaY;
                }

                if (this.state.scrolledPixelsDown >= EvodeskHeaderComponent.TRESHOLD_SCROLL_UP) {
                    this.enableMinifiedMode();
                }
            }
        }
    }

    @HostListener('document:touchstart.out-zone', ['$event'])
    onTouchStart($event: TouchEvent): void {
        if (this.props.isLocked) {
            return;
        }

        this.state = {
            ...this.state,
            touchStartY: $event.touches[0].clientY,
        };
    }

    @HostListener('document:touchend.out-zone', ['$event'])
    onTouchEnd(): void {
        if (this.props.isLocked) {
            return;
        }

        this.state = {
            ...this.state,
            hadTouchInteraction: false,
        };
    }

    @HostListener('document:touchmove.out-zone', ['$event'])
    onTouchMove($event: TouchEvent): void {
        if (this.props.isLocked) {
            return;
        }

        if (! fHasOverlays()) {
            if (! this.state.hadTouchInteraction) {
                const deltaY: number = this.state.touchStartY - $event.touches[0].clientY;

                if (this.state.isMinified) {
                    this.state.scrolledPixelsUp += -deltaY;

                    if (this.state.scrolledPixelsUp > EvodeskHeaderComponent.TRESHOLD_SCROLL_UP) {
                        this.state = {
                            ...this.state,
                            hadTouchInteraction: true,
                        };

                        this.disableMinifiedMode();
                    }
                } else {
                    this.state.scrolledPixelsDown += deltaY;

                    if (this.state.scrolledPixelsDown > EvodeskHeaderComponent.TRESHOLD_SCROLL_DOWN) {
                        this.state = {
                            ...this.state,
                            hadTouchInteraction: true,
                        };

                        this.enableMinifiedMode();
                    }
                }
            }
        }
    }

    enableMinifiedMode(): void {
        this.state = {
            ...this.state,
            isMinified: true,
            scrolledPixelsUp: 0,
            scrolledPixelsDown: 0,
        };

        this.setMinifiedEvent.emit(true);

        (document.scrollingElement || document.documentElement).scrollTop += 80;

        this.cdr.detectChanges();
    }

    disableMinifiedMode(): void {
        this.state = {
            ...this.state,
            isMinified: false,
            scrolledPixelsUp: 0,
            scrolledPixelsDown: 0,
        };

        (document.scrollingElement || document.documentElement).scrollTop -= 80;

        this.setMinifiedEvent.emit(false);

        this.cdr.detectChanges();
    }

    openMobileMenu(): void {
        this.doOpenMobileMenuEvent.emit(undefined);
    }

    closeMobileMenu(): void {
        this.doCloseMobileMenuEvent.emit(undefined);
    }

    setMobileMenuVariant(variant: MobileMenuVariant): void {
        this.state = {
            ...this.state,
            variant: variant,
        };

        this.cdr.detectChanges();
    }
}
