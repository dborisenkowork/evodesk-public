import {ChangeDetectionStrategy, ChangeDetectorRef, Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

import {interval, Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {EVODESK_FEED_INJECT_PROVIDERS} from '../../../EvodeskFeed/EvodeskFeed.module';

import {EvodeskHeaderComponentProps} from './EvodeskHeader.component';

import {EvodeskHeaderStateService} from '../../services/EvodeskHeaderState.service';
import {EvodeskHeaderConfigurationService} from '../../services/EvodeskHeaderConfiguration.service';
import {InstantMessagesRESTService} from '../../../../app/EvodeskRESTApi/services/InstantMessagesREST.service';
import {AuthTokenService} from '../../../EvodeskAuth/services/AuthToken.service';

const updateIMMessagesCounterInterval: number = 10000;

interface State {
    ready: boolean;
    updateIMMessagesCounterInProgress: boolean;
    props?: EvodeskHeaderComponentProps;
}

@Component({
    selector: 'evodesk-header-container',
    template: `
        <ng-container *ngIf="!!state && state.ready">
          <evodesk-header
            [props]="state.props"
            (doOpenMobileMenu)="openMobileMenu()"
            (doCloseMobileMenu)="closeMobileMenu()"
            (doLockPanels)="lockPanels()"
            (doUnlockPanels)="unlockPanels()"
            (setMinified)="setMinified($event)"
          ></evodesk-header>
        </ng-container>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        ...EVODESK_FEED_INJECT_PROVIDERS,
    ],
})
export class EvodeskHeaderContainerComponent implements OnInit, OnDestroy
{
    private isDestroyed: boolean = false;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        updateIMMessagesCounterInProgress: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private router: Router,
        private ngZone: NgZone,
        private headerState: EvodeskHeaderStateService,
        private configService: EvodeskHeaderConfigurationService,
        private imREST: InstantMessagesRESTService,
        private authToken: AuthTokenService,
    ) {}

    ngOnInit(): void {
        this.router.events.pipe(takeUntil(this.ngOnDestroy$), filter(s => s instanceof NavigationEnd)).subscribe(() => {
            this.headerState.setState('Update', (orig) => {
                return {
                    ...orig,
                };
            });
        });

        this.headerState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    isLocked: s.current.isLocked || s.current.isMobileOpened,
                    variant: s.current.variant,
                    isSearchFormOpened: s.current.isSearchFormOpened,
                    isMobileOpened: s.current.isMobileOpened,
                    useAbsolutePositions: s.current.useAbsolutePositions,
                    imMessages: s.current.imMessages,
                },
            };

            this.cdr.detectChanges();
        });

        this.ngZone.runOutsideAngular(() => {
            interval(updateIMMessagesCounterInterval).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                if (this.authToken.has() && ! this.state.updateIMMessagesCounterInProgress) {
                    this.state = {
                        ...this.state,
                        updateIMMessagesCounterInProgress: true,
                    };

                    this.imREST.getNumUnreadMessages().pipe(takeUntil(this.ngOnDestroy$)).subscribe((num) => {

                        this.state = {
                            ...this.state,
                            updateIMMessagesCounterInProgress: false,
                        };

                        if (! this.isDestroyed && (! this.headerState.snapshot || this.headerState.snapshot.imMessages !== num)) {
                            this.headerState.setState('EvodeskHeaderContainerComponent.ngOnInit.imMessages', (orig) => {
                                return {
                                    ...orig,
                                    imMessages: num,
                                };
                            });

                            this.cdr.detectChanges();
                        }
                    });
                }
            });
        });
    }

    ngOnDestroy(): void {
        this.isDestroyed = true;

        this.ngOnDestroy$.next(undefined);
    }

    openMobileMenu(): void {
        this.headerState.setState('[EvodeskHeaderContainerComponent] Open mobile menu', (orig) => {
            return {
                ...orig,
                isMobileOpened: true,
            };
        });
    }

    closeMobileMenu(): void {
        this.headerState.setState('[EvodeskHeaderContainerComponent] Close mobile menu', (orig) => {
            return {
                ...orig,
                isMobileOpened: false,
            };
        });
    }

    setMinified(isMinified: boolean): void {
        this.headerState.setState('[EvodeskHeaderContainerComponent] Set minfied', (orig) => {
            return {
                ...orig,
                isMinified: isMinified,
            };
        });
    }

    lockPanels(): void {
        this.configService.lockPanels();
    }

    unlockPanels(): void {
        this.configService.unlockPanels();
    }
}
