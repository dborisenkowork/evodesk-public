import {ChangeDetectionStrategy, Component} from '@angular/core';

import {environment} from '../../../../../../environments/environment.config';

interface State {
    mobile: {
        [platform: string]: string;
    };
    social: {
        [network: string]: string;
    };
}

@Component({
    selector: 'evodesk-header-mobile-social-networks',
    templateUrl: './EvodeskHeaderMobileSocialNetworks.component.pug',
    styleUrls: [
        './EvodeskHeaderMobileSocialNetworks.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderMobileSocialNetworksComponent
{
    public state: State = {
        mobile: {
            ...environment.mobileAppLinks,
        },
        social: {
            ...environment.socialNetworkLinks,
        },
    };
}
