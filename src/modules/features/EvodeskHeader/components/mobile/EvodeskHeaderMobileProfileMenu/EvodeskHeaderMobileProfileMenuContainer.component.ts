import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {environment} from '../../../../../../environments/environment.config';

import {EvodeskHeaderMobileProfileMenuComponentProps} from './EvodeskHeaderMobileProfileMenu.component';

import {SignOutService} from '../../../../EvodeskAuth/services/SignOut.service';
import {CurrentUserService} from '../../../../EvodeskAuth/services/CurrentUser.service';
import {EvodeskHeaderStateService} from '../../../services/EvodeskHeaderState.service';
import {EvodeskBillingBalanceService} from '../../../../EvodeskBilling/services/EvodeskBillingBalance.service';

interface State {
    ready: boolean;
    props?: EvodeskHeaderMobileProfileMenuComponentProps;
}

@Component({
    selector: 'evodesk-header-mobile-profile-menu-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-header-mobile-profile-menu [props]="state.props" (close)="close()" (signOut)="signOut()"></evodesk-header-mobile-profile-menu>
      </ng-container>
    `,
})
export class EvodeskHeaderMobileProfileMenuContainerComponent implements OnInit, OnDestroy
{
    @Input() disableContextMenu: boolean = false;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private router: Router,
        private evodeskHeaderStateService: EvodeskHeaderStateService,
        private currentUserService: CurrentUserService,
        private signOutService: SignOutService,
        private billingBalanceService: EvodeskBillingBalanceService,
    ) {}

    ngOnInit(): void {
        this.router.events.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(e => <any>e instanceof NavigationEnd),
        ).subscribe(() => this.close());

        const tryReady: Function = () => {
            if (this.state.props && this.state.props.avatar && this.state.props.balance) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            }
        };

        this.currentUserService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            if (next) {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        avatar: next.avatar ? `${environment.modules.EvoDeskRESTApi.apiEndpoint}/profile/${next.id}/avatar` : undefined,
                        name: next.email,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }

            tryReady();
        });

        this.billingBalanceService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    balance: {
                        evocoin: next.balance.evocoin.toFixed(2),
                        points: next.balance.points.toFixed(0),
                        rub: next.balance.rub.toFixed(2),
                    },
                },
            };

            tryReady();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    close(): void {
        this.evodeskHeaderStateService.setState('[EvodeskHeaderMobileProfileMenuContainerComponent] Close', (orig) => {
            return {
                ...orig,
                isMobileOpened: false,
            };
        });
    }

    signOut(): void {
        this.signOutService.signOut();
    }
}
