import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

type PointsVariants = 'F1' | 'F234' | 'F056789D';

enum Screen {
    Menu = 'menu',
    Balance = 'balance',
}

export interface EvodeskHeaderMobileProfileMenuComponentProps {
    avatar: string | undefined;
    name: string;
    balance: {
        points: string;
        rub: string;
        evocoin: string;
    };
}

interface State {
    ready: boolean;
    screen: Screen;
    pointsUseVariant: string;
}

@Component({
    selector: 'evodesk-header-mobile-profile-menu',
    templateUrl: './EvodeskHeaderMobileProfileMenu.component.pug',
    styleUrls: [
        './EvodeskHeaderMobileProfileMenu.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderMobileProfileMenuComponent implements OnChanges
{
    public static DEFAULT_POINTS_VARIANT: PointsVariants = 'F056789D';

    @Input() props: EvodeskHeaderMobileProfileMenuComponentProps;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('signOut') signOutEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        screen: Screen.Menu,
        pointsUseVariant: EvodeskHeaderMobileProfileMenuComponent.DEFAULT_POINTS_VARIANT,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    pointsUseVariant: (() => {
                        if (this.props.balance.points && (typeof <any>this.props.balance.points === 'string') && this.props.balance.points.length > 0) {
                            const asInt: number = parseInt(this.props.balance.points, 10);
                            const lastDigit: number = parseInt(asInt.toString().split('').pop(), 10);

                            if (asInt >= 10 && asInt <= 19) {
                                return 'F056789D';
                            } else {
                                if (lastDigit === 1) {
                                    return 'F1';
                                } else if (lastDigit >= 2 && lastDigit <= 4) {
                                    return 'F234';
                                } else {
                                    return 'F056789D';
                                }
                            }
                        } else {
                            return EvodeskHeaderMobileProfileMenuComponent.DEFAULT_POINTS_VARIANT;
                        }
                    })(),
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    signOut(): void {
        this.signOutEvent.emit(undefined);
    }

    openBalance(): void {
        this.state = {
            ...this.state,
            screen: Screen.Balance,
        };
    }
}
