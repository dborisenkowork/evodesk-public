import {Component, OnDestroy, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';

import {EvodeskHeaderMobileLanguagesComponentProps} from './EvodeskHeaderMobileLanguages.component';

import {environment} from '../../../../../../environments/environment.config';

interface State {
    ready: boolean;
    props?: EvodeskHeaderMobileLanguagesComponentProps;
}

@Component({
    selector: 'evodesk-header-mobile-languages-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-header-mobile-languages [props]="state.props" (select)="doChangeLanguage($event)"></evodesk-header-mobile-languages>
      </ng-container>
    `,
})
export class EvodeskHeaderMobileLanguagesContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private translate: TranslateService,
    ) {}

    ngOnInit(): void {
        this.state = {
            ...this.state,
            ready: true,
            props: {
                currentLocale: this.translate.getDefaultLang(),
                availableLocales: [...environment.availableLocales],
            },
        };
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    doChangeLanguage(newLang: string): void {
        this.translate.setDefaultLang(newLang);

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                currentLocale: newLang,
            },
        };
    }
}
