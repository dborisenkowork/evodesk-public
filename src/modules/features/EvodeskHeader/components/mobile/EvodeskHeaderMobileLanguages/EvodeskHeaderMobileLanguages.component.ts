import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

export interface EvodeskHeaderMobileLanguagesComponentProps {
    currentLocale: string;
    availableLocales: Array<string>;
}

interface State {
    ready: boolean;
    areOptionsOpened: boolean;
}

@Component({
    selector: 'evodesk-header-mobile-languages',
    templateUrl: './EvodeskHeaderMobileLanguages.component.pug',
    styleUrls: [
        './EvodeskHeaderMobileLanguages.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderMobileLanguagesComponent implements OnChanges
{
    @Input() props: EvodeskHeaderMobileLanguagesComponentProps;

    @Output('select') selectEvent: EventEmitter<string> = new EventEmitter<string>();

    public state: State = {
        ready: false,
        areOptionsOpened: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    select(locale: string): void {
        this.closeOptions();
        this.selectEvent.emit(locale);
    }

    toggleOptions(): void {
        this.state = {
            ...this.state,
            areOptionsOpened: ! this.state.areOptionsOpened,
        };
    }

    openOptions(): void {
        this.state = {
            ...this.state,
            areOptionsOpened: true,
        };
    }

    closeOptions(): void {
        this.state = {
            ...this.state,
            areOptionsOpened: false,
        };
    }
}
