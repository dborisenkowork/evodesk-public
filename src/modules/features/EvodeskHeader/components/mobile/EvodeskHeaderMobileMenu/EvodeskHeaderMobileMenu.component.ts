import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

import {EvodeskHeaderAvailableScreens, EvodeskHeaderMenuItem} from '../../../models/EvodeskHeader.models';

export interface EvodeskHeaderMobileMenuComponentProps {
    menuItems: Array<EvodeskHeaderMenuItem>;
}

@Component({
    selector: 'evodesk-header-mobile-menu',
    templateUrl: './EvodeskHeaderMobileMenu.component.pug',
    styleUrls: [
        './EvodeskHeaderMobileMenu.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderMobileMenuComponent
{
    @Input() props: EvodeskHeaderMobileMenuComponentProps;
    @Input() filter: Array<EvodeskHeaderAvailableScreens> = ['*', 'mobile'];

    @Output('itemClick') itemClickEvent: EventEmitter<EvodeskHeaderMenuItem> = new EventEmitter<EvodeskHeaderMenuItem>();

    get menuItems(): Array<EvodeskHeaderMenuItem> {
        return this.props.menuItems
            .filter(menuItem => ! menuItem.disabled)
            .filter(menuItem => {
                return menuItem.available.some((a) => !!~this.filter.indexOf(a));
            });
    }

    itemClick(item: EvodeskHeaderMenuItem): void {
        this.itemClickEvent.emit(item);
    }
}
