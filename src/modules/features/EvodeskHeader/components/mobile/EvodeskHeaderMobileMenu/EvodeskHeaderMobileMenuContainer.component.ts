import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskHeaderMobileMenuComponentProps} from './EvodeskHeaderMobileMenu.component';

import {EvodeskHeaderMenuItem} from '../../../models/EvodeskHeader.models';
import {AppEvent} from '../../../../../app/EvodeskAppMessageBus/models/app-events.model';

import {EvodeskHeaderStateService} from '../../../services/EvodeskHeaderState.service';
import {EvodeskAppMessageBusService} from '../../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';

interface State {
    ready: boolean;
    props?: EvodeskHeaderMobileMenuComponentProps;
}

@Component({
    selector: 'evodesk-header-mobile-menu-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-header-mobile-menu [props]="state.props" (itemClick)="onMenuItemClick($event)"></evodesk-header-mobile-menu>
      </ng-container>
    `,
})
export class EvodeskHeaderMobileMenuContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    @Output('menuItemClick') menuItemClickEvent: EventEmitter<EvodeskHeaderMenuItem> = new EventEmitter<EvodeskHeaderMenuItem>();

    public state: State = {
        ready: false,
    };

    constructor(
        private appBus: EvodeskAppMessageBusService,
        private evodeskHeaderStateService: EvodeskHeaderStateService,
    ) {}

    ngOnInit(): void {
        this.evodeskHeaderStateService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    menuItems: s.current.menuItems,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onMenuItemClick(menuItem: EvodeskHeaderMenuItem): void {
        this.menuItemClickEvent.emit(menuItem);
        this.appBus.dispatch({ type: AppEvent.HeaderMobileClickItem, payload: menuItem });
    }
}
