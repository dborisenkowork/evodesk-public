import {ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder} from '@angular/forms';

import {Subject} from 'rxjs';
import {filter, distinctUntilChanged, takeUntil} from 'rxjs/operators';

import {evodeskHeaderCommonSearchComponentFormBuilder, EvodeskHeaderCommonSearchComponentFormValue, EvodeskHeaderCommonSearchComponentProps} from './EvodeskHeaderCommonSearch.component';

import {EvodeskHeaderStateService} from '../../../services/EvodeskHeaderState.service';
import {SearchRESTService, SearchResults} from '../../../../../app/EvodeskRESTApi/services/SearchREST.service';

interface State {
    ready: boolean;
    props: EvodeskHeaderCommonSearchComponentProps;
}

@Component({
    selector: 'evodesk-header-common-search-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-header-common-search #view [props]="state.props" (searchFormEnabledChanged)="searchFormEnabledChanged($event)"></evodesk-header-common-search>
      </ng-container>
    `,
})
export class EvodeskHeaderCommonSearchContainerComponent implements OnInit, OnDestroy
{
    public static MIN_LENGTH_TRIGGER: number = 3;
    public static MAX_LENGTH_TRIGGER: number = 255;

    private nextSearch$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        props: {
            searchForm: evodeskHeaderCommonSearchComponentFormBuilder(this.fb),
        },
    };

    @Output('onSearchFormToggle') onSearchFormToggleEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(
        private fb: FormBuilder,
        private cdr: ChangeDetectorRef,
        private evodeskHeaderStateService: EvodeskHeaderStateService,
        private searchRESTService: SearchRESTService,
    ) {}

    ngOnInit(): void {
        this.state.props.searchForm.valueChanges.pipe(
            takeUntil(this.ngOnDestroy$),
            distinctUntilChanged(),
            filter((formValue: EvodeskHeaderCommonSearchComponentFormValue) => {
                return formValue.query.length >= EvodeskHeaderCommonSearchContainerComponent.MIN_LENGTH_TRIGGER
                    && formValue.query.length <= EvodeskHeaderCommonSearchContainerComponent.MAX_LENGTH_TRIGGER;
            }),
        ).subscribe((formValue: EvodeskHeaderCommonSearchComponentFormValue) => {
            this.nextSearch$.next(undefined);

            this.searchRESTService.search({ queryString: formValue.query }).pipe(takeUntil(this.nextSearch$)).subscribe((results) => {
                this.setSearchResults(results);
            });
        });
    }

    ngOnDestroy(): void {
        this.nextSearch$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    searchFormEnabledChanged(isOpened: boolean): void {
        this.evodeskHeaderStateService.setState('[EvodeskHeaderCommonSearchContainerComponent] searchFormEnabledChanged', (orig) => {
            this.onSearchFormToggleEvent.emit(isOpened);

            if (isOpened) {
                this.cdr.detectChanges();

                return {
                    ...orig,
                    isSearchFormOpened: isOpened,
                };
            } else {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        searchResults: undefined,
                    },
                };

                this.cdr.detectChanges();

                return {
                    ...orig,
                    isSearchFormOpened: isOpened,
                };
            }
        });
    }

    setSearchResults(results: SearchResults): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                searchResults: results,
            },
        };

        this.cdr.detectChanges();
    }
}
