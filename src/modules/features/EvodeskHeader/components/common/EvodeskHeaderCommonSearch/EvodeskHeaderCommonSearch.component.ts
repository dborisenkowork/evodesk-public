import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {SearchResults} from '../../../../../app/EvodeskRESTApi/services/SearchREST.service';

interface State {
    ready: boolean;
    isSearchFormEnabled: boolean;
}

export interface EvodeskHeaderCommonSearchComponentFormValue {
    query: string;
}

export interface EvodeskHeaderCommonSearchComponentProps {
    searchForm: FormGroup;
    searchResults?: SearchResults | undefined;
}

export function evodeskHeaderCommonSearchComponentFormBuilder(fb: FormBuilder): FormGroup {
    return fb.group({
        query: [''],
    });
}

@Component({
    selector: 'evodesk-header-common-search',
    templateUrl: './EvodeskHeaderCommonSearch.component.pug',
    styleUrls: [
        './EvodeskHeaderCommonSearch.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderCommonSearchComponent implements OnChanges
{
    @ViewChild('searchInput') searchInputRef: ElementRef;

    @Input() props: EvodeskHeaderCommonSearchComponentProps;

    @Output() searchFormEnabledChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

    private nextTimeOut: any;

    public state: State = {
        ready: false,
        isSearchFormEnabled: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }

        this.cdr.detectChanges();
    }

    get hasSearchResults(): boolean {
        return this.state.isSearchFormEnabled && !! this.props.searchResults && this.props.searchResults.profiles.length > 0;
    }

    onSearchInputKeyEscape(): void {
        this.closeSearchForm();
    }

    onClickOutside(): void {
        this.closeSearchForm();
    }

    onSearchInputKeyDown($event: KeyboardEvent): void {
        if (!!~[38, 40].indexOf($event.keyCode || $event.which)) {
            $event.preventDefault();
        }
    }

    toggleSearchForm($event?: MouseEvent | TouchEvent): void {
        if ($event) {
            $event.stopPropagation();
        }

        this.state = {
            ...this.state,
            isSearchFormEnabled: ! this.state.isSearchFormEnabled,
        };

        setTimeout(() => {
            if (this.searchInputRef && this.state.isSearchFormEnabled) {
                (this.searchInputRef.nativeElement as HTMLElement).focus();
            }
        });

        this.searchFormEnabledChanged.emit(this.state.isSearchFormEnabled);
    }

    openSearchForm(): void {
        this.state = {
            ...this.state,
            isSearchFormEnabled: true,
        };

        if (this.nextTimeOut) {
            window.clearTimeout(this.nextTimeOut);
        }

        setTimeout(() => {
            if (this.searchInputRef) {
                (this.searchInputRef.nativeElement as HTMLElement).focus();
            }
        });

        this.searchFormEnabledChanged.emit(this.state.isSearchFormEnabled);
    }

    closeSearchForm($event?: MouseEvent | TouchEvent): void {
        if ($event) {
            $event.stopPropagation();
        }

        this.nextTimeOut = setTimeout(() => {
            if (this.props && this.props.searchForm) {
                this.props.searchForm.setValue({
                    query: '',
                });
            }
        }, 200);

        this.state = {
            ...this.state,
            isSearchFormEnabled: false,
        };

        this.searchFormEnabledChanged.emit(this.state.isSearchFormEnabled);
    }
}
