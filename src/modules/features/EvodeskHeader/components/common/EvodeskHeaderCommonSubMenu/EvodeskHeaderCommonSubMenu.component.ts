import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {EvodeskHeaderSubMenuItem} from '../../../models/EvodeskHeader.models';

import {CurrentUserService} from '../../../../EvodeskAuth/services/CurrentUser.service';

export interface EvodeskHeaderCommonSubMenuContainerComponentProps {
    menuItems: Array<EvodeskHeaderSubMenuItem>;
    imMessages: number;
}

interface State {
    ready: boolean;
    shouldBeVisible: boolean;
    matTabBodies: Array<{ position: string, items: Array<EvodeskHeaderSubMenuItem> }>;
}

@Component({
    selector: 'evodesk-header-common-sub-menu',
    templateUrl: './EvodeskHeaderCommonSubMenu.component.pug',
    styleUrls: [
        './EvodeskHeaderCommonSubMenu.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderCommonSubMenuComponent implements OnChanges
{
    @Input() props: EvodeskHeaderCommonSubMenuContainerComponentProps;

    public state: State = {
        ready: false,
        shouldBeVisible: false,
        matTabBodies: [],
    };

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private currentUser: CurrentUserService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    shouldBeVisible: this.props.menuItems.length > 0,
                    matTabBodies: [
                        { position: 'left', items: this.menuItemsLeft },
                        { position: 'center', items: this.menuItemsCenter },
                        { position: 'right', items: this.menuItemRight },
                    ],
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    getNgClasses(item: EvodeskHeaderSubMenuItem): any {
        const ngClasses: any = {};

        if (item.customActive) {
            ngClasses['custom-active'] = item.customActive(this.activatedRoute, this.router);
        } else {
            if (item.link) {
                let linkAsStr: string;

                if (item.link.type === 'routerLink') {
                    linkAsStr = item.link.routerLink[0].toString();
                } else if (item.link.type === 'url') {
                    linkAsStr = item.link.url;
                }

                if (linkAsStr.indexOf('profile') >= 0) {
                    this.currentUser.current$.subscribe(value => {
                        ngClasses['custom-active'] = this.router.url.indexOf(`profile/${value.id}`) >= 0;
                    });
                }
            }
        }

        return ngClasses;
    }

    get menuItemsLeft(): Array<EvodeskHeaderSubMenuItem> {
        return this.props.menuItems
            .filter(i => ! i.disabled)
            .filter(i => i.position > 0);
    }

    get menuItemsCenter(): Array<EvodeskHeaderSubMenuItem> {
        return this.props.menuItems
            .filter(i => ! i.disabled)
            .filter(i => i.position === 0);
    }

    get menuItemRight(): Array<EvodeskHeaderSubMenuItem> {
        return this.props.menuItems
            .filter(i => ! i.disabled)
            .filter(i => i.position < 0);
    }
}
