import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskHeaderCommonSubMenuContainerComponentProps} from './EvodeskHeaderCommonSubMenu.component';

import {EvodeskHeaderStateService} from '../../../services/EvodeskHeaderState.service';

interface State {
    ready: boolean;
    props?: EvodeskHeaderCommonSubMenuContainerComponentProps;
}

@Component({
    selector: 'evodesk-header-common-sub-menu-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-header-common-sub-menu [props]="state.props"></evodesk-header-common-sub-menu>
      </ng-container>
    `,
})
export class EvodeskHeaderCommonSubMenuContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private evodeskHeaderStateService: EvodeskHeaderStateService,
    ) {}

    ngOnInit(): void {
        this.evodeskHeaderStateService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    menuItems: s.current.subMenuItems,
                    imMessages: s.current.imMessages,
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
