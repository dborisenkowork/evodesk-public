import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
    selector: 'evodesk-header-common-logo',
    templateUrl: './EvodeskHeaderCommonLogo.component.html',
    styleUrls: [
        './EvodeskHeaderCommonLogo.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderCommonLogoComponent
{}
