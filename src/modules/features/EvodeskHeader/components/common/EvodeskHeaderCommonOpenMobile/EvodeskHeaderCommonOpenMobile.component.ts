import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

export interface EvodeskHeaderCommonOpenMobileComponentProps {
    isOpened: boolean;
}

@Component({
    selector: 'evodesk-header-common-open-mobile',
    templateUrl: './EvodeskHeaderCommonOpenMobile.component.pug',
    styleUrls: [
        './EvodeskHeaderCommonOpenMobile.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderCommonOpenMobileComponent
{
    @Input() props: EvodeskHeaderCommonOpenMobileComponentProps;

    @Output('toggleMobile') toggleMobileEvent: EventEmitter<void> = new EventEmitter<void>();

    toggleMobile(): void {
        this.toggleMobileEvent.emit(undefined);
    }
}
