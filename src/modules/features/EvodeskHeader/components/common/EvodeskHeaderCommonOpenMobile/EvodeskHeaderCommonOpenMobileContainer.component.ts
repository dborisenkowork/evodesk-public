import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskHeaderCommonOpenMobileComponentProps} from './EvodeskHeaderCommonOpenMobile.component';

import {EvodeskHeaderStateService} from '../../../services/EvodeskHeaderState.service';

interface State {
    props: EvodeskHeaderCommonOpenMobileComponentProps;
}

@Component({
    selector: 'evodesk-header-common-open-mobile-container',
    template: `<evodesk-header-common-open-mobile [props]="state.props" (toggleMobile)="toggleMobileMenu()"></evodesk-header-common-open-mobile>`,
})
export class EvodeskHeaderCommonOpenMobileContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        props: {
            isOpened: false,
        },
    };

    constructor(
        private evodeskHeaderStateService: EvodeskHeaderStateService,
    ) {}

    ngOnInit(): void {
        this.evodeskHeaderStateService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(s => {
            this.state = {
                ...this.state,
                props: {
                    isOpened: s.current.isMobileOpened,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    toggleMobileMenu(): void {
        this.evodeskHeaderStateService.setState('[EvodeskHeaderCommonOpenMobileContainerComponent] Toggle mobile menu', (orig) => {
            return {
                ...orig,
                isMobileOpened: ! orig.isMobileOpened,
            };
        });
    }
}
