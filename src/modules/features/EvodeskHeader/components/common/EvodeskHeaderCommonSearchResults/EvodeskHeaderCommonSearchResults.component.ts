import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, HostListener, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {SearchResults} from '../../../../../app/EvodeskRESTApi/services/SearchREST.service';
import {ProfileModel} from '../../../../../app/EvodeskRESTApi/models/profile/Profile.model';

type CurrentActive = { type: 'profile'; profile: ProfileModel; };

export interface EvodeskHeaderCommonSearchResultsComponentProps {
    searchResults: SearchResults;
}

interface State {
    ready: boolean;
    active?: CurrentActive | undefined;
    activeMap?: Array<CurrentActive>;
}

@Component({
    selector: 'evodesk-header-common-search-results',
    templateUrl: './EvodeskHeaderCommonSearchResults.component.pug',
    styleUrls: [
        './EvodeskHeaderCommonSearchResults.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderCommonSearchResultsComponent implements OnChanges
{
    @Input() props: EvodeskHeaderCommonSearchResultsComponentProps;

    @Output('selectProfile') selectProfileEvent: EventEmitter<ProfileModel> = new EventEmitter<ProfileModel>();

    private state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            this.state = {
                ...this.state,
                ready: true,
                active: undefined,
                activeMap: (() => {
                    const activeMap: Array<CurrentActive> = [];

                    if (this.props.searchResults && Array.isArray(this.props.searchResults.profiles)) {
                        this.props.searchResults.profiles.forEach(p => {
                            activeMap.push({
                                type: 'profile',
                                profile: p,
                            });
                        });
                    }

                    return activeMap;
                })(),
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    @HostListener('document:keydown', ['$event'])
    onDocumentKeyUp($event: KeyboardEvent): void {
        if (($event.keyCode || $event.which) === 38) {
            $event.preventDefault();
            $event.stopPropagation();

            this.up();
        }

        if (($event.keyCode || $event.which) === 40) {
            $event.preventDefault();
            $event.stopPropagation();

            this.down();
        }

        if (($event.keyCode || $event.which) === 13) {
            $event.preventDefault();
            $event.stopPropagation();

            this.enter();
        }
    }

    isProfileActive(profile: ProfileModel): boolean {
        return this.state.active
            && this.state.active.type === 'profile'
            && this.state.active.profile === profile;
    }

    selectProfile(profile: ProfileModel): void {
        this.selectProfileEvent.emit(profile);
    }

    setActiveProfile(profile: ProfileModel): void {
        const active: CurrentActive = this.state.activeMap.filter(a => a.type === 'profile' && a.profile === profile)[0];

        if (active) {
            this.state = {
                ...this.state,
                active: this.state.activeMap[this.state.activeMap.indexOf(active)],
            };
        }
    }

    up(): void {
        const activeMap: Array<CurrentActive> = this.state.activeMap;

        if (this.state.active) {
            const index: number = activeMap.indexOf(this.state.active);

            if (index > 0) {
                this.state = {
                    ...this.state,
                    active: activeMap[index - 1],
                };
            } else {
                this.state = {
                    ...this.state,
                    active: activeMap[activeMap.length - 1],
                };
            }
        } else {
            this.state = {
                ...this.state,
                active: activeMap[activeMap.length - 1],
            };
        }

        this.cdr.detectChanges();
    }

    down(): void {
        const activeMap: Array<CurrentActive> = this.state.activeMap;

        if (this.state.active) {
            const index: number = activeMap.indexOf(this.state.active);

            if (index > -1 && index < (this.state.activeMap.length - 1)) {
                this.state = {
                    ...this.state,
                    active: activeMap[index + 1],
                };
            } else {
                this.state = {
                    ...this.state,
                    active: activeMap[0],
                };
            }
        } else {
            this.state = {
                ...this.state,
                active: activeMap[0],
            };
        }

        this.cdr.detectChanges();
    }

    enter(): void {
        if (this.state.active) {
            if (this.state.active.type === 'profile') {
                setTimeout(() => {
                    this.selectProfile(this.state.active.profile);
                });
            }
        }
    }
}
