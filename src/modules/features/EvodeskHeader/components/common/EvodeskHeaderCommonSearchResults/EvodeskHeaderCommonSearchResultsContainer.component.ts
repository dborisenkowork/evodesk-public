import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {Router} from '@angular/router';

import {EvodeskHeaderCommonSearchResultsComponentProps} from './EvodeskHeaderCommonSearchResults.component';

import {SearchResults} from '../../../../../app/EvodeskRESTApi/services/SearchREST.service';

import {ProfileModel} from '../../../../../app/EvodeskRESTApi/models/profile/Profile.model';

interface State {
    ready: boolean;
    props?: EvodeskHeaderCommonSearchResultsComponentProps;
}

@Component({
    selector: 'evodesk-header-common-search-results-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-header-common-search-results [props]="state.props" (selectProfile)="onSelectProfile($event)"></evodesk-header-common-search-results>
      </ng-container>
    `,
})
export class EvodeskHeaderCommonSearchResultsContainerComponent implements OnChanges
{
    @Input() searchResults: SearchResults;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private router: Router,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['searchResults']) {
            if (this.searchResults) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        searchResults: this.searchResults,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }

            this.cdr.detectChanges();
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    onSelectProfile(profile: ProfileModel): void {
        this.router.navigate(['/profile', profile.id]);

        this.close();
    }
}
