import {ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import * as moment from 'moment';

import {environment} from '../../../../../../environments/environment.config';

import {EvocoinCreateCheckFormValue, EvodeskHeaderCommonUserBalanceComponent, EvodeskHeaderCommonUserBalanceComponentProps, RublesReplenishFormValue, RublesWithdrawFormValue} from './EvodeskHeaderCommonUserBalance.component';

import {EvodeskHeaderConfigurationService} from '../../../services/EvodeskHeaderConfiguration.service';
import {EvodeskHeaderStateService} from '../../../services/EvodeskHeaderState.service';
import {EvodeskAlertModalService} from '../../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {BillingCreateEvoCheckResponse200, BillingRESTService, BillingWithdrawResponse200, PayeerBuyResponse200} from '../../../../../app/EvodeskRESTApi/services/BillingREST.service';
import {CurrentUserService} from '../../../../EvodeskAuth/services/CurrentUser.service';
import {EvodeskAppStateService} from '../../../../../app/EvodeskAppState/services/EvodeskAppState.service';
import {EvodeskConfirmModalService} from '../../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {EvodeskEvoChecksService} from '../../../../EvodeskBilling/services/EvodeskEvoChecks.service';
import {EvodeskBillingBalanceService} from '../../../../EvodeskBilling/services/EvodeskBillingBalance.service';
import {EvodeskExchangeRatesService} from '../../../../EvodeskBilling/services/EvodeskExchangeRates.service';
import {EvoCheckModel} from '../../../../../app/EvodeskRESTApi/models/billing/EvoCheck.model';

let payResultsProcessed: boolean = false;

interface State {
    ready: boolean;
    props?: EvodeskHeaderCommonUserBalanceComponentProps;
    sendRequest?: undefined | PayeerBuyResponse200;
    payeer: {
        sendUrl: string;
        mSHop: string;
    };
}

@Component({
    selector: 'evodesk-header-common-user-balance-container',
    template: `
        <ng-container *ngIf="!!state && state.ready">
          <evodesk-header-common-user-balance #view
              [props]="state.props"
              [isMobile]="isMobile"
              (expanded)="lockPanels()"
              (shrink)="unlockPanels()"
              (rublesHowToReplenish)="showModalHowToReplenishRubles()"
              (rublesHowToWithdraw)="showModalHowToWithdrawRubles()"
              (whatIsEvocoin)="showModalWhatIsEvoCoin()"
              (whatIsEvoCheck)="showModalWhatIsEvoCheck()"
              (rublesReplenish)="submitRublesReplenishForm($event)"
              (rublesWithdraw)="submitRublesWithdrawForm($event)"
              (evocoinCreateCheck)="submitEvocoinCreateCheck($event)"
              (copyEvoCheckLink)="copyEvoCheckLink($event)"
          ></evodesk-header-common-user-balance>
          <form #payeerSendForm *ngIf="!!state.sendRequest" [action]="state.payeer.sendUrl" style="display: none;">
            <input type="hidden" name="m_shop" [value]="state.payeer.mSHop">
            <input type="hidden" id="m_orderid" name="m_orderid" [value]="state.sendRequest.order_id">
            <input type="hidden" id="m_amount" name="m_amount" [value]="state.sendRequest.amount">
            <input type="hidden" name="m_curr" value="RUB">
            <input type="hidden" id="m_desc" name="m_desc" [value]="state.sendRequest.m_desc">
            <input type="hidden" id="m_sign" name="m_sign" [value]="state.sendRequest.sign">
            <input type="submit" name="m_process" value="send" />
          </form>
        </ng-container>
    `,
})
export class EvodeskHeaderCommonUserBalanceContainerComponent implements OnInit, OnDestroy
{
    @Input() isMobile: boolean = false;

    @Output('copyLink') copyLinkEvent: EventEmitter<EvoCheckModel> = new EventEmitter<EvoCheckModel>();

    @ViewChild('view') view: EvodeskHeaderCommonUserBalanceComponent;
    @ViewChild('payeerSendForm') payeerSendFormRef: ElementRef;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        payeer: {
            mSHop: environment.modules.EvoDeskRESTApi.payeer.m_shop,
            sendUrl: environment.modules.EvoDeskRESTApi.payeer.sendUrl,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private activatedRoute: ActivatedRoute,
        private uiAlertService: EvodeskAlertModalService,
        private uiConfirmService: EvodeskConfirmModalService,
        private appState: EvodeskAppStateService,
        private currentUserService: CurrentUserService,
        private evodeskHeaderStateService: EvodeskHeaderStateService,
        private evodeskHeaderConfigurationService: EvodeskHeaderConfigurationService,
        private billingRESTService: BillingRESTService,
        private billingBalanceService: EvodeskBillingBalanceService,
        private billingEvoChecksService: EvodeskEvoChecksService,
        private billingExchangeRatesService: EvodeskExchangeRatesService,
    ) {}

    ngOnInit(): void {
        if (! payResultsProcessed) {
            this.activatedRoute.queryParams.pipe(takeUntil(this.ngOnDestroy$)).subscribe((qp) => {
                if (qp['paystatus']) {
                    if (qp['paystatus'] === 'success') {
                        this.uiAlertService.open({
                            title: {
                                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ReplenishPayeerSuccess.Title',
                                translate: true,
                            },
                            text: {
                                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ReplenishPayeerSuccess.Content',
                                translate: true,
                            },
                            ok: {
                                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ReplenishPayeerSuccess.Close',
                                translate: true,
                            },
                        }).subscribe();
                    } else {
                        this.uiAlertService.open({
                            title: {
                                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ReplenishPayeerFail.Title',
                                translate: true,
                            },
                            text: {
                                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ReplenishPayeerFail.Content',
                                translate: true,
                            },
                            ok: {
                                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ReplenishPayeerFail.Close',
                                translate: true,
                            },
                        }).subscribe();
                    }
                }

                payResultsProcessed = true;
            });
        }

        this.billingBalanceService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(s => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    stat: {
                        points: s.balance.points.toFixed(0),
                        evocoin: s.balance.evocoin.toFixed(2),
                        rubles: s.balance.rub.toFixed(2),
                    },
                },
            };
        });

        this.billingEvoChecksService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(s => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    evoChecks: s,
                },
            };
        });

        this.billingExchangeRatesService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(s => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    evocoinToRubles: s.evocoinToRub.toFixed(2),
                },
            };
        });

        this.state = {
            ...this.state,
            ready: true,
        };

        this.evodeskHeaderStateService.current$.pipe(takeUntil(this.ngOnDestroy$), filter(s => s.current.isMinified)).subscribe(() => {
            if (this.view) {
                this.view.shrink();
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    lockPanels(): void {
        this.evodeskHeaderConfigurationService.lockPanels();
    }

    unlockPanels(): void {
        this.evodeskHeaderConfigurationService.unlockPanels();
    }

    showModalHowToReplenishRubles(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                disableShrink: true,
            },
        };

        this.uiAlertService.open({
            title: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ReplenishPayeer.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ReplenishPayeer.Content',
                translate: true,
                asHtml: true,
            },
            ok: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ReplenishPayeer.Close',
                translate: true,
            },
        }).subscribe(() => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    disableShrink: false,
                },
            };
        });
    }

    showModalHowToWithdrawRubles(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                disableShrink: true,
            },
        };

        this.uiAlertService.open({
            title: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WithdrawFunds.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WithdrawFunds.Content',
                translate: true,
                asHtml: true,
            },
            ok: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WithdrawFunds.Close',
                translate: true,
            },
        }).subscribe(() => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    disableShrink: false,
                },
            };
        });
    }

    showModalHowToUseEvoCoins(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                disableShrink: true,
            },
        };

        this.uiAlertService.open({
            title: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WithdrawFunds.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WithdrawFunds.Content',
                translate: true,
                asHtml: true,
            },
            ok: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WithdrawFunds.Close',
                translate: true,
            },
        }).subscribe(() => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    disableShrink: false,
                },
            };
        });
    }

    showModalWhatIsEvoCheck(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                disableShrink: true,
            },
        };

        this.uiAlertService.open({
            title: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WhatIsEvoCheck.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WhatIsEvoCheck.Content',
                translate: true,
                asHtml: true,
            },
            ok: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WhatIsEvoCheck.Close',
                translate: true,
            },
        }).subscribe(() => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    disableShrink: false,
                },
            };
        });
    }

    showModalWhatIsEvoCoin(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                disableShrink: true,
            },
        };

        this.uiAlertService.open({
            title: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WhatIsEvocoin.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WhatIsEvocoin.Content',
                translate: true,
                asHtml: true,
            },
            ok: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WhatIsEvocoin.Close',
                translate: true,
            },
        }).subscribe(() => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    disableShrink: false,
                },
            };
        });
    }

    copyEvoCheckLink(evoCheck: EvoCheckModel): void {
        this.uiAlertService.open({
            title: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.EvoCheckCopyLink.Title',
                translate: true,
                replaces: [
                    moment(evoCheck.created_at.date).format('LLL'),
                ],
            },
            text: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.EvoCheckCopyLink.Message',
                translate: true,
            },
            ok: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.EvoCheckCopyLink.Close',
                translate: true,
            },
            copyLink: this.billingEvoChecksService.getEvoCheckLink(evoCheck),
        }).subscribe();

        this.copyLinkEvent.emit(evoCheck);
    }

    submitRublesReplenishForm(formValue: RublesReplenishFormValue): void {
        this.view.state.rublesReplenishForm.disable();

        const fail: Function = () => {
            this.view.state.rublesReplenishForm.enable();

            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    disableShrink: true,
                },
            };
            window.requestAnimationFrame(() => {
                if (this.view.rublesReplenishAutoFocusRef) {
                    (this.view.rublesReplenishAutoFocusRef.nativeElement as HTMLElement).focus();
                }
            });

            this.uiAlertService.open({
                title: {
                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ReplenishPayeerFail.Title',
                    translate: true,
                },
                text: {
                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ReplenishPayeerFail.Content',
                    translate: true,
                },
                ok: {
                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ReplenishPayeerFail.Close',
                    translate: true,
                },
            }).subscribe(() => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        disableShrink: false,
                    },
                };
            });
        };

        this.billingRESTService.payeerBuy({
            userId: this.currentUserService.current.id,
            amount: formValue.value,
        }).subscribe(
            (httpResponse) => {
                this.state = {
                    ...this.state,
                    sendRequest: httpResponse,
                };

                this.cdr.detectChanges();
                this.cdr.detectChanges();

                setTimeout(() => {
                    if (this.payeerSendFormRef) {
                        (this.payeerSendFormRef.nativeElement as HTMLFormElement).submit();
                    } else {
                        fail();
                    }
                });
            },
            () => {
                fail();
            },
        );
    }

    submitRublesWithdrawForm(formValue: RublesWithdrawFormValue): void {
        this.view.state.rublesWithdrawForm.disable();

        const amount: number = parseFloat(parseFloat(formValue.value).toFixed(2));

        const success: Function = (id: string) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    disableShrink: true,
                },
            };

            this.view.state.rublesWithdrawForm.enable();
            this.view.state.rublesWithdrawForm.reset();

            this.appState.setState((orig) => {
                return {
                    ...orig,
                    currentUserBalance: {
                        ...orig.currentUserBalance,
                        rub: Math.max(0, orig.currentUserBalance.rub - amount),
                    },
                };
            });

            setTimeout(() => {
                this.uiAlertService.open({
                    title: {
                        text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WithdrawSuccess.Title',
                        translate: true,
                    },
                    text: {
                        text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WithdrawSuccess.Content',
                        translate: true,
                        replaces: [id],
                    },
                    ok: {
                        text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WithdrawSuccess.Close',
                        translate: true,
                    },
                }).subscribe(() => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            disableShrink: false,
                        },
                    };
                });
            });
        };

        const fail: Function = (error?: string) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    disableShrink: true,
                },
            };

            this.view.state.rublesWithdrawForm.enable();

            window.requestAnimationFrame(() => {
                if (this.view.rublesWithdrawAutoFocusRef) {
                    (this.view.rublesWithdrawAutoFocusRef.nativeElement as HTMLElement).focus();
                }
            });

            this.uiAlertService.open({
                title: {
                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WithdrawFail.Title',
                    translate: true,
                },
                text: {
                    text: error ? error : 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WithdrawFail.Unknown',
                    translate: true,
                },
                ok: {
                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.WithdrawFail.Close',
                    translate: true,
                },
            }).subscribe(() => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        disableShrink: false,
                    },
                };
            });
        };

        this.billingRESTService.payeerWithdraw({
            account: formValue.id,
            sum: formValue.value,
        }).subscribe(
            (httpResponse: BillingWithdrawResponse200) => {
                success(httpResponse.historyId);
            },
            (httpError: HttpErrorResponse) => {
                if (httpError.error && httpError.error.errors && Array.isArray(httpError.error.errors) && (typeof httpError.error.errors[0] === 'string')) {
                    fail(httpError.error.errors[0]);
                } else {
                    fail();
                }
            },
        );
    }

    submitEvocoinCreateCheck(formValue: EvocoinCreateCheckFormValue): void {
        this.view.state.evocoinCreateCheckForm.disable();

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                disableShrink: true,
            },
        };

        this.uiConfirmService.open({
            title: {
                text: '',
                translate: false,
            },
            text: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ConfirmCreateEvoCheck.Message',
                translate: true,
                replaces: [parseFloat(formValue.amount).toFixed(2), (parseFloat(formValue.amount) * 1.02).toFixed(2)],
            },
            cancelButton: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ConfirmCreateEvoCheck.Cancel',
                translate: true,
            },
            confirmButton: {
                text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.ConfirmCreateEvoCheck.Confirm',
                translate: true,
            },
            cancel: () => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        disableShrink: false,
                    },
                };

                this.view.state.evocoinCreateCheckForm.enable();

                if (this.view.evocoinCreateCheckAutoFocusRef) {
                    (this.view.evocoinCreateCheckAutoFocusRef.nativeElement as HTMLElement).focus();
                }
            },
            confirm: () => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        disableShrink: false,
                    },
                };

                const success: Function = (httpResponse: BillingCreateEvoCheckResponse200) => {
                    this.view.state.evocoinCreateCheckForm.enable();

                    window.requestAnimationFrame(() => {
                        if (this.view.evocoinCreateCheckAutoFocusRef) {
                            (this.view.evocoinCreateCheckAutoFocusRef.nativeElement as HTMLElement).focus();
                        }
                    });

                    this.uiAlertService.open({
                        title: {
                            text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.EvocoinCreateCheckSuccess.Title',
                            translate: true,
                        },
                        text: {
                            text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.EvocoinCreateCheckSuccess.Message',
                            translate: true,
                        },
                        ok: {
                            text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.EvocoinCreateCheckSuccess.Close',
                            translate: true,
                        },
                        copyLink: this.billingEvoChecksService.getEvoCheckLinkViaCodeAndSecret(httpResponse.code, httpResponse.secret),
                    }).subscribe(() => {
                        this.state = {
                            ...this.state,
                            props: {
                                ...this.state.props,
                                disableShrink: false,
                            },
                        };
                    });
                };

                const fail: Function = (error?: string) => {
                    this.view.state.evocoinCreateCheckForm.enable();

                    window.requestAnimationFrame(() => {
                        if (this.view.evocoinCreateCheckAutoFocusRef) {
                            (this.view.evocoinCreateCheckAutoFocusRef.nativeElement as HTMLElement).focus();
                        }
                    });

                    this.uiAlertService.open({
                        title: {
                            text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.EvocoinCreateCheckFail.Title',
                            translate: true,
                        },
                        text: {
                            text: error ? error : 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.EvocoinCreateCheckFail.Unknown',
                            translate: true,
                        },
                        ok: {
                            text: 'EvodeskHeader.components.common.EvodeskHeaderCommonUserBalance.Modals.EvocoinCreateCheckFail.Close',
                            translate: true,
                        },
                    }).subscribe(() => {
                        this.state = {
                            ...this.state,
                            props: {
                                ...this.state.props,
                                disableShrink: false,
                            },
                        };
                    });
                };

                this.billingRESTService.createEvoCheck({ sum: parseFloat(formValue.amount) }).subscribe(
                    (httpResponse) => {
                        success(httpResponse);
                    },
                    (httpError) => {
                        if (httpError.error && httpError.error.errors && Array.isArray(httpError.error.errors) && (typeof httpError.error.errors[0] === 'string')) {
                            fail(httpError.error.errors[0]);
                        } else {
                            fail();
                        }
                    },
                );
            },
        }).subscribe();
    }
}
