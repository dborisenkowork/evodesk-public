import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {EvoCheckModel} from '../../../../../app/EvodeskRESTApi/models/billing/EvoCheck.model';

import * as moment from 'moment';

type CurrentRublesForm = undefined | 'replenish' | 'withdraw';
type CurrentEvocoinForm = undefined | 'create-check';

export interface EvodeskHeaderCommonUserBalanceComponentProps {
    stat: {
        points: string;
        evocoin: string;
        rubles: string;
    };
    evocoinToRubles: string;
    disableShrink: boolean;
    evoChecks: Array<EvoCheckModel>;
}

interface State {
    ready: boolean;
    isExpanded: boolean;
    pointsUseVariant: PointsVariants;
    currentRublesForm: CurrentRublesForm;
    currentEvocoinForm: CurrentEvocoinForm;
    rublesReplenishForm: FormGroup;
    rublesWithdrawForm: FormGroup;
    evocoinCreateCheckForm: FormGroup;
}

type PointsVariants = 'F1' | 'F234' | 'F056789D';

export interface RublesReplenishFormValue {
    value: string;
}

export interface RublesWithdrawFormValue {
    id: string;
    value: string;
}

export interface EvocoinCreateCheckFormValue {
    amount: string;
}

@Component({
    selector: 'evodesk-header-common-user-balance',
    templateUrl: './EvodeskHeaderCommonUserBalance.component.pug',
    styleUrls: [
        './EvodeskHeaderCommonUserBalance.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderCommonUserBalanceComponent implements OnChanges
{
    public static DEFAULT_POINTS_VARIANT: PointsVariants = 'F056789D';

    @Input() props: EvodeskHeaderCommonUserBalanceComponentProps;
    @Input() isMobile: boolean = false;

    @Output('shrink') shrinkEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('expanded') expandedEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output('rublesHowToReplenish') rublesHowToReplenishEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('rublesHowToWithdraw') rublesHowToWithdrawEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('whatIsEvocoin') whatIsEvocoinEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('whatIsEvoCheck') whatIsEvoCheckEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output('rublesReplenish') rublesReplenishEvent: EventEmitter<RublesReplenishFormValue> = new EventEmitter<RublesReplenishFormValue>();
    @Output('rublesWithdraw') rublesWithdrawEvent: EventEmitter<RublesWithdrawFormValue> = new EventEmitter<RublesWithdrawFormValue>();
    @Output('evocoinCreateCheck') evocoinCreateCheckEvent: EventEmitter<EvocoinCreateCheckFormValue> = new EventEmitter<EvocoinCreateCheckFormValue>();

    @Output('copyEvoCheckLink') copyEvoCheckLinkEvent: EventEmitter<EvoCheckModel> = new EventEmitter<EvoCheckModel>();

    @ViewChild('rublesReplenishAutoFocus') rublesReplenishAutoFocusRef: ElementRef;
    @ViewChild('rublesWithdrawAutoFocus') rublesWithdrawAutoFocusRef: ElementRef;
    @ViewChild('evocoinCreateCheckAutoFocus') evocoinCreateCheckAutoFocusRef: ElementRef;

    public state: State = {
        ready: false,
        isExpanded: false,
        pointsUseVariant: EvodeskHeaderCommonUserBalanceComponent.DEFAULT_POINTS_VARIANT,
        currentRublesForm: undefined,
        currentEvocoinForm: undefined,
        rublesReplenishForm: this.fb.group({
            value: ['', [Validators.required]],
        }),
        rublesWithdrawForm: this.fb.group({
            id: ['', [Validators.required]],
            value: ['', [Validators.required]],
        }),
        evocoinCreateCheckForm: this.fb.group({
            amount: ['', [Validators.required]],
        }),
    };

    constructor(
        private fb: FormBuilder,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    pointsUseVariant: (() => {
                        if (this.props.stat.points && (typeof <any>this.props.stat.points === 'string') && this.props.stat.points.length > 0) {
                            const asInt: number = parseInt(this.props.stat.points, 10);
                            const lastDigit: number = parseInt(asInt.toString().split('').pop(), 10);

                            if (asInt >= 10 && asInt <= 19) {
                                return 'F056789D';
                            } else {
                                if (lastDigit === 1) {
                                    return 'F1';
                                } else if (lastDigit >= 2 && lastDigit <= 4) {
                                    return 'F234';
                                } else {
                                    return 'F056789D';
                                }
                            }
                        } else {
                            return EvodeskHeaderCommonUserBalanceComponent.DEFAULT_POINTS_VARIANT;
                        }
                    })(),
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    getEvoCheckDate(evoCheck: EvoCheckModel): string {
        return moment(evoCheck.created_at.date).format('LLL');
    }

    copyEvoCheckLink(evoCheck: EvoCheckModel): void {
        this.shrink();
        this.copyEvoCheckLinkEvent.emit(evoCheck);
    }

    toggle(): void {
        this.state.isExpanded ? this.shrink() : this.expand();
    }

    expand(): void {
        this.state = {
            ...this.state,
            isExpanded: true,
        };

        this.expandedEvent.emit(undefined);
    }

    shrink(): void {
        if (this.state.isExpanded && ! this.props.disableShrink) {
            this.state = {
                ...this.state,
                isExpanded: false,
                currentEvocoinForm: undefined,
                currentRublesForm: undefined,
            };

            this.shrinkEvent.emit(undefined);

            this.resetExtendedForms();
        }
    }

    resetExtendedForms(): void {
        this.state = {
            ...this.state,
            currentRublesForm: undefined,
        };
    }

    openRublesForm(form: CurrentRublesForm): void {
        this.state = {
            ...this.state,
            currentRublesForm: form,
            currentEvocoinForm: undefined,
        };

        window.requestAnimationFrame(() => {
            if (form === 'replenish') {
                if (this.rublesReplenishAutoFocusRef) {
                    (this.rublesReplenishAutoFocusRef.nativeElement as HTMLElement).focus();
                }
            } else if (form === 'withdraw') {
                if (this.rublesWithdrawAutoFocusRef) {
                    (this.rublesWithdrawAutoFocusRef.nativeElement as HTMLElement).focus();
                }
            }
        });
    }

    openEvocoinForm(form: CurrentEvocoinForm): void {
        this.state = {
            ...this.state,
            currentRublesForm: undefined,
            currentEvocoinForm: form,
        };

        window.requestAnimationFrame(() => {
            if (form === 'create-check') {
                if (this.evocoinCreateCheckAutoFocusRef) {
                    (this.evocoinCreateCheckAutoFocusRef.nativeElement as HTMLElement).focus();
                }
            }
        });
    }

    whatIsEvocoin(): void {
        this.whatIsEvocoinEvent.emit(undefined);
    }

    whatIsEvoCheck(): void {
        this.whatIsEvoCheckEvent.emit(undefined);
    }

    submitRublesReplenishForm(): void {
        if (this.state.rublesReplenishForm.valid) {
            this.rublesReplenishEvent.emit(this.state.rublesReplenishForm.value);
        }
    }

    submitRublesWithdrawForm(): void {
        if (this.state.rublesWithdrawForm.valid) {
            this.rublesWithdrawEvent.emit(this.state.rublesWithdrawForm.value);
        }
    }

    submitCreateEvocheckForm(): void {
        if (this.state.evocoinCreateCheckForm.valid) {
            this.evocoinCreateCheckEvent.emit(this.state.evocoinCreateCheckForm.value);
        }
    }
}
