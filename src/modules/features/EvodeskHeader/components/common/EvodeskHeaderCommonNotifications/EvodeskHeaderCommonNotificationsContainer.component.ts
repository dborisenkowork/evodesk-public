import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';

import {interval, Subject} from 'rxjs';
import {filter, take, takeUntil} from 'rxjs/operators';

import * as R from 'ramda';

import {proxy} from '../../../../../../functions/proxy.function';

import {EvodeskNotification, NotificationId} from '../../../../../app/EvodeskRESTApi/models/Notification.model';

import {EvodeskHeaderCommonNotificationsComponentOpenPostEvent as OpenPostEvent, EvodeskHeaderCommonNotificationsComponentProps} from './EvodeskHeaderCommonNotifications.component';

import {NotificationRESTService} from '../../../../../app/EvodeskRESTApi/services/NotificationREST.service';
import {PostRESTService} from '../../../../../app/EvodeskRESTApi/services/PostREST.service';
import {EvodeskFeedStateAction, EvodeskFeedStateService, FeedEntry, FeedEntryType, FeedPostEntry} from '../../../../EvodeskFeed/state/EvodeskFeedState.service';
import {CurrentUserService} from '../../../../EvodeskAuth/services/CurrentUser.service';

type Props = EvodeskHeaderCommonNotificationsComponentProps;

interface State {
    ready: boolean;
    pullInProgress: boolean;
    markIsViewedInProgress: boolean;
    pagesLoaded: number;
    markAsViewed: Array<NotificationId>;
    isOpened: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-header-common-notifications-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-header-common-notifications
          [props]="state.props"
          (markAsViewed)="markAsViewed($event)"
          (opened)="onOpened()"
          (closed)="onClosed()"
          (next)="next()"
          (openPost)="onOpenPost($event)"
          (closePost)="onClosePost()"
        ></evodesk-header-common-notifications>
      </ng-container>
    `,
    styles: [`:host { height: 100% }`],
})
export class EvodeskHeaderCommonNotificationsContainerComponent implements OnInit, OnChanges, OnDestroy
{
    @Input() entriesPerPage: number = 10;
    @Input() pullInterval: number = 10000;

    private stopPull$: Subject<void> = new Subject<void>();
    private markAsViewed$: Subject<void> = new Subject<void>();
    private stopLoadingPost$: Subject<void> = new Subject<void>();
    private stopPreviewPost$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        pullInProgress: false,
        markIsViewedInProgress: false,
        pagesLoaded: 0,
        markAsViewed: [],
        isOpened: false,
        props: {
            numUnreadNotifications: 0,
            notifications: [],
            hasMoreNotificationsToLoad: false,
            loading: false,
            loadingPost: false,
            currentPost: undefined,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private notificationsRESTService: NotificationRESTService,
        private postRESTService: PostRESTService,
        private feedState: EvodeskFeedStateService,
        private currentUser: CurrentUserService,
    ) {}

    ngOnInit(): void {
        this.setUp();

        this.stopPull$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    loading: false,
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['pullInterval']) {
            this.setUp();
        }
    }

    ngOnDestroy(): void {
        this.stopPull$.next(undefined);
        this.stopLoadingPost$.next(undefined);
        this.stopPreviewPost$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    onOpened(): void {
        this.stopPull$.next(undefined);

        this.state = {
            ...this.state,
            isOpened: true,
        };

        if (! this.state.props.notifications.length) {
            this.pull(1);
        }
    }

    onClosed(): void {
        if (this.state.markAsViewed.length) {
            proxy(this.notificationsRESTService.setAsViewed(this.state.markAsViewed)).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                () => {
                    this.markAsViewed$.next(undefined);

                    this.state = {
                        ...this.state,
                        markIsViewedInProgress: false,
                    };
                },
                () => {
                    this.markAsViewed$.next(undefined);

                    this.state = {
                        ...this.state,
                        markIsViewedInProgress: false,
                    };
                },
            );

            this.state = {
                ...this.state,
                markAsViewed: [],
                markIsViewedInProgress: true,
                props: {
                    ...this.state.props,
                    numUnreadNotifications: Math.max(0, this.state.props.numUnreadNotifications - this.state.markAsViewed.length),
                },
            };
        }

        this.state = {
            ...this.state,
            isOpened: false,
            pagesLoaded: 1,
            props: {
                ...this.state.props,
                notifications: [],
            },
        };

        this.cdr.markForCheck();
    }

    onOpenPost($event: OpenPostEvent) {
        this.stopLoadingPost$.next(undefined);

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                loadingPost: true,
            },
        };

        this.feedState.reset();

        this.feedState.current$.pipe(takeUntil(this.stopPreviewPost$)).subscribe((s) => {
            const result: FeedEntry = s.current.posts.filter(e => e.type === FeedEntryType.Post && e.post.id === $event.postId)[0];

            if (result) {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        currentPost: (s.current.posts.filter(e => e.type === FeedEntryType.Post && e.post.id === $event.postId)[0] as FeedPostEntry).post,
                    },
                };

                this.cdr.detectChanges();
            }
        });

        this.postRESTService.getPost($event.postId).pipe(takeUntil(this.stopLoadingPost$)).subscribe(
            (httpResponse) => {
                this.feedState.dispatch({
                    type: EvodeskFeedStateAction.PushFeedEntries,
                    payload: {
                        feedEntries: [
                            <FeedPostEntry>{
                                type: FeedEntryType.Post,
                                sid: 'preview',
                                post: <any>httpResponse,
                                isOwn: parseInt(<any>httpResponse.user_id, 10) === this.currentUser.impersonatedAs.id,
                            },
                        ],
                        hasMoreToLoad: false,
                    },
                });

                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        loadingPost: false,
                    },
                };

                this.cdr.detectChanges();
            },
        );

        this.cdr.detectChanges();
    }

    onClosePost(): void {
        this.stopLoadingPost$.next(undefined);
        this.stopPreviewPost$.next(undefined);
    }

    setUp(): void {
        this.stopPull$.next(undefined);

        interval(this.pullInterval).pipe(
            takeUntil(this.ngOnDestroy$),
            filter(() => ! this.state.pullInProgress && ! this.state.isOpened),
        ).subscribe(() => {
            this.pull();
        });

        this.pull();
    }

    pull(page: number = 1): void {
        const doPull: Function = (() => {
            this.notificationsRESTService.pull({ limit: this.entriesPerPage, page: page }).subscribe(
                (httpResponse) => {
                    if (! this.state.isOpened || this.state.props.notifications.length === 0 || page > 1) {
                        const sourceNotifications: Array<EvodeskNotification> = R.uniqBy((n: EvodeskNotification) => n.model.id, [
                            ...this.state.props.notifications,
                            ...httpResponse.notifications,
                        ]);

                        this.state = {
                            ...this.state,
                            pullInProgress: false,
                            pagesLoaded: Math.max(page, this.state.pagesLoaded),
                            props: {
                                ...this.state.props,
                                hasMoreNotificationsToLoad: httpResponse.pagination.total_pages > Math.max(page, this.state.pagesLoaded),
                                notifications: (() => {
                                    if (page === 1) {
                                        return [
                                            ...sourceNotifications.filter(n => n.model.viewedAt === null),
                                            ...sourceNotifications.filter(n => n.model.viewedAt !== null),
                                        ];
                                    } else {
                                        return sourceNotifications;
                                    }
                                })(),
                                numUnreadNotifications: httpResponse.count_new,
                                loading: false,
                            },
                        };

                        this.cdr.detectChanges();
                    }
                },
                () => {
                    this.state = {
                        ...this.state,
                        pullInProgress: false,
                        props: {
                            ...this.state.props,
                            loading: false,
                        },
                    };

                    this.cdr.markForCheck();
                },
            );
        });

        if (this.state.markIsViewedInProgress) {
            this.markAsViewed$.pipe(takeUntil(this.stopPull$), take(1)).subscribe(() => {
                doPull();
            });
        } else {
            doPull();
        }

        this.state = {
            ...this.state,
            pullInProgress: true,
            props: {
                ...this.state.props,
                loading: true,
            },
        };

        this.cdr.markForCheck();
    }

    markAsViewed(notificationIds: Array<NotificationId>): void {
        this.state = {
            ...this.state,
            markAsViewed: [...this.state.markAsViewed, ...notificationIds],
        };
    }

    next(): void {
        this.pull(this.state.pagesLoaded + 1);
    }
}
