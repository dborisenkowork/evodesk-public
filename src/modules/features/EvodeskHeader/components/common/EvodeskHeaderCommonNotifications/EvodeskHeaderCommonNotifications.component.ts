import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, HostListener, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {EvodeskNotification, NotificationId, NotificationTypeKind} from '../../../../../app/EvodeskRESTApi/models/Notification.model';
import {PostId, PostModel} from '../../../../../app/EvodeskRESTApi/models/post/Post.model';

enum CurrentTab {
    List = 'list',
    PreviewPost = 'preview-post',
}

interface Props {
    hasMoreNotificationsToLoad: boolean;
    notifications: Array<EvodeskNotification>;
    numUnreadNotifications: number;
    loading: boolean;
    loadingPost: boolean;
    currentPost: PostModel | undefined;
}

interface State {
    ready: boolean;
    isExpanded: boolean;
    disableToggle: boolean;
    clickOutsideDisabled: boolean;
    currentTab: CurrentTab;
}

const scrollTrigger: number = 50;

export interface EvodeskHeaderCommonNotificationsComponentOpenPostEvent {
    notification: EvodeskNotification;
    postId: PostId;
}

export {Props as EvodeskHeaderCommonNotificationsComponentProps};

@Component({
    selector: 'evodesk-header-common-notifications',
    templateUrl: './EvodeskHeaderCommonNotifications.component.pug',
    styleUrls: [
        './EvodeskHeaderCommonNotifications.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderCommonNotificationsComponent implements OnChanges
{
    @Input() props: Props;

    @Output('next') nextEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('markAsViewed') markAsViewedEvent: EventEmitter<Array<NotificationId>> = new EventEmitter<Array<NotificationId>>();
    @Output('closed') closedEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('opened') openedEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openPost') openPostEvent: EventEmitter<EvodeskHeaderCommonNotificationsComponentOpenPostEvent> = new EventEmitter<EvodeskHeaderCommonNotificationsComponentOpenPostEvent>();
    @Output('closePost') closePost: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        isExpanded: false,
        disableToggle: false,
        clickOutsideDisabled: false,
        currentTab: CurrentTab.List,
    };

    constructor(
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    @HostListener('document:mouseup')
    onDocumentMouseDown(): void {
        this.state = {
            ...this.state,
            clickOutsideDisabled: false,
            disableToggle: false,
        };
    }

    @HostListener('document:touchstart')
    onDocumentTouchEnd(): void {
        this.state = {
            ...this.state,
            clickOutsideDisabled: false,
            disableToggle: false,
        };
    }

    trackByNotificationId(index: number, item: EvodeskNotification): any {
        return item.model.id;
    }

    clickOutside(): void {
        if (! this.state.clickOutsideDisabled) {
            this.toggle();
        }
    }

    toggle(): void {
        if (! this.state.disableToggle) {
            this.state = {
                ...this.state,
                isExpanded: ! this.state.isExpanded,
                disableToggle: true,
            };

            if (! this.state.isExpanded) {
                this.markAsViewedEvent.emit(this.props.notifications.filter(n => ! n.model.viewedAt).map(n => n.model.id));
            }

            if (this.state.isExpanded) {
                this.state = {
                    ...this.state,
                    clickOutsideDisabled: true,
                };

                this.openedEvent.emit(undefined);
            } else {
                this.closedEvent.emit(undefined);
            }

            this.cdr.markForCheck();
        }
    }

    get hasNotifications(): boolean {
        return this.props.notifications.length > 0;
    }

    get notifications(): Array<EvodeskNotification> {
        return this.props.notifications;
    }

    onScroll($event): void {
        if (this.props.hasMoreNotificationsToLoad) {
            const scrollTop: number = ($event.target as HTMLElement).scrollTop + ($event.target.clientHeight);
            const scrollHeight: number = ($event.target as HTMLElement).scrollHeight;

            if (scrollTop >= (scrollHeight - scrollTrigger)) {
                this.nextEvent.emit(undefined);
            }
        }
    }

    next(): void {
        this.nextEvent.emit(undefined);
    }

    goBackToNotifications(): void {
        this.state = {
            ...this.state,
            currentTab: CurrentTab.List,
        };

        if (this.props.loadingPost || this.props.currentPost) {
            this.closePost.emit(undefined);
        }

        this.cdr.detectChanges();
    }

    onNotificationOpen(n: EvodeskNotification): void {
        if (!~[
            NotificationTypeKind.Post,
            NotificationTypeKind.PostLiked,
            NotificationTypeKind.PostReposted,
            NotificationTypeKind.PostCommented,
            NotificationTypeKind.PostCommentLiked,
        ].indexOf(n.kind)) {
            this.toggle();
        }
    }

    onNotificationOpenPost(n: EvodeskNotification, postId: PostId): void {
        this.state = {
            ...this.state,
            currentTab: CurrentTab.PreviewPost,
        };

        this.openPostEvent.emit({
            notification: n,
            postId: postId,
        });
    }
}
