import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {ProfileId} from '../../../../../app/EvodeskRESTApi/models/profile/Profile.model';

export interface EvodeskHeaderCommonProfileMenuComponentProps {
    avatar: string | undefined;
    name: string;
    disableContextMenu: boolean;
    profileRouterLink: any;
    profileId: ProfileId;
}

interface State {
    ready: boolean;
    isMenuOpened: boolean;
}

@Component({
    selector: 'evodesk-header-common-profile-menu',
    templateUrl: './EvodeskHeaderCommonProfileMenu.component.pug',
    styleUrls: [
        './EvodeskHeaderCommonProfileMenu.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderCommonProfileMenuComponent implements OnChanges
{
    @Input() props: EvodeskHeaderCommonProfileMenuComponentProps;

    @Output('signOut') signOutEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        isMenuOpened: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }

            if (this.props.disableContextMenu) {
                this.state = {
                    ...this.state,
                    isMenuOpened: false,
                };
            }
        }
    }

    showMenu($event: MouseEvent): void {
        if (! ($event.ctrlKey || $event.metaKey)) {
            $event.preventDefault();
            $event.stopImmediatePropagation();

            if (! this.props.disableContextMenu) {
                this.state = {
                    ...this.state,
                    isMenuOpened: true,
                };
            }
        }
    }

    hideMenu(): void {
        if (! this.props.disableContextMenu) {
            this.state = {
                ...this.state,
                isMenuOpened: false,
            };
        }
    }

    signOut(): void {
        this.signOutEvent.emit(undefined);
    }
}
