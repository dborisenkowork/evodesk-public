import {ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {environment} from '../../../../../../environments/environment.config';

import {EvodeskHeaderCommonProfileMenuComponentProps} from './EvodeskHeaderCommonProfileMenu.component';

import {CurrentUserService} from '../../../../EvodeskAuth/services/CurrentUser.service';
import {SignOutService} from '../../../../EvodeskAuth/services/SignOut.service';

interface State {
    ready: boolean;
    props?: EvodeskHeaderCommonProfileMenuComponentProps;
}

@Component({
    selector: 'evodesk-header-common-profile-menu-container',
    template: `
        <ng-container *ngIf="!!state && state.ready">
          <evodesk-header-common-profile-menu [props]="state.props" (signOut)="signOut()"></evodesk-header-common-profile-menu>
        </ng-container>
    `,
})
export class EvodeskHeaderCommonProfileMenuContainerComponent implements OnInit, OnChanges, OnDestroy
{
    @Input() disableContextMenu: boolean = false;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private currentUserService: CurrentUserService,
        private signOutService: SignOutService,
    ) {}

    ngOnInit(): void {
        this.currentUserService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            if (next) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        avatar: next.avatar ? `${environment.modules.EvoDeskRESTApi.apiEndpoint}/profile/${next.id}/avatar` : undefined,
                        name: next.email,
                        disableContextMenu: this.disableContextMenu,
                        profileRouterLink: ['/profile', this.currentUserService.impersonatedAs.url_alias || this.currentUserService.impersonatedAs.id],
                        profileId: this.currentUserService.impersonatedAs.id,
                    },
                };

                this.cdr.detectChanges();
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['disableContextMenu'] && !changes['disableContextMenu'].isFirstChange()) {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    disableContextMenu: this.disableContextMenu,
                },
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    signOut(): void {
        this.signOutService.signOut();
    }
}
