import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

import {EvodeskHeaderAvailableScreens, EvodeskHeaderMenuItem} from '../../../models/EvodeskHeader.models';
import {ActivatedRoute} from '@angular/router';

export interface EvodeskHeaderCommonMenuComponentProps {
    menuItems: Array<EvodeskHeaderMenuItem>;
}

@Component({
    selector: 'evodesk-header-common-menu',
    templateUrl: './EvodeskHeaderCommonMenu.component.pug',
    styleUrls: [
        './EvodeskHeaderCommonMenu.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderCommonMenuComponent
{
    @Input() props: EvodeskHeaderCommonMenuComponentProps;
    @Input() filter: Array<EvodeskHeaderAvailableScreens> = ['*', 'tablet', 'desktop', 'wide'];

    constructor(
        private activatedRoute: ActivatedRoute,
    ) {}

    get menuItems(): Array<EvodeskHeaderMenuItem> {
        return this.props.menuItems
            .filter(menuItem => ! menuItem.disabled)
            .filter(menuItem => {
                return menuItem.available.some((a) => !!~this.filter.indexOf(a));
            });
    }

    trackByMenuItemId(index: number, item: EvodeskHeaderMenuItem): any {
        return item.id;
    }

    getNgClasses(item: EvodeskHeaderMenuItem): any {
        const ngClasses: any = {};

        for (const v of item.available) {
            if (v === '*') {
                ngClasses['is-available-all'] = true;
            } else {
                ngClasses[`is-available-${v}`] = true;
            }
        }

        if (item.customActive) {
            ngClasses['custom-active'] = item.customActive(this.activatedRoute);
        }

        return ngClasses;
    }
}
