import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskHeaderCommonMenuComponentProps} from './EvodeskHeaderCommonMenu.component';

import {EvodeskHeaderStateService} from '../../../services/EvodeskHeaderState.service';

interface State {
    ready: boolean;
    props?: EvodeskHeaderCommonMenuComponentProps;
}

@Component({
    selector: 'evodesk-header-common-menu-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-header-common-menu [props]="state.props"></evodesk-header-common-menu>
      </ng-container>
    `,
})
export class EvodeskHeaderCommonMenuContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private evodeskHeaderStateService: EvodeskHeaderStateService,
    ) {}

    ngOnInit(): void {
        this.evodeskHeaderStateService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    menuItems: s.current.menuItems,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
