import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

export interface EvodeskHeaderCommonLanguagesComponentProps {
    currentLocale: string;
    availableLocales: Array<string>;
}

interface State {
    ready: boolean;
    areOptionsOpened: boolean;
}

@Component({
    selector: 'evodesk-header-common-languages',
    templateUrl: './EvodeskHeaderCommonLanguages.component.pug',
    styleUrls: [
        './EvodeskHeaderCommonLanguages.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskHeaderCommonLanguagesComponent implements OnChanges
{
    @Input() props: EvodeskHeaderCommonLanguagesComponentProps;

    @Output('select') selectEvent: EventEmitter<string> = new EventEmitter<string>();

    public state: State = {
        ready: false,
        areOptionsOpened: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    select(locale: string): void {
        this.closeOptions();
        this.selectEvent.emit(locale);
    }

    toggleOptions(): void {
        this.state = {
            ...this.state,
            areOptionsOpened: ! this.state.areOptionsOpened,
        };
    }

    closeOptions(): void {
        this.state = {
            ...this.state,
            areOptionsOpened: false,
        };
    }
}
