import {Component, OnDestroy, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';

import {EvodeskHeaderCommonLanguagesComponentProps} from './EvodeskHeaderCommonLanguages.component';

import {environment} from '../../../../../../environments/environment.config';

interface State {
    ready: boolean;
    props?: EvodeskHeaderCommonLanguagesComponentProps;
}

@Component({
    selector: 'evodesk-header-common-languages-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-header-common-languages [props]="state.props" (select)="doChangeLanguage($event)"></evodesk-header-common-languages>
      </ng-container>
    `,
})
export class EvodeskHeaderCommonLanguagesContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private translate: TranslateService,
    ) {}

    ngOnInit(): void {
        this.state = {
            ...this.state,
            ready: true,
            props: {
                currentLocale: this.translate.getDefaultLang(),
                availableLocales: [...environment.availableLocales],
            },
        };
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    doChangeLanguage(newLang: string): void {
        this.translate.setDefaultLang(newLang);

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                currentLocale: newLang,
            },
        };
    }
}
