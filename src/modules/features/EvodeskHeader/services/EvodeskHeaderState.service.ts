import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable} from 'rxjs';

import {EvodeskHeaderMenuItem, EvodeskHeaderLayoutVariant, EvodeskHeaderSubMenuItem} from '../models/EvodeskHeader.models';

export interface EvodeskHeaderState {
    variant: EvodeskHeaderLayoutVariant;
    isLocked: boolean;
    isMinified: boolean;
    isMobileOpened: boolean;
    isSearchFormOpened: boolean;
    useAbsolutePositions: boolean;
    menuItems: Array<EvodeskHeaderMenuItem>;
    subMenuItems: Array<EvodeskHeaderSubMenuItem>;
    imMessages: number;
}

export interface EvodeskHeaderStates {
    previous: EvodeskHeaderState | undefined;
    current: EvodeskHeaderState;
}

function initialState(): EvodeskHeaderState {
    return {
        variant: EvodeskHeaderLayoutVariant.None,
        isLocked: false,
        isMinified: false,
        isMobileOpened: false,
        isSearchFormOpened: false,
        useAbsolutePositions: false,
        menuItems: [],
        subMenuItems: [],
        imMessages: 0,
    };
}

@Injectable()
export class EvodeskHeaderStateService
{
    private _current$: BehaviorSubject<EvodeskHeaderStates> = new BehaviorSubject<EvodeskHeaderStates>({
        previous: undefined,
        current: initialState(),
    });

    get current$(): Observable<EvodeskHeaderStates> {
        return this._current$.asObservable();
    }

    get snapshot(): EvodeskHeaderState {
        return this._current$.getValue().current;
    }

    setState(reason: string, query: (orig: EvodeskHeaderState) => EvodeskHeaderState) {
        const prev: EvodeskHeaderState = this._current$.getValue().current;

        this._current$.next({
            previous: prev,
            current: query(prev),
        });
    }

    reset(): void {
        this._current$.next({
            previous: undefined,
            current: initialState(),
        });
    }
}
