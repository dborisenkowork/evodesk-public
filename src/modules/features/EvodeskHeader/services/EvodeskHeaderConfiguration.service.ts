import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {Subject} from 'rxjs';
import {filter, take} from 'rxjs/operators';

import {DeskId} from '../../../app/EvodeskRESTApi/models/Desk.model';
import {EvodeskHeaderCounter, EvodeskHeaderLayoutVariant, EvodeskHeaderMenuItem, EvodeskHeaderSubMenuItem} from '../models/EvodeskHeader.models';

import {EvodeskHeaderStateService} from './EvodeskHeaderState.service';
import {CurrentUserService} from '../../EvodeskAuth/services/CurrentUser.service';

@Injectable()
export class EvodeskHeaderConfigurationService
{
    constructor(
        private state: EvodeskHeaderStateService,
        private currentUser: CurrentUserService,
    ) {}

    setVariant(variant: EvodeskHeaderLayoutVariant): void {
        switch (variant) {
            default:
                this.state.setState('[EvodeskHeaderConfigurationService] Set variant (None)', (orig) => {
                    return {
                        ...orig,
                        variant: EvodeskHeaderLayoutVariant.None,
                    };
                });

                break;

            case EvodeskHeaderLayoutVariant.Common:
                this.state.setState('[EvodeskHeaderConfigurationService] Set variant (Common)', (orig) => {
                    const deskItem: EvodeskHeaderMenuItem = {
                        link: {
                            type: 'routerLink',
                            routerLink: ['/desk'],
                        },
                        title: {
                            text: 'EvodeskHeader.components.common.EvodeskHeaderCommonMenu.MenuItems.Desk',
                            shouldTranslate: true,
                        },
                        available: ['*'],
                    };

                    const deskSubMenuItem: EvodeskHeaderSubMenuItem = {
                        link: {
                            type: 'routerLink',
                            routerLink: ['/desk'],
                        },
                        title: {
                            text: 'EvodeskHeader.components.common.EvodeskHeaderCommonSubMenu.MenuItems.MyDesk',
                            shouldTranslate: true,
                        },
                        customActive: (route: ActivatedRoute, router: Router) => {
                            if (this.currentUser.impersonatedAs) {
                                return router.url.indexOf(`/desk/${this.currentUser.impersonatedAs.desk}`) === 0;
                            } else {
                                return false;
                            }
                        },
                        position: 1,
                    };

                    const profileSubMenuItem: EvodeskHeaderSubMenuItem = {
                        link: {
                            type: 'routerLink',
                            routerLink: ['/profile'],
                        },
                        title: {
                            text: 'EvodeskHeader.components.common.EvodeskHeaderCommonSubMenu.MenuItems.MyProfile',
                            shouldTranslate: true,
                        },
                        position: 3,
                    };

                    const multidesksSubMenuItem: EvodeskHeaderSubMenuItem = {
                        link: {
                            type: 'routerLink',
                            routerLink: ['/desks'],
                        },
                        title: {
                            text: 'EvodeskHeader.components.common.EvodeskHeaderCommonSubMenu.MenuItems.MultiDesks',
                            shouldTranslate: true,
                        },
                        position: 2,
                        customActive: (route: ActivatedRoute, router: Router) => {
                            if (this.currentUser.impersonatedAs.desk) {
                                return router.url.indexOf(`/desk`) === 0 && router.url.indexOf(`/desk/${this.currentUser.impersonatedAs.desk}`) < 0;
                            } else {
                                return false;
                            }
                        },
                        disabled: true,
                    };

                    this.currentUser.current$.pipe(filter(u => !! u), take(1)).subscribe((u) => {
                        const deskId: DeskId = u.desk;

                        deskItem.link = {
                            type: 'routerLink',
                            routerLink: ['/desk/', deskId],
                        };

                        deskSubMenuItem.link = {
                            type: 'routerLink',
                            routerLink: ['/desk/', deskId],
                        };

                        profileSubMenuItem.link = {
                            type: 'routerLink',
                            routerLink: ['/profile/' + (this.currentUser.impersonatedAs.url_alias || this.currentUser.impersonatedAs.id)],
                        };

                        multidesksSubMenuItem.disabled = u.availableDesks.length === 0;
                    });

                    return {
                        ...orig,
                        variant: EvodeskHeaderLayoutVariant.Common,
                        menuItems: [
                            {
                                link: {
                                    type: 'routerLink',
                                    routerLink: ['/shop'],
                                },
                                title: {
                                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonMenu.MenuItems.Shop',
                                    shouldTranslate: true,
                                },
                                available: ['*'],
                            },
                            deskItem,
                            {
                                link: {
                                    type: 'routerLink',
                                    routerLink: ['/people'],
                                },
                                title: {
                                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonMenu.MenuItems.People',
                                    shouldTranslate: true,
                                },
                                available: ['*'],
                                disabled: true,
                            },
                        ],
                        subMenuItems: [
                            deskSubMenuItem,
                            multidesksSubMenuItem,
                            profileSubMenuItem,
                            {
                                link: {
                                    type: 'routerLink',
                                    routerLink: ['/rating'],
                                },
                                title: {
                                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonSubMenu.MenuItems.Achievements',
                                    shouldTranslate: true,
                                },
                                position: 4,
                            },
                            {
                                link: {
                                    type: 'routerLink',
                                    routerLink: ['/leaders'],
                                },
                                title: {
                                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonSubMenu.MenuItems.Leaders',
                                    shouldTranslate: true,
                                },
                                position: 5,
                            },
                            {
                                link: {
                                    type: 'routerLink',
                                    routerLink: ['/im'],
                                },
                                title: {
                                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonSubMenu.MenuItems.Messages',
                                    shouldTranslate: true,
                                },
                                position: 6,
                                disabled: false,
                                counter: EvodeskHeaderCounter.IMMessages,
                            },
                            {
                                link: {
                                    type: 'routerLink',
                                    routerLink: ['/faq'],
                                },
                                title: {
                                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonSubMenu.MenuItems.FAQ',
                                    shouldTranslate: true,
                                },
                                position: -2,
                            },
                        ],
                    };
                });

                break;

            case EvodeskHeaderLayoutVariant.Landing:
            case EvodeskHeaderLayoutVariant.LandingNoBg:
                this.state.setState('[EvodeskHeaderConfigurationService] Set variant (Landing / Landing (no background))', (orig) => {
                    return {
                        ...orig,
                        variant: variant,
                        menuItems: [
                            {
                                link: {
                                    type: 'routerLink',
                                    routerLink: ['/landing'],
                                },
                                title: {
                                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonMenu.MenuItems.LandingAbout',
                                    shouldTranslate: true,
                                },
                                available: ['*'],
                                disabled: true,
                            },
                            {
                                link: {
                                    type: 'routerLink',
                                    routerLink: ['/landing/features'],
                                },
                                title: {
                                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonMenu.MenuItems.LandingFeatures',
                                    shouldTranslate: true,
                                },
                                available: ['*'],
                            },
                            {
                                link: {
                                    type: 'routerLink',
                                    routerLink: ['/landing/evocoin'],
                                },
                                title: {
                                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonMenu.MenuItems.LandingEvocoin',
                                    shouldTranslate: true,
                                },
                                available: ['*'],
                            },
                            {
                                link: {
                                    type: 'routerLink',
                                    routerLink: ['/landing/partners'],
                                },
                                title: {
                                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonMenu.MenuItems.LandingPartners',
                                    shouldTranslate: true,
                                },
                                available: ['mobile', 'wide'],
                            },
                            {
                                id: 'conditions',
                                link: undefined,
                                title: {
                                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonMenu.MenuItems.LandingConditions',
                                    shouldTranslate: true,
                                },
                                available: ['mobile'],
                            },
                            {
                                link: {
                                    type: 'routerLink',
                                    routerLink: ['/landing/developers'],
                                },
                                title: {
                                    text: 'EvodeskHeader.components.common.EvodeskHeaderCommonMenu.MenuItems.LandingDevelopers',
                                    shouldTranslate: true,
                                },
                                available: ['mobile'],
                                disabled: true,
                            },
                        ],
                        subMenuItems: [],
                    };
                });

                break;
        }
    }

    useAbsolutePositions(until$?: Subject<any>): void {
        this.state.setState('[EvodeskHeaderConfigurationService] useAbsolutePositions', (orig) => {
            return {
                ...orig,
                useAbsolutePositions: true,
            };
        });

        if (until$) {
            until$.subscribe(() => {
                this.state.setState('[EvodeskHeaderConfigurationService] useAbsolutePositions (disable)', (orig) => {
                    return {
                        ...orig,
                        useAbsolutePositions: false,
                    };
                });
            });
        }
    }

    lockPanels(until$?: Subject<any>): void {
        this.state.setState('[EvodeskHeaderConfigurationService] Lock panels', (orig) => {
            return {
                ...orig,
                isLocked: true,
                isMinified: false,
            };
        });

        if (until$) {
            until$.subscribe(() => {
                this.unlockPanels();
            });
        }
    }

    unlockPanels(): void {
        this.state.setState('[EvodeskHeaderConfigurationService] Unlock panels', (orig) => {
            return {
                ...orig,
                isLocked: false,
            };
        });
    }

    unminifyPanels(): void {
        this.state.setState('[EvodeskHeaderConfigurationService] Unminify panels', (orig) => {
            return {
                ...orig,
                isMinified: false,
            };
        });
    }
}
