import {NgModule} from '@angular/core';
import {EvodeskFeedModule} from '../EvodeskFeed/EvodeskFeed.module';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';
import {EvodeskNotificationsModule} from '../EvodeskNotifications/EvodeskNotifications.module';

import {EvodeskHeaderComponent} from './shared/EvodeskHeader/EvodeskHeader.component';
import {EvodeskHeaderContainerComponent} from './shared/EvodeskHeader/EvodeskHeaderContainer.component';

import {EvodeskHeaderCommonLogoComponent} from './components/common/EvodeskHeaderCommonLogo/EvodeskHeaderCommonLogo.component';
import {EvodeskHeaderCommonMenuComponent} from './components/common/EvodeskHeaderCommonMenu/EvodeskHeaderCommonMenu.component';
import {EvodeskHeaderCommonUserBalanceComponent} from './components/common/EvodeskHeaderCommonUserBalance/EvodeskHeaderCommonUserBalance.component';
import {EvodeskHeaderCommonProfileMenuComponent} from './components/common/EvodeskHeaderCommonProfileMenu/EvodeskHeaderCommonProfileMenu.component';
import {EvodeskHeaderCommonProfileMenuContainerComponent} from './components/common/EvodeskHeaderCommonProfileMenu/EvodeskHeaderCommonProfileMenuContainer.component';
import {EvodeskHeaderCommonMenuContainerComponent} from './components/common/EvodeskHeaderCommonMenu/EvodeskHeaderCommonMenuContainer.component';
import {EvodeskHeaderCommonUserBalanceContainerComponent} from './components/common/EvodeskHeaderCommonUserBalance/EvodeskHeaderCommonUserBalanceContainer.component';
import {EvodeskHeaderCommonOpenMobileComponent} from './components/common/EvodeskHeaderCommonOpenMobile/EvodeskHeaderCommonOpenMobile.component';
import {EvodeskHeaderCommonOpenMobileContainerComponent} from './components/common/EvodeskHeaderCommonOpenMobile/EvodeskHeaderCommonOpenMobileContainer.component';
import {EvodeskHeaderCommonSearchComponent} from './components/common/EvodeskHeaderCommonSearch/EvodeskHeaderCommonSearch.component';
import {EvodeskHeaderCommonSearchContainerComponent} from './components/common/EvodeskHeaderCommonSearch/EvodeskHeaderCommonSearchContainer.component';
import {EvodeskHeaderCommonSubMenuComponent} from './components/common/EvodeskHeaderCommonSubMenu/EvodeskHeaderCommonSubMenu.component';
import {EvodeskHeaderCommonSubMenuContainerComponent} from './components/common/EvodeskHeaderCommonSubMenu/EvodeskHeaderCommonSubMenuContainer.component';
import {EvodeskHeaderCommonLanguagesComponent} from './components/common/EvodeskHeaderCommonLanguages/EvodeskHeaderCommonLanguages.component';
import {EvodeskHeaderCommonLanguagesContainerComponent} from './components/common/EvodeskHeaderCommonLanguages/EvodeskHeaderCommonLanguagesContainer.component';
import {EvodeskHeaderMobileMenuComponent} from './components/mobile/EvodeskHeaderMobileMenu/EvodeskHeaderMobileMenu.component';
import {EvodeskHeaderMobileMenuContainerComponent} from './components/mobile/EvodeskHeaderMobileMenu/EvodeskHeaderMobileMenuContainer.component';
import {EvodeskHeaderMobileSocialNetworksComponent} from './components/mobile/EvodeskHeaderMobileSocialNetworks/EvodeskHeaderMobileSocialNetworks.component';
import {EvodeskHeaderMobileLanguagesComponent} from './components/mobile/EvodeskHeaderMobileLanguages/EvodeskHeaderMobileLanguages.component';
import {EvodeskHeaderMobileLanguagesContainerComponent} from './components/mobile/EvodeskHeaderMobileLanguages/EvodeskHeaderMobileLanguagesContainer.component';
import {EvodeskHeaderMobileProfileMenuComponent} from './components/mobile/EvodeskHeaderMobileProfileMenu/EvodeskHeaderMobileProfileMenu.component';
import {EvodeskHeaderMobileProfileMenuContainerComponent} from './components/mobile/EvodeskHeaderMobileProfileMenu/EvodeskHeaderMobileProfileMenuContainer.component';
import {EvodeskHeaderCommonSearchResultsComponent} from './components/common/EvodeskHeaderCommonSearchResults/EvodeskHeaderCommonSearchResults.component';
import {EvodeskHeaderCommonSearchResultsContainerComponent} from './components/common/EvodeskHeaderCommonSearchResults/EvodeskHeaderCommonSearchResultsContainer.component';
import {EvodeskHeaderCommonNotificationsComponent} from './components/common/EvodeskHeaderCommonNotifications/EvodeskHeaderCommonNotifications.component';
import {EvodeskHeaderCommonNotificationsContainerComponent} from './components/common/EvodeskHeaderCommonNotifications/EvodeskHeaderCommonNotificationsContainer.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,
        EvodeskNotificationsModule,
        EvodeskFeedModule,
    ],
    declarations: [
        EvodeskHeaderComponent,
        EvodeskHeaderContainerComponent,
        EvodeskHeaderCommonLogoComponent,
        EvodeskHeaderCommonMenuComponent,
        EvodeskHeaderCommonMenuContainerComponent,
        EvodeskHeaderCommonUserBalanceComponent,
        EvodeskHeaderCommonUserBalanceContainerComponent,
        EvodeskHeaderCommonProfileMenuComponent,
        EvodeskHeaderCommonProfileMenuContainerComponent,
        EvodeskHeaderCommonOpenMobileComponent,
        EvodeskHeaderCommonOpenMobileContainerComponent,
        EvodeskHeaderCommonSearchComponent,
        EvodeskHeaderCommonSearchContainerComponent,
        EvodeskHeaderCommonSubMenuComponent,
        EvodeskHeaderCommonSubMenuContainerComponent,
        EvodeskHeaderCommonLanguagesComponent,
        EvodeskHeaderCommonLanguagesContainerComponent,
        EvodeskHeaderCommonNotificationsComponent,
        EvodeskHeaderCommonNotificationsContainerComponent,
        EvodeskHeaderMobileMenuComponent,
        EvodeskHeaderMobileMenuContainerComponent,
        EvodeskHeaderMobileSocialNetworksComponent,
        EvodeskHeaderMobileLanguagesComponent,
        EvodeskHeaderMobileLanguagesContainerComponent,
        EvodeskHeaderMobileProfileMenuComponent,
        EvodeskHeaderMobileProfileMenuContainerComponent,
        EvodeskHeaderCommonSearchResultsComponent,
        EvodeskHeaderCommonSearchResultsContainerComponent,
    ],
    exports: [
        EvodeskHeaderContainerComponent,
    ],
})
export class EvodeskHeaderModule
{}
