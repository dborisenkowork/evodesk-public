import {Injectable} from '@angular/core';

import {Observable, BehaviorSubject} from 'rxjs';
import {take, filter} from 'rxjs/operators';

import {UserBalanceModel} from '../../../app/EvodeskRESTApi/models/user/User.model';
import {EvoCheckModel} from '../../../app/EvodeskRESTApi/models/billing/EvoCheck.model';

export interface EvodeskBillingState {
    balance: UserBalanceModel;
    evoChecks: Array<EvoCheckModel>;
    exchangeEvocoinToRubRate: number | undefined;
}

export type PreviousBillingState = EvodeskBillingState | undefined;
export type CurrentBillingState = EvodeskBillingState | undefined;

interface States {
    previous: PreviousBillingState;
    current: CurrentBillingState;
}

export function initialState(): States {
    return {
        previous: undefined,
        current: undefined,
    };
}

@Injectable()
export class EvodeskBillingStateService
{
    private _current$: BehaviorSubject<States> = new BehaviorSubject<States>(initialState());

    reset(): void {
        this._current$.next(initialState());
    }

    get current$(): Observable<States> {
        return this._current$.asObservable();
    }

    get whenReady$(): Observable<States> {
        return this._current$.pipe(
            filter(s => !! s.current),
            take(1),
        );
    }

    get snapshot(): States {
        return this._current$.getValue();
    }

    setState(reason: string, set$: (orig: EvodeskBillingState) => EvodeskBillingState) {
        const orig: EvodeskBillingState = this._current$.getValue().current;

        this._current$.next({
            previous: orig,
            current: set$(orig),
        });
    }
}
