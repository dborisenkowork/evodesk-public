import {Injectable} from '@angular/core';

import {forkJoin as observableForkJoin, Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskBillingState, EvodeskBillingStateService} from './EvodeskBillingState.service';
import {CurrentUserService} from '../../EvodeskAuth/services/CurrentUser.service';
import {BillingRESTService} from '../../../app/EvodeskRESTApi/services/BillingREST.service';

@Injectable()
export class EvodeskBillingBootstrapService
{
    constructor(
        private billingState: EvodeskBillingStateService,
        private billingRESTService: BillingRESTService,
        private currentUserService: CurrentUserService,
    ) {}

    bootstrap(): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((done) => {
            const queries: Array<Observable<any>> = [];

            const newBillingState: EvodeskBillingState = {
                balance: {
                    points: parseFloat(this.currentUserService.current.balance),
                    evocoin: parseFloat(this.currentUserService.current.balanceEvocoin),
                    rub: parseFloat(this.currentUserService.current.balanceRub),
                },
                evoChecks: [],
                exchangeEvocoinToRubRate: undefined,
            };

            queries.push(Observable.create(q => {
                this.billingRESTService.listOfEvoChecks().pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        newBillingState.evoChecks = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.billingRESTService.getEvocoinToRubExchangeRate().pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        newBillingState.exchangeEvocoinToRubRate = httpResponse.totalRub;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            observableForkJoin(queries).pipe(takeUntil(unsubscribe$)).subscribe(
                () => {
                    this.billingState.setState('EvodeskBillingBootstrapService.done', () => {
                        return newBillingState;
                    });

                    done.next(undefined);
                    done.complete();
                },
                (queryError) => {
                    done.error(queryError);
                },
            );

            queries.push(Observable.create(q => {

            }));

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
