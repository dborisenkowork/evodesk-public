import {NgModule} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil, filter} from 'rxjs/operators';

import {EvodeskBillingRoutingModule} from './EvodeskBillingRouting.module';

import {CurrentUserService} from '../EvodeskAuth/services/CurrentUser.service';
import {EvodeskBillingBootstrapService} from './state/EvodeskBillingBootstrap.service';

import {CheckRouteComponent} from './routes/CheckRoute/CheckRoute.component';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';
import {EvodeskBillingStateService} from './state/EvodeskBillingState.service';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskBillingRoutingModule,
    ],
    declarations: [
        CheckRouteComponent,
    ],
    providers: [
        EvodeskBillingBootstrapService,
    ],
})
export class EvodeskBillingModule
{
    private nextUser$: Subject<void> = new Subject<void>();

    constructor(
        private currentUserService: CurrentUserService,
        private billingState: EvodeskBillingStateService,
        private billingBootstrap: EvodeskBillingBootstrapService,
    ) {
        this.currentUserService.current$.pipe(filter(u => ! u)).subscribe(() => {
            this.billingState.reset();
        });

        this.currentUserService.current$.pipe(filter(u => !! u)).subscribe(() => {
            this.billingBootstrap.bootstrap().pipe(takeUntil(this.nextUser$)).subscribe();
        });
    }
}
