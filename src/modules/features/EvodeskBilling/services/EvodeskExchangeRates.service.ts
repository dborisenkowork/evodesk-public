
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';

import {EvodeskBillingStateService} from '../state/EvodeskBillingState.service';

export interface EvodeskExchangeRates {
    evocoinToRub: number;
}

@Injectable()
export class EvodeskExchangeRatesService
{
    private _current$: Observable<EvodeskExchangeRates> = this.state.current$.pipe(map(s => {
        if (s.current) {
            return <EvodeskExchangeRates>{
                evocoinToRub: s.current.exchangeEvocoinToRubRate,
            };
        } else {
            return <EvodeskExchangeRates>{
                evocoinToRub: 0,
            };
        }
    }));

    get current$(): Observable<EvodeskExchangeRates> {
        return this._current$;
    }

    constructor(
        private state: EvodeskBillingStateService,
    ) {}
}
