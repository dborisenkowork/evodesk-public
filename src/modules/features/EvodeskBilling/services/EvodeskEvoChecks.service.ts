import {Injectable} from '@angular/core';

import {Subject, Observable} from 'rxjs';
import {takeUntil, map} from 'rxjs/operators';

import {environment} from '../../../../environments/environment.config';

import {EvoCheckModel} from '../../../app/EvodeskRESTApi/models/billing/EvoCheck.model';

import {EvodeskBillingStateService} from '../state/EvodeskBillingState.service';
import {BillingRESTService} from '../../../app/EvodeskRESTApi/services/BillingREST.service';
import {EvodeskBillingBalanceService} from './EvodeskBillingBalance.service';

@Injectable()
export class EvodeskEvoChecksService
{
    private _current$: Observable<Array<EvoCheckModel>> = this.billingState.current$.pipe(map(s => {
        if (s.current) {
            return s.current.evoChecks;
        } else {
            return [];
        }
    }));

    get current$(): Observable<Array<EvoCheckModel>> {
        return this._current$;
    }

    constructor(
        private billingState: EvodeskBillingStateService,
        private billingBalanceService: EvodeskBillingBalanceService,
        private billingRESTService: BillingRESTService,
    ) {}

    getEvoCheckLink(evoCheck: EvoCheckModel): string {
        return this.getEvoCheckLinkViaCodeAndSecret(evoCheck.code, evoCheck.secret);
    }

    getEvoCheckLinkViaCodeAndSecret(code: string, secret: string): string {
        return `${environment.serverHostName}/billing/check?code=${code}&secret=${secret}`;
    }

    applyEvoCheck(code: string, secret: string): Observable<EvoCheckModel> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create(response => {
            this.billingRESTService.applyEvoCheck(code, secret).pipe(takeUntil(unsubscribe$)).subscribe(
                (httpResponse) => {
                    if (!! httpResponse.balance_sum) {
                        this.billingBalanceService.setBalance('EvodeskEvoChecksService.applyEvoCheck', (orig) => {
                            return {
                                ...orig,
                                balance: {
                                    ...orig.balance,
                                    evocoin: orig.balance.evocoin + parseFloat(httpResponse.balance_sum),
                                },
                            };
                        });

                        this.billingState.setState('EvodeskEvoChecksService.applyEvoCheck', (orig) => {
                            return {
                                ...orig,
                                evoChecks: orig.evoChecks.filter(e => e.id !== httpResponse.id),
                            };
                        });

                        response.next(httpResponse);
                        response.complete();
                    } else {
                        response.error('No balance_sum');
                    }
                },
                (httpError) => {
                    response.error(httpError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
