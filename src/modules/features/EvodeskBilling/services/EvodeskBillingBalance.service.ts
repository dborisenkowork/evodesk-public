import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import {take, map} from 'rxjs/operators';

import {UserBalanceModel} from '../../../app/EvodeskRESTApi/models/user/User.model';

import {EvodeskBillingStateService} from '../state/EvodeskBillingState.service';

export interface EvodeskBillingBalance {
    balance: UserBalanceModel;
}

@Injectable()
export class EvodeskBillingBalanceService
{
    private _current$: Observable<EvodeskBillingBalance> = this.state.current$.pipe(map(s => {
        if (s.current) {
            return <EvodeskBillingBalance>{
                balance: {
                    points: s.current.balance.points,
                    evocoin: s.current.balance.evocoin,
                    rub: s.current.balance.rub,
                },
            };
        } else {
            return <EvodeskBillingBalance>{
                balance: {
                    points: 0,
                    evocoin: 0,
                    rub: 0,
                },
            };
        }
    }));

    get current$(): Observable<EvodeskBillingBalance> {
        return this._current$;
    }

    constructor(
        private state: EvodeskBillingStateService,
    ) {}

    setBalance(reason: string, set$: (orig: EvodeskBillingBalance) => EvodeskBillingBalance): void {
        this._current$.pipe(take(1)).subscribe((origBalance) => {
            const newBalance: EvodeskBillingBalance = set$(origBalance);

            this.state.setState('EvodeskBillingBalanceService.setBalance', (orig) => {
                return {
                    ...orig,
                    balance: {
                        ...orig.balance,
                        rub: newBalance.balance.rub,
                        points: newBalance.balance.points,
                        evocoin: newBalance.balance.evocoin,
                    },
                };
            });
        });
    }
}
