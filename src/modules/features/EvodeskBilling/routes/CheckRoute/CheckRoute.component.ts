import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {EvodeskBillingStateService} from '../../state/EvodeskBillingState.service';
import {EvodeskEvoChecksService} from '../../services/EvodeskEvoChecks.service';

import * as moment from 'moment';

import {EvoCheckModel} from '../../../../app/EvodeskRESTApi/models/billing/EvoCheck.model';

@Component({
    template: ``,
})
export class CheckRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private uiAlertService: EvodeskAlertModalService,
        private billingState: EvodeskBillingStateService,
        private billingEvoCheckService: EvodeskEvoChecksService,
    ) {}

    ngOnInit(): void {
        this.activatedRoute.queryParams.pipe(takeUntil(this.ngOnDestroy$)).subscribe((params) => {
            if (params['code'] && params['secret']) {
                this.billingState.whenReady$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                    const success: Function = (evoCheck: EvoCheckModel) => {
                        this.uiAlertService.open({
                            title: {
                                text: 'EvodeskBilling.routes.CheckRoute.Success.Title',
                                translate: true,
                                replaces: [
                                    moment(evoCheck.created_at.date).format('LLL'),
                                ],
                            },
                            text: {
                                text: 'EvodeskBilling.routes.CheckRoute.Success.Message',
                                translate: true,
                                replaces: [
                                    evoCheck.balance_sum,
                                ],
                            },
                            ok: {
                                text: 'EvodeskBilling.routes.CheckRoute.Success.Close',
                                translate: true,
                            },
                        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                            this.router.navigate(['/']);
                        });
                    };

                    const fail: Function = () => {
                        this.uiAlertService.open({
                            title: {
                                text: 'EvodeskBilling.routes.CheckRoute.Fail.Title',
                                translate: true,
                            },
                            text: {
                                text: 'EvodeskBilling.routes.CheckRoute.Fail.Message',
                                translate: true,
                            },
                            ok: {
                                text: 'EvodeskBilling.routes.CheckRoute.Fail.Close',
                                translate: true,
                            },
                        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                            this.router.navigate(['/']);
                        });
                    };

                    this.billingEvoCheckService.applyEvoCheck(params['code'], params['secret']).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                        (evoCheck) => {
                            success(evoCheck);
                        },
                        () => {
                            fail();
                        },
                    );
                });
            } else {
                this.router.navigate(['/']);
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
