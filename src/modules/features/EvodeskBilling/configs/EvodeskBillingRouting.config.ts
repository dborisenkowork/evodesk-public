import {Routes} from '@angular/router';
import {CheckRouteComponent} from '../routes/CheckRoute/CheckRoute.component';

import {HasAuthTokenGuard} from '../../EvodeskAuth/guards/HasAuthToken.guard';
import {CurrentUserGuard} from '../../EvodeskAuth/guards/CurrentUser.guard';

export const EvodeskBillingRouting: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/',
    },
    {
        path: 'check',
        component: CheckRouteComponent,
        canActivate: [
            HasAuthTokenGuard,
            CurrentUserGuard,
        ],
    },
];
