import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskBillingRouting} from './configs/EvodeskBillingRouting.config';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskBillingRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskBillingRoutingModule
{
}
