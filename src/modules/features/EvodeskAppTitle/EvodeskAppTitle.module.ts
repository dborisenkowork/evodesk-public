import {NgModule} from '@angular/core';
import {EvodeskAppTitleService} from './services/EvodeskAppTitle.service';

@NgModule({
    providers: [
        EvodeskAppTitleService,
    ],
})
export class EvodeskAppTitleModule
{}
