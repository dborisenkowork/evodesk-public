import {Injectable} from '@angular/core';
import {Title} from '@angular/platform-browser';

import {of as observableOf, forkJoin as observableForkJoin, Subject, Observable, ConnectableObservable} from 'rxjs';
import {publishLast, takeUntil} from 'rxjs/operators';

import {TranslateService} from '@ngx-translate/core';

interface AppTitleSegment
{
    text: string;
    translate: boolean;
}

export type PostProcessCallback = (values: Array<string>) => string;

export function postProcessWithSeparator(separator: string): PostProcessCallback {
    return (values: Array<string>) => values.join(separator);
}

@Injectable()
export class EvodeskAppTitleService
{
    constructor(
        private title: Title,
        private translate: TranslateService,
    ) {}

    setTitle(options: {
        segments: Array<AppTitleSegment>,
        postProcess: PostProcessCallback,
     }): Observable<string> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        const observable: Observable<string> = Observable.create(observer => {
            observableForkJoin(options.segments.map(s => s.translate ? this.translate.get(s.text) : observableOf(s.text))).pipe(takeUntil(unsubscribe$)).subscribe(
                    results => {
                        const newTitle: string = options.postProcess(results);

                        this.title.setTitle(newTitle);

                        observer.next(newTitle);
                        observer.complete();
                    },
                    error => {
                        observer.error(error);
                    },
                );

            return () => {
                unsubscribe$.next(undefined);
            };
        });

        (observable.pipe(publishLast()) as ConnectableObservable<string>).connect();

        return observable;
    }
}
