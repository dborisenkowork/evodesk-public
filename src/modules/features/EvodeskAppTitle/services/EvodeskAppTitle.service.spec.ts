import {Injectable} from '@angular/core';
import {inject, TestBed} from '@angular/core/testing';
import {Title} from '@angular/platform-browser';

import {TranslateService} from '@ngx-translate/core';

import {Subject, Observable} from 'rxjs';
import {EvodeskAppTitleService, postProcessWithSeparator} from './EvodeskAppTitle.service';

const translateFixture: { [key: string]: string } = {
    foo: 'Translated:Foo',
    bar: 'Translated:Bar',
};

const titleServiceStub: {
    current: string;
    setTitle(newTitle: string): void;
} = {
    current: '',
    setTitle(newTitle: string): void {
        titleServiceStub.current = newTitle;
    },
};

@Injectable()
class TranslateServiceMock
{
    // noinspection JSUnusedGlobalSymbols
    get(values: string | Array<string>): Observable<string | Array<string>> {
        return Observable.create(observer => {
            setTimeout(() => {
                if (Array.isArray(values)) {
                    observer.next(values.map(v => translateFixture[v] || v));
                    observer.complete();
                } else {
                    observer.next(translateFixture[values] || values);
                    observer.complete();
                }
            });
        });
    }
}

describe('EvodeskApp/services/AppTitleService', () => {
    const afterEach$: Subject<void> = new Subject<void>();

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: EvodeskAppTitleService,
                    useFactory: (title: Title, translate: TranslateService) => {
                        return new EvodeskAppTitleService(title, translate);
                    },
                    deps: [Title, TranslateService],
                },
                { provide: TranslateService, useClass: TranslateServiceMock },
                { provide: Title, useValue: titleServiceStub },
            ],
        });
    });

    afterEach(() => {
        afterEach$.next(undefined);
    });

    it('will succesful apply title with single non-translated segment', (done) => {
        inject([EvodeskAppTitleService], (appTitle: EvodeskAppTitleService) => {
            const setTitleSpy: jasmine.Spy = spyOn(titleServiceStub, 'setTitle');

            expect(setTitleSpy).toHaveBeenCalledTimes(0);

            appTitle.setTitle({
                postProcess: postProcessWithSeparator(' / '),
                segments: [
                    { text: 'foo', translate: false },
                ],
            });

            setTimeout(() => {
                expect(titleServiceStub.current).toBe('foo');
                done();
            }, 0);
        })();
    });

    it('will succesful apply title with single translated segment', (done) => {
        inject([EvodeskAppTitleService], (appTitle: EvodeskAppTitleService) => {
            const setTitleSpy: jasmine.Spy = spyOn(titleServiceStub, 'setTitle');

            expect(setTitleSpy).toHaveBeenCalledTimes(0);

            appTitle.setTitle({
                postProcess: postProcessWithSeparator(' / '),
                segments: [
                    { text: 'foo', translate: true },
                ],
            });

            setTimeout(() => {
                expect(titleServiceStub.current).toBe('Translated:Foo');
                done();
            }, 0);
        })();
    });

    it('will succesful apply title with multiple non-translated segments', (done) => {
        inject([EvodeskAppTitleService], (appTitle: EvodeskAppTitleService) => {
            const setTitleSpy: jasmine.Spy = spyOn(titleServiceStub, 'setTitle');

            expect(setTitleSpy).toHaveBeenCalledTimes(0);

            appTitle.setTitle({
                postProcess: postProcessWithSeparator(' / '),
                segments: [
                    { text: 'foo', translate: false },
                    { text: 'bar', translate: false },
                    { text: 'foo', translate: false },
                ],
            });

            setTimeout(() => {
                expect(titleServiceStub.current).toBe('foo / bar / foo');
                done();
            }, 0);
        })();
    });

    it('will succesful apply title with multiple translated segments', (done) => {
        inject([EvodeskAppTitleService], (appTitle: EvodeskAppTitleService) => {
            const setTitleSpy: jasmine.Spy = spyOn(titleServiceStub, 'setTitle');

            expect(setTitleSpy).toHaveBeenCalledTimes(0);

            appTitle.setTitle({
                postProcess: postProcessWithSeparator(' / '),
                segments: [
                    { text: 'foo', translate: true },
                    { text: 'bar', translate: true },
                    { text: 'foo', translate: true },
                ],
            });

            setTimeout(() => {
                expect(titleServiceStub.current).toBe('Translated:Foo / Translated:Bar / Translated:Foo');
                done();
            }, 0);
        })();
    });

    it('will succesful apply title with mix of non-translated and translated segments', (done) => {
        inject([EvodeskAppTitleService], (appTitle: EvodeskAppTitleService) => {
            const setTitleSpy: jasmine.Spy = spyOn(titleServiceStub, 'setTitle');

            expect(setTitleSpy).toHaveBeenCalledTimes(0);

            appTitle.setTitle({
                postProcess: postProcessWithSeparator(' / '),
                segments: [
                    { text: 'foo', translate: true },
                    { text: 'bar', translate: true },
                    { text: 'foo', translate: false },
                ],
            });

            setTimeout(() => {
                expect(titleServiceStub.current).toBe('Translated:Foo / Translated:Bar / foo');
                done();
            }, 0);
        })();
    });
});
