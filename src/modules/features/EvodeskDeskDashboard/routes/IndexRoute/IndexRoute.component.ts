import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';

import {EvodeskHeaderLayoutVariant} from '../../../EvodeskHeader/models/EvodeskHeader.models';

import {EvodeskHeaderConfigurationService} from '../../../EvodeskHeader/services/EvodeskHeaderConfiguration.service';
import {DeskDashboardStateService} from '../../state/DeskDashboardState.service';
import {DeskDashboardBootstrapService} from '../../state/DeskDashboardBootstrap.service';

@Component({
    templateUrl: './IndexRoute.component.pug',
    styleUrls: [
        './IndexRoute.component.scss',
    ],
    providers: [
        DeskDashboardStateService,
        DeskDashboardBootstrapService,
    ],
})
export class IndexRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private headerConfiguration: EvodeskHeaderConfigurationService,
    ) {}

    ngOnInit(): void {
        this.headerConfiguration.setVariant(EvodeskHeaderLayoutVariant.Common);
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
