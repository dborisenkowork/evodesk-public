import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {interval as observableInterval, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {FilterMode, DeskDashboardStateService} from '../../state/DeskDashboardState.service';
import {DeskDashboardBootstrapService} from '../../state/DeskDashboardBootstrap.service';

type Section = '' | 'yours' | 'participating-in' | 'invitations';

@Component({
    templateUrl: './IndexRouteContents.component.pug',
    styleUrls: [
        './IndexRouteContents.component.scss',
    ],
})
export class IndexRouteContentsComponent implements OnInit, OnDestroy
{
    public static DEFAULT_UPDATE_INTERVAL_MS: number = 5000;

    private nextInvitesUpdate$: Subject<void> = new Subject<void>();
    private moveNextSection$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private cdr: ChangeDetectorRef,
        private activatedRoute: ActivatedRoute,
        private deskDashboardState: DeskDashboardStateService,
        private bootstrap: DeskDashboardBootstrapService,
    ) {}

    ngOnInit(): void {
        this.activatedRoute.params.pipe(takeUntil(this.ngOnDestroy$)).subscribe((params) => {
            const section: Section = <any>params['section'];

            this.moveNextSection$.next(undefined);

            switch (section) {
                default:
                    this.deskDashboardState.setState((orig) => {
                        return {
                            ...orig,
                            filterMode: FilterMode.Desks,
                        };
                    });

                    this.cdr.detectChanges();

                    break;

                case 'participating-in':
                    this.deskDashboardState.setState((orig) => {
                        return {
                            ...orig,
                            filterMode: FilterMode.Participating,
                        };
                    });

                    this.cdr.detectChanges();

                    break;

                case 'invitations':
                    this.deskDashboardState.setState((orig) => {
                        return {
                            ...orig,
                            filterMode: FilterMode.Invites,
                        };
                    });

                    this.bootstrap.refetchInvites().pipe(takeUntil(this.nextInvitesUpdate$)).subscribe();

                    observableInterval(IndexRouteContentsComponent.DEFAULT_UPDATE_INTERVAL_MS).pipe(takeUntil(this.moveNextSection$)).subscribe(() => {
                        this.nextInvitesUpdate$.next(undefined);

                        this.bootstrap.refetchInvites().pipe(
                            takeUntil(this.nextInvitesUpdate$))
                            .subscribe();
                    });

                    this.cdr.detectChanges();

                    break;
            }
        });
    }

    ngOnDestroy(): void {
        this.nextInvitesUpdate$.next(undefined);
        this.moveNextSection$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }
}

