import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskHeaderLayoutVariant} from '../../../EvodeskHeader/models/EvodeskHeader.models';

import {UserRESTService} from '../../../../app/EvodeskRESTApi/services/UserREST.service';
import {DeskDashboardRESTService} from '../../../../app/EvodeskRESTApi/services/DeskDashboardREST.service';
import {EvodeskHeaderConfigurationService} from '../../../EvodeskHeader/services/EvodeskHeaderConfiguration.service';

@Component({
    template: '',
})
export class InviteRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private userRESTService: UserRESTService,
        private deskRESTService: DeskDashboardRESTService,
        private headerHeaderConfiguration: EvodeskHeaderConfigurationService,
    ) {}

    ngOnInit(): void {
        this.headerHeaderConfiguration.setVariant(EvodeskHeaderLayoutVariant.Common);

        this.activatedRoute.queryParams.pipe(takeUntil(this.ngOnDestroy$)).subscribe((params) => {
            if (params['invite'] === '1' && params['deskId']) {
                const deskId: number = parseInt(params['deskId'], 10);

                this.userRESTService.current()
                    .subscribe(
                        (currentUser) => {
                            if (currentUser) {
                                this.deskRESTService.acceptInviteToDesk(deskId).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                                    () => {
                                        this.goDesk(deskId);
                                    },
                                    () => {
                                        this.goRegister();
                                    },
                                );
                            } else {
                                this.goRegister();
                            }
                        },
                        () => {
                            this.goRegister();
                        },
                    );
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    goDesk(deskId: number): void {
        this.router.navigate([`/desk/${deskId}`]);
    }

    goRegister(): void {
        this.router.navigate(['/']);
    }
}
