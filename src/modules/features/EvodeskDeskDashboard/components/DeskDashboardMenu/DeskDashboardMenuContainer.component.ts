import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskDashboardMenuComponentProps} from './DeskDashboardMenu.component';

import {FilterMode, DeskDashboardState, DeskDashboardStateService} from '../../state/DeskDashboardState.service';

type State = DeskDashboardMenuComponentProps;

@Component({
    selector: 'evodesk-desk-dashboard-menu-container',
    template: `
      <ng-container *ngIf="state">
        <evodesk-desk-dashboard-menu
          [props]="state"
          (toggleMenu)="toggleMenu()"
        ></evodesk-desk-dashboard-menu>
      </ng-container>
    `,
})
export class DeskDashboardMenuContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        filterMode: FilterMode.Desks,
        isOpened: false,
        numInvites: 0,
    };

    constructor(
        private deskDashboardState: DeskDashboardStateService,
    ) {}

    ngOnInit(): void {
        this.deskDashboardState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(next => {
            this.state = {
                ...this.state,
                filterMode: next.filterMode,
                isOpened: next.isMenuOpened,
                numInvites: next.invites.length,
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    toggleMenu(): void {
        this.deskDashboardState.setState((orig: DeskDashboardState) => {
            return {
                ...orig,
                isMenuOpened: !orig.isMenuOpened,
            };
        });
    }
}
