import {Component, EventEmitter, Input, Output} from '@angular/core';

import {FilterMode} from '../../state/DeskDashboardState.service';

export interface DeskDashboardMenuComponentProps
{
    isOpened: boolean;
    filterMode: FilterMode;
    numInvites: number;
}

@Component({
    selector: 'evodesk-desk-dashboard-menu',
    templateUrl: 'DeskDashboardMenu.component.pug',
    styleUrls: [
        './DeskDashboardMenu.component.scss',
    ],
})
export class DeskDashboardMenuComponent
{
    @Input() props: DeskDashboardMenuComponentProps;

    @Output() toggleMenu: EventEmitter<void> = new EventEmitter<void>();

    doToggleMenu(): void {
        this.toggleMenu.emit(undefined);
    }
}
