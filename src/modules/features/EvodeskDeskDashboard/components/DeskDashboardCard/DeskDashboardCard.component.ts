import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import {DeskId, DeskModel} from '../../../../app/EvodeskRESTApi/models/Desk.model';

export interface DeskDashboardCardComponentDeleteEvent
{
    id: DeskId;
}

export interface DeskDashboardCardComponentProps {
    desk: DeskModel;
}

interface State {
    ready: boolean;
    backdropNgStyle: any;
}

@Component({
    selector: 'evodesk-desk-dashboard-card',
    templateUrl: './DeskDashboardCard.component.pug',
    styleUrls: [
        './DeskDashboardCard.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeskDashboardCardComponent implements OnChanges, OnDestroy
{
    @Input() props: DeskDashboardCardComponentProps;

    @Output('openEdit') openEditEvent: EventEmitter<DeskModel> = new EventEmitter<DeskModel>();

    @Output('navigateToCard') navigateToCardEvent: EventEmitter<DeskModel> = new EventEmitter<DeskModel>();

    @Output('delete') deleteEvent: EventEmitter<DeskDashboardCardComponentDeleteEvent> = new EventEmitter<DeskDashboardCardComponentDeleteEvent>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        backdropNgStyle: {},
    };

    constructor(private cdr: ChangeDetectorRef) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    backdropNgStyle: {},
                };

                if (this.props.desk.color) {
                    this.state.backdropNgStyle['background-color'] = this.props.desk.color;
                }
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    navigateToCard(): void {
        this.navigateToCardEvent.emit(this.props.desk);
    }

    edit(): void {
        this.openEditEvent.emit(this.props.desk);
    }

    delete(): void {
        this.deleteEvent.emit({
            id: this.props.desk.id,
        });

        this.cdr.detectChanges();
    }
}
