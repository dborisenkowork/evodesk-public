import {Component, Input, OnChanges, SimpleChanges, ViewChild, ViewContainerRef} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';

import {merge, Observable, Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';

import {DeskModel} from '../../../../app/EvodeskRESTApi/models/Desk.model';

import {DeskDashboardCardComponentProps, DeskDashboardCardComponent} from './DeskDashboardCard.component';
import {DeskDashboardCardEditorContainerComponent} from '../DeskDashboardCardEditor/DeskDashboardCardEditorContainer.component';

import {EvodeskAppMessageBusService} from '../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';
import {EvodeskConfirmModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';
import {DeskDashboardRESTService} from '../../../../app/EvodeskRESTApi/services/DeskDashboardREST.service';
import {DeskDashboardAction, DeskDashboardStateService} from '../../state/DeskDashboardState.service';
import {AppEvent} from '../../../../app/EvodeskAppMessageBus/models/app-events.model';

interface State {
    ready: boolean;
    props?: DeskDashboardCardComponentProps;
}

@Component({
    selector: 'evodesk-desk-dashboard-card-container',
    template: `
        <ng-container *ngIf="!!state && state.ready">
          <evodesk-desk-dashboard-card
            #view
            [props]="state.props"
            (navigateToCard)="navigateToCard()"
            (delete)="onDelete($event)"
            (openEdit)="onEdit($event)"
          ></evodesk-desk-dashboard-card>
        </ng-container>
    `,
    styles: [`
        :host {
            width: 100%;
            height: 100%;
        }
    `],
})
export class DeskDashboardCardContainerComponent implements OnChanges
{
    @Input() desk: DeskModel;
    @Input() showModalOnInit: boolean = false;

    @ViewChild('view') view: DeskDashboardCardComponent;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private router: Router,
        private appBus: EvodeskAppMessageBusService,
        private uiConfirm: EvodeskConfirmModalService,
        private deskRESTService: DeskDashboardRESTService,
        private currentUserService: CurrentUserService,
        private dialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private deskDashboardState: DeskDashboardStateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['desk']) {
            if (this.desk) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        desk: this.desk,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    navigateToCard(): void {
        this.router.navigate(['/desk', this.desk.id]);
    }

    onEdit(desk: DeskModel) {
        this.dialog.open(DeskDashboardCardEditorContainerComponent,
                { ...DeskDashboardCardEditorContainerComponent.dialogConfig, viewContainerRef: this.viewContainerRef, data: { desk: desk } })
                .afterClosed().subscribe(result => {
            if (result) {
                const newDesk: DeskModel = {
                    ...desk,
                };
                const requests: Array<Observable<any>> = [];

                if (desk.name !== result.name) {
                    requests.push(this.deskRESTService.rename(this.desk.id, result.name).pipe(tap(value => newDesk.name = value.title )));
                }
                if (result.color && desk.color !== result.color) {
                    requests.push(this.deskRESTService.setColor(this.desk.id, result.color).pipe(tap(value => newDesk.color = value.color )));
                }
                for (const invite of result.inviteList) {
                    requests.push(this.deskRESTService.inviteToDesk(desk.id, invite.email).pipe(tap(value => {
                        if (value.status === 'ok') {
                            newDesk.users.push(value.user);
                        }

                    })));
                }
                for (const del of result.deleteList) {
                    requests.push(this.deskRESTService.deleteParticipant(desk.id, del.id).pipe(tap(value => {
                        newDesk.users = newDesk.users.filter(user => user.id !== value.user.id);
                    })));
                }
                for (const letter of result.letterList) {
                    requests.push(this.deskRESTService.inviteToEvodesk(desk.id, this.currentUserService.current.id, letter).pipe(tap(value => {
                        newDesk.users.push(value.user);
                    })));
                }

                merge(...requests).subscribe(
                    () => {},
                    (httpError) => {
                        this.appBus.dispatch({ type: AppEvent.HttpError, payload: httpError });
                    },
                    () => {
                        this.deskDashboardState.dispatch({ type: DeskDashboardAction.DeskUpdated, payload: { desk: newDesk } });
                    });
            }
        });
    }

    onDelete(): void {
        this.uiConfirm.open({
            title: { text: this.desk.name, translate: false },
            text: { text: 'EvodeskDeskDashboard.components.DeskDashboardCardContainer.UIDeleteConfirm.Text', translate: true },
            confirmButton: { text: 'EvodeskDeskDashboard.components.DeskDashboardCardContainer.UIDeleteConfirm.ConfirmButton', translate: true },
            cancelButton: { text: 'EvodeskDeskDashboard.components.DeskDashboardCardContainer.UIDeleteConfirm.CancelButton', translate: true },
            confirm: () => {
                this.deskRESTService.deleteDesk(this.desk.id).subscribe(
                    undefined,
                    (httpError) => {
                        this.appBus.dispatch({ type: AppEvent.HttpError, payload: httpError });
                    },
                );

                this.deskDashboardState.dispatch({ type: DeskDashboardAction.DeskDeleted, payload: { desk: this.desk } });
            },
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }
}
