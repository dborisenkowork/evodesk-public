import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {MatDialog} from '@angular/material';

import {Subject, Observable, merge} from 'rxjs';
import {concatMap, takeUntil, tap} from 'rxjs/operators';

import {DeskModel} from '../../../../app/EvodeskRESTApi/models/Desk.model';

import {DeskDashboardComponentProps, DeskDashboardScreen} from './DeskDashboard.component';
import {DeskDashboardCardEditorContainerComponent} from '../DeskDashboardCardEditor/DeskDashboardCardEditorContainer.component';

import {FilterMode, DeskDashboardStateService, DeskDashboardAction} from '../../state/DeskDashboardState.service';
import {EvodeskAppMessageBusService} from '../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';
import {DeskDashboardRESTService} from '../../../../app/EvodeskRESTApi/services/DeskDashboardREST.service';
import {DeskDashboardBootstrapService} from '../../state/DeskDashboardBootstrap.service';
import {AppEvent} from '../../../../app/EvodeskAppMessageBus/models/app-events.model';

interface State {
    ready: boolean;
    props?: DeskDashboardComponentProps;
}

@Component({
    selector: 'evodesk-desk-dashboard-container',
    template: `
        <ng-container *ngIf="!!state && state.ready">
          <evodesk-desk-dashboard
            [props]="state.props"
            (createNewDesk)="onCreateNewDesk()"
          ></evodesk-desk-dashboard>
        </ng-container>
    `,
})
export class DeskDashboardContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: {
            screen: DeskDashboardScreen.OwnOnly,
            invites: [],
            desks: [],
            isAddButtonDisabled: false,
            currentUserId: undefined,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private appBus: EvodeskAppMessageBusService,
        private bootstrap: DeskDashboardBootstrapService,
        private deskState: DeskDashboardStateService,
        private currentUserService: CurrentUserService,
        private rest: DeskDashboardRESTService,
        private dialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private stateService: DeskDashboardStateService,
    ) {}

    ngOnInit(): void {
        this.bootstrap.bootstrap().pipe(
            takeUntil(this.ngOnDestroy$))
        .subscribe(
            () => {
                this.state = {
                    ...this.state,
                    ready: true,
                };

                this.deskState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(next => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            invites: next.invites,
                            desks: next.desks,
                            currentUserId: this.currentUserService.current.id,
                        },
                    };

                    this.onFilterChange(next.filterMode);

                    this.cdr.detectChanges();
                });
            },
        );
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
        this.deskState.destroy();
    }

    onFilterChange(filter: FilterMode): void {
        switch (filter) {
            default:
                throw new Error(`Unknown filter "${filter}"`);

            case FilterMode.Desks:
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        screen: DeskDashboardScreen.OwnOnly,
                    },
                };

                break;

            case FilterMode.Participating:
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        screen: DeskDashboardScreen.Participating,
                    },
                };

                break;

            case FilterMode.Invites:
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        screen: DeskDashboardScreen.Invites,
                    },
                };

                break;
        }
    }

    onCreateNewDesk(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                isAddButtonDisabled: true,
            },
        };

        this.dialog.open(DeskDashboardCardEditorContainerComponent, {
                ...DeskDashboardCardEditorContainerComponent.dialogConfig, viewContainerRef: this.viewContainerRef})
                .afterClosed().subscribe(result => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    isAddButtonDisabled: false,
                },
            };
            if (result) {
                let newDesk: DeskModel;

                const obs: any = this.rest.createNewDesk(result.name).pipe(tap(value => {
                    newDesk = {
                        id: value.id,
                        invite_code: value.invite_code,
                        name: value.title,
                        avatar: null,
                        ownerId: this.currentUserService.current.id,
                        color: value.color,
                        users: [
                            {
                                id: this.currentUserService.current.id,
                                email: this.currentUserService.current.email,
                                active: true,
                                avatar: this.currentUserService.current.avatar,
                            },
                        ],
                    };
                }), concatMap(first => {
                    const requests: Array<Observable<any>> = [];

                    if (result.color) {
                        requests.push(this.rest.setColor(first.id, result.color).pipe(tap(value => {
                            newDesk.color = value.color;
                        })));
                    }
                    for (const invite of result.inviteList) {
                        requests.push(this.rest.inviteToDesk(newDesk.id, invite.email).pipe(tap(value => {
                            newDesk.users.push(value.user);
                        })));
                    }
                    for (const letter of result.letterList) {
                        requests.push(this.rest.inviteToEvodesk(newDesk.id, this.currentUserService.current.id, letter).pipe(tap(value => {
                            newDesk.users.push(value.user);
                        })));
                    }

                    return merge(...requests);
                }));

                obs.subscribe(
                    () => {},
                    (httpError) => {
                        this.state = {
                            ...this.state,
                            props: {
                                ...this.state.props,
                                isAddButtonDisabled: true,
                            },
                        };

                        this.appBus.dispatch({ type: AppEvent.HttpError, payload: httpError });
                    },
                    () => {
                        this.stateService.dispatch({ type: DeskDashboardAction.DeskCreated, payload: { desk: newDesk} });
                    },
                );
            }
        });
    }
}
