import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {DeskModel} from '../../../../app/EvodeskRESTApi/models/Desk.model';
import {DeskInviteModel} from '../../../../app/EvodeskRESTApi/models/DeskInvite.model';

export enum DeskDashboardScreen {
    Index = 'index',
    OwnOnly = 'own-only',
    Participating = 'participating',
    Invites = 'invites',
}

export interface DeskDashboardComponentProps
{
    screen: DeskDashboardScreen;
    desks: Array<DeskModel>;
    invites: Array<DeskInviteModel>;
    isAddButtonDisabled: boolean;
    currentUserId: number;
}

interface State {
    ready: boolean;
    filtered: Array<DeskModel>;
}

@Component({
    selector: 'evodesk-desk-dashboard',
    templateUrl: './DeskDashboard.component.pug',
    styleUrls: [
        './DeskDashboard.component.scss',
    ],
})
export class DeskDashboardComponent implements OnChanges
{
    @Input() props: DeskDashboardComponentProps;

    @Output('createNewDesk') createNewDeskEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        filtered: [],
    };

    isScreen(screenAsString: string): boolean {
        return this.props.screen === <any>screenAsString;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };

                switch (this.props.screen) {
                    default:
                        throw new Error(`Unknown screen "${this.props.screen}"`);

                    case DeskDashboardScreen.Index:
                        this.state = {
                            ...this.state,
                            filtered: [...this.props.desks],
                        };

                        break;

                    case DeskDashboardScreen.OwnOnly:
                        this.state = {
                            ...this.state,
                            filtered: this.props.desks.filter(m => m.ownerId === this.props.currentUserId),
                        };

                        break;

                    case DeskDashboardScreen.Participating:
                        this.state = {
                            ...this.state,
                            filtered: this.props.desks.filter(m => m.ownerId !== this.props.currentUserId),
                        };

                        break;

                    case DeskDashboardScreen.Invites:
                        break;
                }
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    trackByDeskId(index, item: DeskModel): any {
        return item.id;
    }

    createNewDesk(): void {
        this.createNewDeskEvent.emit(undefined);
    }
}
