import {Component, EventEmitter, Input, Output} from '@angular/core';

import {DeskInviteModel} from '../../../../app/EvodeskRESTApi/models/DeskInvite.model';

export interface DeskDashboardInviteCardComponentProps {
    invite: DeskInviteModel;
    decisionMade: boolean;
}

@Component({
    selector: 'evodesk-desk-dashboard-invite-card',
    templateUrl: './DeskDashboardInviteCard.component.pug',
    styleUrls: [
        './DeskDashboardInviteCard.component.scss',
    ],
})
export class DeskDashboardInviteCardComponent
{
    @Input('props') props: DeskDashboardInviteCardComponentProps;

    @Output('navigateToDesk') navigateToDeskEvent: EventEmitter<DeskInviteModel> = new EventEmitter<DeskInviteModel>();

    @Output('accept') acceptEvent: EventEmitter<DeskInviteModel> = new EventEmitter<DeskInviteModel>();
    @Output('decline') declineEvent: EventEmitter<DeskInviteModel> = new EventEmitter<DeskInviteModel>();

    navigateToDesk(): void {
        this.navigateToDeskEvent.emit(this.props.invite);
    }

    accept(): void {
        this.acceptEvent.emit(this.props.invite);
    }

    decline(): void {
        this.declineEvent.emit(this.props.invite);
    }
}
