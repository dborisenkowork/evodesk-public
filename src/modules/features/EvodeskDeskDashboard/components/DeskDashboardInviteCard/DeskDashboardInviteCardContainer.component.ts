import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {Router} from '@angular/router';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskDashboardAction, DeskDashboardStateService} from '../../state/DeskDashboardState.service';

import {DeskDashboardInviteCardComponentProps} from './DeskDashboardInviteCard.component';

import {EvodeskAppMessageBusService} from '../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';
import {EvodeskConfirmModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {DeskDashboardRESTService} from '../../../../app/EvodeskRESTApi/services/DeskDashboardREST.service';
import {DeskInviteModel} from '../../../../app/EvodeskRESTApi/models/DeskInvite.model';
import {AppEvent} from '../../../../app/EvodeskAppMessageBus/models/app-events.model';

interface State
{
    ready: boolean;
    props?: DeskDashboardInviteCardComponentProps;
}

@Component({
    selector: 'evodesk-desk-dashboard-invite-card-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-desk-dashboard-invite-card
          [props]="state.props"
          (navigateToDesk)="navigateToDesk()"
          (accept)="accept()"
          (decline)="decline()"
        ></evodesk-desk-dashboard-invite-card>
      </ng-container>
    `,
})
export class DeskDashboardInviteCardContainerComponent implements OnDestroy, OnChanges
{
    @Input('invite') invite: DeskInviteModel;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private router: Router,
        private appBus: EvodeskAppMessageBusService,
        private uiConfirm: EvodeskConfirmModalService,
        private deskDashboardStateService: DeskDashboardStateService,
        private deskRESTService: DeskDashboardRESTService,
        private deskDashboardState: DeskDashboardStateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['invite']) {
            if (this.invite) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        invite: this.invite,
                        decisionMade: false,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    accept(options: { doNavigate: boolean } = { doNavigate: false }): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                decisionMade: true,
            },
        };

        Observable.create((o) => {
            this.deskRESTService.acceptInviteToDesk(this.invite.id).subscribe(
                (httpResponse) => {
                    this.deskDashboardStateService.setState((orig) => {
                        return {
                            ...orig,
                            desks: [...orig.desks, httpResponse.desk],
                        };
                    });

                    o.next(undefined);
                    o.complete();
                },
                (httpError) => {
                    this.appBus.dispatch({ type: AppEvent.HttpError, payload: httpError });

                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            decisionMade: false,
                        },
                    };
                },
            );
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.deskDashboardState.dispatch({ type: DeskDashboardAction.DeskInviteAccepted, payload: { invite: this.invite } });

            if (options.doNavigate) {
                this.router.navigateByUrl(`/desk/${this.invite.id}`);
            }
        });
    }

    decline(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                decisionMade: true,
            },
        };

        Observable.create((o) => {
            this.deskRESTService.declineInviteToDesk(this.invite.id).subscribe(
                () => {
                    o.next(undefined);
                    o.complete();
                },
                (httpError) => {
                    this.appBus.dispatch({ type: AppEvent.HttpError, payload: httpError });

                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            decisionMade: false,
                        },
                    };
                },
            );
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.deskDashboardState.dispatch({ type: DeskDashboardAction.DeskInviteDeclined, payload: { invite: this.invite } });
        });
    }

    navigateToDesk(): void {
        this.uiConfirm.open({
            title: { text: this.invite.name, translate: false },
            text: { text: 'EvodeskDeskDashboard.components.DeskDashboardInviteCardContainer.UIConfirm.Text', translate: true },
            confirmButton: { text: 'EvodeskDeskDashboard.components.DeskDashboardInviteCardContainer.UIConfirm.ConfirmButton', translate: true },
            cancelButton: { text: 'EvodeskDeskDashboard.components.DeskDashboardInviteCardContainer.UIConfirm.CancelButton', translate: true },
            confirm: () => {
                this.accept({
                    doNavigate: true,
                });
            },
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }
}

