import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild,
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, takeUntil} from 'rxjs/operators';

import {environment} from '../../../../../environments/environment.config';

import {UserId} from '../../../../app/EvodeskRESTApi/models/user/User.model';
import {DeskModel, DeskModelColor} from '../../../../app/EvodeskRESTApi/models/Desk.model';
import {evodeskDeskDefaultPalette} from '../../../EvodeskDesk/configs/EvodeskDesk.constants';
import {DeskParticipantModel} from '../../../../app/EvodeskRESTApi/models/DeskParticipant.model';

export interface ParticipantEmailEvent
{
    email: string;
}

export interface ParticipantUserEvent
{
    user: DeskParticipantModel;
}

export type DeskDashboardCardComponentParticipantEvent = ParticipantEmailEvent | ParticipantUserEvent;

export interface DeskDashboardCardSaveEvent {
    name: string;
    color?: string;
    inviteList: Array<DeskParticipantModel>;
    deleteList: Array<DeskParticipantModel>;
    letterList: Array<string>;
}

export interface DeskDashboardCardEditorComponentProps {
    desk: DeskModel;
    inviteLink: string;
    currentUserId: number;
    usersToDisplay: Array<DeskParticipantModel>;
    searchResults: Array<DeskParticipantModel>;
    searchInProgress: boolean;
}

interface State {
    ready: boolean;
    isInviteSectionEnabled: boolean;
    isInviteLinkCopied: boolean;
    availableColors: Array<DeskModelColor>;
}

@Component({
    selector: 'evodesk-desk-dashboard-card-editor',
    templateUrl: './DeskDashboardCardEditor.component.pug',
    styleUrls: [
        './DeskDashboardCardEditor.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeskDashboardCardEditorComponent implements OnInit, OnChanges, OnDestroy
{
    public static MIN_LENGTH: number = 3;
    public static DEBOUNCE: number = 300;

    @Input() props: DeskDashboardCardEditorComponentProps;

    @Output('close') closeEvent: EventEmitter<DeskDashboardCardSaveEvent> = new EventEmitter<DeskDashboardCardSaveEvent>();

    @Output('openEdit') openEditEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output('navigateToProfile') navigateToProfileEvent: EventEmitter<UserId> = new EventEmitter<UserId>();

    @Output('sendInvite') sendInviteEvent: EventEmitter<DeskDashboardCardComponentParticipantEvent> = new EventEmitter<DeskDashboardCardComponentParticipantEvent>();
    @Output('deleteParticipant') deleteParticipantEvent: EventEmitter<DeskParticipantModel> = new EventEmitter<DeskParticipantModel>();
    @Output('search') searchEvent: EventEmitter<string> = new EventEmitter<string>();
    @Output('showMore') showMoreEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('sendInviteToInput') sendInviteToInputRef: ElementRef;
    @ViewChild('copyLink') copyLinkRef: ElementRef;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        isInviteSectionEnabled: false,
        isInviteLinkCopied: false,
        availableColors: evodeskDeskDefaultPalette.map(c => c.color),
    };

    public form: FormGroup = this.fb.group({
        title: ['', [Validators.maxLength(32)]],
        color: ['', [Validators.minLength(7), Validators.maxLength(7)]],
        sendInviteTo: ['', [Validators.required, Validators.email]],
    });

    constructor(
        private fb: FormBuilder,
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnInit() {
        this.form.get('sendInviteTo').valueChanges.pipe(takeUntil(this.ngOnDestroy$),
            filter(v => !v || v.length === 0)).subscribe(next => {
            this.searchEvent.emit(next);
        });

        this.form.get('sendInviteTo').valueChanges.pipe(takeUntil(this.ngOnDestroy$),
                distinctUntilChanged(),
                filter(v => !!v && v.length > DeskDashboardCardEditorComponent.MIN_LENGTH),
                debounceTime(DeskDashboardCardEditorComponent.DEBOUNCE)).subscribe(next => {
            this.searchEvent.emit(next);
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props && this.props.desk) {
                this.state = {
                    ...this.state,
                };

                if (this.form.pristine) {
                    this.form.patchValue({
                        'title': this.props.desk.name,
                        'color': this.props.desk.color,
                    });
                }
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    navigateToProfile(profileId: UserId): void {
        if (profileId) {
            this.navigateToProfileEvent.emit(profileId);
        }
    }

    isInactive(user: DeskParticipantModel): boolean {
        return this.props.usersToDisplay.find(value => value.id === user.id) !== undefined || this.props.currentUserId === user.id;
    }

    openInviteSection(): void {
        this.state = {
            ...this.state,
            isInviteSectionEnabled: true,
        };

        window.requestAnimationFrame(() => {
            this.sendInviteToInputRef.nativeElement.focus();
        });
    }

    setColor(color: DeskModelColor): void {
        this.form.patchValue({
            'color': color,
        });

        this.cdr.detectChanges();
    }

    selectUser(user: DeskParticipantModel) {
        if (!this.isInactive(user)) {
            this.sendInviteEvent.emit({
                user: user,
            });

            this.form.get('sendInviteTo').setValue('');

            this.searchEvent.emit('');
        }
    }

    cancel() {
        this.closeEvent.emit();
    }

    save() {
        const inviteList: Array<DeskParticipantModel> = this.props.usersToDisplay.filter(value =>
                value.id && (!this.props.desk || !this.props.desk.users.find(user => user.id === value.id)));
        const deleteList: Array<DeskParticipantModel> = this.props.desk ? this.props.desk.users.filter(user =>
            !this.props.usersToDisplay.find(value => user.id === value.id)) : [];
        const letterList: Array<string> = this.props.usersToDisplay.filter(value => !value.id).map(value => value.email);

        this.closeEvent.emit({ name: this.form.get('title').value, color: this.form.get('color').value,
                inviteList: inviteList, deleteList: deleteList, letterList: letterList });
    }

    sendInvite(): void {
        if (this.form.get('sendInviteTo').valid) {

            this.sendInviteEvent.emit({
                email: this.form.get('sendInviteTo').value,
            });

            this.form.get('sendInviteTo').setValue('');

            window.requestAnimationFrame(() => {
                this.sendInviteToInputRef.nativeElement.focus();
            });
        }
    }

    copyInviteLink(): void {
        this.state = {
            ...this.state,
            isInviteLinkCopied: true,
        };

        (this.copyLinkRef.nativeElement as HTMLInputElement).focus();
        (this.copyLinkRef.nativeElement as HTMLInputElement).setSelectionRange(0, this.props.inviteLink.length);

        document.execCommand('Copy');
    }

    deleteParticipant(p: DeskParticipantModel): void {
        this.deleteParticipantEvent.emit(p);
    }

    getParticipantNgStylesFor(p: DeskParticipantModel): any {
        if (p.avatar) {
            return {
                'background-image': `url(${environment.modules.EvoDeskRESTApi.apiEndpoint}/profile/${p.id}/avatar)`,
            };
        } else {
            return {};
        }
    }
}
