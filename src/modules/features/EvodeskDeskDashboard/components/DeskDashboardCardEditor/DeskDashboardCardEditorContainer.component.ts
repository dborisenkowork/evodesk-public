import {Component, Inject, Input} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogConfig, MatDialogRef} from '@angular/material';

import {TranslateService} from '@ngx-translate/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskDashboardCardEditorComponentProps, DeskDashboardCardComponentParticipantEvent, DeskDashboardCardSaveEvent, ParticipantEmailEvent, ParticipantUserEvent} from './DeskDashboardCardEditor.component';

import {environment} from '../../../../../environments/environment.config';

import {AppEvent} from '../../../../app/EvodeskAppMessageBus/models/app-events.model';
import {DeskParticipantModel} from '../../../../app/EvodeskRESTApi/models/DeskParticipant.model';

import {EvodeskAppMessageBusService} from '../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';
import {EvodeskConfirmModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';
import {DeskRESTService} from '../../../../app/EvodeskRESTApi/services/DeskREST.service';

interface State {
    ready: boolean;
    searchOffset: number;
    searchQuery?: string;
    props?: DeskDashboardCardEditorComponentProps;
}

@Component({
    selector: 'evodesk-desk-dashboard-card-container-editor',
    template: `
        <ng-container>
          <evodesk-desk-dashboard-card-editor
            [props]="state.props"
            (sendInvite)="onSendInvite($event)"
            (deleteParticipant)="onDeleteParticipant($event)"
            (close)="close($event)"
            (search)="onSearch($event)"
            (showMore)="onShowMore()"
          ></evodesk-desk-dashboard-card-editor>
        </ng-container>
    `,
    styles: [`
        :host {
            width: 100%;
            height: 100%;
        }
    `],
})
export class DeskDashboardCardEditorContainerComponent
{
    public static dialogConfig: MatDialogConfig = {
        panelClass: '__evodesk-mat-dialog-desk-editor', backdropClass: '__evodesk-mat-dialog-transparent-backdrop',
    };

    @Input() showModalOnInit: boolean = false;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        searchOffset: 0,
    };

    constructor(
        private appBus: EvodeskAppMessageBusService,
        private translate: TranslateService,
        private uiConfirm: EvodeskConfirmModalService,
        private deskRESTService: DeskRESTService,
        private currentUserService: CurrentUserService,
        private dialogRef: MatDialogRef<DeskDashboardCardEditorContainerComponent, DeskDashboardCardSaveEvent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        this.state = {
            ...this.state,
            props: {
                desk: (data && data.desk) ? data.desk : undefined,
                inviteLink: (data && data.desk) ? `${environment.serverHostName}/desk/join/${data.desk.id}/${data.desk.invite_code}` : undefined,
                currentUserId: this.currentUserService.current.id,
                searchInProgress: false,
                searchResults: [],
                usersToDisplay: (data && data.desk) ? data.desk.users : [],
            },
        };
    }

    onSendInvite($event: DeskDashboardCardComponentParticipantEvent): void {
        const user: DeskParticipantModel = (<ParticipantUserEvent>$event).user;
        const email: string = (<ParticipantEmailEvent>$event).email;

        if (this.state.props.usersToDisplay.find(value => user && value.id === user.id || value.email === email ||
                user && this.currentUserService.current.id === user.id || this.currentUserService.current.email === email)) {
            if (this.state.props.desk && this.state.props.desk.users.filter(u => user && u.id === user.id || u.email === email).length > 0) {
                this.translate.get('EvodeskDeskDashboard.components.DeskDashboardCardContainer.UIUserIsAlreadyParticipant').pipe(
                    takeUntil(this.ngOnDestroy$))
                    .subscribe((message) => {
                        window.alert(message);
                    });
            }
            return;
        }

        if (!user) {
            this.deskRESTService.findUser(email).subscribe(
                (response) => {
                    const existingUsers: Array<DeskParticipantModel> = response.filter(value => value.email === email);

                    if (existingUsers.length > 0) {
                        this.state = {
                            ...this.state,
                            props: {
                                ...this.state.props,
                                usersToDisplay: this.state.props.usersToDisplay.concat(existingUsers[0]),
                            },
                        };
                    } else {
                        this.uiConfirm.open({
                            title: { text: email, translate: false },
                            text: { text: 'EvodeskDeskDashboard.components.DeskDashboardCardContainer.UIInviteUserToEvodesk.Text', translate: true },
                            confirmButton: { text: 'EvodeskDeskDashboard.components.DeskDashboardCardContainer.UIInviteUserToEvodesk.ConfirmButton', translate: true },
                            cancelButton: { text: 'EvodeskDeskDashboard.components.DeskDashboardCardContainer.UIInviteUserToEvodesk.CancelButton', translate: true },
                            confirm: () => {

                                this.state = {
                                    ...this.state,
                                    props: {
                                        ...this.state.props,
                                        usersToDisplay: this.state.props.usersToDisplay.concat({ id: undefined, active: false, email: email, avatar: undefined }),
                                    },
                                };
                            },
                        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
                    }
                },
                (httpError) => {
                    this.appBus.dispatch({ type: AppEvent.HttpError, payload: httpError });
                },
            );
        } else {
            this.state = {
                ...this.state,

                props: {
                    ...this.state.props,
                    usersToDisplay: this.state.props.usersToDisplay.concat(user),
                },
            };
        }
    }

    onDeleteParticipant($event: DeskParticipantModel): void {
        this.state = {
            ...this.state,

            props: {
                ...this.state.props,
                usersToDisplay: this.state.props.usersToDisplay.filter(value => value.id !== $event.id),
            },
        };
    }

    onSearch(value: string) {
        if (value.length === 0) {
            this.state = {
                ...this.state,
                searchOffset: 0,
                searchQuery: undefined,
                props: {
                    ...this.state.props,
                    searchResults: [],
                },
            };
        } else {
            this.search(value, true);
        }
    }

    onShowMore() {
        this.state = {
            ...this.state,
            searchOffset: this.state.searchOffset + 1,
        };

        this.search(this.state.searchQuery, false);
    }

    search(value: string, clearResults: boolean) {
        this.state = {
            ...this.state,
            searchOffset: clearResults ? 0 : this.state.searchOffset,
            searchQuery: value,
            props: {
                ...this.state.props,
                searchInProgress: true,
            },
        };

        this.deskRESTService.findUser(value, this.state.searchOffset).subscribe(next => {

            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    searchResults: clearResults ? next : [ ...this.state.props.searchResults, ...next ],
                    searchInProgress: false,
                },
            };
        });
    }

    close(result?: DeskDashboardCardSaveEvent) {
        this.dialogRef.close(result);
    }
}
