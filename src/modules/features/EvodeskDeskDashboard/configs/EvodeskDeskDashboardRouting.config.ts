import {Routes} from '@angular/router';

import {HasAuthTokenGuard} from '../../EvodeskAuth/guards/HasAuthToken.guard';
import {CurrentUserGuard} from '../../EvodeskAuth/guards/CurrentUser.guard';

import {IndexRouteComponent} from '../routes/IndexRoute/IndexRoute.component';
import {InviteRouteComponent} from '../routes/InviteRoute/InviteRoute.component';
import {EvodeskSetCommonHeaderGuard} from '../../EvodeskHeader/guards/EvodeskSetCommonHeader.guard';
import {IndexRouteContentsComponent} from '../routes/IndexRouteContents/IndexRouteContents.component';

export const EvodeskDeskDashboardRouting: Routes = [
    {
        path: 'invite',
        pathMatch: 'full',
        component: InviteRouteComponent,
    },
    {
        path: '',
        pathMatch: 'prefix',
        component: IndexRouteComponent,
        canActivate: [
            HasAuthTokenGuard,
            CurrentUserGuard,
            EvodeskSetCommonHeaderGuard,
        ],
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'yours',
            },
            {
                path: ':section',
                pathMatch: 'full',
                component: IndexRouteContentsComponent,
            },
        ],
    },
];
