import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskDeskRoutingModule} from './EvodeskDeskDashboardRouting.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';
import {InviteRouteComponent} from './routes/InviteRoute/InviteRoute.component';
import {IndexRouteContentsComponent} from './routes/IndexRouteContents/IndexRouteContents.component';

import {DeskDashboardComponent} from './components/DeskDashboard/DeskDashboard.component';
import {DeskDashboardContainerComponent} from './components/DeskDashboard/DeskDashboardContainer.component';
import {DeskDashboardMenuComponent} from './components/DeskDashboardMenu/DeskDashboardMenu.component';
import {DeskDashboardMenuContainerComponent} from './components/DeskDashboardMenu/DeskDashboardMenuContainer.component';
import {DeskDashboardCardComponent} from './components/DeskDashboardCard/DeskDashboardCard.component';
import {DeskDashboardCardContainerComponent} from './components/DeskDashboardCard/DeskDashboardCardContainer.component';
import {DeskDashboardCardEditorComponent} from './components/DeskDashboardCardEditor/DeskDashboardCardEditor.component';
import {DeskDashboardCardEditorContainerComponent} from './components/DeskDashboardCardEditor/DeskDashboardCardEditorContainer.component';
import {DeskDashboardInviteCardComponent} from './components/DeskDashboardInviteCard/DeskDashboardInviteCard.component';
import {DeskDashboardInviteCardContainerComponent} from './components/DeskDashboardInviteCard/DeskDashboardInviteCardContainer.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskDeskRoutingModule,
    ],
    declarations: [
        IndexRouteComponent,
        InviteRouteComponent,
        IndexRouteContentsComponent,

        DeskDashboardComponent,
        DeskDashboardContainerComponent,
        DeskDashboardMenuComponent,
        DeskDashboardMenuContainerComponent,
        DeskDashboardCardComponent,
        DeskDashboardCardEditorComponent,
        DeskDashboardCardEditorContainerComponent,
        DeskDashboardCardContainerComponent,
        DeskDashboardInviteCardComponent,
        DeskDashboardInviteCardContainerComponent,
    ],
    entryComponents: [
        DeskDashboardCardEditorContainerComponent,
    ],
})
export class EvodeskDeskDashboardModule
{}
