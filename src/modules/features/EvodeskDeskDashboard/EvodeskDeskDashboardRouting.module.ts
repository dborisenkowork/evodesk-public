import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskDeskDashboardRouting} from './configs/EvodeskDeskDashboardRouting.config';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskDeskDashboardRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskDeskRoutingModule
{}
