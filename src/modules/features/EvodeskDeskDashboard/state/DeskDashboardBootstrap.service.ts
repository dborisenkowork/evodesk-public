import {Injectable} from '@angular/core';

import {forkJoin as observableForkJoin, Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskDashboardStateService} from './DeskDashboardState.service';
import {DeskDashboardRESTService} from '../../../app/EvodeskRESTApi/services/DeskDashboardREST.service';
import {DeskInviteModel} from '../../../app/EvodeskRESTApi/models/DeskInvite.model';

@Injectable()
export class DeskDashboardBootstrapService
{
    constructor(
        private state: DeskDashboardStateService,
        private deskDashboardRESTService: DeskDashboardRESTService,
    ) {}

    bootstrap(): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((done) => {
            const queries: Array<Observable<any>> = [];

            queries.push(Observable.create(q => {
                this.deskDashboardRESTService.desks().pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        this.state.setState((orig) => {
                            return {
                                ...orig,
                                desks: [...httpResponse.availableDesks, ...httpResponse.myDesks],
                            };
                        });

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.deskDashboardRESTService.invites().pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        this.state.setState((orig) => {
                            return {
                                ...orig,
                                invites: httpResponse,
                            };
                        });

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            observableForkJoin(queries).pipe(takeUntil(unsubscribe$)).subscribe(
                () => {
                    done.next(undefined);
                    done.complete();
                },
                (queryError) => {
                    done.error(queryError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    refetchInvites(): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((done) => {
            const queries: Array<Observable<any>> = [];

            queries.push(Observable.create(q => {
                this.deskDashboardRESTService.invites().pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        this.state.setState((orig) => {
                            const setInvites: Array<DeskInviteModel> = [];
                            const currentIds: Array<number> = orig.invites.map(i => i.id);

                            httpResponse.forEach((newInvite) => {
                                if (!!~currentIds.indexOf(newInvite.id)) {
                                    setInvites.push(orig.invites.filter(n => n.id === newInvite.id)[0]);
                                } else {
                                    setInvites.push(newInvite);
                                }
                            });

                            return {
                                ...orig,
                                invites: setInvites,
                            };
                        });

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            observableForkJoin(queries).pipe(takeUntil(unsubscribe$)).subscribe(
                () => {
                    done.next(undefined);
                    done.complete();
                },
                (queryError) => {
                    done.error(queryError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
