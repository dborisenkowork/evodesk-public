import {Injectable} from '@angular/core';

import {Observable, BehaviorSubject, Subject} from 'rxjs';

import {DeskInviteModel} from '../../../app/EvodeskRESTApi/models/DeskInvite.model';
import {DeskModel} from '../../../app/EvodeskRESTApi/models/Desk.model';
import {DeskParticipantModel} from '../../../app/EvodeskRESTApi/models/DeskParticipant.model';

export enum FilterMode
{
    Desks = <any>'desks',
    Participating = <any>'participating',
    Invites = <any>'invites',
}

export interface DeskDashboardState
{
    filterMode: FilterMode;
    isMenuOpened: boolean;
    desks: Array<DeskModel>;
    invites: Array<DeskInviteModel>;
}

function initialState(): DeskDashboardState {
    return {
        filterMode: FilterMode.Desks,
        isMenuOpened: true,
        desks: [],
        invites: [],
    };
}

export enum DeskDashboardAction {
    DeskInviteAccepted,
    DeskInviteDeclined,
    DeskCreated,
    DeskUpdated,
    DeskDeleted,
    DeskSendInvite,
    DeskDeleteParticipant,
}

type Actions =
      { type: DeskDashboardAction.DeskInviteAccepted, payload: { invite: DeskInviteModel; } }
    | { type: DeskDashboardAction.DeskInviteDeclined, payload: { invite: DeskInviteModel; } }
    | { type: DeskDashboardAction.DeskCreated, payload: { desk: DeskModel; } }
    | { type: DeskDashboardAction.DeskUpdated, payload: { desk: DeskModel; } }
    | { type: DeskDashboardAction.DeskDeleted, payload: { desk: DeskModel; } }
    | { type: DeskDashboardAction.DeskSendInvite, payload: { desk: DeskModel; user: DeskParticipantModel; email: string; } }
    | { type: DeskDashboardAction.DeskDeleteParticipant, payload: { desk: DeskModel; paricipant: DeskParticipantModel; } }
;

@Injectable()
export class DeskDashboardStateService
{
    private destroy$: Subject<void> = new Subject<void>();

    private _current$: BehaviorSubject<DeskDashboardState> = new BehaviorSubject<DeskDashboardState>(initialState());

    dispatch(action: Actions): void {
        switch (action.type) {
            case DeskDashboardAction.DeskInviteAccepted: {
                this.setState((orig: DeskDashboardState) => {
                    return {
                        ...orig,
                        invites: orig.invites.filter(i => i.id !== action.payload.invite.id),
                    };
                });

                break;
            }

            case DeskDashboardAction.DeskInviteDeclined: {
                this.setState((orig: DeskDashboardState) => {
                    return {
                        ...orig,
                        invites: orig.invites.filter(i => i.id !== action.payload.invite.id),
                    };
                });

                break;
            }

            case DeskDashboardAction.DeskCreated: {
                this.setState((orig: DeskDashboardState) => {
                    return {
                        ...orig,
                        desks: [...orig.desks, action.payload.desk],
                    };
                });

                break;
            }

            case DeskDashboardAction.DeskUpdated: {
                this.setState((orig: DeskDashboardState) => {
                    return {
                        ...orig,
                        desks: orig.desks.map(m => {
                            if (m.id === action.payload.desk.id) {
                                return action.payload.desk;
                            } else {
                                return m;
                            }
                        }),
                    };
                });

                break;
            }

            case DeskDashboardAction.DeskSendInvite: {
                this.setState((orig: DeskDashboardState) => {
                    return {
                        ...orig,
                        desks: orig.desks.map(m => {
                            if (m.id === action.payload.desk.id) {
                                return {
                                    ...action.payload.desk,
                                    users: [
                                        ...action.payload.desk.users,
                                        action.payload.user,
                                    ],
                                };
                            } else {
                                return m;
                            }
                        }),
                    };
                });

                break;
            }

            case DeskDashboardAction.DeskDeleteParticipant: {
                this.setState((orig: DeskDashboardState) => {
                    return {
                        ...orig,
                        desks: orig.desks.map(m => {
                            if (m.id === action.payload.desk.id) {
                                return {
                                    ...m,
                                    users: m.users.filter(u => u.id !== action.payload.paricipant.id),
                                };
                            } else {
                                return m;
                            }
                        }),
                    };
                });

                break;
            }
        }
    }

    get current$(): Observable<DeskDashboardState> {
        return this._current$.asObservable();
    }

    get snapshot(): DeskDashboardState {
        return this._current$.getValue();
    }

    setState(query: (orig: DeskDashboardState) => DeskDashboardState) {
        const prev: DeskDashboardState = this._current$.getValue();

        this._current$.next(query(prev));
    }

    reset(): void {
        this.setState(() => {
            return initialState();
        });
    }

    destroy(): void {
        this.destroy$.next(undefined);
    }
}
