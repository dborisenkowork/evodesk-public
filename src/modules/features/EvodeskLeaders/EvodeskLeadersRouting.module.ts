import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskLeadersRouting} from './configs/EvodeskLeadersRouting.module';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskLeadersRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskLeadersRoutingModule
{}
