import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskLeadersRoutingModule} from './EvodeskLeadersRouting.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';

import {EvodeskLeadersComponent} from './components/EvodeskLeaders/EvodeskLeaders.component';
import {EvodeskLeadersContainerComponent} from './components/EvodeskLeaders/EvodeskLeadersContainer.component';
import {EvodeskLeadersNavDesktopComponent} from './components/EvodeskLeadersNavDesktop/EvodeskLeadersNavDesktop.component';
import {EvodeskLeadersNavDesktopContainerComponent} from './components/EvodeskLeadersNavDesktop/EvodeskLeadersNavDesktopContainer.component';
import {EvodeskLeadersNavMobileComponent} from './components/EvodeskLeadersNavMobile/EvodeskLeadersNavMobile.component';
import {EvodeskLeadersNavMobileContainerComponent} from './components/EvodeskLeadersNavMobile/EvodeskLeadersNavMobileContainer.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskLeadersRoutingModule,
    ],
    declarations: [
        IndexRouteComponent,

        EvodeskLeadersComponent,
        EvodeskLeadersContainerComponent,

        EvodeskLeadersNavDesktopComponent,
        EvodeskLeadersNavDesktopContainerComponent,
        EvodeskLeadersNavMobileComponent,
        EvodeskLeadersNavMobileContainerComponent,
    ],
})
export class EvodeskLeadersModule
{}
