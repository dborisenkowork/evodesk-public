import {Injectable} from '@angular/core';

import * as R from 'ramda';

import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {leaderEntriesPerPage} from '../configs/EvodeskLeaders.config';

import {LeaderEntryModel} from '../../../app/EvodeskRESTApi/models/leaders/LeaderEntry.model';
import {ProfileModel} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {LeadersViewMode} from '../models/Leaders.model';

import {LeadersRange} from '../../../app/EvodeskRESTApi/services/LeadersREST.service';

export interface LeadersState {
    profile: ProfileModel;
    section: LeadersSection;
    bootstrap: boolean;
    viewMode: LeadersViewMode;
    leaders: {
        week: {
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
            loading: boolean;
        };
        month: {
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
            loading: boolean;
        };
        total: {
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
            loading: boolean;
        };
    };
    current: {
        positions: {
            week: {
                name: string;
                position: number;
                experience: number;
            };
            month: {
                name: string;
                position: number;
                experience: number;
            };
            total: {
                name: string;
                position: number;
                experience: number;
            };
        };
    };
}

export type LeadersPrevState = LeadersState | undefined;
export type LeadersNextState = LeadersState | undefined;
export type LeadersStates = { current: LeadersNextState, prev?: LeadersPrevState };

export enum LeadersSection {
    Week = 'Week',
    Month = 'Month',
    Total = 'Total',
}

export enum LeadersAction {
    Reset = 'Reset',
    Bootstrap = 'Bootstrap',
    EnableLeadersLoading = 'EnableLeadersLoading',
    DisableLeadersLoading = 'DisableLeadersLoading',
    PushLeaders = 'PushLeaders',
    SetSection = 'SetSection',
    SetViewMode = 'SetViewMode',
}

function initialState(): LeadersState | undefined {
    return <any>{
        bootstrap: false,
        viewMode: LeadersViewMode.Desktop,
    };
}

type Actions =
      { type: LeadersAction.Reset }
    | { type: LeadersAction.Bootstrap, payload: LeadersBootstrap }
    | { type: LeadersAction.EnableLeadersLoading, payload: LeadersSection }
    | { type: LeadersAction.DisableLeadersLoading, payload: LeadersSection }
    | { type: LeadersAction.PushLeaders, payload: { section: LeadersSection; entries: Array<LeaderEntryModel>; } }
    | { type: LeadersAction.SetSection, payload: { section: LeadersSection; } }
    | { type: LeadersAction.SetViewMode, payload: { viewMode: LeadersViewMode; } }
;

export interface LeadersBootstrap {
    profile: ProfileModel;
    leaders: {
        week: {
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
            loading: boolean;
        };
        month: {
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
            loading: boolean;
        };
        total: {
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
            loading: boolean;
        };
    };
    current: {
        positions: {
            week: {
                name: string;
                position: number;
                experience: number;
            };
            month: {
                name: string;
                position: number;
                experience: number;
            };
            total: {
                name: string;
                position: number;
                experience: number;
            };
        };
    };
}

export const mapLeadersSectionToLeadersREST: Array<{ from: LeadersRange, to: LeadersSection }> = [
    { from: LeadersRange.Week, to: LeadersSection.Week },
    { from: LeadersRange.Month, to: LeadersSection.Month },
    { from: LeadersRange.All, to: LeadersSection.Total },
];

export const mapLeadersSectionToFields: Array<{ from: LeadersSection, to: string }> = [
    { from: LeadersSection.Week, to: 'week' },
    { from: LeadersSection.Month, to: 'month' },
    { from: LeadersSection.Total, to: 'total' },
];

@Injectable()
export class EvodeskLeadersStateService
{
    private _current$: BehaviorSubject<LeadersStates> = new BehaviorSubject<LeadersStates>({ prev: undefined, current: initialState() });
    private _side$: Subject<Actions> = new Subject<Actions>();

    get current$(): Observable<LeadersStates> {
        return this._current$.asObservable();
    }

    get side$(): Observable<Actions> {
        return this._side$.asObservable();
    }

    get snapshot(): LeadersState {
        return this._current$.getValue().current;
    }

    reset(): void {
        this._current$.next({
            prev: undefined,
            current: initialState(),
        });
    }

    dispatch(action: Actions): void {
        this._side$.next(action);

        switch (action.type) {
            case LeadersAction.Reset: {
                this.reset();

                break;
            }

            case LeadersAction.Bootstrap: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        ...action.payload,
                        bootstrap: true,
                    };
                });

                break;
            }

            case LeadersAction.EnableLeadersLoading: {
                const field: string = mapLeadersSectionToFields.filter(m => m.from === action.payload)[0].to;

                this.setState((orig) => {
                    return {
                        ...orig,
                        leaders: {
                            ...orig.leaders,
                            [field]: {
                                ...orig.leaders[field],
                                loading: true,
                            },
                        },
                    };
                });

                break;
            }

            case LeadersAction.DisableLeadersLoading: {
                const field: string = mapLeadersSectionToFields.filter(m => m.from === action.payload)[0].to;

                this.setState((orig) => {
                    return {
                        ...orig,
                        leaders: {
                            ...orig.leaders,
                            [field]: {
                                ...orig.leaders[field],
                                loading: false,
                            },
                        },
                    };
                });

                break;
            }

            case LeadersAction.PushLeaders: {
                const field: string = mapLeadersSectionToFields.filter(m => m.from === action.payload.section)[0].to;

                this.setState((orig) => {
                    return {
                        ...orig,
                        leaders: {
                            ...orig.leaders,
                            [field]: {
                                ...orig.leaders[field],
                                entries: R.uniqBy((e: LeaderEntryModel) => e.id, [...orig.leaders[field].entries, ...action.payload.entries]),
                            },
                        },
                    };
                });

                break;
            }

            case LeadersAction.SetSection: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        section: action.payload.section,
                        leaders: {
                            ...orig.leaders,
                            week: {
                                ...orig.leaders.week,
                                entries: orig.leaders.week.entries.slice(0, leaderEntriesPerPage),
                            },
                            month: {
                                ...orig.leaders.month,
                                entries: orig.leaders.month.entries.slice(0, leaderEntriesPerPage),
                            },
                            total: {
                                ...orig.leaders.total,
                                entries: orig.leaders.total.entries.slice(0, leaderEntriesPerPage),
                            },
                        },
                    };
                });

                break;
            }

            case LeadersAction.SetViewMode: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        viewMode: action.payload.viewMode,
                    };
                });

                break;
            }
        }
    }

    private setState(set$: (orig: LeadersPrevState) => LeadersNextState): void {
        this._current$.next({
            prev: this.snapshot,
            current: set$(this.snapshot),
        });
    }
}
