import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {LeadersSection} from '../../state/EvodeskLeadersState.service';

interface Props {
    section: LeadersSection;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskLeadersNavDesktopComponentProps};

@Component({
    selector: 'evodesk-leaders-nav-desktop',
    templateUrl: './EvodeskLeadersNavDesktop.component.pug',
    styleUrls: [
        './EvodeskLeadersNavDesktop.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskLeadersNavDesktopComponent implements OnChanges
{
    @Input() props: Props;

    @Output('setSection') setSectionEvent: EventEmitter<LeadersSection> = new EventEmitter<LeadersSection>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    setSection(section: string): void {
        this.setSectionEvent.emit(<any>section);
    }

    get section(): string {
        return <any>this.props.section;
    }
}
