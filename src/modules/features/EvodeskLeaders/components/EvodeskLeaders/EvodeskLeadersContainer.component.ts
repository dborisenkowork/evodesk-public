import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Platform} from '@angular/cdk/platform';
import {BreakpointObserver} from '@angular/cdk/layout';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {leaderEntriesPerPage} from '../../configs/EvodeskLeaders.config';

import {LeadersViewMode} from '../../models/Leaders.model';

import {EvodeskLeadersComponentProps as Props} from './EvodeskLeaders.component';

import {EvodeskLeadersStateService, LeadersAction, LeadersSection} from '../../state/EvodeskLeadersState.service';
import {LeadersRange, LeadersRESTService} from '../../../../app/EvodeskRESTApi/services/LeadersREST.service';

interface State {
    ready: boolean;
    loading: boolean;
    offsetLeadersWeek: number;
    offsetLeadersMonth: number;
    offsetLeadersTotal: number;
    props?: Props | undefined;
}

@Component({
    selector: 'evodesk-leaders-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-leaders
            *ngIf="state.props"
            [props]="state.props"
            (loadMoreWeek)="loadMoreWeek()"
            (loadMoreMonth)="loadMoreMonth()"
            (loadMoreTotal)="loadMoreTotal()"
          ></evodesk-leaders>
        </ng-container>
    `,
    styles: [`
        .loading {
            position: absolute;
            top: 0; height: 3px; left: 0; right: 0;
            overflow: hidden;
        }
    `],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskLeadersContainerComponent implements OnDestroy, OnInit
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        loading: false,
        offsetLeadersWeek: 0,
        offsetLeadersMonth: 0,
        offsetLeadersTotal: 0,
        props: undefined,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private leadersState: EvodeskLeadersStateService,
        private leadersREST: LeadersRESTService,
        private platform: Platform,
        private breakpointObserver: BreakpointObserver,
    ) {}

    ngOnInit(): void {
        this.leadersState.current$.pipe(takeUntil(this.ngOnDestroy$), filter(s => !! s.current.bootstrap)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    section: s.current.section,
                    viewMode: s.current.viewMode,
                    isMobileDevice: this.platform.ANDROID || this.platform.IOS,
                    profile: s.current.profile,
                    current: s.current.current,
                    leaders: s.current.leaders,
                },
            };

            this.cdr.detectChanges();
        });

        this.breakpointObserver.observe(`(max-width: 1167px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe((e) => {
            this.leadersState.dispatch({
                type: LeadersAction.SetViewMode,
                payload: {
                    viewMode: LeadersViewMode.Mobile,
                },
            });
        });

        this.breakpointObserver.observe(`(min-width: 1168px`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this.leadersState.dispatch({
                type: LeadersAction.SetViewMode,
                payload: {
                    viewMode: LeadersViewMode.Desktop,
                },
            });
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    loadMoreWeek(): void {
        this.leadersState.dispatch({
            type: LeadersAction.EnableLeadersLoading,
            payload: LeadersSection.Week,
        });

        this.leadersREST.leaders({
            limit: leaderEntriesPerPage,
            offset: this.state.offsetLeadersWeek + leaderEntriesPerPage,
            forProfileId: this.leadersState.snapshot.profile.id,
            range: LeadersRange.Week,
        }).subscribe(
            (httpResponse) => {
                this.leadersState.dispatch({
                    type: LeadersAction.DisableLeadersLoading,
                    payload: LeadersSection.Week,
                });

                this.leadersState.dispatch({
                    type: LeadersAction.PushLeaders,
                    payload: {
                        entries: httpResponse.data,
                        section: LeadersSection.Week,
                    },
                });

                this.state = {
                    ...this.state,
                    offsetLeadersWeek: this.state.offsetLeadersWeek + leaderEntriesPerPage,
                };
            },
            () => {
                this.leadersState.dispatch({
                    type: LeadersAction.DisableLeadersLoading,
                    payload: LeadersSection.Week,
                });
            },
        );
    }

    loadMoreMonth(): void {
        this.leadersState.dispatch({
            type: LeadersAction.EnableLeadersLoading,
            payload: LeadersSection.Month,
        });

        this.leadersREST.leaders({
            limit: leaderEntriesPerPage,
            offset: this.state.offsetLeadersMonth + leaderEntriesPerPage,
            forProfileId: this.leadersState.snapshot.profile.id,
            range: LeadersRange.Month,
        }).subscribe(
            (httpResponse) => {
                this.leadersState.dispatch({
                    type: LeadersAction.DisableLeadersLoading,
                    payload: LeadersSection.Month,
                });

                this.leadersState.dispatch({
                    type: LeadersAction.PushLeaders,
                    payload: {
                        entries: httpResponse.data,
                        section: LeadersSection.Month,
                    },
                });

                this.state = {
                    ...this.state,
                    offsetLeadersMonth: this.state.offsetLeadersMonth + leaderEntriesPerPage,
                };
            },
            () => {
                this.leadersState.dispatch({
                    type: LeadersAction.DisableLeadersLoading,
                    payload: LeadersSection.Month,
                });
            },
        );
    }

    loadMoreTotal(): void {
        this.leadersState.dispatch({
            type: LeadersAction.EnableLeadersLoading,
            payload: LeadersSection.Total,
        });

        this.leadersREST.leaders({
            limit: leaderEntriesPerPage,
            offset: this.state.offsetLeadersTotal + leaderEntriesPerPage,
            forProfileId: this.leadersState.snapshot.profile.id,
            range: LeadersRange.All,
        }).subscribe(
            (httpResponse) => {
                this.leadersState.dispatch({
                    type: LeadersAction.DisableLeadersLoading,
                    payload: LeadersSection.Total,
                });

                this.leadersState.dispatch({
                    type: LeadersAction.PushLeaders,
                    payload: {
                        entries: httpResponse.data,
                        section: LeadersSection.Total,
                    },
                });

                this.state = {
                    ...this.state,
                    offsetLeadersTotal: this.state.offsetLeadersTotal + leaderEntriesPerPage,
                };
            },
            () => {
                this.leadersState.dispatch({
                    type: LeadersAction.DisableLeadersLoading,
                    payload: LeadersSection.Total,
                });
            },
        );
    }
}
