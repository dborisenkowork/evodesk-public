import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {ProfileModel} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {LeaderEntryModel} from '../../../../app/EvodeskRESTApi/models/leaders/LeaderEntry.model';
import {LeadersViewMode} from '../../models/Leaders.model';

import {LeadersSection} from '../../state/EvodeskLeadersState.service';

interface Props {
    profile: ProfileModel;
    section: LeadersSection;
    isMobileDevice: boolean;
    viewMode: LeadersViewMode;
    leaders: {
        week: {
            loading: boolean;
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
        };
        month: {
            loading: boolean;
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
        };
        total: {
            loading: boolean;
            entries: Array<LeaderEntryModel>;
            hasMoreToLoad: boolean;
        };
    };
    current: {
        positions: {
            week: {
                name: string;
                position: number;
                experience: number;
            };
            month: {
                name: string;
                position: number;
                experience: number;
            };
            total: {
                name: string;
                position: number;
                experience: number;
            };
        };
    };
}

interface State {
    ready: boolean;
}

export {Props as EvodeskLeadersComponentProps};

@Component({
    selector: 'evodesk-leaders',
    templateUrl: './EvodeskLeaders.component.pug',
    styleUrls: [
        './EvodeskLeaders.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskLeadersComponent implements OnChanges
{
    @Input() props: Props;

    @Output('loadMoreWeek') loadMoreWeekEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('loadMoreMonth') loadMoreMonthEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('loadMoreTotal') loadMoreTotalEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get section(): string {
        return <any>this.props.section;
    }

    get shouldDisplayDesktopNav(): boolean {
        return this.props.viewMode === LeadersViewMode.Desktop;
    }

    get shouldDisplayMobileNav(): boolean {
        return this.props.viewMode === LeadersViewMode.Mobile;
    }

    loadMoreWeek(): void {
        if (this.props.leaders.week.hasMoreToLoad && ! this.props.leaders.week.loading) {
            this.loadMoreWeekEvent.emit(undefined);
        }
    }

    loadMoreMonth(): void {
        if (this.props.leaders.month.hasMoreToLoad && ! this.props.leaders.month.loading) {
            this.loadMoreMonthEvent.emit(undefined);
        }
    }

    loadMoreTotal(): void {
        if (this.props.leaders.total.hasMoreToLoad && ! this.props.leaders.total.loading) {
            this.loadMoreTotalEvent.emit(undefined);
        }
    }

    onLeaderWeekScroll($event: Event): void {
        const elem: HTMLElement = <any>$event.target;

        const scrollTop: number = elem.scrollTop;
        const scrollHeight: number = elem.scrollHeight;
        const clientHeight: number = elem.clientHeight;

        if (scrollHeight > clientHeight) {
            if ((scrollTop + clientHeight) === scrollHeight) {
                this.loadMoreWeek();
            }
        }
    }

    onLeaderMonthScroll($event: Event): void {
        const elem: HTMLElement = <any>$event.target;

        const scrollTop: number = elem.scrollTop;
        const scrollHeight: number = elem.scrollHeight;
        const clientHeight: number = elem.clientHeight;

        if (scrollHeight > clientHeight) {
            if ((scrollTop + clientHeight) === scrollHeight) {
                this.loadMoreMonth();
            }
        }
    }

    onLeaderTotalScroll($event: Event): void {
        const elem: HTMLElement = <any>$event.target;

        const scrollTop: number = elem.scrollTop;
        const scrollHeight: number = elem.scrollHeight;
        const clientHeight: number = elem.clientHeight;

        if (scrollHeight > clientHeight) {
            if ((scrollTop + clientHeight) === scrollHeight) {
                this.loadMoreTotal();
            }
        }
    }
}
