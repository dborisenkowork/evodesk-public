import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {EvodeskLeadersNavMobileComponentProps as Props} from './EvodeskLeadersNavMobile.component';

import {EvodeskLeadersStateService} from '../../state/EvodeskLeadersState.service';

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-leaders-nav-mobile-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-leaders-nav-mobile [props]="state.props" (setSection)="setSection($event)"></evodesk-leaders-nav-mobile>
      </ng-container>
    `,
})
export class EvodeskLeadersNavMobileContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private leadersState: EvodeskLeadersStateService,
    ) {}

    ngOnInit(): void {
        this.leadersState.current$.pipe(takeUntil(this.ngOnDestroy$), filter(s => !! s.current.bootstrap)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    section: s.current.section,
                },
            };

            this.cdr.markForCheck();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
