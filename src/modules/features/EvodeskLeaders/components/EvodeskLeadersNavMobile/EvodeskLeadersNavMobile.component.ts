import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

import {LeadersSection} from '../../state/EvodeskLeadersState.service';

import * as S from 'string';

interface Props {
    section: LeadersSection;
}

interface State {
    ready: boolean;
    isOpened: boolean;
}

export {Props as EvodeskLeadersNavMobileComponentProps};

@Component({
    selector: 'evodesk-leaders-nav-mobile',
    templateUrl: './EvodeskLeadersNavMobile.component.pug',
    styleUrls: [
        './EvodeskLeadersNavMobile.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskLeadersNavMobileComponent implements OnChanges
{
    @Input() props: Props;

    public state: State = {
        ready: false,
        isOpened: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get containerCSSClasses(): any {
        return {
            'is-opened': this.state.isOpened,
        };
    }

    get section(): string {
        return <any>this.props.section;
    }

    get currentNav(): string {
        return `EvodeskLeaders.components.EvodeskLeaders.NavLeaders${S(this.section).titleCase().s}`;
    }

    toggle(): void {
        this.state = {
            ...this.state,
            isOpened: ! this.state.isOpened,
        };
    }

    close(): void {
        this.state = {
            ...this.state,
            isOpened: false,
        };
    }
}
