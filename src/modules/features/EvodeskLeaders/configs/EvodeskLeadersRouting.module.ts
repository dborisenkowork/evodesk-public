import {Routes} from '@angular/router';

import {HasAuthTokenGuard} from '../../EvodeskAuth/guards/HasAuthToken.guard';
import {CurrentUserGuard} from '../../EvodeskAuth/guards/CurrentUser.guard';
import {EvodeskSetCommonHeaderGuard} from '../../EvodeskHeader/guards/EvodeskSetCommonHeader.guard';

import {IndexRouteComponent} from '../routes/IndexRoute/IndexRoute.component';

export const EvodeskLeadersRouting: Routes = [
    {
        path: '',
        redirectTo: 'week',
    },
    {
        path: ':section',
        pathMatch: 'full',
        component: IndexRouteComponent,
        canActivate: [
            HasAuthTokenGuard,
            CurrentUserGuard,
            EvodeskSetCommonHeaderGuard,
        ],
    },
];
