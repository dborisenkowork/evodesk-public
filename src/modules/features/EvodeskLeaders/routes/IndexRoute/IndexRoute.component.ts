import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {EvodeskLeadersStateService, LeadersAction, LeadersSection} from '../../state/EvodeskLeadersState.service';
import {EvodeskLeadersBootstrapService} from '../../state/EvodeskLeadersBootstrap.service';

interface State {
    profileId: ProfileId | undefined;
}

@Component({
    templateUrl: './IndexRoute.component.pug',
    styleUrls: [
        './IndexRoute.component.scss',
    ],
    providers: [
        EvodeskLeadersStateService,
        EvodeskLeadersBootstrapService,
    ],
})
export class IndexRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        profileId: undefined,
    };

    constructor(
        private activatedRoute: ActivatedRoute,
        private lState: EvodeskLeadersStateService,
        private lBootstrap: EvodeskLeadersBootstrapService,
    ) {}

    ngOnInit(): void {
        this.lBootstrap.bootstrap().pipe(takeUntil(this.ngOnDestroy$)).subscribe(
            () => {
                this.activatedRoute.params.pipe(takeUntil(this.ngOnDestroy$)).subscribe((params) => {
                    const section: string = (params['section'] || <any>LeadersSection.Week).toString().toLocaleLowerCase();

                    if (!!~['total', 'month', 'week'].indexOf(section)) {
                        this.lState.dispatch({
                            type: LeadersAction.SetSection,
                            payload: {
                                section: <any>section,
                            },
                        });
                    }
                });
            },
        );
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
