import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskExchangeRoutingModule} from './EvodeskExchangeRouting.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskExchangeRoutingModule,
    ],
    declarations: [
        IndexRouteComponent,
    ],
})
export class EvodeskExchangeModule
{}
