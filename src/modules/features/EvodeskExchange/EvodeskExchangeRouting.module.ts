import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskExchangeRouting} from './configs/EvodeskExchangeRouting.module';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskExchangeRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskExchangeRoutingModule
{}
