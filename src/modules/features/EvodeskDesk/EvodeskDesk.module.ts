import {NgModule} from '@angular/core';
import {Type} from '@angular/core/src/type';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskDesksRoutingModule} from './EvodeskDeskRouting.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';
import {EvodeskDeskComponent} from './components/common/EvodeskDesk/EvodeskDesk.component';
import {EvodeskDeskContainerComponent} from './components/common/EvodeskDesk/EvodeskDeskContainer.component';
import {EvodeskDeskAddAttachmentComponent} from './components/common/EvodeskDeskAddAttachment/EvodeskDeskAddAttachment.component';
import {EvodeskDeskAddChecklistComponent} from './components/common/EvodeskDeskAddChecklist/EvodeskDeskAddChecklist.component';
import {EvodeskDeskAddChecklistContainerComponent} from './components/common/EvodeskDeskAddChecklist/EvodeskDeskAddChecklistContainer.component';
import {EvodeskDeskAddFormComponent} from './components/common/EvodeskDeskAddForm/EvodeskDeskAddForm.component';
import {EvodeskDeskAddFormContainerComponent} from './components/common/EvodeskDeskAddForm/EvodeskDeskAddFormContainer.component';
import {EvodeskDeskAttachAudioComponent} from './components/common/EvodeskDeskAttachAudio/EvodeskDeskAttachAudio.component';
import {EvodeskDeskAttachAudioContainerComponent} from './components/common/EvodeskDeskAttachAudio/EvodeskDeskAttachAudioContainer.component';
import {EvodeskDeskAttachImageComponent} from './components/common/EvodeskDeskAttachImage/EvodeskDeskAttachImage.component';
import {EvodeskDeskAttachImageContainerComponent} from './components/common/EvodeskDeskAttachImage/EvodeskDeskAttachImageContainer.component';
import {EvodeskDeskCardComponent} from './components/common/EvodeskDeskCard/EvodeskDeskCard.component';
import {EvodeskDeskCardContainerComponent} from './components/common/EvodeskDeskCard/EvodeskDeskCardContainer.component';
import {EvodeskDeskColumnComponent} from './components/common/EvodeskDeskColumn/EvodeskDeskColumn.component';
import {EvodeskDeskColumnContainerComponent} from './components/common/EvodeskDeskColumn/EvodeskDeskColumnContainer.component';
import {EvodeskDeskColumnContextMenuComponent} from './components/common/EvodeskDeskColumnContextMenu/EvodeskDeskColumnContextMenu.component';
import {EvodeskDeskColumnContextMenuContainerComponent} from './components/common/EvodeskDeskColumnContextMenu/EvodeskDeskColumnContextMenuContainer.component';
import {EvodeskDeskCopyLinkComponent} from './components/common/EvodeskDeskCopyLink/EvodeskDeskCopyLink.component';
import {EvodeskDeskCopyLinkContainerComponent} from './components/common/EvodeskDeskCopyLink/EvodeskDeskCopyLinkContainer.component';
import {EvodeskDeskDeadlineComponent} from './components/common/EvodeskDeskDeadline/EvodeskDeskDeadline.component';
import {EvodeskDeskDeadlineContainerComponent} from './components/common/EvodeskDeskDeadline/EvodeskDeskDeadlineContainer.component';
import {EvodeskDeskFilterComponent} from './components/common/EvodeskDeskFilter/EvodeskDeskFilter.component';
import {EvodeskDeskFilterContainerComponent} from './components/common/EvodeskDeskFilter/EvodeskDeskFilterContainer.component';
import {EvodeskDeskHistoryRecordActionComponent} from './components/common/EvodeskDeskHistoryRecordAction/EvodeskDeskHistoryRecordAction.component';
import {EvodeskDeskHistoryRecordCommentComponent} from './components/common/EvodeskDeskHistoryRecordComment/EvodeskDeskHistoryRecordComment.component';
import {EvodeskDeskLabelComponent} from './components/common/EvodeskDeskLabel/EvodeskDeskLabel.component';
import {EvodeskDeskLabelContainerComponent} from './components/common/EvodeskDeskLabel/EvodeskDeskLabelContainer.component';
import {EvodeskDeskMoveComponent} from './components/common/EvodeskDeskMove/EvodeskDeskMove.component';
import {EvodeskDeskMoveContainerComponent} from './components/common/EvodeskDeskMove/EvodeskDeskMoveContainer.component';
import {EvodeskDeskMoveMultiComponent} from './components/common/EvodeskDeskMoveMulti/EvodeskDeskMoveMulti.component';
import {EvodeskDeskMoveMultiContainerComponent} from './components/common/EvodeskDeskMoveMulti/EvodeskDeskMoveMultiContainer.component';
import {EvodeskDeskQueryStringFilterComponent} from './components/common/EvodeskDeskQueryStringFilter/EvodeskDeskQueryStringFilter.component';
import {EvodeskDeskQueryStringFilterContainerComponent} from './components/common/EvodeskDeskQueryStringFilter/EvodeskDeskQueryStringFilterContainer.component';
import {EvodeskDeskToolbarComponent} from './components/common/EvodeskDeskToolbar/EvodeskDeskToolbar.component';
import {EvodeskDeskToolbarContainerComponent} from './components/common/EvodeskDeskToolbar/EvodeskDeskToolbarContainer.component';
import {EvodeskDeskAttachmentImageComponent} from './components/common/EvodeskDeskAttachmentImage/EvodeskDeskAttachmentImage.component';
import {EvodeskDeskAttachmentImageContainerComponent} from './components/common/EvodeskDeskAttachmentImage/EvodeskDeskAttachmentImageContainer.component';
import {EvodeskDeskResponsibleComponent} from './components/common/EvodeskDeskResponsible/EvodeskDeskResponsible.component';
import {EvodeskDeskResponsibleContainerComponent} from './components/common/EvodeskDeskResponsible/EvodeskDeskResponsibleContainer.component';
import {EvodeskDeskParticipantComponent} from './components/common/EvodeskDeskParticipant/EvodeskDeskParticipant.component';
import {EvodeskDeskParticipantContainerComponent} from './components/common/EvodeskDeskParticipant/EvodeskDeskParticipantContainer.component';
import {EvodeskDeskParticipantInviteByLinkComponent} from './components/common/EvodeskDeskParticipantInviteByLink/EvodeskDeskParticipantInviteByLink.component';
import {EvodeskDeskParticipantInviteByLinkContainerComponent} from './components/common/EvodeskDeskParticipantInviteByLink/EvodeskDeskParticipantInviteByLinkContainer.component';
import {EvodeskDeskProjectsComponent} from './components/common/EvodeskDeskProjects/EvodeskDeskProjects.component';
import {EvodeskDeskProjectsContainerComponent} from './components/common/EvodeskDeskProjects/EvodeskDeskProjectsContainer.component';
import {EvodeskDeskProjectCardComponent} from './components/common/EvodeskDeskProjectCard/EvodeskDeskProjectCard.component';
import {EvodeskDeskProjectCardContainerComponent} from './components/common/EvodeskDeskProjectCard/EvodeskDeskProjectCardContainer.component';
import {EvodeskDeskCardProjectsComponent} from './components/common/EvodeskDeskCardProjects/EvodeskDeskCardProjects.component';
import {EvodeskDeskCardProjectsContainerComponent} from './components/common/EvodeskDeskCardProjects/EvodeskDeskCardProjectsContainer.component';
import {EvodeskDeskCardFloatComponent} from './components/common/EvodeskDeskCardFloat/EvodeskDeskCardFloat.component';
import {EvodeskDeskCardFloatContainerComponent} from './components/common/EvodeskDeskCardFloat/EvodeskDeskCardFloatContainer.component';
import {EvodeskDeskCardChecklistComponent} from './components/common/EvodeskDeskCardChecklist/EvodeskDeskCardChecklist.component';
import {EvodeskDeskCardChecklistContainerComponent} from './components/common/EvodeskDeskCardChecklist/EvodeskDeskCardChecklistContainer.component';
import {EvodeskDeskCardChecklistsComponent} from './components/common/EvodeskDeskCardChecklists/EvodeskDeskCardChecklists.component';
import {EvodeskDeskCardChecklistsContainerComponent} from './components/common/EvodeskDeskCardChecklists/EvodeskDeskCardChecklistsContainer.component';

import {EvodeskDeskCardModalComponent} from './components/modal/EvodeskDeskCardModal/EvodeskDeskCardModal.component';
import {EvodeskDeskCardModalContainerComponent} from './components/modal/EvodeskDeskCardModal/EvodeskDeskCardModalContainer.component';
import {EvodeskDeskCardModalAttachmentComponent} from './components/modal/EvodeskDeskCardModalAttachment/EvodeskDeskCardModalAttachment.component';
import {EvodeskDeskCardModalAttachmentContainerComponent} from './components/modal/EvodeskDeskCardModalAttachment/EvodeskDeskCardModalAttachmentContainer.component';
import {EvodeskDeskCardModalAttachmentAudioComponent} from './components/modal/EvodeskDeskCardModalAttachmentAudio/EvodeskDeskCardModalAttachmentAudio.component';
import {EvodeskDeskCardModalAttachmentImageComponent} from './components/modal/EvodeskDeskCardModalAttachmentImage/EvodeskDeskCardModalAttachmentImage.component';
import {EvodeskDeskCardModalChecklistComponent} from './components/modal/EvodeskDeskCardModalChecklist/EvodeskDeskCardModalChecklist.component';
import {EvodeskDeskCardModalChecklistContainerComponent} from './components/modal/EvodeskDeskCardModalChecklist/EvodeskDeskCardModalChecklistContainer.component';
import {EvodeskDeskCardModalCommentComponent} from './components/modal/EvodeskDeskCardModalComment/EvodeskDeskCardModalComment.component';
import {EvodeskDeskCardModalDeadlineComponent} from './components/modal/EvodeskDeskCardModalDeadline/EvodeskDeskCardModalDeadline.component';
import {EvodeskDeskCardModalDeadlineContainerComponent} from './components/modal/EvodeskDeskCardModalDeadline/EvodeskDeskCardModalDeadlineContainer.component';
import {EvodeskDeskCardModalDescriptionComponent} from './components/modal/EvodeskDeskCardModalDescription/EvodeskDeskCardModalDescription.component';
import {EvodeskDeskCardModalDescriptionContainerComponent} from './components/modal/EvodeskDeskCardModalDescription/EvodeskDeskCardModalDescriptionContainer.component';
import {EvodeskDeskCardModalHistoryComponent} from './components/modal/EvodeskDeskCardModalHistory/EvodeskDeskCardModalHistory.component';
import {EvodeskDeskCardModalHistoryContainerComponent} from './components/modal/EvodeskDeskCardModalHistory/EvodeskDeskCardModalHistoryContainer.component';
import {EvodeskDeskCardModalLabelComponent} from './components/modal/EvodeskDeskCardModalLabel/EvodeskDeskCardModalLabel.component';
import {EvodeskDeskCardModalLabelContainerComponent} from './components/modal/EvodeskDeskCardModalLabel/EvodeskDeskCardModalLabelContainer.component';
import {EvodeskDeskCardModalTitleComponent} from './components/modal/EvodeskDeskCardModalTitle/EvodeskDeskCardModalTitle.component';
import {EvodeskDeskCardModalTitleContainerComponent} from './components/modal/EvodeskDeskCardModalTitle/EvodeskDeskCardModalTitleContainer.component';
import {EvodeskDeskCardModalCommentContainerComponent} from './components/modal/EvodeskDeskCardModalComment/EvodeskDeskCardModalCommentContainer.component';
import {EvodeskDeskCardModalResponsibleComponent} from './components/modal/EvodeskDeskCardModalResponsible/EvodeskDeskCardModalResponsible.component';
import {EvodeskDeskCardModalResponsibleContainerComponent} from './components/modal/EvodeskDeskCardModalResponsible/EvodeskDeskCardModalResponsibleContainer.component';
import {EvodeskDeskCardModalProjectsComponent} from './components/modal/EvodeskDeskCardModalProjects/EvodeskDeskCardModalProjects.component';
import {EvodeskDeskCardModalProjectsContainerComponent} from './components/modal/EvodeskDeskCardModalProjects/EvodeskDeskCardModalProjectsContainer.component';

const routes: Array<Type<any> | Array<any>> = [
    IndexRouteComponent,
];

const common: Array<Type<any> | Array<any>> = [
    EvodeskDeskComponent,
    EvodeskDeskContainerComponent,
    EvodeskDeskAddAttachmentComponent,
    EvodeskDeskAddChecklistComponent,
    EvodeskDeskAddChecklistContainerComponent,
    EvodeskDeskAddFormComponent,
    EvodeskDeskAddFormContainerComponent,
    EvodeskDeskAttachAudioComponent,
    EvodeskDeskAttachAudioContainerComponent,
    EvodeskDeskAttachImageComponent,
    EvodeskDeskAttachImageContainerComponent,
    EvodeskDeskCardComponent,
    EvodeskDeskCardContainerComponent,
    EvodeskDeskColumnComponent,
    EvodeskDeskColumnContainerComponent,
    EvodeskDeskColumnContextMenuComponent,
    EvodeskDeskColumnContextMenuContainerComponent,
    EvodeskDeskCopyLinkComponent,
    EvodeskDeskCopyLinkContainerComponent,
    EvodeskDeskDeadlineComponent,
    EvodeskDeskDeadlineContainerComponent,
    EvodeskDeskFilterComponent,
    EvodeskDeskFilterContainerComponent,
    EvodeskDeskHistoryRecordActionComponent,
    EvodeskDeskHistoryRecordCommentComponent,
    EvodeskDeskLabelComponent,
    EvodeskDeskLabelContainerComponent,
    EvodeskDeskMoveComponent,
    EvodeskDeskMoveContainerComponent,
    EvodeskDeskMoveMultiComponent,
    EvodeskDeskMoveMultiContainerComponent,
    EvodeskDeskQueryStringFilterComponent,
    EvodeskDeskQueryStringFilterContainerComponent,
    EvodeskDeskToolbarComponent,
    EvodeskDeskToolbarContainerComponent,
    EvodeskDeskAttachmentImageComponent,
    EvodeskDeskAttachmentImageContainerComponent,
    EvodeskDeskResponsibleComponent,
    EvodeskDeskResponsibleContainerComponent,
    EvodeskDeskParticipantComponent,
    EvodeskDeskParticipantContainerComponent,
    EvodeskDeskParticipantInviteByLinkComponent,
    EvodeskDeskParticipantInviteByLinkContainerComponent,
    EvodeskDeskProjectsComponent,
    EvodeskDeskProjectsContainerComponent,
    EvodeskDeskProjectCardComponent,
    EvodeskDeskProjectCardContainerComponent,
    EvodeskDeskCardProjectsComponent,
    EvodeskDeskCardProjectsContainerComponent,
    EvodeskDeskCardFloatComponent,
    EvodeskDeskCardFloatContainerComponent,
    EvodeskDeskCardChecklistComponent,
    EvodeskDeskCardChecklistContainerComponent,
    EvodeskDeskCardChecklistsComponent,
    EvodeskDeskCardChecklistsContainerComponent,
];

const modal: Array<Type<any> | Array<any>> = [
    EvodeskDeskCardModalComponent,
    EvodeskDeskCardModalContainerComponent,
    EvodeskDeskCardModalAttachmentComponent,
    EvodeskDeskCardModalAttachmentContainerComponent,
    EvodeskDeskCardModalAttachmentAudioComponent,
    EvodeskDeskCardModalAttachmentImageComponent,
    EvodeskDeskCardModalChecklistComponent,
    EvodeskDeskCardModalChecklistContainerComponent,
    EvodeskDeskCardModalCommentComponent,
    EvodeskDeskCardModalCommentContainerComponent,
    EvodeskDeskCardModalDeadlineComponent,
    EvodeskDeskCardModalDeadlineContainerComponent,
    EvodeskDeskCardModalDescriptionComponent,
    EvodeskDeskCardModalDescriptionContainerComponent,
    EvodeskDeskCardModalHistoryComponent,
    EvodeskDeskCardModalHistoryContainerComponent,
    EvodeskDeskCardModalLabelComponent,
    EvodeskDeskCardModalLabelContainerComponent,
    EvodeskDeskCardModalTitleComponent,
    EvodeskDeskCardModalTitleContainerComponent,
    EvodeskDeskCardModalResponsibleComponent,
    EvodeskDeskCardModalResponsibleContainerComponent,
    EvodeskDeskCardModalProjectsComponent,
    EvodeskDeskCardModalProjectsContainerComponent,
];

@NgModule({
    imports: [
        EvodeskAppSharedModule,

        EvodeskDesksRoutingModule,
    ],
    declarations: [
        ...routes,
        ...common,
        ...modal,
    ],
    entryComponents: [
        EvodeskDeskCardModalContainerComponent,
        EvodeskDeskAttachImageContainerComponent,
        EvodeskDeskColumnContextMenuContainerComponent,
        EvodeskDeskCopyLinkContainerComponent,
        EvodeskDeskDeadlineContainerComponent,
        EvodeskDeskFilterContainerComponent,
        EvodeskDeskLabelContainerComponent,
        EvodeskDeskMoveContainerComponent,
        EvodeskDeskMoveMultiContainerComponent,
        EvodeskDeskResponsibleContainerComponent,
        EvodeskDeskParticipantContainerComponent,
        EvodeskDeskParticipantInviteByLinkContainerComponent,
        EvodeskDeskCardProjectsContainerComponent,
        EvodeskDeskCardFloatContainerComponent,
        EvodeskDeskCardChecklistsContainerComponent,
    ],
})
export class EvodeskDeskModule
{}
