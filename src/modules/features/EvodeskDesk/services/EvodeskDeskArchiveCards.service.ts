import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import {publishLast, refCount} from 'rxjs/operators';

import {DeskId} from '../../../app/EvodeskRESTApi/models/Desk.model';
import {DeskCardId} from '../../../app/EvodeskRESTApi/models/DeskCard.model';

import {DeskRESTService} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskConfirmModalService} from '../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';
import {EvodeskDeskSyncStateService} from '../state/EvodeskDeskSyncState.service';

@Injectable()
export class EvodeskDeskArchiveCardsService
{
    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
        private syncState: EvodeskDeskSyncStateService,
        private uiConfirm: EvodeskConfirmModalService,
    ) {}

    archiveCards(deskId: DeskId, cardIds: Array<DeskCardId>): Observable<void> {
        const observable: Observable<void> = Observable.create(observer => {
            this.deskState.dispatch({
                type: EvodeskDeskAction.ArchiveCards,
                payload: {
                    cardIds: cardIds,
                },
            });

            this.rest.archiveCards(deskId, cardIds)
                .subscribe(
                    () => {
                        observer.next(undefined);
                        observer.complete();
                    },
                    (error) => {
                        this.syncState.dispatchCommonHttpError(error);

                        observer.error(error);
                    },
                );
        }).pipe(publishLast(), refCount());

        observable.subscribe();

        return observable;
    }

    askForArchiveSingleCard(deskId: number, cardId: number): Observable<boolean> {
        return Observable.create(observer => {
            this.uiConfirm.open({
                title: { text: 'EvodeskDesk.services.EvodeskDeskArchiveCardsService.askForArchiveSingleCard.uiConfirm.Title', translate: true },
                text: { text: 'EvodeskDesk.services.EvodeskDeskArchiveCardsService.askForArchiveSingleCard.uiConfirm.Text', translate: true },
                confirmButton: { text: 'EvodeskDesk.services.EvodeskDeskArchiveCardsService.askForArchiveSingleCard.uiConfirm.ConfirmButton', translate: true },
                cancelButton: { text: 'EvodeskDesk.services.EvodeskDeskArchiveCardsService.askForArchiveSingleCard.uiConfirm.CancelButton', translate: true },
                confirm: () => {
                    this.archiveCards(deskId, [cardId]).subscribe(
                        undefined,
                        (error) => {
                            this.syncState.dispatchCommonHttpError(error);
                        },
                    );

                    observer.next(true);
                    observer.complete();
                },
                cancel: () => {
                    observer.next(false);
                    observer.complete();
                },
            }).subscribe();
        });
    }

    askForArchiveManyCards(deskId: DeskId, cardIds: Array<DeskCardId>): Observable<boolean> {
        return Observable.create(observer => {
            this.uiConfirm.open({
                title: { text: 'EvodeskDesk.services.EvodeskDeskArchiveCardsService.askForArchiveManyCards.uiConfirm.Title', translate: true },
                text: { text: 'EvodeskDesk.services.EvodeskDeskArchiveCardsService.askForArchiveManyCards.uiConfirm.Text', translate: true },
                confirmButton: { text: 'EvodeskDesk.services.EvodeskDeskArchiveCardsService.askForArchiveManyCards.uiConfirm.ConfirmButton', translate: true },
                cancelButton: { text: 'EvodeskDesk.services.EvodeskDeskArchiveCardsService.askForArchiveManyCards.uiConfirm.CancelButton', translate: true },
                confirm: () => {
                    this.archiveCards(deskId, cardIds).subscribe(
                        () => {
                            observer.next(true);
                            observer.complete();
                        },
                        (error) => {
                            this.syncState.dispatchCommonHttpError(error);

                            observer.error(error);
                        },
                    );
                },
                cancel: () => {
                    observer.next(false);
                    observer.complete();
                },
            }).subscribe();
        });
    }
}
