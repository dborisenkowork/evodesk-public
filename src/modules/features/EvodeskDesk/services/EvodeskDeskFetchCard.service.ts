import {Injectable} from '@angular/core';

import {Subject, Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskCardId, DeskCardModel} from '../../../app/EvodeskRESTApi/models/DeskCard.model';

import {DeskRESTService} from '../../../app/EvodeskRESTApi/services/DeskREST.service';

import {EvodeskDeskSyncStateService} from '../state/EvodeskDeskSyncState.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';

@Injectable()
export class EvodeskDeskFetchCardService
{
    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
        private syncState: EvodeskDeskSyncStateService,
    ) {}

    fetchCard(deskId: number, cardId: DeskCardId): Observable<DeskCardModel> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create(observer => {
            this.rest.getCard(deskId, cardId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (next) => {
                        this.deskState.dispatch({
                            type: EvodeskDeskAction.PushCard,
                            payload: {
                                card: next,
                            },
                        });

                        observer.next(next);
                        observer.complete();
                    },
                    (error) => {
                        observer.error(error);

                        this.syncState.dispatchCommonHttpError(error);
                    },
                );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}

