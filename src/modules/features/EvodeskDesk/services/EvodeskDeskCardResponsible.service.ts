import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {DeskCardId} from '../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskParticipantModel} from '../../../app/EvodeskRESTApi/models/DeskParticipant.model';

import {DeskRESTService} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';

@Injectable()
export class EvodeskDeskCardResponsibleService
{
    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
    ) {}

    addDeskParticipantAsResponsibleOfCard(cardId: DeskCardId, participant: DeskParticipantModel): Observable<void> {
        this.deskState.dispatch({
            type: EvodeskDeskAction.AddDeskParticipantAsResponsibleOfCard,
            payload: {
                cardId: cardId,
                participant: participant,
            },
        });

        return this.rest.addResponsibleToCard(this.deskState.snapshot.desk.id, cardId, participant.id).pipe(map(s => undefined));
    }

    remodeDeskParticipanAsResponsibleOfCard(cardId: DeskCardId, participant: DeskParticipantModel): Observable<void> {
        this.deskState.dispatch({
            type: EvodeskDeskAction.RemoveDeskParticipantAsResponsibleOfCard,
            payload: {
                cardId: cardId,
                participant: participant,
            },
        });

        return this.rest.deleteResponsibleFromCard(this.deskState.snapshot.desk.id, cardId, participant.id).pipe(map(s => undefined));
    }
}
