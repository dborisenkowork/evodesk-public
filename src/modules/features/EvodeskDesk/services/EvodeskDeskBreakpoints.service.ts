import {Injectable} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';

import {filter, takeUntil} from 'rxjs/operators';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {evodeskDeskLayoutDesktop, evodeskDeskLayoutMobile} from '../configs/EvodeskDesk.constants';

import {ViewMode} from '../../../../types/ViewMode.types';

@Injectable()
export class EvodeskDeskBreakpointsService
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private _current$: BehaviorSubject<ViewMode> = new BehaviorSubject<ViewMode>(ViewMode.Desktop);

    constructor(
        private breakpointObserver: BreakpointObserver,
    ) {}

    init(): Observable<ViewMode> {
        this.breakpointObserver.observe(`(max-width: ${evodeskDeskLayoutMobile.to}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe((e) => {
            this._current$.next(ViewMode.Mobile);
        });

        this.breakpointObserver.observe(`(min-width: ${evodeskDeskLayoutDesktop.from}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this._current$.next(ViewMode.Desktop);
        });

        return this.current$;
    }

    destroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get current$(): Observable<ViewMode> {
        return this._current$.asObservable();
    }
}
