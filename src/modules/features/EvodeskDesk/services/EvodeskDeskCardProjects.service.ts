import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';

import {DeskId} from '../../../app/EvodeskRESTApi/models/Desk.model';
import {DeskCardId} from '../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskProjectId} from '../../../app/EvodeskRESTApi/models/DeskProject.model';

import {DeskRESTService} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';
import {EvodeskDeskSyncStateService} from '../state/EvodeskDeskSyncState.service';

@Injectable()
export class EvodeskDeskCardProjectsService
{
    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
        private syncState: EvodeskDeskSyncStateService,
    ) {}

    includeCardToProject(deskId: DeskId, cardId: DeskCardId, projectId: DeskProjectId): Observable<void> {
        return Observable.create(observer => {
            this.deskState.dispatch({
                type: EvodeskDeskAction.IncludeCardToProject,
                payload: {
                    cardId: cardId,
                    projectId: projectId,
                },
            });

            this.rest.addCardToProject(deskId, cardId, projectId).subscribe(
                () => {},
                (error) => {
                    this.syncState.dispatchCommonHttpError(error);

                    observer.error(error);
                },
            );
        });
    }

    excludeCardFromProject(deskId: DeskId, cardId: DeskCardId, projectId: DeskProjectId): Observable<void> {
        return Observable.create(observer => {
            this.deskState.dispatch({
                type: EvodeskDeskAction.ExcludeCardFromProject,
                payload: {
                    cardId: cardId,
                    projectId: projectId,
                },
            });

            this.rest.deleteCardFromProject(deskId, cardId, projectId).subscribe(
                () => {},
                (error) => {
                    this.syncState.dispatchCommonHttpError(error);

                    observer.error(error);
                },
            );
        });
    }
}
