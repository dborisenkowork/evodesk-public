import {Injectable} from '@angular/core';
import {HttpEventType, HttpProgressEvent, HttpResponse} from '@angular/common/http';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskCardAttachmentModel} from '../../../app/EvodeskRESTApi/models/DeskCardAttachment.model';

import {DeskRESTService} from '../../../app/EvodeskRESTApi/services/DeskREST.service';

import {EvodeskConfirmModalService} from '../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';
import {EvodeskDeskSyncStateService} from '../state/EvodeskDeskSyncState.service';

@Injectable()
export class EvodeskDeskAttachmentService
{
    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
        private syncState: EvodeskDeskSyncStateService,
        private uiConfirm: EvodeskConfirmModalService,
    ) {}

    attachFile(deskId: number, cardId: number, file: File): Observable<HttpProgressEvent | HttpResponse<DeskCardAttachmentModel>> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create(response => {
            this.rest.uploadFileAndAttachToCard(deskId, cardId, { file: file }).pipe(takeUntil(unsubscribe$)).subscribe(
                httpResponse => {
                    response.next(httpResponse);

                    if (httpResponse.type === HttpEventType.Response) {
                        this.deskState.dispatch({
                            type: EvodeskDeskAction.AttachFile,
                            payload: {
                                cardId: cardId,
                                deskId: deskId,
                                attachment: { ...httpResponse.body },
                            },
                        });

                        response.complete();
                    }
                },
                httpError => {
                    response.error(httpError);

                    this.syncState.dispatchCommonHttpError(httpError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    deleteAttachment(deskId: number, cardId: number, attachmentId: number): Observable<void> {
        return Observable.create(response => {
            this.rest.deleteAttachment(deskId, cardId, attachmentId).subscribe(
                () => {
                    this.deskState.dispatch({
                        type: EvodeskDeskAction.DeleteAttachment,
                        payload: {
                            cardId: cardId,
                            deskId: deskId,
                            attachmentId: attachmentId,
                        },
                    });

                    response.next(undefined);
                    response.complete();
                },
                httpError => {
                    response.error(httpError);

                    this.syncState.dispatchCommonHttpError(httpError);
                },
            );
        });
    }

    askForDeleteAttachment(deskId: number, cardId: number, attachmentId: number): Observable<boolean> {
        return Observable.create(observer => {
            this.uiConfirm.open({
                title: { text: 'EvodeskDesk.services.EvodeskDeskAttachmentService.askForDeleteAttachment.uiConfirm.Title', translate: true },
                text: { text: 'EvodeskDesk.services.EvodeskDeskAttachmentService.askForDeleteAttachment.uiConfirm.Text', translate: true },
                confirmButton: { text: 'EvodeskDesk.services.EvodeskDeskAttachmentService.askForDeleteAttachment.uiConfirm.ConfirmButton', translate: true },
                cancelButton: { text: 'EvodeskDesk.services.EvodeskDeskAttachmentService.askForDeleteAttachment.uiConfirm.CancelButton', translate: true },
                confirm: () => {
                    this.deleteAttachment(deskId, cardId, attachmentId).subscribe(
                        () => {
                            observer.next(true);
                            observer.complete();
                        },
                        (error) => {
                            observer.error(error);

                            this.syncState.dispatchCommonHttpError(error);
                        },
                    );
                },
                cancel: () => {
                    observer.next(false);
                    observer.complete();
                },
            }).subscribe();
        });
    }
}
