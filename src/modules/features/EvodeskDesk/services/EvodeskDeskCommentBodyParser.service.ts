import {Injectable} from '@angular/core';

import * as DOMPurify from 'dompurify';

import {Observable} from 'rxjs';

@Injectable()
export class EvodeskDeskCommentBodyParserService
{
    parseToHtml(input: string): Observable<string> {
        return Observable.create(observer => {
            if (!! input) {
                let result: string = input;

                result = result.replace(/(?:\r\n|\r|\n)/g, '<br/>');
                result = DOMPurify.sanitize(result, { ALLOWED_TAGS: ['b', 'i', 'u', 's', 'br'], KEEP_CONTENT: true });

                observer.next(result);
                observer.complete();
            } else {
                observer.next('');
                observer.complete();
            }
        });
    }
}
