import {Injectable} from '@angular/core';

import {DeskCardLogRecordModel} from '../../../app/EvodeskRESTApi/models/DeskCardLogRecord.model';

interface DictRecord {
    action: string;
    showValue: boolean;
}

type Dict = { [search: string]: DictRecord };

export function getDict(): Dict {
    return {
        'card_create': { action: 'CardCreate', showValue: true },
        'card_create_title': { action: 'CardCreateTitle', showValue: true },
        'card_delete_deletedAt': { action: 'CardDeleteDeletedAt', showValue: false },
        'card_edit_title': { action: 'CardEditTitle', showValue: true },
        'card_edit_description': { action: 'CardEditDescription', showValue: true },
        'card_edit_deadline': { action: 'CardEditDeadline', showValue: true },
        'card_edit_column': { action: 'CardEditColumn', showValue: true },
        'card_check_daily': { action: 'CardCheckDaily', showValue: false },
        'card_uncheck_daily': { action: 'CardUncheckDaily', showValue: false },
        'checklist_add_name': { action: 'ChecklistAddName', showValue: true },
        'checklist_delete_name': { action: 'ChecklistDeleteName', showValue: true },
        'attachment_add_name': { action: 'AttachmentAddName', showValue: true },
        'attachment_delete': { action: 'AttachmentDelete', showValue: true },
        'label_add_name': { action: 'LabelAddName', showValue: true },
        'label_delete_name': { action: 'LabelDeleteName', showValue: true },
        'checkListItem_check_completed': { action: 'CheckListItemCheckCompleted', showValue: true },
        'checkListItem_uncheck_completed': { action: 'CheckListItemUncheckCompleted', showValue: true },
        'card_edit_private': { action: 'CardEditPrivate', showValue: false },
        'card_edit_order': { action: 'CardEditOrder', showValue: false },
        'comment_delete': { action: 'CommentDelete', showValue: false },
        'Comment_edit_text': { action: 'CommentEditText', showValue: false },
    };
}

export interface DeskCommonTranslateEntity
{
    action: string;
    value: string;
    showValue: boolean;
}

export interface EvodeskTranslateLogRecordServiceResources {
    columns: Array<EvodeskTranslateLogRecordServiceResourcesColumn>;
}

export interface EvodeskTranslateLogRecordServiceResourcesColumn
{
    id: number;
    name: string;
}

@Injectable()
export class EvodeskDeskTranslateLogRecordService
{
    private _resources: EvodeskTranslateLogRecordServiceResources = {
        columns: [],
    };

    setResources(set$: (orig: EvodeskTranslateLogRecordServiceResources) => EvodeskTranslateLogRecordServiceResources) {
        this._resources = set$(this._resources);
    }

    translate(input: DeskCardLogRecordModel): DeskCommonTranslateEntity {
        const dictSearch: string = input.changedField
            ? `${input.entity}_${input.action}_${input.changedField}`
            : `${input.entity}_${input.action}`;

        const dictRecord: DictRecord | undefined = getDict()[dictSearch];

        if (dictRecord) {
            return {
                action: `EvodeskDesk.services.EvodeskDeskTranslateLogRecordService.Dict.${dictRecord.action}`,
                showValue: dictRecord.showValue,
                value: dictRecord.showValue
                    ? (this.getValueFor(input) || '')
                    : '',
            };
        } else {
            return {
                action: 'EvodeskDesk.services.EvodeskDeskTranslateLogRecordService.Dict.UNKNOWN',
                showValue: false,
                value: this.getValueFor(input) || '',
            };
        }
    }

    private getValueFor(action: DeskCardLogRecordModel): string {
        if (action.entity === 'card' && action.changedField === 'column') {
            const column: EvodeskTranslateLogRecordServiceResourcesColumn = this._resources.columns.filter(c => c.id === parseInt(action.value, 10))[0];

            if (column) {
                return column.name;
            } else {
                return '[UNKNOWN COLUMN]';
            }
        } else {
            return action.value;
        }
    }
}
