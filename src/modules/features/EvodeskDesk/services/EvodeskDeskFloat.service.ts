import {ComponentRef, Injectable, SimpleChange} from '@angular/core';
import {ComponentPortal} from '@angular/cdk/portal';
import {Overlay, OverlayRef} from '@angular/cdk/overlay';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskCardModel} from '../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskCardFloatContainerComponent as Float} from '../components/common/EvodeskDeskCardFloat/EvodeskDeskCardFloatContainer.component';

import {DeskUpdateCardChangedField} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskDeskUpdateCardService} from './EvodeskDeskUpdateCard.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';

let hasOpenedFloat: boolean = false;

type AfterClosed = undefined;

@Injectable()
export class EvodeskDeskFloatService {
    constructor(
        private overlay: Overlay,
        private updateService: EvodeskDeskUpdateCardService,
        private deskState: EvodeskDeskStateService,
    ) {}

    open(card: DeskCardModel, element: HTMLElement): Observable<AfterClosed> {
        return Observable.create((done) => {
            if (hasOpenedFloat) {
                return;
            }

            const initialTitle: string = card.title;

            const disposed$: Subject<void> = new Subject<void>();
            const componentPortal: ComponentPortal<Float> = new ComponentPortal(Float, this.deskState.snapshot.viewContainerRef);

            hasOpenedFloat = true;

            const overlayRef: OverlayRef = this.overlay.create({
                hasBackdrop: true,
                backdropClass: '__evodesk-mat-dialog-v3-backdrop',
                disposeOnNavigation: true,
                positionStrategy: this.overlay.position().global(),
                scrollStrategy: this.overlay.scrollStrategies.block(),
            });

            overlayRef.backdropClick().pipe(takeUntil(disposed$)).subscribe(() => {
                disposed$.next(undefined);

                overlayRef.dispose();
            });

            const componentRef: ComponentRef<Float> = overlayRef.attach(componentPortal);

            componentRef.instance.deskCardId = card.id;
            componentRef.instance.srcElement = element;

            componentRef.instance.closeEvent.pipe(takeUntil(disposed$)).subscribe(() => {
                overlayRef.dispose();
            });

            componentRef.onDestroy(() => {
                const updatedCard: DeskCardModel = this.deskState.snapshot.sourceCards.filter(sc => sc.id === card.id)[0];

                if (updatedCard) {
                    if (initialTitle !== updatedCard.title) {
                        this.updateService.updateCard(DeskUpdateCardChangedField.Title, {
                            ...updatedCard,
                            title: updatedCard.title,
                        });
                    }
                }

                hasOpenedFloat = false;

                this.deskState.dispatch({
                    type: EvodeskDeskAction.StopEditCard,
                });

                done.next(undefined);
                done.complete();
            });

            componentRef.instance.ngOnChanges({
                deskCardId: new SimpleChange(undefined, componentRef.instance.deskCardId, true),
                srcElement: new SimpleChange(undefined, componentRef.instance.srcElement, true),
            });

            this.deskState.dispatch({
                type: EvodeskDeskAction.EditCard,
                payload: {
                    cardId: card.id,
                    element: element,
                },
            });
        });
    }
}
