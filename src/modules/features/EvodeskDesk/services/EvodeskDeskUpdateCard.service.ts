import {Injectable} from '@angular/core';

import {Subject, Observable} from 'rxjs';
import {filter, takeUntil, refCount, publishLast} from 'rxjs/operators';

import {DeskCardId, DeskCardModel} from '../../../app/EvodeskRESTApi/models/DeskCard.model';

import {filterDeskCardTitle} from '../functions/filterDeskCardTitle.function';

import {DeskRESTService, DeskUpdateCardChangedField} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskDeskSyncStateService} from '../state/EvodeskDeskSyncState.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';

@Injectable()
export class EvodeskDeskUpdateCardService
{
    private nextUpdate: Subject<DeskCardId> = new Subject<DeskCardId>();

    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
        private syncState: EvodeskDeskSyncStateService,
    ) {}

    updateCards(deskId: number, cards: Array<{ card: DeskCardModel, changedField: DeskUpdateCardChangedField }>): Observable<void> {
        const observable: Observable<void> = Observable.create(observer => {
            cards.forEach(i => this.nextUpdate.next(i.card.id));

            this.deskState.dispatch({
                type: EvodeskDeskAction.UpdateCards,
                payload: {
                    cards: cards,
                },
            });

            this.rest.updateCards(deskId, cards).subscribe(
                undefined,
                (error) => {
                    observer.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
            );

            observer.next(undefined);
            observer.complete();
        }).pipe(publishLast(), refCount());

        observable.subscribe();

        return observable;
    }

    updateCard(changedField: DeskUpdateCardChangedField, card: DeskCardModel): Observable<void> {
        this.nextUpdate.next(card.id);

        const observable: Observable<void> = Observable.create(observer => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            if (card.system) {
                observer.next(undefined);
                observer.complete();
            } else {
                this.deskState.dispatch({
                    type: EvodeskDeskAction.UpdateCards,
                    payload: {
                        cards: [{
                            card: {
                                ...card,
                                title: filterDeskCardTitle(card.title),
                            },
                            changedField: changedField,
                        }],
                    },
                });

                this.rest.updateCard({ ...card, title: filterDeskCardTitle(card.title) }, changedField).pipe(
                    takeUntil(this.nextUpdate.pipe(filter(nU => nU === card.id))),
                ).subscribe(
                    (response) => {
                        this.deskState.dispatch({
                            type: EvodeskDeskAction.UpdateCards,
                            payload: {
                                cards: [{
                                    card: response,
                                    changedField: changedField,
                                }],
                            },
                        });

                        observer.next(undefined);
                        observer.complete();
                    },
                    (error) => {
                        observer.error(error);

                        this.syncState.dispatchCommonHttpError(error);
                    },
                );
            }

            return () => {
                unsubscribe$.next(undefined);
            };
        }).pipe(publishLast(), refCount());

        observable.subscribe();

        return observable;
    }
}
