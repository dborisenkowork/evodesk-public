import {Injectable} from '@angular/core';

import {Subject, Observable} from 'rxjs';
import {publishLast, refCount, takeUntil} from 'rxjs/operators';

import * as R from 'ramda';

import {proxy} from '../../../../functions/proxy.function';

import {DeskModel} from '../../../app/EvodeskRESTApi/models/Desk.model';
import {DeskCardModel} from '../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskProjectId} from '../../../app/EvodeskRESTApi/models/DeskProject.model';

import {DeskRESTService} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';
import {EvodeskDeskSyncStateService} from '../state/EvodeskDeskSyncState.service';

export interface AddCardSubmitData
{
    title: string;
    column: number;
    order: number;
    project: DeskProjectId | undefined;
}

let nextMinusId: number = -1;

@Injectable()
export class EvodeskDeskAddCardService
{
    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
        private syncState: EvodeskDeskSyncStateService,
    ) {}

    addCard(formData: AddCardSubmitData): Observable<DeskCardModel> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<any> = new Subject<any>();

            const sameColumnCardIds: Array<number> = this.deskState.snapshot.sourceCards
                .filter(c => c.column === formData.column)
                .map(c => c.order);

            const desk: DeskModel = this.deskState.snapshot.desk;
            const fakeId: number = --nextMinusId;
            const newPosition: number = sameColumnCardIds.length ? sameColumnCardIds.reduce(R.max) + 1 : 0;

            this.deskState.dispatch({
                type: EvodeskDeskAction.AddCard,
                payload: {
                    formData: {
                        ...formData,
                        order: newPosition,
                    },
                    fakeId: fakeId,
                },
            });

            const emit: Function = (next) => {
                this.deskState.dispatch({
                    type: EvodeskDeskAction.AddCardConfirmed,
                    payload: {
                        response: next,
                        formData: formData,
                        fakeId: fakeId,
                    },
                });

                observer.next(next);
                observer.complete();
            };

            this.rest.addCard({
                ...formData,
                deskId: desk.id,
                deskName: desk.name,
                order: newPosition,
            }).pipe(takeUntil(unsubscribe$)).subscribe(
                (next) => {
                    this.deskState.dispatch({
                        type: EvodeskDeskAction.AddCardConfirmed,
                        payload: {
                            response: next,
                            formData: formData,
                            fakeId: fakeId,
                        },
                    });

                    if (formData.project) {
                        next.projects = this.deskState.snapshot.projects
                            .filter(p => p.id === formData.project)
                            .map(p => {
                                return {
                                    id: p.id,
                                    deskId: desk.id,
                                    order: p.order,
                                    title: p.title,
                                };
                            })
                        ;

                        proxy(this.rest.addCardToProject(desk.id, next.id, formData.project)).pipe(takeUntil(unsubscribe$)).subscribe(
                            () => {
                                emit(next);
                            },
                            (error) => {
                                this.syncState.dispatchCommonHttpError(error);

                                observer.error(error);
                            },
                        );
                    } else {
                        emit(next);
                    }
                },
                (error) => {
                    this.syncState.dispatchCommonHttpError(error);

                    observer.error(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        }).pipe(publishLast(), refCount());
    }
}
