import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import {publishLast, refCount} from 'rxjs/operators';

import {DeskId} from '../../../app/EvodeskRESTApi/models/Desk.model';
import {DeskCardId} from '../../../app/EvodeskRESTApi/models/DeskCard.model';

import {DeskRESTService} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskConfirmModalService} from '../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';
import {EvodeskDeskSyncStateService} from '../state/EvodeskDeskSyncState.service';

@Injectable()
export class EvodeskDeskDeleteCardsService
{
    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
        private syncState: EvodeskDeskSyncStateService,
        private uiConfirm: EvodeskConfirmModalService,
    ) {}

    deleteCards(deskId: DeskId, cardIds: Array<DeskCardId>): Observable<void> {
        const observable: Observable<void> = Observable.create(observer => {
            this.deskState.dispatch({
                type: EvodeskDeskAction.DeleteCards,
                payload: {
                    cardIds: cardIds,
                },
            });

            this.rest.deleteCards(deskId, cardIds)
                .subscribe(
                    () => {
                        observer.next(undefined);
                        observer.complete();
                    },
                    (error) => {
                        this.syncState.dispatchCommonHttpError(error);

                        observer.error(error);
                    },
                );
        }).pipe(publishLast(), refCount());

        observable.subscribe();

        return observable;
    }

    askForDeleteSingleCard(deskId: number, cardId: number): Observable<boolean> {
        return Observable.create(observer => {
            this.uiConfirm.open({
                title: { text: 'EvodeskDesk.services.EvodeskDeskDeleteCardsService.askForDeleteSingleCard.uiConfirm.Title', translate: true },
                text: { text: 'EvodeskDesk.services.EvodeskDeskDeleteCardsService.askForDeleteSingleCard.uiConfirm.Text', translate: true },
                confirmButton: { text: 'EvodeskDesk.services.EvodeskDeskDeleteCardsService.askForDeleteSingleCard.uiConfirm.ConfirmButton', translate: true },
                cancelButton: { text: 'EvodeskDesk.services.EvodeskDeskDeleteCardsService.askForDeleteSingleCard.uiConfirm.CancelButton', translate: true },
                confirm: () => {
                    this.deleteCards(deskId, [cardId]).subscribe(
                        undefined,
                        (error) => {
                            this.syncState.dispatchCommonHttpError(error);
                        },
                    );

                    observer.next(true);
                    observer.complete();
                },
                cancel: () => {
                    observer.next(false);
                    observer.complete();
                },
            }).subscribe();
        });
    }

    askForDeleteManyCards(deskId: DeskId, cardIds: Array<DeskCardId>): Observable<boolean> {
        return Observable.create(observer => {
            this.uiConfirm.open({
                title: { text: 'EvodeskDesk.services.EvodeskDeskDeleteCardsService.askForDeleteManyCards.uiConfirm.Title', translate: true },
                text: { text: 'EvodeskDesk.services.EvodeskDeskDeleteCardsService.askForDeleteManyCards.uiConfirm.Text', translate: true },
                confirmButton: { text: 'EvodeskDesk.services.EvodeskDeskDeleteCardsService.askForDeleteManyCards.uiConfirm.ConfirmButton', translate: true },
                cancelButton: { text: 'EvodeskDesk.services.EvodeskDeskDeleteCardsService.askForDeleteManyCards.uiConfirm.CancelButton', translate: true },
                confirm: () => {
                    this.deleteCards(deskId, cardIds).subscribe(
                        () => {
                            observer.next(true);
                            observer.complete();
                        },
                        (error) => {
                            this.syncState.dispatchCommonHttpError(error);

                            observer.error(error);
                        },
                    );
                },
                cancel: () => {
                    observer.next(false);
                    observer.complete();
                },
            }).subscribe();
        });
    }
}
