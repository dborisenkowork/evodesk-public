import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';

import {DeskColumnId} from '../../../app/EvodeskRESTApi/models/DeskColumnDefinition.model';
import {DeskCardId, DeskCardModel} from '../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskId} from '../../../app/EvodeskRESTApi/models/Desk.model';
import {DeskProjectId, DeskProjectModel} from '../../../app/EvodeskRESTApi/models/DeskProject.model';

import {DeskRESTService, DeskUpdateCardChangedField} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';
import {EvodeskDeskSyncStateService} from '../state/EvodeskDeskSyncState.service';
import {EvodeskDeskProjectsService} from './EvodeskDeskProjects.service';

type Column = Array<DeskCardModel>;

function normalizeColumn(input: Column): Column {
    let nextOrder: number = 0;

    return input
        .sort((a, b) => {
            if (a.order > b.order) {
                return 1;
            } else if (a.order === b.order) {
                return 0;
            } else {
                return -1;
            }
        })
        .map(c => {
            return {
                ...c,
                order: nextOrder++,
            };
        });
}

@Injectable()
export class EvodeskDeskMoveCardsService
{
    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
        private syncState: EvodeskDeskSyncStateService,
        private projectsService: EvodeskDeskProjectsService,
    ) {}

    moveCard(cardId: DeskCardId, toColumn: DeskColumnId, toPosition: number): Observable<void> {
        return Observable.create(response => {
            const sourceCard: DeskCardModel = this.deskState.snapshot.sourceCards.filter(c => c.id === cardId)[0];
            const isMovingToAnotherColumn: boolean = toColumn !== sourceCard.column;

            const updateField: DeskUpdateCardChangedField = isMovingToAnotherColumn ? DeskUpdateCardChangedField.Column : DeskUpdateCardChangedField.Order;

            if (isMovingToAnotherColumn) {
                const sourceColumn: Column = normalizeColumn(
                    this.deskState.snapshot.sourceCards.filter(c => c.id !== cardId && c.column === sourceCard.column),
                );

                this.saveColumn(sourceColumn, updateField);
            }

            const currentColumn: Column = normalizeColumn(
                this.deskState.snapshot.sourceCards.filter(c => c.id !== cardId && c.column === toColumn),
            );

            const newColumn: Column = (() => {
                if (currentColumn.length) {
                    return [
                        ...currentColumn.slice(0, toPosition),
                        { ...sourceCard, order: toPosition },
                        ...currentColumn.slice(toPosition),
                    ];
                } else {
                    return [
                        { ...sourceCard, toPosition: 0 },
                    ];
                }
            })();

            let nextOrder: number = 0;

            newColumn.forEach(c => {
                c.order = nextOrder++;
                c.column = toColumn;
            });

            this.saveColumn(newColumn, updateField).subscribe(() => {
                response.next(undefined);
                response.complete();
            });
        });
    }

    moveCardInFilteredSet(cardId: DeskCardId, toColumn: DeskColumnId, toPosition: number): Observable<void> {
        return Observable.create(response => {
            const normalizedSourceColumn: Column = normalizeColumn(this.deskState.snapshot.sourceCards.filter(c => c.column === toColumn));
            const normalizedFilteredColumn: Column = normalizeColumn(this.deskState.snapshot.filteredCards.filter(c => c.column === toColumn));

            if (normalizedFilteredColumn.length) {
                const swapWithCard: DeskCardModel | undefined = normalizedFilteredColumn.filter(c => c.column === toColumn)[toPosition];

                if (swapWithCard) {
                    const set: number = normalizedSourceColumn.filter(c => c.id === swapWithCard.id)[0].order;

                    this.moveCard(cardId, toColumn, set).subscribe(() => {
                        response.next(undefined);
                        response.complete();
                    });
                } else {
                    this.moveCard(cardId, toColumn, normalizedFilteredColumn[normalizedFilteredColumn.length - 1].order + 1).subscribe(() => {
                        response.next(undefined);
                        response.complete();
                    });
                }
            } else {
                this.moveCard(cardId, toColumn, normalizedSourceColumn.length).subscribe(() => {
                    response.next(undefined);
                    response.complete();
                });
            }
        });
    }

    moveAllCardsFromColumnTo(fromColumn: DeskColumnId, toColumn: DeskColumnId): Observable<void> {
        return Observable.create(response => {
            const deskId: DeskId = this.deskState.snapshot.desk.id;
            const sourceCards: Array<DeskCardModel> = this.deskState.snapshot.sourceCards.filter(c => c.column === fromColumn);
            const replaceCards: Array<DeskCardModel> = sourceCards.map(sourceCard => {
                return {
                    ...sourceCard,
                    column: toColumn,
                };
            });

            this.deskState.dispatch({
                type: EvodeskDeskAction.UpdateCards,
                payload: {
                    cards: replaceCards.map(rC => {
                        return {
                            card: rC,
                            changedField: DeskUpdateCardChangedField.Column,
                        };
                    }),
                },
            });

            this.rest.updateCards(deskId, replaceCards.map(card => {
                return {
                    card: card,
                    changedField: DeskUpdateCardChangedField.Column,
                };
            })).subscribe(
                (nextUpdatedCard) => {
                    this.deskState.dispatch({
                        type: EvodeskDeskAction.UpdateCards,
                        payload: {
                            cards: [
                                {
                                    card: nextUpdatedCard,
                                    changedField: DeskUpdateCardChangedField.Column,
                                },
                            ],
                        },
                    });
                },
                (error) => {
                    response.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
                () => {
                    response.next(undefined);
                    response.complete();
                },
            );
        });
    }

    public moveProject(projectId: DeskProjectId, newIndex: number): Observable<void> {
        let counter: number = 1;
        const newOrder: number = newIndex + 1;

        const projects: Array<DeskProjectModel> = this.deskState.snapshot.projects.map(value => {
            if (counter === newOrder) {
                counter++;
            }
            if (value.id === projectId) {
                return {
                    ...value,
                    order: newOrder,
                };
            }
            return {
                ...value,
                order: counter++,
            };
        }).sort((a, b) => {
            return a.order - b.order;
        });

        return this.projectsService.updateProjects(projects);
    }

    private saveColumn(input: Column, changedField: DeskUpdateCardChangedField): Observable<Array<DeskCardModel>> {
        return Observable.create(response => {
            const filtered: Array<DeskCardModel> = input
                .filter(head => ! head.system)
                .filter(head => {
                    const orig: DeskCardModel = this.deskState.snapshot.sourceCards.filter(c => c.id === head.id)[0];

                    return ! orig || orig.order !== head.order || orig.column !== head.column;
                })
            ;

            if (filtered.length) {
                this.deskState.dispatch({
                    type: EvodeskDeskAction.UpdateCards,
                    payload: {
                        cards: filtered.map(f => {
                            return {
                                card: f,
                                changedField: changedField,
                            };
                        }),
                    },
                });

                this.rest.updateCards(this.deskState.snapshot.desk.id, filtered.map(f => {
                    return {
                        card: f,
                        changedField: changedField,
                    };
                })).subscribe(
                    (nextUpdatedCard) => {
                        this.deskState.dispatch({
                            type: EvodeskDeskAction.UpdateCards,
                            payload: {
                                cards: [
                                    {
                                        card: nextUpdatedCard,
                                        changedField: changedField,
                                    },
                                ],
                            },
                        });
                    },
                    (httpError) => {
                        this.syncState.dispatchCommonHttpError(httpError);
                    },
                    () => {
                        response.next(filtered);
                        response.complete();
                    },
                );
            } else {
                response.next([]);
                response.complete();
            }
        });
    }
}
