import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';

import {DeskLabelModel} from '../../../app/EvodeskRESTApi/models/DeskLabel.model';
import {DeskCardModel} from '../../../app/EvodeskRESTApi/models/DeskCard.model';

import {DeskRESTService, DeskUpdateCardChangedField} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';
import {EvodeskDeskSyncStateService} from '../state/EvodeskDeskSyncState.service';

export interface NewLabelRequest
{
    deskId: number;
    color: string;
    name: string;
}

export interface EditLabelRequest
{
    deskId: number;
    labelId: number;
    color: string;
    name: string;
}

export interface DeleteLabelRequest
{
    deskId: number;
    labelId: number;
}

export interface AddLabelToEvodeskDeskCardRequest
{
    deskId: number;
    cardId: number;
    label: DeskLabelModel;
}

export interface ExcludeLabelFromEvodeskDeskCardRequest
{
    deskId: number;
    cardId: number;
    labelId: number;
}

@Injectable()
export class EvodeskDeskLabelService
{
    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
        private syncState: EvodeskDeskSyncStateService,
    ) {}

    createNewLabel(request: NewLabelRequest): Observable<DeskLabelModel> {
        return Observable.create(response => {
            this.rest.newLabel(request.deskId, request.name, request.color)
                .subscribe(
                    (next) => {
                        this.deskState.dispatch({
                            type: EvodeskDeskAction.AddLabel,
                            payload: {
                                label: next,
                            },
                        });

                        response.next(next);
                        response.complete();
                    },
                    (error) => {
                        response.error(error);

                        this.syncState.dispatchCommonHttpError(error);
                    },
                );
        });
    }

    editLabel(request: EditLabelRequest): Observable<DeskLabelModel> {
        return Observable.create(response => {
            const origLabel: DeskLabelModel = this.deskState.snapshot.labels.filter(l => l.id === request.labelId)[0];

            this.rest.editLabel(request.deskId, request.labelId, request.name, request.color)
                .subscribe(
                    (next) => {
                        this.deskState.dispatch({
                            type: EvodeskDeskAction.UpdateLabel,
                            payload: {
                                origLabel: origLabel,
                                shouldBe: next,
                            },
                        });

                        response.next(next);
                        response.complete();
                    },
                    (error) => {
                        response.error(error);

                        this.syncState.dispatchCommonHttpError(error);
                    },
                );
        });
    }

    deleteLabel(request: DeleteLabelRequest): Observable<void> {
        return Observable.create(response => {
            this.deskState.dispatch({
                type: EvodeskDeskAction.DeleteLabel,
                payload: {
                    labelId: request.labelId,
                },
            });

            this.rest.deleteLabel(request.deskId, request.labelId)
                .subscribe(
                    () => {
                        response.next(undefined);
                        response.complete();
                    },
                    (error) => {
                        response.error(error);

                        this.syncState.dispatchCommonHttpError(error);
                    },
                );
        });
    }

    includeLabelToCard(request: AddLabelToEvodeskDeskCardRequest): Observable<DeskLabelModel> {
        return Observable.create(response => {
            const sourceCard: DeskCardModel = this.deskState.snapshot.sourceCards.filter(sc => sc.id === request.cardId)[0];

            if (sourceCard.labels.filter(l => l.id === request.label.id).length) {
                response.next(sourceCard);
                response.complete();
            } else {
                this.deskState.dispatch({
                    type: EvodeskDeskAction.UpdateCards,
                    payload: {
                        cards: [{
                            card: {
                                ...sourceCard,
                                labels: [...sourceCard.labels, request.label],
                            },
                            changedField: DeskUpdateCardChangedField.Labels,
                        }],
                    },
                });

                this.rest.includeLabelToCard(request.deskId, request.cardId, request.label.id)
                    .subscribe(
                        (next) => {
                            response.next(next);
                            response.complete();
                        },
                        (error) => {
                            response.error(error);

                            this.syncState.dispatchCommonHttpError(error);
                        },
                    );
            }
        });
    }

    excludeLabelFromCard(request: ExcludeLabelFromEvodeskDeskCardRequest): Observable<void> {
        return Observable.create(response => {
            const sourceCard: DeskCardModel = this.deskState.snapshot.sourceCards.filter(sc => sc.id === request.cardId)[0];

            if (sourceCard.labels.filter(l => l.id === request.labelId).length === 0) {
                response.next(sourceCard);
                response.complete();
            } else {
                this.deskState.dispatch({
                    type: EvodeskDeskAction.UpdateCards,
                    payload: {
                        cards: [{
                            card: {
                                ...sourceCard,
                                labels: sourceCard.labels.filter(l => l.id !== request.labelId),
                            },
                            changedField: DeskUpdateCardChangedField.Labels,
                        }],
                    },
                });

                this.rest.excludeLabelFromCard(request.deskId, request.cardId, request.labelId).subscribe(
                    () => {
                        response.next(undefined);
                        response.complete();
                    },
                    (error) => {
                        response.error(error);

                        this.syncState.dispatchCommonHttpError(error);
                    },
                );
            }
        });
    }

    getLabelsForCard(cardId: number): Observable<Array<DeskLabelModel>> {
        return Observable.create(response => {
            const sourceCard: DeskCardModel = this.deskState.snapshot.sourceCards.filter(sc => sc.id === cardId)[0];

            if (this.deskState.snapshot.desk.id === sourceCard.deskId) {
                response.next(this.deskState.snapshot.labels);
                response.complete();
            } else {
                this.rest.labels(sourceCard.deskId).subscribe(next => {
                    response.next(next);
                    response.complete();
                }, () => {
                    response.error();
                });
            }
        });
    }
}
