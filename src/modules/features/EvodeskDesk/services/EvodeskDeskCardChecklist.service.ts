import {Injectable} from '@angular/core';

import {Subject, Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskCardChecklistItemModel, DeskCardChecklistModel} from '../../../app/EvodeskRESTApi/models/DeskCardChecklist.model';

import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';
import {DeskRESTService} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskDeskSyncStateService} from '../state/EvodeskDeskSyncState.service';

export interface EvodeskDeskCardChecklistAddRequest
{
    title: string;
}

export interface EvodeskDeskCardChecklistAddItemRequest
{
    title: string;
    completed: boolean;
}

export interface EvodeskDeskCardChecklistUpdateDeleteItemRequest
{
    checklistItemId: number;
}

@Injectable()
export class EvodeskDeskCardChecklistService
{
    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
        private syncState: EvodeskDeskSyncStateService,
    ) {}

    addChecklist(cardId: number, request: EvodeskDeskCardChecklistAddRequest): Observable<DeskCardChecklistModel> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<any> = new Subject<any>();

            const deskId: number = this.deskState.snapshot.desk.id;

            this.rest.newChecklist(deskId, cardId, { title: request.title }).pipe(takeUntil(unsubscribe$)).subscribe(
                (response) => {
                    this.deskState.dispatch({
                        type: EvodeskDeskAction.AddChecklist,
                        payload: {
                            cardId: cardId,
                            checklist: response,
                        },
                    });

                    observer.next(response);
                    observer.complete();
                },
                (error) => {
                    observer.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    updateChecklist(cardId: number, checklistId, set$: DeskCardChecklistModel): Observable<DeskCardChecklistModel> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<any> = new Subject<any>();

            const deskId: number = this.deskState.snapshot.desk.id;

            this.rest.updateChecklist(deskId, cardId, set$).pipe(takeUntil(unsubscribe$)).subscribe(
                (response) => {
                    this.deskState.dispatch({
                        type: EvodeskDeskAction.UpdateChecklist,
                        payload: {
                            cardId: cardId,
                            checklist: response,
                        },
                    });

                    observer.next(response);
                    observer.complete();
                },
                (error) => {
                    observer.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    deleteChecklist(cardId: number, checklistId): Observable<void> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<any> = new Subject<any>();

            const deskId: number = this.deskState.snapshot.desk.id;

            this.deskState.dispatch({
                type: EvodeskDeskAction.DeleteChecklist,
                payload: {
                    cardId: cardId,
                    checklist: this.deskState.snapshot.sourceCards.filter(sc => sc.id === cardId)[0].checklists.filter(c => c.id === checklistId)[0],
                },
            });

            this.rest.deleteChecklist(deskId, cardId, { checklistId: checklistId }).pipe(takeUntil(unsubscribe$)).subscribe(
                () => {
                    observer.next(undefined);
                    observer.complete();
                },
                (error) => {
                    observer.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    addChecklistItem(cardId: number, checklistId: number, request: EvodeskDeskCardChecklistAddItemRequest): Observable<DeskCardChecklistItemModel> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<any> = new Subject<any>();

            const deskId: number = this.deskState.snapshot.desk.id;

            this.rest.newChecklistItem(deskId, cardId, { checklistId: checklistId, title: request.title, completed: request.completed }).pipe(takeUntil(unsubscribe$)).subscribe(
                (response) => {
                    this.deskState.dispatch({
                        type: EvodeskDeskAction.AddChecklistItem,
                        payload: {
                            cardId: cardId,
                            checklistId: checklistId,
                            checklistItem: response,
                        },
                    });

                    observer.next(response);
                    observer.complete();
                },
                (error) => {
                    observer.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    updateChecklistItem(cardId: number, checklistId: number, set$: DeskCardChecklistItemModel): Observable<DeskCardChecklistItemModel> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<any> = new Subject<any>();

            const deskId: number = this.deskState.snapshot.desk.id;

            this.rest.updateChecklistItem(deskId, cardId, checklistId, set$).pipe(takeUntil(unsubscribe$)).subscribe(
                (response) => {
                    this.deskState.dispatch({
                        type: EvodeskDeskAction.UpdateChecklistItem,
                        payload: {
                            cardId: cardId,
                            checklistId: checklistId,
                            checklistItem: response,
                        },
                    });

                    observer.next(response);
                    observer.complete();
                },
                (error) => {
                    observer.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    deleteChecklistItem(cardId: number, checklistId: number, request: EvodeskDeskCardChecklistUpdateDeleteItemRequest): Observable<void> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<any> = new Subject<any>();

            const deskId: number = this.deskState.snapshot.desk.id;

            this.deskState.dispatch({
                type: EvodeskDeskAction.DeleteChecklistItem,
                payload: {
                    cardId: cardId,
                    checklistId: checklistId,
                    checklistItem: this.deskState.snapshot.sourceCards
                        .filter(sc => sc.id === cardId)[0].checklists
                        .filter(cl => cl.id === checklistId)[0].items
                        .filter(ci => ci.id === request.checklistItemId)[0],
                },
            });

            this.rest.deleteChecklistItem(deskId, cardId, { checklistId: checklistId, checklistItemId: request.checklistItemId }).pipe(takeUntil(unsubscribe$)).subscribe(
                () => {
                    observer.next(undefined);
                    observer.complete();
                },
                (error) => {
                    observer.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
