import {Injectable} from '@angular/core';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import * as R from 'ramda';
import * as moment from 'moment';
import {Moment} from 'moment';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../types/Backend.types';

import {EvodeskDeskAction, EvodeskDeskState, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';

import {DeskCardModel} from '../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskFilterDeadlineOptions, DeskFilterDeadlineVariant, DeskFilterLabelOptions, DeskFilterParticipantOptions, DeskFilterProjectOptions, DeskFilterQueryStringOptions, DeskFilterType, IDeskFilters} from '../models/DeskFilters.model';
import {DeskLabelId} from '../../../app/EvodeskRESTApi/models/DeskLabel.model';
import {DeskProjectId} from '../../../app/EvodeskRESTApi/models/DeskProject.model';
import {DeskParticipantId} from '../../../app/EvodeskRESTApi/models/DeskParticipant.model';

export interface EvodeskDeskFilter
{
    filter(input: Array<DeskCardModel>, options: any): Array<DeskCardModel>;
}

@Injectable()
export class EvodeskDeskFilterService
{
    private unsubscribe$: Subject<void> = new Subject<void>();

    constructor(
        private state: EvodeskDeskStateService,
        private deskState: EvodeskDeskStateService,
    ) {}

    enableSync(): void {
        this.state.current$.pipe(
            takeUntil(this.unsubscribe$),
            filter(states => states.prev !== undefined),
            filter(states => (states.current.sourceCards !== states.prev.sourceCards) || (states.current.filters !== states.prev.filters)),
        ).subscribe(() => this.applyFilters());
    }

    disableSync(): void {
        this.unsubscribe$.next(undefined);
    }

    public applyFilters(): void {
        const orig: EvodeskDeskState = this.deskState.snapshot;

        let filtered: Array<DeskCardModel> = [...orig.sourceCards];

        const filtersQueryString: Array<IDeskFilters<DeskFilterQueryStringOptions>> = orig.filters.filter(f => f.type === DeskFilterType.QueryString) as any;
        const filtersParticipant: Array<IDeskFilters<DeskFilterParticipantOptions>> = orig.filters.filter(f => f.type === DeskFilterType.Participant) as any;
        const filtersLabel: Array<IDeskFilters<DeskFilterLabelOptions>> = orig.filters.filter(f => f.type === DeskFilterType.Label) as any;
        const filtersProject: Array<IDeskFilters<DeskFilterProjectOptions>> = orig.filters.filter(f => f.type === DeskFilterType.Project) as any;
        const filtersDeadline: Array<IDeskFilters<DeskFilterDeadlineOptions>> = orig.filters.filter(f => f.type === DeskFilterType.Deadline) as any;

        if (filtersQueryString.length) {
            const queryStrings: Array<string> = filtersQueryString.map(f => f.options.search);

            filtered = filtered.filter(f => queryStrings.some((search) => !!~f.title.indexOf(search)));
        }

        if (filtersParticipant.length) {
            const participantIds: Array<DeskParticipantId> = filtersParticipant.map(f => f.options.participantId);

            filtered = filtered.filter(c => R.intersection(participantIds, c.responsible.map(p => p.id)).length > 0);
        }

        if (filtersDeadline.length) {
            const reference: Moment = moment();

            const enableToday: boolean = filtersDeadline.filter(f => f.options.variant === DeskFilterDeadlineVariant.ExceedToday).length > 0;
            const enableWeek: boolean = filtersDeadline.filter(f => f.options.variant === DeskFilterDeadlineVariant.ExceedWeek).length > 0;
            const enableExceeded: boolean = filtersDeadline.filter(f => f.options.variant === DeskFilterDeadlineVariant.Exceeded).length > 0;

            filtered = filtered.filter(f => {
                if (f.deadline) {
                    const compare: Moment = moment(f.deadline, BACKEND_DATE_FORMAT_AS_MOMENT);

                    if (enableToday && compare.isSame(reference.clone().startOf('day'), 'd')) {
                        return true;
                    }

                    if (enableWeek && reference.startOf('week').diff(compare) < 0 && reference.endOf('week').diff(compare) > 0) {
                        return true;
                    }

                    if (enableExceeded && reference.diff(compare) > 0) {
                        return true;
                    }

                    return false;
                } else {
                    return false;
                }
            });
        }

        if (filtersLabel.length) {
            const labelIds: Array<DeskLabelId> = filtersLabel.map(f => f.options.labelId);

            filtered = filtered.filter(c => R.intersection(labelIds, c.labels.map(l => l.id)).length > 0);
        }

        if (filtersProject.length) {
            const projectIds: Array<DeskProjectId> = filtersProject.map(f => f.options.projectId);

            filtered = filtered.filter(c => R.intersection(projectIds, c.projects.map(p => p.id)).length > 0);
        }

        this.deskState.dispatch({
            type: EvodeskDeskAction.FiltersApply,
            payload: {
                filtered: filtered,
            },
        });
    }
}
