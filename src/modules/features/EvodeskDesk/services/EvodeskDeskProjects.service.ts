import {Injectable, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Subject, Observable, forkJoin} from 'rxjs';
import {publishLast, refCount, takeUntil} from 'rxjs/operators';

import * as R from 'ramda';

import {DeskProjectId, DeskProjectModel} from '../../../app/EvodeskRESTApi/models/DeskProject.model';
import {DeskId, DeskModel} from '../../../app/EvodeskRESTApi/models/Desk.model';

import {DeskAddProjectResponse200, DeskRESTService, DeskUpdateProjectResponse200} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';
import {EvodeskDeskSyncStateService} from '../state/EvodeskDeskSyncState.service';
import {EvodeskInputModalService} from '../../../app/EvodeskApp/components/shared/EvodeskInputModal/EvodeskInputModal.service';
import {EvodeskConfirmModalService} from '../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';

export interface AddProjectSubmitData
{
    title: string;
    order: number;
}

export interface AddProjectSubmitEventData
{
    formData: AddProjectSubmitData;
    fakeId: number;
}

export interface AddAndFetchedProjectSubmitEventData
{
    response: DeskAddProjectResponse200;
    formData: AddProjectSubmitData;
    fakeId: number;
}

let nextMinusId: number = -1;

@Injectable()
export class EvodeskDeskProjectsService
{
    private nextUpdate: Subject<DeskProjectId> = new Subject<DeskProjectId>();

    constructor(
        private fb: FormBuilder,
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
        private syncState: EvodeskDeskSyncStateService,
        private inputModal: EvodeskInputModalService,
        private confirmModal: EvodeskConfirmModalService,
    ) {}

    askForAddProject(viewContainerRef: ViewContainerRef): void {
        const prefix: string = 'EvodeskDesk.services.EvodeskDeskProjectsService.AddProjectInputModal';
        const form: FormGroup = this.fb.group({ response: [null, [Validators.required, Validators.maxLength(32)]] });

        this.inputModal.open({
            title: {
                text: `${prefix}.Title`,
                translate: true,
            },
            text: {
                text: `${prefix}.Text`,
                translate: true,
            },
            confirmButton: {
                text: `${prefix}.Confirm`,
                translate: true,
            },
            cancelButton: {
                text: `${prefix}.Cancel`,
                translate: true,
            },
            formControl: form.get('response'),
            viewContainerRef: viewContainerRef,
            confirm: () => {
                const projectTitle: string = form.value.response;

                this.addProject({
                    title: projectTitle,
                    order: this.deskState.snapshot.projects.length,
                }).subscribe();
            },
        }).subscribe();
    }

    addProject(formData: AddProjectSubmitData): Observable<DeskProjectModel> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<any> = new Subject<any>();

            const projects: Array<DeskProjectModel> = this.deskState.snapshot.projects;

            const desk: DeskModel = this.deskState.snapshot.desk;
            const fakeId: number = --nextMinusId;
            const newPosition: number = projects.length ? projects.map(p => p.order).reduce(R.max) + 1 : 0;

            this.deskState.dispatch({
                type: EvodeskDeskAction.AddProject,
                payload: {
                    formData: {
                        ...formData,
                        order: newPosition,
                    },
                    fakeId: fakeId,
                },
            });

            this.rest.addProject({
                deskId: desk.id,
                deskName: desk.name,
                orders: newPosition,
                title: formData.title,
                members: [],
                columnId: this.deskState.snapshot.columns.filter(c => c.entities === 'projects')[0].id,
            }).pipe(takeUntil(unsubscribe$)).subscribe(
                (next) => {
                    this.deskState.dispatch({
                        type: EvodeskDeskAction.AddProjectConfirmed,
                        payload: {
                            response: next,
                            formData: formData,
                            fakeId: fakeId,
                        },
                    });

                    observer.next(next);
                    observer.complete();
                },
                (error) => {
                    this.syncState.dispatchCommonHttpError(error);

                    observer.error(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        }).pipe(publishLast(), refCount());
    }

    askForEditProject(source: DeskProjectModel, viewContainerRef: ViewContainerRef): void {
        const prefix: string = 'EvodeskDesk.services.EvodeskDeskProjectsService.EditProjectInputModal';
        const form: FormGroup = this.fb.group({ response: [null, [Validators.required, Validators.maxLength(32)]] });

        form.patchValue({
            response: source.title,
        });

        this.inputModal.open({
            title: {
                text: `${prefix}.Title`,
                translate: true,
            },
            text: {
                text: `${prefix}.Text`,
                translate: true,
            },
            confirmButton: {
                text: `${prefix}.Confirm`,
                translate: true,
            },
            cancelButton: {
                text: `${prefix}.Cancel`,
                translate: true,
            },
            formControl: form.get('response'),
            viewContainerRef: viewContainerRef,
            confirm: () => {
                const projectTitle: string = form.value.response;

                this.updateProject({
                    ...source,
                    title: projectTitle,
                }).subscribe();
            },
        }).subscribe();
    }

    updateProject(project: DeskProjectModel): Observable<void> {
        this.nextUpdate.next(project.id);

        const observable: Observable<void> = Observable.create(observer => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            this.deskState.dispatch({
                type: EvodeskDeskAction.UpdateProjects,
                payload: {
                    projects: [{
                        project: project,
                    }],
                },
            });

            this.rest.updateProject({ deskId: this.deskState.snapshot.desk.id, projectId: project.id, title: project.title, orders: project.order }).pipe(takeUntil(this.nextUpdate)).subscribe(
                () => {
                    this.deskState.dispatch({
                        type: EvodeskDeskAction.UpdateProjectsConfirmed,
                        payload: {
                            projects: [{
                                project: project,
                            }],
                        },
                    });

                    observer.next(undefined);
                    observer.complete();
                },
                (error) => {
                    observer.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
            );

            return () => {
                this.nextUpdate.next(undefined);

                unsubscribe$.next(undefined);
            };
        }).pipe(publishLast(), refCount());

        observable.subscribe();

        return observable;
    }

    updateProjects(projects: Array<DeskProjectModel>): Observable<void> {
        this.nextUpdate.next(undefined);

        const observable: Observable<void> = Observable.create(observer => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            this.deskState.dispatch({
                type: EvodeskDeskAction.UpdateProjects,
                payload: {
                    projects: projects.map(p => {
                        return { project: p };
                    }),
                },
            });

            const queries: Array<Observable<DeskUpdateProjectResponse200>> = [];
            projects.forEach(p => {
                queries.push(this.rest.updateProject({ deskId: this.deskState.snapshot.desk.id, projectId: p.id, title: p.title, orders: p.order }));
            });

            forkJoin(queries).pipe(takeUntil(this.nextUpdate)).subscribe(
                () => {
                    this.deskState.dispatch({
                        type: EvodeskDeskAction.UpdateProjectsConfirmed,
                        payload: {
                            projects: projects.map(p => {
                                return { project: p };
                            }),
                        },
                    });

                    observer.next(undefined);
                    observer.complete();
                },
                (error) => {
                    observer.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
            );

            return () => {
                this.nextUpdate.next(undefined);

                unsubscribe$.next(undefined);
            };
        }).pipe(publishLast(), refCount());

        observable.subscribe();

        return observable;
    }

    askForDeleteProject(project: DeskProjectModel, viewContainerRef: ViewContainerRef): void {
        const prefix: string = 'EvodeskDesk.services.EvodeskDeskProjectsService.ConfirmDeleteProjectModal';

        this.confirmModal.open({
            title: {
                text: project.title,
                translate: true,
            },
            text: {
                text: `${prefix}.Text`,
                translate: true,
            },
            confirmButton: {
                text: `${prefix}.Confirm`,
                translate: true,
            },
            cancelButton: {
                text: `${prefix}.Cancel`,
                translate: true,
            },
            viewContainerRef: viewContainerRef,
            confirm: () => {
                this.deleteProjects(this.deskState.snapshot.desk.id, project.id).subscribe();
            },
        }).subscribe();
    }

    deleteProjects(deskId: DeskId, projectId: DeskProjectId): Observable<void> {
        const observable: Observable<void> = Observable.create(observer => {
            this.deskState.dispatch({
                type: EvodeskDeskAction.DeleteProject,
                payload: {
                    projectId: projectId,
                },
            });

            this.rest.deleteProject(deskId, projectId).subscribe(
                () => {
                    observer.next(undefined);
                    observer.complete();
                },
                (error) => {
                    this.syncState.dispatchCommonHttpError(error);

                    observer.error(error);
                },
            );
        }).pipe(publishLast(), refCount());

        observable.subscribe();

        return observable;
    }
}
