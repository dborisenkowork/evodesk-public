import {Injectable, SimpleChange, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';

import {Subject} from 'rxjs';
import {take, takeUntil} from 'rxjs/operators';

import {v3BottomDeskCardModalMatDialogConfig} from '../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {DeskCardId} from '../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskCardModalContainerComponent} from '../components/modal/EvodeskDeskCardModal/EvodeskDeskCardModalContainer.component';

@Injectable()
export class EvodeskDeskCardModalService
{
    private _viewContainerRef: ViewContainerRef;
    private _current: MatDialogRef<EvodeskDeskCardModalContainerComponent>;

    constructor(
        private matDialog: MatDialog,
        private router: Router,
        private activatedRoute: ActivatedRoute,
    ) {}

    set viewContainerRef(v: ViewContainerRef) {
        this._viewContainerRef = v;
    }

    navigateToCardModel(cardId: DeskCardId): void {
        this.router.navigate(['.'], { relativeTo: this.activatedRoute, queryParams: { card: cardId } });
    }

    openCardModal(cardId: DeskCardId): void {
        const closed$: Subject<void> = new Subject<void>();

        const dialogRef: MatDialogRef<EvodeskDeskCardModalContainerComponent> = this.matDialog.open(EvodeskDeskCardModalContainerComponent, {
            ...v3BottomDeskCardModalMatDialogConfig,
            autoFocus: true,
            viewContainerRef: this._viewContainerRef,
            closeOnNavigation: true,
            disableClose: true,
            data: {
                cardId: cardId,
            },
        });

        dialogRef.afterClosed().pipe(takeUntil(closed$)).subscribe(() => {
            this.activatedRoute.queryParams.pipe(take(1)).subscribe((qp) => {
                if (qp['card']) {
                    this.router.navigate(['.'], { relativeTo: this.activatedRoute, queryParams: { card: undefined } });
                }
            });

            closed$.next(undefined);
        });

        dialogRef.backdropClick().pipe(takeUntil(closed$)).subscribe(() => {
            dialogRef.close();
        });

        dialogRef.componentInstance.closeEvent.pipe(takeUntil(closed$)).subscribe(() => {
            this._current = undefined;

            dialogRef.close();
        });

        dialogRef.componentInstance.cardId = cardId;
        dialogRef.componentInstance.ngOnChanges({ cardId: new SimpleChange(undefined, dialogRef.componentInstance.cardId, true) });

        setTimeout(() => {
            dialogRef.updatePosition();
        });

        this._current = dialogRef;
    }

    isOpened(): boolean {
        return !! this._current;
    }

    closeCardModal(): void {
        if (this._current) {
            this._current.close();
        }
    }
}
