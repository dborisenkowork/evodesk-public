import {Injectable} from '@angular/core';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskId} from '../../../app/EvodeskRESTApi/models/Desk.model';
import {UserModel} from '../../../app/EvodeskRESTApi/models/user/User.model';
import {DeskParticipantId, DeskParticipantModel} from '../../../app/EvodeskRESTApi/models/DeskParticipant.model';

import {DeskRESTService, DeskSendInviteResponseError, DeskSendInviteResponseSuccess} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';

@Injectable()
export class EvodeskDeskParticipantService
{
    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
    ) {}

    addParticipantToDesk(deskId: DeskId, user: UserModel): Observable<DeskParticipantModel> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((response) => {
            this.rest.addMemberToDesk(deskId, { email: user.email, active: true }).pipe(takeUntil(unsubscribe$)).subscribe(
                (httpResponse) => {
                    if (httpResponse.status === 'ok') {
                        const member: DeskParticipantModel = {
                            id: httpResponse.user.id,
                            avatar: httpResponse.user.avatar,
                            email: httpResponse.user.email,
                        };

                        this.deskState.dispatch({
                            type: EvodeskDeskAction.AddDeskParticipant,
                            payload: {
                                participant: member,
                            },
                        });

                        response.next(member);
                    } else {
                        response.error();
                    }
                },
                (error) => {
                    response.error(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    removeParticipantFromDesk(deskId: DeskId, deskParticipantId: DeskParticipantId): Observable<void> {
        return this.rest.deleteMemberFromDesk(deskId, deskParticipantId);
    }

    inviteByEmail(deskId: DeskId, email: string): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create((response) => {
            this.rest.sendInviteToDesk(deskId, email).pipe(takeUntil(unsubscribe$)).subscribe(
                (httpResponse) => {
                    if ((<DeskSendInviteResponseSuccess>httpResponse).status === 'ok') {
                        response.next(undefined);
                        response.complete();
                    } else {
                        if ((<DeskSendInviteResponseError>httpResponse).errors) {
                            response.error((<DeskSendInviteResponseError>httpResponse).errors[0]);
                        } else {
                            response.error(undefined);
                        }
                    }
                },
                () => {
                    response.error();
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
