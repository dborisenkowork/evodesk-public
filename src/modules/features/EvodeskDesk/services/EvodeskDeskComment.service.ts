import {Injectable} from '@angular/core';

import {Observable, Subject} from 'rxjs';

import {DeskCardCommentModel} from '../../../app/EvodeskRESTApi/models/DeskCardComment.model';
import {DeskCardLogRecordModel} from '../../../app/EvodeskRESTApi/models/DeskCardLogRecord.model';

import {EvodeskConfirmModalService} from '../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';
import {EvodeskDeskSyncStateService} from '../state/EvodeskDeskSyncState.service';
import {DeskRESTService, DeskUpdateCardCommentChangedField} from '../../../app/EvodeskRESTApi/services/DeskREST.service';

export interface EvodeskDeskCommentServiceAddRequest {
    deskId: number;
    cardId: number;
    text: string;
}

export interface EvodeskDeskCommentServiceUpdateRequest {
    deskId: number;
    cardId: number;
    updatedComment: DeskCardCommentModel;
}

export interface EvodeskDeskCommentServiceDeleteRequest {
    deskId: number;
    cardId: number;
    commentId: number;
}

@Injectable()
export class EvodeskDeskCommentService
{
    constructor(
        private rest: DeskRESTService,
        private deskState: EvodeskDeskStateService,
        private syncState: EvodeskDeskSyncStateService,
        private uiConfirm: EvodeskConfirmModalService,
    ) {}

    addComment(request: EvodeskDeskCommentServiceAddRequest): Observable<DeskCardCommentModel> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            this.rest.addComment(request.deskId, request.cardId, request.text).subscribe(
                (next) => {
                    this.deskState.dispatch({
                        type: EvodeskDeskAction.AddComment,
                        payload: next,
                    });

                    observer.next(next);
                    observer.complete();
                },
                (error) => {
                    observer.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    updateComment(request: EvodeskDeskCommentServiceUpdateRequest): Observable<DeskCardCommentModel> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            this.rest.updateComment(request.deskId, request.updatedComment, DeskUpdateCardCommentChangedField.Text).subscribe(
                (next) => {
                    this.deskState.dispatch({
                        type: EvodeskDeskAction.UpdateComment,
                        payload: next,
                    });

                    observer.next(next);
                    observer.complete();
                },
                (error) => {
                    observer.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    deleteComment(request: EvodeskDeskCommentServiceDeleteRequest): Observable<void> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            this.deskState.dispatch({
                type: EvodeskDeskAction.DeleteComment,
                payload: {
                    cardId: request.cardId,
                    commentId: request.commentId,
                },
            });

            this.rest.deleteComment(request.deskId, request.cardId, request.commentId).subscribe(
                () => {
                    observer.next(undefined);
                    observer.complete();
                },
                (error) => {
                    observer.error(error);

                    this.syncState.dispatchCommonHttpError(error);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    askAndDeleteOnConfirmComment(request: EvodeskDeskCommentServiceDeleteRequest): Observable<boolean> {
        return Observable.create(observer => {
            this.uiConfirm.open({
                title: {
                    text: 'EvodeskDesk.services.EvodeskDeskCommentService.askAndDeleteOnConfirmComment.uiConfirm.Title',
                    translate: true,
                },
                text: {
                    text: 'EvodeskDesk.services.EvodeskDeskCommentService.askAndDeleteOnConfirmComment.uiConfirm.Text',
                    translate: true,
                },
                confirmButton: {
                    text: 'EvodeskDesk.services.EvodeskDeskCommentService.askAndDeleteOnConfirmComment.uiConfirm.ConfirmButton',
                    translate: true,
                },
                cancelButton: {
                    text: 'EvodeskDesk.services.EvodeskDeskCommentService.askAndDeleteOnConfirmComment.uiConfirm.CancelButton',
                    translate: true,
                },
                confirm: () => {
                    this.deleteComment(request)
                        .subscribe(
                            () => {
                                observer.next(true);
                                observer.complete();
                            },
                            (error) => {
                                observer.error(error);
                            },
                        )
                    ;
                },
                cancel: () => {
                    observer.next(false);
                    observer.complete();
                },
            }).subscribe();
        });
    }

    markChangedComments(comments: Array<DeskCardCommentModel>, actions: Array<DeskCardLogRecordModel>) {
        actions
            .filter(action => action.entity === 'comment' && action.action === 'edit')
            .forEach(action => {
                const foundComment: DeskCardCommentModel = comments.find(comment => comment.id.toString() === action.entity_id);

                if (foundComment) {
                    foundComment.changed = true;
                }
            });
    }
}
