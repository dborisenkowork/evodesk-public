export function filterDeskCardTitle(input: string): string {
    let result: string = input;

    result = result.replace(/(?:\r\n|\r|\n)/g, ' ');

    return result;
}
