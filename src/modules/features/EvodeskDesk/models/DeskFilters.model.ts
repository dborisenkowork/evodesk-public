import {DeskLabelId} from '../../../app/EvodeskRESTApi/models/DeskLabel.model';
import {DeskParticipantId} from '../../../app/EvodeskRESTApi/models/DeskParticipant.model';
import {DeskProjectId} from '../../../app/EvodeskRESTApi/models/DeskProject.model';

export enum DeskFilterType {
    QueryString = 'query-string',
    Label = 'label',
    Deadline = 'deadline',
    Participant = 'participant',
    Project = 'project',
}

export enum DeskFilterDeadlineVariant {
    ExceedToday = <any>'exceed-today',
    ExceedWeek = <any>'exceed-week',
    Exceeded = <any>'exceeded',
}

export interface DeskFilterQueryStringOptions {
    search: string;
}

export interface DeskFilterLabelOptions {
    labelId: DeskLabelId;
}

export interface DeskFilterDeadlineOptions {
    variant: DeskFilterDeadlineVariant;
}

export interface DeskFilterParticipantOptions {
    participantId: DeskParticipantId;
}

export interface DeskFilterProjectOptions {
    projectId: DeskProjectId;
}

export interface IDeskFilters<T> {
    type: DeskFilterType;
    createdAt: Date;
    options: T;
}

export type DeskFilter =
    { createdAt: Date; type: DeskFilterType.QueryString; options: DeskFilterQueryStringOptions; }
  | { createdAt: Date; type: DeskFilterType.Label; options: DeskFilterLabelOptions; }
  | { createdAt: Date; type: DeskFilterType.Deadline; options: DeskFilterDeadlineOptions; }
  | { createdAt: Date; type: DeskFilterType.Participant; options: DeskFilterParticipantOptions; }
  | { createdAt: Date; type: DeskFilterType.Project; options: DeskFilterProjectOptions; }
;

export const deskFilterAvailableDeadlineVariant: Array<DeskFilterDeadlineVariant> = [
    DeskFilterDeadlineVariant.ExceedToday,
    DeskFilterDeadlineVariant.ExceedWeek,
    DeskFilterDeadlineVariant.Exceeded,
];
