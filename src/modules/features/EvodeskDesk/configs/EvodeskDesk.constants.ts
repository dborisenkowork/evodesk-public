import {DeskLabelColor, DeskLabelColorDescription} from '../../../app/EvodeskRESTApi/models/DeskLabel.model';
import {DeskCardAttachmentModel} from '../../../app/EvodeskRESTApi/models/DeskCardAttachment.model';

export const evodeskDeskMobileBreakpoint: number = 600;

export const evodeskDeskLayoutMobile: { to: number } = { to: evodeskDeskMobileBreakpoint };
export const evodeskDeskLayoutDesktop: { from: number } = { from: evodeskDeskMobileBreakpoint + 1 };

type MimeType = string;

export type DeskCardAttachmentType = 'image' | 'audio' | undefined;

export const evodeskDeskDefaultPalette: Array<DeskLabelColorDescription> = [
    { id: DeskLabelColor.Red, color: '#FB4A4A', translate: 'EvodeskDesk.configs.palette.Red' },
    { id: DeskLabelColor.Orange, color: '#FC9F31', translate: 'EvodeskDesk.configs.palette.Orange' },
    { id: DeskLabelColor.Yellow, color: '#F9CA21', translate: 'EvodeskDesk.configs.palette.Yellow' },
    { id: DeskLabelColor.Green, color: '#A9D26A', translate: 'EvodeskDesk.configs.palette.Green' },
    { id: DeskLabelColor.Cyan, color: '#6AD2D2', translate: 'EvodeskDesk.configs.palette.Cyan' },
    { id: DeskLabelColor.Blue, color: '#6BB6FB', translate: 'EvodeskDesk.configs.palette.Blue' },
    { id: DeskLabelColor.DeepBlue, color: '#6B82FB', translate: 'EvodeskDesk.configs.palette.DeepBlue' },
    { id: DeskLabelColor.Purple, color: '#A26BFB', translate: 'EvodeskDesk.configs.palette.Purple' },
    { id: DeskLabelColor.Pink, color: '#F777DB', translate: 'EvodeskDesk.configs.palette.Pink' },
    { id: DeskLabelColor.Gray, color: '#BABFCB', translate: 'EvodeskDesk.configs.palette.Gray' },
];

export const evodeskDeskAllowedImageAttachmentTypes: Array<MimeType> = [
    'image/jpeg',
    'image/png',
    'image/svg',
    'image/gif',
    'image/bmp',
];

export const evodeskDeskAllowedAudioAttachmentTypes: Array<MimeType> = [
    'audio/mp3',
];

export function guessDeskCardAttachmentType(input: DeskCardAttachmentModel): DeskCardAttachmentType {
    if (!!~evodeskDeskAllowedImageAttachmentTypes.indexOf(<any>input.type)) {
        return 'image';
    } else if (!!~evodeskDeskAllowedAudioAttachmentTypes.indexOf(input.type)) {
        return 'audio';
    } else {
        return undefined;
    }
}

export const evodeskTitleMaxLength: number = 256;
