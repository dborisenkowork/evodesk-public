import {Injectable} from '@angular/core';

import * as R from 'ramda';

import {EvodeskDeskFilter} from '../services/EvodeskDeskFilter.service';

import {DeskCardModel} from '../../../app/EvodeskRESTApi/models/DeskCard.model';

export interface EvodeskDeskCardLabelFilterOptions
{
    labelIds: Array<number>;
}

@Injectable()
export class EvodeskDeskCardLabelFilter implements EvodeskDeskFilter
{
    filter(input: Array<DeskCardModel>, options: EvodeskDeskCardLabelFilterOptions): Array<DeskCardModel> {
        return input.filter(c => {
            return R.intersection(options.labelIds, c.labels.map(l => l.id)).length > 0;
        });
    }
}
