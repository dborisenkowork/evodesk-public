import {Injectable} from '@angular/core';

import {DeskCardModel} from '../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskFilter} from '../services/EvodeskDeskFilter.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';

export interface QueryStringFilterOptions
{
    queryString: string;
}

@Injectable()
export class EvodeskDeskQueryStringFilter implements EvodeskDeskFilter
{
    constructor(
        private deskState: EvodeskDeskStateService,
    ) {}

    enable(withOptions: QueryStringFilterOptions): void {
        this.deskState.dispatch({
            type: EvodeskDeskAction.FiltersEnableQueryStringFilter,
            payload: {
                withOptions: withOptions,
            },
        });
    }

    disable(): void {
        this.deskState.dispatch({
            type: EvodeskDeskAction.FiltersDisableQueryStringFilter,
        });
    }

    setOptions(options: QueryStringFilterOptions): void {
        if (options.queryString.length) {
            this.enable(options);
        } else {
            this.disable();
        }
    }

    filter(input: Array<DeskCardModel>, options: QueryStringFilterOptions): Array<DeskCardModel> {
        return input.filter(c => {
            return !!~c.title.indexOf(options.queryString);
        });
    }
}
