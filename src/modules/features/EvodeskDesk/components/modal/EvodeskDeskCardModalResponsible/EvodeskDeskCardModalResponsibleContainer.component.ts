import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChange, SimpleChanges, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {v3Bottom349pxMatDialogConfig} from '../../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskParticipantModel} from '../../../../../app/EvodeskRESTApi/models/DeskParticipant.model';

import {EvodeskDeskCardModalResponsibleComponentProps} from './EvodeskDeskCardModalResponsible.component';
import {EvodeskDeskResponsibleContainerComponent as Modal} from '../../common/EvodeskDeskResponsible/EvodeskDeskResponsibleContainer.component';

import {EvodeskDeskCardResponsibleService} from '../../../services/EvodeskDeskCardResponsible.service';


interface State {
    ready: boolean;
    props?: EvodeskDeskCardModalResponsibleComponentProps;
}

@Component({
    selector: 'evodesk-desk-card-modal-responsible-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-card-modal-responsible [props]="state.props" (add)="add()" (remove)="remove($event)"></evodesk-desk-card-modal-responsible>
      </ng-container>
    `,
})
export class EvodeskDeskCardModalResponsibleContainerComponent implements OnChanges
{
    @Input() card: DeskCardModel;

    public state: State = {
        ready: false,
    };

    constructor(
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private service: EvodeskDeskCardResponsibleService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['card']) {
            if (this.card) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        card: this.card,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    add(): void {
        const closed$: Subject<void> = new Subject<void>();

        const dialogRef: MatDialogRef<Modal> = this.matDialog.open(Modal, {
            ...v3Bottom349pxMatDialogConfig,
            viewContainerRef: this.viewContainerRef,
            closeOnNavigation: true,
            disableClose: true,
        });

        dialogRef.componentInstance.card = this.card;
        dialogRef.componentInstance.ngOnChanges({ card: new SimpleChange(undefined, dialogRef.componentInstance.card, true) });

        dialogRef.componentInstance.closeEvent.pipe(takeUntil(closed$)).subscribe(() => {
            dialogRef.close();
        });

        dialogRef.afterClosed().pipe(takeUntil(closed$)).subscribe(() => {
            closed$.next(undefined);
        });

        dialogRef.backdropClick().pipe(takeUntil(closed$)).subscribe(() => {
            dialogRef.close();
        });
    }

    remove(input: DeskParticipantModel): void {
        this.service.remodeDeskParticipanAsResponsibleOfCard(this.card.id, input).subscribe();
    }
}
