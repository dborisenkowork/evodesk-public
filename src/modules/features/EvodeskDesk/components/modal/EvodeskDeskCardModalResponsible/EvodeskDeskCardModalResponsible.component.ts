import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskParticipantModel} from '../../../../../app/EvodeskRESTApi/models/DeskParticipant.model';

interface Props {
    card: DeskCardModel;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskDeskCardModalResponsibleComponentProps};

@Component({
    selector: 'evodesk-desk-card-modal-responsible',
    templateUrl: './EvodeskDeskCardModalResponsible.component.pug',
    styleUrls: ['./EvodeskDeskCardModalResponsible.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardModalResponsibleComponent implements OnChanges
{
    @Input() props: Props;

    @Output('add') addEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('remove') removeEvent: EventEmitter<DeskParticipantModel> = new EventEmitter<DeskParticipantModel>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    add(): void {
        this.addEvent.emit(undefined);
    }

    remove(input: DeskParticipantModel): void {
        this.removeEvent.emit(input);
    }
}
