import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';
import {MatMenu, MatMenuTrigger} from '@angular/material';

import {Subject} from 'rxjs';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskCardModalLabelComponentProps} from './EvodeskDeskCardModalLabel.component';

import {EvodeskDeskLabelService} from '../../../services/EvodeskDeskLabel.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskCardModalLabelComponentProps;
}

@Component({
    selector: 'evodesk-desk-modal-label-container',
    template: `
        <ng-container *ngIf="!!state && state.ready">
            <evodesk-desk-card-modal-label
              [props]="state.props"
              (delete)="delete($event)"
              (open)="openEvent.emit($event)"
            ></evodesk-desk-card-modal-label>
        </ng-container>
    `,
})
export class EvodeskDeskCardModalLabelContainerComponent implements OnChanges, OnDestroy
{
    @Input() card: DeskCardModel;
    @Input() labelMatMenu: MatMenu;

    @Output('open') openEvent: EventEmitter<MatMenuTrigger> = new EventEmitter<MatMenuTrigger>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: undefined,
    };

    constructor(
        private evodeskDeskLabelService: EvodeskDeskLabelService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['card']) {
            if (this.card) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        card: this.card,
                        matMenu: this.labelMatMenu,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    delete(label: DeskCardModel): void {
        this.evodeskDeskLabelService.excludeLabelFromCard({
            labelId: label.id,
            cardId: this.card.id,
            deskId: this.card.deskId,
        }).subscribe();
    }
}
