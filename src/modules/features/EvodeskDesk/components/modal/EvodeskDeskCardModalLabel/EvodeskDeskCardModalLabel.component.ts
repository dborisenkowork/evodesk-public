import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {MatMenu, MatMenuTrigger} from '@angular/material';

import {evodeskDeskDefaultPalette} from '../../../configs/EvodeskDesk.constants';

import {DeskLabelModel} from '../../../../../app/EvodeskRESTApi/models/DeskLabel.model';
import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

interface Props {
    card: DeskCardModel;
    matMenu?: MatMenu;
}

interface State {
    ready: boolean;
    labels?: Array<DeskLabelModel>;
}

export {Props as EvodeskDeskCardModalLabelComponentProps};

@Component({
    selector: 'evodesk-desk-card-modal-label',
    templateUrl: './EvodeskDeskCardModalLabel.component.pug',
    styleUrls: [
        './EvodeskDeskCardModalLabel.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardModalLabelComponent implements OnChanges
{
    @Input() props: Props;

    @Output('open') openEvent: EventEmitter<MatMenuTrigger> = new EventEmitter<MatMenuTrigger>();
    @Output('delete') deleteEvent: EventEmitter<DeskLabelModel> = new EventEmitter<DeskLabelModel>();

    @ViewChild('matMenuTrigger', { read: MatMenuTrigger }) matMenuTrigger: MatMenuTrigger;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ready: true,
                labels: (() => {
                    const labels: Array<DeskLabelModel> = [...this.props.card.labels];

                    labels.sort((a, b) => a.id - b.id);

                    return labels;
                })(),
            };
        } else {
            this.state = {
                ready: false,
            };
        }
    }

    open(): void {
        this.openEvent.emit(this.matMenuTrigger);
    }

    trackByLabelId(index: number, item: DeskLabelModel): any {
        return item.id;
    }

    isPresetColor(input: DeskLabelModel): boolean {
        return ! input.name && !!~evodeskDeskDefaultPalette.map(c => c.color).indexOf(input.color);
    }

    presetColorName(input: DeskLabelModel): string {
        return evodeskDeskDefaultPalette.filter(c => c.color === input.color)[0].translate;
    }

    labelPinNgStyles(label: DeskLabelModel): any {
        return {
            'background-color': label.color,
        };
    }

    delete(label: DeskLabelModel): void {
        this.deleteEvent.emit(label);
    }
}
