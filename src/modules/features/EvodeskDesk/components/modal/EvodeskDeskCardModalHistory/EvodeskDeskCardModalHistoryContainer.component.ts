import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskCardCommentModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardComment.model';

import {EvodeskDeskCardModalHistoryComponentProps} from './EvodeskDeskCardModalHistory.component';
import {EvodeskDeskHistoryRecordCommentComponentDeleteEvent, EvodeskDeskHistoryRecordCommentComponentUpdateEvent} from '../../common/EvodeskDeskHistoryRecordComment/EvodeskDeskHistoryRecordComment.component';

import {EvodeskDeskCommentService} from '../../../services/EvodeskDeskComment.service';
import {EvodeskDeskFetchCardService} from '../../../services/EvodeskDeskFetchCard.service';
import {CurrentUserService} from '../../../../EvodeskAuth/services/CurrentUser.service';
import {AuthTokenService} from '../../../../EvodeskAuth/services/AuthToken.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskCardModalHistoryComponentProps;
}

@Component({
    selector: 'evodesk-desk-modal-history-container',
    template: `
        <ng-container *ngIf="!!state && state.ready">
            <evodesk-desk-card-modal-history
              [props]="state.props"
              (deleteComment)="deleteComment($event)"
              (updateComment)="updateComment($event)"
            ></evodesk-desk-card-modal-history>
        </ng-container>
    `,
})
export class EvodeskDeskCardModalHistoryContainerComponent implements OnChanges, OnDestroy
{
    @Input() card: DeskCardModel;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: undefined,
    };

    constructor(
        private fetchService: EvodeskDeskFetchCardService,
        private commentService: EvodeskDeskCommentService,
        private currentUserService: CurrentUserService,
        private authTokenService: AuthTokenService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['card']) {
            if (this.card) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        card: this.card,
                        records: this.card.actions,
                        currentProfileId: this.currentUserService.impersonatedAs.id,
                        authToken: this.authTokenService.token.access_token,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    deleteComment($event: EvodeskDeskHistoryRecordCommentComponentDeleteEvent): void {
        this.commentService.deleteComment({
            cardId: this.card.id,
            deskId: this.card.deskId,
            commentId: $event.id,
        }).subscribe(() => {
            this.fetchService.fetchCard(this.card.deskId, this.card.id).subscribe();
        });
    }

    updateComment($event: EvodeskDeskHistoryRecordCommentComponentUpdateEvent): void {
        const origComment: DeskCardCommentModel = this.card.comments.filter(c => c.id === $event.id)[0];

        this.commentService.updateComment({
            cardId: this.card.id,
            deskId: this.card.deskId,
            updatedComment: {
                ...origComment,
                text: $event.formValue.body,
            },
        }).subscribe(() => {
            this.fetchService.fetchCard(this.card.deskId, this.card.id).subscribe();
        });
    }
}
