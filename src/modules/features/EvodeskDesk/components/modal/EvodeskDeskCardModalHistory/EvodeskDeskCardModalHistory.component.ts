import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {ProfileId} from '../../../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskCardLogRecordModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardLogRecord.model';

import {EvodeskDeskHistoryRecordActionComponentProps} from '../../common/EvodeskDeskHistoryRecordAction/EvodeskDeskHistoryRecordAction.component';
import {EvodeskDeskHistoryRecordCommentComponentDeleteEvent, EvodeskDeskHistoryRecordCommentComponentProps, EvodeskDeskHistoryRecordCommentComponentUpdateEvent} from '../../common/EvodeskDeskHistoryRecordComment/EvodeskDeskHistoryRecordComment.component';

interface Props {
    card: DeskCardModel;
    records: Array<DeskCardLogRecordModel>;
    currentProfileId: ProfileId;
    authToken: string;
}

interface State {
    ready: boolean;
    filter: Filter;
    filtered: Array<DeskCardLogRecordModel>;
    limit: number;
}

type Filter = undefined | 'actions' | 'comments';
type RecordType = 'action' | 'comment';

export {Props as EvodeskDeskCardModalHistoryComponentProps};

@Component({
    selector: 'evodesk-desk-card-modal-history',
    templateUrl: './EvodeskDeskCardModalHistory.component.pug',
    styleUrls: [
        './EvodeskDeskCardModalHistory.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardModalHistoryComponent implements OnChanges
{
    @Input() props: Props;
    @Input() pageSize: number = 15;

    @Output('deleteComment') deleteCommentEvent: EventEmitter<EvodeskDeskHistoryRecordCommentComponentDeleteEvent> = new EventEmitter<EvodeskDeskHistoryRecordCommentComponentDeleteEvent>();
    @Output('updateComment') updateCommentEvent: EventEmitter<EvodeskDeskHistoryRecordCommentComponentUpdateEvent> = new EventEmitter<EvodeskDeskHistoryRecordCommentComponentUpdateEvent>();

    public state: State = {
        ready: false,
        filter: undefined,
        filtered: [],
        limit: this.pageSize,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };

            this.update();
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    setFilter(filter: Filter): void {
        this.state = {
            ...this.state,
            filter: filter,
        };

        this.update();
    }

    update(): void {
        const reversed: Array<DeskCardLogRecordModel> = [...this.props.records].reverse();

        if (this.state.filter === undefined) {
            this.state = {
                ...this.state,
                filtered: reversed,
            };
        } else {
            switch (this.state.filter) {
                default: {
                    throw new Error(`Unknown filter "${this.state.filter}"`);
                }

                case 'actions': {
                    this.state = { ...this.state, filtered: reversed.filter(r => this.recordType(r) === 'action') };

                    break;
                }

                case 'comments': {
                    this.state = { ...this.state, filtered: reversed.filter(r => this.recordType(r) === 'comment') };

                    break;
                }
            }
        }
    }

    nextPage(): void {
        this.state = {
            ...this.state,
            limit: this.state.limit + this.pageSize,
        };

        this.update();
    }

    get hasMore(): boolean {
        return this.state.limit < this.state.filtered.length;
    }

    get current(): number {
        return this.state.limit;
    }

    get total(): number {
        return this.state.filtered.length;
    }

    recordType(r): RecordType {
        if (r.entity === 'comment' && r.action !== 'delete') {
            return 'comment';
        } else {
            return 'action';
        }
    }

    get records(): Array<DeskCardLogRecordModel> {
        return this.state.filtered.slice(0, this.state.limit);
    }

    get hasRecords(): boolean {
        return this.records.length > 0;
    }

    trackByRecordId(index: number, record: DeskCardLogRecordModel): any {
        return record.id;
    }

    recordActionProps(r: DeskCardLogRecordModel): EvodeskDeskHistoryRecordActionComponentProps {
        return {
            logRecord: r,
            authToken: this.props.authToken,
            card: this.props.card,
        };
    }

    recordCommentProps(r: DeskCardLogRecordModel): EvodeskDeskHistoryRecordCommentComponentProps {
        return {
            logRecord: r,
            card: this.props.card,
            canEdit: r.user.id === this.props.currentProfileId,
            canDelete: r.user.id === this.props.currentProfileId,
        };
    }
}
