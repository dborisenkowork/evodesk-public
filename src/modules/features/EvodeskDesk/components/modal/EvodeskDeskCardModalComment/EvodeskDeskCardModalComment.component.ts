import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

interface Props {
    card: DeskCardModel;
    username: string;
}

interface State {
    ready: boolean;
    form: FormGroup;
    submitting: boolean;
}

interface FormValue {
    body: string;
}

export interface CreateCommentEvent {
    successCallback: Function;
    errorCallback: Function;
    formValue: FormValue;
}

export {Props as EvodeskDeskCardModalCommentComponentProps};
export {FormValue as EvodeskDeskCardModalCommentComponentFormValue};
export {CreateCommentEvent as EvodeskDeskCardModalCommentComponentCreateCommentEvent};

@Component({
    selector: 'evodesk-desk-card-modal-comment',
    templateUrl: './EvodeskDeskCardModalComment.component.pug',
    styleUrls: [
        './EvodeskDeskCardModalComment.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardModalCommentComponent implements OnChanges
{
    @Input() props: Props;

    @Output('createComment') createCommentEvent: EventEmitter<CreateCommentEvent> = new EventEmitter<CreateCommentEvent>();

    @ViewChild('body') bodyRef: ElementRef;

    public state: State = {
        ready: false,
        form: this.fb.group({
            body: '',
        }),
        submitting: false,
    };

    constructor(
        private fb: FormBuilder,
        private cdr: ChangeDetectorRef,
        private ngZone: NgZone,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    submit(): void {
        if (! this.state.submitting && this.state.form.valid) {
            this.state = {
                ...this.state,
                submitting: true,
            };

            this.state.form.disable();

            this.createCommentEvent.emit({
                formValue: this.formValue,
                errorCallback: () => {
                    this.state = {
                        ...this.state,
                        submitting: false,
                    };

                    this.state.form.enable();

                    this.ngZone.runOutsideAngular(() => {
                        setTimeout(() => {
                            (this.bodyRef.nativeElement as HTMLElement).focus();
                        });
                    });

                    this.cdr.detectChanges();
                },
                successCallback: () => {
                    this.state = {
                        ...this.state,
                        submitting: false,
                    };

                    this.state.form.reset();
                    this.state.form.enable();

                    this.cdr.detectChanges();
                },
            });
        }
    }

    cancel(): void {
        this.state.form.reset();

        (this.bodyRef.nativeElement as HTMLElement).blur();
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    get hasBody(): boolean {
        return !! this.formValue.body;
    }
}
