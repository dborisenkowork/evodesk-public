import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {proxy} from '../../../../../../functions/proxy.function';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskCardModalCommentComponentCreateCommentEvent as CreateCommentEvent, EvodeskDeskCardModalCommentComponentProps} from './EvodeskDeskCardModalComment.component';

import {EvodeskDeskCommentService} from '../../../services/EvodeskDeskComment.service';
import {CurrentUserService} from '../../../../EvodeskAuth/services/CurrentUser.service';
import {EvodeskDeskFetchCardService} from '../../../services/EvodeskDeskFetchCard.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskCardModalCommentComponentProps;
}

@Component({
    selector: 'evodesk-desk-modal-comment-container',
    template: `
        <ng-container *ngIf="!!state && state.ready">
            <evodesk-desk-card-modal-comment
              [props]="state.props"
              (createComment)="createComment($event)"
            ></evodesk-desk-card-modal-comment>
        </ng-container>
    `,
})
export class EvodeskDeskCardModalCommentContainerComponent implements OnChanges, OnDestroy
{
    @Input() card: DeskCardModel;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: undefined,
    };

    constructor(
        private fetch: EvodeskDeskFetchCardService,
        private service: EvodeskDeskCommentService,
        private currentUser: CurrentUserService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['card']) {
            if (this.card) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        card: this.card,
                        username: this.currentUser.impersonatedAs.name || this.currentUser.impersonatedAs.email,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    createComment($event: CreateCommentEvent): void {
        proxy(this.service.addComment({
            cardId: this.card.id,
            deskId: this.card.deskId,
            text: $event.formValue.body,
        })).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
            () => {
                this.fetch.fetchCard(this.card.deskId, this.card.id).subscribe(
                    () => {
                        this.fetch.fetchCard(this.card.deskId, this.card.id).subscribe(
                            () => {
                                $event.successCallback();
                            },
                            () => {
                                $event.errorCallback();
                            },
                        );
                    },
                    () => {
                        $event.errorCallback();
                    },
                );
            },
            () => {
                $event.errorCallback();
            },
        );
    }
}
