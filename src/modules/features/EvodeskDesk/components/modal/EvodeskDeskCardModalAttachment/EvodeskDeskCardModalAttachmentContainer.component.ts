import {Component, Input, OnChanges, OnDestroy, SimpleChanges, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {v3Bottom317pxMatDialogConfig, v3Bottom600pxMatDialogConfig} from '../../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {EvodeskDeskCardModalAttachmentComponentProps} from './EvodeskDeskCardModalAttachment.component';
import {EvodeskDeskAttachImageContainerComponentProps} from '../../common/EvodeskDeskAttachImage/EvodeskDeskAttachImageContainer.component';
import {EvodeskDeskAttachAudioContainerComponentProps} from '../../common/EvodeskDeskAttachAudio/EvodeskDeskAttachAudioContainer.component';
import {EvodeskDeskAddAttachmentComponentProps} from '../../common/EvodeskDeskAddAttachment/EvodeskDeskAddAttachment.component';

import {AuthTokenService} from '../../../../EvodeskAuth/services/AuthToken.service';
import {DeskCardAttachmentModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardAttachment.model';
import {EvodeskDeskAttachmentService} from '../../../services/EvodeskDeskAttachment.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskCardModalAttachmentComponentProps;
    matDialogRef?: MatDialogRef<any>;
}

@Component({
    selector: 'evodesk-desk-modal-attachment-container',
    template: `
        <ng-container *ngIf="!!state && state.ready">
            <evodesk-desk-card-modal-attachment
              [props]="state.props"
              (addAttachment)="openAddAttachment()"
              (deleteAttachment)="deleteAttachment($event)"
            ></evodesk-desk-card-modal-attachment>
        </ng-container>

        <ng-template #addAttachmentMatDialog>
          <evodesk-desk-add-attachment [props]="attachProps" (close)="state.matDialogRef.close()" (requestImage)="openAttachImageMatDialog()" (requestAudio)="openAttachAudioMatDialog()"></evodesk-desk-add-attachment>
        </ng-template>

        <ng-template #attachImageMatDialog>
          <evodesk-desk-attach-image-container [props]="attachImageProps" (close)="state.matDialogRef.close()"></evodesk-desk-attach-image-container>
        </ng-template>

        <ng-template #attachAudioMatDialog>
          <evodesk-desk-attach-audio-container [props]="attachAudioProps" (close)="state.matDialogRef.close()"></evodesk-desk-attach-audio-container>
        </ng-template>
    `,
})
export class EvodeskDeskCardModalAttachmentContainerComponent implements OnChanges, OnDestroy
{
    @Input() card: DeskCardModel;

    @ViewChild('addAttachmentMatDialog') addAttachmentMatDialog: TemplateRef<any>;
    @ViewChild('attachImageMatDialog') attachImageMatDialog: TemplateRef<any>;
    @ViewChild('attachAudioMatDialog') attachAudioMatDialog: TemplateRef<any>;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: undefined,
    };

    constructor(
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private authToken: AuthTokenService,
        private attachmentService: EvodeskDeskAttachmentService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['card']) {
            if (this.card) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        card: this.card,
                        accessToken: this.authToken.token.access_token,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    deleteAttachment(a: DeskCardAttachmentModel): void {
        this.attachmentService.askForDeleteAttachment(this.card.deskId, this.card.id, a.id).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }

    openAddAttachment(): void {
        this.state = {
            ...this.state,
            matDialogRef: this.matDialog.open(this.addAttachmentMatDialog, {
                ...v3Bottom317pxMatDialogConfig,
                autoFocus: false,
                viewContainerRef: this.viewContainerRef,
                closeOnNavigation: true,
            }),
        };
    }

    openAttachImageMatDialog(): void {
        setTimeout(() => {
            this.state = {
                ...this.state,
                matDialogRef: this.matDialog.open(this.attachImageMatDialog, {
                    ...v3Bottom600pxMatDialogConfig,
                    autoFocus: false,
                    viewContainerRef: this.viewContainerRef,
                    closeOnNavigation: true,
                }),
            };
        });
    }

    openAttachAudioMatDialog(): void {
        setTimeout(() => {
            this.state = {
                ...this.state,
                matDialogRef: this.matDialog.open(this.attachAudioMatDialog, {
                    ...v3Bottom600pxMatDialogConfig,
                    autoFocus: false,
                    viewContainerRef: this.viewContainerRef,
                    closeOnNavigation: true,
                }),
            };
        });
    }

    get attachProps(): EvodeskDeskAddAttachmentComponentProps {
        return {
            card: this.state.props.card,
            variant: 'modal',
        };
    }

    get attachImageProps(): EvodeskDeskAttachImageContainerComponentProps {
        return {
            card: this.state.props.card,
            variant: 'modal',
        };
    }

    get attachAudioProps(): EvodeskDeskAttachAudioContainerComponentProps {
        return {
            card: this.state.props.card,
            variant: 'modal',
        };
    }
}
