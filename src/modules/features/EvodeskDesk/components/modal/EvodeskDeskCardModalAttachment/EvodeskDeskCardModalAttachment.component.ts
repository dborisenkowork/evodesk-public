import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskCardAttachmentModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardAttachment.model';

import {EvodeskDeskCardModalAttachmentImageComponentProps} from '../EvodeskDeskCardModalAttachmentImage/EvodeskDeskCardModalAttachmentImage.component';
import {EvodeskDeskCardModalAttachmentAudioComponentProps} from '../EvodeskDeskCardModalAttachmentAudio/EvodeskDeskCardModalAttachmentAudio.component';

interface Props {
    card: DeskCardModel;
    accessToken: string;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskDeskCardModalAttachmentComponentProps};

@Component({
    selector: 'evodesk-desk-card-modal-attachment',
    templateUrl: './EvodeskDeskCardModalAttachment.component.pug',
    styleUrls: [
        './EvodeskDeskCardModalAttachment.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardModalAttachmentComponent implements OnChanges
{
    @Input() props: Props;

    @Output('addAttachment') addAttachmentEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('deleteAttachment') deleteAttachmentEvent: EventEmitter<DeskCardAttachmentModel> = new EventEmitter<DeskCardAttachmentModel>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ready: true,
            };
        } else {
            this.state = {
                ready: false,
            };
        }
    }

    trackByAttachmentId(index: number, item: DeskCardAttachmentModel): any {
        return item.id;
    }

    isImageAttachment(a: DeskCardAttachmentModel): boolean {
        return true;
    }

    isAudioAttachment(a: DeskCardAttachmentModel): boolean {
        return false;
    }

    addAttachment(): void {
        this.addAttachmentEvent.emit(undefined);
    }

    deleteAttachment(a: DeskCardAttachmentModel): void {
        this.deleteAttachmentEvent.emit(a);
    }

    imageProps(a: DeskCardAttachmentModel): EvodeskDeskCardModalAttachmentImageComponentProps {
        return {
            attachment: a,
            card: this.props.card,
            accessToken: this.props.accessToken,
        };
    }

    audioProps(a: DeskCardAttachmentModel): EvodeskDeskCardModalAttachmentAudioComponentProps {
        return {
            attachment: a,
        };
    }
}
