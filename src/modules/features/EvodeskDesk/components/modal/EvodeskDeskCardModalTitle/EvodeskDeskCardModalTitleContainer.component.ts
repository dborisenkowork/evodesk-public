import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskCardTitleComponentProps} from './EvodeskDeskCardModalTitle.component';

import {EvodeskDeskUpdateCardService} from '../../../services/EvodeskDeskUpdateCard.service';
import {DeskUpdateCardChangedField} from '../../../../../app/EvodeskRESTApi/services/DeskREST.service';

interface State {
    ready: boolean;
    form: FormGroup;
    props?: EvodeskDeskCardTitleComponentProps;
}

@Component({
    selector: 'evodesk-desk-modal-title-container',
    template: `
        <ng-container *ngIf="!!state && state.ready">
            <evodesk-desk-card-modal-title
              [props]="state.props"
              [form]="state.form"
              (onSubmit)="onSubmit()"
            ></evodesk-desk-card-modal-title>
        </ng-container>
    `,
})
export class EvodeskDeskCardModalTitleContainerComponent implements OnChanges, OnDestroy
{
    @Input() card: DeskCardModel;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: undefined,
        form: this.fb.group({
            title: [null],
        }),
    };

    constructor(
        private fb: FormBuilder,
        private service: EvodeskDeskUpdateCardService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['card']) {
            if (this.card) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        card: this.card,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onSubmit(): void {
        if (this.state.form.valid) {
            this.service.updateCard(DeskUpdateCardChangedField.Title, {...this.card, title: this.state.form.value.title }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
        }
    }
}
