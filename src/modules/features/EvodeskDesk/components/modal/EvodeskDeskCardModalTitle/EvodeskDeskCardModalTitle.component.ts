import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

export interface EvodeskDeskCardTitleComponentProps {
    card: DeskCardModel;
}

interface State {
    ready: boolean;
    isFocused: boolean;
}

@Component({
    selector: 'evodesk-desk-card-modal-title',
    templateUrl: './EvodeskDeskCardModalTitle.component.pug',
    styleUrls: [
        './EvodeskDeskCardModalTitle.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardModalTitleComponent implements OnInit, OnChanges
{
    @Input() props: EvodeskDeskCardTitleComponentProps;
    @Input() form: FormGroup;

    @Output('onSubmit') onSubmitEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('titleInput') titleInputRef: ElementRef;

    public state: State = {
        ready: false,
        isFocused: false,
    };

    ngOnInit(): void {
        this.form.setValue({
            title: this.props.card.title,
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props && this.form) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngSubmit(): void {
        this.onSubmitEvent.emit(undefined);

        (this.titleInputRef.nativeElement as HTMLElement).blur();
    }

    focus(): void {
        this.state = {
            ...this.state,
            isFocused: true,
        };
    }

    blur(): void {
        this.state = {
            ...this.state,
            isFocused: false,
        };
    }

    revertAndBack(): void {
        (this.titleInputRef.nativeElement as HTMLElement).blur();

        this.form.setValue({
            title: this.props.card.title,
        });
    }
}
