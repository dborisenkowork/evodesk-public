import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {DeskCardAttachmentModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardAttachment.model';

interface Props {
    attachment: DeskCardAttachmentModel;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskDeskCardModalAttachmentAudioComponentProps};

@Component({
    selector: 'evodesk-desk-card-modal-attachment-audio',
    templateUrl: './EvodeskDeskCardModalAttachmentAudio.component.pug',
    styleUrls: [
        './EvodeskDeskCardModalAttachmentAudio.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardModalAttachmentAudioComponent implements OnChanges
{
    @Input() props: Props;

    @Output('delete') deleteEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }
}
