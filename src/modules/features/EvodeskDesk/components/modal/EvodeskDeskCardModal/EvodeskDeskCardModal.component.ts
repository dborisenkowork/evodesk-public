import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {MatMenu, MatMenuTrigger} from '@angular/material';

import {DeskCardChecklistModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardChecklist.model';
import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

interface Props {
    card: DeskCardModel;
    deadlineMatMenu?: MatMenu;
    labelMatMenu?: MatMenu;
    isProjectsFeatureEnabled: boolean;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskDeskCardModalComponentProps};

@Component({
    selector: 'evodesk-desk-card-modal',
    templateUrl: './EvodeskDeskCardModal.component.pug',
    styleUrls: [
        './EvodeskDeskCardModal.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardModalComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('archive') archiveEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('delete') deleteEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openLabel') openLabelEvent: EventEmitter<MatMenuTrigger> = new EventEmitter<MatMenuTrigger>();
    @Output('openDeadline') openDeadlineEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openAddChecklist') openAddChecklistEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openCopyLink') openCopyLinkEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openMove') openMoveEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openAddAttachment') openAddAttachmentEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('sLabelMatMenuTrigger', { read: MatMenuTrigger }) labelMatMenuTrigger: MatMenuTrigger;
    @ViewChild('deadlineMatMenuTrigger', { read: MatMenuTrigger }) deadlineMatMenuTrigger: MatMenuTrigger;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    trackByChecklistId(index: number, checklist: DeskCardChecklistModel): any {
        return checklist.id;
    }
}
