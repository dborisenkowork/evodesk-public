import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef, MatMenu, MatMenuTrigger} from '@angular/material';
import {BreakpointObserver} from '@angular/cdk/layout';
import {ActivatedRoute, Router} from '@angular/router';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {evodeskDeskMobileBreakpoint} from '../../../configs/EvodeskDesk.constants';

import {v3Bottom294pxMatDialogConfig, v3Bottom317pxMatDialogConfig, v3Bottom600pxMatDialogConfig} from '../../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {EvodeskDeskCardModalComponent, EvodeskDeskCardModalComponentProps} from './EvodeskDeskCardModal.component';

import {EvodeskDeskDeadlineComponentProps} from '../../common/EvodeskDeskDeadline/EvodeskDeskDeadline.component';
import {EvodeskDeskLabelComponent} from '../../common/EvodeskDeskLabel/EvodeskDeskLabel.component';

import {AppEvent} from '../../../../../app/EvodeskAppMessageBus/models/app-events.model';
import {DeskCardId} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskAppMessageBusService} from '../../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {EvodeskDeskDeleteCardsService} from '../../../services/EvodeskDeskDeleteCards.service';
import {EvodeskDeskArchiveCardsService} from '../../../services/EvodeskDeskArchiveCards.service';

const breakpointMobile: number = evodeskDeskMobileBreakpoint;

interface State {
    ready: boolean;
    props?: EvodeskDeskCardModalComponentProps;
    isMobile: boolean;
    matDialogRef?: MatDialogRef<any>;
}

const useMatMenusForRightMenu: boolean = false;

@Component({
    selector: 'evodesk-desk-card-modal-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <mat-menu #labelMatMenu class="__evodesk-desk-modal">
        <evodesk-desk-label-container [card]="this.state.props.card" [variant]="'mat-menu'" (close)="view.labelMatMenuTrigger.closeMenu()"></evodesk-desk-label-container>
      </mat-menu>

      <ng-template #labelMatDialog>
        <evodesk-desk-label-container [card]="this.state.props.card" [variant]="'modal'" (close)="state.matDialogRef.close()"></evodesk-desk-label-container>
      </ng-template>

      <mat-menu #deadlineMatMenu class="__evodesk-desk-modal">
        <evodesk-desk-deadline-container [props]="commonMatMenuProps" (close)="view.deadlineMatMenuTrigger.closeMenu()"></evodesk-desk-deadline-container>
      </mat-menu>

      <ng-template #deadlineMatDialog>
        <evodesk-desk-deadline-container [props]="commonMatModalProps" (close)="state.matDialogRef.close()"></evodesk-desk-deadline-container>
      </ng-template>

      <ng-template #addChecklistMatDialog>
        <evodesk-desk-add-checklist-container [props]="commonMatModalProps" (close)="state.matDialogRef.close()"></evodesk-desk-add-checklist-container>
      </ng-template>

      <ng-template #copyLinkMatDialog>
        <evodesk-desk-copy-link-container [props]="commonMatModalProps" (close)="state.matDialogRef.close()"></evodesk-desk-copy-link-container>
      </ng-template>

      <ng-template #moveMatDialog>
        <evodesk-desk-move-container [props]="commonMatModalProps" (close)="state.matDialogRef.close()"></evodesk-desk-move-container>
      </ng-template>

      <ng-template #addAttachmentMatDialog>
        <evodesk-desk-add-attachment [props]="commonMatModalProps" (close)="state.matDialogRef.close()" (requestImage)="openAttachImageMatDialog()" (requestAudio)="openAttachAudioMatDialog()"></evodesk-desk-add-attachment>
      </ng-template>

      <ng-template #attachImageMatDialog>
        <evodesk-desk-attach-image-container [props]="commonMatModalProps" (close)="state.matDialogRef.close()"></evodesk-desk-attach-image-container>
      </ng-template>

      <ng-template #attachAudioMatDialog>
        <evodesk-desk-attach-audio-container [props]="commonMatModalProps" (close)="state.matDialogRef.close()"></evodesk-desk-attach-audio-container>
      </ng-template>

      <ng-container *ngIf="state.ready">
        <evodesk-desk-card-modal
          #view [props]="state.props" (close)="close()"
          (archive)="archive()"
          (delete)="delete()"
          (openLabel)="openLabel($event)"
          (openDeadline)="openDeadline()"
          (openAddChecklist)="openAddChecklist()"
          (openCopyLink)="openCopyLink()"
          (openMove)="openMove()"
          (openAddAttachment)="openAddAttachment()"
        ></evodesk-desk-card-modal>
      </ng-container>
    `,
})
export class EvodeskDeskCardModalContainerComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit
{
    @Input() cardId: DeskCardId;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('view') view: EvodeskDeskCardModalComponent;
    @ViewChild('labelMatMenu', { read: MatMenu }) labelMatMenu: MatMenu;
    @ViewChild('labelMatDialog') labelMatDialogRef: TemplateRef<EvodeskDeskLabelComponent>;
    @ViewChild('deadlineMatMenu', { read: MatMenu }) deadlineMatMenu: MatMenu;
    @ViewChild('deadlineMatDialog') deadlineMatDialogRef: TemplateRef<any>;
    @ViewChild('addChecklistMatDialog') addChecklistMatDialog: TemplateRef<any>;
    @ViewChild('copyLinkMatDialog') copyLinkMatDialog: TemplateRef<any>;
    @ViewChild('moveMatDialog') moveMatDialog: TemplateRef<any>;
    @ViewChild('addAttachmentMatDialog') addAttachmentMatDialog: TemplateRef<any>;
    @ViewChild('attachImageMatDialog') attachImageMatDialog: TemplateRef<any>;
    @ViewChild('attachAudioMatDialog') attachAudioMatDialog: TemplateRef<any>;

    private nextCard$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        isMobile: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private matDialog: MatDialog,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private viewContainerRef: ViewContainerRef,
        private breakpointObserver: BreakpointObserver,
        private appBus: EvodeskAppMessageBusService,
        private deskState: EvodeskDeskStateService,
        private archiveCardService: EvodeskDeskArchiveCardsService,
        private deleteCardService: EvodeskDeskDeleteCardsService,
    ) {}

    ngOnInit(): void {
        const notifyStateAboutOpenedCard: Function = () => {
            this.deskState.dispatch({
                type: EvodeskDeskAction.OpenCard,
                payload: {
                    cardId: this.cardId,
                },
            });
        };

        const subscribeToDeskCardModel: Function = () => {
            this.deskState.current$.pipe(
                takeUntil(this.ngOnDestroy$),
                map(s => s.current.sourceCards.filter(sc => sc.id === this.cardId)[0]),
                distinctUntilChanged(),
            ).subscribe((card) => {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        card: card,
                        labelMatMenu: this.labelMatMenu,
                        deadlineMatMenu: this.deadlineMatMenu,
                        isProjectsFeatureEnabled: this.deskState.snapshot.columns.filter(e => e.entities === 'projects').length > 0,
                    },
                };

                this.cdr.detectChanges();
            });
        };

        const closeModalOnDeskForceReload: Function = () => {
            this.appBus.stream$.pipe(
                takeUntil(this.ngOnDestroy$),
                filter(e => !!~[AppEvent.DeskForceReload].indexOf(e.type)),
            ).subscribe(() => {
                this.close();
            });
        };

        const subscribeToBreakpointObserver: Function = () => {
            this.breakpointObserver.observe(`(max-width: ${breakpointMobile}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
                this.state = {
                    ...this.state,
                    isMobile: true,
                };
            });

            this.breakpointObserver.observe(`(min-width: ${breakpointMobile + 1}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
                this.state = {
                    ...this.state,
                    isMobile: false,
                };
            });
        };

        const setUrl: Function = () => {
            this.router.navigate(['.'], { relativeTo: this.activatedRoute, queryParams: { card: this.cardId } });
        };

        subscribeToBreakpointObserver();
        subscribeToDeskCardModel();
        closeModalOnDeskForceReload();
        notifyStateAboutOpenedCard();
        setUrl();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.nextCard$.next(undefined);
    }

    ngOnDestroy(): void {
        const notifyStateAboutClosingCard: Function = () => {
            this.deskState.dispatch({
                type: EvodeskDeskAction.CloseCard,
                payload: {
                    cardId: this.cardId,
                },
            });
        };

        notifyStateAboutClosingCard();

        this.nextCard$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    ngAfterViewInit(): void {
        this.cdr.detectChanges();
    }

    archive(): void {
        this.archiveCardService.askForArchiveSingleCard(this.state.props.card.deskId, this.cardId)
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe((isConfirmed) => {
                if (isConfirmed) {
                    this.close();
                }
            });
    }

    delete(): void {
        this.deleteCardService.askForDeleteSingleCard(this.state.props.card.deskId, this.cardId)
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe((isConfirmed) => {
                if (isConfirmed) {
                    this.close();
                }
            });
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    openLabel($event: MatMenuTrigger): void {
        if (this.state.isMobile || ! useMatMenusForRightMenu) {
            this.state = {
                ...this.state,
                matDialogRef: this.matDialog.open(this.labelMatDialogRef, {
                    ...v3Bottom317pxMatDialogConfig,
                    autoFocus: false,
                    viewContainerRef: this.viewContainerRef,
                    closeOnNavigation: true,
                    disableClose: true,
                }),
            };

            this.state.matDialogRef.backdropClick().pipe(takeUntil(this.state.matDialogRef.afterClosed())).subscribe(() => {
                this.state.matDialogRef.close();
            });
        } else {
            $event.openMenu();
        }
    }

    openDeadline(): void {
        if (this.state.isMobile || ! useMatMenusForRightMenu) {
            this.state = {
                ...this.state,
                matDialogRef: this.matDialog.open(this.deadlineMatDialogRef, {
                    ...v3Bottom317pxMatDialogConfig,
                    autoFocus: false,
                    viewContainerRef: this.viewContainerRef,
                    closeOnNavigation: true,
                    disableClose: true,
                }),
            };

            this.state.matDialogRef.backdropClick().pipe(takeUntil(this.state.matDialogRef.afterClosed())).subscribe(() => {
                this.state.matDialogRef.close();
            });
        } else {
            this.view.deadlineMatMenuTrigger.openMenu();
        }
    }

    openAddChecklist(): void {
        this.state = {
            ...this.state,
            matDialogRef: this.matDialog.open(this.addChecklistMatDialog, {
                ...v3Bottom294pxMatDialogConfig,
                autoFocus: false,
                viewContainerRef: this.viewContainerRef,
                closeOnNavigation: true,
                disableClose: true,
            }),
        };

        this.state.matDialogRef.backdropClick().pipe(takeUntil(this.state.matDialogRef.afterClosed())).subscribe(() => {
            this.state.matDialogRef.close();
        });
    }

    openCopyLink(): void {
        this.state = {
            ...this.state,
            matDialogRef: this.matDialog.open(this.copyLinkMatDialog, {
                ...v3Bottom317pxMatDialogConfig,
                autoFocus: false,
                viewContainerRef: this.viewContainerRef,
                closeOnNavigation: true,
                disableClose: true,
            }),
        };

        this.state.matDialogRef.backdropClick().pipe(takeUntil(this.state.matDialogRef.afterClosed())).subscribe(() => {
            this.state.matDialogRef.close();
        });
    }

    openMove(): void {
        this.state = {
            ...this.state,
            matDialogRef: this.matDialog.open(this.moveMatDialog, {
                ...v3Bottom317pxMatDialogConfig,
                autoFocus: false,
                viewContainerRef: this.viewContainerRef,
                closeOnNavigation: true,
                disableClose: true,
            }),
        };

        this.state.matDialogRef.backdropClick().pipe(takeUntil(this.state.matDialogRef.afterClosed())).subscribe(() => {
            this.state.matDialogRef.close();
        });
    }

    openAddAttachment(): void {
        this.openAttachImageMatDialog();
    }

    openAttachImageMatDialog(): void {
        setTimeout(() => {
            this.state = {
                ...this.state,
                matDialogRef: this.matDialog.open(this.attachImageMatDialog, {
                    ...v3Bottom600pxMatDialogConfig,
                    autoFocus: false,
                    viewContainerRef: this.viewContainerRef,
                    closeOnNavigation: true,
                    disableClose: true,
                }),
            };

            this.state.matDialogRef.backdropClick().pipe(takeUntil(this.state.matDialogRef.afterClosed())).subscribe(() => {
                this.state.matDialogRef.close();
            });
        });
    }

    openAttachAudioMatDialog(): void {
        setTimeout(() => {
            this.state = {
                ...this.state,
                matDialogRef: this.matDialog.open(this.attachAudioMatDialog, {
                    ...v3Bottom600pxMatDialogConfig,
                    autoFocus: false,
                    viewContainerRef: this.viewContainerRef,
                    closeOnNavigation: true,
                    disableClose: true,
                }),
            };

            this.state.matDialogRef.backdropClick().pipe(takeUntil(this.state.matDialogRef.afterClosed())).subscribe(() => {
                this.state.matDialogRef.close();
            });
        });
    }

    get commonMatMenuProps(): EvodeskDeskDeadlineComponentProps {
        return {
            card: this.state.props.card,
            variant: 'mat-menu',
        };
    }

    get commonMatModalProps(): EvodeskDeskDeadlineComponentProps {
        return {
            card: this.state.props.card,
            variant: 'modal',
        };
    }
}
