import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskCardChecklistModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardChecklist.model';

interface Props {
    card: DeskCardModel;
    checklist: DeskCardChecklistModel;
}

interface State {
    ready: boolean;
}
export {Props as EvodeskDeskCardModalChecklistComponentProps};

@Component({
    selector: 'evodesk-desk-card-modal-checklist',
    templateUrl: './EvodeskDeskCardModalChecklist.component.pug',
    styleUrls: [
        './EvodeskDeskCardModalChecklist.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardModalChecklistComponent implements OnChanges
{
    @Input() props: Props;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }
}
