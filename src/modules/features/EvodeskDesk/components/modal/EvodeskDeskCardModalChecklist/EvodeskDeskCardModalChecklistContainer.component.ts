import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskCardChecklistModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardChecklist.model';

import {EvodeskDeskCardModalChecklistComponentProps} from './EvodeskDeskCardModalChecklist.component';

interface State {
    ready: boolean;
    props?: EvodeskDeskCardModalChecklistComponentProps;
}

@Component({
    selector: 'evodesk-desk-modal-checklist-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <ng-container *ngIf="!!state && state.ready">
            <evodesk-desk-card-modal-checklist [props]="state.props"></evodesk-desk-card-modal-checklist>
        </ng-container>
    `,
})
export class EvodeskDeskCardModalChecklistContainerComponent implements OnChanges, OnDestroy
{
    @Input() card: DeskCardModel;
    @Input() checklist: DeskCardChecklistModel;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: undefined,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['card']) {
            if (this.card && this.checklist) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        card: this.card,
                        checklist: this.checklist,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
