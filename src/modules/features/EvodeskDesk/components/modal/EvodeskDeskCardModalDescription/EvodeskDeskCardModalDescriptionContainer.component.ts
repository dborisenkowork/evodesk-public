import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskCardModalDescriptionComponentProps, EvodeskDeskCardModalDescriptionComponentSetDescriptionEvent as SetDescriptionEvent} from './EvodeskDeskCardModalDescription.component';

import {EvodeskDeskUpdateCardService} from '../../../services/EvodeskDeskUpdateCard.service';
import {DeskUpdateCardChangedField} from '../../../../../app/EvodeskRESTApi/services/DeskREST.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskCardModalDescriptionComponentProps;
}

@Component({
    selector: 'evodesk-desk-modal-description-container',
    template: `
        <ng-container *ngIf="!!state && state.ready">
            <evodesk-desk-card-modal-description
              [props]="state.props"
              (setDescription)="setDescription($event)"
            ></evodesk-desk-card-modal-description>
        </ng-container>
    `,
})
export class EvodeskDeskCardModalDescriptionContainerComponent implements OnChanges, OnDestroy
{
    @Input() card: DeskCardModel;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: undefined,
    };

    constructor(
        private service: EvodeskDeskUpdateCardService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['card']) {
            if (this.card) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        card: this.card,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    setDescription($event: SetDescriptionEvent): void {
        this.service.updateCard(DeskUpdateCardChangedField.Description, {...this.card, description: $event }).subscribe();
    }
}
