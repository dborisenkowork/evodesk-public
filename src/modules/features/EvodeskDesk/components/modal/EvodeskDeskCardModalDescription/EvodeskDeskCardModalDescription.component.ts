import {AfterViewChecked, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskConfirmModalService} from '../../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';

interface Props {
    card: DeskCardModel;
}

interface State {
    ready: boolean;
    form: FormGroup;
    isFocused: boolean;
    isEditing: boolean;
    doFocus: boolean;
}

type SetDescriptionEvent = string | undefined;

interface FormValue {
    description: string | null;
}

export {Props as EvodeskDeskCardModalDescriptionComponentProps};
export {SetDescriptionEvent as EvodeskDeskCardModalDescriptionComponentSetDescriptionEvent};

@Component({
    selector: 'evodesk-desk-card-modal-description',
    templateUrl: './EvodeskDeskCardModalDescription.component.pug',
    styleUrls: [
        './EvodeskDeskCardModalDescription.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardModalDescriptionComponent implements OnChanges, AfterViewChecked, OnDestroy
{
    @Input() props: Props;

    @ViewChild('descriptionInput') descriptionInputRef: ElementRef;

    @Output('setDescription') setDescriptionEvent: EventEmitter<SetDescriptionEvent> = new EventEmitter<SetDescriptionEvent>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        form: this.fb.group({
            description: [''],
        }),
        isFocused: false,
        isEditing: false,
        doFocus: false,
    };

    constructor(
        private fb: FormBuilder,
        private ngZone: NgZone,
        private cdr: ChangeDetectorRef,
        private viewContainerRef: ViewContainerRef,
        private confirm: EvodeskConfirmModalService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };

            this.state.form.patchValue({
                description: this.props.card.description,
            });
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngAfterViewChecked(): void {
        if (this.state.doFocus) {
            this.ngZone.runOutsideAngular(() => {
                setTimeout(() => {
                    (this.descriptionInputRef.nativeElement as HTMLElement).focus();
                });
            });

            this.state = {
                ...this.state,
                doFocus: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get shouldDisplayInput(): boolean {
        return this.state.isEditing || !! this.props.card.description;
    }

    get isReadonly(): boolean {
        return ! this.state.isEditing;
    }

    doFocus(): void {
        this.state = {
            ...this.state,
            doFocus: true,
        };
    }

    edit(): void {
        if (! this.state.isEditing) {
            this.state = {
                ...this.state,
                isFocused: true,
                isEditing: true,
            };

            this.doFocus();
        }
    }

    focus(): void {
        if (this.state.isEditing) {
            this.state = {
                ...this.state,
                isFocused: true,
            };
        }
    }

    blur(): void {
        this.state = {
            ...this.state,
            isFocused: false,
        };
    }

    ngSubmit(): void {
        this.state = {
            ...this.state,
            isFocused: false,
            isEditing: false,
        };

        this.setDescriptionEvent.emit(!! (this.state.form.value as FormValue).description ? (this.state.form.value as FormValue).description : undefined);
    }

    revert(): void {
        const prefix: string = 'EvodeskDesk.components.modal.EvodeskDeskCardModalDescription.ConfirmRevert';

        const orig: string = this.props.card.description;
        const head: string = (this.state.form.value as FormValue).description;

        if (orig !== head) {
            this.confirm.open({
                viewContainerRef: this.viewContainerRef,
                closeOnNavigation: true,
                title: {
                    text: `${prefix}.Title`,
                    translate: true,
                },
                text: {
                    text: `${prefix}.Text`,
                    translate: true,
                },
                confirmButton: {
                    text: `${prefix}.Confirm`,
                    translate: true,
                },
                cancelButton: {
                    text: `${prefix}.Cancel`,
                    translate: true,
                },
                confirm: (() => {
                    this.state = {
                        ...this.state,
                        isFocused: false,
                        isEditing: false,
                    };

                    this.state.form.patchValue({
                        description: this.props.card.description,
                    });

                    this.cdr.detectChanges();
                }),
                cancel: (() => {
                    this.doFocus();
                }),
            }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
        } else {
            this.state = {
                ...this.state,
                isFocused: false,
                isEditing: false,
            };
        }
    }
}
