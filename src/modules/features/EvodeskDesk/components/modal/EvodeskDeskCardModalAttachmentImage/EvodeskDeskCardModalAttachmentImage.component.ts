import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {Observable} from 'rxjs';

import * as moment from 'moment';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../../types/Backend.types';

import {environment} from '../../../../../../environments/environment.config';

import {DeskCardAttachmentModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardAttachment.model';
import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDateService} from '../../../../../app/EvodeskApp/services/EvodeskDate.service';

interface Props {
    card: DeskCardModel;
    attachment: DeskCardAttachmentModel;
    accessToken: string;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskDeskCardModalAttachmentImageComponentProps};

@Component({
    selector: 'evodesk-desk-card-modal-attachment-image',
    templateUrl: './EvodeskDeskCardModalAttachmentImage.component.pug',
    styleUrls: [
        './EvodeskDeskCardModalAttachmentImage.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardModalAttachmentImageComponent implements OnChanges
{
    @Input() props: Props;

    @Output('delete') deleteEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private dates: EvodeskDateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    get date$(): Observable<string> {
        return this.dates.diffFromNowV5(moment(this.props.attachment.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate());
    }

    get text(): any {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/desk/${this.props.card.deskId}/card/${this.props.card.id}/attachment/${this.props.attachment.id}/download`;
    }

    get link(): any {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/desk/${this.props.card.deskId}/card/${this.props.card.id}/attachment/${this.props.attachment.id}/download?access_token=${this.props.accessToken}`;
    }

    get previewNgStyle(): any {
        return {
            'background-image': `url(${this.link})`,
        };
    }

    delete(): void {
        this.deleteEvent.emit(undefined);
    }
}
