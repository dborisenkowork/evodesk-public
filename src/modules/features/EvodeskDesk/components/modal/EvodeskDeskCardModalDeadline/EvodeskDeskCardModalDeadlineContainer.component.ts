import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {MatDialog, MatDialogRef, MatMenu} from '@angular/material';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {evodeskDeskMobileBreakpoint} from '../../../configs/EvodeskDesk.constants';

import {v3Bottom317pxMatDialogConfig} from '../../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskCardModalDeadlineComponent, EvodeskDeskCardModalDeadlineComponentProps} from './EvodeskDeskCardModalDeadline.component';
import {EvodeskDeskDeadlineComponent, EvodeskDeskDeadlineComponentProps} from '../../common/EvodeskDeskDeadline/EvodeskDeskDeadline.component';

import {EvodeskDeskUpdateCardService} from '../../../services/EvodeskDeskUpdateCard.service';
import {DeskUpdateCardChangedField} from '../../../../../app/EvodeskRESTApi/services/DeskREST.service';

const breakpointMobile: number = evodeskDeskMobileBreakpoint;

interface State {
    ready: boolean;
    props?: EvodeskDeskCardModalDeadlineComponentProps;
    isMobile: boolean;
    matDialogRef?: MatDialogRef<EvodeskDeskDeadlineComponent>;
}

@Component({
    selector: 'evodesk-desk-modal-deadline-container',
    template: `
        <ng-container *ngIf="!!state && state.ready">
            <evodesk-desk-card-modal-deadline
              #view
              [props]="state.props"
              [enableRepeat]="true"
              [enablePrivate]="false"
              (openDeadlineModal)="openDeadlineModal($event)"
              (toggleRepeat)="toggleRepeat()"
            ></evodesk-desk-card-modal-deadline>
        </ng-container>

        <mat-menu #deadlineMatMenu class="__evodesk-desk-modal">
          <evodesk-desk-deadline-container [props]="deadlineMatMenuProps" (close)="view.deadlineMatMenuTrigger.closeMenu()"></evodesk-desk-deadline-container>
        </mat-menu>

        <ng-template #deadlineMatDialog>
          <evodesk-desk-deadline-container [props]="deadlineMatModalProps" (close)="state.matDialogRef.close()" ></evodesk-desk-deadline-container>
        </ng-template>
    `,
})
export class EvodeskDeskCardModalDeadlineContainerComponent implements OnChanges, OnInit, OnDestroy
{
    @Input() card: DeskCardModel;

    @ViewChild('view') view: EvodeskDeskCardModalDeadlineComponent;
    @ViewChild('deadlineMatMenu', { read: MatMenu }) deadlineMatMenu: MatMenu;
    @ViewChild('deadlineMatDialog') deadlineMatDialogRef: TemplateRef<EvodeskDeskDeadlineComponent>;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: undefined,
        isMobile: false,
    };

    constructor(
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private breakpointObserver: BreakpointObserver,
        private updateCardService: EvodeskDeskUpdateCardService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['card']) {
            if (this.card) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        card: this.card,
                        deadlineMatMenu: this.deadlineMatMenu,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnInit(): void {
        this.breakpointObserver.observe(`(max-width: ${breakpointMobile}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this.state = {
                ...this.state,
                isMobile: true,
            };
        });

        this.breakpointObserver.observe(`(min-width: ${breakpointMobile + 1}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this.state = {
                ...this.state,
                isMobile: false,
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    openDeadlineModal(): void {
        if (this.state.isMobile) {
            this.state = {
                ...this.state,
                matDialogRef: this.matDialog.open(this.deadlineMatDialogRef, {
                    ...v3Bottom317pxMatDialogConfig,
                    autoFocus: false,
                    viewContainerRef: this.viewContainerRef,
                    closeOnNavigation: true,
                }),
            };
        } else {
            this.view.deadlineMatMenuTrigger.openMenu();
        }
    }

    get deadlineMatMenuProps(): EvodeskDeskDeadlineComponentProps {
        return {
            card: this.card,
            variant: 'mat-menu',
        };
    }

    get deadlineMatModalProps(): EvodeskDeskDeadlineComponentProps {
        return {
            card: this.card,
            variant: 'modal',
        };
    }

    toggleRepeat(): void {
        this.updateCardService.updateCard(DeskUpdateCardChangedField.Daily, { ...(this.card as DeskCardModel), daily: ! this.card.daily }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }
}
