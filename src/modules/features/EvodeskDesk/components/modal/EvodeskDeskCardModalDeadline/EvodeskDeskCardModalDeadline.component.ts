import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatMenu, MatMenuTrigger} from '@angular/material';

import * as moment from 'moment';
import {Moment} from 'moment';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../../types/Backend.types';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

interface Props {
    card: DeskCardModel;
    deadlineMatMenu: MatMenu;
}

interface State {
    ready: boolean;
    form: FormGroup;
}

export {Props as EvodeskDeskCardModalDeadlineComponentProps};

function ucfirst(input: string): string {
    return input.charAt(0).toLocaleUpperCase() + input.slice(1);
}

function lcfirst(input: string): string {
    return input.charAt(0).toLocaleLowerCase() + input.slice(1);
}

@Component({
    selector: 'evodesk-desk-card-modal-deadline',
    templateUrl: './EvodeskDeskCardModalDeadline.component.pug',
    styleUrls: [
        './EvodeskDeskCardModalDeadline.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardModalDeadlineComponent implements OnChanges
{
    @Input() props: Props;

    @Input() enableRepeat: boolean = true;
    @Input() enablePrivate: boolean = false;

    @Output('openDeadlineModal') openDeadlineModalEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('toggleRepeat') toggleRepeatEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('deadlineMatMenuTrigger', { read: MatMenuTrigger }) deadlineMatMenuTrigger: MatMenuTrigger;

    public state: State = {
        ready: false,
        form: this.fb.group({
            deadline: [null],
            isRepeat: [false],
            isPrivate: [false],
        }),
    };

    constructor(
        private fb: FormBuilder,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    get hasDeadline(): boolean {
        return !! this.props.card.deadline;
    }

    get deadline(): string {
        const now: Date = new Date();
        const deadline: Moment = moment(this.props.card.deadline, BACKEND_DATE_FORMAT_AS_MOMENT);

        if (now.getFullYear() === deadline.toDate().getFullYear()) {
            return `${ucfirst(deadline.format('dd'))}, ${deadline.format('D')} ${lcfirst(deadline.format('MMM'))} ${deadline.format('HH:mm')}`;
        } else {
            return `${ucfirst(deadline.format('dd'))}, ${deadline.format('D')} ${lcfirst(deadline.format('MMM'))} YYYY ${deadline.format('HH:mm')}`;
        }
    }

    get isRepeat(): boolean {
        return this.props.card.daily;
    }

    openDeadlineModal(): void {
        this.openDeadlineModalEvent.emit(undefined);
    }

    toggleRepeat(): void {
        this.toggleRepeatEvent.emit(undefined);
    }
}
