import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChange, SimpleChanges, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {v3Bottom349pxMatDialogConfig} from '../../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskParticipantModel} from '../../../../../app/EvodeskRESTApi/models/DeskParticipant.model';

import {EvodeskDeskCardModalProjectsComponentProps} from './EvodeskDeskCardModalProjects.component';
import {EvodeskDeskCardProjectsContainerComponent as Modal} from '../../common/EvodeskDeskCardProjects/EvodeskDeskCardProjectsContainer.component';

import {EvodeskDeskCardProjectsService} from '../../../services/EvodeskDeskCardProjects.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskCardModalProjectsComponentProps;
}

@Component({
    selector: 'evodesk-desk-card-modal-projects-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-card-modal-projects [props]="state.props" (add)="add()" (remove)="remove($event)"></evodesk-desk-card-modal-projects>
      </ng-container>
    `,
})
export class EvodeskDeskCardModalProjectsContainerComponent implements OnChanges
{
    @Input() card: DeskCardModel;

    public state: State = {
        ready: false,
    };

    constructor(
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private service: EvodeskDeskCardProjectsService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['card']) {
            if (this.card) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        card: this.card,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    add(): void {
        const closed$: Subject<void> = new Subject<void>();

        const dialogRef: MatDialogRef<Modal> = this.matDialog.open(Modal, {
            ...v3Bottom349pxMatDialogConfig,
            viewContainerRef: this.viewContainerRef,
            closeOnNavigation: true,
            disableClose: true,
        });

        dialogRef.componentInstance.card = this.card;
        dialogRef.componentInstance.ngOnChanges({ card: new SimpleChange(undefined, dialogRef.componentInstance.card, true) });

        dialogRef.componentInstance.closeEvent.pipe(takeUntil(closed$)).subscribe(() => {
            dialogRef.close();
        });

        dialogRef.afterClosed().pipe(takeUntil(closed$)).subscribe(() => {
            closed$.next(undefined);
        });

        dialogRef.backdropClick().pipe(takeUntil(closed$)).subscribe(() => {
            dialogRef.close();
        });
    }

    remove(input: DeskParticipantModel): void {
        this.service.excludeCardFromProject(this.card.deskId, this.card.id, input.id).subscribe();
    }
}
