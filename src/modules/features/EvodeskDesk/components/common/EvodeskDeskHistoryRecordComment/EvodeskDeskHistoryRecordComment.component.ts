import {AfterViewChecked, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, Output, SimpleChanges, ViewChild, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import * as moment from 'moment';

import {Observable} from 'rxjs';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../../types/Backend.types';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskCardCommentId} from '../../../../../app/EvodeskRESTApi/models/DeskCardComment.model';
import {DeskCardLogRecordModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardLogRecord.model';

import {EvodeskDateService} from '../../../../../app/EvodeskApp/services/EvodeskDate.service';
import {EvodeskDeskCommentBodyParserService} from '../../../services/EvodeskDeskCommentBodyParser.service';
import {EvodeskConfirmModalService} from '../../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';

interface FormValue {
    body?: string;
}

interface DeleteEvent {
    id: DeskCardCommentId;
}

interface UpdateEvent {
    id: DeskCardCommentId;
    formValue: FormValue;
}

type Screen = 'view' | 'edit';

interface State {
    ready: boolean;
    form: FormGroup;
    screen: Screen;
    doFocus: boolean;
    body: string;
}

interface Props {
    card: DeskCardModel;
    logRecord: DeskCardLogRecordModel;
    canEdit: boolean;
    canDelete: boolean;
}

export {Props as EvodeskDeskHistoryRecordCommentComponentProps};
export {DeleteEvent as EvodeskDeskHistoryRecordCommentComponentDeleteEvent};
export {UpdateEvent as EvodeskDeskHistoryRecordCommentComponentUpdateEvent};

@Component({
    selector: 'evodesk-desk-history-record-comment',
    templateUrl: './EvodeskDeskHistoryRecordComment.component.pug',
    styleUrls: [
        './EvodeskDeskHistoryRecordComment.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskHistoryRecordCommentComponent implements OnChanges, AfterViewChecked
{
    @Input() props: Props;

    @Output('delete') deleteEvent: EventEmitter<DeleteEvent> = new EventEmitter<DeleteEvent>();
    @Output('update') updateEvent: EventEmitter<UpdateEvent> = new EventEmitter<UpdateEvent>();

    @ViewChild('body') bodyRef: ElementRef;

    public state: State = {
        ready: false,
        screen: 'view',
        body: '',
        form: this.fb.group({
            body: [null, [Validators.required]],
        }),
        doFocus: false,
    };

    constructor(
        private fb: FormBuilder,
        private ngZone: NgZone,
        private viewContainerRef: ViewContainerRef,
        private dates: EvodeskDateService,
        private commentBodyParsed: EvodeskDeskCommentBodyParserService,
        private uiConfirm: EvodeskConfirmModalService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
                body: this.props.card.comments.filter(c => c.id === parseInt(this.props.logRecord.entity_id, 10))[0].text,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngAfterViewChecked(): void {
        if (this.state.doFocus) {
            this.state = {
                ...this.state,
                doFocus: false,
            };

            this.ngZone.runOutsideAngular(() => {
                window.requestAnimationFrame(() => {
                    (this.bodyRef.nativeElement as HTMLElement).focus();
                });
            });
        }
    }

    get username(): string {
        return this.props.logRecord.user.name || this.props.logRecord.user.email;
    }

    get date$(): Observable<string> {
        return this.dates.diffFromNowV1(moment(this.props.logRecord.creation_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate());
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    get body(): string {
        return this.state.body;
    }

    get parsedBody(): Observable<string> {
        return this.commentBodyParsed.parseToHtml(this.body);
    }

    submit(): void {
        if (this.state.form.valid) {
            this.updateEvent.emit({
                id: parseInt(this.props.logRecord.entity_id, 10),
                formValue: this.formValue,
            });

            this.state = {
                ...this.state,
                screen: 'view',
                body: this.formValue.body,
            };
        }
    }

    edit(): void {
        this.state = {
            ...this.state,
            screen: 'edit',
            doFocus: true,
        };

        this.state.form.patchValue(<FormValue>{
            body: this.body,
        });
    }

    cancel(): void {
        this.state = {
            ...this.state,
            screen: 'view',
        };
    }

    delete(): void {
        const prefix: string = 'EvodeskDesk.components.common.EvodeskDeskHistoryRecordComment.ConfirmDelete';

        this.uiConfirm.open({
            title: {
                text: `${prefix}.Title`,
                translate: true,
            },
            text: {
                text: `${prefix}.Text`,
                translate: true,
            },
            confirmButton: {
                text: `${prefix}.Confirm`,
                translate: true,
            },
            cancelButton: {
                text: `${prefix}.Cancel`,
                translate: true,
            },
            confirm: () => {
                this.deleteEvent.emit({
                    id: parseInt(this.props.logRecord.entity_id, 10),
                });
            },
            viewContainerRef: this.viewContainerRef,
            closeOnNavigation: true,
        }).subscribe();
    }
}
