import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewContainerRef} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {EvodeskDeskProjectCardComponentProps} from './EvodeskDeskProjectCard.component';

import {DeskProjectModel} from '../../../../../app/EvodeskRESTApi/models/DeskProject.model';
import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskColumnType, doneColumns, failedColumns, getDeskColumnTypeById} from '../../../../../app/EvodeskRESTApi/models/DeskColumnDefinition.model';

import {EvodeskDeskAction, EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {EvodeskDeskProjectsService} from '../../../services/EvodeskDeskProjects.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskProjectCardComponentProps;
}

@Component({
    selector: 'evodesk-desk-project-card-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-project-card [props]="state.props" (edit)="edit()" (toggle)="toggle()" (delete)="delete()"></evodesk-desk-project-card>
      </ng-container>
    `,
})
export class EvodeskDeskProjectCardContainerComponent implements OnChanges, OnInit, OnDestroy
{
    @Input() project: DeskProjectModel;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private viewContainerRef: ViewContainerRef,
        private deskState: EvodeskDeskStateService,
        private service: EvodeskDeskProjectsService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['project']) {
            if (this.project) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        project: this.project,
                    },
                };

                this.recalculateStats();

                this.cdr.detectChanges();
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnInit(): void {
        const subscribeToCardsChanges: Function = () => {
            this.deskState.current$.pipe(
                takeUntil(this.ngOnDestroy$),
                map(s => s.current.sourceCards),
                filter(s => !! s),
                distinctUntilChanged(),
            ).subscribe(() => {
                this.recalculateStats();

                this.cdr.detectChanges();
            });
        };

        const subscribeToCurrentProject: Function = () => {
            this.deskState.current$.pipe(
                takeUntil(this.ngOnDestroy$),
                map(s => s.current.currentProject),
                distinctUntilChanged(),
            ).subscribe((currentProjectId) => {
                const isActive: boolean = currentProjectId === this.project.id;

                if (isActive && ! this.state.props.isActive) {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            isActive: isActive,
                        },
                    };

                    this.cdr.detectChanges();
                } else if (! isActive && this.state.props.isActive) {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            isActive: isActive,
                        },
                    };

                    this.cdr.detectChanges();
                }
            });
        };

        subscribeToCardsChanges();
        subscribeToCurrentProject();
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    recalculateStats(): void {
        const cards: Array<DeskCardModel> = this.deskState.snapshot.sourceCards.filter(c => !!~c.projects.map(e => e.id).indexOf(this.project.id));

        const week: number = cards.filter(c => getDeskColumnTypeById(c.column) === DeskColumnType.CommittedWeek).length;
        const inProgress: number = cards.filter(c => getDeskColumnTypeById(c.column) === DeskColumnType.CommittedToday).length;
        const done: number = cards.filter(c => !!~doneColumns.indexOf(getDeskColumnTypeById(c.column))).length;
        const failed: number = cards.filter(c => !!~failedColumns.indexOf(getDeskColumnTypeById(c.column))).length;
        const total: number = week + inProgress + done + failed;

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                countCardsTotal: total,
                countCardsWeek: week,
                countCardsInProgress: inProgress,
                countCardsDone: done,
                countCardsFailed: failed,
                progress: total > 0 ? ((100 / total) * (done + failed)) : 100,
            },
        };
    }

    edit(): void {
        this.service.askForEditProject(this.project, this.viewContainerRef);
    }

    delete(): void {
        this.service.askForDeleteProject(this.project, this.viewContainerRef);
    }

    toggle(): void {
        if (this.state.props.isActive) {
            this.deskState.dispatch({
                type: EvodeskDeskAction.SetCurrentProject,
                payload: {
                    projectId: undefined,
                },
            });

            this.deskState.dispatch({
                type: EvodeskDeskAction.FiltersClearProjectFilter,
            });
        } else {
            this.deskState.dispatch({
                type: EvodeskDeskAction.SetCurrentProject,
                payload: {
                    projectId: this.project.id,
                },
            });

            this.deskState.dispatch({
                type: EvodeskDeskAction.FiltersSetProjectFilter,
                payload: {
                    projectId: this.project.id,
                },
            });
        }
    }
}
