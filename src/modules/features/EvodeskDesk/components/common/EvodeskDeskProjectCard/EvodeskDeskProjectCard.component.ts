import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {DeskProjectModel} from '../../../../../app/EvodeskRESTApi/models/DeskProject.model';

interface Props {
    project: DeskProjectModel;
    progress: number;
    isActive: boolean;
    countCardsTotal: number;
    countCardsWeek: number;
    countCardsInProgress: number;
    countCardsDone: number;
    countCardsFailed: number;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskDeskProjectCardComponentProps};

@Component({
    selector: 'evodesk-desk-project-card',
    templateUrl: './EvodeskDeskProjectCard.component.pug',
    styleUrls: ['./EvodeskDeskProjectCard.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskProjectCardComponent implements OnChanges
{
    @Input() props: Props;

    @Output('edit') editEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('delete') deleteEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('toggle') toggleEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    edit(): void {
        this.editEvent.emit(undefined);
    }

    delete(): void {
        this.deleteEvent.emit(undefined);
    }

    toggle(): void {
        this.toggleEvent.emit(undefined);
    }
}
