import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';
import {HttpEventType, HttpProgressEvent, HttpResponse} from '@angular/common/http';

import {Observable, Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskCardAttachmentModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardAttachment.model';

import {EvodeskDeskAttachImageComponentInputProcess as Process, EvodeskDeskAttachImageComponentProps, EvodeskDeskAttachImageComponentStartProcessEvent, EvodeskDeskAttachImageComponentVariant} from './EvodeskDeskAttachImage.component';

import {EvodeskDeskAttachmentService} from '../../../services/EvodeskDeskAttachment.service';

type Upload = Observable<HttpProgressEvent | HttpResponse<DeskCardAttachmentModel>>;

interface Props {
    card: DeskCardModel;
    variant: EvodeskDeskAttachImageComponentVariant;
}

interface State {
    ready: boolean;
    props?: EvodeskDeskAttachImageComponentProps;
    processes: Array<{
        observable: Upload;
        subscription: Subscription;
        process: Process;
    }>;
}

export {Props as EvodeskDeskAttachImageContainerComponentProps};

@Component({
    selector: 'evodesk-desk-attach-image-container',
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-attach-image [props]="state.props" (close)="closeEvent.emit($event)" (startProcess)="startProcess($event)" (stopProcess)="stopProcess($event)" (stopAllProcesses)="stopAllProcesses()"></evodesk-desk-attach-image>
      </ng-container>
    `,
})
export class EvodeskDeskAttachImageContainerComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        processes: [],
    };

    constructor(
        private service: EvodeskDeskAttachmentService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    card: this.props.card,
                    variant: this.props.variant,
                },
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    startProcess($event: EvodeskDeskAttachImageComponentStartProcessEvent): void {
        this.state = {
            ...this.state,
            processes: $event.processes.map((process) => {
                const observable: Upload = this.service.attachFile(this.props.card.deskId, this.props.card.id, process.input);
                const subscription: Subscription = observable.pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                    (event) => {
                        if (event.type === HttpEventType.UploadProgress) {
                            process.setPercents(Math.ceil((event.total / 100) * event.loaded));
                        } else if (event.type === HttpEventType.Response) {
                            process.setAsProcessed();
                        }
                    },
                    () => {
                        process.setAsError();
                    },
                );

                return {
                    process: process,
                    observable: observable,
                    subscription: subscription,
                };
            }),
        };
    }

    stopProcess($event: Process): void {
        this.state.processes.forEach(p => {
            if (p.process === $event) {
                p.subscription.unsubscribe();
            }
        });
    }

    stopAllProcesses(): void {
        this.state.processes.forEach(p => {
            p.subscription.unsubscribe();
        });
    }
}
