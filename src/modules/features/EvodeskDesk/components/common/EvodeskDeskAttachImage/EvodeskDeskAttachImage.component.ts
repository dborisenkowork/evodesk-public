import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {SafeUrl} from '@angular/platform-browser/src/security/dom_sanitization_service';
import {DomSanitizer} from '@angular/platform-browser';

import {evodeskDeskAllowedImageAttachmentTypes} from '../../../configs/EvodeskDesk.constants';

import {EvodeskAlertModalService} from '../../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

interface InputFile {
    file: File;
    preview: SafeUrl;
    ngStyle: any;
    status: InputStatus;
    percents: number;
}

interface Props {
    card: DeskCardModel;
    variant: Variant;
}

type Screen = 'select' | 'lobby';
type Variant = 'modal' | 'control' | 'mat-menu';
type InputStatus = 'waiting' | 'processing' | 'processed' | 'error';

interface Process {
    input: File;
    setPercents: (percents: number) => void;
    setAsProcessed: () => void;
    setAsError: () => void;
}

interface StartProcessEvent {
    processes: Array<Process>;
}

interface State {
    ready: boolean;
    screen: Screen;
    isDndActive: boolean;
    selectedFiles: Array<InputFile>;
    processes: Array<Process>;
    processing: boolean;
}

export {Props as EvodeskDeskAttachImageComponentProps};
export {Variant as EvodeskDeskAttachImageComponentVariant};
export {Process as EvodeskDeskAttachImageComponentInputProcess};
export {StartProcessEvent as EvodeskDeskAttachImageComponentStartProcessEvent};

@Component({
    selector: 'evodesk-desk-attach-image',
    templateUrl: './EvodeskDeskAttachImage.component.pug',
    styleUrls: [
        './EvodeskDeskAttachImage.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskAttachImageComponent implements OnChanges, AfterViewInit
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('submit') submitEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output('startProcess') startProcessEvent: EventEmitter<StartProcessEvent> = new EventEmitter<StartProcessEvent>();
    @Output('stopProcess') stopProcessEvent: EventEmitter<Process> = new EventEmitter<Process>();
    @Output('stopAllProcesses') stopAllProcessesEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('fileInput') fileInputRef: ElementRef;

    public state: State = {
        ready: false,
        screen: 'select',
        isDndActive: false,
        selectedFiles: [],
        processes: [],
        processing: false,
    };

    constructor(
        private ngZone: NgZone,
        private cdr: ChangeDetectorRef,
        private uiAlert: EvodeskAlertModalService,
        private sanitizer: DomSanitizer,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngAfterViewInit(): void {
        this.ngZone.runOutsideAngular(() => {
            setTimeout(() => {
                this.fileInput.click();
            });
        });
    }

    get fileInput(): HTMLInputElement {
        return this.fileInputRef.nativeElement;
    }

    get shouldDisplayModalHeader(): boolean {
        return this.props.variant !== 'control';
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    other(): void {
        this.state = {
            ...this.state,
            selectedFiles: [],
            screen: 'select',
        };
    }

    cancel(): void {
        if (this.state.processing) {
            this.stopAllProcesses();
        } else {
            this.close();
        }
    }

    onFileDropAreaNext(files: Array<File>): void {
        this.state = {
            ...this.state,
            isDndActive: false,
        };

        this.processInput(files);
    }

    onFileDropAreaDragEnter(): void {
        this.state = {
            ...this.state,
            isDndActive: true,
        };
    }

    onFileDropAreaDragLeave(): void {
        this.state = {
            ...this.state,
            isDndActive: false,
        };
    }

    onFileInputChange(): void {
        const files: FileList = this.fileInput.files;

        if (files.length > 0) {
            const result: Array<File> = [];

            for (let i: number = 0; i < files.length; i++) {
                result.push(files[i]);
            }

            this.processInput(result);
            this.fileInput.value = '';
        }
    }

    processInput(files: Array<File>): void {
        if (files.length > 12) {
            this.uiAlert.openTranslated('EvodeskDesk.components.common.EvodeskDeskAttachImage.TooManyFiles').subscribe();

            return;
        }

        for (const file of files) {
            if (!~evodeskDeskAllowedImageAttachmentTypes.indexOf(file.type)) {
                this.uiAlert.openTranslated('EvodeskDesk.components.common.EvodeskDeskAttachImage.InvalidImage').subscribe();

                return;
            }
        }

        if (files.length) {
            this.state = {
                ...this.state,
                selectedFiles: files.map(f => {
                    const objectUrl: string = URL.createObjectURL(f);
                    const ngStyle: any = { 'background-image': `url(${objectUrl})` };

                    return {
                        file: f,
                        preview: this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(f)),
                        ngStyle: ngStyle,
                        status: <any>'waiting',
                        percents: 0,
                    };
                }),
                screen: 'lobby',
            };
        }
    }

    progressBarFillNgStyle(input: InputFile): any {
        if (input.status === 'processed') {
            return {
                width: '100%',
            };
        } else {
            return {
                width: `${input.percents}%`,
            };
        }
    }

    deleteFromLobby(input: InputFile): void {
        this.state = {
            ...this.state,
            selectedFiles: this.state.selectedFiles.filter(i => i.file !== input.file),
        };

        if (input.status === 'processing') {
            this.stopProcessEvent.emit(this.state.processes.filter(p => p.input === input.file)[0]);
        }

        if (this.state.selectedFiles.length === 0) {
            this.other();
        }
    }

    startProcess(): void {
        this.state = {
            ...this.state,
            processes: this.state.selectedFiles.filter(i => i.status !== 'processed').map(i => {
                return {
                    input: i.file,
                    setAsError: () => {
                    i.status = 'error';

                        this.cdr.detectChanges();
                    },
                    setAsProcessed: () => {
                        i.status = 'processed';
                        i.percents = 100;

                        if (this.state.selectedFiles.filter(p => !!~['processing', 'error'].indexOf(p.status)).length === 0) {
                            setTimeout(() => {
                                this.close();
                            }, 500);
                        }

                        this.cdr.detectChanges();
                    },
                    setPercents: (percents: number) => {
                        i.percents = percents;

                        this.cdr.detectChanges();
                    },
                };
            }),
        };

        this.startProcessEvent.emit({
            processes: this.state.processes,
        });
    }

    stopAllProcesses(): void {
        this.stopAllProcessesEvent.emit(undefined);
    }
}
