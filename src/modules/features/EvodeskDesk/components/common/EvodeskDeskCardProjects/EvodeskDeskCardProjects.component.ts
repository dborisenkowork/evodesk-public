import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskProjectModel} from '../../../../../app/EvodeskRESTApi/models/DeskProject.model';

type Variant = 'modal' | 'control' | 'mat-menu';

interface Props {
    card: DeskCardModel;
    available: Array<DeskProjectModel>;
    variant: Variant;
}

interface State {
    ready: boolean;
    isSearchFieldFocused: boolean;
    searchForm: FormGroup;
}

interface SearchFormValue {
    search: string;
}

export {Props as EvodeskDeskCardProjectsComponentProps};
export {Variant as EvodeskDeskCardProjectsComponentVariant};

@Component({
    selector: 'evodesk-desk-card-projects',
    templateUrl: './EvodeskDeskCardProjects.component.pug',
    styleUrls: [
        './EvodeskDeskCardProjects.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardProjectsComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('toggle') toggleEvent: EventEmitter<DeskProjectModel> = new EventEmitter<DeskProjectModel>();
    @Output('addProject') addProjectEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('editProject') editProjectEvent: EventEmitter<DeskProjectModel> = new EventEmitter<DeskProjectModel>();
    @Output('deleteProject') deleteEvent: EventEmitter<DeskProjectModel> = new EventEmitter<DeskProjectModel>();

    public state: State = {
        ready: false,
        isSearchFieldFocused: false,
        searchForm: this.fb.group({
            search: [null],
        }),
    };

    constructor(
        private fb: FormBuilder,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    isActive(input: DeskProjectModel): boolean {
        return !!~this.props.card.projects.map(p => p.id).indexOf(input.id);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    toggle(input: DeskProjectModel): void {
        this.toggleEvent.emit(input);
    }

    addProject(): void {
        this.addProjectEvent.emit(undefined);
    }

    edit(p: DeskProjectModel): void {
        this.editProjectEvent.emit(p);
    }

    delete(p: DeskProjectModel): void {
        this.deleteEvent.emit(p);
    }

    get shouldDisplayModalHeader(): boolean {
        return this.props.variant !== 'control';
    }

    get filtered(): Array<DeskProjectModel> {
        const search: string | null = (this.state.searchForm.value as SearchFormValue).search;

        if (search) {
            return this.props.available.filter(a => !!~(a.title).toLocaleLowerCase().indexOf(search));
        } else {
            return this.props.available;
        }
    }
}
