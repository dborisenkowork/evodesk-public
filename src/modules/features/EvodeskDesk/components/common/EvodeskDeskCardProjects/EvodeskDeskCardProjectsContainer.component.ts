import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewContainerRef} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskProjectId, DeskProjectModel} from '../../../../../app/EvodeskRESTApi/models/DeskProject.model';

import {EvodeskDeskCardProjectsComponentProps, EvodeskDeskCardProjectsComponentVariant} from './EvodeskDeskCardProjects.component';

import {EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {EvodeskDeskCardProjectsService} from '../../../services/EvodeskDeskCardProjects.service';
import {EvodeskDeskProjectsService} from '../../../services/EvodeskDeskProjects.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskCardProjectsComponentProps;
}

@Component({
    selector: 'evodesk-desk-card-projects-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-card-projects
          [props]="state.props"
          (close)="close()"
          (toggle)="toggle($event)"
          (addProject)="addProject()"
          (editProject)="editProject($event)"
          (deleteProject)="deleteProject($event)"
        ></evodesk-desk-card-projects>
      </ng-container>
    `,
})
export class EvodeskDeskCardProjectsContainerComponent implements OnChanges, OnInit, OnDestroy
{
    @Input() card: DeskCardModel;
    @Input() variant: EvodeskDeskCardProjectsComponentVariant;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private stop$: Subject<DeskProjectId> = new Subject<DeskProjectId>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private deskState: EvodeskDeskStateService,
        private viewContainerRef: ViewContainerRef,
        private service: EvodeskDeskCardProjectsService,
        private projectsService: EvodeskDeskProjectsService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (changes['card']) {
            if (this.card) {
                this.deskState.current$.pipe(
                    takeUntil(this.ngOnChanges$),
                    map(s => s.current.sourceCards.filter(sc => sc.id === this.card.id)[0]),
                    distinctUntilChanged(),
                ).subscribe((card) => {
                    this.state = {
                        ...this.state,
                        ready: true,
                        props: {
                            ...this.state.props,
                            card: card,
                            available: this.deskState.snapshot.projects,
                            variant: this.variant,
                        },
                    };

                    this.cdr.detectChanges();
                });
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }

        if (changes['variant'] && this.state.ready) {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    variant: this.variant,
                },
            };
        }
    }

    ngOnInit(): void {
        this.deskState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.projects),
            distinctUntilChanged(),
        ).subscribe((projects) => {
            if (this.state.ready) {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        available: [...projects],
                    },
                };

                this.cdr.detectChanges();
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    toggle(input: DeskProjectModel): void {
        const isActive: boolean = !!~this.state.props.card.projects.map(p => p.id).indexOf(input.id);

        this.stop$.next(input.id);

        if (isActive) {
            this.service.excludeCardFromProject(this.card.deskId, this.card.id, input.id).pipe(takeUntil(this.stop$.pipe(filter(id => id === input.id)))).subscribe();
        } else {
            this.service.includeCardToProject(this.card.deskId, this.card.id, input.id).pipe(takeUntil(this.stop$.pipe(filter(id => id === input.id)))).subscribe();
        }
    }

    addProject(): void {
        this.projectsService.askForAddProject(this.viewContainerRef);
    }

    editProject(p: DeskProjectModel): void {
        this.projectsService.askForEditProject(p, this.viewContainerRef);
    }

    deleteProject(p: DeskProjectModel): void {
        this.projectsService.askForDeleteProject(p, this.viewContainerRef);
    }
}
