import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import {EvodeskDeskCardChecklistsComponentProps} from './EvodeskDeskCardChecklists.component';

import {DeskCardId} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {EvodeskDeskAddChecklistComponentProps} from '../EvodeskDeskAddChecklist/EvodeskDeskAddChecklist.component';

type Screen = 'list' | 'add';

interface State {
    ready: boolean;
    screen: Screen;
    addChecklistProps?: EvodeskDeskAddChecklistComponentProps;
    props?: EvodeskDeskCardChecklistsComponentProps;
}

@Component({
    selector: 'evodesk-desk-card-checklists-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="state.ready && state.screen === 'list'">
        <evodesk-desk-card-checklists [props]="state.props" (close)="close()" (addChecklist)="addChecklist()"></evodesk-desk-card-checklists>
      </ng-container>
      <ng-container *ngIf="state.ready && state.screen === 'add'">
        <evodesk-desk-add-checklist-container [props]="state.addChecklistProps" (close)="close()"></evodesk-desk-add-checklist-container>
      </ng-container>
    `,
})
export class EvodeskDeskCardChecklistsContainerComponent implements OnChanges, OnInit, OnDestroy {
    @Input() cardId: DeskCardId;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private next$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        screen: 'list',
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private deskState: EvodeskDeskStateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['cardId']) {
            this.next$.next();

            if (this.cardId) {
                this.deskState.current$.pipe(
                    takeUntil(this.next$),
                    map(s => s.current.sourceCards.filter(sc => sc.id === this.cardId)[0]),
                    distinctUntilChanged(),
                ).subscribe((card) => {
                    if (card) {
                        this.state = {
                            ...this.state,
                            ready: true,
                            props: {
                                ...this.state.props,
                                card: card,
                                checklists: card.checklists,
                            },
                            addChecklistProps: {
                                card: card,
                                variant: 'mat-menu',
                            },
                        };
                    } else {
                        this.state = {
                            ...this.state,
                            ready: false,
                        };
                    }

                    this.cdr.detectChanges();
                });
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnInit(): void {
        if (this.state.props && this.state.props.checklists.length > 0) {
            this.state = {
                ...this.state,
                screen: 'list',
            };
        } else {
            this.state = {
                ...this.state,
                screen: 'add',
            };
        }

        this.cdr.detectChanges();
    }

    ngOnDestroy(): void {
        this.next$.next();
        this.ngOnDestroy$.next(undefined);
    }

    close(): void {
        if (this.state.screen === 'add') {
            if (this.state.props.checklists.length) {
                this.goList();
            } else {
                this.closeEvent.emit(undefined);
            }
        } else {
            this.closeEvent.emit(undefined);
        }
    }

    goList(): void {
        this.state = {
            ...this.state,
            screen: 'list',
        };
    }

    addChecklist(): void {
        this.state = {
            ...this.state,
            screen: 'add',
        };
    }
}
