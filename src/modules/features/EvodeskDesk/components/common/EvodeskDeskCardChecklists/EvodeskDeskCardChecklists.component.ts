import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskCardChecklistModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardChecklist.model';

interface Props {
    card: DeskCardModel;
    checklists: Array<DeskCardChecklistModel>;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskDeskCardChecklistsComponentProps};

@Component({
    selector: 'evodesk-desk-card-checklists',
    templateUrl: './EvodeskDeskCardChecklists.component.pug',
    styleUrls: [
        './EvodeskDeskCardChecklists.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardChecklistsComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('addChecklist') addChecklistEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    trackByChecklistId(index: number, item: DeskCardChecklistModel): any {
        return item.id;
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    addChecklist(): void {
        this.addChecklistEvent.emit(undefined);
    }
}
