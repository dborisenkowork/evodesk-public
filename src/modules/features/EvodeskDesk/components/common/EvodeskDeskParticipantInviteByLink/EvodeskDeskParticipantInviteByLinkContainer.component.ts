import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {take} from 'rxjs/operators';

import {environment} from '../../../../../../environments/environment.config';

import {EvodeskDeskParticipantInviteByLinkComponentProps, EvodeskDeskParticipantInviteByLinkComponentVariant} from './EvodeskDeskParticipantInviteByLink.component';

import {EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskParticipantInviteByLinkComponentProps;
}

@Component({
    selector: 'evodesk-desk-participant-invite-by-link-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-participant-invite-by-link [props]="state.props" (close)="close()"></evodesk-desk-participant-invite-by-link>
      </ng-container>
    `,
})
export class EvodeskDeskParticipantInviteByLinkContainerComponent implements OnInit
{
    @Input() variant: EvodeskDeskParticipantInviteByLinkComponentVariant = 'modal';

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private deskState: EvodeskDeskStateService,
    ) {}

    ngOnInit(): void {
        this.deskState.current$.pipe(take(1)).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    link: `${environment.serverHostName}/desk/join/${s.current.desk.id}/${s.current.desk.invite_code}`,
                    variant: this.variant,
                },
            };

            this.cdr.detectChanges();
        });
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
