import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';

type Variant = 'modal' | 'control' | 'mat-menu';

interface Props {
    link: string;
    variant: Variant;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskDeskParticipantInviteByLinkComponentProps};
export {Variant as EvodeskDeskParticipantInviteByLinkComponentVariant};

@Component({
    selector: 'evodesk-desk-participant-invite-by-link',
    templateUrl: './EvodeskDeskParticipantInviteByLink.component.pug',
    styleUrls: [
        './EvodeskDeskParticipantInviteByLink.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskParticipantInviteByLinkComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('copyLinkRef') copyLinkRef: ElementRef;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get shouldDisplayModalHeader(): boolean {
        return this.props.variant !== 'control';
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    copyLink(): void {
        (this.copyLinkRef.nativeElement as HTMLInputElement).focus();
        (this.copyLinkRef.nativeElement as HTMLInputElement).setSelectionRange(0, this.props.link.length);

        document.execCommand('Copy');
    }
}

