import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {proxy} from '../../../../../../functions/proxy.function';

import {EvodeskDeskAddChecklistComponentFormValue, EvodeskDeskAddChecklistComponentProps} from './EvodeskDeskAddChecklist.component';

import {EvodeskDeskCardChecklistService} from '../../../services/EvodeskDeskCardChecklist.service';

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-desk-add-checklist-container',
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-add-checklist [props]="props" (close)="closeEvent.emit($event)" (create)="create($event)"></evodesk-desk-add-checklist>
      </ng-container>
    `,
})
export class EvodeskDeskAddChecklistContainerComponent implements OnChanges, OnDestroy
{
    @Input() props: EvodeskDeskAddChecklistComponentProps;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private checklistCardService: EvodeskDeskCardChecklistService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    create($event: EvodeskDeskAddChecklistComponentFormValue): void {
        proxy(this.checklistCardService.addChecklist(this.props.card.id, {
            title: $event.title,
        })).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.closeEvent.emit(undefined);
        });
    }
}
