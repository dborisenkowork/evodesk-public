import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Subject} from 'rxjs';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';


interface State {
    ready: boolean;
    form: FormGroup;
    isTitleFocused: boolean;
}

interface Props {
    card: DeskCardModel;
    variant: Variant;
}

interface FormValue {
    title: string;
}

type Variant = 'modal' | 'control' | 'mat-menu';

export {Props as EvodeskDeskAddChecklistComponentProps};
export {Variant as EvodeskDeskAddChecklistComponentVariant};
export {FormValue as EvodeskDeskAddChecklistComponentFormValue};

@Component({
    selector: 'evodesk-desk-add-checklist',
    templateUrl: './EvodeskDeskAddChecklist.component.pug',
    styleUrls: [
        './EvodeskDeskAddChecklist.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskAddChecklistComponent implements OnChanges, OnDestroy, AfterViewInit
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('create') createEvent: EventEmitter<FormValue> = new EventEmitter<FormValue>();

    @ViewChild('titleInput') titleInputRef: ElementRef;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        form: this.fb.group({
            title: [null, [Validators.required]],
        }),
        isTitleFocused: false,
    };

    constructor(
        private fb: FormBuilder,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props.card && this.props.variant) {
            this.state = {
                ...this.state,
                ready: true,
            };

            if (changes['card']) {
            }
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    ngAfterViewInit(): void {
        window.requestAnimationFrame(() => {
            (this.titleInputRef.nativeElement as HTMLElement).focus();
        });
    }

    submit(): void {
        if (this.state.form.valid) {
            this.createEvent.emit(this.formValue);
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    get shouldDisplayModalHeader(): boolean {
        return this.props.variant !== 'control';
    }
}
