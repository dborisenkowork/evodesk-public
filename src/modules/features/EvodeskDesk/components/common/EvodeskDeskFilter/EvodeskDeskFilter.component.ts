import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {evodeskDeskDefaultPalette} from '../../../configs/EvodeskDesk.constants';

import {DeskLabelColorDescription, DeskLabelId, DeskLabelModel} from '../../../../../app/EvodeskRESTApi/models/DeskLabel.model';
import {deskFilterAvailableDeadlineVariant, DeskFilterDeadlineOptions, DeskFilterDeadlineVariant, DeskFilterLabelOptions, DeskFilterParticipantOptions, DeskFilterType, IDeskFilters} from '../../../models/DeskFilters.model';
import {DeskParticipantId, DeskParticipantModel} from '../../../../../app/EvodeskRESTApi/models/DeskParticipant.model';

type Screen = 'labels' | 'deadline' | 'participants';

export interface EvodeskDeskFilterComponentProps {
    filters: Array<IDeskFilters<any>>;
    labels: Array<DeskLabelModel>;
    participants: Array<DeskParticipantModel>;
}

interface State {
    ready: boolean;
    screen: Screen;
    isSearchFieldFocused: boolean;
    searchForm: FormGroup;
}

@Component({
    selector: 'evodesk-desk-filter',
    templateUrl: './EvodeskDeskFilter.component.pug',
    styleUrls: [
        './EvodeskDeskFilter.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskFilterComponent implements OnChanges
{
    static DEFAULT_LABELS_PER_LINE: number = 3;

    @Input() props: EvodeskDeskFilterComponentProps;
    @Input() labelsPerLine: number = EvodeskDeskFilterComponent.DEFAULT_LABELS_PER_LINE;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('reset') resetEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output('toggleLabel') toggleLabelEvent: EventEmitter<DeskLabelId> = new EventEmitter<DeskLabelId>();
    @Output('toggleDeadline') toggleDeadlineEvent: EventEmitter<DeskFilterDeadlineVariant> = new EventEmitter<DeskFilterDeadlineVariant>();
    @Output('toggleParticipant') toggleParticipantEvent: EventEmitter<DeskParticipantId> = new EventEmitter<DeskParticipantId>();

    public state: State = {
        ready: false,
        screen: 'labels',
        isSearchFieldFocused: false,
        searchForm: this.formBuilder.group({
            search: [''],
        }),
    };

    constructor(
        private formBuilder: FormBuilder,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props && this.props.filters !== undefined) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get sortedFilters(): Array<IDeskFilters<any>> {
        const sorted: Array<IDeskFilters<any>> = [...this.props.filters];

        sorted.sort((a, b) => {
            if (a.createdAt > b.createdAt) {
                return 1;
            } else if (a.createdAt.getTime() === b.createdAt.getTime()) {
                return 0;
            } else {
                return -1;
            }
        });

        return sorted;
    }

    get enabledLabels(): Array<DeskLabelId> {
        const result: Array<DeskLabelId> = [];

        this.props.filters.filter(f => f.type === DeskFilterType.Label).forEach(f => result.push(f.options.labelId));

        return result;
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    setScreen(screen: Screen): void {
        this.state = {
            ...this.state,
            screen: screen,
        };
    }

    toggleLabel(label: DeskLabelModel): void {
        this.toggleLabelEvent.emit(label.id);
    }

    trackByLabelId(index: number, item: DeskLabelModel): any {
        return item.id;
    }

    isLabelEnabled(labelId: number): boolean {
        return this.enabledLabels.filter(id => id === labelId).length > 0;
    }

    hasAnyLabels(): boolean {
        return this.enabledLabels.length > 0;
    }

    colorPinNgStyles(label: DeskLabelModel): any {
        return {
            'background-color': label.color,
        };
    }

    findDefaultPaletteByColor(color: string): DeskLabelColorDescription | undefined {
        return evodeskDeskDefaultPalette.filter(c => c.color.toLowerCase() === color.toLowerCase())[0];
    }

    onSearchInputFocus(): void {
        this.state = {
            ...this.state,
            isSearchFieldFocused: true,
        };
    }

    onSearchInputBlur(): void {
        this.state = {
            ...this.state,
            isSearchFieldFocused: false,
        };
    }

    clearSearchInput(): void {
        this.state.searchForm.setValue({
            search: '',
        });
    }

    get hasSearchInput(): boolean {
        return !! this.state.searchForm.get('search').value;
    }

    get hasFilters(): boolean {
        return this.enabledLabels.length > 0;
    }

    labelOfFilter(input: IDeskFilters<DeskFilterLabelOptions>): DeskLabelModel {
        return this.props.labels.filter(l => l.id === input.options.labelId)[0];
    }

    get availableDeadlines(): Array<DeskFilterDeadlineVariant> {
        return deskFilterAvailableDeadlineVariant;
    }

    translateDeadlineVariant(v: DeskFilterDeadlineVariant): string {
        return `EvodeskDesk.components.common.EvodeskDeskFilter.DeadlineVariants.${<any>v}`;
    }

    isDeadlineEnabled(v: DeskFilterDeadlineVariant): boolean {
        return this.props.filters.filter(f => f.type === DeskFilterType.Deadline && (f.options as DeskFilterDeadlineOptions).variant === v).length > 0;
    }

    toggleDeadline(v: DeskFilterDeadlineVariant): void {
        this.toggleDeadlineEvent.emit(v);
    }

    trackByParticipantId(index: number, p: DeskParticipantModel): any {
        return p.id;
    }

    toggleParticipant(p: DeskParticipantModel): void {
        this.toggleParticipantEvent.emit(p.id);
    }

    isParticipantEnabled(pId: DeskParticipantId): boolean {
        return this.props.filters.filter(f => f.type === DeskFilterType.Participant && (f.options as DeskFilterParticipantOptions).participantId === pId).length > 0;
    }

    participant(pId: DeskParticipantId): DeskParticipantModel {
        return this.props.participants.filter(p => p.id === pId)[0];
    }
}
