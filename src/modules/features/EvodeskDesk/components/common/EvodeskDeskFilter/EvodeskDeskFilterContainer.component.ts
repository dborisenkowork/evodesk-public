import {ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {DeskLabelId} from '../../../../../app/EvodeskRESTApi/models/DeskLabel.model';

import {EvodeskDeskFilterComponentProps} from './EvodeskDeskFilter.component';

import {EvodeskDeskAction, EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {DeskFilterDeadlineOptions, DeskFilterDeadlineVariant, DeskFilterLabelOptions, DeskFilterParticipantOptions, DeskFilterType, IDeskFilters} from '../../../models/DeskFilters.model';
import {DeskParticipantId} from '../../../../../app/EvodeskRESTApi/models/DeskParticipant.model';

interface State {
    ready: boolean;
    props?: EvodeskDeskFilterComponentProps;
}

@Component({
    selector: 'evodesk-desk-filter-container',
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-filter
          [props]="state.props"
          (close)="close()"
          (reset)="reset()"
          (toggleLabel)="toggleLabel($event)"
          (toggleDeadline)="toggleDeadline($event)"
          (toggleParticipant)="toggleParticipant($event)"
        ></evodesk-desk-filter>
      </ng-container>
    `,
})
export class EvodeskDeskFilterContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        props: {
            labels: [],
            filters: [],
            participants: [],
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private deskState: EvodeskDeskStateService,
    ) {}

    ngOnInit(): void {
        const subscribeToFilters: Function = () => {
            this.deskState.current$.pipe(
                takeUntil(this.ngOnDestroy$),
                map(s => s.current.filters),
                filter(s => !!s),
                distinctUntilChanged(),
            ).subscribe((filters) => {
                const blacklisted: Array<DeskFilterType> = [
                    DeskFilterType.Project,
                    DeskFilterType.QueryString,
                ];

                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        filters: filters.filter(f => !~blacklisted.indexOf(f.type)),
                        labels: this.deskState.snapshot.labels,
                    },
                };

                this.cdr.detectChanges();
            });
        };

        const subscribeToLabels: Function = () => {
            this.deskState.current$.pipe(
                takeUntil(this.ngOnDestroy$),
                map(s => s.current.labels),
                filter(s => !!s),
                distinctUntilChanged(),
            ).subscribe((labels) => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        labels: labels,
                    },
                };

                this.cdr.detectChanges();
            });
        };

        const subscribeToParticipants: Function = () => {
            this.deskState.current$.pipe(
                takeUntil(this.ngOnDestroy$),
                filter(s => !! s.current.desk && !! s.current.desk.members),
                map(s => s.current.desk.members),
                filter(s => !!s),
                distinctUntilChanged(),
            ).subscribe((participants) => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        participants: participants,
                    },
                };

                this.cdr.detectChanges();
            });
        };

        subscribeToFilters();
        subscribeToLabels();
        subscribeToParticipants();
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    reset(): void {
        this.deskState.dispatch({
            type: EvodeskDeskAction.FiltersReset,
        });
    }

    toggleLabel(labelId: DeskLabelId): void {
        const f: IDeskFilters<DeskFilterLabelOptions> = this.deskState.snapshot.filters.filter(ff => ff.type === DeskFilterType.Label && ff.options.labelId === labelId)[0];

        if (f) {
            this.deskState.dispatch({
                type: EvodeskDeskAction.FiltersDisableLabelFilter,
                payload: {
                    labelIds: [labelId],
                },
            });
        } else {
            this.deskState.dispatch({
                type: EvodeskDeskAction.FiltersEnableLabelFilter,
                payload: {
                    labelIds: [labelId],
                },
            });
        }
    }

    toggleDeadline(variant: DeskFilterDeadlineVariant): void {
        const f: IDeskFilters<DeskFilterDeadlineOptions> = this.deskState.snapshot.filters.filter(ff => ff.type === DeskFilterType.Deadline && ff.options.variant === variant)[0];

        if (f) {
            this.deskState.dispatch({
                type: EvodeskDeskAction.FiltersDisableDeadlineFilter,
                payload: {
                    variant: variant,
                },
            });
        } else {
            this.deskState.dispatch({
                type: EvodeskDeskAction.FiltersEnableDeadlineFilter,
                payload: {
                    variant: variant,
                },
            });
        }
    }

    toggleParticipant(participantId: DeskParticipantId): void {
        const f: IDeskFilters<DeskFilterParticipantOptions> = this.deskState.snapshot.filters.filter(ff => ff.type === DeskFilterType.Participant && ff.options.participantId === participantId)[0];

        if (f) {
            this.deskState.dispatch({
                type: EvodeskDeskAction.FiltersDisableParticipantFilter,
                payload: {
                    participantId: participantId,
                },
            });
        } else {
            this.deskState.dispatch({
                type: EvodeskDeskAction.FiltersEnableParticipantFilter,
                payload: {
                    participantId: participantId,
                },
            });
        }
    }
}

