import {AfterViewChecked, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild, ViewContainerRef} from '@angular/core';
import {MatMenu} from '@angular/material';
import {BreakpointObserver} from '@angular/cdk/layout';
import {CdkDragEnd, CdkDragDrop} from '@angular/cdk/drag-drop';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {evodeskDeskMobileBreakpoint} from '../../../configs/EvodeskDesk.constants';

import {proxy} from '../../../../../../functions/proxy.function';

import {DeskColumnComponentProps, EvodeskDeskColumnComponent, EvodeskDeskColumnComponentCdkDropData as CdkDropData} from './EvodeskDeskColumn.component';

import {deskCardCanBeAddedTo, DeskColumnDefinitionModel, DeskColumnId, DeskColumnType, doneColumns, failedColumns} from '../../../../../app/EvodeskRESTApi/models/DeskColumnDefinition.model';
import {DeskCardId, DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskAction, EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {EvodeskDeskMoveCardsService} from '../../../services/EvodeskDeskMoveCards.service';

interface State {
    ready: boolean;
    isMobile: boolean;
    props?: DeskColumnComponentProps;
}

const breakpointMobile: number = evodeskDeskMobileBreakpoint;

@Component({
    selector: 'evodesk-desk-column-container',
    template: `
      <ng-container *ngIf="state.ready">
        <mat-menu #contextMenu>
          <evodesk-desk-column-context-menu-container
            [column]="column"
            [columnCards]="state.props.cards"
            [displayHeader]="state.isMobile"
            [columnViewContainerRef]="viewContainerRef"
          ></evodesk-desk-column-context-menu-container>
        </mat-menu>
        <evodesk-desk-column
          #view
          [props]="state.props"
          (addButtonClick)="openAddCardForm()"
          (addFormClose)="closeAddCardForm()"
          (openContextMenu)="openContextMenu()"
          (cdkDragStarted)="cdkDragStarted()"
          (cdkDragEnded)="cdkDragEnded($event)"
          (cdkDropListDropped)="cdkDropListDropped($event)"
        ></evodesk-desk-column>
      </ng-container>
    `,
    styles: [
        ':host { height: 100% }',
    ],
})
export class EvodeskDeskColumnContainerComponent implements OnChanges, OnInit, OnDestroy, AfterViewChecked
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    @Input() cards: Array<DeskCardModel>;
    @Input() column: DeskColumnDefinitionModel;
    @Input() columns: Array<DeskColumnDefinitionModel>;

    @ViewChild('view') view: EvodeskDeskColumnComponent;
    @ViewChild('contextMenu', { read: MatMenu }) contextMenu: MatMenu;

    public state: State = {
        ready: false,
        isMobile: true,
        props: {
            cards: [],
            columns: [],
            column: undefined,
            display: undefined,
            displayAddForm: false,
            displayStatsDone: false,
            displayStatsFailed: false,
            isEdgeScrollEnabled: false,
            isEdgeScrollDragEnabled: true,
            contextMenu: this.contextMenu,
            isContextMenuEnabled: true,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        public viewContainerRef: ViewContainerRef,
        private breakpointObserver: BreakpointObserver,
        private deskState: EvodeskDeskStateService,
        private moveService: EvodeskDeskMoveCardsService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.column) {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    cards: this.cards,
                    column: this.column,
                    display: (() => {
                        if (!!~deskCardCanBeAddedTo.indexOf(this.column.type)) {
                            return 'add';
                        } else if (!!~doneColumns.indexOf(this.column.type)) {
                            return 'done';
                        } else if (!!~failedColumns.indexOf(this.column.type)) {
                            return 'failed';
                        } else {
                            return undefined;
                        }
                    })(),
                    isContextMenuEnabled: (() => {
                        if (!!~[
                            DeskColumnType.Done,
                            DeskColumnType.Failed,
                        ].indexOf(this.column.type)) {
                            return this.cards.length > 0;
                        } else {
                            return true;
                        }
                    })(),
                },
            };
        }

        if (changes['columns'] && Array.isArray(this.columns)) {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    columns: this.columns,
                },
            };
        }
    }

    ngOnInit(): void {
        this.breakpointObserver.observe(`(max-width: ${breakpointMobile}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this.state = {
                ...this.state,
                isMobile: true,
            };
        });

        this.breakpointObserver.observe(`(min-width: ${breakpointMobile + 1}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this.state = {
                ...this.state,
                isMobile: false,
            };
        });

        this.deskState.side$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((action) => {
            switch (action.type) {
                case EvodeskDeskAction.AddCard:
                    if (action.payload.formData.column === this.column.id) {
                        this.view.scrollToBot();
                        this.cdr.detectChanges();
                    }

                    break;

                case EvodeskDeskAction.DragCardStart:
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            isEdgeScrollEnabled: true,
                        },
                    };

                    this.cdr.detectChanges();
                    break;

                case EvodeskDeskAction.DragCardEnd:
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            isEdgeScrollEnabled: false,
                        },
                    };

                    this.cdr.detectChanges();
                    break;

                case EvodeskDeskAction.OpenAddCardForm:
                    if (!!this.column && action.payload.columnId === this.column.id) {
                        this.state = {
                            ...this.state,
                            props: {
                                ...this.state.props,
                                displayAddForm: true,
                            },
                        };

                        this.cdr.detectChanges();
                    } else {
                        this.state = {
                            ...this.state,
                            props: {
                                ...this.state.props,
                                displayAddForm: false,
                            },
                        };

                        this.cdr.detectChanges();
                    }

                    break;

                case EvodeskDeskAction.CloseAddCardForm:
                    if (this.state.props.displayAddForm) {
                        this.state = {
                            ...this.state,
                            props: {
                                ...this.state.props,
                                displayAddForm: false,
                            },
                        };

                        this.cdr.detectChanges();
                    }

                    break;
            }
        });

        this.deskState.side$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((action) => {
            if (!!~[
                EvodeskDeskAction.DragCardStart,
                EvodeskDeskAction.OpenDeskColumnContextMenu,
                EvodeskDeskAction.EditCard,
                EvodeskDeskAction.OpenCard,
            ].indexOf(action.type)) {
                if (this.state.props.displayAddForm) {
                    this.closeAddCardForm();
                }

                this.cdr.detectChanges();
            }
        });
    }

    ngAfterViewChecked(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                contextMenu: this.contextMenu,
            },
        };
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    openAddCardForm(): void {
        this.deskState.dispatch({
            type: EvodeskDeskAction.OpenAddCardForm,
            payload: {
                columnId: this.column.id,
            },
        });
    }

    closeAddCardForm(): void {
        this.deskState.dispatch({
            type: EvodeskDeskAction.CloseAddCardForm,
        });
    }

    openContextMenu(): void {
        this.view.contextMenuTrigger.openMenu();
    }

    cdkDragStarted(): void {
        this.deskState.dispatch({
            type: EvodeskDeskAction.DragCardStart,
        });
    }

    cdkDragEnded($event: CdkDragEnd<CdkDropData>): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                cards: this.state.props.cards.slice().filter(value => {
                    return value.id !== $event.source.data.card.id;
                }),
            },
        };

        this.deskState.dispatch({
            type: EvodeskDeskAction.DragCardEnd,
            payload: {
                card: $event.source.data.card,
                origColumnId: $event.source.data.origColumnId,
            },
        });
    }

    cdkDropListDropped($event: CdkDragDrop<CdkDropData>): void {
        let counter: number = 0;

        const data: CdkDropData = $event.item.data;
        const cardId: DeskCardId = data.card.id;
        const newColumnId: DeskColumnId = this.column.id;
        const newIndex: number = $event.currentIndex;
        const newCards: Array<DeskCardModel> = this.state.props.cards.slice();

        newCards.push(data.card);

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                cards: newCards.map(value => {
                    if (counter === newIndex) {
                        counter++;
                    }
                    if (value.id === cardId) {
                        return {
                            ...value,
                            order: newIndex,
                            column: newColumnId,
                        };
                    }
                    return {
                        ...value,
                        order: counter++,
                    };
                }).sort((a, b) => {
                    return a.order - b.order;
                }),
            },
        };

        this.deskState.dispatch({
            type: EvodeskDeskAction.DropCard,
            payload: {
                cards: this.state.props.cards,
            },
        });

        if ((data.origColumnId !== this.view.cdkColumnId) || ($event.currentIndex !== $event.previousIndex)) {
            if (this.deskState.snapshot.sourceCards.length !== this.deskState.snapshot.filteredCards.length) {
                proxy(this.moveService.moveCardInFilteredSet(cardId, newColumnId, newIndex)).subscribe();
            } else {
                proxy(this.moveService.moveCard(cardId, newColumnId, newIndex)).subscribe();
            }
        }
    }
}

