import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, NgZone, OnDestroy, Output, ViewChild} from '@angular/core';
import {MatMenu, MatMenuTrigger} from '@angular/material';
import {CdkDragEnd, CdkDragDrop} from '@angular/cdk/drag-drop';

import {Subject} from 'rxjs';
import {take, takeUntil} from 'rxjs/operators';

import {DeskColumnDefinitionModel, DeskColumnType} from '../../../../../app/EvodeskRESTApi/models/DeskColumnDefinition.model';
import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {defaultOptions, EvodeskEdgeScrollDirectiveOptions} from '../../../../../app/EvodeskApp/directives/EvodeskEdgeScroll.directive';
import {EvodeskDeskAddFormContainerComponent} from '../EvodeskDeskAddForm/EvodeskDeskAddFormContainer.component';

export interface DeskColumnComponentProps
{
    cards: Array<DeskCardModel>;
    column: DeskColumnDefinitionModel;
    columns: Array<DeskColumnDefinitionModel>;
    display: undefined | 'add' | 'done' | 'failed';
    displayStatsDone: boolean;
    displayStatsFailed: boolean;
    displayAddForm: boolean;
    isEdgeScrollDragEnabled: boolean;
    isEdgeScrollEnabled: boolean;
    contextMenu?: MatMenu;
    isContextMenuEnabled: boolean;
}

interface CdkDropData {
    card: DeskCardModel;
    origColumn: DeskColumnDefinitionModel;
    origColumnId: string;
}

interface State
{
    destroyed: boolean;
    hasScroll: boolean;
    edgeScrollOptions: EvodeskEdgeScrollDirectiveOptions;
}

export {CdkDropData as EvodeskDeskColumnComponentCdkDropData};

@Component({
    selector: 'evodesk-desk-column',
    templateUrl: './EvodeskDeskColumn.component.pug',
    styleUrls: [
        './EvodeskDeskColumn.component.scss',
    ],
})
export class EvodeskDeskColumnComponent implements OnDestroy, AfterViewInit
{
    private stickToCard$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    @Input() props: DeskColumnComponentProps;

    @Output('addButtonClick') addButtonClickEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('addFormClose') addFormCloseEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('addedCard') addedCardEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openContextMenu') openContextMenuEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output('cdkDragStarted') cdkDragStartedEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('cdkDragEnded') cdkDragEndedEvent: EventEmitter<CdkDragEnd<CdkDropData>> = new EventEmitter<CdkDragEnd<CdkDropData>>();
    @Output('cdkDropListDropped') cdkDropListDroppedEvent: EventEmitter<CdkDragDrop<CdkDropData>> = new EventEmitter<CdkDragDrop<CdkDropData>>();

    @ViewChild('contents') contentsRef: ElementRef;
    @ViewChild('contextMenuTrigger', { read: MatMenuTrigger }) contextMenuTrigger: MatMenuTrigger;

    @ViewChild('addCardForm') addCardForm: EvodeskDeskAddFormContainerComponent;

    @ViewChild('shadowTop') shadowTopRef: ElementRef;
    @ViewChild('shadowTopBg') shadowTopBgRef: ElementRef;
    @ViewChild('shadowBottom') shadowBottomRef: ElementRef;

    public state: State = {
        destroyed: false,
        hasScroll: true,
        edgeScrollOptions: {
            ...defaultOptions,
            enabled: 'y',
            dragDelayAndPrioritize: true,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private ngZone: NgZone,
    ) {}

    ngAfterViewInit(): void {
        const watchShadows: Function = () => {
            this.ngZone.runOutsideAngular(() => {
                const tick: Function = () => {
                    if (this.contentsRef) {
                        const contents: HTMLElement = this.contentsRef.nativeElement as HTMLElement;
                        const hasScroll: boolean = contents.scrollHeight > contents.clientHeight;

                        if (hasScroll && ! this.state.hasScroll) {
                            this.state = {
                                ...this.state,
                                hasScroll: true,
                            };

                            [
                                this.shadowTopRef,
                                this.shadowTopBgRef,
                                this.shadowBottomRef,
                            ].forEach(e => {
                                if (e && e.nativeElement) {
                                    (e.nativeElement as HTMLElement).style.display = 'block';
                                }
                            });

                            this.cdr.detectChanges();
                        } else if (! hasScroll && this.state.hasScroll) {
                            this.state = {
                                ...this.state,
                                hasScroll: false,
                            };

                            [
                                this.shadowTopRef,
                                this.shadowTopBgRef,
                                this.shadowBottomRef,
                            ].forEach(e => {
                                if (e && e.nativeElement) {
                                    (e.nativeElement as HTMLElement).style.display = 'none';
                                }
                            });

                            this.cdr.detectChanges();
                        }

                        if (hasScroll && this.shadowTopRef && this.contentsRef) {
                            const elem: HTMLElement = this.contentsRef.nativeElement as HTMLElement;
                            const stElem: HTMLElement = this.shadowTopRef.nativeElement as HTMLElement;

                            stElem.style.opacity = (elem.scrollTop > 0 ? 1 : 0).toString();
                        }
                    }

                    window.requestAnimationFrame(() => {
                        if (! this.state.destroyed) {
                            tick();
                        }
                    });
                };

                tick();
            });
        };

        watchShadows();
    }

    ngOnDestroy(): void {
        this.state = {
            ...this.state,
            destroyed: true,
        };

        this.stickToCard$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    get containerNgClass(): any {
        if (this.contentsRef) {
            const contents: HTMLElement = this.contentsRef.nativeElement as HTMLElement;

            return {
                'has-scroll': contents.scrollHeight > contents.clientHeight,
            };
        }
    }

    cdkDropListDropped($event: CdkDragDrop<CdkDropData>): void {
        this.cdkDropListDroppedEvent.emit($event);
    }

    cdkDropListDroppedAtBottom($event: CdkDragDrop<CdkDropData>): void {
        $event.currentIndex = this.props.cards.length;
        this.cdkDropListDroppedEvent.emit($event);
    }

    cdkDragStarted(): void {
        this.cdkDragStartedEvent.emit(undefined);
    }

    cdkDragEnded($event: CdkDragEnd<CdkDropData>): void {
        this.cdkDragEndedEvent.emit($event);
    }

    openContextMenu(): void {
        this.openContextMenuEvent.emit(undefined);
    }

    trackByDeskCardId(index, item: DeskCardModel): any {
        return item.id;
    }

    onAddButtonClick(): void {
        if (this.props.displayAddForm) {
            this.addCardForm.sendSubmitAction();
        } else {
            this.addButtonClickEvent.emit(undefined);

            this.ngZone.runOutsideAngular(() => {
                setTimeout(() => {
                    this.scrollToBot();
                });
            });

            this.scrollToBot();
        }
    }

    addedCard(): void {
        this.addedCardEvent.emit(undefined);
    }

    closeAddCardForm(): void {
        this.addFormCloseEvent.emit(undefined);
    }

    scrollToBot(): void {
        if (this.contentsRef && this.contentsRef.nativeElement) {
            window.requestAnimationFrame(() => {
                const element: HTMLElement = this.contentsRef.nativeElement;

                element.scrollTop = element.scrollHeight + element.clientHeight;
            });
        }
    }

    scrollToCard(element: HTMLElement): void {
        const container: HTMLElement = this.contentsRef.nativeElement as HTMLElement;

        const offsetTop: number = element.offsetTop;
        const clientHeight: number = element.clientHeight;
        const scrollTop: number = container.scrollTop;
        const containerClientHeight: number = container.clientHeight;
        const addButtonHeight: number = 49;
        const boxShadow: number = 6;

        if (scrollTop > offsetTop) {
            container.scrollTop = offsetTop;
        } else if ((offsetTop + clientHeight + addButtonHeight + boxShadow) > (scrollTop + containerClientHeight)) {
            container.scrollTop = (offsetTop + clientHeight + addButtonHeight + boxShadow) - containerClientHeight;
        }
    }

    stickToCard(element: HTMLElement): void {
        let stop: boolean = false;

        this.ngZone.runOutsideAngular(() => {
            this.stickToCard$.pipe(take(1), takeUntil(this.ngOnDestroy$)).subscribe(() => {
                stop = true;
            });

            const tick: Function = () => {
                this.scrollToCard(element);

                if (! stop) {
                    window.requestAnimationFrame(() => {
                        tick();
                    });
                }
            };

            tick();
        });
    }

    stopStickToCard(): void {
        this.stickToCard$.next(undefined);
    }

    cdkDropData(card: DeskCardModel): CdkDropData {
        return {
            card: card,
            origColumn: this.props.column,
            origColumnId: this.cdkColumnId,
        };
    }

    onAfterCardCreated(): void {
        setTimeout(() => {
            this.scrollToBot();
        }, 100);
    }

    get isDoneColumn(): boolean {
        return this.props.column.type === DeskColumnType.Done;
    }

    get isFailedColumn(): boolean {
        return this.props.column.type === DeskColumnType.Failed;
    }

    get cdkColumnId(): string {
        return `evodeskDeskColumn-${this.props.column.id}`;
    }

    get cdkColumnOverlayId(): string {
        return `evodeskDeskColumn-${this.props.column.id}-overlay`;
    }

    get cdkConnectedColumns(): Array<string> {
        return [
            ...this.props.columns.map(c => `evodeskDeskColumn-${c.id}`),
            ...this.props.columns.map(c => `evodeskDeskColumn-${c.id}-overlay`),
        ];
    }

    get doneText(): string {
        const plural: string = (() => {
            const num: number = this.props.cards.length;
            const lastDigit: number = parseInt(num.toString()[num.toString().length - 1], 10);

            if (num === 0) {
                return 'P0';
            } else if (num === 1) {
                return 'P1';
            } else if (!!~[2, 3, 4].indexOf(num)) {
                return 'P234';
            } else if (num >= 5 && num <= 19) {
                return 'P56789D';
            } else if (num >= 20 && !!~[0, 1, 5, 6, 7, 8, 9].indexOf(lastDigit)) {
                return 'PD1567890';
            } else {
                return 'PD234';
            }
        })();

        return `EvodeskDesk.components.common.EvodeskDeskColumn.Done.Plural.${plural}`;
    }

    get failedText(): string {
        const plural: string = (() => {
            const num: number = this.props.cards.length;
            const lastDigit: number = parseInt(num.toString()[num.toString().length - 1], 10);

            if (num === 0) {
                return 'P0';
            } else if (num === 1) {
                return 'P1';
            } else if (!!~[2, 3, 4].indexOf(num)) {
                return 'P234';
            } else if (num >= 5 && num <= 19) {
                return 'P56789D';
            } else if (num >= 20 && !!~[0, 1, 5, 6, 7, 8, 9].indexOf(lastDigit)) {
                return 'PD1567890';
            } else {
                return 'PD234';
            }
        })();

        return `EvodeskDesk.components.common.EvodeskDeskColumn.Failed.Plural.${plural}`;
    }
}
