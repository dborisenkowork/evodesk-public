import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskCardChecklistItemModel, DeskCardChecklistModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardChecklist.model';

import {EvodeskDeskCardChecklistComponentCreateChecklistItemEvent as CreateChecklistItemEvent, EvodeskDeskCardChecklistComponentProps, EvodeskDeskCardChecklistComponentUpdateChecklistItemEvent as UpdateChecklistItemEvent} from './EvodeskDeskCardChecklist.component';

import {EvodeskDeskCardChecklistService} from '../../../services/EvodeskDeskCardChecklist.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskCardChecklistComponentProps;
}

@Component({
    selector: 'evodesk-desk-card-checklist-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <ng-container *ngIf="!!state && state.ready">
            <evodesk-desk-card-checklist
              [props]="state.props"
              (deleteChecklist)="deleteChecklist()"
              (createChecklistItem)="createChecklistItem($event)"
              (updateChecklistItem)="updateChecklistItem($event)"
              (toggleChecklistItem)="toggleChecklistItem($event)"
              (deleteChecklistItem)="deleteChecklistItem($event)"
            ></evodesk-desk-card-checklist>
        </ng-container>
    `,
})
export class EvodeskDeskCardChecklistContainerComponent implements OnChanges, OnDestroy
{
    @Input() card: DeskCardModel;
    @Input() checklist: DeskCardChecklistModel;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: undefined,
    };

    constructor(
        private service: EvodeskDeskCardChecklistService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['card']) {
            if (this.card && this.checklist) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        card: this.card,
                        checklist: this.checklist,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    deleteChecklist(): void {
        this.service.deleteChecklist(this.card.id, this.checklist.id).subscribe();
    }

    createChecklistItem($event: CreateChecklistItemEvent): void {
        this.service.addChecklistItem(this.card.id, this.checklist.id, {
            completed: false,
            title: $event.formValue.title,
        }).subscribe();
    }

    updateChecklistItem($event: UpdateChecklistItemEvent): void {
        this.service.updateChecklistItem(this.card.id, this.checklist.id, {
            ...$event.checklistItem,
            title: $event.formValue.title,
        }).subscribe();
    }

    toggleChecklistItem($event: DeskCardChecklistItemModel): void {
        this.service.updateChecklistItem(this.card.id, this.checklist.id, {
            ...$event,
            completed: ! $event.completed,
        }).subscribe();
    }

    deleteChecklistItem($event: DeskCardChecklistItemModel): void {
        this.service.deleteChecklistItem(this.card.id, this.checklist.id, {
            checklistItemId: $event.id,
        }).subscribe();
    }
}
