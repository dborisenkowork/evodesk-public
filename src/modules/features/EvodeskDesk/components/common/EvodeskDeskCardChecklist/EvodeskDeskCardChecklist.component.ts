import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Subject} from 'rxjs';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskCardChecklistItemId, DeskCardChecklistItemModel, DeskCardChecklistModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardChecklist.model';

import {EvodeskConfirmModalService} from '../../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';

interface Props {
    card: DeskCardModel;
    checklist: DeskCardChecklistModel;
}

interface State {
    ready: boolean;
    add: boolean;
    editing: DeskCardChecklistItemId | -1 | undefined;
    itemForm: FormGroup;
    isTitleFocused: boolean;
}

interface ItemFormValue {
    title?: string;
}

interface CreateChecklistItemEvent {
    formValue: ItemFormValue;
}

interface UpdateChecklistItemEvent {
    checklistItem: DeskCardChecklistItemModel;
    formValue: ItemFormValue;
}

export {Props as EvodeskDeskCardChecklistComponentProps};
export {CreateChecklistItemEvent as EvodeskDeskCardChecklistComponentCreateChecklistItemEvent};
export {UpdateChecklistItemEvent as EvodeskDeskCardChecklistComponentUpdateChecklistItemEvent};

@Component({
    selector: 'evodesk-desk-card-checklist',
    templateUrl: './EvodeskDeskCardChecklist.component.pug',
    styleUrls: [
        './EvodeskDeskCardChecklist.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardChecklistComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    @Output('deleteChecklist') deleteChecklistEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('createChecklistItem') createChecklistItemEvent: EventEmitter<CreateChecklistItemEvent> = new EventEmitter<CreateChecklistItemEvent>();
    @Output('updateChecklistItem') updateChecklistItemEvent: EventEmitter<UpdateChecklistItemEvent> = new EventEmitter<UpdateChecklistItemEvent>();
    @Output('toggleChecklistItem') toggleChecklistItemEvent: EventEmitter<DeskCardChecklistItemModel> = new EventEmitter<DeskCardChecklistItemModel>();
    @Output('deleteChecklistItem') deleteChecklistItemEvent: EventEmitter<DeskCardChecklistItemModel> = new EventEmitter<DeskCardChecklistItemModel>();

    @ViewChild('titleInput') titleInputRef: ElementRef;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        add: false,
        editing: undefined,
        itemForm: this.fb.group({
            title: [null, [Validators.required]],
        }),
        isTitleFocused: false,
    };

    constructor(
        private fb: FormBuilder,
        private uiConfirm: EvodeskConfirmModalService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get checklistItems(): Array<DeskCardChecklistItemModel> {
        if (this.state.add) {
            return [...this.props.checklist.items, <any>{
                id: -1,
            }];
        } else {
            return this.props.checklist.items;
        }
    }

    get itemFormValue(): ItemFormValue {
        return this.state.itemForm.value;
    }

    get percents(): number {
        const total: number = this.checklistItems.length;
        const completed: number = this.checklistItems.filter(i => i.completed).length;

        return Math.min(Math.floor((100 / total) * completed), 100);
    }

    get fillNgStyle(): any {
        return {
            width: `${this.percents}%`,
        };
    }

    add(): void {
        this.state = {
            ...this.state,
            add: true,
            editing: -1,
        };

        this.state.itemForm.reset();

        window.requestAnimationFrame(() => {
            (this.titleInputRef.nativeElement as HTMLElement).focus();
        });
    }

    edit(label: DeskCardChecklistItemModel): void {
        this.state = {
            ...this.state,
            add: false,
            editing: label.id,
        };

        this.state.itemForm.reset();
        this.state.itemForm.patchValue(<ItemFormValue>{
            title: label.title,
        });

        window.requestAnimationFrame(() => {
            (this.titleInputRef.nativeElement as HTMLElement).focus();
        });
    }

    submit(): void {
        if (this.state.itemForm.valid) {
            if (this.state.add) {
                this.createChecklistItemEvent.emit({
                    formValue: {
                        ...this.itemFormValue,
                    },
                });
            } else if (this.state.editing > 0) {
                this.updateChecklistItemEvent.emit({
                    checklistItem: this.props.checklist.items.filter(i => i.id === this.state.editing)[0],
                    formValue: {
                        ...this.itemFormValue,
                    },
                });

                this.state = {
                    ...this.state,
                    editing: undefined,
                };
            }

            this.state.itemForm.reset();
        }
    }

    cancel(): void {
        this.state = {
            ...this.state,
            add: false,
            editing: undefined,
        };

        this.state.itemForm.reset();
    }

    deleteChecklist(): void {
        const prefix: string = 'EvodeskDesk.components.common.EvodeskDeskCardChecklist.ConfirmDelete';

        this.uiConfirm.open({
            title: {
                text: this.props.checklist.name,
                translate: false,
            },
            text: {
                text: `${prefix}.Text`,
                translate: true,
                replaces: [this.props.checklist.name],
            },
            confirmButton: {
                text: `${prefix}.Confirm`,
                translate: true,
            },
            cancelButton: {
                text: `${prefix}.Cancel`,
                translate: true,
            },
            closeOnNavigation: true,
            confirm: (() => {
                this.deleteChecklistEvent.emit(undefined);
            }),
        }).subscribe();
    }

    toggleChecklistItem(item: DeskCardChecklistItemModel): void {
        this.toggleChecklistItemEvent.emit(item);
    }

    deleteChecklistItem(item: DeskCardChecklistItemModel): void {
        this.deleteChecklistItemEvent.emit(item);
    }

    trackByChecklistItemId(index: number, item: DeskCardChecklistItemModel): any {
        return item.id;
    }

    onTitleInputFocus(): void {
        this.state = {
            ...this.state,
            isTitleFocused: true,
        };
    }

    onTitleInputBlur(): void {
        if ((this.itemFormValue.title || '').length === 0) {
            this.state = {
                ...this.state,
                add: false,
                editing: undefined,
                isTitleFocused: false,
            };
        } else {
            this.state = {
                ...this.state,
                isTitleFocused: false,
            };
        }
    }
}
