import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild} from '@angular/core';
import {CdkDragDrop, CdkDragEnd} from '@angular/cdk/drag-drop';

import {Subject} from 'rxjs';

import {DeskProjectModel} from '../../../../../app/EvodeskRESTApi/models/DeskProject.model';

import {defaultOptions, EvodeskEdgeScrollDirectiveOptions} from '../../../../../app/EvodeskApp/directives/EvodeskEdgeScroll.directive';

interface Props {
    projects: Array<DeskProjectModel>;
    isEdgeScrollDragEnabled: boolean;
    isEdgeScrollEnabled: boolean;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskDeskProjectsComponentProps};

interface State
{
    ready: boolean;
    destroyed: boolean;
    hasScroll: boolean;
    edgeScrollOptions: EvodeskEdgeScrollDirectiveOptions;
}

@Component({
    selector: 'evodesk-desk-projects',
    templateUrl: './EvodeskDeskProjects.component.pug',
    styleUrls: ['./EvodeskDeskProjects.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskProjectsComponent implements OnChanges, AfterViewInit, OnDestroy
{
    @Input() props: Props;

    @Output('addProject') addProjectEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output('cdkDragStarted') cdkDragStartedEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('cdkDragEnded') cdkDragEndedEvent: EventEmitter<CdkDragEnd<DeskProjectModel>> = new EventEmitter<CdkDragEnd<DeskProjectModel>>();
    @Output('cdkDropListDropped') cdkDropListDroppedEvent: EventEmitter<CdkDragDrop<DeskProjectModel>> = new EventEmitter<CdkDragDrop<DeskProjectModel>>();

    @ViewChild('contents') contentsRef: ElementRef;

    @ViewChild('shadowTop') shadowTopRef: ElementRef;
    @ViewChild('shadowTopBg') shadowTopBgRef: ElementRef;
    @ViewChild('shadowBottom') shadowBottomRef: ElementRef;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        destroyed: false,
        hasScroll: true,
        edgeScrollOptions: {
            ...defaultOptions,
            enabled: 'y',
            dragDelayAndPrioritize: true,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private ngZone: NgZone,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngAfterViewInit(): void {
        const watchShadows: Function = () => {
            this.ngZone.runOutsideAngular(() => {
                const tick: Function = () => {
                    if (this.contentsRef) {
                        const contents: HTMLElement = this.contentsRef.nativeElement as HTMLElement;
                        const hasScroll: boolean = contents.scrollHeight > contents.clientHeight;

                        if (hasScroll && ! this.state.hasScroll) {
                            this.state = {
                                ...this.state,
                                hasScroll: true,
                            };

                            [
                                this.shadowTopRef,
                                this.shadowTopBgRef,
                                this.shadowBottomRef,
                            ].forEach(e => {
                                if (e && e.nativeElement) {
                                    (e.nativeElement as HTMLElement).style.display = 'block';
                                }
                            });

                            this.cdr.detectChanges();
                        } else if (! hasScroll && this.state.hasScroll) {
                            this.state = {
                                ...this.state,
                                hasScroll: false,
                            };

                            [
                                this.shadowTopRef,
                                this.shadowTopBgRef,
                                this.shadowBottomRef,
                            ].forEach(e => {
                                if (e && e.nativeElement) {
                                    (e.nativeElement as HTMLElement).style.display = 'none';
                                }
                            });

                            this.cdr.detectChanges();
                        }

                        if (hasScroll && this.shadowTopRef && this.contentsRef) {
                            const elem: HTMLElement = this.contentsRef.nativeElement as HTMLElement;
                            const stElem: HTMLElement = this.shadowTopRef.nativeElement as HTMLElement;

                            stElem.style.opacity = (elem.scrollTop > 0 ? 1 : 0).toString();
                        }
                    }

                    window.requestAnimationFrame(() => {
                        if (! this.state.destroyed) {
                            tick();
                        }
                    });
                };

                tick();
            });
        };

        watchShadows();
    }

    ngOnDestroy(): void {
        this.state = {
            ...this.state,
            destroyed: true,
        };

        this.ngOnDestroy$.next(undefined);
    }

    get containerNgClass(): any {
        if (this.contentsRef) {
            const contents: HTMLElement = this.contentsRef.nativeElement as HTMLElement;

            return {
                'has-scroll': contents.scrollHeight > contents.clientHeight,
            };
        }
    }

    get cdkColumnId(): string {
        return `evodeskDeskProjectColumn`;
    }

    trackByDeskProjectId(index, item: DeskProjectModel): any {
        return item.id;
    }

    onAddButtonClick(): void {
        this.addProjectEvent.emit(undefined);
    }

    cdkDragStarted(): void {
        this.cdkDragStartedEvent.emit(undefined);
    }

    cdkDragEnded($event: CdkDragEnd<DeskProjectModel>): void {
        this.cdkDragEndedEvent.emit($event);
    }

    cdkDropListDropped($event: CdkDragDrop<DeskProjectModel>): void {
        this.cdkDropListDroppedEvent.emit($event);
    }
}
