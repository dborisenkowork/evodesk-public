import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {CdkDragDrop, CdkDragEnd} from '@angular/cdk/drag-drop';


import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {proxy} from '../../../../../../functions/proxy.function';

import {DeskProjectModel} from '../../../../../app/EvodeskRESTApi/models/DeskProject.model';

import {EvodeskDeskProjectsComponentProps} from './EvodeskDeskProjects.component';

import {EvodeskDeskAction, EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {EvodeskDeskProjectsService} from '../../../services/EvodeskDeskProjects.service';
import {EvodeskDeskMoveCardsService} from '../../../services/EvodeskDeskMoveCards.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskProjectsComponentProps;
}

@Component({
    selector: 'evodesk-desk-projects-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-projects [props]="state.props" (addProject)="addProject()" (cdkDragStarted)="cdkDragStarted()"
          (cdkDragEnded)="cdkDragEnded($event)" (cdkDropListDropped)="cdkDropListDropped($event)"></evodesk-desk-projects>
      </ng-container>
    `,
})
export class EvodeskDeskProjectsContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private viewContainerRef: ViewContainerRef,
        private deskState: EvodeskDeskStateService,
        private service: EvodeskDeskProjectsService,
        private moveService: EvodeskDeskMoveCardsService,
    ) {}

    ngOnInit(): void {
        const subscribeToProjects: Function = () => {
            this.deskState.current$.pipe(
                takeUntil(this.ngOnDestroy$),
                map(s => s.current.projects),
                filter(i => !! i),
                distinctUntilChanged(),
            ).subscribe((projects) => {
                const newProjects: Array<DeskProjectModel> = projects.sort((a, b) => a.order - b.order);

                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        ...this.state.props,
                        projects: newProjects,
                    },
                };

                this.cdr.detectChanges();
            });
        };

        const subscribeToStateSideEffects: Function = () => {
            this.deskState.side$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((action) => {
                switch (action.type) {
                    case EvodeskDeskAction.DragCardStart:
                    case EvodeskDeskAction.DragProjectStart:
                        this.state = {
                            ...this.state,
                            props: {
                                ...this.state.props,
                                isEdgeScrollEnabled: true,
                            },
                        };

                        this.cdr.detectChanges();
                        break;

                    case EvodeskDeskAction.DragCardEnd:
                    case EvodeskDeskAction.DragProjectEnd:
                        this.state = {
                            ...this.state,
                            props: {
                                ...this.state.props,
                                isEdgeScrollEnabled: false,
                            },
                        };

                        this.cdr.detectChanges();
                        break;
                }
            });
        };

        subscribeToProjects();
        subscribeToStateSideEffects();
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    addProject(): void {
        this.service.askForAddProject(this.viewContainerRef);
    }

    cdkDragStarted(): void {
        this.deskState.dispatch({
            type: EvodeskDeskAction.DragProjectStart,
        });
    }

    cdkDragEnded($event: CdkDragEnd<DeskProjectModel>): void {
        this.deskState.dispatch({
            type: EvodeskDeskAction.DragProjectEnd,
            payload: {
                project: $event.source.data,
            },
        });
    }

    cdkDropListDropped($event: CdkDragDrop<DeskProjectModel>): void {
        let counter: number = 1;
        const project: DeskProjectModel = $event.item.data;
        const newIndex: number = $event.currentIndex;

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                projects: this.state.props.projects.map(value => {
                    if (counter === newIndex + 1) {
                        counter++;
                    }
                    if (value.id === project.id) {
                        return {
                            ...value,
                            order: newIndex + 1,
                        };
                    }
                    return {
                        ...value,
                        order: counter++,
                    };
                }).sort((a, b) => {
                    return a.order - b.order;
                }),
            },
        };

        proxy(this.moveService.moveProject(project.id, newIndex)).subscribe();
    }
}
