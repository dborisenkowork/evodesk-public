import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import {environment} from '../../../../../../environments/environment.config';

import {EvodeskDeskCopyLinkComponentProps, EvodeskDeskCopyLinkComponentVariant} from './EvodeskDeskCopyLink.component';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

interface Props {
    card: DeskCardModel;
    variant: EvodeskDeskCopyLinkComponentVariant;
}

interface State {
    ready: boolean;
    props?: EvodeskDeskCopyLinkComponentProps;
}

@Component({
    selector: 'evodesk-desk-copy-link-container',
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-copy-link [props]="state.props" (close)="closeEvent.emit($event)"></evodesk-desk-copy-link>
      </ng-container>
    `,
})
export class EvodeskDeskCopyLinkContainerComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props && this.props.card) {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    variant: this.props.variant,
                    linkUrl: `${environment.serverHostName}/evodesk-desk/?card=${this.props.card.id}`,
                },
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
