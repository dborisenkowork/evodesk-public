import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';

interface State {
    ready: boolean;
    linkUrlFocused: boolean;
}

interface Props {
    variant: Variant;
    linkUrl: string;
}

type Variant = 'modal' | 'control' | 'mat-menu';

export {Props as EvodeskDeskCopyLinkComponentProps};
export {Variant as EvodeskDeskCopyLinkComponentVariant};

@Component({
    selector: 'evodesk-desk-copy-link',
    templateUrl: './EvodeskDeskCopyLink.component.pug',
    styleUrls: [
        './EvodeskDeskCopyLink.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCopyLinkComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('input') inputRef: ElementRef;

    public state: State = {
        ready: false,
        linkUrlFocused: false,
    };

    constructor(
        private ngZone: NgZone,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };

            this.ngZone.runOutsideAngular(() => {
                setTimeout(() => {
                    this.selectAndCopy();
                });
            });
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    get shouldDisplayModalHeader(): boolean {
        return this.props.variant !== 'control';
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    selectAndCopy(): void {
        (this.inputRef.nativeElement as HTMLInputElement).focus();
        (this.inputRef.nativeElement as HTMLInputElement).setSelectionRange(0, this.props.linkUrl.length);

        document.execCommand('Copy');
    }
}
