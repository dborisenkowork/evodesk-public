import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import {EvodeskDeskLabelComponent, EvodeskDeskLabelComponentFormValue, EvodeskDeskLabelComponentProps, EvodeskDeskLabelComponentVariant} from './EvodeskDeskLabel.component';

import {EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {EvodeskDeskLabelService} from '../../../services/EvodeskDeskLabel.service';

import {DeskLabelModel} from '../../../../../app/EvodeskRESTApi/models/DeskLabel.model';
import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

interface State {
    ready: boolean;
    props?: EvodeskDeskLabelComponentProps;
}

@Component({
    selector: 'evodesk-desk-label-container',
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-label
          #view
          [props]="state.props"
          (close)="closeEvent.emit($event)"
          (toggle)="toggle($event)"
          (delete)="delete($event)"
          (create)="create($event)"
          (update)="update($event)"
        ></evodesk-desk-label>
      </ng-container>
    `,
})
export class EvodeskDeskLabelContainerComponent implements OnChanges, OnInit, OnDestroy
{
    @Input() card: DeskCardModel;
    @Input() variant: EvodeskDeskLabelComponentVariant = 'modal';

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('view') view: EvodeskDeskLabelComponent;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private deskState: EvodeskDeskStateService,
        private service: EvodeskDeskLabelService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.card) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }

        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                card: this.card,
                variant: this.variant,
            },
        };
    }

    ngOnInit(): void {
        this.deskState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.labels),
            distinctUntilChanged(),
        ).subscribe((labels) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    availableLabels: labels,
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    toggle(label: DeskLabelModel): void {
        const has: boolean = !!~this.card.labels.map(l => l.id).indexOf(label.id);

        if (has) {
            this.service.excludeLabelFromCard({
                deskId: this.card.deskId,
                cardId: this.card.id,
                labelId: label.id,
            }).subscribe();
        } else {
            this.service.includeLabelToCard({
                deskId: this.card.deskId,
                cardId: this.card.id,
                label: label,
            }).subscribe();
        }
    }

    delete(label: DeskCardModel): void {
        this.service.deleteLabel({
            deskId: this.card.deskId,
            labelId: label.id,
        }).subscribe();
    }

    create(formValue: EvodeskDeskLabelComponentFormValue): void {
        this.service.createNewLabel({
            deskId: this.card.deskId,
            color: formValue.color,
            name: formValue.name,
        }).subscribe();
    }

    update(formValue: EvodeskDeskLabelComponentFormValue): void {
        this.service.editLabel({
            labelId: formValue.id,
            deskId: this.card.deskId,
            color: formValue.color,
            name: formValue.name,
        }).subscribe();
    }
}
