import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {evodeskDeskDefaultPalette} from '../../../configs/EvodeskDesk.constants';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskLabelColorDescription, DeskLabelId, DeskLabelModel} from '../../../../../app/EvodeskRESTApi/models/DeskLabel.model';

import {EvodeskConfirmModalService} from '../../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';

interface State {
    ready: boolean;
    form: FormGroup;
    add: boolean;
    editing: DeskLabelId | -1 | undefined;
    searchForm: FormGroup;
    isSearchFieldFocused: boolean;
}

interface Props {
    card: DeskCardModel;
    variant: Variant;
    availableLabels: Array<DeskLabelModel>;
}

interface FormValue {
    id?: DeskLabelId | null;
    name?: string;
    color?: string;
}

interface SearchFormValue {
    search: string | null;
}

type Variant = 'modal' | 'control' | 'mat-menu';

export {Props as EvodeskDeskLabelComponentProps};
export {Variant as EvodeskDeskLabelComponentVariant};
export {FormValue as EvodeskDeskLabelComponentFormValue};

@Component({
    selector: 'evodesk-desk-label',
    templateUrl: './EvodeskDeskLabel.component.pug',
    styleUrls: [
        './EvodeskDeskLabel.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskLabelComponent implements OnChanges, OnDestroy, AfterViewInit
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('toggle') toggleEvent: EventEmitter<DeskLabelModel> = new EventEmitter<DeskLabelModel>();
    @Output('delete') deleteEvent: EventEmitter<DeskLabelModel> = new EventEmitter<DeskLabelModel>();
    @Output('create') createEvent: EventEmitter<FormValue> = new EventEmitter<FormValue>();
    @Output('update') updateEvent: EventEmitter<FormValue> = new EventEmitter<FormValue>();

    @ViewChild('formNameInput') formNameInput: ElementRef;
    @ViewChild('searchInput') searchInput: ElementRef;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        form: this.fb.group({
            id: [null],
            name: [''],
            color: ['', [Validators.required]],
        }),
        add: false,
        editing: undefined,
        searchForm: this.fb.group({
            search: [null],
        }),
        isSearchFieldFocused: false,
    };

    constructor(
        private fb: FormBuilder,
        private ngZone: NgZone,
        private viewContainerRef: ViewContainerRef,
        private uiConfirm: EvodeskConfirmModalService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props.card && this.props.variant) {
            this.state = {
                ...this.state,
                ready: true,
            };

            if (changes['card']) {
            }
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    ngAfterViewInit(): void {
        this.focusSearchInput();

        setTimeout(() => {
            this.focusSearchInput();
        }, 1500);
    }

    toggle(label: DeskLabelModel): void {
        this.toggleEvent.emit(label);

        this.focusSearchInput();
    }

    add(): void {
        this.state = {
            ...this.state,
            add: true,
            editing: -1,
        };

        this.state.form.reset();

        setTimeout(() => {
            (this.formNameInput.nativeElement as HTMLElement).focus();
        });
    }

    edit(label: DeskLabelModel): void {
        this.state = {
            ...this.state,
            add: false,
            editing: label.id,
        };

        this.state.form.reset();
        this.state.form.patchValue(<FormValue>{
            id: label.id,
            name: label.name,
            color: label.color,
        });

        this.focusFormInput();
    }

    delete(label: DeskLabelModel, labelName: string): void {
        const prefix: string = 'EvodeskDesk.components.common.EvodeskDeskLabel.ConfirmDelete';

        this.uiConfirm.open({
            title: {
                text: labelName,
                translate: false,
            },
            text: {
                text: `${prefix}.Text`,
                translate: true,
                replaces: [labelName],
            },
            confirmButton: {
                text: `${prefix}.Confirm`,
                translate: true,
            },
            cancelButton: {
                text: `${prefix}.Cancel`,
                translate: true,
            },
            closeOnNavigation: true,
            viewContainerRef: this.viewContainerRef,
            confirm: (() => {
                this.deleteEvent.emit(label);
            }),
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }

    cancel(): void {
        this.state.form.reset();

        this.state = {
            ...this.state,
            editing: undefined,
            add: false,
        };

        this.focusSearchInput();
    }

    submit(): void {
        if (this.state.form.valid) {
            if (this.state.add) {
                this.createEvent.emit(this.formValue);
            } else {
                this.updateEvent.emit(this.formValue);
            }

            this.state.form.reset();

            this.state = {
                ...this.state,
                editing: undefined,
                add: false,
            };

            this.focusSearchInput();
        }
    }

    stopSearch(): void {
        this.state.searchForm.patchValue({
            search: null,
        });
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    get searchFormValue(): SearchFormValue {
        return this.state.searchForm.value;
    }

    get shouldDisplayModalHeader(): boolean {
        return true;
    }

    get preset(): Array<DeskLabelColorDescription> {
        return evodeskDeskDefaultPalette;
    }

    presetPinNgStyle(p: DeskLabelColorDescription): any {
        return {
            'background-color': p.color,
        };
    }

    trackByPresetColor(index: number, p: DeskLabelColorDescription): any {
        return p.color;
    }

    get filteredLabels(): Array<DeskLabelModel> {
        if (this.state.add) {
            return [...this.props.availableLabels, <any>{
                id: -1,
            }];
        } else {
            return this.props.availableLabels;
        }
    }

    trackByLabelId(index: number, item: DeskLabelModel): any {
        return item.id;
    }

    labelPinNgStyles(input: DeskLabelModel): any {
        return {
            'background-color': input.id < 0 ? '#EFF0F1' : input.color,
        };
    }

    isActive(label: DeskLabelModel): boolean {
        return !!~this.props.card.labels.map(l => l.id).indexOf(label.id);
    }

    isPresetColor(input: DeskLabelModel): boolean {
        return ! input.name && !!~evodeskDeskDefaultPalette.map(c => c.color).indexOf(input.color);
    }

    presetColorName(input: DeskLabelModel): string {
        return evodeskDeskDefaultPalette.filter(c => c.color === input.color)[0].translate;
    }

    get selectedColorPinNgStyle(): any {
        return {
            'background-color': this.formValue.color ? this.formValue.color : '#EFF0F1',
        };
    }

    setColor(color: string): void {
        this.state.form.patchValue({
            color: color,
        });
    }

    isColorSelected(color: string): boolean {
        return this.formValue.color === color;
    }

    isFiltered(label: DeskLabelModel): boolean {
        if (this.searchFormValue.search) {
            return label.id === -1 || this.state.editing === label.id || !!~(label.name || '').toLocaleLowerCase().indexOf(this.searchFormValue.search.toLocaleLowerCase());
        } else {
            return true;
        }
    }

    focusSearchInput(): void {
        this.ngZone.runOutsideAngular(() => {
            (this.searchInput.nativeElement as HTMLElement).focus();
        });
    }

    focusFormInput(): void {
        this.ngZone.runOutsideAngular(() => {
            setTimeout(() => {
                (this.formNameInput.nativeElement as HTMLElement).focus();
            });
        });
    }
}
