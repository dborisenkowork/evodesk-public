import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import * as moment from 'moment';

import {Observable, Subject} from 'rxjs';
import {take, takeUntil} from 'rxjs/operators';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../../types/Backend.types';

import {DeskCardLogRecordModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardLogRecord.model';

import {EvodeskDateService} from '../../../../../app/EvodeskApp/services/EvodeskDate.service';
import {DeskCommonTranslateEntity, EvodeskDeskTranslateLogRecordService} from '../../../services/EvodeskDeskTranslateLogRecord.service';
import {environment} from '../../../../../../environments/environment.config';
import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

interface State {
    ready: boolean;
}

interface Props {
    logRecord: DeskCardLogRecordModel;
    card: DeskCardModel;
    authToken: string;
}

export {Props as EvodeskDeskHistoryRecordActionComponentProps};

function htmlEntities(str: string) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

@Component({
    selector: 'evodesk-desk-history-record-action',
    templateUrl: './EvodeskDeskHistoryRecordAction.component.pug',
    styleUrls: [
        './EvodeskDeskHistoryRecordAction.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskHistoryRecordActionComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private dates: EvodeskDateService,
        private ngxTranslate: TranslateService,
        private logTranslate: EvodeskDeskTranslateLogRecordService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    get username(): string {
        return this.props.logRecord.user.name || this.props.logRecord.user.email;
    }

    get date$(): Observable<string> {
        return this.dates.diffFromNowV5(moment(this.props.logRecord.creation_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate());
    }

    get description(): Observable<string> {
        return Observable.create(response => {
            const r: DeskCommonTranslateEntity = this.logTranslate.translate(this.props.logRecord);

            this.ngxTranslate.get(r.action, { value: htmlEntities(r.value) }).pipe(takeUntil(this.ngOnDestroy$), take(1)).subscribe(
                (result) => {
                    response.next(result);
                    response.complete();
                },
                () => {
                    response.next('{NGX TRANSLATE ERROR}');
                },
            );
        });
    }

    get text(): any {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/desk/${this.props.card.deskId}/card/${this.props.card.id}/attachment/${this.props.logRecord.entity_id}/download`;
    }

    get attachmentLink(): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/desk/${this.props.card.deskId}/card/${this.props.card.id}/attachment/${this.props.logRecord.entity_id}/download?access_token=${this.props.authToken}`;
    }

    get previewNgStyle(): any {
        return {
            'background-image': `url(${this.attachmentLink})`,
        };
    }
}
