import {Component, EventEmitter, Output} from '@angular/core';

import {DeskColumnDefinitionModel, DeskColumnId, DeskColumnType} from '../../../../../app/EvodeskRESTApi/models/DeskColumnDefinition.model';

import {ColumnId, DeskMoveMultiComponentProps} from './EvodeskDeskMoveMulti.component';

import {EvodeskDeskMoveCardsService} from '../../../services/EvodeskDeskMoveCards.service';
import {EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';

type State = {
    fromColumnId: number;
    props: DeskMoveMultiComponentProps;
};

@Component({
    selector: 'evodesk-desk-move-multi-container',
    template: `
      <evodesk-desk-move-multi
        [props]="state.props"
        (close)="close()"
        (move)="submit($event)"
      ></evodesk-desk-move-multi>
    `,
})
export class EvodeskDeskMoveMultiContainerComponent
{
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        fromColumnId: -1,
        props: {
            toColumns: [],
            doneColumnTypes: [DeskColumnType.Done],
            failedColumnTypes: [DeskColumnType.Failed],
        },
    };

    constructor(
        private deskState: EvodeskDeskStateService,
        private moveService: EvodeskDeskMoveCardsService,
    ) {}

    get columns(): Array<DeskColumnDefinitionModel> {
        return this.deskState.snapshot.columns;
    }

    use(fromColumnId: DeskColumnId): void {
        this.state = {
            ...this.state,
            fromColumnId: fromColumnId,
            props: {
                ...this.state.props,
                toColumns: this.columns.filter(c => c.id !== fromColumnId),
            },
        };
    }

    submit(toColumnId: ColumnId): void {
        this.moveService.moveAllCardsFromColumnTo(this.state.fromColumnId, toColumnId).subscribe();
        this.close();
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
