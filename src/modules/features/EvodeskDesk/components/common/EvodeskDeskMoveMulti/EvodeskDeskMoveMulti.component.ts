import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

export type ColumnId = number;

interface ColumnDefinition {
    id: ColumnId;
    type: number;
    name: string;
    order: number;
}

export interface DeskMoveMultiComponentProps
{
    toColumns: Array<ColumnDefinition>;
    doneColumnTypes: Array<number>;
    failedColumnTypes: Array<number>;
}

interface State {
    ready: boolean;
    toColumnId: ColumnId | undefined;
}

@Component({
    selector: 'evodesk-desk-move-multi',
    templateUrl: './EvodeskDeskMoveMulti.component.pug',
    styleUrls: [
        './EvodeskDeskMoveMulti.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskMoveMultiComponent implements OnChanges
{
    @Input() props: DeskMoveMultiComponentProps;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('move') moveEvent: EventEmitter<ColumnId> = new EventEmitter<ColumnId>();

    public state: State = {
        ready: false,
        toColumnId: undefined,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    trackByColumnId(index: number, column: ColumnDefinition): any {
        return column.id;
    }

    select(toColumnId: ColumnId): void {
        this.state = {
            ...this.state,
            toColumnId: toColumnId,
        };
    }

    move(): void {
        this.moveEvent.next(this.state.toColumnId);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    isDoneColumn(column: ColumnDefinition): boolean {
        return !!~this.props.doneColumnTypes.indexOf(column.type);
    }

    isFailedColumn(column: ColumnDefinition): boolean {
        return !!~this.props.failedColumnTypes.indexOf(column.type);
    }
}
