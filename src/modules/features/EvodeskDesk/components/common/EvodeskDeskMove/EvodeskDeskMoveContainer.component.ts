import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import {EvodeskDeskMoveComponentMoveEvent, EvodeskDeskMoveComponentProps, EvodeskDeskMoveComponentVariant} from './EvodeskDeskMove.component';

import {DeskColumnType} from '../../../../../app/EvodeskRESTApi/models/DeskColumnDefinition.model';
import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {EvodeskDeskMoveCardsService} from '../../../services/EvodeskDeskMoveCards.service';

interface Props {
    card: DeskCardModel;
    variant: EvodeskDeskMoveComponentVariant;
}

interface State {
    ready: boolean;
    props?: EvodeskDeskMoveComponentProps;
}

@Component({
    selector: 'evodesk-desk-move-container',
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-move [props]="state.props" (close)="closeEvent.emit($event)" (move)="move($event)"></evodesk-desk-move>
      </ng-container>
    `,
})
export class EvodeskDeskMoveContainerComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private deskState: EvodeskDeskStateService,
        private moveService: EvodeskDeskMoveCardsService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props && this.props.card) {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    card: this.props.card,
                    variant: this.props.variant,
                    currentColumn: this.deskState.snapshot.columns.filter(c => c.id === this.props.card.column)[0],
                    availableColumns: this.deskState.snapshot.columns,
                    doneColumnTypes: [DeskColumnType.Done],
                    failedColumnTypes: [DeskColumnType.Failed],
                },
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    move($event: EvodeskDeskMoveComponentMoveEvent): void {
        const toPosition: number = (() => {
            switch ($event.toPosition) {
                default: {
                    throw new Error(`Unknown position "${$event.toPosition}"`);
                }

                case 'start': {
                    return 0;
                }

                case 'end': {
                    const filtered: Array<number> = this.deskState.snapshot.sourceCards
                        .filter(sc => sc.column === $event.toColumnId)
                        .map(sc => sc.order);

                    if (filtered.length) {
                        return this.deskState.snapshot.sourceCards
                            .filter(sc => sc.column === $event.toColumnId)
                            .map(sc => sc.order)
                            .reduce((p: number, v: number) => {
                                return ( p > v ? p : v );
                            }) + 1;
                    } else {
                        return 0;
                    }
                }
            }
        })();

        this.moveService.moveCard(this.props.card.id, $event.toColumnId, toPosition).subscribe();

        this.closeEvent.emit(undefined);
    }
}
