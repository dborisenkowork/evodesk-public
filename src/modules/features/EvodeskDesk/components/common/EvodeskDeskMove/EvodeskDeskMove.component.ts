import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

type ColumnId = number;

interface Column {
    id: ColumnId;
    name: string;
    type: number;
}

interface State {
    ready: boolean;
    toColumnId: ColumnId | undefined;
    toPosition: ToPosition;
}

interface MoveEvent {
    toColumnId: ColumnId;
    toPosition: ToPosition;
}

type ToPosition = 'start' | 'end';
type Variant = 'modal' | 'control' | 'mat-menu';

interface Props {
    card: DeskCardModel;
    currentColumn: Column;
    availableColumns: Array<Column>;
    doneColumnTypes: Array<ColumnId>;
    failedColumnTypes: Array<ColumnId>;
    variant: Variant;
}

export {Props as EvodeskDeskMoveComponentProps};
export {Variant as EvodeskDeskMoveComponentVariant};
export {MoveEvent as EvodeskDeskMoveComponentMoveEvent};

@Component({
    selector: 'evodesk-desk-move',
    templateUrl: './EvodeskDeskMove.component.pug',
    styleUrls: [
        './EvodeskDeskMove.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskMoveComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('move') moveEvent: EventEmitter<MoveEvent> = new EventEmitter<MoveEvent>();

    @ViewChild('input') inputRef: ElementRef;

    public state: State = {
        ready: false,
        toColumnId: undefined,
        toPosition: 'start',
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    get shouldDisplayModalHeader(): boolean {
        return this.props.variant !== 'control';
    }

    get availableColumns(): Array<Column> {
        return this.props.availableColumns;
    }

    get disabled(): boolean {
        return this.state.toColumnId === undefined;
    }

    isDoneColumn(column: Column): boolean {
        return !!~this.props.doneColumnTypes.indexOf(column.type);
    }

    isFailedColumn(column: Column): boolean {
        return !!~this.props.failedColumnTypes.indexOf(column.type);
    }

    select(column: Column): void {
        this.state = {
            ...this.state,
            toColumnId: column.id,
        };
    }

    usePosition(position: ToPosition): void {
        this.state = {
            ...this.state,
            toPosition: position,
        };
    }

    submit(): void {
        if (this.state.toColumnId) {
            this.moveEvent.emit({
                toColumnId: this.state.toColumnId,
                toPosition: this.state.toPosition,
            });
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
