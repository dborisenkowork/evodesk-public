import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

interface State {
    ready: boolean;
}

interface Props {
    card: DeskCardModel;
    variant: Variant;
}

type Variant = 'modal' | 'control' | 'mat-menu';

export {Props as EvodeskDeskAddAttachmentComponentProps};
export {Variant as EvodeskDeskAddAttachmentComponentVariant};

@Component({
    selector: 'evodesk-desk-add-attachment',
    templateUrl: './EvodeskDeskAddAttachment.component.pug',
    styleUrls: [
        './EvodeskDeskAddAttachment.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskAddAttachmentComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('requestImage') requestImageEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('requestAudio') requestAudioEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('input') inputRef: ElementRef;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    get shouldDisplayModalHeader(): boolean {
        return this.props.variant !== 'control';
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    requestImage(): void {
        this.requestImageEvent.emit(undefined);
        this.close();
    }

    requestAudio(): void {
        this.requestAudioEvent.emit(undefined);
        this.close();
    }
}
