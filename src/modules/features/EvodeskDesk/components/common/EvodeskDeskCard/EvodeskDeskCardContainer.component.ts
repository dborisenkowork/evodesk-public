import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {filter, take, takeUntil} from 'rxjs/operators';

import {EvodeskDeskCardComponentEditEvent, EvodeskDeskCardComponentOpenEvent, EvodeskDeskCardComponentProps} from './EvodeskDeskCard.component';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {deskCardCanBeMovedNextTo, DeskColumnDefinitionModel} from '../../../../../app/EvodeskRESTApi/models/DeskColumnDefinition.model';

import {EvodeskDeskAction, EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {CurrentUserService} from '../../../../EvodeskAuth/services/CurrentUser.service';
import {EvodeskDeskCardModalService} from '../../../services/EvodeskDeskCardModal.service';
import {EvodeskDeskFloatService} from '../../../services/EvodeskDeskFloat.service';

type State = EvodeskDeskCardComponentProps;

@Component({
    selector: 'evodesk-desk-card-container',
    template: `
      <ng-container *ngIf="state">
        <evodesk-desk-card
          [props]="state"
          (open)="open($event)"
          (edit)="edit($event)"
          (beforeEdit)="beforeEditEvent.emit(undefined)"
          (stopEdit)="stopEditEvent.emit(undefined)"
          (setTitle)="setTitle($event)"
        ></evodesk-desk-card>
      </ng-container>
    `,
})
export class EvodeskDeskCardContainerComponent implements OnChanges, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    @Input() card: DeskCardModel;
    @Input() editMode: boolean;

    @Output('beforeEdit') beforeEditEvent: EventEmitter<undefined> = new EventEmitter<undefined>();
    @Output('afterEdit') afterEditEvent: EventEmitter<undefined> = new EventEmitter<undefined>();
    @Output('stopEdit') stopEditEvent: EventEmitter<undefined> = new EventEmitter<undefined>();

    public state: State = {
        card: <any>undefined,
        canBeMovedToNextColumn: false,
        currentUserId: -1,
        currentDeskId: -1,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private currentUserService: CurrentUserService,
        private deskState: EvodeskDeskStateService,
        private modalCard: EvodeskDeskCardModalService,
        private floatService: EvodeskDeskFloatService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.deskState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(states => !!states.current && !!states.current.columns && states.current.columns.length > 0),
            take(1),
        ).subscribe(states => {
            const column: DeskColumnDefinitionModel = states.current.columns.filter(c => c.id === this.card.column)[0];

            this.state = {
                ...this.state,
                currentDeskId: states.current.desk.id,
                card: this.card,
                canBeMovedToNextColumn: !!column
                    ? deskCardCanBeMovedNextTo.filter(d => d.from === column.type).length > 0
                    : false,
                currentUserId: this.currentUserService.impersonatedAs.id,
                editMode: this.editMode,
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    open($event: EvodeskDeskCardComponentOpenEvent): void {
        setTimeout(() => {
            if (! this.deskState.snapshot.editingCard) {
                this.modalCard.navigateToCardModel($event.card.id);
            }
        });
    }

    edit($event: EvodeskDeskCardComponentEditEvent): void {
        this.floatService.open($event.card, $event.element).subscribe(() => {
            this.afterEditEvent.emit(undefined);
        });
    }

    setTitle(title: string): void {
        this.deskState.dispatch({
            type: EvodeskDeskAction.SetTitle,
            payload: {
                cardId: this.card.id,
                newTitle: title,
            },
        });
    }
}
