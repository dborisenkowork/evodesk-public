import {AfterViewInit, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

import * as R from 'ramda';
import * as moment from 'moment';

import {evodeskTitleMaxLength, guessDeskCardAttachmentType} from '../../../configs/EvodeskDesk.constants';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../../types/Backend.types';

import {DeskLabelModel} from '../../../../../app/EvodeskRESTApi/models/DeskLabel.model';
import {DeskId} from '../../../../../app/EvodeskRESTApi/models/Desk.model';
import {UserId} from '../../../../../app/EvodeskRESTApi/models/user/User.model';
import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskCardBorderModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardBorderColor.model';
import {DeskCardAttachmentModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardAttachment.model';

const capitalize: (input: string) => string = R.compose(
    R.join(''),
    R.juxt([R.compose(R.toUpper, R.head), R.tail]),
);

export interface EvodeskDeskCardComponentProps {
    currentDeskId: DeskId;
    card: DeskCardModel;
    canBeMovedToNextColumn: boolean;
    currentUserId: UserId;
    editMode?: boolean;
}

interface State {
    form: FormGroup;
    isFormFocused: boolean;
    uniqueId: string;
    isSavedToBackend: boolean;
    hasLabels: boolean;
    hasStatusWidgets: boolean;
    isRepeatable: boolean;
    hasDeadline: boolean;
    isDeadlineOverdue: boolean;
    numAttachments: number;
    numComments: number;
    deadlineStr: string;
    hasChecklist: boolean;
    totalChecklistItems: number;
    completedChecklistItems: number;
    imageAttachment?: DeskCardAttachmentModel | undefined;
    cardNgClasses: {
        'card__is-disabled': boolean,
        'card__is-common': boolean,
        'card__is-todo-today': boolean;
        'card__is-done-today': boolean;
        'card__is-responsible-for': boolean;
        'card_has-attachment-image': boolean;
    };
    labels: Array<DeskLabelModel>;
}

interface FormValue {
    title: string;
}

interface OpenEvent {
    card: DeskCardModel;
    element: HTMLElement;
}

interface EditEvent {
    card: DeskCardModel;
    element: HTMLElement;
}

export {OpenEvent as EvodeskDeskCardComponentOpenEvent};
export {EditEvent as EvodeskDeskCardComponentEditEvent};

let ignoreNext: boolean = false;

@Component({
    selector: 'evodesk-desk-card',
    templateUrl: './EvodeskDeskCard.component.pug',
    styleUrls: [
        './EvodeskDeskCard.component.scss',
    ],
})
export class EvodeskDeskCardComponent implements OnChanges, OnInit, AfterViewInit, OnDestroy
{
    @Input() props: EvodeskDeskCardComponentProps;

    @Output('open') openEvent: EventEmitter<OpenEvent> = new EventEmitter<OpenEvent>();
    @Output('edit') editEvent: EventEmitter<EditEvent> = new EventEmitter<EditEvent>();
    @Output('beforeEdit') beforeEditEvent: EventEmitter<undefined> = new EventEmitter<undefined>();
    @Output('stopEdit') stopEditEvent: EventEmitter<undefined> = new EventEmitter<undefined>();
    @Output('setTitle') setTitleEvent: EventEmitter<string> = new EventEmitter<undefined>();

    @ViewChild('input') inputRef: ElementRef;
    @ViewChild('element') elementRef: ElementRef;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            title: [null, [Validators.required, Validators.maxLength(evodeskTitleMaxLength)]],
        }),
        isFormFocused: false,
        uniqueId: 'none',
        isSavedToBackend: false,
        hasLabels: false,
        hasStatusWidgets: false,
        isRepeatable: false,
        hasDeadline: false,
        isDeadlineOverdue: false,
        numAttachments: 0,
        numComments: 0,
        deadlineStr: '',
        hasChecklist: false,
        totalChecklistItems: 0,
        completedChecklistItems: 0,
        cardNgClasses: {
            'card__is-disabled': false,
            'card__is-common': false,
            'card__is-todo-today': false,
            'card__is-done-today': false,
            'card__is-responsible-for': false,
            'card_has-attachment-image': false,
        },
        labels: [],
    };

    constructor(
        private fb: FormBuilder,
        private ngZone: NgZone,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        const isRepeatable: boolean = this.props.card.daily;
        const hasDeadline: boolean = R.not(R.isNil(this.props.card.deadline));
        const numAttachments: number = Array.isArray(this.props.card.attachments) ? this.props.card.attachments.length : 0;
        const isDeadlineOverdue: boolean = moment(this.props.card.deadline, 'YYYY-MM-DD').diff(new Date()) < 0;
        const hasChecklist: boolean = Array.isArray(this.props.card.checklists) && this.props.card.checklists.length > 0;
        const isResponsibleFor: boolean = this.props.card.deskId !== this.props.currentDeskId && this.props.card.responsible.filter(r => r.id === this.props.currentUserId).length > 0;
        const labels: Array<DeskLabelModel> = this.props.card.labels.slice();
        const numComments: number = this.props.card.comments.length;
        const imageAttachment: DeskCardAttachmentModel | undefined = this.props.card.attachments.filter(a => guessDeskCardAttachmentType(a) === 'image')[0];

        let totalChecklistItems: number = 0;
        let completedChecklistItems: number = 0;

        if (hasChecklist) {
            this.props.card.checklists.forEach(list => {
                list.items.forEach(i => {
                    totalChecklistItems++;

                    if (i.completed) {
                        completedChecklistItems++;
                    }
                });
            });
        }

        this.state = {
            ...this.state,
            uniqueId: `evodesk-desk-card-${this.props.card.id}`,
            isSavedToBackend: this.props.card.id > 0,
            hasLabels: Array.isArray(this.props.card.labels) && this.props.card.labels.length > 0,
            hasStatusWidgets: isRepeatable || hasDeadline || numAttachments > 0 || numComments > 0 || hasChecklist,
            isRepeatable: isRepeatable,
            hasDeadline: hasDeadline,
            isDeadlineOverdue: isDeadlineOverdue,
            numAttachments: numAttachments,
            numComments: numComments,
            deadlineStr: hasDeadline ? capitalize(moment(this.props.card.deadline, BACKEND_DATE_FORMAT_AS_MOMENT).format('dd, D MMM HH:mm')) : '',
            hasChecklist: hasChecklist,
            totalChecklistItems: totalChecklistItems,
            completedChecklistItems: completedChecklistItems,
            imageAttachment: imageAttachment,
            cardNgClasses: {
                ...this.state.cardNgClasses,
                'card__is-disabled': this.props.card.id < 0,
                'card__is-common': ! isResponsibleFor && ! (this.props.card.color === DeskCardBorderModel.Blue) && ! (this.props.card.color === DeskCardBorderModel.Green),
                'card__is-todo-today': this.props.card.color === DeskCardBorderModel.Blue,
                'card__is-done-today': this.props.card.color === DeskCardBorderModel.Green,
                'card__is-responsible-for': isResponsibleFor,
                'card_has-attachment-image': this.props.card.id < 0,
            },
            labels: labels.sort((a, b) => a.id - b.id),
        };

        if (! this.props.editMode || ! this.state.isFormFocused) {
            ignoreNext = true;

            this.state.form.patchValue(<FormValue>{
                title: this.props.card.title,
            });
        }
    }

    ngOnInit(): void {
        if (this.props.editMode) {
            this.state.form.get('title').valueChanges.pipe(takeUntil(this.ngOnDestroy$)).subscribe((v) => {
                if (ignoreNext) {
                    ignoreNext = false;
                } else {
                    if (this.state.form.valid) {
                        this.setTitleEvent.emit(v);
                    }
                }
            });
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    ngAfterViewInit(): void {
        if (this.props.editMode) {
            this.focusInput();
        }
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    onCardMouseDown($event: MouseEvent): void {
        if (! this.state.isSavedToBackend) {
            $event.stopPropagation();
            $event.preventDefault();
        }
    }

    onCardTouchStart($event: TouchEvent): void {
        if (! this.state.isSavedToBackend) {
            $event.stopPropagation();
            $event.preventDefault();
        }
    }

    open(): void {
        if (this.state.isSavedToBackend && ! this.props.editMode) {
            this.openEvent.emit({
                card: this.props.card,
                element: this.elementRef.nativeElement,
            });
        }
    }

    edit(): void {
        if (this.state.isSavedToBackend) {
            const eventData: EditEvent = {
                card: this.props.card,
                element: this.elementRef.nativeElement,
            };

            this.beforeEditEvent.emit();

            setTimeout(() => {
                this.editEvent.emit(eventData);
            });
        }
    }

    formFocus(): void {
        this.state = {
            ...this.state,
            isFormFocused: true,
        };
    }

    formBlur(): void {
        this.state = {
            ...this.state,
            isFormFocused: false,
        };
    }

    focusInput(): void {
        this.ngZone.runOutsideAngular(() => {
            if (this.inputRef) {
                (this.inputRef.nativeElement as HTMLInputElement).focus();
            }
        });
    }

    onKeyDownEnter(): void {
        this.stopEditEvent.emit(undefined);
    }
}
