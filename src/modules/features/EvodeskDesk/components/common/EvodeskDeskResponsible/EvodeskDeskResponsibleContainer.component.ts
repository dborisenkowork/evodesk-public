import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {v3Bottom349pxMatDialogConfig} from '../../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskParticipantId, DeskParticipantModel} from '../../../../../app/EvodeskRESTApi/models/DeskParticipant.model';

import {EvodeskDeskResponsibleComponentProps, EvodeskDeskResponsibleComponentVariant} from './EvodeskDeskResponsible.component';
import {EvodeskDeskParticipantContainerComponent as ParticipantModal} from '../EvodeskDeskParticipant/EvodeskDeskParticipantContainer.component';

import {EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {EvodeskDeskCardResponsibleService} from '../../../services/EvodeskDeskCardResponsible.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskResponsibleComponentProps;
}

@Component({
    selector: 'evodesk-desk-responsible-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-responsible
          [props]="state.props"
          (close)="close()"
          (toggle)="toggle($event)"
          (participants)="participants()"
        ></evodesk-desk-responsible>
      </ng-container>
    `,
})
export class EvodeskDeskResponsibleContainerComponent implements OnChanges, OnInit, OnDestroy
{
    @Input() card: DeskCardModel;
    @Input() variant: EvodeskDeskResponsibleComponentVariant;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private stop$: Subject<DeskParticipantId> = new Subject<DeskParticipantId>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private matDialog: MatDialog,
        private deskState: EvodeskDeskStateService,
        private service: EvodeskDeskCardResponsibleService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (changes['card']) {
            if (this.card) {
                this.deskState.current$.pipe(
                    takeUntil(this.ngOnChanges$),
                    map(s => s.current.sourceCards.filter(sc => sc.id === this.card.id)[0]),
                    distinctUntilChanged(),
                ).subscribe((card) => {
                    this.state = {
                        ...this.state,
                        ready: true,
                        props: {
                            ...this.state.props,
                            card: card,
                            available: this.deskState.snapshot.desk.members,
                            variant: this.variant,
                        },
                    };

                    this.cdr.detectChanges();
                });
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }

        if (changes['variant'] && this.state.ready) {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    variant: this.variant,
                },
            };
        }
    }

    ngOnInit(): void {
        this.deskState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.desk.members),
            distinctUntilChanged(),
        ).subscribe((participants) => {
            if (this.state.ready) {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        available: [...participants],
                    },
                };
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    toggle(input: DeskParticipantModel): void {
        const isActive: boolean = !!~this.state.props.card.responsible.map(m => m.id).indexOf(input.id);

        this.stop$.next(input.id);

        if (isActive) {
            this.service.remodeDeskParticipanAsResponsibleOfCard(this.card.id, input).pipe(takeUntil(this.stop$.pipe(filter(id => id === input.id)))).subscribe();
        } else {
            this.service.addDeskParticipantAsResponsibleOfCard(this.card.id, input).pipe(takeUntil(this.stop$.pipe(filter(id => id === input.id)))).subscribe();
        }
    }

    participants(): void {
        setTimeout(() => {
            const closed$: Subject<void> = new Subject<void>();

            const dialogRef: MatDialogRef<ParticipantModal> = this.matDialog.open(ParticipantModal, {
                ...v3Bottom349pxMatDialogConfig,
                viewContainerRef: this.deskState.snapshot.viewContainerRef,
                closeOnNavigation: true,
                disableClose: true,
            });

            dialogRef.afterClosed().pipe(takeUntil(closed$)).subscribe(() => {
                closed$.next(undefined);
            });

            dialogRef.componentInstance.closeEvent.pipe(takeUntil(closed$)).subscribe(() => {
                dialogRef.close();
            });

            dialogRef.backdropClick().pipe(takeUntil(closed$)).subscribe(() => {
                dialogRef.close();
            });
        },  100);

        this.close();
    }
}
