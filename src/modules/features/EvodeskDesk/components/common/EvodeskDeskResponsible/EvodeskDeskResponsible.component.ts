import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskParticipantModel} from '../../../../../app/EvodeskRESTApi/models/DeskParticipant.model';

type Variant = 'modal' | 'control' | 'mat-menu';

interface Props {
    card: DeskCardModel;
    available: Array<DeskParticipantModel>;
    variant: Variant;
}

interface State {
    ready: boolean;
    isSearchFieldFocused: boolean;
    searchForm: FormGroup;
}

interface SearchFormValue {
    search: string;
}

export {Props as EvodeskDeskResponsibleComponentProps};
export {Variant as EvodeskDeskResponsibleComponentVariant};

@Component({
    selector: 'evodesk-desk-responsible',
    templateUrl: './EvodeskDeskResponsible.component.pug',
    styleUrls: [
        './EvodeskDeskResponsible.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskResponsibleComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('toggle') toggleEvent: EventEmitter<DeskParticipantModel> = new EventEmitter<DeskParticipantModel>();
    @Output('participants') participantsEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        isSearchFieldFocused: false,
        searchForm: this.fb.group({
            search: [null],
        }),
    };

    constructor(
        private fb: FormBuilder,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    isActive(input: DeskParticipantModel): boolean {
        return !!~this.props.card.responsible.map(m => m.id).indexOf(input.id);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    toggle(input: DeskParticipantModel): void {
        this.toggleEvent.emit(input);
    }

    participants(): void {
        this.participantsEvent.emit(undefined);
    }

    get shouldDisplayModalHeader(): boolean {
        return this.props.variant !== 'control';
    }

    get filtered(): Array<DeskParticipantModel> {
        const search: string | null = (this.state.searchForm.value as SearchFormValue).search;

        if (search) {
            return this.props.available.filter(a => !!~(a.name || a.email).toLocaleLowerCase().indexOf(search));
        } else {
            return this.props.available;
        }
    }
}
