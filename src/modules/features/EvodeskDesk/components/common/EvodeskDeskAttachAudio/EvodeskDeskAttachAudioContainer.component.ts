import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import {EvodeskDeskAttachAudioComponentProps, EvodeskDeskAttachAudioComponentVariant} from './EvodeskDeskAttachAudio.component';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

interface Props {
    card: DeskCardModel;
    variant: EvodeskDeskAttachAudioComponentVariant;
}

interface State {
    ready: boolean;
    props?: EvodeskDeskAttachAudioComponentProps;
}

export {Props as EvodeskDeskAttachAudioContainerComponentProps};

@Component({
    selector: 'evodesk-desk-attach-audio-container',
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-attach-audio [props]="state.props" (close)="closeEvent.emit($event)"></evodesk-desk-attach-audio>
      </ng-container>
    `,
})
export class EvodeskDeskAttachAudioContainerComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    card: this.props.card,
                    variant: this.props.variant,
                },
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
