import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

interface State {
    ready: boolean;
    screen: Screen;
}

interface Props {
    card: DeskCardModel;
    variant: Variant;
}

type Variant = 'modal' | 'control' | 'mat-menu';
type Screen = 'select' | 'lobby' | 'submitting';

export {Props as EvodeskDeskAttachAudioComponentProps};
export {Variant as EvodeskDeskAttachAudioComponentVariant};

@Component({
    selector: 'evodesk-desk-attach-audio',
    templateUrl: './EvodeskDeskAttachAudio.component.pug',
    styleUrls: [
        './EvodeskDeskAttachAudio.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskAttachAudioComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('input') inputRef: ElementRef;

    public state: State = {
        ready: false,
        screen: 'select',
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    get shouldDisplayModalHeader(): boolean {
        return this.props.variant !== 'control';
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
