import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {DeskCardResponsibleModel} from '../../../../../app/EvodeskRESTApi/models/DeskResponsible.model';
import {DeskParticipantModel} from '../../../../../app/EvodeskRESTApi/models/DeskParticipant.model';
import {ProfileId} from '../../../../../app/EvodeskRESTApi/models/profile/Profile.model';

type Variant = 'modal' | 'control' | 'mat-menu';

interface Props {
    members: Array<DeskCardResponsibleModel>;
    variant: Variant;
    ownerProfileId: ProfileId;
}

interface State {
    ready: boolean;
    isSearchFieldFocused: boolean;
    searchForm: FormGroup;
    inviteByEmailForm: FormGroup;
}

interface SearchFormValue {
    search: string;
}

interface InviteByEmailFormValue {
    email: string;
}

interface InviteByEmailEvent {
    form: FormGroup;
    formValue: InviteByEmailFormValue;
}

export {Props as EvodeskDeskParticipantComponentProps};
export {Variant as EvodeskDeskParticipantComponentVariant};
export {InviteByEmailEvent as EvodeskDeskParticipantComponentInviteByEmailEvent};

@Component({
    selector: 'evodesk-desk-participant',
    templateUrl: './EvodeskDeskParticipant.component.pug',
    styleUrls: [
        './EvodeskDeskParticipant.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskParticipantComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('remove') removeEvent: EventEmitter<DeskParticipantModel> = new EventEmitter<DeskParticipantModel>();
    @Output('inviteByEmail') inviteByEmailEvent: EventEmitter<InviteByEmailEvent> = new EventEmitter<InviteByEmailEvent>();
    @Output('inviteByLink') inviteByLinkEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        isSearchFieldFocused: false,
        searchForm: this.fb.group({
            search: [null],
        }),
        inviteByEmailForm: this.fb.group({
            email: [null, [Validators.required, Validators.email]],
        }),
    };

    constructor(
        private fb: FormBuilder,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    get searchFormValue(): SearchFormValue {
        return this.state.searchForm.value;
    }

    get inviteByEmailFormValue(): InviteByEmailFormValue {
        return this.state.inviteByEmailForm.value;
    }

    get shouldDisplayModalHeader(): boolean {
        return this.props.variant !== 'control';
    }

    get filtered(): Array<DeskParticipantModel> {
        const search: string | null = this.searchFormValue.search;

        if (search) {
            return this.props.members.filter(a => !!~(a.name || a.email).toLocaleLowerCase().indexOf(search));
        } else {
            return this.props.members;
        }
    }

    resetSearch(): void {
        this.state.searchForm.reset();
    }

    isOwner(p: DeskParticipantModel): boolean {
        return p.id.toString() === this.props.ownerProfileId.toString();
    }

    remove(p: DeskParticipantModel): void {
        this.removeEvent.emit(p);
    }

    inviteByEmail(): void {
        if (this.state.inviteByEmailForm.valid) {
            this.inviteByEmailEvent.emit({
                form: this.state.inviteByEmailForm,
                formValue: this.inviteByEmailFormValue,
            });
        }
    }

    inviteByLink(): void {
        this.inviteByLinkEvent.emit(undefined);
    }
}
