import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import {proxy} from '../../../../../../functions/proxy.function';

import {v3Bottom420pxMatDialogConfig} from '../../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {DeskParticipantModel} from '../../../../../app/EvodeskRESTApi/models/DeskParticipant.model';

import {EvodeskDeskParticipantComponentInviteByEmailEvent, EvodeskDeskParticipantComponentProps, EvodeskDeskParticipantComponentVariant} from './EvodeskDeskParticipant.component';
import {EvodeskDeskParticipantInviteByLinkContainerComponent as InviteByLink} from '../EvodeskDeskParticipantInviteByLink/EvodeskDeskParticipantInviteByLinkContainer.component';

import {EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {EvodeskDeskParticipantService} from '../../../services/EvodeskDeskParticipant.service';
import {EvodeskAlertModalService} from '../../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {EvodeskConfirmModalService} from '../../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';

interface State {
    ready: boolean;
    props?: EvodeskDeskParticipantComponentProps;
}

@Component({
    selector: 'evodesk-desk-participant-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-participant
          [props]="state.props"
          (close)="close()"
          (remove)="remove($event)"
          (inviteByEmail)="inviteByEmail($event)"
          (inviteByLink)="inviteByLink()"
        ></evodesk-desk-participant>
      </ng-container>
    `,
})
export class EvodeskDeskParticipantContainerComponent implements OnInit, OnDestroy
{
    @Input() variant: EvodeskDeskParticipantComponentVariant = 'modal';

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private uiAlert: EvodeskAlertModalService,
        private uiConfirm: EvodeskConfirmModalService,
        private deskState: EvodeskDeskStateService,
        private service: EvodeskDeskParticipantService,
    ) {}

    ngOnInit(): void {
        this.deskState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            map(s => s.current.desk.members),
            distinctUntilChanged(),
        ).subscribe((members) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    members: members,
                    variant: this.variant,
                    ownerProfileId: this.deskState.snapshot.desk.ownerId,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    remove($event: DeskParticipantModel): void {
        const prefix: string = `EvodeskDesk.components.common.EvodeskDeskParticipant.ConfirmRemove`;

        this.uiConfirm.open({
            viewContainerRef: this.viewContainerRef,
            closeOnNavigation: true,
            title: {
                text: `${prefix}.Title`,
                translate: true,
            },
            text: {
                text: `${prefix}.Text`,
                translate: true,
                replaces: [$event.name || $event.email],
                asHtml: true,
            },
            confirmButton: {
                text: `${prefix}.Confirm`,
                translate: true,
            },
            cancelButton: {
                text: `${prefix}.Cancel`,
                translate: true,
            },
            confirm: (() => {
                this.service.removeParticipantFromDesk(this.deskState.snapshot.desk.id, $event.id).subscribe();
            }),
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }

    inviteByEmail($event: EvodeskDeskParticipantComponentInviteByEmailEvent): void {
        $event.form.disable();

        proxy(this.service.inviteByEmail(this.deskState.snapshot.desk.id, $event.formValue.email)).subscribe(
            () => {
                $event.form.reset();
                $event.form.enable();
            },
            (error) => {
                if (error) {
                    this.uiAlert.open({
                        text: {
                            text: error,
                            translate: true,
                        },
                        title: {
                            text: `EvodeskDesk.components.common.EvodeskDeskParticipant.InviteByEmailError.Title`,
                            translate: true,
                        },
                        ok: {
                            text: `EvodeskDesk.components.common.EvodeskDeskParticipant.InviteByEmailError.Ok`,
                            translate: true,
                        },
                    }).subscribe();

                    $event.form.enable();
                } else {
                    this.uiAlert.httpError().subscribe();

                    $event.form.enable();
                }
            },
        );
    }

    inviteByLink(): void {
        const closed$: Subject<void> = new Subject<void>();

        const dialogRef: MatDialogRef<InviteByLink> = this.matDialog.open(InviteByLink, {
            ...v3Bottom420pxMatDialogConfig,
            closeOnNavigation: true,
            viewContainerRef: this.viewContainerRef,
        });

        dialogRef.afterClosed().pipe(takeUntil(closed$)).subscribe(() => {
            closed$.next(undefined);
        });

        dialogRef.componentInstance.closeEvent.pipe(takeUntil(closed$)).subscribe(() => {
            dialogRef.close();
        });
    }
}
