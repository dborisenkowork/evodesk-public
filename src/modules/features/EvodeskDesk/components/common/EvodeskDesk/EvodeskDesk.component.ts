import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild} from '@angular/core';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskColumnDefinitionModel} from '../../../../../app/EvodeskRESTApi/models/DeskColumnDefinition.model';

export interface EvodeskDeskComponentProps
{
    cards: Array<DeskCardModel>;
    columns: Array<DeskColumnDefinitionModel>;
    isDragEnabled: boolean;
    isScrollEnabled: boolean;
}

interface State
{
    destroyed: boolean;
    hasScroll: boolean;
    columnCards: { [columnId: number]: Array<DeskCardModel> };
}

@Component({
    selector: 'evodesk-desk',
    templateUrl: './EvodeskDesk.component.pug',
    styleUrls: [
        './EvodeskDesk.component.scss',
    ],
})
export class EvodeskDeskComponent implements OnChanges, AfterViewInit, OnDestroy
{
    @Input() props: EvodeskDeskComponentProps;

    @Output('toggleSubMenu') toggleSubMenu: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('columns') columnsRef: ElementRef;

    public state: State = {
        destroyed: false,
        hasScroll: false,
        columnCards: {},
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private ngZone: NgZone,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.state = {
            ...this.state,
            columnCards: (() => {
                const result: { [columnId: number]: Array<DeskCardModel> } = {};

                this.props.columns.forEach(column => {
                    result[column.id] = this.props.cards
                        .filter(c => c.column === column.id)
                        .sort((a, b) => {
                            if (a.order > b.order) {
                                return 1;
                            } else if (a.order === b.order) {
                                return 0;
                            } else {
                                return -1;
                            }
                        });
                });

                return result;
            })(),
        };
    }

    ngAfterViewInit(): void {
        const tick: Function = () => {
            if (! this.state.destroyed && this.columnsRef) {
                const hasScroll: boolean = (this.columnsRef.nativeElement as HTMLElement).scrollWidth > (this.columnsRef.nativeElement as HTMLElement).clientWidth;

                if (hasScroll && ! this.state.hasScroll) {
                    this.state = {
                        ...this.state,
                        hasScroll: true,
                    };

                    this.cdr.detectChanges();
                } else if (! hasScroll && this.state.hasScroll) {
                    this.state = {
                        ...this.state,
                        hasScroll: false,
                    };

                    this.cdr.detectChanges();
                }

                window.requestAnimationFrame(() => {
                    tick();
                });
            }
        };

        this.ngZone.runOutsideAngular(() => {
            tick();
        });
    }

    ngOnDestroy(): void {
        this.state = {
            ...this.state,
            destroyed: true,
        };
    }

    trackByColumnId(index, item: DeskColumnDefinitionModel): any {
        return item.type;
    }

    isCardsColumn(item: DeskColumnDefinitionModel): boolean {
        return item.entities === 'cards';
    }

    isProjectColumn(item: DeskColumnDefinitionModel): boolean {
        return item.entities === 'projects';
    }
}
