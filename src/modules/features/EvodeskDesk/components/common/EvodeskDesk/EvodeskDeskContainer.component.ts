import {ChangeDetectorRef, Component, Input, NgZone, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewContainerRef} from '@angular/core';

import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, takeUntil} from 'rxjs/operators';

import {EvodeskDeskComponentProps} from './EvodeskDesk.component';

import {AppEvent} from '../../../../../app/EvodeskAppMessageBus/models/app-events.model';
import {DeskId} from '../../../../../app/EvodeskRESTApi/models/Desk.model';

import {BootstrapEvodeskDeskScript} from '../../../scripts/BootstrapEvodeskDesk.script';

import {DeskCardId, DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskAction, EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';
import {EvodeskAppMessageBusService} from '../../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';
import {EvodeskAppLoading, EvodeskAppLoadingStatusService} from '../../../../../app/EvodeskApp/services/EvodeskAppLoadingStatus.service';
import {EvodeskDeskBreakpointsService} from '../../../services/EvodeskDeskBreakpoints.service';
import {EvodeskDeskCardModalService} from '../../../services/EvodeskDeskCardModal.service';
import {EvodeskDeskTranslateLogRecordService, EvodeskTranslateLogRecordServiceResources} from '../../../services/EvodeskDeskTranslateLogRecord.service';
import {EvodeskDeskFilterService} from '../../../services/EvodeskDeskFilter.service';

interface State {
    ready: boolean;
    props: EvodeskDeskComponentProps;
}

@Component({
    selector: 'evodesk-desk-container',
    template: `
      <ng-container *ngIf="state">
        <evodesk-desk [props]="state.props"></evodesk-desk>
      </ng-container>
    `,
})
export class EvodeskDeskContainerComponent implements OnChanges, OnInit, OnDestroy
{
    @Input() deskId: DeskId;
    @Input() cardId: DeskCardId;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        props: {
            cards: [],
            columns: [],
            isDragEnabled: true,
            isScrollEnabled: false,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private ngZone: NgZone,
        private viewContainerRef: ViewContainerRef,
        private appBus: EvodeskAppMessageBusService,
        private appLoading: EvodeskAppLoadingStatusService,
        private deskState: EvodeskDeskStateService,
        private bootstrap: BootstrapEvodeskDeskScript,
        private logRecordService: EvodeskDeskTranslateLogRecordService,
        private breakpontService: EvodeskDeskBreakpointsService,
        private modalService: EvodeskDeskCardModalService,
        private filtersService: EvodeskDeskFilterService,
    ) {}

    ngOnInit(): void {
        this.modalService.viewContainerRef = this.viewContainerRef;
        this.filtersService.enableSync();

        this.deskState.side$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((action) => {
            switch (action.type) {
                case EvodeskDeskAction.DragCardStart:
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            isDragEnabled: false,
                            isScrollEnabled: true,
                        },
                    };

                    this.cdr.detectChanges();

                    break;

                case EvodeskDeskAction.DragCardEnd:
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            isDragEnabled: true,
                            isScrollEnabled: false,
                            cards: this.state.props.cards.filter(value => value.id !== action.payload.card.id || value.column !== action.payload.card.column),
                        },
                    };

                    this.cdr.detectChanges();

                    break;

                case EvodeskDeskAction.DropCard:
                    const updatedCardsMap: { [id: number]: DeskCardModel } = {};

                    action.payload.cards.forEach(c => {
                        updatedCardsMap[c.id] = c;
                    });

                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            cards: this.deskState.snapshot.filteredCards.map(value => {
                                if (updatedCardsMap[value.id]) {
                                    return updatedCardsMap[value.id];
                                }

                                return value;
                            }),
                        },
                    };

                    this.cdr.detectChanges();

                    break;
            }
        });

        this.appBus.stream$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(e => !!~[AppEvent.DeskForceReload].indexOf(e.type)),
        ).subscribe(() => {
            this.startBootstrap();
        });

        this.deskState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !s.prev || s.prev.columns !== s.current.columns),
        ).subscribe(s => {
            this.logRecordService.setResources((orig: EvodeskTranslateLogRecordServiceResources) => {
                return {
                    ...orig,
                    columns: s.current.columns,
                };
            });
        });

        this.breakpontService.init().pipe(takeUntil(this.ngOnDestroy$), distinctUntilChanged()).subscribe(next => {
            this.deskState.dispatch({
                type: EvodeskDeskAction.SetLayoutVariant,
                payload: {
                    layout: next,
                },
            });
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['deskId'] && this.deskId) {
            if (this.modalService.isOpened()) {
                this.modalService.closeCardModal();
            }

            this.startBootstrap();
        }

        if (changes['cardId'] && this.state.ready) {
            if (this.cardId) {
                this.modalService.openCardModal(this.cardId);
            } else {
                this.modalService.closeCardModal();
            }
        }
    }

    ngOnDestroy(): void {
        this.deskState.reset();

        this.ngOnDestroy$.next(undefined);
    }

    private startBootstrap(): void {
        const appLoading: EvodeskAppLoading = this.appLoading.addLoading();

        this.state = { ...this.state, ready: false };

        this.bootstrap.bootstrap(this.deskId).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.state = { ...this.state, ready: true };

            const setColumns: Function = () => {
                this.deskState.current$.pipe(
                    takeUntil(this.ngOnDestroy$),
                    filter(s => !s.prev || this.state.props.columns.length === 0 || s.prev.columns !== s.current.columns),
                ).subscribe(s => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            columns: s.current.columns,
                        },
                    };

                    this.cdr.detectChanges();
                });
            };

            const setCards: Function = () => {
                this.deskState.current$.pipe(
                    takeUntil(this.ngOnDestroy$),
                    debounceTime(50),
                    filter(s => !s.prev || this.state.props.cards.length === 0 || (s.current.filteredCards !== this.state.props.cards)),
                ).subscribe(s => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            cards: s.current.filteredCards,
                        },
                    };

                    this.cdr.detectChanges();
                });
            };

            const openCardModalFromQueryParams: Function = () => {
                if (this.cardId) {
                    this.ngZone.runOutsideAngular(() => {
                        setTimeout(() => {
                            if (! this.modalService.isOpened()) {
                                this.modalService.openCardModal(this.cardId);
                            }
                        });
                    });
                }
            };

            setColumns();
            setCards();
            openCardModalFromQueryParams();

            appLoading.done();
        });
    }
}
