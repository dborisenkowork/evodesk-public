import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {MatMenu, MatMenuTrigger} from '@angular/material';

import {DeskParticipantModel} from '../../../../../app/EvodeskRESTApi/models/DeskParticipant.model';

interface Props {
    filtersMatMenu: MatMenu;
    participants: Array<DeskParticipantModel>;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskDeskToolbarComponentProps};

@Component({
    selector: 'evodesk-desk-toolbar',
    templateUrl: './EvodeskDeskToolbar.component.pug',
    styleUrls: [
        './EvodeskDeskToolbar.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskToolbarComponent
{
    @Input() props: Props;

    @Output('openFilters') openFiltersEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('addParticipants') addParticipantsEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('filtersMatMenuTrigger', { read: MatMenuTrigger }) filtersMatMenuTrigger: MatMenuTrigger;

    public state: State = {
        ready: true,
    };

    openFilters(): void {
        this.openFiltersEvent.emit(undefined);
    }

    get numParticipants(): string {
        if (this.props.participants.length > 9) {
            return '9+';
        } else {
            return this.props.participants.length.toString();
        }
    }

    addParticipants(): void {
        this.addParticipantsEvent.emit(undefined);
    }
}
