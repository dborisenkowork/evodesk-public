import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {MatDialog, MatDialogRef, MatMenu} from '@angular/material';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {v3Bottom349pxMatDialogConfig, v3Bottom420pxMatDialogConfig} from '../../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {evodeskDeskMobileBreakpoint} from '../../../configs/EvodeskDesk.constants';

import {EvodeskDeskToolbarComponent, EvodeskDeskToolbarComponentProps} from './EvodeskDeskToolbar.component';
import {EvodeskDeskFilterContainerComponent as FilterComponent} from '../EvodeskDeskFilter/EvodeskDeskFilterContainer.component';
import {EvodeskDeskParticipantContainerComponent as ParticipantModal} from '../EvodeskDeskParticipant/EvodeskDeskParticipantContainer.component';

import {EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';

type State = {
    ready: boolean;
    isMobile: boolean;
    props?: EvodeskDeskToolbarComponentProps;
};

const breakpointMobile: number = evodeskDeskMobileBreakpoint;

@Component({
    selector: 'evodesk-desk-toolbar-container',
    template: `
      <ng-container *ngIf="state.ready">
        <mat-menu #filtersMenu>
          <ng-template matMenuContent>
            <evodesk-desk-filter-container (close)="onFiltersMatMenuClose()"></evodesk-desk-filter-container>
          </ng-template>
        </mat-menu>

        <evodesk-desk-toolbar #view [props]="state.props" (openFilters)="openFilters()" (addParticipants)="addParticipants()"></evodesk-desk-toolbar>
      </ng-container>
    `,
})
export class EvodeskDeskToolbarContainerComponent implements OnInit, OnDestroy, AfterViewChecked
{
    @ViewChild('view') view: EvodeskDeskToolbarComponent;
    @ViewChild('filtersMenu', { read: MatMenu }) filtersMenu: MatMenu;

    private filtersClosed$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        isMobile: false,
        props: {
            filtersMatMenu: this.filtersMenu,
            participants: [],
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private breakpointObserver: BreakpointObserver,
        private deskState: EvodeskDeskStateService,
    ) {}

    ngOnInit(): void {
        this.breakpointObserver.observe(`(max-width: ${breakpointMobile}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this.state = {
                ...this.state,
                isMobile: true,
            };
        });

        this.breakpointObserver.observe(`(min-width: ${breakpointMobile + 1}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this.state = {
                ...this.state,
                isMobile: false,
            };
        });

        this.deskState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !! s.current.desk),
            map(s => s.current.desk.members),
            distinctUntilChanged(),
        ).subscribe((participants) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    participants: participants,
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.filtersClosed$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    ngAfterViewChecked(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                filtersMatMenu: this.filtersMenu,
            },
        };
    }

    openFilters(): void {
        if (this.state.isMobile) {
            const matDialogRef: MatDialogRef<FilterComponent> = this.matDialog.open(FilterComponent, {
                ...v3Bottom420pxMatDialogConfig,
                viewContainerRef: this.viewContainerRef,
                closeOnNavigation: true,
            });

            matDialogRef.afterClosed().pipe(takeUntil(this.filtersClosed$)).subscribe(() => {
                this.filtersClosed$.next(undefined);
            });

            matDialogRef.componentInstance.closeEvent.pipe(takeUntil(this.filtersClosed$)).subscribe(() => {
                matDialogRef.close();
            });
        } else {
            this.view.filtersMatMenuTrigger.openMenu();
        }
    }

    onFiltersMatMenuClose(): void {
        this.view.filtersMatMenuTrigger.closeMenu();
    }

    addParticipants(): void {
        const closed$: Subject<void> = new Subject<void>();

        const dialogRef: MatDialogRef<ParticipantModal> = this.matDialog.open(ParticipantModal, {
            ...v3Bottom349pxMatDialogConfig,
            viewContainerRef: this.deskState.snapshot.viewContainerRef,
            closeOnNavigation: true,
            disableClose: true,
        });

        dialogRef.afterClosed().pipe(takeUntil(closed$)).subscribe(() => {
            closed$.next(undefined);
        });

        dialogRef.componentInstance.closeEvent.pipe(takeUntil(closed$)).subscribe(() => {
            dialogRef.close();
        });

        dialogRef.backdropClick().pipe(takeUntil(closed$)).subscribe(() => {
            dialogRef.close();
        });
    }
}
