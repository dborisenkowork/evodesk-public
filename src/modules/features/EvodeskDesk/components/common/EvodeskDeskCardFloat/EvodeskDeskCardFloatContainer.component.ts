import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef, MatMenu, MatMenuTrigger} from '@angular/material';

import {Subject} from 'rxjs';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import {v3Bottom600pxMatDialogConfig} from '../../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {EvodeskDeskCardFloatComponent, EvodeskDeskCardFloatComponentProps} from './EvodeskDeskCardFloat.component';
import {EvodeskDeskDeadlineComponentProps} from '../EvodeskDeskDeadline/EvodeskDeskDeadline.component';
import {EvodeskDeskLabelComponent} from '../EvodeskDeskLabel/EvodeskDeskLabel.component';

import {DeskCardId} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';

import {EvodeskDeskArchiveCardsService} from '../../../services/EvodeskDeskArchiveCards.service';
import {EvodeskDeskDeleteCardsService} from '../../../services/EvodeskDeskDeleteCards.service';

interface State {
    ready: boolean;
    isMobile: boolean;
    matDialogRef?: MatDialogRef<any>;
    props?: EvodeskDeskCardFloatComponentProps;
}

@Component({
    selector: 'evodesk-desk-card-float-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
      <mat-menu #labelMatMenu class="__evodesk-desk-modal">
        <evodesk-desk-label-container #label [card]="this.state.props.card" [variant]="'mat-menu'" (close)="view.labelMatMenuTrigger.closeMenu()"></evodesk-desk-label-container>
      </mat-menu>

      <mat-menu #deadlineMatMenu class="__evodesk-desk-modal">
        <evodesk-desk-deadline-container [props]="commonMatMenuProps" (close)="view.deadlineMatMenuTrigger.closeMenu()"></evodesk-desk-deadline-container>
      </mat-menu>

      <mat-menu #checklistsMatMenu>
        <evodesk-desk-card-checklists-container [cardId]="deskCardId" (close)="view.checklistsMatMenuTrigger.closeMenu()"></evodesk-desk-card-checklists-container>
      </mat-menu>

      <mat-menu #copyLinkMatMenu>
        <evodesk-desk-copy-link-container [props]="commonMatModalProps" (close)="view.copyLinkMatMenu.closeMenu()"></evodesk-desk-copy-link-container>
      </mat-menu>

      <mat-menu #moveMatMenu>
        <evodesk-desk-move-container [props]="commonMatModalProps" (close)="view.moveMatMenu.closeMenu()"></evodesk-desk-move-container>
      </mat-menu>

      <mat-menu #addAttachmentMatMenu>
        <evodesk-desk-add-attachment [props]="commonMatModalProps" (close)="view.addAttachmentMatMenu.closeMenu()" (requestImage)="openAttachImageMatDialog()"></evodesk-desk-add-attachment>
      </mat-menu>

      <ng-template #attachImageMatDialog>
        <evodesk-desk-attach-image-container [props]="commonMatModalProps" (close)="state.matDialogRef.close()"></evodesk-desk-attach-image-container>
      </ng-template>

      <ng-container *ngIf="state.ready">
        <evodesk-desk-card-float
          #view
          [props]="state.props"
          (close)="close()"
          (archive)="archive()"
          (delete)="delete()"
          (openLabel)="openLabel($event)"
          (openDeadline)="openDeadline($event)"
          (openChecklist)="openChecklist($event)"
          (openCopyLink)="openCopyLink($event)"
          (openMove)="openMove($event)"
          (openAddAttachment)="openAddAttachment($event)"
        ></evodesk-desk-card-float>
      </ng-container>
    `,
})
export class EvodeskDeskCardFloatContainerComponent implements OnChanges, OnDestroy
{
    @Input() deskCardId: DeskCardId;
    @Input() srcElement: HTMLElement;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('view') view: EvodeskDeskCardFloatComponent;

    @ViewChild('labelMatMenu', { read: MatMenu }) labelMatMenu: MatMenu;
    @ViewChild('deadlineMatMenu', { read: MatMenu }) deadlineMatMenu: MatMenu;
    @ViewChild('checklistsMatMenu', { read: MatMenu }) checklistsMatMenu: MatMenu;
    @ViewChild('addAttachmentMatMenu', { read: MatMenu }) addAttachmentMatMenu: MatMenu;
    @ViewChild('copyLinkMatMenu', { read: MatMenu }) copyLinkMatMenu: MatMenu;
    @ViewChild('moveMatMenu', { read: MatMenu }) moveMatMenu: MatMenu;

    @ViewChild('labelMatDialog') labelMatDialogRef: TemplateRef<EvodeskDeskLabelComponent>;

    @ViewChild('deadlineMatDialog') deadlineMatDialogRef: TemplateRef<any>;
    @ViewChild('addChecklistMatDialog') addChecklistMatDialog: TemplateRef<any>;
    @ViewChild('copyLinkMatDialog') copyLinkMatDialog: TemplateRef<any>;
    @ViewChild('moveMatDialog') moveMatDialog: TemplateRef<any>;
    @ViewChild('addAttachmentMatDialog') addAttachmentMatDialog: TemplateRef<any>;
    @ViewChild('attachImageMatDialog') attachImageMatDialog: TemplateRef<any>;

    private nextCardId$: Subject<DeskCardId> = new Subject<DeskCardId>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        isMobile: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private deskState: EvodeskDeskStateService,
        private archiveCardService: EvodeskDeskArchiveCardsService,
        private deleteCardService: EvodeskDeskDeleteCardsService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['deskCardId']) {
            if (this.deskCardId) {
                this.nextCardId$.next(this.deskCardId);

                this.deskState.current$.pipe(
                    takeUntil(this.nextCardId$),
                    map(s => s.current.sourceCards.filter(sc => sc.id === this.deskCardId)[0]),
                    distinctUntilChanged(),
                ).subscribe((card) => {
                    this.state = {
                        ...this.state,
                        ready: true,
                        props: {
                            card: card,
                            srcElement: this.srcElement,
                            labelMatMenu: this.labelMatMenu,
                            deadlineMatMenu: this.deadlineMatMenu,
                            checklistsMatMenu: this.checklistsMatMenu,
                            addAttachmentMatMenu: this.addAttachmentMatMenu,
                            moveMatMenu: this.moveMatMenu,
                            copyLinkMatMenu: this.copyLinkMatMenu,
                        },
                    };

                    this.cdr.detectChanges();
                });
            } else {
                this.nextCardId$.next(undefined);

                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.nextCardId$.next(this.deskCardId);
        this.ngOnDestroy$.next(undefined);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    archive(): void {
        this.archiveCardService.askForArchiveSingleCard(this.state.props.card.deskId, this.deskCardId)
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe((isConfirmed) => {
                if (isConfirmed) {
                    this.close();
                }
            });
    }

    delete(): void {
        this.deleteCardService.askForDeleteSingleCard(this.state.props.card.deskId, this.deskCardId)
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe((isConfirmed) => {
                if (isConfirmed) {
                    this.close();
                }
            });
    }

    openLabel($event: MatMenuTrigger): void {
        $event.openMenu();
    }

    openDeadline($event: MatMenuTrigger): void {
        $event.openMenu();
    }

    openChecklist($event: MatMenuTrigger): void {
        $event.openMenu();
    }

    openCopyLink($event: MatMenuTrigger): void {
        $event.openMenu();
    }

    openMove($event: MatMenuTrigger): void {
        $event.openMenu();
    }

    openAddAttachment($event: MatMenuTrigger): void {
        $event.openMenu();
    }

    openAttachImageMatDialog(): void {
        setTimeout(() => {
            this.state = {
                ...this.state,
                matDialogRef: this.matDialog.open(this.attachImageMatDialog, {
                    ...v3Bottom600pxMatDialogConfig,
                    autoFocus: false,
                    viewContainerRef: this.viewContainerRef,
                    closeOnNavigation: true,
                    disableClose: true,
                }),
            };

            this.state.matDialogRef.backdropClick().pipe(takeUntil(this.state.matDialogRef.afterClosed())).subscribe(() => {
                this.state.matDialogRef.close();
            });
        });
    }

    get commonMatMenuProps(): EvodeskDeskDeadlineComponentProps {
        return {
            card: this.state.props.card,
            variant: 'mat-menu',
        };
    }

    get commonMatModalProps(): EvodeskDeskDeadlineComponentProps {
        return {
            card: this.state.props.card,
            variant: 'modal',
        };
    }
}
