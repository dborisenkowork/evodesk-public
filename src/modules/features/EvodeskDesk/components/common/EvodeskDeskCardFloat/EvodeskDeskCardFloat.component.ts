import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild} from '@angular/core';
import {ViewportRuler} from '@angular/cdk/overlay';

import {Subject} from 'rxjs';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {MatMenu, MatMenuTrigger} from '@angular/material';

interface Props {
    card: DeskCardModel;
    srcElement: HTMLElement;
    deadlineMatMenu?: MatMenu;
    labelMatMenu?: MatMenu;
    checklistsMatMenu?: MatMenu;
    addAttachmentMatMenu?: MatMenu;
    moveMatMenu?: MatMenu;
    copyLinkMatMenu?: MatMenu;
}

interface State {
    ready: boolean;
    destroyed: boolean;
    inverseX: boolean;
    inverseY: boolean;
    top: string;
    left: string;
    width: string;
}

export {Props as EvodeskDeskCardFloatComponentProps};

@Component({
    selector: 'evodesk-desk-card-float',
    templateUrl: './EvodeskDeskCardFloat.component.pug',
    styleUrls: [
        './EvodeskDeskCardFloat.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskCardFloatComponent implements OnChanges, OnDestroy, AfterViewInit
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('archive') archiveEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('delete') deleteEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openLabel') openLabelEvent: EventEmitter<MatMenuTrigger> = new EventEmitter<MatMenuTrigger>();
    @Output('openDeadline') openDeadlineEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openChecklist') openChecklistEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openCopyLink') openCopyLinkEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openMove') openMoveEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openAddAttachment') openAddAttachmentEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('container') containerRef: ElementRef;
    @ViewChild('card') cardRef: ElementRef;
    @ViewChild('actions') actionsRef: ElementRef;

    @ViewChild('sLabelMatMenuTrigger', { read: MatMenuTrigger }) labelMatMenuTrigger: MatMenuTrigger;
    @ViewChild('sDeadlineMatMenuTrigger', { read: MatMenuTrigger }) deadlineMatMenuTrigger: MatMenuTrigger;
    @ViewChild('sChecklistsMatMenuTrigger', { read: MatMenuTrigger }) checklistsMatMenuTrigger: MatMenuTrigger;
    @ViewChild('sAddAttachmentMatMenu', { read: MatMenuTrigger }) addAttachmentMatMenu: MatMenuTrigger;
    @ViewChild('sMoveMatMenu', { read: MatMenuTrigger }) moveMatMenu: MatMenuTrigger;
    @ViewChild('sCopyLinkMatMenu', { read: MatMenuTrigger }) copyLinkMatMenu: MatMenuTrigger;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        destroyed: false,
        inverseX: false,
        inverseY: false,
        top: 'auto',
        left: 'auto',
        width: 'auto',
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private viewport: ViewportRuler,
        private ngZone: NgZone,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props && this.props.card) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngAfterViewInit(): void {
        const inverses: Function = () => {
            this.ngZone.runOutsideAngular(() => {
                const tick: Function = () => {
                    if (this.containerRef) {
                        this.update();
                    }

                    if (! this.state.destroyed) {
                        window.requestAnimationFrame(() => {
                            tick();
                        });
                    }
                };

                tick();
            });
        };

        inverses();

        this.cdr.detectChanges();
    }

    ngOnDestroy(): void {
        this.state = {
            ...this.state,
            destroyed: true,
        };

        this.ngOnDestroy$.next(undefined);
    }

    get srcElement(): Element {
        return document.querySelector(`[data-evodesk-desk-card="${this.props.card.id}"]`);
    }

    get containerNgStyle(): any {
        return {
            top: this.state.top,
            left: this.state.left,
            width: this.state.width,
        };
    }

    get cardNgStyle(): any {
        const rect: ClientRect = this.srcElement.getBoundingClientRect();

        return {
            'width': `${rect.width}px`,
        };
    }

    update(): void {
        if (this.state.destroyed) {
            return;
        }

        const paddingX: number = 186;
        const paddingY: number = 20;

        const cardRect: ClientRect = this.srcElement.getBoundingClientRect();
        const cardActions: ClientRect = (this.actionsRef.nativeElement as HTMLElement).getBoundingClientRect();

        const maxLeft: number = cardRect.width + cardRect.left + paddingX;

        const exceedX: boolean = maxLeft > this.viewport.getViewportRect().width;
        const exceedY: boolean = (Math.max(cardRect.height, cardActions.height) + cardRect.top + paddingY) > this.viewport.getViewportRect().height;

        if (exceedX && ! this.state.inverseX) {
            this.state = {
                ...this.state,
                inverseX: true,
            };

            this.cdr.detectChanges();
        } else if (! exceedX && this.state.inverseX) {
            this.state = {
                ...this.state,
                inverseX: false,
            };

            this.cdr.detectChanges();
        }

        if (exceedY && ! this.state.inverseY) {
            this.state = {
                ...this.state,
                inverseY: true,
            };

            this.cdr.detectChanges();
        } else if (! exceedY && this.state.inverseY) {
            this.state = {
                ...this.state,
                inverseY: false,
            };

            this.cdr.detectChanges();
        }

        const rTop: string = `${cardRect.top}px`;
        const rLeft: string = `${Math.min(cardRect.left, maxLeft)}px`;
        const rWidth: string = `${cardRect.width}px`;

        if (rWidth !== this.state.width || rLeft !== this.state.left || rWidth !== this.state.width) {
            this.state = {
                ...this.state,
                top: rTop,
                left: rLeft,
                width: rWidth,
            };

            this.cdr.detectChanges();
        }

        (this.containerRef.nativeElement as HTMLElement).style.top = `${cardRect.top}px`;
        (this.containerRef.nativeElement as HTMLElement).style.left = `${Math.min(cardRect.left, maxLeft)}px`;
        (this.cardRef.nativeElement as HTMLElement).style.width = `${cardRect.width}px`;
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
