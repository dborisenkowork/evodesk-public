import {AfterViewChecked, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, OnChanges, SimpleChanges, ViewChild} from '@angular/core';

import {DeskCardAttachmentModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardAttachment.model';

interface Props {
    loading: boolean;
    attachment: DeskCardAttachmentModel;
    maxHeight: number;
    objectUrl?: any;
}

interface State {
    ready: boolean;
    imageNgStyle: any;
    recalculate: boolean;
}

export {Props as EvodeskDeskAttachmentImageComponentProps};

@Component({
    selector: 'evodesk-desk-attachment-image',
    templateUrl: './EvodeskDeskAttachmentImage.component.pug',
    styleUrls: ['./EvodeskDeskAttachmentImage.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskAttachmentImageComponent implements OnChanges, AfterViewChecked
{
    @Input() props: Props;

    @ViewChild('imageContainer') imageContainerRef: ElementRef;

    public state: State = {
        ready: false,
        imageNgStyle: {},
        recalculate: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    recalculate: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngAfterViewChecked(): void {
        if (this.state.recalculate) {
            this.state = {
                ...this.state,
                imageNgStyle: this.imageNgStyle,
                recalculate: false,
            };

            this.cdr.detectChanges();
        }
    }

    get imageNgStyle(): any {
        const width: number = this.props.attachment.width;
        const height: number = this.props.attachment.height;
        const maxWidth: number = (this.imageContainerRef.nativeElement as HTMLElement).clientWidth;

        if (width < maxWidth) {
            if (height < this.props.maxHeight) {
                return {
                    'width': 'auto',
                    'max-width': '100%',
                    'height': 'auto',
                };
            } else {
                const resultHeight: number = height * (height / this.props.maxHeight);

                if (resultHeight > this.props.maxHeight) {
                    return {
                        'width': 'auto',
                        'max-width': '100%',
                        'height': Math.min(this.props.maxHeight, Math.ceil(height * (this.props.maxHeight / height))) + 'px',
                    };
                } else {
                    return {
                        'width': 'auto',
                        'max-width': '100%',
                        'height': 'auto',
                    };
                }
            }
        } else {
            return {
                'width': 'auto',
                'max-width': '100%',
                'height': 'auto',
            };
        }
    }
}
