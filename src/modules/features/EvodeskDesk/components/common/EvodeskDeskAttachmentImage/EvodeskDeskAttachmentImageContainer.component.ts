import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';

import {Subject} from 'rxjs';
import {delay, retryWhen, takeUntil} from 'rxjs/operators';

import {DeskCardAttachmentModel} from '../../../../../app/EvodeskRESTApi/models/DeskCardAttachment.model';

import {EvodeskDeskAttachmentImageComponentProps} from './EvodeskDeskAttachmentImage.component';

import {DeskCardId} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskId} from '../../../../../app/EvodeskRESTApi/models/Desk.model';

import {environment} from '../../../../../../environments/environment.config';

import {AuthTokenService} from '../../../../EvodeskAuth/services/AuthToken.service';
import {AttachmentId} from '../../../../../app/EvodeskRESTApi/models/post/Post.model';

interface CacheEntry { aId: AttachmentId; objectUrl: any; }

interface State {
    ready: boolean;
    props?: EvodeskDeskAttachmentImageComponentProps;
}

const cached: Array<CacheEntry> = [];

@Component({
    selector: 'evodesk-desk-attachment-image-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <evodesk-desk-attachment-image [props]="state.props"></evodesk-desk-attachment-image>
    `,
})
export class EvodeskDeskAttachmentImageContainerComponent implements OnChanges, OnDestroy
{
    @Input() deskId: DeskId;
    @Input() cardId: DeskCardId;
    @Input() attachment: DeskCardAttachmentModel;
    @Input() maxHeight: number;

    private nextImage$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private http: HttpClient,
        private auth: AuthTokenService,
        private cdr: ChangeDetectorRef,
        private sanitize: DomSanitizer,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['maxHeight']) {
            if (this.attachment && this.state.props) {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        maxHeight: this.maxHeight,
                    },
                };
            }
        }

        if (changes['attachment']) {
            this.load();
        } else {
            this.state = {
                ...this.state,
                ready: false,
                props: undefined,
            };
        }
    }

    ngOnDestroy(): void {
        this.nextImage$.next(undefined);
    }

    get link(): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/desk/${this.deskId}/card/${this.cardId}/attachment/${this.attachment.id}/download?access_token=${this.auth.token.access_token}`;
    }

    load(): void {
        this.nextImage$.next(undefined);

        const alreadyLoaded: CacheEntry = cached.filter(c => c.aId === this.attachment.id)[0];

        if (alreadyLoaded) {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    loading: false,
                    objectUrl: alreadyLoaded.objectUrl,
                    attachment: this.attachment,
                    maxHeight: this.maxHeight,
                },
            };
        } else {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    loading: true,
                    attachment: this.attachment,
                    maxHeight: this.maxHeight,
                },
            };

            this.http.get(this.link, { responseType: 'blob' }).pipe(
                takeUntil(this.nextImage$),
                retryWhen(errors => errors.pipe(delay(1000))),
            ).subscribe((blob: Blob) => {
                const objectUrl: any = this.sanitize.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));

                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        loading: false,
                        objectUrl: objectUrl,
                    },
                };

                cached.push({
                    aId: this.attachment.id,
                    objectUrl: objectUrl,
                });

                this.cdr.detectChanges();
            });
        }
    }
}

