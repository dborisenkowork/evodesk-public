import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import * as moment from 'moment';

import {Subject} from 'rxjs';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../../types/Backend.types';

import {EvodeskDeskDeadlineComponentProps, EvodeskDeskDeadlineComponentSetDeadlineEvent} from './EvodeskDeskDeadline.component';

import {EvodeskDeskUpdateCardService} from '../../../services/EvodeskDeskUpdateCard.service';
import {DeskUpdateCardChangedField} from '../../../../../app/EvodeskRESTApi/services/DeskREST.service';

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-desk-deadline-container',
    template: `
      <ng-container *ngIf="state.ready">
        <evodesk-desk-deadline [props]="props" (setDeadline)="setDeadline($event)" (close)="closeEvent.emit($event)"></evodesk-desk-deadline>
      </ng-container>
    `,
})
export class EvodeskDeskDeadlineContainerComponent implements OnChanges, OnDestroy
{
    @Input() props: EvodeskDeskDeadlineComponentProps;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private updateCardService: EvodeskDeskUpdateCardService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.props) {
            this.state = {
                ...this.state,
                ready: true,
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    setDeadline($event: EvodeskDeskDeadlineComponentSetDeadlineEvent): void {
        this.updateCardService.updateCard(DeskUpdateCardChangedField.Deadline, { ...(this.props.card as DeskCardModel), deadline: $event ? moment($event).format(BACKEND_DATE_FORMAT_AS_MOMENT) : undefined }).subscribe();
    }
}
