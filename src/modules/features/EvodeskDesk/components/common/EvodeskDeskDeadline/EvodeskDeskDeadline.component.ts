import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatCalendar} from '@angular/material';
import {TextMaskConfig} from 'angular2-text-mask';

import {Moment} from 'moment';
import * as moment from 'moment';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../../types/Backend.types';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskAlertModalService} from '../../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';

interface State {
    ready: boolean;
    form: FormGroup;
    isDateInputFocused: boolean;
    isTimeInputFocused: boolean;
    dateInputMask: TextMaskConfig;
    timeInputMask: TextMaskConfig;
}

interface Props {
    card: DeskCardModel;
    variant: Variant;
}

interface FormValue {
    matDate?: null | undefined | Date;
    timeInput?: string;
    dateInput?: string;
}

type Variant = 'modal' | 'control' | 'mat-menu';

type SetDeadlineEvent = Date | undefined;

const defaultTime: string = '12:00';

export {Props as EvodeskDeskDeadlineComponentProps};
export {Variant as EvodeskDeskDeadlineComponentVariant};
export {SetDeadlineEvent as EvodeskDeskDeadlineComponentSetDeadlineEvent};

@Component({
    selector: 'evodesk-desk-deadline',
    templateUrl: './EvodeskDeskDeadline.component.pug',
    styleUrls: [
        './EvodeskDeskDeadline.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskDeskDeadlineComponent implements OnChanges, OnDestroy
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('setDeadline') setDeadlineEvent: EventEmitter<SetDeadlineEvent> = new EventEmitter<SetDeadlineEvent>();

    @ViewChild('matCalendar') matCalendar: MatCalendar<any>;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        form: this.fb.group({
            matDate: [null, [Validators.required]],
            timeInput: [null, [Validators.required]],
            dateInput: [''],
        }),
        isDateInputFocused: false,
        isTimeInputFocused: false,
        dateInputMask: {
            mask: [/\d/, /\d/, '.', /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/],
        },
        timeInputMask: {
            mask: [/\d/, /\d/, ':', /\d/, /\d/],
        },
    };

    constructor(
        private fb: FormBuilder,
        private cdr: ChangeDetectorRef,
        private uiAlert: EvodeskAlertModalService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props'] && ! this.state.ready) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };

                if (this.props.card.deadline) {
                    const d: Date = moment(this.props.card.deadline, BACKEND_DATE_FORMAT_AS_MOMENT).toDate();

                    this.state.form.patchValue(<FormValue>{
                        matDate: d,
                        dateInput: moment(d).format('DD.MM.YYYY'),
                        timeInput: moment(d).format('HH:mm'),
                    });
                } else {
                    const now: Date = new Date();

                    this.state.form.patchValue(<FormValue>{
                        matDate: now,
                        dateInput: moment(now).format('DD.MM.YYYY'),
                        timeInput: defaultTime,
                    });
                }
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    get shouldDisplayModalHeader(): boolean {
        return this.props.variant !== 'control';
    }

    get matCalendarDate(): Date {
        return this.formValue.matDate;
    }

    clear(): void {
        this.state.form.patchValue(<FormValue>{
            matDate: null,
            dateInput: null,
            timeInput: null,
        });

        this.setDeadlineEvent.emit(undefined);
        this.close();
    }

    submit(): void {
        const invalid: Function = (() => {
            this.uiAlert.openTranslated('EvodeskDesk.components.common.EvodeskDeskDeadline.InvalidDate').pipe(takeUntil(this.ngOnDestroy$)).subscribe();
        });

        if (this.formValue.dateInput && this.formValue.timeInput) {
            const set: Moment = moment(`${this.formValue.dateInput} ${this.formValue.timeInput}`, 'DD.MM.YYYY HH:mm');

            if (set.isValid()) {
                this.setDeadlineEvent.emit(set.toDate());
                this.close();
            } else {
                invalid();
            }
        } else {
            invalid();
        }
    }

    onSelectedChange(v: Date): void {
        this.state.form.patchValue(<FormValue>{
            matDate: v,
            dateInput: moment(v).format('DD.MM.YYYY'),
        });

        this.matCalendar.activeDate = v;

        if (! this.formValue.timeInput) {
            this.state.form.patchValue(<FormValue>{
                timeInput: defaultTime,
            });
        }

        this.cdr.detectChanges();
    }

    onDateInputBlur(): void {
        const date: Moment = moment(this.formValue.dateInput, 'DD.MM.YYYY');

        if (! date.isValid()) {
            if (this.props.card.deadline) {
                const d: Date = moment(this.props.card.deadline, BACKEND_DATE_FORMAT_AS_MOMENT).toDate();

                this.state.form.patchValue(<FormValue>{
                    dateInput: moment(d).format('DD.MM.YYYY'),
                });
            } else {
                this.state.form.patchValue(<FormValue>{
                    dateInput: moment(new Date()).format('DD.MM.YYYY'),
                });
            }
        } else {
            this.state.form.patchValue(<FormValue>{
                matDate: date.toDate(),
            });

            if (! this.formValue.timeInput) {
                this.state.form.patchValue(<FormValue>{
                    timeInput: defaultTime,
                });
            }
        }
    }

    onTimeInputBlur(): void {
        const time: Moment = moment(this.formValue.timeInput, 'HH:mm');

        if (! time.isValid()) {
            if (this.props.card.deadline) {
                const d: Date = moment(this.props.card.deadline, BACKEND_DATE_FORMAT_AS_MOMENT).toDate();

                this.state.form.patchValue(<FormValue>{
                    timeInput: moment(d).format('HH:mm'),
                });
            } else {
                this.state.form.patchValue(<FormValue>{
                    timeInput: defaultTime,
                });
            }
        }
    }
}
