import {Component, EventEmitter, Input, OnDestroy, Output, ViewChild} from '@angular/core';

import * as R from 'ramda';

import {Subject} from 'rxjs';
import {take, filter, takeUntil} from 'rxjs/operators';

import {proxy} from '../../../../../../functions/proxy.function';

import {EvodeskCardAddFormComponentFormValue, EvodeskDeskAddFormComponent} from './EvodeskDeskAddForm.component';

import {EvodeskDeskAddCardService} from '../../../services/EvodeskDeskAddCard.service';
import {EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';

@Component({
    selector: 'evodesk-desk-card-add-form-container',
    template: `
      <evodesk-desk-card-add-form
        #view
        (create)="create($event, true)"
        (enter)="create($event)"
        (close)="closeEvent.emit(undefined)"
        (created)="createdEvent.emit($event)"
      ></evodesk-desk-card-add-form>
    `,
})
export class EvodeskDeskAddFormContainerComponent implements OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    @Input() columnId: number;
    @Input() deskId: number;

    @Output('submitted') submittedEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output('created') createdEvent: EventEmitter<undefined> = new EventEmitter();

    @ViewChild('view') view: EvodeskDeskAddFormComponent;

    constructor(
        private add: EvodeskDeskAddCardService,
        private state: EvodeskDeskStateService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    sendSubmitAction(): void {
        this.view.tryCreate();
    }

    create($event: EvodeskCardAddFormComponentFormValue, autoClose: boolean = false): void {
        this.state.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(states => !!states.current && !!states.current.columns),
            take(1),
        ).subscribe(states => {
            const cardsInColumns: Array<number> = states.current.sourceCards
                .filter(c => c.column === this.columnId)
                .map(c => c.order);

            proxy(this.add.addCard({
                title: $event.title,
                column: this.columnId,
                order: cardsInColumns.length ? cardsInColumns.reduce(R.max) + 1 : 1,
                project: this.state.snapshot.currentProject,
            })).subscribe(() => {
                this.submittedEvent.emit(undefined);
            });
        });

        if (autoClose) {
            this.closeEvent.emit(undefined);
        }
    }
}
