import {AfterViewInit, Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as keycodes from '@angular/cdk/keycodes';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

export interface EvodeskCardAddFormComponentFormValue
{
    title: string;
}

interface State
{
    sendButtonShouldBeVisible: boolean;
}

@Component({
    selector: 'evodesk-desk-card-add-form',
    templateUrl: './EvodeskDeskAddForm.component.pug',
    styleUrls: [
        './EvodeskDeskAddForm.component.scss',
    ],
})
export class EvodeskDeskAddFormComponent implements OnInit, OnDestroy, AfterViewInit
{
    @ViewChild('titleTextarea') titleTextarea: ElementRef;

    @Output('create') createEvent: EventEmitter<EvodeskCardAddFormComponentFormValue> = new EventEmitter<EvodeskCardAddFormComponentFormValue>();
    @Output('enter') enterEvent: EventEmitter<EvodeskCardAddFormComponentFormValue> = new EventEmitter<EvodeskCardAddFormComponentFormValue>();
    @Output('close') closeEvent: EventEmitter<EvodeskCardAddFormComponentFormValue> = new EventEmitter<EvodeskCardAddFormComponentFormValue>();

    @Output('created') createdEvent: EventEmitter<undefined> = new EventEmitter();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        sendButtonShouldBeVisible: false,
    };

    public form: FormGroup = this.fb.group({
        title: ['', [Validators.required, Validators.maxLength(65535)]],
    });

    constructor(
        private fb: FormBuilder,
    ) {}

    ngOnInit(): void {
        this.form.valueChanges.pipe(takeUntil(this.ngOnDestroy$)).subscribe(next => {
            this.state = {
                ...this.state,
                sendButtonShouldBeVisible: !!next.title && next.title.length > 0,
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            if (this.titleTextarea && this.titleTextarea.nativeElement) {
                this.titleTextarea.nativeElement.focus();
            }
        });
    }

    enterKeyDown($event: KeyboardEvent): void {
        $event.preventDefault();
    }

    handlePropagation($event) {
        if ($event.keyCode === keycodes.UP_ARROW || $event.keyCode === keycodes.DOWN_ARROW ||
                $event.keyCode === keycodes.LEFT_ARROW || $event.keyCode === keycodes.RIGHT_ARROW) {
            $event.stopPropagation();
        }
    }

    enter($event?: KeyboardEvent): void {
        $event.preventDefault();

        if (!!this.form.value.title && this.form.value.title.length) {
            this.enterEvent.emit(this.form.value);

            setTimeout(() => {
                this.reset();
                this.createdEvent.emit(undefined);
            });
        } else {
            this.close();
        }
    }

    onBlur(): void {
        if (! (this.form.value.title && this.form.value.title.length > 0)) {
            this.close();
        }
    }

    close(): void {
        this.form.reset();
        this.closeEvent.emit(this.form.value);
    }

    reset(): void {
        this.form.reset();

        if (this.titleTextarea && this.titleTextarea.nativeElement) {
            this.titleTextarea.nativeElement.focus();
        }
    }

    tryCreate(): void {
        if (this.form.valid) {
            this.createEvent.emit(this.form.value);
        }
    }
}
