import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

interface State
{
    ready: boolean;
    isSearchFieldFocused: boolean;
}

@Component({
    selector: 'evodesk-desk-query-string-filter',
    templateUrl: './EvodeskDeskQueryStringFilter.component.pug',
    styleUrls: [
        './EvodeskDeskQueryStringFilter.component.scss',
    ],
})
export class EvodeskDeskQueryStringFilterComponent implements OnInit, OnDestroy
{
    @Output('next') nextEvent: EventEmitter<string> = new EventEmitter<string>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        isSearchFieldFocused: false,
    };

    public form: FormGroup = this.formBuilder.group({
        search: [''],
    });

    constructor(
        private formBuilder: FormBuilder,
    ) {}

    ngOnInit(): void {
        this.form.get('search').valueChanges.pipe(takeUntil(this.ngOnDestroy$)).subscribe(next => {
            this.nextEvent.emit(next);
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    clear(): void {
        this.form.setValue({
            search: '',
        });

        this.nextEvent.emit('');
    }

    onSearchInputFocus(): void {
        this.state = {
            ...this.state,
            isSearchFieldFocused: true,
        };
    }

    onSearchInputBlur(): void {
        this.state = {
            ...this.state,
            isSearchFieldFocused: false,
        };
    }

    get hasInput(): boolean {
        return !! this.form.get('search').value;
    }
}
