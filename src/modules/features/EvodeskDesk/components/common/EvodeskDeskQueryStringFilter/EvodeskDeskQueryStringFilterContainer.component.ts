import {Component} from '@angular/core';

import {EvodeskDeskQueryStringFilter} from '../../../filters/EvodeskDeskQueryString.filter';

@Component({
    selector: 'evodesk-desk-query-string-filter-container',
    template: `
      <evodesk-desk-query-string-filter (next)="doSearch($event)"></evodesk-desk-query-string-filter>
    `,
})
export class EvodeskDeskQueryStringFilterContainerComponent
{
    constructor(
        private queryStringFilter: EvodeskDeskQueryStringFilter,
    ) {}

    doSearch(queryString: string): void {
        this.queryStringFilter.setOptions({
            queryString: queryString,
        });
    }
}
