import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewContainerRef} from '@angular/core';

import {Actions, EvodeskDeskColumnContextMenuComponentProps} from './EvodeskDeskColumnContextMenu.component';

import {DeskColumnDefinitionModel, DeskColumnType} from '../../../../../app/EvodeskRESTApi/models/DeskColumnDefinition.model';
import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskColumnContextMenuService} from './EvodeskDeskColumnContextMenu.service';

type State = EvodeskDeskColumnContextMenuComponentProps;

@Component({
    selector: 'evodesk-desk-column-context-menu-container',
    template: `
        <ng-container *ngIf="!!state">
          <evodesk-desk-column-context-menu
            [props]="state"
            (close)="close()"
            (action)="doAction($event)"
          ></evodesk-desk-column-context-menu>
        </ng-container>
    `,
    providers: [
        EvodeskDeskColumnContextMenuService,
    ],
})
export class EvodeskDeskColumnContextMenuContainerComponent implements OnChanges
{
    @Input() displayHeader: boolean;
    @Input() column: DeskColumnDefinitionModel;
    @Input() columnCards: Array<DeskCardModel>;
    @Input() columnViewContainerRef: ViewContainerRef;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        displayHeader: false,
        columnType: DeskColumnType.CommittedToday,
        numCardsInColumn: 0,
        numNonSystemCardsInColumn: 0,
    };

    constructor(
        private service: EvodeskDeskColumnContextMenuService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (this.column) {
            this.state = {
                ...this.state,
                displayHeader: this.displayHeader,
                columnType: this.column.type,
                numCardsInColumn: this.columnCards.length,
                numNonSystemCardsInColumn: this.columnCards.filter(c => ! c.system).length,
            };
        }
    }

    doAction(action: Actions): void {
        this.state = {
            ...this.state,
        };

        switch (action) {
            default:
                throw new Error(`No actions available for action "${action}"`);

            case Actions.AddCard:
                this.service.actionAddCard(this.column.id);
                break;

            case Actions.MoveCards:
                this.service.actionMoveCardsOfColumn(this.column.id, this.columnViewContainerRef);
                break;

            case Actions.DeleteCards:
            case Actions.Clear:
                this.service.actionDeleteAllCardsOfColumn(this.column.id);
                break;

            case Actions.SortByDateCreatedAsc:
                this.service.actionSortByDateCreatedAtAsc(this.column.id);
                break;

            case Actions.SortByDateCreatedDesc:
                this.service.actionSortByDateCreatedAtDesc(this.column.id);
                break;
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}

