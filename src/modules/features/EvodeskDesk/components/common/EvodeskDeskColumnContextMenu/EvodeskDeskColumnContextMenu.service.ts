import {Injectable, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {v3Bottom420pxMatDialogConfig} from '../../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {DeskCardModel} from '../../../../../app/EvodeskRESTApi/models/DeskCard.model';

import {EvodeskDeskMoveMultiContainerComponent} from '../EvodeskDeskMoveMulti/EvodeskDeskMoveMultiContainer.component';

import {EvodeskDeskAction, EvodeskDeskStateService} from '../../../state/EvodeskDeskState.service';

import {EvodeskDeskDeleteCardsService} from '../../../services/EvodeskDeskDeleteCards.service';
import {EvodeskDeskUpdateCardService} from '../../../services/EvodeskDeskUpdateCard.service';
import {DeskUpdateCardChangedField} from '../../../../../app/EvodeskRESTApi/services/DeskREST.service';

@Injectable()
export class EvodeskDeskColumnContextMenuService
{
    constructor(
        private matDialog: MatDialog,
        private deskState: EvodeskDeskStateService,
        private deleteCardService: EvodeskDeskDeleteCardsService,
        private updateCardService: EvodeskDeskUpdateCardService,
    ) {}

    actionAddCard(columnId: number): void {
        this.deskState.dispatch({
            type: EvodeskDeskAction.OpenAddCardForm,
            payload: {
                columnId: columnId,
            },
        });
    }

    actionMoveCardsOfColumn(columnId: number, viewContainerRef: ViewContainerRef): void {
        const closed$: Subject<void> = new Subject<void>();

        const dialogRef: MatDialogRef<EvodeskDeskMoveMultiContainerComponent> = this.matDialog.open(EvodeskDeskMoveMultiContainerComponent, {
            ...v3Bottom420pxMatDialogConfig,
            closeOnNavigation: true,
            viewContainerRef: viewContainerRef,
        });

        dialogRef.afterClosed().pipe(takeUntil(closed$)).subscribe(() => {
            closed$.next(undefined);
        });

        dialogRef.componentInstance.closeEvent.pipe(takeUntil(closed$)).subscribe(() => {
            dialogRef.close();
        });

        dialogRef.componentInstance.use(columnId);
    }

    actionDeleteAllCardsOfColumn(columnId: number): void {
        this.deleteCardService.askForDeleteManyCards(this.deskState.snapshot.desk.id, this.deskState.snapshot.sourceCards.filter(c => c.column === columnId && ! c.system).map(c => c.id)).subscribe();
    }

    actionSortByDateCreatedAtAsc(columnId: number): void {
        const cards: Array<DeskCardModel> = this.deskState.snapshot.sourceCards
            .filter(c => c.column === columnId && ! c.system);

        cards.sort((a, b) => {
            if (a.id < b.id) {
                return -1;
            } else if (a.id === b.id) {
                return 0;
            } else {
                return 1;
            }
        });

        let index: number = 1;

        this.updateCardService.updateCards(this.deskState.snapshot.desk.id, cards.map(c => {
            return {
                card: {
                    ...c,
                    order: index++,
                },
                changedField: DeskUpdateCardChangedField.Order,
            };
        }));
    }

    actionSortByDateCreatedAtDesc(columnId: number): void {
        const cards: Array<DeskCardModel> = this.deskState.snapshot.sourceCards
            .filter(c => c.column === columnId && ! c.system);

        cards.sort((a, b) => {
            if (a.id > b.id) {
                return -1;
            } else if (a.id === b.id) {
                return 0;
            } else {
                return 1;
            }
        });

        let index: number = 1;

        this.updateCardService.updateCards(this.deskState.snapshot.desk.id, cards.map(c => {
            return {
                card: {
                    ...c,
                    order: index++,
                },
                changedField: DeskUpdateCardChangedField.Order,
            };
        }));
    }
}
