import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {DeskColumnType} from '../../../../../app/EvodeskRESTApi/models/DeskColumnDefinition.model';

export interface EvodeskDeskColumnContextMenuComponentProps
{
    displayHeader: boolean;
    columnType: DeskColumnType;
    numCardsInColumn: number;
    numNonSystemCardsInColumn: number;
}

enum ListSet
{
    FullList = <any>'full-list',
    ClearOnly = <any>'clear-only',
}

export enum Actions
{
    AddCard = <any>'add-card',
    MoveCards = <any>'move-cards',
    DeleteCards = <any>'remove-cards',
    SortByDateCreatedAsc = <any>'sort-by-date-created-at-asc',
    SortByDateCreatedDesc = <any>'sort-by-date-created-at-desc',
    Clear = <any>'clear',
}

interface State
{
    listSet: ListSet;
}

@Component({
    selector: 'evodesk-desk-column-context-menu',
    templateUrl: './EvodeskDeskColumnContextMenu.component.pug',
    styleUrls: [
        './EvodeskDeskColumnContextMenu.component.scss',
    ],
})
export class EvodeskDeskColumnContextMenuComponent implements OnChanges
{
    @Input() props: EvodeskDeskColumnContextMenuComponentProps;

    @Output('action') actionEvent: EventEmitter<Actions> = new EventEmitter<Actions>();
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        listSet: ListSet.ClearOnly,
    };

    get listSet(): string {
        return <any>this.state.listSet as string;
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.state = {
            ...this.state,
            listSet: !!~[
                DeskColumnType.Backlog,
                DeskColumnType.CommittedWeek,
                DeskColumnType.CommittedToday,
            ].indexOf(this.props.columnType) ? ListSet.FullList : ListSet.ClearOnly,
        };
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    doAction(action: Actions) {
        this.actionEvent.emit(action);
    }
}
