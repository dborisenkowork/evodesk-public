import {Injectable} from '@angular/core';

import {forkJoin as observableForkJoin, Observable, Subject} from 'rxjs';
import {publishLast, takeUntil} from 'rxjs/operators';

import {DeskId} from '../../../app/EvodeskRESTApi/models/Desk.model';

import {DeskRESTService} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {EvodeskDeskAction, EvodeskDeskStateBootstrap, EvodeskDeskStateService} from '../state/EvodeskDeskState.service';
import {CurrentUserService} from '../../EvodeskAuth/services/CurrentUser.service';

interface Payload {
    bootstrap: EvodeskDeskStateBootstrap;
}

@Injectable()
export class BootstrapEvodeskDeskScript
{
    constructor(
        private state: EvodeskDeskStateService,
        private rest: DeskRESTService,
        private currentUser: CurrentUserService,
    ) {}

    public bootstrap(deskId: DeskId): Observable<any> {
        return Observable.create(observer => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            const payload: Payload = {
                bootstrap: {},
            };

            observableForkJoin([
                this._fetchDesk(deskId, payload),
                this._fetchColumns(deskId, payload),
                this._fetchProjects(deskId, payload),
                this._fetchCards(deskId, payload),
                this._fetchLabels(deskId, payload),
            ]).subscribe(
                () => {
                    this.state.dispatch({
                        type: EvodeskDeskAction.Bootstrap,
                        payload: {
                            bootstrap: payload.bootstrap,
                        },
                    });
                },
                (error) => {
                    observer.error(error);
                },
                () => {
                    observer.next(true);
                    observer.complete();
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        }).pipe(publishLast()).refCount();
    }

    private _fetchDesk(deskId: DeskId, payload: Payload): Observable<any> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create(o => {
            this.rest.desk(deskId).pipe(takeUntil(unsubscribe$)).subscribe(
                (next) => {
                    payload.bootstrap = {
                        ...payload.bootstrap,
                        desk: {
                            ...next,
                            members: next.members || [],
                            ownerId: next.ownerId || this.currentUser.impersonatedAs.id,
                        },
                    };

                    o.next(undefined);
                    o.complete();
                },
                (httpError) => {
                    o.error(httpError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    private _fetchColumns(deskId: DeskId, payload: Payload): Observable<any> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create(o => {
            this.rest.columns(deskId).pipe(takeUntil(unsubscribe$)).subscribe(
                (next) => {
                    payload.bootstrap = {
                        ...payload.bootstrap,
                        columns: next,
                    };

                    o.next(undefined);
                    o.complete();
                },
                (httpError) => {
                    o.error(httpError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    private _fetchProjects(deskId: DeskId, payload: Payload): Observable<any> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create(o => {
            this.rest.projects(deskId).pipe(takeUntil(unsubscribe$)).subscribe(
                (next) => {
                    if (next.length) {
                        payload.bootstrap = {
                            ...payload.bootstrap,
                            projects: next.length ? [...next] : [],
                        };
                    }

                    o.next(undefined);
                    o.complete();
                },
                (httpError) => {
                    o.error(httpError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    private _fetchCards(deskId: DeskId, payload: Payload): Observable<any> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create(o => {
            this.rest.cards(deskId).pipe(takeUntil(unsubscribe$)).subscribe(
                (next) => {
                    payload.bootstrap = {
                        ...payload.bootstrap,
                        sourceCards: next,
                        filteredCards: [...next],
                    };

                    o.next(undefined);
                    o.complete();
                },
                (httpError) => {
                    o.error(httpError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }

    private _fetchLabels(deskId: DeskId, payload: Payload): Observable<any> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        return Observable.create(o => {
            this.rest.labels(deskId).pipe(takeUntil(unsubscribe$)).subscribe(
                (next) => {
                    payload.bootstrap = {
                        ...payload.bootstrap,
                        labels: next,
                    };

                    o.next(undefined);
                    o.complete();
                },
                (httpError) => {
                    o.error(httpError);
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
