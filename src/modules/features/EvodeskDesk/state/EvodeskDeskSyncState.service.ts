import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';

import {EvodeskAppMessageBusService} from '../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';

import {AppEvent} from '../../../app/EvodeskAppMessageBus/models/app-events.model';

export enum EvodeskDeskSyncErrorReason {
    CommonHttpError,
}

export type EvodeskDeskSyncState =
    { reason: EvodeskDeskSyncErrorReason.CommonHttpError, payload: { httpResponse: HttpErrorResponse } }
;

@Injectable()
export class EvodeskDeskSyncStateService
{
    constructor(
        private appMessageBusService: EvodeskAppMessageBusService,
    ) {}

    dispatchCommonHttpError(httpError: HttpErrorResponse): void {
        this.dispatchSyncError({
            reason: EvodeskDeskSyncErrorReason.CommonHttpError,
            payload: {
                httpResponse: httpError,
            },
        });
    }

    dispatchSyncError(reason: EvodeskDeskSyncState): void {
        this.appMessageBusService.dispatch({ type: AppEvent.DeskForceReload, payload: { reason: reason } });
    }
}
