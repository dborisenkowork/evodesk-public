import {Injectable, ViewContainerRef} from '@angular/core';

import {BehaviorSubject, Observable, Subject} from 'rxjs';

import * as moment from 'moment';
import {QueryStringFilterOptions} from '../filters/EvodeskDeskQueryString.filter';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../types/Backend.types';
import {ViewMode} from '../../../../types/ViewMode.types';

import {filterDeskCardTitle} from '../functions/filterDeskCardTitle.function';

import {DeskColumnDefinitionModel, DeskColumnId} from '../../../app/EvodeskRESTApi/models/DeskColumnDefinition.model';
import {DeskModel} from '../../../app/EvodeskRESTApi/models/Desk.model';
import {DeskCardId, DeskCardModel, DeskCardProjectEntity} from '../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskCardChecklistItemModel, DeskCardChecklistModel} from '../../../app/EvodeskRESTApi/models/DeskCardChecklist.model';
import {DeskCardAttachmentModel} from '../../../app/EvodeskRESTApi/models/DeskCardAttachment.model';
import {DeskCardCommentModel} from '../../../app/EvodeskRESTApi/models/DeskCardComment.model';
import {DeskLabelModel} from '../../../app/EvodeskRESTApi/models/DeskLabel.model';
import {DeskCardBorderModel} from '../../../app/EvodeskRESTApi/models/DeskCardBorderColor.model';
import {DeskParticipantId, DeskParticipantModel} from '../../../app/EvodeskRESTApi/models/DeskParticipant.model';
import {DeskProjectId, DeskProjectModel} from '../../../app/EvodeskRESTApi/models/DeskProject.model';
import {DeskFilterDeadlineVariant, DeskFilterType, IDeskFilters} from '../models/DeskFilters.model';

import {AddCardSubmitData} from '../services/EvodeskDeskAddCard.service';
import {CurrentUserService} from '../../EvodeskAuth/services/CurrentUser.service';
import {DeskAddProjectResponse200, DeskUpdateCardChangedField} from '../../../app/EvodeskRESTApi/services/DeskREST.service';
import {AddProjectSubmitData} from '../services/EvodeskDeskProjects.service';

export type PrevEvodeskDeskState = EvodeskDeskState | undefined;
export type NextEvodeskDeskState = EvodeskDeskState;
export type EvodeskDeskStates = { current: NextEvodeskDeskState, prev?: PrevEvodeskDeskState };

export enum EvodeskDeskAction
{
    Reset = 'Reset',
    Bootstrap = 'Bootstrap',
    SetViewContainerRef = 'SetViewContainerRef',
    AddCard = 'AddCard',
    AddCardConfirmed = 'AddCardConfirmed',
    UpdateCards = 'UpdateCards',
    UpdateCardsConfirmed = 'UpdateCardsConfirmed',
    DeleteCards = 'DeleteCards',
    ArchiveCards = 'ArchiveCards',
    PushCard = 'PushCard',
    SetTitle = 'SetTitle',
    AddLabel = 'AddLabel',
    UpdateLabel = 'UpdateLabel',
    DeleteLabel = 'DeleteLabel',
    AddComment = 'AddComment',
    UpdateComment = 'UpdateComment',
    DeleteComment = 'DeleteComment',
    AddChecklist = 'AddChecklist',
    UpdateChecklist = 'UpdateChecklist',
    DeleteChecklist = 'DeleteChecklist',
    AddChecklistItem = 'AddChecklistItem',
    UpdateChecklistItem = 'UpdateChecklistItem',
    DeleteChecklistItem = 'DeleteChecklistItem',
    AttachFile = 'AttachFile',
    DeleteAttachment = 'DeleteAttachment',
    EditCard = 'EditCard',
    StopEditCard = 'StopEditCard',
    OpenCard = 'OpenCard',
    CloseCard = 'CloseCard',
    OpenAddCardForm = 'OpenAddCardForm',
    CloseAddCardForm = 'CloseAddCardForm',
    DragCardStart = 'DragCardStart',
    DragCardEnd = 'DragCardEnd',
    DropCard = 'DropCard',
    DragProjectStart = 'DragProjectStart',
    DragProjectEnd = 'DragProjectEnd',
    DropProject = 'DropProject',
    OpenDeskColumnContextMenu = 'OpenDeskColumnContextMenu',
    CloseDeskColumnContextMenu = 'CloseDeskColumnContextMenu',
    FiltersApply = 'FiltersApply',
    FiltersReset = 'FiltersReset',
    FiltersEnableLabelFilter = 'FiltersEnableLabelFilter',
    FiltersDisableLabelFilter = 'FiltersDisableLabelFilter',
    FiltersClearLabelFilter = 'FiltersClearLabelFilter',
    FiltersEnableQueryStringFilter = 'FiltersEnableQueryStringFilter',
    FiltersDisableQueryStringFilter = 'FiltersDisableQueryStringFilter',
    FiltersSetProjectFilter = 'FiltersSetProjectFilter',
    FiltersClearProjectFilter = 'FiltersClearProjectFilter',
    FiltersEnableDeadlineFilter = 'FiltersEnableDeadlineFilter',
    FiltersDisableDeadlineFilter = 'FiltersDisableDeadlineFilter',
    FiltersEnableParticipantFilter = 'FiltersEnableParticipantFilter',
    FiltersDisableParticipantFilter = 'FiltersDisableParticipantFilter',
    SetLayoutVariant = 'SetLayoutVariant',
    AddDeskParticipantAsResponsibleOfCard = 'AddDeskParticipantAsResponsibleOfCard',
    RemoveDeskParticipantAsResponsibleOfCard = 'RemoveDeskParticipantAsResponsibleOfCard',
    AddDeskParticipant = 'AddDeskParticipant',
    RemoveDeskParticipant = 'RemoveDeskParticipant',
    SetProjects = 'SetProjects',
    AddProject = 'AddProject',
    AddProjectConfirmed = 'AddProjectConfirmed',
    UpdateProjects = 'UpdateProjects',
    UpdateProjectsConfirmed = 'UpdateProjectsConfirmed',
    SetCurrentProject = 'SetCurrentProject',
    DeleteProject = 'DeleteProject',
    IncludeCardToProject = 'IncludeCardToProject',
    ExcludeCardFromProject = 'ExcludeCardFromProject',
}

type Actions =
      { type: EvodeskDeskAction.Reset }
    | { type: EvodeskDeskAction.Bootstrap, payload: { bootstrap: EvodeskDeskStateBootstrap } }
    | { type: EvodeskDeskAction.SetViewContainerRef, payload: ViewContainerRef }
    | { type: EvodeskDeskAction.AddCard, payload: { formData: AddCardSubmitData; fakeId: number; } }
    | { type: EvodeskDeskAction.AddCardConfirmed, payload: { formData: AddCardSubmitData; fakeId: number; response: DeskCardModel; } }
    | { type: EvodeskDeskAction.UpdateCards, payload: { cards: Array<{ card: DeskCardModel, changedField: DeskUpdateCardChangedField; }>; } }
    | { type: EvodeskDeskAction.UpdateCardsConfirmed, payload: { cards: Array<{ card: DeskCardModel, changedField: DeskUpdateCardChangedField; }>; } }
    | { type: EvodeskDeskAction.DeleteCards, payload: { cardIds: Array<DeskCardId>; } }
    | { type: EvodeskDeskAction.ArchiveCards, payload: { cardIds: Array<DeskCardId>; } }
    | { type: EvodeskDeskAction.PushCard, payload: { card: DeskCardModel; } }
    | { type: EvodeskDeskAction.SetTitle, payload: { cardId: DeskCardId; newTitle: string } }
    | { type: EvodeskDeskAction.AddLabel, payload: { label: DeskLabelModel; } }
    | { type: EvodeskDeskAction.UpdateLabel, payload: { origLabel: DeskLabelModel; shouldBe: DeskLabelModel; } }
    | { type: EvodeskDeskAction.DeleteLabel, payload: { labelId: number; } }
    | { type: EvodeskDeskAction.AddComment, payload: DeskCardCommentModel }
    | { type: EvodeskDeskAction.UpdateComment, payload: DeskCardCommentModel }
    | { type: EvodeskDeskAction.DeleteComment, payload: { cardId: number; commentId: number } }
    | { type: EvodeskDeskAction.AddChecklist, payload: { cardId: number; checklist: DeskCardChecklistModel; } }
    | { type: EvodeskDeskAction.UpdateChecklist, payload: { cardId: number; checklist: DeskCardChecklistModel; } }
    | { type: EvodeskDeskAction.DeleteChecklist, payload: { cardId: number; checklist: DeskCardChecklistModel; } }
    | { type: EvodeskDeskAction.AddChecklistItem, payload: { cardId: number; checklistId: number; checklistItem: DeskCardChecklistItemModel; } }
    | { type: EvodeskDeskAction.UpdateChecklistItem, payload: { cardId: number; checklistId: number; checklistItem: DeskCardChecklistItemModel; } }
    | { type: EvodeskDeskAction.DeleteChecklistItem, payload: { cardId: number; checklistId: number; checklistItem: DeskCardChecklistItemModel; } }
    | { type: EvodeskDeskAction.AttachFile, payload: { cardId: number; deskId: number; attachment: DeskCardAttachmentModel; } }
    | { type: EvodeskDeskAction.DeleteAttachment, payload: { cardId: number; deskId: number; attachmentId: number; } }
    | { type: EvodeskDeskAction.EditCard, payload: { cardId: DeskCardId; element: HTMLElement; } }
    | { type: EvodeskDeskAction.StopEditCard }
    | { type: EvodeskDeskAction.OpenCard, payload: { cardId: DeskCardId } }
    | { type: EvodeskDeskAction.CloseCard, payload: { cardId: DeskCardId } }
    | { type: EvodeskDeskAction.OpenDeskColumnContextMenu, payload: { columnId: DeskCardId } }
    | { type: EvodeskDeskAction.CloseDeskColumnContextMenu }
    | { type: EvodeskDeskAction.FiltersApply, payload: { filtered: Array<DeskCardModel> } }
    | { type: EvodeskDeskAction.FiltersReset }
    | { type: EvodeskDeskAction.OpenAddCardForm, payload: { columnId: DeskColumnId } }
    | { type: EvodeskDeskAction.CloseAddCardForm }
    | { type: EvodeskDeskAction.DragCardStart }
    | { type: EvodeskDeskAction.DragCardEnd, payload: { card: DeskCardModel, origColumnId: string } }
    | { type: EvodeskDeskAction.DropCard, payload: { cards: Array<DeskCardModel> } }
    | { type: EvodeskDeskAction.DragProjectStart }
    | { type: EvodeskDeskAction.DragProjectEnd, payload: { project: DeskProjectModel } }
    | { type: EvodeskDeskAction.DropProject, payload: { projects: Array<DeskProjectModel> } }
    | { type: EvodeskDeskAction.FiltersEnableLabelFilter, payload: { labelIds: Array<number> } }
    | { type: EvodeskDeskAction.FiltersDisableLabelFilter, payload: { labelIds: Array<number> } }
    | { type: EvodeskDeskAction.FiltersClearLabelFilter }
    | { type: EvodeskDeskAction.FiltersEnableQueryStringFilter, payload: { withOptions: QueryStringFilterOptions } }
    | { type: EvodeskDeskAction.FiltersDisableQueryStringFilter }
    | { type: EvodeskDeskAction.FiltersSetProjectFilter, payload: { projectId: DeskProjectId } }
    | { type: EvodeskDeskAction.FiltersClearProjectFilter }
    | { type: EvodeskDeskAction.FiltersEnableDeadlineFilter, payload: { variant: DeskFilterDeadlineVariant } }
    | { type: EvodeskDeskAction.FiltersDisableDeadlineFilter, payload: { variant: DeskFilterDeadlineVariant } }
    | { type: EvodeskDeskAction.FiltersEnableParticipantFilter, payload: { participantId: DeskParticipantId } }
    | { type: EvodeskDeskAction.FiltersDisableParticipantFilter, payload: { participantId: DeskParticipantId } }
    | { type: EvodeskDeskAction.SetLayoutVariant, payload: { layout: ViewMode } }
    | { type: EvodeskDeskAction.AddDeskParticipantAsResponsibleOfCard, payload: { cardId: DeskCardId; participant: DeskParticipantModel } }
    | { type: EvodeskDeskAction.RemoveDeskParticipantAsResponsibleOfCard, payload: { cardId: DeskCardId; participant: DeskParticipantModel } }
    | { type: EvodeskDeskAction.AddDeskParticipant, payload: { participant: DeskParticipantModel } }
    | { type: EvodeskDeskAction.RemoveDeskParticipant, payload: { participant: DeskParticipantModel } }
    | { type: EvodeskDeskAction.SetProjects, payload: { projects: Array<DeskProjectModel>; } }
    | { type: EvodeskDeskAction.AddProject, payload: { formData: AddProjectSubmitData; fakeId: number; } }
    | { type: EvodeskDeskAction.AddProjectConfirmed, payload: { formData: AddProjectSubmitData; fakeId: number; response: DeskAddProjectResponse200; } }
    | { type: EvodeskDeskAction.UpdateProjects, payload: { projects: Array<{ project: DeskProjectModel }>; } }
    | { type: EvodeskDeskAction.UpdateProjectsConfirmed, payload: { projects: Array<{ project: DeskProjectModel }>; } }
    | { type: EvodeskDeskAction.SetCurrentProject, payload: { projectId: DeskProjectId | undefined; } }
    | { type: EvodeskDeskAction.DeleteProject, payload: { projectId: DeskProjectId; } }
    | { type: EvodeskDeskAction.IncludeCardToProject, payload: { cardId: DeskCardId; projectId: DeskProjectId; } }
    | { type: EvodeskDeskAction.ExcludeCardFromProject, payload: { cardId: DeskCardId; projectId: DeskProjectId; } }
;

export interface EvodeskDeskState
{
    desk: DeskModel | undefined;
    columns: Array<DeskColumnDefinitionModel>;
    sourceCards: Array<DeskCardModel>;
    filteredCards: Array<DeskCardModel>;
    labels: Array<DeskLabelModel>;
    projects: Array<DeskProjectModel>;
    filters: Array<IDeskFilters<any>>;
    addCardFormOpenedForColumn: DeskCardId;
    draggingCards: boolean;
    openedCard: DeskCardId | undefined;
    editingCard: DeskCardId | undefined;
    contextMenuOpenedForColumn: DeskCardId | undefined;
    viewMode: ViewMode;
    viewContainerRef?: ViewContainerRef;
    currentProject?: DeskProjectId | undefined;
}

export interface EvodeskDeskStateBootstrap {
    desk?: DeskModel | undefined;
    columns?: Array<DeskColumnDefinitionModel>;
    sourceCards?: Array<DeskCardModel>;
    filteredCards?: Array<DeskCardModel>;
    labels?: Array<DeskLabelModel>;
    projects?: Array<DeskProjectModel>;
}

function initialState(): EvodeskDeskState {
    return {
        desk: undefined,
        columns: [],
        sourceCards: [],
        filteredCards: [],
        projects: [],
        labels: [],
        filters: [],
        addCardFormOpenedForColumn: undefined,
        draggingCards: false,
        openedCard: undefined,
        editingCard: undefined,
        contextMenuOpenedForColumn: undefined,
        viewMode: ViewMode.Desktop,
    };
}

@Injectable()
export class EvodeskDeskStateService
{
    private _current$: BehaviorSubject<EvodeskDeskStates> = new BehaviorSubject<EvodeskDeskStates>({ current: initialState() });
    private _side$: Subject<Actions> = new Subject<Actions>();

    constructor(
        private currentUser: CurrentUserService,
    ) {}

    get current$(): Observable<EvodeskDeskStates> {
        return this._current$.asObservable();
    }

    get snapshot(): EvodeskDeskState {
        return this._current$.getValue().current;
    }

    get side$(): Observable<Actions> {
        return this._side$.asObservable();
    }

    reset(): void {
        this.dispatch({
            type: EvodeskDeskAction.Reset,
        });
    }

    dispatch(action: Actions): void {
        this._side$.next(action);

        switch (action.type) {
            case EvodeskDeskAction.Reset:
                this.setState(() => {
                    return initialState();
                });

                break;

            case EvodeskDeskAction.Bootstrap:
                this.setState((orig) => {
                    return {
                        ...orig,
                        ...action.payload.bootstrap,
                    };
                });

                break;

            case EvodeskDeskAction.SetViewContainerRef:
                this.setState((orig) => {
                    return {
                        ...orig,
                        viewContainerRef: action.payload,
                    };
                });

                break;

            case EvodeskDeskAction.AddCard:
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: [ ...orig.sourceCards, {
                            id: action.payload.fakeId,
                            deskId: orig.desk.id,
                            deskName: orig.desk.name,
                            column: action.payload.formData.column,
                            title: action.payload.formData.title,
                            description: '',
                            video: null,
                            deadline: null,
                            daily: false,
                            private: 0,
                            system: false,
                            deletedAt: null,
                            order: action.payload.formData.order,
                            labels: [],
                            attachments: [],
                            checklists: [],
                            actions: [],
                            comments: [],
                            responsible: [],
                            projects: [],
                            members: [],
                            color: DeskCardBorderModel.None,
                        }],
                    };
                });

                break;

            case EvodeskDeskAction.AddCardConfirmed: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sC => {
                            if (sC.id === action.payload.fakeId) {
                                return {
                                    ...action.payload.response,
                                    actions: action.payload.response.actions.length ? action.payload.response.actions : [
                                        {
                                            action: 'create',
                                            changedField: 'title',
                                            creation_at: moment(new Date()).format(BACKEND_DATE_FORMAT_AS_MOMENT),
                                            entity: 'card',
                                            entity_id: action.payload.response.id.toString(),
                                            id: action.payload.fakeId,
                                            user: this.currentUser.impersonatedAs,
                                            value: action.payload.response.title,
                                        },
                                    ],
                                };
                            } else {
                                return sC;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.UpdateCards: {
                const updatedCardsMap: { [id: number]: DeskCardModel } = {};

                action.payload.cards.forEach(c => {
                    updatedCardsMap[c.card.id] = c.card;
                });

                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(c => {
                            if (updatedCardsMap[c.id]) {
                                return updatedCardsMap[c.id];
                            } else {
                                return c;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.UpdateCardsConfirmed: {
                const updatedCardsMap: { [id: number]: DeskCardModel } = {};

                action.payload.cards.forEach(c => {
                    updatedCardsMap[c.card.id] = c.card;
                });

                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(c => {
                            if (updatedCardsMap[c.id]) {
                                return updatedCardsMap[c.id];
                            } else {
                                return c;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.DeleteCards: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.filter(c => !~action.payload.cardIds.indexOf(c.id)),
                    };
                });

                break;
            }

            case EvodeskDeskAction.ArchiveCards: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.filter(c => !~action.payload.cardIds.indexOf(c.id)),
                    };
                });

                break;
            }

            case EvodeskDeskAction.PushCard: {
                this.setState((orig) => {
                    const cards: Array<DeskCardModel> = orig.sourceCards.filter(sc => sc.id !== action.payload.card.id);

                    cards.push(action.payload.card);

                    return {
                        ...orig,
                        sourceCards: cards,
                    };
                });

                break;
            }

            case EvodeskDeskAction.SetTitle: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.cardId) {
                                return {
                                    ...sc,
                                    title: filterDeskCardTitle(action.payload.newTitle),
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.AddLabel: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        labels: (orig.labels.length ? [...orig.labels, action.payload.label] : [action.payload.label]).filter((v, i, a) => a.indexOf(v) === i),
                    };
                });

                break;
            }

            case EvodeskDeskAction.UpdateLabel: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        labels: orig.labels.map(l => {
                            if (l.id === action.payload.origLabel.id) {
                                return action.payload.shouldBe;
                            } else {
                                return l;
                            }
                        }),
                        sourceCards: orig.sourceCards.map(card => {
                            return {
                                ...card,
                                labels: card.labels.map(oldLabel => {
                                    if (oldLabel.id === action.payload.origLabel.id) {
                                        return action.payload.shouldBe;
                                    }
                                    return oldLabel;
                                }),
                            };
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.DeleteLabel: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        labels: orig.labels.filter(l => l.id !== action.payload.labelId),
                        sourceCards: orig.sourceCards.map(sc => {
                            return {
                                ...sc,
                                labels: sc.labels.filter(l => l.id !== action.payload.labelId),
                            };
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.AddComment: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.card) {
                                return {
                                    ...sc,
                                    comments: sc.comments.length ? [...sc.comments, action.payload] : [action.payload],
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.UpdateComment: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.card) {
                                return {
                                    ...sc,
                                    comments: sc.comments.map(comment => {
                                        if (comment.id === action.payload.id) {
                                            return action.payload;
                                        } else {
                                            return comment;
                                        }
                                    }),
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.DeleteComment: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.cardId) {
                                return {
                                    ...sc,
                                    comments: sc.comments.filter(c => c.id !== action.payload.cardId),
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.AddChecklist: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.cardId) {
                                return {
                                    ...sc,
                                    checklists: [
                                        ...sc.checklists,
                                        action.payload.checklist,
                                    ],
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.UpdateChecklist: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.cardId) {
                                return {
                                    ...sc,
                                    checklists: sc.checklists.map(cl => {
                                        if (cl.id === action.payload.checklist.id) {
                                            return action.payload.checklist;
                                        } else {
                                            return cl;
                                        }
                                    }),
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.DeleteChecklist: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.cardId) {
                                return {
                                    ...sc,
                                    checklists: sc.checklists.filter(cl => cl.id !== action.payload.checklist.id),
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.AddChecklistItem: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.cardId) {
                                return {
                                    ...sc,
                                    checklists: sc.checklists.map(cl => {
                                        if (cl.id === action.payload.checklistId) {
                                            return {
                                                ...cl,
                                                items: cl.items.length ? [...cl.items, action.payload.checklistItem] : [action.payload.checklistItem],
                                            };
                                        } else {
                                            return cl;
                                        }
                                    }),
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.UpdateChecklistItem: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.cardId) {
                                return {
                                    ...sc,
                                    checklists: sc.checklists.map(cl => {
                                        if (cl.id === action.payload.checklistId) {
                                            return {
                                                ...cl,
                                                items: cl.items.map(ci => {
                                                    if (ci.id === action.payload.checklistItem.id) {
                                                        return action.payload.checklistItem;
                                                    } else {
                                                        return ci;
                                                    }
                                                }),
                                            };
                                        } else {
                                            return cl;
                                        }
                                    }),
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.DeleteChecklistItem: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.cardId) {
                                return {
                                    ...sc,
                                    checklists: sc.checklists.map(cl => {
                                        if (cl.id === action.payload.checklistId) {
                                            return {
                                                ...cl,
                                                items: cl.items.filter(ci => ci.id !== action.payload.checklistItem.id),
                                            };
                                        } else {
                                            return cl;
                                        }
                                    }),
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.AttachFile: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.cardId) {
                                return {
                                    ...sc,
                                    attachments: sc.attachments.length ? [...sc.attachments, action.payload.attachment] : [action.payload.attachment],
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.DeleteAttachment: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.cardId) {
                                return {
                                    ...sc,
                                    attachments: sc.attachments.filter(a => a.id !== action.payload.attachmentId),
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.OpenAddCardForm: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        addCardFormOpenedForColumn: action.payload.columnId,
                    };
                });

                break;
            }

            case EvodeskDeskAction.CloseAddCardForm: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        addCardFormOpenedForColumn: undefined,
                    };
                });

                break;
            }

            case EvodeskDeskAction.DragCardStart: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        draggingCards: true,
                    };
                });

                break;
            }

            case EvodeskDeskAction.DragCardEnd: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        draggingCards: false,
                    };
                });

                break;
            }

            case EvodeskDeskAction.DragProjectStart: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        draggingCards: true,
                    };
                });

                break;
            }


            case EvodeskDeskAction.DragProjectEnd: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        draggingCards: false,
                    };
                });

                break;
            }

            case EvodeskDeskAction.EditCard: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        editingCard: action.payload.cardId,
                        openedCard: undefined,
                    };
                });

                break;
            }

            case EvodeskDeskAction.StopEditCard: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        editingCard: undefined,
                        openedCard: undefined,
                    };
                });

                break;
            }

            case EvodeskDeskAction.OpenCard: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        openedCard: action.payload.cardId,
                        editingCard: undefined,
                    };
                });

                break;
            }

            case EvodeskDeskAction.CloseCard: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        editingCard: undefined,
                        openedCard: undefined,
                    };
                });

                break;
            }

            case EvodeskDeskAction.OpenDeskColumnContextMenu: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        contextMenuOpenedForColumn: action.payload.columnId,
                    };
                });

                break;
            }

            case EvodeskDeskAction.CloseDeskColumnContextMenu: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        contextMenuOpenedForColumn: undefined,
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersApply: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filteredCards: action.payload.filtered,
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersReset: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filters: [],
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersEnableLabelFilter: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filters: [
                            ...orig.filters,
                            ...action.payload.labelIds.map(lId => {
                                return                             {
                                    type: DeskFilterType.Label,
                                    createdAt: new Date(),
                                    options: {
                                        labelId: lId,
                                    },
                                };
                            }),
                        ],
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersClearLabelFilter: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filters: orig.filters.filter(f => f.type !== DeskFilterType.Label),
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersDisableLabelFilter: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filters: orig.filters.filter(f => ! (f.type === DeskFilterType.Label && !!~action.payload.labelIds.indexOf(f.options.labelId))),
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersEnableQueryStringFilter: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filters: [
                            ...orig.filters.filter(f => f.type !== DeskFilterType.QueryString),
                            {
                                type: DeskFilterType.QueryString,
                                createdAt: new Date(),
                                options: {
                                    search: action.payload.withOptions.queryString,
                                },
                            },
                        ],
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersDisableQueryStringFilter: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filters: orig.filters.filter(f => f.type !== DeskFilterType.QueryString),
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersSetProjectFilter: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filters: [
                            ...orig.filters.filter(f => f.type !== DeskFilterType.Project),
                            {
                                type: DeskFilterType.Project,
                                createdAt: new Date(),
                                options: {
                                    projectId: action.payload.projectId,
                                },
                            },
                        ],
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersClearProjectFilter: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filters: orig.filters.filter(f => f.type !== DeskFilterType.Project),
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersEnableDeadlineFilter: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filters: [
                            ...orig.filters,
                            {
                                type: DeskFilterType.Deadline,
                                createdAt: new Date(),
                                options: {
                                    variant: action.payload.variant,
                                },
                            },
                        ],
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersDisableDeadlineFilter: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filters: orig.filters.filter(f => ! (f.type === DeskFilterType.Deadline && f.options.variant === action.payload.variant)),
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersEnableParticipantFilter: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filters: [
                            ...orig.filters,
                            {
                                type: DeskFilterType.Participant,
                                createdAt: new Date(),
                                options: {
                                    participantId: action.payload.participantId,
                                },
                            },
                        ],
                    };
                });

                break;
            }

            case EvodeskDeskAction.FiltersDisableParticipantFilter: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        filters: orig.filters.filter(f => ! (f.type === DeskFilterType.Participant && f.options.participantId === action.payload.participantId)),
                    };
                });

                break;
            }

            case EvodeskDeskAction.SetLayoutVariant: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        viewMode: action.payload.layout,
                    };
                });

                break;
            }

            case EvodeskDeskAction.AddDeskParticipantAsResponsibleOfCard: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.cardId) {
                                return {
                                    ...sc,
                                    responsible: [...sc.responsible, action.payload.participant],
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.RemoveDeskParticipantAsResponsibleOfCard: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        sourceCards: orig.sourceCards.map(sc => {
                            if (sc.id === action.payload.cardId) {
                                return {
                                    ...sc,
                                    responsible: sc.responsible.filter(p => p.id !== action.payload.participant.id),
                                };
                            } else {
                                return sc;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.AddDeskParticipant: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        desk: {
                            ...orig.desk,
                            members: [...orig.desk.members, action.payload.participant],
                        },
                    };
                });

                break;
            }

            case EvodeskDeskAction.RemoveDeskParticipant: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        desk: {
                            ...orig.desk,
                            members: orig.desk.members.filter(m => m.id !== action.payload.participant.id),
                        },
                    };
                });

                break;
            }

            case EvodeskDeskAction.SetProjects: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        projects: action.payload.projects,
                    };
                });

                break;
            }

            case EvodeskDeskAction.AddProject: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        projects: [ ...orig.projects, {
                            cards: [],
                            desk: orig.desk,
                            order: action.payload.formData.order,
                            id: action.payload.fakeId,
                            title: action.payload.formData.title,
                            users: [],
                        }],
                    };
                });

                break;
            }

            case EvodeskDeskAction.AddProjectConfirmed: {
                this.setState((orig) => {
                    const fakeId: number = action.payload.fakeId;

                    return {
                        ...orig,
                        projects: orig.projects.map(sC => {
                            if (sC.id === fakeId) {
                                return {
                                    ...sC,
                                    id: action.payload.response.id,
                                    order: action.payload.response.order,
                                    title: action.payload.response.title,
                                    users: [],
                                };
                            } else {
                                return sC;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.UpdateProjects: {
                this.setState((orig) => {
                    const updatedProjectsMap: { [id: number]: DeskProjectModel } = {};

                    action.payload.projects.forEach(c => {
                        updatedProjectsMap[c.project.id] = c.project;
                    });

                    return {
                        ...orig,
                        projects: orig.projects.map(p => {
                            if (updatedProjectsMap[p.id]) {
                                return updatedProjectsMap[p.id];
                            } else {
                                return p;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.UpdateProjectsConfirmed: {
                this.setState((orig) => {
                    const updatedProjectsMap: { [id: number]: DeskProjectModel } = {};

                    action.payload.projects.forEach(c => {
                        updatedProjectsMap[c.project.id] = c.project;
                    });

                    return {
                        ...orig,
                        projects: orig.projects.map(p => {
                            if (updatedProjectsMap[p.id]) {
                                return updatedProjectsMap[p.id];
                            } else {
                                return p;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.SetCurrentProject: {
                this.setState((orig) => {
                    return {
                        ...orig,
                        currentProject: action.payload.projectId,
                    };
                });

                break;
            }

            case EvodeskDeskAction.DeleteProject: {
                const deletedProjectId: DeskProjectId = action.payload.projectId;

                this.setState((orig) => {
                    return {
                        ...orig,
                        currentProject: orig.currentProject === deletedProjectId ? undefined : orig.currentProject,
                        projects: orig.projects.filter(p => p.id !== deletedProjectId),
                        sourceCards: orig.sourceCards.map(sC => {
                            if (!!~sC.projects.map(p => p.id).indexOf(deletedProjectId)) {
                                return {
                                    ...sC,
                                    projects: sC.projects.filter(p => p.id !== deletedProjectId),
                                };
                            } else {
                                return sC;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.IncludeCardToProject: {
                const project: DeskProjectModel = this.snapshot.projects
                    .filter(p => p.id === action.payload.projectId)
                    .map(p => {
                        return {
                            ...p,
                            cards: p.cards.length ? [...p.cards, action.payload.cardId] : [action.payload.cardId],
                        };
                    })[0];

                const projectEntity: DeskCardProjectEntity = {
                    id: project.id,
                    deskId: project.desk.id,
                    order: project.order,
                    title: project.title,
                };

                this.setState((orig) => {
                    return {
                        ...orig,
                        projects: orig.projects.map(p => {
                            if (p.id === action.payload.projectId) {
                                return project;
                            } else {
                                return p;
                            }
                        }),
                        sourceCards: orig.sourceCards.map(sC => {
                            if (sC.id === action.payload.cardId) {
                                return {
                                    ...sC,
                                    projects: sC.projects.length ? [...sC.projects, projectEntity] : [projectEntity],
                                };
                            } else {
                                return sC;
                            }
                        }),
                    };
                });

                break;
            }

            case EvodeskDeskAction.ExcludeCardFromProject: {
                const project: DeskProjectModel = this.snapshot.projects
                    .filter(p => p.id === action.payload.projectId)
                    .map(p => {
                        return {
                            ...p,
                            cards: p.cards.filter(c => c !== action.payload.cardId),
                        };
                    })[0];

                this.setState((orig) => {
                    return {
                        ...orig,
                        projects: orig.projects.map(p => {
                            if (p.id === action.payload.projectId) {
                                return project;
                            } else {
                                return p;
                            }
                        }),
                        sourceCards: orig.sourceCards.map(sC => {
                            if (sC.id === action.payload.cardId) {
                                return {
                                    ...sC,
                                    projects: sC.projects.filter(p => p.id !== action.payload.projectId),
                                };
                            } else {
                                return sC;
                            }
                        }),
                    };
                });

                break;
            }
        }
    }

    private setState(query: (orig: PrevEvodeskDeskState) => NextEvodeskDeskState) {
        const prev: EvodeskDeskState = this._current$.getValue().current;

        this._current$.next({
            prev: prev,
            current: query(prev),
        });
    }
}
