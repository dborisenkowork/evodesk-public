import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/internal/operators';

import {EvodeskHeaderLayoutVariant} from '../../../EvodeskHeader/models/EvodeskHeader.models';
import {DeskCardId} from '../../../../app/EvodeskRESTApi/models/DeskCard.model';
import {DeskId} from '../../../../app/EvodeskRESTApi/models/Desk.model';

import {EvodeskHeaderConfigurationService} from '../../../EvodeskHeader/services/EvodeskHeaderConfiguration.service';
import {EvodeskDeskAction, EvodeskDeskStateService} from '../../state/EvodeskDeskState.service';
import {BootstrapEvodeskDeskScript} from '../../scripts/BootstrapEvodeskDesk.script';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';
import {EvodeskDeskSyncStateService} from '../../state/EvodeskDeskSyncState.service';
import {EvodeskDeskQueryStringFilter} from '../../filters/EvodeskDeskQueryString.filter';
import {EvodeskDeskCommentService} from '../../services/EvodeskDeskComment.service';
import {EvodeskDeskFetchCardService} from '../../services/EvodeskDeskFetchCard.service';
import {EvodeskDeskAttachmentService} from '../../services/EvodeskDeskAttachment.service';
import {EvodeskDeskUpdateCardService} from '../../services/EvodeskDeskUpdateCard.service';
import {EvodeskDeskAddCardService} from '../../services/EvodeskDeskAddCard.service';
import {EvodeskDeskDeleteCardsService} from '../../services/EvodeskDeskDeleteCards.service';
import {EvodeskDeskMoveCardsService} from '../../services/EvodeskDeskMoveCards.service';
import {EvodeskDeskCardLabelFilter} from '../../filters/EvodeskDeskCardLabel.filter';
import {EvodeskDeskFilterService} from '../../services/EvodeskDeskFilter.service';
import {EvodeskDeskLabelService} from '../../services/EvodeskDeskLabel.service';
import {EvodeskDeskCardModalService} from '../../services/EvodeskDeskCardModal.service';
import {EvodeskDeskBreakpointsService} from '../../services/EvodeskDeskBreakpoints.service';
import {EvodeskDeskTranslateLogRecordService} from '../../services/EvodeskDeskTranslateLogRecord.service';
import {EvodeskDeskCommentBodyParserService} from '../../services/EvodeskDeskCommentBodyParser.service';
import {EvodeskDeskCardChecklistService} from '../../services/EvodeskDeskCardChecklist.service';
import {EvodeskDeskCardResponsibleService} from '../../services/EvodeskDeskCardResponsible.service';
import {EvodeskDeskParticipantService} from '../../services/EvodeskDeskParticipant.service';
import {EvodeskDeskArchiveCardsService} from '../../services/EvodeskDeskArchiveCards.service';
import {EvodeskDeskProjectsService} from '../../services/EvodeskDeskProjects.service';
import {EvodeskDeskCardProjectsService} from '../../services/EvodeskDeskCardProjects.service';
import {EvodeskDeskFloatService} from '../../services/EvodeskDeskFloat.service';

interface State {
    ready: boolean;
    deskId: DeskId | undefined;
    cardId: DeskCardId | undefined;
}

@Component({
    templateUrl: './IndexRoute.component.pug',
    styleUrls: [
        './IndexRoute.component.scss',
    ],
    providers: [
        BootstrapEvodeskDeskScript,
        EvodeskDeskSyncStateService,
        EvodeskDeskStateService,

        EvodeskDeskCardLabelFilter,
        EvodeskDeskQueryStringFilter,
        EvodeskDeskFilterService,
        EvodeskDeskAddCardService,
        EvodeskDeskDeleteCardsService,
        EvodeskDeskUpdateCardService,
        EvodeskDeskMoveCardsService,
        EvodeskDeskLabelService,
        EvodeskDeskCardChecklistService,
        EvodeskDeskAttachmentService,
        EvodeskDeskCommentService,
        EvodeskDeskFetchCardService,
        EvodeskDeskCardModalService,
        EvodeskDeskBreakpointsService,
        EvodeskDeskTranslateLogRecordService,
        EvodeskDeskCommentBodyParserService,
        EvodeskDeskCardResponsibleService,
        EvodeskDeskParticipantService,
        EvodeskDeskArchiveCardsService,
        EvodeskDeskProjectsService,
        EvodeskDeskCardProjectsService,
        EvodeskDeskFloatService,
    ],
})
export class IndexRouteComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        deskId: undefined,
        cardId: undefined,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private viewContainerRef: ViewContainerRef,
        private deskState: EvodeskDeskStateService,
        private headerHeaderConfiguration: EvodeskHeaderConfigurationService,
        private currentUserService: CurrentUserService,
    ) {}

    ngOnInit(): void {
        this.headerHeaderConfiguration.setVariant(EvodeskHeaderLayoutVariant.Common);

        this.deskState.dispatch({
            type: EvodeskDeskAction.SetViewContainerRef,
            payload: this.viewContainerRef,
        });

        this.activatedRoute.queryParams.pipe(takeUntil(this.ngOnDestroy$)).subscribe((qp) => {
            if (qp['card']) {
                this.state = {
                    ...this.state,
                    cardId: parseInt(qp['card'], 10),
                };

                this.cdr.detectChanges();
            } else {
                this.state = {
                    ...this.state,
                    cardId: undefined,
                };

                this.cdr.detectChanges();
            }
        });

        this.activatedRoute.params.pipe(takeUntil(this.ngOnDestroy$)).subscribe((params) => {
            if (params['deskId']) {
                const deskId: DeskId = parseInt(params['deskId'], 10);

                this.state = {
                    ...this.state,
                    ready: true,
                    deskId: deskId,
                };

                this.cdr.detectChanges();
            } else {
                const personalDeskId: DeskId = this.currentUserService.impersonatedAs.desk;

                this.router.navigate(['/desk', personalDeskId]);

                this.state = {
                    ...this.state,
                    ready: false,
                };

                this.cdr.detectChanges();
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
