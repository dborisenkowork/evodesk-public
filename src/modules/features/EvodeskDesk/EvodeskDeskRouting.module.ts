import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskDeskRouting} from './configs/EvodeskDeskRouting.config';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskDeskRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskDesksRoutingModule
{}
