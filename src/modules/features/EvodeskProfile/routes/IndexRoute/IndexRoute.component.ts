import {ChangeDetectorRef, Component, OnDestroy, OnInit, SimpleChange, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Observable, Subject} from 'rxjs';
import {distinctUntilChanged, take, takeUntil} from 'rxjs/operators';

import {EVODESK_FEED_INJECT_PROVIDERS} from '../../../EvodeskFeed/EvodeskFeed.module';

import {defaultBottomMatDialog600pxConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {ProfileModel, validateUrlAlias} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {EvodeskHeaderLayoutVariant} from '../../../EvodeskHeader/models/EvodeskHeader.models';

import {ProfileFeedSourceAdapter} from '../../../EvodeskFeed/source-adapters/ProfileFeed.source-adapter';

import {EvodeskProfileWorkExperienceDecideContainerComponent as WEDecisionModal} from '../../components/EvodeskProfileWorkExperienceDecide/EvodeskProfileWorkExperienceDecideContainer.component';

import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {EvodeskProfileBootstrapService} from '../../services/EvodeskProfileBootstrap.service';
import {EvodeskProfileStateAction, EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';
import {EvodeskAlbumsStateService} from '../../../EvodeskAlbums/services/EvodeskAlbumsState.service';
import {EvodeskBootstrapAlbumsService} from '../../../EvodeskAlbums/services/EvodeskBootstrapAlbums.service';
import {EvodeskFeedStateService} from '../../../EvodeskFeed/state/EvodeskFeedState.service';
import {EvodeskFeedService} from '../../../EvodeskFeed/services/EvodeskFeed.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';
import {ProfileRESTService} from '../../../../app/EvodeskRESTApi/services/ProfileREST.service';
import {EvodeskHeaderConfigurationService} from '../../../EvodeskHeader/services/EvodeskHeaderConfiguration.service';
import {EvodeskProfileBreakpointsService} from '../../services/EvodeskProfileBreakpoints.service';
import {EvodeskProfileWorkExperienceBodyParserService} from '../../services/EvodeskProfileWorkExperienceBodyParser.service';
import {EvodeskProfileUrlService} from '../../shared/services/EvodeskProfileUrl.service';
import {EvodeskProfileCompetenceModalsService} from '../../services/EvodeskProfileCompetenceModals.service';
import {EvodeskProfileCompetenceMarkBodyParserService} from '../../services/EvodeskProfileCompetenceMarkBodyParser.service';
import {EvodeskAppLoading, EvodeskAppLoadingStatusService} from '../../../../app/EvodeskApp/services/EvodeskAppLoadingStatus.service';

const defaultFeedPostsPerPage: number = 15;

interface State {
    ready: boolean;
    loading: boolean;
}

@Component({
    templateUrl: './IndexRoute.component.pug',
    styleUrls: [
        './IndexRoute.component.scss',
    ],
    providers: [
        ProfileFeedSourceAdapter,
        EvodeskAlbumsStateService,
        EvodeskBootstrapAlbumsService,
        ...EVODESK_FEED_INJECT_PROVIDERS,
        EvodeskProfileBootstrapService,
        EvodeskProfileStateService,
        EvodeskProfileBreakpointsService,
        EvodeskProfileWorkExperienceBodyParserService,
        EvodeskProfileCompetenceModalsService,
        EvodeskProfileCompetenceMarkBodyParserService,
    ],
})
export class IndexRouteComponent implements OnInit, OnDestroy
{
    private nextParams$: Subject<void> = new Subject<void>();
    private nextProfile$: Subject<void> = new Subject<void>();
    private modalClose$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        loading: true,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private router: Router,
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private activatedRoute: ActivatedRoute,
        private appLoading: EvodeskAppLoadingStatusService,
        private uiAlertService: EvodeskAlertModalService,
        private bootstrap: EvodeskProfileBootstrapService,
        private albumsBootstrap: EvodeskBootstrapAlbumsService,
        private albumsStateService: EvodeskAlbumsStateService,
        private profileStateService: EvodeskProfileStateService,
        private feedStateService: EvodeskFeedStateService,
        private feed: EvodeskFeedService,
        private currentUserService: CurrentUserService,
        private profileRESTService: ProfileRESTService,
        private headerConfigurationService: EvodeskHeaderConfigurationService,
        private profileFeedSourceAdapter: ProfileFeedSourceAdapter,
        private profileBreakpoint: EvodeskProfileBreakpointsService,
        private profileUrlService: EvodeskProfileUrlService,
    ) {}

    ngOnInit(): void {
        this.profileBreakpoint.init();

        this.activatedRoute.params.pipe(takeUntil(this.ngOnDestroy$)).subscribe((params) => {
            this.nextParams$.next(undefined);
            this.modalClose$.next(undefined);

            this.state = {
                ...this.state,
                ready: false,
                loading: true,
            };

            this.cdr.detectChanges();

            setTimeout(() => {
                if (! params['id']) {
                    if (!! (this.currentUserService.current.url_alias)) {
                      this.router.navigate(['/profile', this.currentUserService.current.url_alias], { replaceUrl: true });
                    } else {
                      this.router.navigate(['/profile', this.currentUserService.current.id], { replaceUrl: true });
                    }
                } else {
                    const paramsId: string | undefined = params['id'];

                    if (!! paramsId) {
                        this.resolve(params['id']).pipe(takeUntil(this.nextParams$)).subscribe(
                            (id) => {
                                const appLoading: EvodeskAppLoading = this.appLoading.addLoading();

                                this.profileStateService.reset();
                                this.feedStateService.reset();
                                this.albumsStateService.reset();

                                this.profileFeedSourceAdapter.config = {
                                    profileId: id === undefined ? this.currentUserService.current.id : id,
                                    postsPerPage: defaultFeedPostsPerPage,
                                };

                                this.feed.setConfig(() => {
                                    return {
                                        source: this.profileFeedSourceAdapter,
                                    };
                                });

                                this.bootstrap.bootstrap(id).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                                    () => {
                                        this.state = {
                                            ...this.state,
                                            ready: true,
                                            loading: false,
                                        };

                                        this.profileBreakpoint.current$.pipe(distinctUntilChanged(), takeUntil(this.nextProfile$)).subscribe((layoutVariant) => {
                                            this.profileStateService.dispatch({
                                                type: EvodeskProfileStateAction.SetLayoutVariant,
                                                payload: {
                                                    layoutVariant: layoutVariant,
                                                },
                                            });
                                        });

                                        (() => {
                                            this.activatedRoute.queryParams.pipe(takeUntil(this.nextProfile$), take(1)).subscribe((qp) => {
                                                const profile: ProfileModel = this.profileStateService.snapshot.profile;

                                                if ((qp['approve'] && parseInt(qp['approve'], 10) > 0) || (qp['reject'] && parseInt(qp['reject'], 10) > 0)) {
                                                    const matDialogRef: MatDialogRef<WEDecisionModal> = this.matDialog.open(WEDecisionModal, {
                                                        ...defaultBottomMatDialog600pxConfig,
                                                        viewContainerRef: this.viewContainerRef,
                                                        closeOnNavigation: true,
                                                        autoFocus: false,
                                                    });

                                                    matDialogRef.componentInstance.approveId = parseInt(qp['approve'] || qp['reject'], 10);
                                                    matDialogRef.componentInstance.ngOnChanges({ approveId: new SimpleChange(undefined, matDialogRef.componentInstance.approveId, true) });

                                                    matDialogRef.componentInstance.closeEvent.pipe(takeUntil(this.modalClose$)).subscribe(() => {
                                                        this.modalClose$.next(undefined);

                                                        matDialogRef.close();
                                                    });

                                                    matDialogRef.afterClosed().pipe(takeUntil(this.nextProfile$)).subscribe(() => {
                                                        this.router.navigate(this.profileUrlService.getRouterLinkByProfileModel(profile), {
                                                            replaceUrl: true,
                                                        });
                                                    });
                                                }
                                            });
                                        })();

                                        this.albumsBootstrap.bootstrap(id).pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                                            (() => {
                                                appLoading.done();

                                                this.cdr.detectChanges();
                                            })();
                                        });
                                    },
                                    () => {
                                        appLoading.done();

                                        this.state = {
                                            ...this.state,
                                            ready: false,
                                            loading: false,
                                        };

                                        this.notFound();
                                    },
                                );
                            },
                            () => {
                                this.notFound();
                            },
                        );
                    } else {
                        this.router.navigate(['/profile', this.currentUserService.current.url_alias], { replaceUrl: true });
                    }
                }
            });
        });

        this.headerConfigurationService.setVariant(EvodeskHeaderLayoutVariant.Common);
    }

    ngOnDestroy(): void {
        this.nextParams$.next(undefined);
        this.nextProfile$.next(undefined);
        this.modalClose$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    private notFound(): void {
        this.uiAlertService.openTranslated('EvodeskProfile.routes.IndexRoute.NotFound').pipe(takeUntil(this.nextParams$)).subscribe();
    }

    private resolve(paramsId: string | undefined): Observable<number> {
        return Observable.create((result) => {
            const unsubscribe$: Subject<void> = new Subject<void>();

            if (paramsId === undefined) {
                result.next(this.currentUserService.current.id);
                result.complete();
            } else if (/^\d+$/.test(paramsId)) {
                this.profileRESTService.getProfile(parseInt(paramsId, 10)).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                    (profile) => {
                        if (profile.url_alias) {
                            this.router.navigate(['/profile', profile.url_alias], { replaceUrl: true });

                            result.complete();
                        } else {
                            result.next(parseInt(paramsId, 10));
                            result.complete();
                        }
                    },
                    (httpError) => {
                        result.error(httpError);
                    },
                );
            } else {
                if (validateUrlAlias(paramsId)) {
                    this.profileRESTService.resolve(paramsId).pipe(takeUntil(unsubscribe$)).subscribe(
                        (resolvedId) => {
                            if (resolvedId > 0) {
                                result.next(resolvedId);
                            } else {
                                result.error();
                            }
                        },
                        (httpError) => {
                            result.error(httpError);
                        },
                    );
                } else {
                    result.error();
                }
            }

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
