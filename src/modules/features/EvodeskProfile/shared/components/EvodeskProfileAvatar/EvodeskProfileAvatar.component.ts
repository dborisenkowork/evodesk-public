import {filter, takeUntil} from 'rxjs/operators';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';

import {BehaviorSubject, Subject} from 'rxjs';

import {environment} from '../../../../../../environments/environment.config';

import {AppEvent} from '../../../../../app/EvodeskAppMessageBus/models/app-events.model';

import {AuthTokenService} from '../../../../EvodeskAuth/services/AuthToken.service';
import {EvodeskAppMessageBusService} from '../../../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';

type CacheStatus = 'has-image' | 'has-no-image' | 'fetching';

const cache: { [profileId: number]: BehaviorSubject<CacheStatus> } = {};
const blobs: { [profileId: number]: Blob } = {};

interface State {
    profileImage: any;
}

@Component({
    selector: 'evodesk-profile-avatar',
    templateUrl: './EvodeskProfileAvatar.component.pug',
    styleUrls: [
        './EvodeskProfileAvatar.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileAvatarComponent implements OnInit, OnChanges, OnDestroy
{
    private stop$: Subject<void> = new Subject<void>();
    private ngDestroy$: Subject<void> = new Subject<void>();

    @Input() profileId: number;
    @Input() size: number = 32;

    public state: State = {
        profileImage: undefined,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private http: HttpClient,
        private sanitize: DomSanitizer,
        private authToken: AuthTokenService,
        private appBus: EvodeskAppMessageBusService,
    ) {}

    ngOnInit(): void {
        this.appBus.stream$.pipe(
            takeUntil(this.ngDestroy$),
            filter(e => e.type === AppEvent.InvalidateProfileImage && e.payload.profileId === this.profileId),
        ).subscribe(() => {
            if ((cache[this.profileId] || blobs[this.profileId]) && cache[this.profileId].getValue() !== 'fetching') {
                cache[this.profileId] = undefined;
                blobs[this.profileId] = undefined;
            }

            this.load();
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.load();
    }

    ngOnDestroy(): void {
        this.stop$.next(undefined);
        this.ngDestroy$.next(undefined);
    }

    getApiAvatar(profileId: number): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/profile/${profileId}/avatar`;
    }

    get cAvatarStyles(): any {
        return {
            width: this.size + 'px',
            height: this.size + 'px',
        };
    }

    private load(): void {
        this.stop$.next();

        const apiAvatar: string = this.getApiAvatar(this.profileId);

        this.state = {
            ...this.state,
            profileImage: undefined,
        };

        if (this.profileId) {
            if (! cache[this.profileId]) {
                cache[this.profileId] = new BehaviorSubject<CacheStatus>('fetching');

                this.http.get(apiAvatar, { responseType: 'blob', headers: { Token: this.authToken.token.access_token } }).pipe(takeUntil(this.stop$)).subscribe(
                    (response) => {
                        blobs[this.profileId] = response;
                        cache[this.profileId].next('has-image');
                    },
                    () => {
                        blobs[this.profileId] = undefined;
                        cache[this.profileId].next('has-no-image');
                    },
                );
            }

            cache[this.profileId].pipe(takeUntil(this.stop$)).subscribe((status) => {
                switch (status) {
                    case 'has-image': {
                        this.setImage(blobs[this.profileId]);

                        break;
                    }

                    case 'has-no-image': {
                        this.setNoImage();

                        break;
                    }
                }
            });

            if (blobs[this.profileId]) {
                cache[this.profileId].next('has-image');
            }
        } else {
            console.warn('[EvodeskProfileAvatarComponent] No profileId or image (deprecated) provided');
        }
    }

    private setImage(blob: Blob): void {
        const objectUrl: string = window.URL.createObjectURL(blob);

        this.state = {
            ...this.state,
            profileImage: this.sanitize.bypassSecurityTrustResourceUrl(objectUrl),
        };

        this.cdr.detectChanges();
    }

    private setNoImage(): void {
        this.state = {
            ...this.state,
            profileImage: undefined,
        };

        this.cdr.detectChanges();
    }
}
