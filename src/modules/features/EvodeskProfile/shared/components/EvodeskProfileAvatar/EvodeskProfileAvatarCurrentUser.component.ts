import {ChangeDetectionStrategy, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {UserModel} from '../../../../../app/EvodeskRESTApi/models/user/User.model';

import {CurrentUserService} from '../../../../EvodeskAuth/services/CurrentUser.service';

interface State {
    ready: boolean;
    currentUser?: UserModel;
    props?: {
        profileId: number;
        image: string | undefined;
        size: number;
    };
}

@Component({
    selector: 'evodesk-profile-avatar-current-user',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-profile-avatar [profileId]="state.props.profileId" [size]="state.props.size"></evodesk-profile-avatar>
        </ng-container>
    `,
    styles: [
        `
            .c {
                display: inline-block;
            }
        `,
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileAvatarCurrentUserComponent implements OnChanges, OnInit, OnDestroy
{
    @Input() size: number = 32;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private currentUserService: CurrentUserService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.update();
    }

    ngOnInit(): void {
        this.currentUserService.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(u => !! u),
        ).subscribe((u) => {
            this.state = {
                ...this.state,
                currentUser: u,
            };

            this.update();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    private update(): void {
        if (this.state.currentUser) {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    profileId: this.state.currentUser.id,
                    image: this.state.currentUser.images[0],
                    size: this.size,
                },
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }
}
