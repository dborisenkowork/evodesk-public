import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

interface State {
    ready: boolean;
}

export interface EvodeskProfileSubscribeButtonComponentProps {
    isSubscribed: boolean;
    isLoading: boolean;
}

@Component({
    selector: 'evodesk-profile-subscribe-button',
    templateUrl: './EvodeskProfileSubscribeButton.component.pug',
    styleUrls: [
        './EvodeskProfileSubscribeButton.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileSubscribeButtonComponent implements OnChanges
{
    @Input() props: EvodeskProfileSubscribeButtonComponentProps;

    @Output('subscribe') subscribeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('unsubscribe') unsubscribeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    subscribe(): void {
        this.subscribeEvent.emit(undefined);
    }

    unsubscribe(): void {
        this.unsubscribeEvent.emit(undefined);
    }
}
