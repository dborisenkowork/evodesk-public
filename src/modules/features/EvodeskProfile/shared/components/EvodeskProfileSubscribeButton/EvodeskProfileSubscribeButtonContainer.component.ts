import {ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {proxy} from '../../../../../../functions/proxy.function';

import {defaultBottomMatDialogConfig} from '../../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {EvodeskProfileSubscribeButtonComponentProps} from './EvodeskProfileSubscribeButton.component';

import {ProfileRESTService} from '../../../../../app/EvodeskRESTApi/services/ProfileREST.service';
import {EvodeskProfileStateAction, EvodeskProfileStateRelation, EvodeskProfileStateService} from '../../../services/EvodeskProfileState.service';
import {EvodeskProfileFollowNotificationModalComponent, EvodeskProfileFollowNotificationModalComponentProps} from '../../../components/EvodeskProfileFollowNotification/EvodeskProfileFollowNotificationModal.component';
import {CurrentUserService} from '../../../../EvodeskAuth/services/CurrentUser.service';

interface State {
    ready: boolean;
    props?: EvodeskProfileSubscribeButtonComponentProps;
    notificationModal?: MatDialogRef<any>;
}

@Component({
    selector: 'evodesk-profile-subscribe-button-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-profile-subscribe-button [props]="state.props" (subscribe)="subscribe()" (unsubscribe)="unsubscribe()"></evodesk-profile-subscribe-button>
      </ng-container>
    `,
})
export class EvodeskProfileSubscribeButtonContainerComponent implements OnChanges, OnDestroy
{
    @Input() profileId: number;
    @Input() profileName: string;
    @Input() profilePost: string;
    @Input() profileImage: string | undefined | null;
    @Input() isSubscribed: boolean = false;

    private nextAction$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
        props: {
            isSubscribed: this.isSubscribed,
            isLoading: false,
        },
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private matDialog: MatDialog,
        private currentUserService: CurrentUserService,
        private profileRESTService: ProfileRESTService,
        private profileStateService: EvodeskProfileStateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['isSubscribed']) {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    isSubscribed: this.isSubscribed,
                },
            };

            this.cdr.detectChanges();
            this.cdr.detectChanges();
        }
    }

    ngOnDestroy(): void {
        this.nextAction$.next(undefined);
        this.ngOnDestroy$.next(undefined);

        if (this.state.notificationModal) {
            this.state.notificationModal.close();
        }
    }

    subscribe(): void {
        this.nextAction$.next(undefined);

        const commit: Function = () => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    isLoading: true,
                    isSubscribed: true,
                },
            };

            this.profileStateService.dispatch({
                type: EvodeskProfileStateAction.Subscribe,
                payload: {
                    newRelation: {
                        id: this.currentUserService.impersonatedAs.id,
                        name: this.currentUserService.impersonatedAs.name,
                        email: this.currentUserService.impersonatedAs.email,
                        position: this.currentUserService.impersonatedAs.position,
                    },
                },
            });

            this.cdr.detectChanges();
        };

        const revert: Function = () => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    isLoading: false,
                    isSubscribed: false,
                },
            };

            this.profileStateService.dispatch({
                type: EvodeskProfileStateAction.RevertSubscribe,
                payload: {
                    profileId: this.currentUserService.impersonatedAs.id,
                },
            });

            this.cdr.detectChanges();
        };

        commit();

        proxy(this.profileRESTService.followOn(
            this.profileId).pipe(takeUntil(this.nextAction$)),
        ).pipe(takeUntil(this.nextAction$)).subscribe(
            () => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        isLoading: false,
                    },
                };

                this.notifyAboutFollow({
                    isFollowed: true,
                });
            },
            () => {
                revert();
            },
        );
    }

    unsubscribe(): void {
        this.nextAction$.next(undefined);

        const follower: EvodeskProfileStateRelation = this.profileStateService.snapshot.relations.followers.filter(f => f.id.toString() === this.currentUserService.impersonatedAs.id.toString())[0];

        if (follower) {
            const commit: Function = () => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        isLoading: true,
                        isSubscribed: false,
                    },
                };

                this.profileStateService.dispatch({
                    type: EvodeskProfileStateAction.Unsubscribe,
                    payload: {
                        profileId: this.currentUserService.impersonatedAs.id,
                    },
                });
            };

            const revert: Function = () => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        isLoading: false,
                        isSubscribed: true,
                    },
                };

                this.profileStateService.dispatch({
                    type: EvodeskProfileStateAction.RevertUnsubscribe,
                    payload: {
                        origRelation: follower,
                    },
                });

                this.cdr.detectChanges();
                this.cdr.detectChanges();
            };

            commit();

            proxy(
                this.profileRESTService.followOff(this.profileId).pipe(takeUntil(this.nextAction$)),
            ).pipe(takeUntil(this.nextAction$)).subscribe(
                () => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            isLoading: false,
                        },
                    };

                    this.notifyAboutFollow({
                        isFollowed: false,
                    });
                },
                () => {
                    revert();
                },
            );
        }
    }

    private notifyAboutFollow(options: { isFollowed: boolean }): void {
        const matDialog: MatDialogRef<any> = this.matDialog.open(EvodeskProfileFollowNotificationModalComponent, {
            ...defaultBottomMatDialogConfig,
            data: <EvodeskProfileFollowNotificationModalComponentProps>{
                profileId: this.profileId,
                profileName: this.profileName,
                profilePost: this.profilePost,
                profileImage: this.profileImage,
                followed: options.isFollowed,
            },
        });

        matDialog.afterClosed().pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
            this.state = {
                ...this.state,
                notificationModal: undefined,
            };
        });

        this.state = {
            ...this.state,
            notificationModal: matDialog,
        };
    }
}
