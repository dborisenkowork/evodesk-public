import {Injectable} from '@angular/core';

import {ProfileId, ProfileModel, ProfileUrlAlias} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

@Injectable()
export class EvodeskProfileUrlService
{
    getRouterLinkByProfileId(id: ProfileId): Array<any> {
        return ['/profile', id];
    }

    getRouterLinkByIdAndUrlAlias(id: ProfileId, urlAlias: ProfileUrlAlias): Array<any> {
        if (urlAlias) {
            return ['/profile', urlAlias];
        } else {
            return ['/profile', id];
        }
    }

    getRouterLinkByProfileModel(profile: ProfileModel): Array<any> {
        return this.getRouterLinkByIdAndUrlAlias(profile.id, profile.url_alias);
    }
}
