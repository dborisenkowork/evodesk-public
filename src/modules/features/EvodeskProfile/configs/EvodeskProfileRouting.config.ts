import {Routes} from '@angular/router';

import {HasAuthTokenGuard} from '../../EvodeskAuth/guards/HasAuthToken.guard';
import {CurrentUserGuard} from '../../EvodeskAuth/guards/CurrentUser.guard';

import {IndexRouteComponent} from '../routes/IndexRoute/IndexRoute.component';

export const EvodeskProfileRouting: Routes = [
    {
        path: '',
        component: IndexRouteComponent,
        canActivate: [
            HasAuthTokenGuard,
            CurrentUserGuard,
        ],
    },
    {
        path: ':id',
        component: IndexRouteComponent,
        canActivate: [
            HasAuthTokenGuard,
            CurrentUserGuard,
        ],
    },
];
