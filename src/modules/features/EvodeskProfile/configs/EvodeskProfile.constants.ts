export const evodeskProfileLayoutMobile: { to: number } = { to: 420 };
export const evodeskProfileLayoutTablet: { from: number, to: number } = { from: 421, to: 1160 };
export const evodeskProfileLayoutDesktop: { from: number } = { from: 1161 };

export const evodeskMinUserCompetenceCommentLength: number = 10;
export const evodeskMaxUserCompetenceCommentLength: number = 65535;
