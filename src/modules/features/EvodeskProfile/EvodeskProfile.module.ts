import {NgModule} from '@angular/core';

import {EvodeskAppSharedModule} from '../../app/EvodeskApp/EvodeskAppShared.module';

import {EvodeskFeedModule} from '../EvodeskFeed/EvodeskFeed.module';

import {EvodeskProfileRoutingModule} from './EvodeskProfileRouting.module';
import {EvodeskAlbumsModule} from '../EvodeskAlbums/EvodeskAlbums.module';

import {IndexRouteComponent} from './routes/IndexRoute/IndexRoute.component';

import {EvodeskProfileWorkExperienceBodyParserPipe} from './pipes/EvodeskProfileWorkExperienceBodyParser.pipe';

import {EvodeskProfileComponent} from './components/EvodeskProfile/EvodeskProfile.component';
import {EvodeskProfileContainerComponent} from './components/EvodeskProfile/EvodeskProfileContainer.component';
import {EvodeskProfileHeaderComponent} from './components/EvodeskProfileHeader/EvodeskProfileHeader.component';
import {EvodeskProfileHeaderContainerComponent} from './components/EvodeskProfileHeader/EvodeskProfileHeaderContainer.component';
import {EvodeskProfileAchievementsComponent} from './components/EvodeskProfileAchievements/EvodeskProfileAchievements.component';
import {EvodeskProfileAchievementsContainerComponent} from './components/EvodeskProfileAchievements/EvodeskProfileAchievementsContainer.component';
import {EvodeskProfileCommonComponent} from './components/EvodeskProfileCommon/EvodeskProfileCommon.component';
import {EvodeskProfileCommonContainerComponent} from './components/EvodeskProfileCommon/EvodeskProfileCommonContainer.component';
import {EvodeskProfileFeedContainerComponent} from './components/EvodeskProfileFeed/EvodeskProfileFeedContainer.component';
import {EvodeskProfileWorkExperienceComponent} from './components/EvodeskProfileWorkExperience/EvodeskProfileWorkExperience.component';
import {EvodeskProfileWorkExperienceContainerComponent} from './components/EvodeskProfileWorkExperience/EvodeskProfileWorkExperienceContainer.component';
import {EvodeskProfileFeedComponent} from './components/EvodeskProfileFeed/EvodeskProfileFeed.component';
import {EvodeskProfileFeedPostEditorContainerComponent} from './components/EvodeskProfileFeedPostEditor/EvodeskProfileFeedPostEditorContainer.component';
import {EvodeskProfileFollowersComponent} from './components/EvodeskProfileFollowers/EvodeskProfileFollowers.component';
import {EvodeskProfileFollowersContainerComponent} from './components/EvodeskProfileFollowers/EvodeskProfileFollowersContainer.component';
import {EvodeskProfileFollowersModalComponent} from './components/EvodeskProfileFollowers/EvodeskProfileFollowersModal.component';
import {EvodeskProfileFollowingComponent} from './components/EvodeskProfileFollowing/EvodeskProfileFollowing.component';
import {EvodeskProfileFollowingContainerComponent} from './components/EvodeskProfileFollowing/EvodeskProfileFollowingContainer.component';
import {EvodeskProfileFollowingModalComponent} from './components/EvodeskProfileFollowing/EvodeskProfileFollowingModal.component';
import {EvodeskProfileFollowNotificationModalComponent} from './components/EvodeskProfileFollowNotification/EvodeskProfileFollowNotificationModal.component';
import {EvodeskProfilePhotosComponent} from './components/EvodeskProfilePhotos/EvodeskProfilePhotos.component';
import {EvodeskProfilePhotosContainerComponent} from './components/EvodeskProfilePhotos/EvodeskProfilePhotosContainer.component';
import {EvodeskProfileFooterComponent} from './components/EvodeskProfileFooter/EvodeskProfileFooter.component';
import {EvodeskProfileFooterContainerComponent} from './components/EvodeskProfileFooter/EvodeskProfileFooterContainer.component';
import {EvodeskProfileWorkExperienceModalComponent} from './components/EvodeskProfileWorkExperienceModal/EvodeskProfileWorkExperienceModal.component';
import {EvodeskProfileWorkExperienceModalContainerComponent} from './components/EvodeskProfileWorkExperienceModal/EvodeskProfileWorkExperienceModalContainer.component';
import {EvodeskProfileWorkExperienceDecideComponent} from './components/EvodeskProfileWorkExperienceDecide/EvodeskProfileWorkExperienceDecide.component';
import {EvodeskProfileWorkExperienceDecideContainerComponent} from './components/EvodeskProfileWorkExperienceDecide/EvodeskProfileWorkExperienceDecideContainer.component';
import {EvodeskProfileWorkExperienceEntryComponent} from './components/EvodeskProfileWorkExperienceEntry/EvodeskProfileWorkExperienceEntry.component';
import {EvodeskProfileWorkExperienceEntryContainerComponent} from './components/EvodeskProfileWorkExperienceEntry/EvodeskProfileWorkExperienceEntryContainer.component';
import {EvodeskProfileCompetenceComponent} from './components/EvodeskProfileCompetence/EvodeskProfileCompetence.component';
import {EvodeskProfileCompetenceContainerComponent} from './components/EvodeskProfileCompetence/EvodeskProfileCompetenceContainer.component';
import {EvodeskProfileCompetenceDetailsComponent} from './components/EvodeskProfileCompetenceDetails/EvodeskProfileCompetenceDetails.component';
import {EvodeskProfileCompetenceDetailsContainerComponent} from './components/EvodeskProfileCompetenceDetails/EvodeskProfileCompetenceDetailsContainer.component';
import {EvodeskProfileCompetenceMarkComponent} from './components/EvodeskProfileCompetenceMark/EvodeskProfileCompetenceMark.component';
import {EvodeskProfileCompetenceMarkContainerComponent} from './components/EvodeskProfileCompetenceMark/EvodeskProfileCompetenceMark.Container.component';
import {EvodeskProfileCompetenceDocumentsComponent} from './components/EvodeskProfileCompetenceDocuments/EvodeskProfileCompetenceDocuments.component';
import {EvodeskProfileCompetenceDocumentsContainerComponent} from './components/EvodeskProfileCompetenceDocuments/EvodeskProfileCompetenceDocumentsContainer.component';
import {EvodeskProfileCompetenceMarksComponent} from './components/EvodeskProfileCompetenceMarks/EvodeskProfileCompetenceMarks.component';
import {EvodeskProfileCompetenceMarksContainerComponent} from './components/EvodeskProfileCompetenceMarks/EvodeskProfileCompetenceMarksContainer.component';
import {EvodeskProfileCompetenceProgressComponent} from './components/EvodeskProfileCompetenceProgress/EvodeskProfileCompetenceProgress.component';
import {EvodeskProfileCompetenceSkillsComponent} from './components/EvodeskProfileCompetenceSkills/EvodeskProfileCompetenceSkills.component';
import {EvodeskProfileCompetencePointsComponent} from './components/EvodeskProfileCompetencePoints/EvodeskProfileCompetencePoints.component';
import {EvodeskProfileCompetencePointsContainerComponent} from './components/EvodeskProfileCompetencePoints/EvodeskProfileCompetencePointsContainer.component';

@NgModule({
    imports: [
        EvodeskAppSharedModule,
        EvodeskAlbumsModule,
        EvodeskFeedModule,
        EvodeskProfileRoutingModule,
    ],
    exports: [
        EvodeskProfileFollowersModalComponent,
        EvodeskProfileFollowingModalComponent,
    ],
    declarations: [
        IndexRouteComponent,

        EvodeskProfileWorkExperienceBodyParserPipe,

        EvodeskProfileFollowersModalComponent,
        EvodeskProfileFollowingModalComponent,
        EvodeskProfileFollowNotificationModalComponent,

        EvodeskProfileComponent,
        EvodeskProfileContainerComponent,
        EvodeskProfileAchievementsComponent,
        EvodeskProfileAchievementsContainerComponent,
        EvodeskProfileCommonComponent,
        EvodeskProfileCommonContainerComponent,
        EvodeskProfileFeedContainerComponent,
        EvodeskProfileFeedPostEditorContainerComponent,
        EvodeskProfileHeaderComponent,
        EvodeskProfileHeaderContainerComponent,
        EvodeskProfileCompetenceComponent,
        EvodeskProfileCompetenceContainerComponent,
        EvodeskProfileWorkExperienceComponent,
        EvodeskProfileWorkExperienceContainerComponent,
        EvodeskProfileFeedComponent,
        EvodeskProfileFeedContainerComponent,
        EvodeskProfileFollowersComponent,
        EvodeskProfileFollowersContainerComponent,
        EvodeskProfileFollowingComponent,
        EvodeskProfileFollowingContainerComponent,
        EvodeskProfilePhotosComponent,
        EvodeskProfilePhotosContainerComponent,
        EvodeskProfileFooterComponent,
        EvodeskProfileFooterContainerComponent,
        EvodeskProfileWorkExperienceEntryComponent,
        EvodeskProfileWorkExperienceEntryContainerComponent,
        EvodeskProfileWorkExperienceModalComponent,
        EvodeskProfileWorkExperienceModalContainerComponent,
        EvodeskProfileWorkExperienceDecideComponent,
        EvodeskProfileWorkExperienceDecideContainerComponent,
        EvodeskProfileCompetenceDetailsComponent,
        EvodeskProfileCompetenceDetailsContainerComponent,
        EvodeskProfileCompetenceMarkComponent,
        EvodeskProfileCompetenceMarkContainerComponent,
        EvodeskProfileCompetenceDocumentsComponent,
        EvodeskProfileCompetenceDocumentsContainerComponent,
        EvodeskProfileCompetenceMarksComponent,
        EvodeskProfileCompetenceMarksContainerComponent,
        EvodeskProfileCompetenceProgressComponent,
        EvodeskProfileCompetenceSkillsComponent,
        EvodeskProfileCompetencePointsComponent,
        EvodeskProfileCompetencePointsContainerComponent,
    ],
    entryComponents: [
        EvodeskProfileFollowersModalComponent,
        EvodeskProfileFollowingModalComponent,
        EvodeskProfileFollowNotificationModalComponent,
        EvodeskProfileWorkExperienceModalContainerComponent,
        EvodeskProfileWorkExperienceDecideContainerComponent,
        EvodeskProfileCompetenceDetailsContainerComponent,
        EvodeskProfileCompetenceMarkContainerComponent,
        EvodeskProfileCompetenceDocumentsContainerComponent,
        EvodeskProfileCompetenceMarksContainerComponent,
    ],
})
export class EvodeskProfileModule
{
}
