import {Injectable} from '@angular/core';

import {Subject, BehaviorSubject, Observable} from 'rxjs';

import {ProfileId, ProfileModel} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';
import {AchievementModel} from '../../../app/EvodeskRESTApi/models/achievements/Achievement.model';
import {ProfileLayoutVariant, ProfileRelationModel} from '../models/EvodeskProfile.model';
import {ProfileEducationModel} from '../../../app/EvodeskRESTApi/models/profile/ProfileEducation.model';
import {ProfileExperienceId, ProfileExperienceModel} from '../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';
import {CompetenceIndexesModel} from '../../../app/EvodeskRESTApi/models/CompetenceIndexes.model';
import {UserCompetenceModel, UserCompetenceModelId} from '../../../app/EvodeskRESTApi/models/UserCompetence.model';
import {UserCompetenceSkillModel} from '../../../app/EvodeskRESTApi/models/UserCompetenceSkill.model';
import {UserCompetenceDocumentModel} from '../../../app/EvodeskRESTApi/models/UserCompetenceDocument.model';

export interface EvodeskProfileStateRelations {
    followers: Array<EvodeskProfileStateRelation>;
    following: Array<EvodeskProfileStateRelation>;
}

export type EvodeskProfileStateRelation = ProfileRelationModel;

export interface EvodeskProfileState {
    layoutVariant: ProfileLayoutVariant;
    isOwnProfile: boolean;
    profile: ProfileModel;
    relations: EvodeskProfileStateRelations;
    viewerRelations: EvodeskProfileStateRelations;
    achievements: Array<AchievementModel>;
    educations: Array<ProfileEducationModel>;
    experiences: Array<ProfileExperienceModel>;
    competences: Array<UserCompetenceModel>;
    competenceSkills: Array<UserCompetenceSkillModel>;
    competenceDocuments: Array<UserCompetenceDocumentModel>;
    indexes: CompetenceIndexesModel;
}

type PrevProfileState = EvodeskProfileState | undefined;
type CurrentProfileState = EvodeskProfileState | undefined;

interface States {
    previous: PrevProfileState;
    current: CurrentProfileState;
}

function initialState(): CurrentProfileState {
    return undefined;
}

export enum EvodeskProfileStateAction {
    Bootstrap,
    SetLayoutVariant,
    RemoveFollower,
    RemoveFollowing,
    Subscribe,
    RevertSubscribe,
    Unsubscribe,
    RevertUnsubscribe,
    WorkExperienceAcceptApprove,
    WorkExperienceRejectApprove,
    SetUserCompetenceMark,
    UpdateUserCompetences,
}

export interface EvodeskProfileStateBootstrapQuery {
    isOwnProfile: boolean;
    profile: ProfileModel;
    relations: EvodeskProfileStateRelations;
    viewerRelations: EvodeskProfileStateRelations;
    achievements: Array<AchievementModel>;
    educations: Array<ProfileEducationModel>;
    experiences: Array<ProfileExperienceModel>;
    competences: Array<UserCompetenceModel>;
    competenceSkills: Array<UserCompetenceSkillModel>;
    competenceDocuments: Array<UserCompetenceDocumentModel>;
    indexes: CompetenceIndexesModel;
}

type EvodeskProfileStateActions =
      { type: EvodeskProfileStateAction.Bootstrap, payload: EvodeskProfileStateBootstrapQuery }
    | { type: EvodeskProfileStateAction.SetLayoutVariant, payload: { layoutVariant: ProfileLayoutVariant } }
    | { type: EvodeskProfileStateAction.RemoveFollower, payload: { relationId: number } }
    | { type: EvodeskProfileStateAction.RemoveFollowing, payload: { relationId: number } }
    | { type: EvodeskProfileStateAction.Subscribe, payload: { newRelation: EvodeskProfileStateRelation } }
    | { type: EvodeskProfileStateAction.RevertSubscribe, payload: { profileId: ProfileId } }
    | { type: EvodeskProfileStateAction.Unsubscribe, payload: { profileId: ProfileId } }
    | { type: EvodeskProfileStateAction.RevertUnsubscribe, payload: { origRelation: ProfileRelationModel } }
    | { type: EvodeskProfileStateAction.WorkExperienceAcceptApprove, payload: { experienceId: ProfileExperienceId, from: ProfileId } }
    | { type: EvodeskProfileStateAction.WorkExperienceRejectApprove, payload: { experienceId: ProfileExperienceId, from: ProfileId } }
    | { type: EvodeskProfileStateAction.SetUserCompetenceMark, payload: { userCompetenceId: UserCompetenceModelId, newMark: string } }
    | { type: EvodeskProfileStateAction.UpdateUserCompetences, payload: { userCompetences: Array<UserCompetenceModel> } }
;

@Injectable()
export class EvodeskProfileStateService
{
    private destroy$: Subject<void> = new Subject<void>();

    private _current$: BehaviorSubject<States> = new BehaviorSubject<States>({
        previous: undefined,
        current: initialState(),
    });

    get current$(): Observable<States> {
        return this._current$.asObservable();
    }

    get snapshot(): CurrentProfileState {
        return this._current$.getValue().current;
    }

    reset(): void {
        this._current$.next({
            previous: undefined,
            current: initialState(),
        });
    }

    destroy(): void {
        this.destroy$.next(undefined);
        this.reset();
    }

    dispatch(action: EvodeskProfileStateActions): void {
        switch (action.type) {
            case EvodeskProfileStateAction.Bootstrap:
                this.setState((orig) => {
                    return {
                        ...orig,
                        ...action.payload,
                    };
                });

                break;

            case EvodeskProfileStateAction.SetLayoutVariant:
                this.setState((orig) => {
                    return {
                        ...orig,
                        layoutVariant: action.payload.layoutVariant,
                    };
                });

                break;

            case EvodeskProfileStateAction.RemoveFollower:
                this.setState((orig) => {
                    return {
                        ...orig,
                        relations: {
                            ...orig.relations,
                            followers: orig.relations.followers.filter(f => f.id !== action.payload.relationId),
                        },
                    };
                });

                break;

            case EvodeskProfileStateAction.RemoveFollowing:
                this.setState((orig) => {
                    return {
                        ...orig,
                        relations: {
                            ...orig.relations,
                            following: orig.relations.following.filter(f => f.id !== action.payload.relationId),
                        },
                    };
                });

                break;

            case EvodeskProfileStateAction.Subscribe:
                this.setState((orig) => {
                    if (orig.isOwnProfile) {
                        return {
                            ...orig,
                            relations: {
                                ...orig.relations,
                                followers: [
                                    ...orig.relations.followers,
                                    action.payload.newRelation,
                                ],
                            },
                            viewerRelations: {
                                ...orig.viewerRelations,
                                following: [
                                    ...orig.viewerRelations.following,
                                    action.payload.newRelation,
                                ],
                            },
                        };
                    } else {
                        return {
                            ...orig,
                            relations: {
                                ...orig.relations,
                                followers: [
                                    ...orig.relations.followers,
                                    action.payload.newRelation,
                                ],
                            },
                        };
                    }
                });

                break;

            case EvodeskProfileStateAction.RevertSubscribe:
                this.setState((orig) => {
                    if (orig.isOwnProfile) {
                        return {
                            ...orig,
                            relations: {
                                ...orig.relations,
                                followers: orig.relations.followers.filter(f => f.id.toString() !== action.payload.profileId.toString()),
                            },
                        };
                    } else {
                        return {
                            ...orig,
                            relations: {
                                ...orig.relations,
                                followers: orig.relations.followers.filter(f => f.id.toString() !== action.payload.profileId.toString()),
                            },
                        };
                    }
                });

                break;

            case EvodeskProfileStateAction.Unsubscribe:
                this.setState((orig) => {
                    return {
                        ...orig,
                        relations: {
                            ...orig.relations,
                            followers: orig.relations.followers.filter(f => f.id.toString() !== action.payload.profileId.toString()),
                        },
                    };
                });

                break;

            case EvodeskProfileStateAction.RevertUnsubscribe:
                this.setState((orig) => {
                    return {
                        ...orig,
                        relations: {
                            ...orig.relations,
                            followers: [
                                ...orig.relations.followers,
                                action.payload.origRelation,
                            ],
                        },
                    };
                });

                break;

            case EvodeskProfileStateAction.WorkExperienceAcceptApprove:
                this.setState((orig) => {
                    return {
                        ...orig,
                        experiences: orig.experiences.map((e) => {
                            if (e.id.toString() === action.payload.experienceId.toString()) {
                                return {
                                    ...e,
                                    approves: e.approves.map((a) => {
                                        if (a.user_id.toString() === action.payload.from.toString()) {
                                            return {
                                                ...a,
                                                status: <0 | 1>1,
                                            };
                                        } else {
                                            return a;
                                        }
                                    }),
                                };
                            } else {
                                return e;
                            }
                        }),
                    };
                });

                break;

            case EvodeskProfileStateAction.WorkExperienceRejectApprove:
                this.setState((orig) => {
                    return {
                        ...orig,
                        experiences: orig.experiences.map((e) => {
                            if (e.id.toString() === action.payload.experienceId.toString()) {
                                return {
                                    ...e,
                                    approves: e.approves.filter((a) => a.user_id.toString() !== action.payload.from.toString()),
                                };
                            } else {
                                return e;
                            }
                        }),
                    };
                });

                break;

            case EvodeskProfileStateAction.SetUserCompetenceMark:
                this.setState((orig) => {
                    return {
                        ...orig,
                        competences: orig.competences.map((c) => {
                            if (c.id === action.payload.userCompetenceId) {
                                return {
                                    ...c,
                                    can_mark: false,
                                    points: action.payload.newMark,
                                };
                            } else {
                                return c;
                            }
                        }),
                    };
                });

                break;

            case EvodeskProfileStateAction.UpdateUserCompetences:
                this.setState((orig) => {
                    return {
                        ...orig,
                        competences: action.payload.userCompetences,
                    };
                });

                break;
        }
    }

    private setState(set$: (orig: EvodeskProfileState) => EvodeskProfileState) {
        const orig: EvodeskProfileState = this._current$.getValue().current;

        this._current$.next({
            previous: orig,
            current: set$(orig),
        });
    }
}
