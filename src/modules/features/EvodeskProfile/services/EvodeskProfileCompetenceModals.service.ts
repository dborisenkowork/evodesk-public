import {Injectable, ViewContainerRef, SimpleChange} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject, Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {defaultBottomMatDialog720pxConfig, defaultBottomMatDialog800pxConfig, defaultBottomMatDialogConfig} from '../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {UserCompetenceModel} from '../../../app/EvodeskRESTApi/models/UserCompetence.model';
import {UserCompetenceMark} from '../models/EvodeskProfile.model';
import {UserCompetenceDocumentModel, UserCompetenceDocumentStatus} from '../../../app/EvodeskRESTApi/models/UserCompetenceDocument.model';
import {CompetenceMarkModel} from '../../../app/EvodeskRESTApi/models/CompetenceMark.model';

import {EvodeskProfileCompetenceMarkContainerComponent as MarkModal} from '../components/EvodeskProfileCompetenceMark/EvodeskProfileCompetenceMark.Container.component';
import {EvodeskProfileCompetenceDetailsContainerComponent as DetailsModal} from '../components/EvodeskProfileCompetenceDetails/EvodeskProfileCompetenceDetailsContainer.component';
import {EvodeskProfileCompetenceMarksContainerComponent as MarksModal} from '../components/EvodeskProfileCompetenceMarks/EvodeskProfileCompetenceMarksContainer.component';
import {EvodeskProfileCompetenceDocumentsContainerComponent as DocumentsModal} from '../components/EvodeskProfileCompetenceDocuments/EvodeskProfileCompetenceDocumentsContainer.component';

import {EvodeskAlertModalService} from '../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';
import {EvodeskProfileStateService} from './EvodeskProfileState.service';

@Injectable()
export class EvodeskProfileCompetenceModalsService
{
    constructor(
        private matDialog: MatDialog,
        private profileState: EvodeskProfileStateService,
        private uiAlertService: EvodeskAlertModalService,
    ) {}

    mark(userCompetence: UserCompetenceModel, mark: UserCompetenceMark, viewContainerRef: ViewContainerRef): Observable<CompetenceMarkModel> {
        const modalClose$: Subject<void> = new Subject<void>();

        return Observable.create((response) => {
            const matDialogRef: MatDialogRef<MarkModal> = this.matDialog.open(MarkModal, {
                ...defaultBottomMatDialogConfig,
                closeOnNavigation: true,
                autoFocus: true,
                viewContainerRef: viewContainerRef,
                disableClose: true,
            });

            matDialogRef.componentInstance.mark = mark;
            matDialogRef.componentInstance.userCompetence = userCompetence;

            matDialogRef.componentInstance.ngOnChanges({
                mark: new SimpleChange(undefined, matDialogRef.componentInstance.mark, true),
                userCompetence: new SimpleChange(undefined, matDialogRef.componentInstance.userCompetence, true),
            });

            matDialogRef.componentInstance.closeEvent.pipe(takeUntil(modalClose$)).subscribe(() => {
                modalClose$.next(undefined);
                matDialogRef.close();
                response.complete();
            });

            matDialogRef.componentInstance.successEvent.pipe(takeUntil(modalClose$)).subscribe((s) => {
                response.next(s);
            });

            matDialogRef.afterClosed().pipe(takeUntil(modalClose$)).subscribe(() => {
                modalClose$.next(undefined);
                matDialogRef.close();
                response.complete();
            });

            return () => {
                modalClose$.next(undefined);
                response.complete();
            };
        });
    }

    details(userCompetence: UserCompetenceModel, mark: UserCompetenceMark, viewContainerRef: ViewContainerRef): void {
        const modalClose$: Subject<void> = new Subject<void>();

        const matDialogRef: MatDialogRef<DetailsModal> = this.matDialog.open(DetailsModal, {
            ...defaultBottomMatDialog800pxConfig,
            closeOnNavigation: true,
            autoFocus: true,
            viewContainerRef: viewContainerRef,
            disableClose: true,
        });

        matDialogRef.componentInstance.userCompetence = userCompetence;

        matDialogRef.componentInstance.ngOnChanges({
            userCompetence: new SimpleChange(undefined, matDialogRef.componentInstance.userCompetence, true),
        });

        matDialogRef.componentInstance.closeEvent.pipe(takeUntil(modalClose$)).subscribe(() => {
            modalClose$.next(undefined);
            matDialogRef.close();
        });

        matDialogRef.afterClosed().pipe(takeUntil(modalClose$)).subscribe(() => {
            modalClose$.next(undefined);
            matDialogRef.close();
        });
    }

    marks(userCompetence: UserCompetenceModel, mark: UserCompetenceMark, viewContainerRef: ViewContainerRef): void {
        const modalClose$: Subject<void> = new Subject<void>();

        if (parseInt(<any>userCompetence.marks, 10) > 0) {
            const matDialogRef: MatDialogRef<MarksModal> = this.matDialog.open(MarksModal, {
                ...defaultBottomMatDialog720pxConfig,
                closeOnNavigation: true,
                autoFocus: true,
                viewContainerRef: viewContainerRef,
                disableClose: true,
            });

            matDialogRef.componentInstance.userCompetence = userCompetence;
            matDialogRef.componentInstance.asModal = true;

            matDialogRef.componentInstance.ngOnChanges({
                userCompetence: new SimpleChange(undefined, matDialogRef.componentInstance.userCompetence, true),
            });

            matDialogRef.componentInstance.closeEvent.pipe(takeUntil(modalClose$)).subscribe(() => {
                modalClose$.next(undefined);
                matDialogRef.close();
            });

            matDialogRef.afterClosed().pipe(takeUntil(modalClose$)).subscribe(() => {
                modalClose$.next(undefined);
                matDialogRef.close();
            });
        } else {
            this.uiAlertService.openTranslated('EvodeskProfile.services.EvodeskProfileCompetenceModalService.NoMarks').subscribe();
        }
    }

    documents(userCompetence: UserCompetenceModel, mark: UserCompetenceMark, viewContainerRef: ViewContainerRef): void {
        const modalClose$: Subject<void> = new Subject<void>();

        const activeDocuments: Array<UserCompetenceDocumentModel> = this.profileState.snapshot.competenceDocuments.filter(d => d.competence_id.id === userCompetence.competence.id && d.status === UserCompetenceDocumentStatus.Accepted);

        if (activeDocuments.length > 0) {
            const matDialogRef: MatDialogRef<DocumentsModal> = this.matDialog.open(DocumentsModal, {
                ...defaultBottomMatDialog720pxConfig,
                closeOnNavigation: true,
                autoFocus: true,
                viewContainerRef: viewContainerRef,
                disableClose: true,
            });

            matDialogRef.componentInstance.userCompetence = userCompetence;
            matDialogRef.componentInstance.asModal = true;

            matDialogRef.componentInstance.ngOnChanges({
                userCompetence: new SimpleChange(undefined, matDialogRef.componentInstance.userCompetence, true),
                asModal: new SimpleChange(undefined, matDialogRef.componentInstance.asModal, true),
            });

            matDialogRef.componentInstance.closeEvent.pipe(takeUntil(modalClose$)).subscribe(() => {
                modalClose$.next(undefined);
                matDialogRef.close();
            });

            matDialogRef.afterClosed().pipe(takeUntil(modalClose$)).subscribe(() => {
                modalClose$.next(undefined);
                matDialogRef.close();
            });
        } else {
            this.uiAlertService.openTranslated('EvodeskProfile.services.EvodeskProfileCompetenceModalService.NoDocuments').subscribe();
        }
    }
}
