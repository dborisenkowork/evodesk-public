import {Injectable} from '@angular/core';

import {forkJoin, Observable, Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';

import {AppEvent} from '../../../app/EvodeskAppMessageBus/models/app-events.model';
import {UserModel} from '../../../app/EvodeskRESTApi/models/user/User.model';

import {CurrentUserService} from '../../EvodeskAuth/services/CurrentUser.service';
import {EvodeskProfileStateAction, EvodeskProfileStateBootstrapQuery, EvodeskProfileStateRelation, EvodeskProfileStateService} from './EvodeskProfileState.service';
import {ProfileRESTService} from '../../../app/EvodeskRESTApi/services/ProfileREST.service';
import {EvodeskFeedStateAction, EvodeskFeedStateService} from '../../EvodeskFeed/state/EvodeskFeedState.service';
import {ProfileEducationRESTService} from '../../../app/EvodeskRESTApi/services/ProfileEducationREST.service';
import {ProfileWorkExperienceRESTService} from '../../../app/EvodeskRESTApi/services/ProfileWorkExperienceREST.service';
import {EvodeskFeedService} from '../../EvodeskFeed/services/EvodeskFeed.service';
import {CompetenceRESTService} from '../../../app/EvodeskRESTApi/services/CompetenceREST.service';
import {EvodeskAppMessageBusService} from '../../../app/EvodeskAppMessageBus/services/EvodeskAppMessageBus.service';

export type ProfileOfCurrentUser = undefined;

@Injectable()
export class EvodeskProfileBootstrapService
{
    constructor(
        private state: EvodeskProfileStateService,
        private feedState: EvodeskFeedStateService,
        private feed: EvodeskFeedService,
        private appBus: EvodeskAppMessageBusService,
        private currentUserService: CurrentUserService,
        private profileRESTService: ProfileRESTService,
        private profileEducationRESTService: ProfileEducationRESTService,
        private profileExperienceRESTService: ProfileWorkExperienceRESTService,
        private competenceRESTService: CompetenceRESTService,
    ) {}

    bootstrap(id: number | ProfileOfCurrentUser): Observable<void> {
        const unsubscribe$: Subject<void> = new Subject<void>();

        const profileId: number = id === undefined ? this.currentUserService.current.id : id;
        const isOwner: boolean = profileId.toString() === this.currentUserService.current.id.toString();

        const bootstrapQuery: EvodeskProfileStateBootstrapQuery = <any>{};

        bootstrapQuery.isOwnProfile = isOwner;
        bootstrapQuery.viewerRelations = { followers: [], following: [] };
        bootstrapQuery.relations = { followers: [], following: [] };

        return Observable.create((done) => {
            const queries: Array<Observable<any>> = [];

            queries.push(Observable.create(q => {
                this.profileRESTService.getProfile(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.profile = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.competenceRESTService.indexes(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.indexes = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.competenceRESTService.getCompetenceOfProfile(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.competences = httpResponse;

                        if (httpResponse.length) {
                            const cMetadataQueries: Array<Observable<any>> = [];

                            httpResponse.forEach((userCompetence) => {
                                cMetadataQueries.push(this.competenceRESTService.getSkillsOfCurrentProfile(userCompetence.id).pipe(
                                    tap((skills) => {
                                        bootstrapQuery.competenceSkills = [...(bootstrapQuery.competenceSkills || []), ...skills];
                                    }),
                                ));

                                cMetadataQueries.push(this.competenceRESTService.getDocumentsOfCurrentProfile(userCompetence.id).pipe(
                                    tap((documents) => {
                                        bootstrapQuery.competenceDocuments = [...(bootstrapQuery.competenceDocuments || []), ...documents];
                                    }),
                                ));
                            });

                            forkJoin(cMetadataQueries).subscribe(
                                () => {
                                    q.next(undefined);
                                    q.complete();
                                },
                                (httpError) => {
                                    q.error(httpError);
                                },
                            );
                        } else {
                            q.next(undefined);
                            q.complete();
                        }
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.profileRESTService.getProfileAchievements(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.achievements = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.profileRESTService.getFollowers(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        const followers: Array<EvodeskProfileStateRelation> = httpResponse.followers;

                        if (isOwner) {
                            bootstrapQuery.relations.followers = followers;
                            bootstrapQuery.viewerRelations.followers = followers;
                        } else {
                            bootstrapQuery.relations.followers = followers;
                        }

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.profileRESTService.getFollowing(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        const following: Array<EvodeskProfileStateRelation> = httpResponse.following;

                        if (isOwner) {
                            bootstrapQuery.relations.following = following;
                            bootstrapQuery.viewerRelations.following = following;
                        } else {
                            bootstrapQuery.relations.following = following;
                        }

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.profileEducationRESTService.getEducation(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.educations = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.profileExperienceRESTService.getExperienceOfProfile(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                    (httpResponse) => {
                        bootstrapQuery.experiences = httpResponse;

                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                this.feed.loadMore().pipe(takeUntil(unsubscribe$)).subscribe(
                    () => {
                        q.next(undefined);
                        q.complete();
                    },
                    (httpError) => {
                        q.error(httpError);
                    },
                );
            }));

            queries.push(Observable.create(q => {
                if (profileId === this.currentUserService.current.id) {
                    const current: UserModel = this.currentUserService.current;

                    this.feedState.dispatch({
                        type: EvodeskFeedStateAction.Setup,
                        payload: {
                            isOwnProfile: true,
                            profile: current,
                        },
                    });

                    q.next(undefined);
                    q.complete();
                } else {
                    this.profileRESTService.getProfile(profileId).pipe(takeUntil(unsubscribe$)).subscribe(
                        (profile) => {
                            this.feedState.dispatch({
                                type: EvodeskFeedStateAction.Setup,
                                payload: {
                                    isOwnProfile: profileId === this.currentUserService.current.id,
                                    profile: profile,
                                },
                            });

                            q.next(undefined);
                            q.complete();
                        },
                        (httpError) => {
                            q.error(httpError);
                        },
                    );
                }
            }));

            if (! isOwner) {
                queries.push(Observable.create(q => {
                    this.profileRESTService.getFollowers(this.currentUserService.current.id).pipe(takeUntil(unsubscribe$)).subscribe(
                        (httpResponse) => {
                            bootstrapQuery.viewerRelations.followers = httpResponse.followers;

                            q.next(undefined);
                            q.complete();
                        },
                        (httpError) => {
                            q.error(httpError);
                        },
                    );
                }));

                queries.push(Observable.create(q => {
                    this.profileRESTService.getFollowing(this.currentUserService.current.id).pipe(takeUntil(unsubscribe$)).subscribe(
                        (httpResponse) => {
                            bootstrapQuery.viewerRelations.following = httpResponse.following;

                            q.next(undefined);
                            q.complete();
                        },
                        (httpError) => {
                            q.error(httpError);
                        },
                    );
                }));
            }

            forkJoin(queries).pipe(takeUntil(unsubscribe$)).subscribe(
                () => {
                    this.state.dispatch({
                        type: EvodeskProfileStateAction.Bootstrap,
                        payload: bootstrapQuery,
                    });

                    done.next(undefined);
                    done.complete();
                },
                (queryError) => {
                    done.error(queryError);

                    this.appBus.dispatch({
                        type: AppEvent.HttpError,
                        payload: {
                            reason: queryError,
                        },
                    });
                },
            );

            return () => {
                unsubscribe$.next(undefined);
            };
        });
    }
}
