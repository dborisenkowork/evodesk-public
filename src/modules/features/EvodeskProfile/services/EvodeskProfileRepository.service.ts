import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';

import {of as observableOf, Observable} from 'rxjs';
import {delay, retryWhen, mergeMap, refCount, publishLast} from 'rxjs/operators';

import {ProfileAvatar, ProfileId, ProfileModel} from '../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {ProfileRESTService} from '../../../app/EvodeskRESTApi/services/ProfileREST.service';

@Injectable()
export class EvodeskProfileRepositoryService
{
    public static RETRY_DELAY: number = 500;

    private _cache: { [profileId: number]: Observable<ProfileModel> } = [];
    private _cacheAvatar: { [profileId: number]: Observable<ProfileAvatar> } = [];

    constructor(
        private profileRESTService: ProfileRESTService,
    ) {}

    getProfile(profileId: ProfileId): Observable<ProfileModel> {
        if (! this._cache[profileId]) {
            this._cache[profileId] = Observable.create(response => {
                this.profileRESTService.getProfile(profileId).pipe(
                    retryWhen(error => {
                        return error.pipe(
                            mergeMap((httpError: HttpErrorResponse) => {
                                return observableOf(httpError.status).pipe(delay(EvodeskProfileRepositoryService.RETRY_DELAY));
                            }));
                    }))
                    .subscribe((httpResponse) => {
                        response.next(httpResponse);
                        response.complete();
                    });
            }).pipe(publishLast(), refCount());
        }

        return this._cache[profileId];
    }

    getProfileAvatar(profileId: ProfileId): Observable<ProfileAvatar> {
        if (! this._cacheAvatar[profileId]) {
            this._cacheAvatar[profileId] = Observable.create(response => {
                this.getProfile(profileId).subscribe((profile) => {
                    response.next(profile.avatar);
                    response.complete();
                });
            }).pipe(publishLast(), refCount());
        }

        return this._cacheAvatar[profileId];
    }

    cacheProfileAvatar(profileId: ProfileId, avatar: ProfileAvatar): void {
        this._cacheAvatar[profileId] = observableOf(avatar);
    }
}
