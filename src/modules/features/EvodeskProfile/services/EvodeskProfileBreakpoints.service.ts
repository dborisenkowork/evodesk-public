import {Injectable} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';

import {filter, takeUntil} from 'rxjs/operators';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {ProfileLayoutVariant} from '../models/EvodeskProfile.model';

import {evodeskProfileLayoutDesktop, evodeskProfileLayoutMobile, evodeskProfileLayoutTablet} from '../configs/EvodeskProfile.constants';

@Injectable()
export class EvodeskProfileBreakpointsService
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private _current$: BehaviorSubject<ProfileLayoutVariant> = new BehaviorSubject<ProfileLayoutVariant>(ProfileLayoutVariant.Desktop);

    constructor(
        private breakpointObserver: BreakpointObserver,
    ) {}

    init(): Observable<ProfileLayoutVariant> {
        this.breakpointObserver.observe(`(max-width: ${evodeskProfileLayoutMobile.to}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe((e) => {
            this._current$.next(ProfileLayoutVariant.Mobile);
        });

        this.breakpointObserver.observe(`(min-width: ${evodeskProfileLayoutTablet.from}px) and (max-width: ${evodeskProfileLayoutTablet.to}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this._current$.next(ProfileLayoutVariant.Tablet);
        });

        this.breakpointObserver.observe(`(min-width: ${evodeskProfileLayoutDesktop.from}px)`).pipe(takeUntil(this.ngOnDestroy$), filter(e => e.matches)).subscribe(() => {
            this._current$.next(ProfileLayoutVariant.Desktop);
        });

        return this.current$;
    }

    destroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    get current$(): Observable<ProfileLayoutVariant> {
        return this._current$.asObservable();
    }
}
