import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';

@Injectable()
export class EvodeskProfileCompetenceMarkBodyParserService
{
    parseToHtml(input: string): Observable<string> {
        return Observable.create(observer => {
            if (!! input) {
                let result: string = input;

                result = result.replace(/(?:\r\n|\r|\n)/g, '<br/>');

                observer.next(result);
                observer.complete();
            } else {
                observer.next('');
                observer.complete();
            }
        });
    }
}
