import {ViewMode} from '../../../../types/ViewMode.types';

export interface ProfileRelationModel {
    id: number;
    email: string;
    name: string | null;
    position: string | null;
}

export enum UserCompetenceMark {
    Plus = 'plus',
    Minus = 'minus',
}

export {ViewMode as ProfileLayoutVariant};
