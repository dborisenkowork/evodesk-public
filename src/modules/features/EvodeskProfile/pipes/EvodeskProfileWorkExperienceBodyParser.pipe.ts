import {Pipe, PipeTransform} from '@angular/core';

import {EvodeskProfileWorkExperienceBodyParserService} from '../services/EvodeskProfileWorkExperienceBodyParser.service';

@Pipe({
    name: 'evodeskProfileWorkExperienceBodyParser',
})
export class EvodeskProfileWorkExperienceBodyParserPipe implements PipeTransform
{
    constructor(
        private bodyParser: EvodeskProfileWorkExperienceBodyParserService,
    ) {}

    transform(value: any, ...args: Array<any>): any {
        return this.bodyParser.parseToHtml(value);
    }
}
