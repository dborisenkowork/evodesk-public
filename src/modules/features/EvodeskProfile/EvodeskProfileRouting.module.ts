import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {EvodeskProfileRouting} from './configs/EvodeskProfileRouting.config';

@NgModule({
    imports: [
        RouterModule.forChild(EvodeskProfileRouting),
    ],
    exports: [
        RouterModule,
    ],
})
export class EvodeskProfileRoutingModule
{}
