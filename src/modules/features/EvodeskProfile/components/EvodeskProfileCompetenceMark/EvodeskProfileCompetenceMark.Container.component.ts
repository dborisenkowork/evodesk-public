import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject, Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskProfileCompetenceMarkCompetenceMarkEvent, EvodeskProfileCompetenceMarkCompetenceProps} from './EvodeskProfileCompetenceMark.component';

import {proxy} from '../../../../../functions/proxy.function';

import {UserCompetenceMark} from '../../models/EvodeskProfile.model';
import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';
import {CompetenceMarkModel} from '../../../../app/EvodeskRESTApi/models/CompetenceMark.model';

import {EvodeskProfileStateAction, EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';
import {CompetenceRESTService} from '../../../../app/EvodeskRESTApi/services/CompetenceREST.service';

interface State {
    ready: boolean;
    props?: EvodeskProfileCompetenceMarkCompetenceProps;
}

@Component({
    selector: 'evodesk-profile-competence-mark-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-profile-competence-mark [props]="state.props" (close)="onClose()" (mark)="onMark($event)"></evodesk-profile-competence-mark>
        </ng-container>
    `,
})
export class EvodeskProfileCompetenceMarkContainerComponent implements OnDestroy, OnChanges
{
    @Input() mark: UserCompetenceMark;
    @Input() userCompetence: UserCompetenceModel;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('success') successEvent: EventEmitter<CompetenceMarkModel> = new EventEmitter<CompetenceMarkModel>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private profileState: EvodeskProfileStateService,
        private competenceRESTService: CompetenceRESTService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (this.mark !== undefined && this.userCompetence !== undefined) {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    mark: this.mark,
                    userCompetence: this.userCompetence,
                    submitting: false,
                },
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    onClose(): void {
        this.closeEvent.emit(undefined);
    }

    onMark($event: EvodeskProfileCompetenceMarkCompetenceMarkEvent): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                submitting: true,
            },
        };

        const httpQuery$: Observable<any> = (() => {
            if ($event.mark === UserCompetenceMark.Plus) {
                return this.competenceRESTService.plusMarkToUserCompetence(
                    $event.userCompetence.user_id, $event.userCompetence.competence.id, $event.comment,
                );
            } else if ($event.mark === UserCompetenceMark.Minus) {
                return this.competenceRESTService.minusMarkToUserCompetence(
                    $event.userCompetence.user_id, $event.userCompetence.competence.id, $event.comment,
                );
            } else {
                throw new Error('Unknown mark');
            }
        })();

        proxy(httpQuery$).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
            (mark) => {
                this.competenceRESTService.getCompetenceOfProfile($event.userCompetence.user_id).pipe(takeUntil(this.ngOnDestroy$)).subscribe(
                    (userCompetences) => {
                        this.profileState.dispatch({
                            type: EvodeskProfileStateAction.UpdateUserCompetences,
                            payload: {
                                userCompetences: userCompetences,
                            },
                        });

                        this.successEvent.emit(mark);
                        this.closeEvent.emit(undefined);
                    },
                );
            },
            () => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        submitting: false,
                    },
                };

                this.cdr.detectChanges();
            },
        );
    }
}
