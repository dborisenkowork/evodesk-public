import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {UserCompetenceMark} from '../../models/EvodeskProfile.model';
import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';

import {evodeskMaxUserCompetenceCommentLength, evodeskMinUserCompetenceCommentLength} from '../../configs/EvodeskProfile.constants';

interface State {
    ready: boolean;
    form: FormGroup;
}

interface Props {
    mark: UserCompetenceMark;
    userCompetence: UserCompetenceModel;
    submitting: boolean;
}

interface MarkEvent {
    userCompetence: UserCompetenceModel;
    comment: string;
    mark: UserCompetenceMark;
}

interface FormValue {
    comment: string;
}

export {Props as EvodeskProfileCompetenceMarkCompetenceProps};
export {MarkEvent as EvodeskProfileCompetenceMarkCompetenceMarkEvent};

@Component({
    selector: 'evodesk-profile-competence-mark',
    templateUrl: './EvodeskProfileCompetenceMark.component.pug',
    styleUrls: [
        './EvodeskProfileCompetenceMark.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileCompetenceMarkComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('mark') markEvent: EventEmitter<MarkEvent> = new EventEmitter<MarkEvent>();

    public state: State = {
        ready: false,
        form: this.fb.group({
            comment: ['', [Validators.required, Validators.minLength(evodeskMinUserCompetenceCommentLength), Validators.maxLength(evodeskMaxUserCompetenceCommentLength)]],
        }),
    };

    constructor(
        private fb: FormBuilder,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };

                if (this.props.submitting) {
                    this.state.form.disable();
                } else {
                    this.state.form.enable();
                }
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    closeIfNotSubmitting(): void {
        if (! this.props.submitting) {
            this.close();
        }
    }

    get canSubmit(): boolean {
        return this.state.form.valid;
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    submit(): void {
        if (this.canSubmit) {
            this.markEvent.emit({
                comment: this.formValue.comment,
                mark: this.props.mark,
                userCompetence: this.props.userCompetence,
            });

            this.close();
        }
    }
}
