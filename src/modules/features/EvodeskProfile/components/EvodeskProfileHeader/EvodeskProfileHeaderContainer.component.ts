import {Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskProfileHeaderComponentProps} from './EvodeskProfileHeader.component';
import {EvodeskProfileFollowersModalComponent} from '../EvodeskProfileFollowers/EvodeskProfileFollowersModal.component';
import {EvodeskProfileFollowingModalComponent} from '../EvodeskProfileFollowing/EvodeskProfileFollowingModal.component';
import {IMStartDialogContainerComponent} from '../../../EvodeskIM/shared/components/IMStartDialog/IMStartDialogContainer.component';

import {defaultBottomMatDialogConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';

import {ProfileModel} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

interface State {
    ready: boolean;
    props?: EvodeskProfileHeaderComponentProps;
    profile?: ProfileModel;
    dialogRef?: MatDialogRef<any> | undefined;
}

@Component({
    selector: 'evodesk-profile-header-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-profile-header
          [props]="state.props"
          (following)="openFollowingModal()"
          (followers)="openFollowerModal()"
          (sendMessage)="openStartDialogModal()"
        ></evodesk-profile-header>
      </ng-container>
    `,
})
export class EvodeskProfileHeaderContainerComponent implements OnInit, OnDestroy
{
    private closeModal$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private router: Router,
        private matDialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private profileStateService: EvodeskProfileStateService,
    ) {}

    ngOnInit(): void {
        this.profileStateService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(s => {
            if (s.current) {
                this.state = {
                    ...this.state,
                    ready: true,
                    profile: s.current.profile,
                    props: {
                        ...this.state.props,
                        profileId: s.current.profile.id,
                        profileImage: s.current.profile.avatar === null ? undefined : s.current.profile.avatar,
                        profileName: !!s.current.profile.name ? s.current.profile.name : s.current.profile.email,
                        profilePost: !!s.current.profile.position ? s.current.profile.position : undefined,
                        profileLevel: s.current.profile.level,
                        profileProgress: s.current.profile.currentExpPercent,
                        isOwnProfile: s.current.isOwnProfile,
                        isSubscribed: s.current.viewerRelations.following.filter(f => f.id.toString() === s.current.profile.id.toString()).length > 0,
                        numSubscribers: s.current.relations.followers.length,
                        numSubscriptions: s.current.relations.following.length,
                        canEditAvatar: s.current.isOwnProfile,
                        indexes: s.current.indexes,
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        });
    }

    ngOnDestroy(): void {
        this.closeModal$.next(undefined);
        this.ngOnDestroy$.next(undefined);

        if (this.state.dialogRef) {
            this.state.dialogRef.close();
        }
    }

    openFollowingModal(): void {
        this.state.dialogRef = this.matDialog.open(EvodeskProfileFollowingModalComponent, {
            ...defaultBottomMatDialogConfig,
            viewContainerRef: this.viewContainerRef,
        });

        this.state.dialogRef.afterClosed().pipe(takeUntil(this.closeModal$)).subscribe(() => {
            this.closeModal$.next(undefined);

            this.state = {
                ...this.state,
                dialogRef: undefined,
            };
        });
    }

    openFollowerModal(): void {
        this.state.dialogRef = this.matDialog.open(EvodeskProfileFollowersModalComponent, {
            ...defaultBottomMatDialogConfig,
            viewContainerRef: this.viewContainerRef,
        });

        this.state.dialogRef.afterClosed().pipe(takeUntil(this.closeModal$)).subscribe(() => {
            this.closeModal$.next(undefined);

            this.state = {
                ...this.state,
                dialogRef: undefined,
            };
        });
    }

    openStartDialogModal(): void {
        const dialogRef: MatDialogRef<IMStartDialogContainerComponent> = this.matDialog.open(IMStartDialogContainerComponent, {
            ...defaultBottomMatDialogConfig,
            viewContainerRef: this.viewContainerRef,
        });

        dialogRef.componentInstance.addRecipient(this.state.profile);

        dialogRef.componentInstance.closeEvent.pipe(takeUntil(this.closeModal$)).subscribe((e) => {
            dialogRef.close();
        });

        dialogRef.componentInstance.successEvent.pipe(takeUntil(this.closeModal$)).subscribe((e) => {
            dialogRef.close();

            this.closeModal$.next(undefined);

            this.state = {
                ...this.state,
                dialogRef: undefined,
            };

            this.router.navigate([`/im`], {
                queryParams: <any>{
                    dialog: e.dialogId,
                },
            });
        });

        this.state.dialogRef = dialogRef;

        this.state.dialogRef.afterClosed().pipe(takeUntil(this.closeModal$)).subscribe(() => {
            this.closeModal$.next(undefined);

            this.state = {
                ...this.state,
                dialogRef: undefined,
            };
        });
    }
}
