import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {CompetenceIndexesModel} from '../../../../app/EvodeskRESTApi/models/CompetenceIndexes.model';

interface State {
    ready: boolean;
}

export interface EvodeskProfileHeaderComponentProps {
    profileId: number;
    profileImage: string | undefined;
    profileName: string;
    profilePost: string;
    profileLevel: number;
    profileProgress: number; // %
    isOwnProfile: boolean;
    isSubscribed: boolean;
    numSubscribers: number;
    numSubscriptions: number;
    canEditAvatar: boolean;
    indexes: CompetenceIndexesModel;
}

@Component({
    selector: 'evodesk-profile-header',
    templateUrl: './EvodeskProfileHeader.component.pug',
    styleUrls: [
        './EvodeskProfileHeader.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileHeaderComponent implements OnChanges
{
    @Input() props: EvodeskProfileHeaderComponentProps;

    @Output() following: EventEmitter<void> = new EventEmitter<void>();
    @Output() followers: EventEmitter<void> = new EventEmitter<void>();
    @Output('sendMessage') sendMessageEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get hasAvailableActions(): boolean {
        return this.canSubscribe || this.canSendMessage;
    }

    get canSubscribe(): boolean {
        return ! this.props.isOwnProfile;
    }

    get canSendMessage(): boolean {
        return ! this.props.isOwnProfile;
    }

    get pluralSubscribers(): string {
        const numSubscribers: string = this.props.numSubscribers.toString();
        const lastDigit: string = numSubscribers[numSubscribers.length - 1];

        if (numSubscribers === '1') {
            return 'F1';
        } else if (this.props.numSubscribers >= 10 && this.props.numSubscribers <= 19) {
            return 'F0156789D';
        } else if (!!~['2', '3', '4'].indexOf(lastDigit)) {
            return 'F234';
        } else {
            return 'F0156789D';
        }
    }

    get pluralSubscriptions(): string {
        const numSubscriptions: string = this.props.numSubscriptions.toString();
        const lastDigit: string = numSubscriptions[numSubscriptions.length - 1];

        if (numSubscriptions === '1') {
            return 'F1';
        } else if (this.props.numSubscriptions >= 10 && this.props.numSubscriptions <= 19) {
            return 'F0156789D';
        } else if (!!~['2', '3', '4'].indexOf(lastDigit)) {
            return 'F234';
        } else {
            return 'F0156789D';
        }
    }

    formatDouble(input: number): string {
        return input.toFixed(1);
    }

    sendMessage(): void {
        this.sendMessageEvent.emit(undefined);
    }
}
