import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {proxy} from '../../../../../functions/proxy.function';

import {EvodeskProfileWorkExperienceEntryComponentLayoutVariant as LayoutVariant, EvodeskProfileWorkExperienceEntryComponentProps as Props} from './EvodeskProfileWorkExperienceEntry.component';

import {ProfileExperienceModel} from '../../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';
import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {EvodeskProfileStateAction, EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';
import {CurrentUserService} from '../../../EvodeskAuth/services/CurrentUser.service';
import {EvodeskProfileUrlService} from '../../shared/services/EvodeskProfileUrl.service';
import {ProfileWorkExperienceRESTService} from '../../../../app/EvodeskRESTApi/services/ProfileWorkExperienceREST.service';

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-profile-work-experience-entry-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-profile-work-experience-entry
            [props]="state.props"
            (approveAccept)="approveAccept()"
            (approveReject)="approveReject()"
            (approveNotNow)="approveNotNow()"
          ></evodesk-profile-work-experience-entry>
        </ng-container>
    `,
})
export class EvodeskProfileWorkExperienceEntryContainerComponent implements OnChanges, OnDestroy
{
    @Input() experience: ProfileExperienceModel;
    @Input() layoutVariant: LayoutVariant = LayoutVariant.Short;

    @Output('approveAccepted') approveAcceptedEvent: EventEmitter<ProfileExperienceModel> = new EventEmitter<ProfileExperienceModel>();
    @Output('approveRejected') approveRejectedEvent: EventEmitter<ProfileExperienceModel> = new EventEmitter<ProfileExperienceModel>();
    @Output('approveNotNow') approveNotNowEvent: EventEmitter<ProfileExperienceModel> = new EventEmitter<ProfileExperienceModel>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private profileState: EvodeskProfileStateService,
        private currentUser: CurrentUserService,
        private urlProfile: EvodeskProfileUrlService,
        private restWorkExperience: ProfileWorkExperienceRESTService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['experience']) {
            if (this.experience) {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        experience: this.experience,
                        layoutVariant: this.layoutVariant,
                        approving: false,
                        currentProfileId: this.currentUser.impersonatedAs.id,
                        profileUrls: (() => {
                            const ids: Array<ProfileId> = [];

                            this.experience.approves.forEach((a) => {
                                ids.push(parseInt(<any>a.user_id, 10));
                            });

                            return ids.map((id) => {
                                return {
                                    profileId: id,
                                    url: this.urlProfile.getRouterLinkByProfileId(id),
                                };
                            });
                        })(),
                    },
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                    props: undefined,
                };
            }
        }

        if (changes['layoutVariant']) {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    layoutVariant: this.layoutVariant,
                },
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    approveAccept(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                approving: true,
            },
        };

        proxy(this.restWorkExperience.acceptApproveRequest(this.experience.id, this.experience.approves.filter(a => a.user_id === this.currentUser.impersonatedAs.id)[0].id))
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe(
                () => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            approving: false,
                        },
                    };

                    this.profileState.dispatch({
                        type: EvodeskProfileStateAction.WorkExperienceAcceptApprove,
                        payload: {
                            from: this.currentUser.impersonatedAs.id,
                            experienceId: this.experience.id,
                        },
                    });

                    this.approveAcceptedEvent.emit(this.experience);
                },
                () => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            approving: false,
                        },
                    };
                },
            );
    }

    approveReject(): void {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                approving: true,
            },
        };

        proxy(this.restWorkExperience.rejectApproveRequest(this.experience.id, this.experience.approves.filter(a => a.user_id === this.currentUser.impersonatedAs.id)[0].id))
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe(
                () => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            approving: false,
                        },
                    };

                    this.profileState.dispatch({
                        type: EvodeskProfileStateAction.WorkExperienceRejectApprove,
                        payload: {
                            from: this.currentUser.impersonatedAs.id,
                            experienceId: this.experience.id,
                        },
                    });

                    this.approveRejectedEvent.emit(this.experience);
                },
                () => {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            approving: true,
                        },
                    };
                },
            );
    }

    approveNotNow(): void {
        this.approveNotNowEvent.emit(this.experience);
    }
}
