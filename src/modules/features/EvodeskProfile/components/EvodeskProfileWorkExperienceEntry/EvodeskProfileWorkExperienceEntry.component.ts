import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import * as moment from 'moment';

import {WorkCompetenceApproveModel} from '../../../../app/EvodeskRESTApi/models/WorkCompetenceApprove.model';
import {parseWorkExperienceJSON, ProfileExperienceJSON, ProfileExperienceModel} from '../../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';
import {ProfileId} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

import {capitalizeFirstLetter} from '../../../../../functions/ucfirst.functions';

enum LayoutVariant {
    Short = 'short',
    Expanded = 'expanded',
    DecideModal = 'decide-modal',
}

interface Props {
    experience: ProfileExperienceModel;
    layoutVariant: LayoutVariant;
    currentProfileId: ProfileId;
    profileUrls: Array<{ profileId: ProfileId, url: Array<any> }>;
    approving: boolean;
}

interface State {
    ready: boolean;
    visibility?: EnabledBlocks;
    json?: ProfileExperienceJSON;
}

interface EnabledBlocks {
    url: boolean;
    dates: boolean;
    responsibilities: boolean;
    tools: boolean;
    skills: boolean;
    results: boolean;
    approvesList: boolean;
    approvesForm: boolean;
    approvesFormNotNow: boolean;
}

export {Props as EvodeskProfileWorkExperienceEntryComponentProps};
export {LayoutVariant as EvodeskProfileWorkExperienceEntryComponentLayoutVariant};

const blockConfigs: Array<{ lv: LayoutVariant; config: EnabledBlocks }> = [
    {
        lv: LayoutVariant.Short,
        config: {
            url: false,
            dates: true,
            responsibilities: true,
            tools: false,
            skills: false,
            results: false,
            approvesList: false,
            approvesForm: false,
            approvesFormNotNow: false,
        },
    },
    {
        lv: LayoutVariant.Expanded,
        config: {
            url: true,
            dates: true,
            responsibilities: true,
            tools: true,
            skills: true,
            results: true,
            approvesList: true,
            approvesForm: true,
            approvesFormNotNow: false,
        },
    },
    {
        lv: LayoutVariant.DecideModal,
        config: {
            url: true,
            dates: true,
            responsibilities: true,
            tools: true,
            skills: true,
            results: true,
            approvesList: true,
            approvesForm: true,
            approvesFormNotNow: true,
        },
    },
];

@Component({
    selector: 'evodesk-profile-work-experience-entry',
    templateUrl: './EvodeskProfileWorkExperienceEntry.component.pug',
    styleUrls: [
        './EvodeskProfileWorkExperienceEntry.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileWorkExperienceEntryComponent implements OnChanges
{
    @Input() props: Props;

    @Output('approveAccept') approveAcceptEvent: EventEmitter<ProfileExperienceModel> = new EventEmitter<ProfileExperienceModel>();
    @Output('approveReject') approveRejectEvent: EventEmitter<ProfileExperienceModel> = new EventEmitter<ProfileExperienceModel>();
    @Output('approveNotNow') approveNotNowEvent: EventEmitter<ProfileExperienceModel> = new EventEmitter<ProfileExperienceModel>();

    public state: State = {
        ready: false,
        visibility: blockConfigs[0].config,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    visibility: blockConfigs.filter(b => b.lv === this.props.layoutVariant)[0].config,
                    json: parseWorkExperienceJSON(this.props.experience.text),
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }


    get dateStart(): string {
        return capitalizeFirstLetter(moment(this.props.experience.date_start).format('MMMM, YYYY'));
    }

    get dateFinish(): string {
        if (this.props.experience.date_finish) {
            return capitalizeFirstLetter(moment(this.props.experience.date_finish).format('MMMM, YYYY'));
        } else {
            return 'EvodeskProfile.components.EvodeskProfileWorkExperienceEntry.UntilToday';
        }
    }

    get hasPendingRequest(): boolean {
        return this.props.experience.approves.filter(a => a.user_id === this.props.currentProfileId && a.status.toString() === '0').length === 1;
    }

    get acceptedApproves(): Array<WorkCompetenceApproveModel> {
        return this.props.experience.approves.filter(a => a.status.toString() === '1');
    }

    get hasAcceptedApproves(): boolean {
        return this.acceptedApproves.length > 0;
    }

    approveAccept(): void {
        this.approveAcceptEvent.emit(this.props.experience);
    }

    approveReject(): void {
        this.approveRejectEvent.emit(this.props.experience);
    }

    approveNotNow(): void {
        this.approveNotNowEvent.emit(this.props.experience);
    }

    urlProfile(profileId: ProfileId): Array<any> {
        return this.props.profileUrls.filter(p => p.profileId === profileId)[0].url;
    }
}
