import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskProfileFeedComponentProps} from './EvodeskProfileFeed.component';

import {EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';

interface State {
    ready: boolean;
    page: number;
    props?: EvodeskProfileFeedComponentProps;
}

@Component({
    selector: 'evodesk-profile-feed-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-profile-feed [props]="state.props"></evodesk-profile-feed>
      </ng-container>
    `,
})
export class EvodeskProfileFeedContainerComponent implements OnDestroy, OnInit
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        page: 0,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private profileStateService: EvodeskProfileStateService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    ngOnInit(): void {
        this.profileStateService.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(s => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                },
            };

            this.cdr.detectChanges();
        });
    }
}
