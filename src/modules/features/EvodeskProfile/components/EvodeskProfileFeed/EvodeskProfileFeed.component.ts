import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {PostModel} from '../../../../app/EvodeskRESTApi/models/post/Post.model';

interface State {
    ready: boolean;
}

export interface EvodeskProfileFeedComponentProps {
    posts: Array<PostModel>;
    hasPostsToLoad: boolean;
    loadingMore: boolean;
}

@Component({
    selector: 'evodesk-profile-feed',
    templateUrl: './EvodeskProfileFeed.component.pug',
    styleUrls: [
        './EvodeskProfileFeed.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileFeedComponent implements OnChanges
{
    @Input() props: EvodeskProfileFeedComponentProps;

    @Output('removed') removedEvent: EventEmitter<PostModel> = new EventEmitter<PostModel>();
    @Output('removedError') removedErrorEvent: EventEmitter<PostModel> = new EventEmitter<PostModel>();
    @Output('loadMore') loadMoreEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    loadMore(): void {
        this.loadMoreEvent.emit(undefined);
    }
}
