import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, ViewContainerRef} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';
import {UserCompetenceMark} from '../../models/EvodeskProfile.model';
import {CompetenceMarkModel} from '../../../../app/EvodeskRESTApi/models/CompetenceMark.model';

import {EvodeskProfileCompetencePointsComponentProps} from './EvodeskProfileCompetencePoints.component';

import {EvodeskProfileCompetenceModalsService} from '../../services/EvodeskProfileCompetenceModals.service';


interface State {
    ready: boolean;
    props?: EvodeskProfileCompetencePointsComponentProps;
}

@Component({
    selector: 'evodesk-profile-competence-points-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-profile-competence-points
          [props]="state.props"
          (plus)="plus($event)"
          (minus)="minus($event)"
        ></evodesk-profile-competence-points>
      </ng-container>
    `,
})
export class EvodeskProfileCompetencePointsContainerComponent implements OnChanges, OnDestroy
{
    @Input() userCompetence: UserCompetenceModel;

    @Output('success') successEvent: EventEmitter<CompetenceMarkModel> = new EventEmitter<CompetenceMarkModel>();

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private viewContainerRef: ViewContainerRef,
        private competenceModals: EvodeskProfileCompetenceModalsService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (this.userCompetence) {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    userCompetence: this.userCompetence,
                },
            };
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    plus(): void {
        this.competenceModals.mark(this.userCompetence, UserCompetenceMark.Plus, this.viewContainerRef).pipe(takeUntil(this.ngOnChanges$)).subscribe((mark) => {
            this.successEvent.emit(mark);
        });
    }

    minus(): void {
        this.competenceModals.mark(this.userCompetence, UserCompetenceMark.Minus, this.viewContainerRef).pipe(takeUntil(this.ngOnChanges$)).subscribe((mark) => {
            this.successEvent.emit(mark);
        });
    }
}
