import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';

interface Props {
    userCompetence: UserCompetenceModel;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskProfileCompetencePointsComponentProps};

@Component({
    selector: 'evodesk-profile-competence-points',
    templateUrl: './EvodeskProfileCompetencePoints.component.pug',
    styleUrls: [
        './EvodeskProfileCompetencePoints.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileCompetencePointsComponent implements OnChanges
{
    @Input() props: Props;

    @Output('plus') plusEvent: EventEmitter<UserCompetenceModel> = new EventEmitter<UserCompetenceModel>();
    @Output('minus') minusEvent: EventEmitter<UserCompetenceModel> = new EventEmitter<UserCompetenceModel>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    plus(userCompetence: UserCompetenceModel): void {
        this.plusEvent.emit(userCompetence);
    }

    minus(userCompetence: UserCompetenceModel): void {
        this.minusEvent.emit(userCompetence);
    }
}
