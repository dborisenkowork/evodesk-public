import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';

import * as moment from 'moment';

import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';

import {EvodeskProfileCompetenceDocumentsCompetenceProps} from './EvodeskProfileCompetenceDocuments.component';
import {EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';
import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';
import {distinctUntilChanged, takeUntil} from 'rxjs/operators';
import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../types/Backend.types';
import {CompetenceRESTService} from '../../../../app/EvodeskRESTApi/services/CompetenceREST.service';
import {UserCompetenceDocumentStatus} from '../../../../app/EvodeskRESTApi/models/UserCompetenceDocument.model';

interface State {
    ready: boolean;
    props?: EvodeskProfileCompetenceDocumentsCompetenceProps;
}

@Component({
    selector: 'evodesk-profile-competence-documents-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-profile-competence-documents [props]="state.props" (close)="close()"></evodesk-profile-competence-documents>
        </ng-container>
    `,
})
export class EvodeskProfileCompetenceDocumentsContainerComponent implements OnDestroy, OnChanges
{
    @Input() asModal: boolean = false;
    @Input() userCompetence: UserCompetenceModel;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private profileState: EvodeskProfileStateService,
        private evodeskDates: EvodeskDateService,
        private competenceRESTService: CompetenceRESTService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (this.userCompetence) {
            this.profileState.current$.pipe(
                takeUntil(this.ngOnChanges$),
                distinctUntilChanged(),
            ).subscribe((s) => {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        userCompetence: this.userCompetence,
                        loading: false,
                        asModal: this.asModal,
                        documents: (() => {
                            return s.current.competenceDocuments.filter(d => d.competence_id.id.toString() === this.userCompetence.competence.id.toString() && d.status === UserCompetenceDocumentStatus.Accepted).map((d) => {
                                return {
                                    document: d,
                                    createdAt: this.evodeskDates.diffFromNowV3(moment(d.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate()),
                                    url: this.competenceRESTService.getDocumentUrl(d.id),
                                };
                            });
                        })(),
                    },
                };
            });
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
