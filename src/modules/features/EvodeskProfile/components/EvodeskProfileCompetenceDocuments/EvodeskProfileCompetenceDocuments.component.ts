import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';
import {Observable} from 'rxjs/internal/Observable';
import {UserCompetenceDocumentModel} from '../../../../app/EvodeskRESTApi/models/UserCompetenceDocument.model';

interface State {
    ready: boolean;
}

interface Props {
    userCompetence: UserCompetenceModel;
    documents: Array<{
        document: UserCompetenceDocumentModel;
        createdAt: Observable<string>;
        url: string;
    }>;
    loading: boolean;
    asModal: boolean;
}

export {Props as EvodeskProfileCompetenceDocumentsCompetenceProps};

@Component({
    selector: 'evodesk-profile-competence-documents',
    templateUrl: './EvodeskProfileCompetenceDocuments.component.pug',
    styleUrls: [
        './EvodeskProfileCompetenceDocuments.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileCompetenceDocumentsComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get containerCSSClasses(): any {
        return {
            'as-default': ! this.props.asModal,
            'as-modal': this.props.asModal,
        };
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
