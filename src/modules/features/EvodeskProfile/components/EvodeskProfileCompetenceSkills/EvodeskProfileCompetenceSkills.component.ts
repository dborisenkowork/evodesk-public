import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

import {UserCompetenceSkillModel} from '../../../../app/EvodeskRESTApi/models/UserCompetenceSkill.model';

@Component({
    selector: 'evodesk-profile-competence-skills',
    templateUrl: './EvodeskProfileCompetenceSkills.component.pug',
    styleUrls: [
        './EvodeskProfileCompetenceSkills.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileCompetenceSkillsComponent
{
    @Input() skills: Array<UserCompetenceSkillModel>;
}
