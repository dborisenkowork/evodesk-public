import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

export interface EvodeskProfileFollowNotificationModalComponentProps {
    followed: boolean;
    profileId: number;
    profileName: string;
    profileImage: string | undefined | null;
    profilePost: string;
}

@Component({
    selector: 'evodesk-profile-follow-notification-modal',
    templateUrl: './EvodeskProfileFollowNotificationModal.component.pug',
    styleUrls: [
        './EvodeskProfileFollowNotificationModal.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileFollowNotificationModalComponent {
    constructor(
        @Inject(MAT_DIALOG_DATA) public props: EvodeskProfileFollowNotificationModalComponentProps,
    ) {}
}
