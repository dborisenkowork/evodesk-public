import {Component, Input, OnDestroy} from '@angular/core';

import {Subject} from 'rxjs';

import {EvodeskFeedPostEditorFormContainerComponentProps} from '../../../EvodeskFeed/components/EvodeskFeedPostEditorForm/EvodeskFeedPostEditorFormContainer.component';

interface State {
    ready: boolean;
    props?: EvodeskFeedPostEditorFormContainerComponentProps;
}

@Component({
    selector: 'evodesk-profile-feed-post-editor-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-feed-post-editor-form-container [enableAutoFocus]="enableAutoFocus"></evodesk-feed-post-editor-form-container>
      </ng-container>
    `,
})
export class EvodeskProfileFeedPostEditorContainerComponent implements OnDestroy
{
    @Input() enableAutoFocus: boolean = true;

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
    };

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
