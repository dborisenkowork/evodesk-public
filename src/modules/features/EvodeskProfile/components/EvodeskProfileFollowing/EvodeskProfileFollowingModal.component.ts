import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
    template: `
        <evodesk-profile-following-container (close)="close()"></evodesk-profile-following-container>
    `,
})
export class EvodeskProfileFollowingModalComponent
{
    constructor(
        private matDialogRef: MatDialogRef<EvodeskProfileFollowingModalComponent>,
    ) {}

    close(): void {
        this.matDialogRef.close();
    }
}
