import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import * as moment from 'moment';

import {Observable} from 'rxjs';

import {BACKEND_DATE_FORMAT_AS_MOMENT} from '../../../../../types/Backend.types';

import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';
import {CompetenceMarkModel} from '../../../../app/EvodeskRESTApi/models/CompetenceMark.model';

import {EvodeskDateService} from '../../../../app/EvodeskApp/services/EvodeskDate.service';
import {EvodeskProfileCompetenceMarkBodyParserService} from '../../services/EvodeskProfileCompetenceMarkBodyParser.service';

enum Filter {
    OnlyNegative,
    OnlyPositive,
}

interface State {
    ready: boolean;
    filter: Filter | undefined;
}

interface Props {
    page: number;
    hasMoreMarksToLoad: boolean;
    marks: Array<CompetenceMarkModel>;
    userCompetence: UserCompetenceModel;
    loading: boolean;
    asModal: boolean;
}

export {Props as EvodeskProfileCompetenceMarksCompetenceProps};

@Component({
    selector: 'evodesk-profile-competence-marks',
    templateUrl: './EvodeskProfileCompetenceMarks.component.pug',
    styleUrls: [
        './EvodeskProfileCompetenceMarks.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileCompetenceMarksComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('next') nextEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        filter: undefined,
    };

    constructor(
        private dates: EvodeskDateService,
        private bodyParser: EvodeskProfileCompetenceMarkBodyParserService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    next(): void {
        this.nextEvent.emit(undefined);
    }

    get containerCSSClasses(): any {
        return {
            'as-default': ! this.props.asModal,
            'as-modal': this.props.asModal,
        };
    }

    get positiveMarks(): Array<CompetenceMarkModel> {
        return this.props.marks.filter(m => m.mark.toString() === '1');
    }

    get negativeMarks(): Array<CompetenceMarkModel> {
        return this.props.marks.filter(m => m.mark.toString() === '0');
    }

    get numMarks(): number {
        return this.props.marks.length;
    }

    get numPositiveMarks(): number {
        return this.positiveMarks.length;
    }

    get numNegativeMarks(): number {
        return this.negativeMarks.length;
    }

    get marks(): Array<CompetenceMarkModel> {
        if (this.isNoneFilterActive) {
            return this.props.marks;
        } else if (this.isPositiveFilterActive) {
            return this.positiveMarks;
        } else if (this.isNegativeFilterActive) {
            return this.negativeMarks;
        }
    }

    get noMarksMessage(): string {
        if (this.isNoneFilterActive) {
            return 'EvodeskProfile.components.EvodeskProfileCompetenceMarks.NoMarks';
        } else if (this.isPositiveFilterActive) {
            return 'EvodeskProfile.components.EvodeskProfileCompetenceMarks.NoMarksNegativeFilter';
        } else if (this.isNegativeFilterActive) {
            return 'EvodeskProfile.components.EvodeskProfileCompetenceMarks.NoMarksPositiveFilter';
        }
    }

    get isNoneFilterActive(): boolean {
        return this.state.filter === undefined;
    }

    get isPositiveFilterActive(): boolean {
        return this.state.filter === Filter.OnlyPositive;
    }

    get isNegativeFilterActive(): boolean {
        return this.state.filter === Filter.OnlyNegative;
    }

    getMarkCSSClasses(mark: CompetenceMarkModel): any {
        return {
            'is-positive': mark.mark.toString() === '1',
            'is-negative': mark.mark.toString() === '0',
        };
    }

    setNoFilter(): void {
        this.state = {
            ...this.state,
            filter: undefined,
        };
    }

    setPositiveOnlyFilter(): void {
        this.state = {
            ...this.state,
            filter: Filter.OnlyPositive,
        };
    }

    setNegativeOnlyFilter(): void {
        this.state = {
            ...this.state,
            filter: Filter.OnlyNegative,
        };
    }

    getMarkDate(mark: CompetenceMarkModel): Observable<string> {
        return this.dates.diffFromNowV3(moment(mark.created_at, BACKEND_DATE_FORMAT_AS_MOMENT).toDate());
    }

    getMarkBody(mark: CompetenceMarkModel): Observable<string> {
        return this.bodyParser.parseToHtml(mark.comment);
    }
}
