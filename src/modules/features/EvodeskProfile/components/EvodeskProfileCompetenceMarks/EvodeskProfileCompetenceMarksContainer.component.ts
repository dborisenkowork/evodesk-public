import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';
import {CompetenceMarkModel} from '../../../../app/EvodeskRESTApi/models/CompetenceMark.model';

import {EvodeskProfileCompetenceMarksCompetenceProps} from './EvodeskProfileCompetenceMarks.component';

import {CompetenceRESTService} from '../../../../app/EvodeskRESTApi/services/CompetenceREST.service';

interface State {
    ready: boolean;
    props?: EvodeskProfileCompetenceMarksCompetenceProps;
}

const marksPerPage: number = 10;

@Component({
    selector: 'evodesk-profile-competence-marks-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-profile-competence-marks [props]="state.props" (close)="close()" (next)="next()"></evodesk-profile-competence-marks>
        </ng-container>
    `,
})
export class EvodeskProfileCompetenceMarksContainerComponent implements OnDestroy, OnChanges
{
    @Input() asModal: boolean = false;
    @Input() userCompetence: UserCompetenceModel;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private competenceRESTService: CompetenceRESTService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['userCompetence']) {
            this.ngOnChanges$.next(undefined);
        }

        if (this.userCompetence) {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    page: 1,
                    asModal: this.asModal,
                    userCompetence: this.userCompetence,
                    loading: false,
                    hasMoreMarksToLoad: true,
                    marks: [],
                },
            };

            this.next();
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    pushMark(mark: CompetenceMarkModel) {
        this.state = {
            ...this.state,
            props: {
                ...this.state.props,
                marks: [...this.state.props.marks, mark],
            },
        };

        this.cdr.detectChanges();
    }

    next(): void {
        this.state = { ...this.state, props: { ...this.state.props, loading: true }};

        this.competenceRESTService.getMarksOfUserCompetence(this.userCompetence.id,  { limit: marksPerPage + 1, offset: (this.state.props.page - 1) * marksPerPage }).pipe(
            takeUntil(this.ngOnChanges$),
        ).subscribe(
            (marks) => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        loading: false,
                        marks: [...this.state.props.marks, ...marks.slice(0, marksPerPage)],
                        hasMoreMarksToLoad: marks.length > marksPerPage,
                        page: this.state.props.page + 1,
                    },
                };

                this.cdr.detectChanges();
            },
            () => {
                this.state = {
                    ...this.state,
                    props: {
                        ...this.state.props,
                        loading: false,
                    },
                };

                this.cdr.detectChanges();
            },
        );
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
