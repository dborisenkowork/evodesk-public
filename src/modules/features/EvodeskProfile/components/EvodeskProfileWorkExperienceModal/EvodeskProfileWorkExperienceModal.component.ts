import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import * as moment from 'moment';

import {ProfileExperienceModel} from '../../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';

interface State {
    ready: boolean;
    sorted: Array<ProfileExperienceModel>;
}

export interface Props {
    experiences: Array<ProfileExperienceModel>;
}

export {Props as EvodeskProfileWorkExperienceModalComponentProps};

@Component({
    selector: 'evodesk-profile-work-experience-modal',
    templateUrl: './EvodeskProfileWorkExperienceModal.component.pug',
    styleUrls: [
        './EvodeskProfileWorkExperienceModal.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileWorkExperienceModalComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        sorted: [],
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    sorted: [...this.props.experiences],
                };

                this.state.sorted.sort((a, b) => {
                    const aDate: Date = moment(a.date_start).toDate();
                    const bDate: Date = moment(b.date_start).toDate();

                    if (aDate < bDate) {
                        return 1;
                    } else if (aDate > bDate) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    get experiences(): Array<ProfileExperienceModel> {
        return this.state.sorted;
    }
}
