import {ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {EvodeskProfileWorkExperienceModalComponentProps} from './EvodeskProfileWorkExperienceModal.component';

import {EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';

interface State {
    ready: boolean;
    props?: EvodeskProfileWorkExperienceModalComponentProps;
}

@Component({
    selector: 'evodesk-profile-work-experience-modal-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-profile-work-experience-modal
          [props]="state.props"
          (close)="close()"
        ></evodesk-profile-work-experience-modal>
      </ng-container>
    `,
})
export class EvodeskProfileWorkExperienceModalContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private profileState: EvodeskProfileStateService,
    ) {}

    ngOnInit(): void {
        this.profileState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter((s) => !!s && !!s.current),
        ).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    experiences: s.current.experiences,
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
