import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {MatDialog} from '@angular/material';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskProfilePhotosComponentProps} from './EvodeskProfilePhotos.component';

import {defaultDialogConfig} from '../../../EvodeskAlbums/EvodeskDialogConfig';

import {Photo} from '../../../../app/EvodeskRESTApi/models/album/Album.model';

import {EvodeskViewAllAlbumsComponent} from '../../../EvodeskAlbums/components/EvodeskViewAllAlbums/EvodeskViewAllAlbums.component';
import {dialogConfig, EvodeskViewPhotoComponent} from '../../../EvodeskAlbums/components/EvodeskViewPhoto/EvodeskViewPhoto.component';

import {EvodeskAlbumsStateService} from '../../../EvodeskAlbums/services/EvodeskAlbumsState.service';
import {EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';

interface State {
    ready: boolean;
    props?: EvodeskProfilePhotosComponentProps;
}

@Component({
    selector: 'evodesk-profile-photos-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-profile-photos [props]="state.props" (openPhotosDialog)="openPhotosDialog()" (openPhotoView)="openPhotoView($event)"></evodesk-profile-photos>
      </ng-container>
    `,
})
export class EvodeskProfilePhotosContainerComponent implements OnInit, OnDestroy
{
    public state: State = {
        ready: false,
    };

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private cdr: ChangeDetectorRef,
        private dialog: MatDialog,
        private viewContainerRef: ViewContainerRef,
        private albumsState: EvodeskAlbumsStateService,
        private profileState: EvodeskProfileStateService,
    ) {}

    ngOnInit(): void {
        this.albumsState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            const photos: Array<Photo> = [];

            s.current.albums.map(album => {
                photos.push(...album.photos);
            });

            photos.sort((a, b) => b.id - a.id);

            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    photos: photos,
                },
            };

            this.cdr.detectChanges();
        });

        this.profileState.current$.pipe(takeUntil(this.ngOnDestroy$)).subscribe((s) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    canAddPhotos: s.current.isOwnProfile,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    openPhotosDialog(): void {
        this.dialog.open(EvodeskViewAllAlbumsComponent, {
            ...defaultDialogConfig,
            viewContainerRef: this.viewContainerRef,
            hasBackdrop: true,
        });
    }

    openPhotoView(index: number) {
        this.dialog.open(EvodeskViewPhotoComponent, {
            ...dialogConfig,
            viewContainerRef: this.viewContainerRef,
            data: {photos: this.state.props.photos, index: index},
            backdropClass: '',
        });
    }
}
