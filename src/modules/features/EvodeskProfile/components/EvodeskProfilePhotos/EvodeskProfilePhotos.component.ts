import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {SwiperConfigInterface} from 'ngx-swiper-wrapper';

import {environment} from '../../../../../environments/environment.config';

import {Photo} from '../../../../app/EvodeskRESTApi/models/album/Album.model';

interface State {
    ready: boolean;
    swiperConfig: SwiperConfigInterface;
}

export interface EvodeskProfilePhotosComponentProps {
    canAddPhotos: boolean;
    photos: Array<Photo>;
}

@Component({
    selector: 'evodesk-profile-photos',
    templateUrl: './EvodeskProfilePhotos.component.pug',
    styleUrls: [
        './EvodeskProfilePhotos.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfilePhotosComponent implements OnChanges {
    public static IMAGES_PER_SCREEN: number = 4;

    public state: State = {
        ready: true,
        swiperConfig: {
            slidesPerView: EvodeskProfilePhotosComponent.IMAGES_PER_SCREEN,
        },
    };

    @Input() props: EvodeskProfilePhotosComponentProps;

    @Output('openPhotosDialog') openPhotosDialogEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('openPhotoView') openPhotoViewEvent: EventEmitter<number> = new EventEmitter<number>();

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    openPhotosDialog(): void {
        this.openPhotosDialogEvent.emit(undefined);
    }

    openPhotoView(index: number) {
        this.openPhotoViewEvent.emit(index);
    }

    getPhotoPreview(photo: Photo): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/albums/preview/${photo.id}`;
    }
}
