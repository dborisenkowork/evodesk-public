import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, takeUntil} from 'rxjs/operators';

import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';

import {EvodeskProfileCompetenceDetailsCompetenceProps} from './EvodeskProfileCompetenceDetails.component';

import {EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';

interface State {
    ready: boolean;
    props?: EvodeskProfileCompetenceDetailsCompetenceProps;
}

@Component({
    selector: 'evodesk-profile-competence-details-container',
    template: `
        <ng-container *ngIf="!! state && state.ready">
          <evodesk-profile-competence-details [props]="state.props" (close)="close()"></evodesk-profile-competence-details>
        </ng-container>
    `,
})
export class EvodeskProfileCompetenceDetailsContainerComponent implements OnDestroy, OnChanges
{
    @Input() userCompetence: UserCompetenceModel;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    private ngOnChanges$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private profileState: EvodeskProfileStateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnChanges$.next(undefined);

        if (this.userCompetence) {
            this.profileState.current$.pipe(
                takeUntil(this.ngOnChanges$),
                filter(s => !! s.current),
                distinctUntilChanged(),
            ).subscribe((s) => {
                this.state = {
                    ...this.state,
                    ready: true,
                    props: {
                        userCompetence: this.userCompetence,
                        loading: false,
                        skills: s.current.competenceSkills.filter(skill => skill.competence_id.toString() === this.userCompetence.competence.id.toString()),
                    },
                };
            });
        } else {
            this.state = {
                ...this.state,
                ready: false,
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnChanges$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
