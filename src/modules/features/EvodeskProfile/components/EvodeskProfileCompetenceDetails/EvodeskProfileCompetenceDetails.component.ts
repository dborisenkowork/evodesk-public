import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';

import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';
import {UserCompetenceSkillModel} from '../../../../app/EvodeskRESTApi/models/UserCompetenceSkill.model';

import {EvodeskProfileCompetenceMarksContainerComponent} from '../EvodeskProfileCompetenceMarks/EvodeskProfileCompetenceMarksContainer.component';

interface State {
    ready: boolean;
}

interface Props {
    userCompetence: UserCompetenceModel;
    skills: Array<UserCompetenceSkillModel>;
    loading: boolean;
}

export {Props as EvodeskProfileCompetenceDetailsCompetenceProps};

@Component({
    selector: 'evodesk-profile-competence-details',
    templateUrl: './EvodeskProfileCompetenceDetails.component.pug',
    styleUrls: [
        './EvodeskProfileCompetenceDetails.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileCompetenceDetailsComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('marks') marks: EvodeskProfileCompetenceMarksContainerComponent;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
