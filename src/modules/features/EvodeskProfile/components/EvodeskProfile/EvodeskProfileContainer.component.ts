import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskProfileComponentProps} from './EvodeskProfile.component';

import {EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';
import {EvodeskFeedStateService} from '../../../EvodeskFeed/state/EvodeskFeedState.service';

interface State {
    ready: boolean;
    props?: EvodeskProfileComponentProps;
}

@Component({
    selector: 'evodesk-profile-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-profile [props]="state.props" (checkLoadMore)="checkLoadMore()"></evodesk-profile>
      </ng-container>
    `,
})
export class EvodeskProfileContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private profileStateService: EvodeskProfileStateService,
        private evodeskFeedState: EvodeskFeedStateService,
    ) {}

    ngOnInit(): void {
        this.profileStateService.current$.pipe(
            takeUntil(this.ngOnDestroy$))
            .subscribe(s => {
                if (!! s.current)  {
                    this.state = {
                        ...this.state,
                        ready: true,
                        props: {
                            isOwnProfile: s.current.isOwnProfile,
                        },
                    };
                } else {
                    this.state = {
                        ready: false,
                    };
                }
            });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    @HostListener('window:resize.out-zone')
    onWindowResize(): void {
        this.evodeskFeedState.checkLoadMore();
    }

    checkLoadMore(): void {
        this.evodeskFeedState.checkLoadMore();
    }
}
