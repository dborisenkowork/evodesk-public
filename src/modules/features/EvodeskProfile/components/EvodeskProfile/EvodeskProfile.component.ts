import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

interface State {
    ready: boolean;
}

export interface EvodeskProfileComponentProps {
    isOwnProfile: boolean;
}

@Component({
    selector: 'evodesk-profile',
    templateUrl: './EvodeskProfile.component.pug',
    styleUrls: [
        './EvodeskProfile.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileComponent implements OnChanges
{
    @Input() props: EvodeskProfileComponentProps;

    @Output('checkLoadMore') checkLoadMoreEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    onScroll() {
        this.checkLoadMoreEvent.emit(undefined);
    }
}
