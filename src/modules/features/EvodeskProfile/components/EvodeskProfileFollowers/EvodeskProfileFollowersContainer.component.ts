import {ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

import {Subject} from 'rxjs';
import {takeUntil, filter} from 'rxjs/operators';

import {EvodeskProfileFollowersComponentProps} from './EvodeskProfileFollowers.component';

import {proxy} from '../../../../../functions/proxy.function';

import {ProfileRelationModel} from '../../models/EvodeskProfile.model';

import {EvodeskProfileStateAction, EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';
import {EvodeskConfirmModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskConfirmModal/EvodeskConfirmModal.service';
import {ProfileRESTService} from '../../../../app/EvodeskRESTApi/services/ProfileREST.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';

interface State {
    ready: boolean;
    props?: EvodeskProfileFollowersComponentProps;
}

@Component({
    selector: 'evodesk-profile-followers-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-profile-followers
            [props]="state.props"
            (close)="closeEvent.emit(undefined)"
            (open)="onOpen($event)"
            (remove)="onRemove($event)"
        ></evodesk-profile-followers>
      </ng-container>
    `,
})
export class EvodeskProfileFollowersContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private router: Router,
        private cdr: ChangeDetectorRef,
        private uiAlertService: EvodeskAlertModalService,
        private uiConfirmService: EvodeskConfirmModalService,
        private profileState: EvodeskProfileStateService,
        private profileRESTService: ProfileRESTService,
    ) {}

    ngOnInit(): void {
        this.profileState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !! s.current && !! s.current.relations),
        ).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    followers: s.current.relations.followers,
                    canRemove: s.current.isOwnProfile,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onOpen(relation: ProfileRelationModel): void {
        this.closeEvent.emit(undefined);

        setTimeout(() => {
            this.router.navigate([`/profile/${relation.id}`]);
        });
    }

    onRemove(relation: ProfileRelationModel): void {
        this.uiConfirmService.open({
            title: {
                text: 'EvodeskProfile.components.EvodeskProfileFollowers.ConfirmRemove.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskProfile.components.EvodeskProfileFollowers.ConfirmRemove.Text',
                translate: true,
                replaces: [relation.name || relation.email],
            },
            confirmButton: {
                text: 'EvodeskProfile.components.EvodeskProfileFollowers.ConfirmRemove.ConfirmButton',
                translate: true,
            },
            cancelButton: {
                text: 'EvodeskProfile.components.EvodeskProfileFollowers.ConfirmRemove.CancelButton',
                translate: true,
            },
            confirm: (() => {
                this.profileState.dispatch({
                    type: EvodeskProfileStateAction.RemoveFollower,
                    payload: {
                        relationId: relation.id,
                    },
                });

                this.cdr.detectChanges();

                proxy(this.profileRESTService.followerOff(relation.id)).pipe(
                    takeUntil(this.ngOnDestroy$),
                ).subscribe(
                    undefined,
                    () => {
                        this.uiAlertService.open({
                            title: {
                                text: 'EvodeskProfile.components.EvodeskProfileFollowers.FailedToRemove.Title',
                                translate: true,
                            },
                            text: {
                                text: 'EvodeskProfile.components.EvodeskProfileFollowers.FailedToRemove.Text',
                                translate: true,
                                replaces: [relation.name || relation.email],
                            },
                            ok: {
                                text: 'EvodeskProfile.components.EvodeskProfileFollowers.FailedToRemove.Close',
                                translate: true,
                            },
                        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
                    },
                );
            }),
        }).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
    }
}
