import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
    template: `
        <evodesk-profile-followers-container (close)="close()"></evodesk-profile-followers-container>
    `,
})
export class EvodeskProfileFollowersModalComponent
{
    constructor(
        private matDialogRef: MatDialogRef<EvodeskProfileFollowersModalComponent>,
    ) {}

    close(): void {
        this.matDialogRef.close();
    }
}
