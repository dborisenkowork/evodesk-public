import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import {Subject} from 'rxjs';
import {distinctUntilChanged, takeUntil} from 'rxjs/operators';

import {ProfileRelationModel} from '../../models/EvodeskProfile.model';

export interface EvodeskProfileFollowersComponentProps {
    followers: Array<ProfileRelationModel>;
    canRemove: boolean;
}

interface State {
    ready: boolean;
    filtered: Array<ProfileRelationModel> | undefined;
    isSearchBoxActive: boolean;
    searchForm: FormGroup;
}

@Component({
    selector: 'evodesk-profile-followers',
    templateUrl: './EvodeskProfileFollowers.component.pug',
    styleUrls: [
        './EvodeskProfileFollowers.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileFollowersComponent implements OnChanges, OnInit, OnDestroy
{
    @ViewChild('searchInput') searchInputRef: ElementRef;

    @Input() props: EvodeskProfileFollowersComponentProps;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    @Output('open') openEvent: EventEmitter<ProfileRelationModel> = new EventEmitter<ProfileRelationModel>();
    @Output('remove') removeEvent: EventEmitter<ProfileRelationModel> = new EventEmitter<ProfileRelationModel>();

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
        isSearchBoxActive: false,
        filtered: undefined,
        searchForm: this.fb.group({
            query: [''],
        }),
    };

    constructor(
        private fb: FormBuilder,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    ngOnInit(): void {
        this.state.searchForm.valueChanges.pipe(
            takeUntil(this.ngOnDestroy$),
            distinctUntilChanged(),
        ).subscribe((formValue) => {
            if (formValue.query && formValue.query.length > 0) {
                this.doFilter(formValue.query);
            } else {
                this.resetFilter();
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    onSearchBoxClick(): void {
        if (this.searchInputRef) {
            (this.searchInputRef.nativeElement as HTMLInputElement).focus();
        }
    }

    onSearchBoxFocus(): void {
        this.state = {
            ...this.state,
            isSearchBoxActive: true,
        };
    }

    onSearchBoxBlur(): void {
        this.state = {
            ...this.state,
            isSearchBoxActive: false,
        };
    }

    get followers(): Array<ProfileRelationModel> {
        return this.state.filtered === undefined
            ? this.props.followers
            : this.state.filtered;
    }

    get hasNoSearchResults(): boolean {
        return this.state.filtered !== undefined && this.state.filtered.length === 0;
    }

    get hasFollowers(): boolean {
        return this.props.followers.length > 0;
    }

    get hasNoFollowers(): boolean {
        return this.state.filtered === undefined && this.props.followers.length === 0;
    }

    get hasSearchResults(): boolean {
        return this.state.filtered !== undefined && this.state.filtered.length > 0;
    }

    get noSearchAndHasFollowers(): boolean {
        return this.state.filtered === undefined && this.props.followers.length > 0;
    }

    getFollowerName(follower: ProfileRelationModel) {
        return follower.name || follower.email;
    }

    doFilter(input: string): void {
        this.state = {
            ...this.state,
            filtered: this.props.followers.filter(f => !!~(f.name || f.email).toLocaleLowerCase().indexOf(input.toLocaleLowerCase())),
        };
    }

    resetFilter(): void {
        this.state = {
            ...this.state,
            filtered: undefined,
        };
    }

    openProfile(follower: ProfileRelationModel): void {
        this.openEvent.emit(follower);
    }

    remove(follower: ProfileRelationModel): void {
        this.removeEvent.emit(follower);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
