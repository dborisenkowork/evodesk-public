import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';
import {UserCompetenceSkillModel} from '../../../../app/EvodeskRESTApi/models/UserCompetenceSkill.model';
import {ProfileLayoutVariant} from '../../models/EvodeskProfile.model';

interface State {
    ready: boolean;
    sorted: Array<UserCompetenceModel>;
}

export interface EvodeskProfileCompetenceComponentProps {
    skills: Array<UserCompetenceSkillModel>;
    competences: Array<UserCompetenceModel>;
    canEdit: boolean;
    layoutVariant: ProfileLayoutVariant;
}

@Component({
    selector: 'evodesk-profile-competence',
    templateUrl: './EvodeskProfileCompetence.component.pug',
    styleUrls: [
        './EvodeskProfileCompetence.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileCompetenceComponent implements OnChanges
{
    @Input() props: EvodeskProfileCompetenceComponentProps;

    @Output('marks') marksEvent: EventEmitter<UserCompetenceModel> = new EventEmitter<UserCompetenceModel>();
    @Output('documents') documentsEvent: EventEmitter<UserCompetenceModel> = new EventEmitter<UserCompetenceModel>();
    @Output('details') detailsEvent: EventEmitter<UserCompetenceModel> = new EventEmitter<UserCompetenceModel>();

    public state: State = {
        ready: false,
        sorted: [],
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    sorted: [...this.props.competences],
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get containerCSSClasses(): any {
        return {
            '___mobile': this.props.layoutVariant === ProfileLayoutVariant.Mobile,
            '___tablet': this.props.layoutVariant === ProfileLayoutVariant.Tablet,
            '___tablet-and-desktop': this.props.layoutVariant === ProfileLayoutVariant.Tablet || this.props.layoutVariant === ProfileLayoutVariant.Desktop,
            '___desktop': this.props.layoutVariant === ProfileLayoutVariant.Desktop,
        };
    }

    get userCompetences(): Array<UserCompetenceModel> {
        return this.props.competences.filter(uC => uC.active);
    }

    userCompetenceClasses(userCompetence: UserCompetenceModel): any {
        const progress: number = parseInt(userCompetence.points, 10);

        return {
            'progress-low': progress < 3,
            'progress-medium': progress >= 3 && progress < 7,
            'progress-high': progress >= 7,
        };
    }

    skills(userCompetence: UserCompetenceModel): Array<UserCompetenceSkillModel> {
        return this.props.skills.filter(s => s.competence_id === userCompetence.competence.id);
    }

    marks(userCompetence: UserCompetenceModel): void {
        this.marksEvent.emit(userCompetence);
    }

    documents(userCompetence: UserCompetenceModel): void {
        this.documentsEvent.emit(userCompetence);
    }

    details(userCompetence: UserCompetenceModel): void {
        this.detailsEvent.emit(userCompetence);
    }
}
