import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, takeUntil} from 'rxjs/operators';

import {UserCompetenceMark} from '../../models/EvodeskProfile.model';
import {UserCompetenceModel} from '../../../../app/EvodeskRESTApi/models/UserCompetence.model';

import {EvodeskProfileCompetenceComponentProps} from './EvodeskProfileCompetence.component';

import {EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';
import {EvodeskProfileCompetenceModalsService} from '../../services/EvodeskProfileCompetenceModals.service';

interface State {
    ready: boolean;
    props?: EvodeskProfileCompetenceComponentProps;
}

@Component({
    selector: 'evodesk-profile-competence-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-profile-competence
          [props]="state.props"
          (details)="details($event)"
          (marks)="marks($event)"
          (documents)="documents($event)"
        ></evodesk-profile-competence>
      </ng-container>
    `,
})
export class EvodeskProfileCompetenceContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private viewContainerRef: ViewContainerRef,
        private profileState: EvodeskProfileStateService,
        private competenceModals: EvodeskProfileCompetenceModalsService,
    ) {}

    ngOnInit(): void {
        this.profileState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !! s.current),
            distinctUntilChanged(),
        ).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    skills: s.current.competenceSkills,
                    competences: s.current.competences,
                    canEdit: s.current.isOwnProfile,
                    layoutVariant: s.current.layoutVariant,
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    details(userCompetence: UserCompetenceModel): void {
        this.competenceModals.details(userCompetence, UserCompetenceMark.Minus, this.viewContainerRef);
    }

    marks(userCompetence: UserCompetenceModel): void {
        this.competenceModals.marks(userCompetence, UserCompetenceMark.Minus, this.viewContainerRef);
    }

    documents(userCompetence: UserCompetenceModel): void {
        this.competenceModals.documents(userCompetence, UserCompetenceMark.Minus, this.viewContainerRef);
    }
}
