import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import * as moment from 'moment';

import {capitalizeFirstLetter} from '../../../../../functions/ucfirst.functions';

import {ProfileExperienceModel} from '../../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';

interface State {
    ready: boolean;
    limit: number;
    sorted: Array<ProfileExperienceModel>;
}

export interface EvodeskProfileWorkExperienceComponentProps {
    experiences: Array<ProfileExperienceModel>;
    canEdit: boolean;
}

@Component({
    selector: 'evodesk-profile-work-experience',
    templateUrl: './EvodeskProfileWorkExperience.component.pug',
    styleUrls: [
        './EvodeskProfileWorkExperience.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileWorkExperienceComponent implements OnChanges
{
    public static PAGE_SIZE: number = 3;

    @Input() props: EvodeskProfileWorkExperienceComponentProps;

    @Output('showMore') showMoreEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
        limit: EvodeskProfileWorkExperienceComponent.PAGE_SIZE,
        sorted: [],
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                    sorted: [...this.props.experiences],
                };

                this.state.sorted.sort((a, b) => {
                    const aDate: Date = moment(a.date_start).toDate();
                    const bDate: Date = moment(b.date_start).toDate();

                    if (aDate < bDate) {
                        return 1;
                    } else if (aDate > bDate) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    getDateStart(experience: ProfileExperienceModel): string {
        return capitalizeFirstLetter(moment(experience.date_start).format('MMMM, YYYY'));
    }

    getDateFinish(experience: ProfileExperienceModel): string {
        if (experience.date_finish) {
            return capitalizeFirstLetter(moment(experience.date_finish).format('MMMM, YYYY'));
        } else {
            return 'EvodeskProfile.components.EvodeskProfileWorkExperience.UntilToday';
        }
    }

    get experiences(): Array<ProfileExperienceModel> {
        return this.state.sorted.slice(0, this.state.limit);
    }

    showMore(): void {
        this.showMoreEvent.emit(undefined);
    }
}
