import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

import {Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {defaultBottomMatDialog600pxConfig} from '../../../../app/EvodeskApp/configs/EvodeskMatDialog.config';

import {EvodeskProfileWorkExperienceComponentProps} from './EvodeskProfileWorkExperience.component';
import {EvodeskProfileWorkExperienceModalContainerComponent} from '../EvodeskProfileWorkExperienceModal/EvodeskProfileWorkExperienceModalContainer.component';

import {EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';

interface State {
    ready: boolean;
    props?: EvodeskProfileWorkExperienceComponentProps;
}

@Component({
    selector: 'evodesk-profile-work-experience-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-profile-work-experience [props]="state.props" (showMore)="showMore()"></evodesk-profile-work-experience>
      </ng-container>
    `,
})
export class EvodeskProfileWorkExperienceContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private modalClose$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private viewContainerRef: ViewContainerRef,
        private matDialog: MatDialog,
        private profileState: EvodeskProfileStateService,
    ) {}

    ngOnInit(): void {
        this.profileState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !! s.current),
            map(s => s.current.experiences),
            distinctUntilChanged(),
        ).subscribe((experiences) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    experiences: experiences,
                },
            };

            this.cdr.detectChanges();
        });

        this.profileState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter((s) => !! s.current),
            map(s => s.current.isOwnProfile),
            distinctUntilChanged(),
        ).subscribe((isOwnProfile) => {
            this.state = {
                ...this.state,
                props: {
                    ...this.state.props,
                    canEdit: isOwnProfile,
                },
            };

            this.cdr.detectChanges();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
        this.modalClose$.next(undefined);
    }

    showMore(): void {
        const matDialogRef: MatDialogRef<EvodeskProfileWorkExperienceModalContainerComponent> = this.matDialog.open(EvodeskProfileWorkExperienceModalContainerComponent, {
            ...defaultBottomMatDialog600pxConfig,
            viewContainerRef: this.viewContainerRef,
            closeOnNavigation: true,
            autoFocus: false,
        });

        matDialogRef.afterClosed().pipe(takeUntil(this.modalClose$)).subscribe(() => {
            this.modalClose$.next(undefined);
        });

        matDialogRef.componentInstance.closeEvent.pipe(takeUntil(this.modalClose$)).subscribe(() => {
            matDialogRef.close();
        });
    }
}
