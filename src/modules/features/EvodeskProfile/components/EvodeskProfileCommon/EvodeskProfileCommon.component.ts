import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

interface State {
    ready: boolean;
}

export interface EvodeskProfileCommonComponentPropsMetadataEntry {
    title: string;
    value: string;
}

export interface EvodeskProfileCommonComponentProps {
    canEdit: boolean;
    metadata: Array<EvodeskProfileCommonComponentPropsMetadataEntry>;
}

@Component({
    selector: 'evodesk-profile-common',
    templateUrl: './EvodeskProfileCommon.component.pug',
    styleUrls: [
        './EvodeskProfileCommon.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileCommonComponent implements OnChanges
{
    @Input() props: EvodeskProfileCommonComponentProps;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get metadata(): Array<EvodeskProfileCommonComponentPropsMetadataEntry> {
        return this.props.metadata.filter(m => m.value !== undefined && m.value !== null);
    }
}
