import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {EvodeskProfileCommonComponentProps, EvodeskProfileCommonComponentPropsMetadataEntry} from './EvodeskProfileCommon.component';

import {EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';

interface State {
    ready: boolean;
    props?: EvodeskProfileCommonComponentProps;
}

@Component({
    selector: 'evodesk-profile-common-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-profile-common [props]="state.props"></evodesk-profile-common>
      </ng-container>
    `,
})
export class EvodeskProfileCommonContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: true,
    };

    constructor(
        private profileStateService: EvodeskProfileStateService,
    ) {}

    ngOnInit(): void {
        this.profileStateService.current$.pipe(takeUntil(this.ngOnDestroy$))
        .subscribe((s) => {
            const metadata: Array<EvodeskProfileCommonComponentPropsMetadataEntry> = [
                {
                    title: 'EvodeskProfile.components.EvodeskProfileCommon.Metadata.Address',
                    value: s.current.profile.address,
                },
                {
                    title: 'EvodeskProfile.components.EvodeskProfileCommon.Metadata.Email',
                    value: s.current.profile.email,
                },
                {
                    title: 'EvodeskProfile.components.EvodeskProfileCommon.Metadata.Phone',
                    value: s.current.profile.phone
                    ,
                },
                {
                    title: 'EvodeskProfile.components.EvodeskProfileCommon.Metadata.Industry',
                    value: s.current.profile.industry,
                },
            ];

            if (this.profileStateService.snapshot.educations.length > 0) {
                metadata.push({
                    title: 'EvodeskProfile.components.EvodeskProfileCommon.Metadata.Education',
                    value: this.profileStateService.snapshot.educations[this.profileStateService.snapshot.educations.length - 1].name,
                });
            }

            this.state = {
                ...this.state,
                props: {
                    canEdit: s.current.isOwnProfile,
                    metadata: metadata,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
