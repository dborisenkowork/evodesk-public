import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

import {environment} from '../../../../../environments/environment.config';

import {SwiperConfigInterface} from 'ngx-swiper-wrapper/dist/lib/swiper.interfaces';

interface State {
    ready: boolean;
}

export interface EvodeskProfileAchievementsComponentProps {
    profileId: number;
    areOwnAchievements: boolean;
    achievements: Array<{
        image: string;
        title: string;
    }>;
}

@Component({
    selector: 'evodesk-profile-achievements',
    templateUrl: './EvodeskProfileAchievements.component.pug',
    styleUrls: [
        './EvodeskProfileAchievements.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileAchievementsComponent implements OnChanges
{
    @Input() props: EvodeskProfileAchievementsComponentProps;

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    get swiperConfig(): SwiperConfigInterface {
        return {
            slidesPerView: 3,
        };
    }

    getAchievementsImageUrl(input: string): string {
        return `${environment.modules.EvoDeskRESTApi.apiEndpoint}/${input}`;
    }
}
