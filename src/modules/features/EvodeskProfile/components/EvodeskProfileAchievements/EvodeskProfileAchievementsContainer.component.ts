import {Component, OnDestroy, OnInit} from '@angular/core';

import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {EvodeskProfileAchievementsComponentProps} from './EvodeskProfileAchievements.component';

import {EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';

interface State {
    ready: boolean;
    props?: EvodeskProfileAchievementsComponentProps;
}

@Component({
    selector: 'evodesk-profile-achievements-container',
    template: `
      <ng-container *ngIf="!!state && state.ready">
        <evodesk-profile-achievements [props]="state.props"></evodesk-profile-achievements>
      </ng-container>
    `,
})
export class EvodeskProfileAchievementsContainerComponent implements OnInit, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private profileState: EvodeskProfileStateService,
    ) {}

    ngOnInit(): void {
        this.profileState.current$.pipe(
            takeUntil(this.ngOnDestroy$),
            filter(s => !s.previous || ! this.state.props || s.previous.achievements !== s.current.achievements),
        ).subscribe((s) => {
            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    profileId: s.current.profile.id,
                    areOwnAchievements: s.current.isOwnProfile,
                    achievements: s.current.achievements
                        .reverse()
                        .map(a => {
                            return {
                                image: a.image,
                                title: a.title,
                            };
                        })
                    ,
                },
            };
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }
}
