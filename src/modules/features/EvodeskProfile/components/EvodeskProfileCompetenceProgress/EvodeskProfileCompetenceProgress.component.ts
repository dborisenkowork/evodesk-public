import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
    selector: 'evodesk-profile-competence-progress',
    templateUrl: './EvodeskProfileCompetenceProgress.component.pug',
    styleUrls: [
        './EvodeskProfileCompetenceProgress.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileCompetenceProgressComponent
{
    @Input() points: string;

    get userCompetenceClasses(): any {
        const progress: number = parseInt(this.points, 10);

        return {
            'progress-low': progress < 3,
            'progress-medium': progress >= 3 && progress < 7,
            'progress-high': progress >= 7,
        };
    }

    get progressCSSStyles(): any {
        return {
            width: `${Math.max(1, parseInt(this.points, 10) * 10)}%`,
        };
    }
}
