import {Component} from '@angular/core';

import {EvodeskProfileFooterComponentProps} from './EvodeskProfileFooter.component';

import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';

type Props = EvodeskProfileFooterComponentProps;

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-profile-footer-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-profile-footer [props]="state.props" (conditions)="conditions()"></evodesk-profile-footer>
      </ng-container>
    `,
})
export class EvodeskProfileFooterContainerComponent
{
    public state: State = {
        ready: true,
        props: {},
    };

    constructor(
        private uiAlertService: EvodeskAlertModalService,
    ) {}

    conditions(): void {
        this.uiAlertService.open({
            title: {
                text: 'EvodeskLanding.__shared.conditions.Title',
                translate: true,
            },
            text: {
                text: 'EvodeskLanding.__shared.conditions.TextInnerHTML',
                translate: true,
                asHtml: true,
            },
            ok: {
                text: 'EvodeskLanding.__shared.conditions.OK',
                translate: true,
            },
        }).subscribe();
    }
}
