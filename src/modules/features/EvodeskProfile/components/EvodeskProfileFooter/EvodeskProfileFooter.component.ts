import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

export interface EvodeskProfileFooterComponentProps {}

type Props = EvodeskProfileFooterComponentProps;

interface State {
    ready: boolean;
}

@Component({
    selector: 'evodesk-profile-footer',
    templateUrl: './EvodeskProfileFooter.component.pug',
    styleUrls: [
        './EvodeskProfileFooter.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileFooterComponent implements OnChanges
{
    @Input() props: Props;

    @Output('conditions') conditionsEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: true,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    conditions(): void {
        this.conditionsEvent.emit(undefined);
    }
}
