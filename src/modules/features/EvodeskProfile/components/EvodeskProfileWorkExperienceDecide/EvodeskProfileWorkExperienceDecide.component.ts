import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {ProfileExperienceModel} from '../../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';
import {WorkCompetenceApproveModel} from '../../../../app/EvodeskRESTApi/models/WorkCompetenceApprove.model';
import {ProfileModel} from '../../../../app/EvodeskRESTApi/models/profile/Profile.model';

interface Props {
    loading: boolean;
    profile: ProfileModel;
    experience: ProfileExperienceModel;
    approve: WorkCompetenceApproveModel;
}

interface State {
    ready: boolean;
}

export {Props as EvodeskProfileWorkExperienceDecideComponentProps};

@Component({
    selector: 'evodesk-profile-work-experience-decide',
    templateUrl: './EvodeskProfileWorkExperienceDecide.component.pug',
    styleUrls: [
        './EvodeskProfileWorkExperienceDecide.component.scss',
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EvodeskProfileWorkExperienceDecideComponent implements OnChanges
{
    @Input() props: Props;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['props']) {
            if (this.props) {
                this.state = {
                    ...this.state,
                    ready: true,
                };
            } else {
                this.state = {
                    ...this.state,
                    ready: false,
                };
            }
        }
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }
}
