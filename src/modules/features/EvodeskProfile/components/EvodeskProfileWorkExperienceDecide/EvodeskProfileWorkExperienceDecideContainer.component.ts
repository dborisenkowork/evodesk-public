import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subject} from 'rxjs';
import {filter, take, takeUntil} from 'rxjs/operators';

import {ProfileExperienceModel} from '../../../../app/EvodeskRESTApi/models/profile/ProfileExperience.model';
import {WorkCompetenceApproveId, WorkCompetenceApproveModel} from '../../../../app/EvodeskRESTApi/models/WorkCompetenceApprove.model';

import {EvodeskProfileWorkExperienceDecideComponentProps} from './EvodeskProfileWorkExperienceDecide.component';

import {EvodeskProfileStateAction, EvodeskProfileStateService} from '../../services/EvodeskProfileState.service';
import {EvodeskAlertModalService} from '../../../../app/EvodeskApp/components/shared/EvodeskAlertModal/EvodeskAlertModal.service';

type Props = EvodeskProfileWorkExperienceDecideComponentProps;

interface State {
    ready: boolean;
    props?: Props;
}

@Component({
    selector: 'evodesk-profile-work-experience-decide-container',
    template: `
      <ng-container *ngIf="!! state && state.ready">
        <evodesk-profile-work-experience-decide
          [props]="state.props"
          (close)="close()"
        ></evodesk-profile-work-experience-decide>
      </ng-container>
    `,
})
export class EvodeskProfileWorkExperienceDecideContainerComponent implements OnChanges, OnDestroy
{
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    private nextExperienceId$: Subject<void> = new Subject<void>();

    @Input() approveId: WorkCompetenceApproveId;

    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        ready: false,
    };

    constructor(
        private cdr: ChangeDetectorRef,
        private profileState: EvodeskProfileStateService,
        private uiAlertService: EvodeskAlertModalService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['approveId']) {
            this.nextExperienceId$.next(undefined);

            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    loading: true,
                },
            };

            this.profileState.current$.pipe(
                takeUntil(this.ngOnDestroy$),
                filter(s => !! s.current.experiences && s.current.experiences.length > 0),
                take(1),
            ).subscribe((s) => {
                const mapApprovesToExperience: { [approuveId: number]: { approve: WorkCompetenceApproveModel, experience: ProfileExperienceModel } } = {};

                s.current.experiences.forEach((e) => {
                    e.approves.forEach((a) => {
                        mapApprovesToExperience[a.id] = {
                            approve: a,
                            experience: e,
                        };
                    });
                });

                if (mapApprovesToExperience[this.approveId] === undefined) {
                    this.close();
                } else {
                    this.state = {
                        ...this.state,
                        props: {
                            ...this.state.props,
                            loading: false,
                            profile: s.current.profile,
                            experience: mapApprovesToExperience[this.approveId].experience,
                            approve: mapApprovesToExperience[this.approveId].approve,
                        },
                    };
                }

                setTimeout(() => {
                    if (this.state.props.approve.status !== 0) {
                        this.uiAlertService.openTranslated('EvodeskProfile.components.EvodeskProfileWorkExperienceDecideContainerComponent.AlreadyDecided').pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                            this.close();
                        });
                    }
                }, 1000);

                this.cdr.detectChanges();
            });
        }
    }

    ngOnDestroy(): void {
        this.nextExperienceId$.next(undefined);
        this.ngOnDestroy$.next(undefined);
    }

    close(): void {
        this.closeEvent.emit(undefined);
    }

    accept(): void {
        this.state = {
            ...this.state,
            ready: true,
            props: {
                ...this.state.props,
                loading: true,
            },
        };

        setTimeout(() => {
            this.profileState.dispatch({
                type: EvodeskProfileStateAction.WorkExperienceAcceptApprove,
                payload: {
                    from: this.state.props.approve.user_id,
                    experienceId: this.state.props.experience.id,
                },
            });

            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    loading: false,
                },
            };

            this.close();
        }, 5000);
    }

    reject(): void {
        this.state = {
            ...this.state,
            ready: true,
            props: {
                ...this.state.props,
                loading: true,
            },
        };

        setTimeout(() => {
            this.profileState.dispatch({
                type: EvodeskProfileStateAction.WorkExperienceRejectApprove,
                payload: {
                    from: this.state.props.approve.user_id,
                    experienceId: this.state.props.experience.id,
                },
            });

            this.state = {
                ...this.state,
                ready: true,
                props: {
                    ...this.state.props,
                    loading: false,
                },
            };

            this.close();
        }, 5000);

        this.close();
    }

    notNow(): void {
        this.close();
    }
}
