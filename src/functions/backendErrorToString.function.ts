export type BackendError = string | BackendErrorObject;

export interface BackendErrorObject
{
    errors: Array<string>;
}

export interface BackendErrors {
    errors: Array<string>;
}

export function backendErrorToString(input: any) {
    if (typeof input.error === 'string') {
        return (<any>input.error as string) || 'Unknown error';
    } else {
        return (input.error as BackendErrorObject).errors[0] || 'Unknown error';
    }
}
