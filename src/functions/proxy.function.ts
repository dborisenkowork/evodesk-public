import {Observable} from 'rxjs';

export function proxy<T>(input: Observable<T>): Observable<T> {
    let isUnsubscribed: boolean = false;

    return Observable.create(response => {
        input.subscribe(
            (next) => {
                if (! isUnsubscribed) {
                    response.next(next);
                }
            },
            (error) => {
                if (! isUnsubscribed) {
                    response.error(error);
                }
            },
            () => {
                if (! isUnsubscribed) {
                    response.complete();
                }
            },
        );

        return () => {
            isUnsubscribed = true;
        };
    });
}
