let calls: number = 0;

export function iSuspectToBeLoopingInfititely() {
    calls += 1;

    if (calls > 100) {
        /* tslint:disable:no-debugger */
        debugger;
    }
}
