export function capitalizeFirstLetter(input: string) {
    return input.charAt(0).toLocaleUpperCase() + input.slice(1);
}
