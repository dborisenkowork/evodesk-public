export function tidyUrl(input: string): string {
    if (!!input && input[0] === '/') {
        input = input.slice(1);
    }

    return input;
}
