export interface ObjectWithId {
    id: number;
}

export function sortById(input: Array<ObjectWithId>): Array<ObjectWithId> {
    const result: Array<ObjectWithId> = [...input];

    result.sort((a, b) => {
        if (a.id > b.id) {
            return -1;
        } else if (a.id === b.id) {
            return 0;
        } else {
            return 1;
        }
    });

    return result;
}
