export function isScrolledIntoView(el: Element): boolean {
    const rect: ClientRect = el.getBoundingClientRect();
    const elemTop: number = rect.top;
    const elemBottom: number = rect.bottom;

    return (elemTop >= 0) && (elemBottom <= window.innerHeight);
}
